package com.getinsured.hix.webservice.plandisplay.endpoints;

import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.SoapHeader;

import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.model.PdHousehold;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIException.GhixErrors;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Members;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Members.Member;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalResponse;
import com.getinsured.hix.webservice.plandisplay.autorenewal.ProductType;
import com.getinsured.hix.webservice.plandisplay.autorenewal.YesNoVal;
import com.getinsured.plandisplay.service.AutoRenewalService;
import com.getinsured.plandisplay.util.PlanDisplayUtil;
import com.google.gson.Gson;

@Endpoint
public class AutoRenewalEndPoint {
private static final String TARGET_NAMESPACE = "http://webservice.hix.getinsured.com/plandisplay/autoRenewal";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AutoRenewalEndPoint.class);
			
	@Autowired private ZipCodeService zipCodeService;
	@Autowired private AutoRenewalService autoRenewalService;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private PlanDisplayUtil planDisplayUtil;
	@Autowired private Gson platformGson;
	@Autowired private GIMonitorService giMonitorService;
	
	@PayloadRoot(localPart = "autoRenewalRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload AutoRenewalResponse processAutoRenewalInformation(@RequestPayload AutoRenewalRequest autoRenewalRequest, SoapHeader header) 
	{
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside processAutoRenewalInformation---->", null, false);
		AutoRenewalResponse autoRenewalResponse = new AutoRenewalResponse();
		try {
			this.validateRequest(autoRenewalRequest);
			PdHousehold pdHousehold = autoRenewalService.saveEligibility(autoRenewalRequest);
			if (pdHousehold == null || pdHousehold.getShoppingId() == null) {
				throw new GIException(GhixErrors.ERROR_WHILE_GENERATING_SHOPPING_ID);
			}
			String giHouseholdId = pdHousehold.getShoppingId();
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----AutoRenewal shoppingId---->"+giHouseholdId, null, false);
			autoRenewalResponse.setGiHouseholdId(giHouseholdId);
			String renewalPlanId = autoRenewalRequest.getHousehold().getIndividual().getPlanId();
			ProductType productType = autoRenewalRequest.getHousehold().getProductType();
			boolean isOrderCreated = false;
			if(StringUtils.isNotEmpty(renewalPlanId))
			{
				InsuranceType insurancetype = ProductType.H.equals(productType) ? InsuranceType.HEALTH : InsuranceType.DENTAL;
				if(EnrollmentType.S.equals(pdHousehold.getEnrollmentType())){
					isOrderCreated = this.createOrderAndUpdateEnrollment(pdHousehold.getId(), renewalPlanId, autoRenewalRequest, autoRenewalResponse, header,insurancetype);
				}else{
					isOrderCreated = this.createOrderAndEnrollment(pdHousehold.getId(), renewalPlanId, autoRenewalRequest, autoRenewalResponse, header,insurancetype);
				}
			}
			else
			{
				
				String healthEnrollmentId = getEnrollmentId(autoRenewalRequest, InsuranceType.HEALTH.toString());
				if ((ProductType.A.equals(productType) || ProductType.H.equals(productType)) && StringUtils.isNotEmpty(healthEnrollmentId))
				{
					String healthCmsPlanId = getRenewalPlanId(healthEnrollmentId, header);
					if(EnrollmentType.S.equals(pdHousehold.getEnrollmentType())){
						isOrderCreated = this.createOrderAndUpdateEnrollment(pdHousehold.getId(), renewalPlanId, autoRenewalRequest, autoRenewalResponse, header,InsuranceType.HEALTH);
					}else{
						isOrderCreated = this.createOrderAndEnrollment(pdHousehold.getId(), healthCmsPlanId, autoRenewalRequest, autoRenewalResponse, header,InsuranceType.HEALTH);
					}
				}
				
				String dentalEnrollmentId = getEnrollmentId(autoRenewalRequest, InsuranceType.DENTAL.toString());
				if ((ProductType.A.equals(productType) || ProductType.D.equals(productType)) &&  StringUtils.isNotEmpty(dentalEnrollmentId))
				{
					String dentalCmsPlanId = getRenewalPlanId(dentalEnrollmentId, header);										
					if(EnrollmentType.S.equals(pdHousehold.getEnrollmentType())){
						isOrderCreated = this.createOrderAndUpdateEnrollment(pdHousehold.getId(), renewalPlanId, autoRenewalRequest, autoRenewalResponse, header,InsuranceType.DENTAL);
					}else{
						isOrderCreated = this.createOrderAndEnrollment(pdHousehold.getId(), dentalCmsPlanId, autoRenewalRequest, autoRenewalResponse, header,InsuranceType.DENTAL);
					}
				}
			}
			if(isOrderCreated){
				generateAutoRenewalResponse(pdHousehold.getId(), autoRenewalRequest, header, autoRenewalResponse);
			}else{
				autoRenewalResponse.setResponseCode("124");
				autoRenewalResponse.setResponseDescription("Order Creation Failed");			
			}		
		} catch (GIException e) {
			autoRenewalResponse.setResponseDescription(e.getErrorMsg());
			autoRenewalResponse.setResponseCode(e.getErrorCode()+"");
			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----GIException occured while processing autoRenewalRequest---->", e, false);
			String errorMsg = e.getErrorMsg();
			if(autoRenewalRequest != null && autoRenewalRequest.getHousehold() != null && autoRenewalRequest.getHousehold().getIndividual() != null) {
				errorMsg += "Case Id: "+autoRenewalRequest.getHousehold().getIndividual().getHouseholdCaseId();
			}
			giMonitorService.saveOrUpdateErrorLog(e.getErrorCode()+"", new TSDate(), "AutoRenewalEndPoint", ExceptionUtils.getFullStackTrace(e) + "\n\nCaused by: "+errorMsg, null, null, "PLANDISPLAY", null);
		} catch (Exception e) {
			autoRenewalResponse.setResponseDescription("Invalid Data.");
			autoRenewalResponse.setResponseCode("114");
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Exception occured while processing autoRenewalRequest---->", e, false);
			String errorMsg = "Exception occured while processing autoRenewalRequest ";
			if(autoRenewalRequest != null && autoRenewalRequest.getHousehold() != null && autoRenewalRequest.getHousehold().getIndividual() != null) {
				errorMsg += "Case Id: "+autoRenewalRequest.getHousehold().getIndividual().getHouseholdCaseId();
			}
			giMonitorService.saveOrUpdateErrorLog("114", new TSDate(), "AutoRenewalEndPoint", ExceptionUtils.getFullStackTrace(e) + "\n\nCaused by: "+errorMsg, null, null, "PLANDISPLAY", null);
		}
		return autoRenewalResponse;
	}

	private String getRenewalPlanId(String healthEnrollmentId, SoapHeader header) {
		EnrollmentResponse enrollmentResponse = findPlanIdAndOrderItemIdByEnrollmentId(healthEnrollmentId, header);
		String initialHealthCmsPlanId = null;
		if(enrollmentResponse != null && enrollmentResponse.getPlanId() != null && enrollmentResponse.getCmsPlanId() != null )	{
			initialHealthCmsPlanId = enrollmentResponse.getCmsPlanId();
		}
		return initialHealthCmsPlanId;
	}
	
	private EnrollmentResponse findPlanIdAndOrderItemIdByEnrollmentId(String id, SoapHeader header)
	{		
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		enrollmentRequest.setEnrollmentId(Integer.parseInt(id));
		String userName = planDisplayUtil.getCurrentUser(header);
		String postResp = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_PLANID_ORDERITEM_ID_BY_ENROLLMENT_ID_JSON, userName, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollmentRequest)).getBody();
		return platformGson.fromJson(postResp, EnrollmentResponse.class);
	}

	private String getEnrollmentId(AutoRenewalRequest autoRenewalRequest, String insuranceType) {
		List<Member> members =  autoRenewalRequest.getHousehold().getMembers().getMember();
		for(Member member : members)
		{
			if("HEALTH".equals(insuranceType) && StringUtils.isNotBlank(member.getExistingMedicalEnrollmentID()))
			{
				return member.getExistingMedicalEnrollmentID();
			}
			else if("DENTAL".equals(insuranceType) && StringUtils.isNotBlank(member.getExistingSADPEnrollmentID()))
			{
				return member.getExistingSADPEnrollmentID();
			}
		}
		return null;
	}

	private void validateRequest(AutoRenewalRequest autoRenewalRequest) throws GIException {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside AutoRenewal validateRequest---->", null, false);
		
		AutoRenewalRequest.Household.Members members = autoRenewalRequest.getHousehold().getMembers();
		Set<String> memberIds = new HashSet<String>();
		for (com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Members.Member member : members.getMember()) {
			memberIds.add(member.getMemberId());
		}
		
		if (autoRenewalRequest.getHousehold().getResponsiblePerson() != null) {
			if (autoRenewalRequest.getHousehold().getResponsiblePerson().getResponsiblePersonSecondaryPhone() != null && autoRenewalRequest.getHousehold().getResponsiblePerson().getResponsiblePersonPrimaryPhone() == null) {
				throw new GIException(GhixErrors.INVALID_RESPONSIBLE_PERSON_PRIMARY_PHONE);
			}
		}
		
		if (autoRenewalRequest.getHousehold().getHouseHoldContact().getHouseHoldContactSecondaryPhone() != null && autoRenewalRequest.getHousehold().getHouseHoldContact().getHouseHoldContactPrimaryPhone() == null) {
			throw new GIException(GhixErrors.INVALID_HOUSEHOLD_CONTACT_PRIMARY_PHONE);
		}
		
		if (autoRenewalRequest.getHousehold().getUserRoleType() != null && autoRenewalRequest.getHousehold().getUserRoleId() == null) {
			throw new GIException(GhixErrors.INVALID_AGENT_ID);
		}
		
		this.validateMembers(autoRenewalRequest.getHousehold().getMembers(),autoRenewalRequest.getHousehold().getProductType());
		
		//this.validatePlanId(autoRenewalRequest.getHousehold().getIndividual().getPlanId());
		//this.validateEnrollmentId(autoRenewalRequest.getHousehold().getMembers().getMember().get(0).getExistingMedicalEnrollmentID());
		
	}
	
	private void validateMembers(Members members,ProductType productType) throws GIException {
		
		String dentalEnrollmentID = null;
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		
		for(Member member : members.getMember()) {

			if(ProductType.H.equals(productType) && StringUtils.isNotBlank(member.getExistingSADPEnrollmentID()))
			{
				throw new GIException(GhixErrors.INVALID_EXISTING_SADP_ENROLLMENTID_FOR_PRODUCTTYPE);
			}
			else if(ProductType.D.equals(productType) && StringUtils.isNotBlank(member.getExistingMedicalEnrollmentID()))
			{
				throw new GIException(GhixErrors.INVALID_EXISTING_MEDICAL_ENROLLMENTID_FOR_PRODUCTTYPE);
			}
			if(!YesNoVal.Y.equals(member.getNewPersonFLAG()) &&((ProductType.H.equals(productType) && StringUtils.isBlank(member.getExistingMedicalEnrollmentID())) || (ProductType.D.equals(productType) && StringUtils.isBlank(member.getExistingSADPEnrollmentID())) || (ProductType.A.equals(productType) && StringUtils.isBlank(member.getExistingMedicalEnrollmentID()))))
			{
				throw new GIException(GhixErrors.INVALID_ENROLLMENT_ID);
			}
			if(ProductType.A.equals(productType)){
				if(StringUtils.isBlank(dentalEnrollmentID)) {
					dentalEnrollmentID = member.getExistingSADPEnrollmentID() != null ? member.getExistingSADPEnrollmentID().toString() : null;
				}
			}
			//county code validation
			if(member.getCountyCode()!= null && !("".equals(member.getCountyCode()))) {
				//HIX-119187 MN - Bypass check for out of state scenarios
				boolean byPassValidationForCounty = "MN".equalsIgnoreCase(stateCode) && "999".equalsIgnoreCase(member.getCountyCode());
				if (!(zipCodeService.validateZipAndCountyCode(member.getHomeZip(), member.getCountyCode())) && !byPassValidationForCounty) {
					throw new GIException(GhixErrors.INVALID_COUNTY_CODE);
				}				
			}
			
			this.validateCustodialParent(member);
			
			if (member.getSecondaryPhone() != null && member.getPrimaryPhone() == null)	{
				throw new GIException(GhixErrors.INVALID_PRIMARY_PHONE);
			} 
			
			try {
				validateElement(member.getCustodialParentFirstName(), member.getCustodialParentId());
				validateElement(member.getCustodialParentLastName(), member.getCustodialParentId());
				
				if(!"CA".equalsIgnoreCase(stateCode)){
					validateElement(member.getCustodialParentSsn(), member.getCustodialParentId());
					validateElement(member.getCustodialParentPrimaryPhone(), member.getCustodialParentId());
				}				
				validateElement(member.getCustodialParentHomeAddress1(), member.getCustodialParentId());
				validateElement(member.getCustodialParentHomeCity(), member.getCustodialParentId());
				validateElement(member.getCustodialParentHomeState(), member.getCustodialParentId());
				validateElement(member.getCustodialParentHomeZip(), member.getCustodialParentId());
			} catch(GIException giException) {
				giException.setErrorMsg("CustodialParentId missing");
				throw giException;
			}
			
		}
		
		if(ProductType.A.equals(productType) && StringUtils.isBlank(dentalEnrollmentID)){
			throw new GIException(GhixErrors.INVALID_ENROLLMENT_ID);
		}
	}

	private void validateElement(String custodialParentElement, String custodialParentId) throws GIException {
		if (StringUtils.isNotBlank(custodialParentElement) && StringUtils.isBlank(custodialParentId)) {
			throw new GIException(GhixErrors.MISSING_CUSTODIAL_ELEMENT);
		}
	}

	private void validateCustodialParent(Member member) throws GIException {
		String parentId = member.getCustodialParentId();
		if (StringUtils.isNotBlank(parentId)) {
			this.validateCustodialElementsPresent(member);
		}
	}
	
	private void validateCustodialElementsPresent(Member member) throws GIException	{
		GIException giException = new GIException(GhixErrors.MISSING_CUSTODIAL_ELEMENT);
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		if (StringUtils.isBlank(member.getCustodialParentFirstName())) {
			giException.setErrorMsg("CustodialParentFirstName missing");
			throw giException;
		} else if (StringUtils.isBlank(member.getCustodialParentLastName())) {
			giException.setErrorMsg("CustodialParentLastName missing");
			throw giException;
		} else if (StringUtils.isBlank(member.getCustodialParentSsn()) && !"CA".equalsIgnoreCase(stateCode)) {
			giException.setErrorMsg("CustodialParentSsn missing");
			throw giException;
		} else if (StringUtils.isBlank(member.getCustodialParentPrimaryPhone()) && !"CA".equalsIgnoreCase(stateCode)) {
			giException.setErrorMsg("CustodialParentPrimaryPhone missing");
			throw giException;
		} else if (StringUtils.isBlank(member.getCustodialParentHomeAddress1())) {
			giException.setErrorMsg("CustodialParentHomeAddress1 missing");
			throw giException;
		} else if (StringUtils.isBlank(member.getCustodialParentHomeCity())) {
			giException.setErrorMsg("CustodialParentHomeCity missing");
			throw giException;
		} else if (StringUtils.isBlank(member.getCustodialParentHomeState())) {
			giException.setErrorMsg("CustodialParentHomeState missing");
			throw giException;
		} else if (StringUtils.isBlank(member.getCustodialParentHomeZip())) {
			giException.setErrorMsg("CustodialParentHomeZip missing");
			throw giException;
		}		
	}
	
	/**
	 * @deprecated This method is not in use.
	 * @param planId
	 * @throws GIException
	 */
	@Deprecated
	private void validatePlanId(String planId) throws GIException {
		if (planId == null || planId.isEmpty() || planId.length() != 16) {
			throw new GIException(GhixErrors.INVALID_PLAN_ID);
		}
	}
	
	/**
	 * @deprecated This method is not in use.
	 * @param enrollmentId
	 * @throws GIException
	 */
	@Deprecated
	private void validateEnrollmentId(String enrollmentId) throws GIException {
		if (enrollmentId == null || enrollmentId.isEmpty()) {
			throw new GIException(GhixErrors.INVALID_ENROLLMENT_ID);
		}
	}
	
	private boolean createOrderAndUpdateEnrollment(Long householdId, String renewalPlanId, AutoRenewalRequest autoRenewalRequest, AutoRenewalResponse autoRenewalResponse, SoapHeader header,InsuranceType insurancetype) throws GIException
	{
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----START of createOrderForAutoRenewal call---->"+ new Timestamp((new TSDate()).getTime()), null, false);
		long time = System.currentTimeMillis();
		Long pdOrderItemId = autoRenewalService.createOrderForSpecialEnrollment(householdId, insurancetype, autoRenewalRequest.getHousehold().getProductType());
		boolean isOrderCreated = pdOrderItemId!=null?true:false;
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----End of createOrderForAutoRenewal call---->"+ new Timestamp((new TSDate()).getTime()), null, false);
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.WARN, "received createOrderForSpecialEnrollment response for SSAP-Application " + autoRenewalRequest.getHousehold().getApplicationId()+ " in " + (System.currentTimeMillis() - time) + " ms", null, false);
		return isOrderCreated;

	}
	
	private boolean createOrderAndEnrollment(Long householdId, String renewalPlanId, AutoRenewalRequest autoRenewalRequest, AutoRenewalResponse autoRenewalResponse, SoapHeader header,InsuranceType insurancetype) throws GIException
	{
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----START of createOrderForAutoRenewal call---->"+ new Timestamp((new TSDate()).getTime()), null, false);
		long time = System.currentTimeMillis();
		Long pdOrderItemId = autoRenewalService.createOrderForAutoRenewal(householdId, renewalPlanId,insurancetype, autoRenewalRequest.getHousehold().getProductType());
		boolean isOrderCreated = pdOrderItemId!=null?true:false;
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----End of createOrderForAutoRenewal call---->"+ new Timestamp((new TSDate()).getTime()), null, false);
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.WARN, "received createOrderForAutoRenewal response for SSAP-Application " + autoRenewalRequest.getHousehold().getApplicationId()+ " in " + (System.currentTimeMillis() - time) + " ms", null, false);
		return isOrderCreated;
	}

	private void generateAutoRenewalResponse(Long householdId, AutoRenewalRequest autoRenewalRequest, SoapHeader header,  AutoRenewalResponse autoRenewalResponse) throws GIException {
		long time = System.currentTimeMillis();	
		EnrollmentResponse enrollmentResponse = autoRenewalService.createEnrollmentForGivenOrder(householdId, autoRenewalRequest, header);
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.WARN, "received createEnrollmentForGivenOrder response for SSAP-Application " + autoRenewalRequest.getHousehold().getApplicationId()+ " in " + (System.currentTimeMillis() - time) + " ms", null, false);
			
			if("SUCCESS".equals(enrollmentResponse.getStatus()))
			{
				autoRenewalResponse.setResponseCode("200");
				autoRenewalResponse.setResponseDescription("Enrollment Created Successfully.");
			}
			else if("FAILURE".equals(enrollmentResponse.getStatus()))
			{
				if(enrollmentResponse.getErrCode() == 204)
				{
					autoRenewalResponse.setResponseCode("204");
					autoRenewalResponse.setResponseDescription(enrollmentResponse.getErrMsg());
				}
				else if(enrollmentResponse.getErrCode() == 201)
				{
					autoRenewalResponse.setResponseCode("201");
					autoRenewalResponse.setResponseDescription(enrollmentResponse.getErrMsg());
				}
				else if(enrollmentResponse.getErrCode() == 202)
				{
					autoRenewalResponse.setResponseCode("202");
					autoRenewalResponse.setResponseDescription(enrollmentResponse.getErrMsg());
				}
				else if(enrollmentResponse.getErrCode() == 203)
				{
					autoRenewalResponse.setResponseCode("203");
					autoRenewalResponse.setResponseDescription("Failed to create Enrollment");
				}
				else if(enrollmentResponse.getErrCode() == 299)
				{
					autoRenewalResponse.setResponseCode("299");
					autoRenewalResponse.setResponseDescription(enrollmentResponse.getErrMsg());
				}
				else
				{
					autoRenewalResponse.setResponseCode("125");
					autoRenewalResponse.setResponseDescription("Unknown error happened in Enrollment: Code:"+enrollmentResponse.getErrCode()+"Message:"+enrollmentResponse.getErrMsg());
				}
			}
			else
			{
				autoRenewalResponse.setResponseCode("126");
				autoRenewalResponse.setResponseDescription("Error while calling Enrollment Api");
			}
		}
		
}
