package com.getinsured.hix.webservice.plandisplay.payload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.model.PdHousehold;
import com.getinsured.hix.platform.util.payload.GIWSPayloadHanlder;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalResponse;
import com.getinsured.plandisplay.service.PdHouseholdService;

@Component
public class AutoRenewalGIPayloadHandler implements ApplicationContextAware, GIWSPayloadHanlder {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AutoRenewalGIPayloadHandler.class);
	private ApplicationContext context;
	
	@Override
	public void handlePayload(GIWSPayload giwsPayload, Object responseObj) {
		try {
			if(responseObj != null && giwsPayload != null) {
				AutoRenewalResponse autoRenewalResponse = (AutoRenewalResponse) responseObj;
				String shoppingId = autoRenewalResponse.getGiHouseholdId();
				PdHouseholdService pdHouseholdService = (PdHouseholdService) context.getBean("pdHouseholdService");
				PdHousehold pdHousehold = pdHouseholdService.findByShoppingId(shoppingId);
				pdHousehold.setGiWsPayloadId(giwsPayload.getId().longValue());
				pdHouseholdService.save(pdHousehold);
			}
		} catch(Exception e) {
			LOGGER.error("Exception occured in handlePayload: ", e );
		}
	}
	
	@Override
	public void setApplicationContext(ApplicationContext appContext) throws BeansException {
			context=appContext;
	}

}
