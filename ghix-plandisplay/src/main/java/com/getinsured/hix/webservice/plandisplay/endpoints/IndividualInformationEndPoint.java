package com.getinsured.hix.webservice.plandisplay.endpoints;

import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.SoapHeader;

import com.getinsured.hix.model.PdHousehold;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIException.GhixErrors;
import com.getinsured.hix.webservice.plandisplay.individualinformation.EnrollmentVal;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.DisenrollMembers;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.DisenrollMembers.DisenrollMember;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Members;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Members.Member;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationResponse;
import com.getinsured.hix.webservice.plandisplay.individualinformation.ProductType;
import com.getinsured.hix.webservice.plandisplay.individualinformation.YesNoVal;
import com.getinsured.plandisplay.service.PdHouseholdService;
import com.getinsured.plandisplay.util.PlanDisplayUtil;

@Endpoint
public class IndividualInformationEndPoint {
private static final String TARGET_NAMESPACE = "http://webservice.hix.getinsured.com/plandisplay/individualInformation";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IndividualInformationEndPoint.class);
			
	@Autowired private PdHouseholdService pdHouseholdService;
	@Autowired private ZipCodeService zipCodeService;
	@Autowired private GIMonitorService giMonitorService;

	
	@PayloadRoot(localPart = "individualInformationRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload IndividualInformationResponse getHouseholdId(@RequestPayload IndividualInformationRequest individualInformationRequest, SoapHeader header) {
		IndividualInformationResponse individualInformationResponse = new IndividualInformationResponse();
		try {
			this.validateRequest(individualInformationRequest);
			PdHousehold pdHousehold = pdHouseholdService.saveEligibility(individualInformationRequest, header);
			individualInformationResponse.setGiHouseholdId(pdHousehold.getShoppingId());
			individualInformationResponse.setResponseCode("200");
			individualInformationResponse.setResponseDescription("Success");
		} catch (GIException e) {
			individualInformationResponse.setResponseDescription(e.getErrorMsg());
			individualInformationResponse.setResponseCode(e.getErrorCode()+"");
			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----GIException occured while processing individualInformationRequest---->", e, false);
			String errorMsg = e.getErrorMsg();
			if(individualInformationRequest != null && individualInformationRequest.getHousehold() != null && individualInformationRequest.getHousehold().getIndividual() != null) {
				errorMsg += "Case Id: "+individualInformationRequest.getHousehold().getIndividual().getHouseholdCaseId();
			}
			giMonitorService.saveOrUpdateErrorLog(e.getErrorCode()+"", new TSDate(), "IndividualInformationEndPoint", ExceptionUtils.getFullStackTrace(e) + "\n\nCaused by: "+errorMsg, null, null, "PLANDISPLAY", null);
		}catch (Exception e) {
			individualInformationResponse.setResponseDescription("Invalid Data.");
			individualInformationResponse.setResponseCode("114");
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Exception occured while processing individualInformationRequest---->", e, false);
			String errorMsg = "Exception occured while processing individualInformationRequest";
			if(individualInformationRequest != null && individualInformationRequest.getHousehold() != null && individualInformationRequest.getHousehold().getIndividual() != null) {
				errorMsg += "Case Id: "+individualInformationRequest.getHousehold().getIndividual().getHouseholdCaseId();
			}
			giMonitorService.saveOrUpdateErrorLog("114", new TSDate(), "IndividualInformationEndPoint", ExceptionUtils.getFullStackTrace(e) + "\n\nCaused by: "+errorMsg, null, null, "PLANDISPLAY", null);

		}
		return individualInformationResponse;
	}

	private void validateRequest(IndividualInformationRequest individualInformationRequest) throws GIException {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside IndividualInformationEndPoint validateRequest---->", null, false);
		
		IndividualInformationRequest.Household.Members members = individualInformationRequest.getHousehold().getMembers();
		Set<String> memberIds = new HashSet<String>();
		for (Member member : members.getMember()) {
			memberIds.add(member.getMemberId());
		}
		
		if (individualInformationRequest.getHousehold().getResponsiblePerson() != null) {
			if (individualInformationRequest.getHousehold().getResponsiblePerson().getResponsiblePersonSecondaryPhone() != null && individualInformationRequest.getHousehold().getResponsiblePerson().getResponsiblePersonPrimaryPhone() == null) {
				throw new GIException(GhixErrors.INVALID_RESPONSIBLE_PERSON_PRIMARY_PHONE);
			}
		}
		
		if (individualInformationRequest.getHousehold().getHouseHoldContact().getHouseHoldContactSecondaryPhone() != null && individualInformationRequest.getHousehold().getHouseHoldContact().getHouseHoldContactPrimaryPhone() == null) {
			throw new GIException(GhixErrors.INVALID_HOUSEHOLD_CONTACT_PRIMARY_PHONE);
		}

		boolean isSpecialEnrollment =false;
		EnrollmentVal enrollmentVal = null;
		if (individualInformationRequest.getHousehold().getIndividual() != null) {
			isSpecialEnrollment = isSpecialEnrollment(individualInformationRequest.getHousehold().getIndividual().getEnrollmentType());
			enrollmentVal = individualInformationRequest.getHousehold().getIndividual().getEnrollmentType();
		}
		
		if (individualInformationRequest.getHousehold().getUserRoleType() != null && individualInformationRequest.getHousehold().getUserRoleId() == null) {
			throw new GIException(GhixErrors.INVALID_AGENT_ID);
		}
		
		if (individualInformationRequest.getHousehold().getShop() != null) {
			if(individualInformationRequest.getHousehold().getShop().getSpouseId() != null && !("".equals(individualInformationRequest.getHousehold().getShop().getSpouseId()))){
				this.validateSpouse(individualInformationRequest.getHousehold().getShop().getSpouseId(), memberIds);
			}
			if(individualInformationRequest.getHousehold().getShop().getEmployeeId() != null && !("".equals(individualInformationRequest.getHousehold().getShop().getEmployeeId()))){
				this.validateEmployee(individualInformationRequest.getHousehold().getShop().getEmployeeId(), memberIds);
			}
			//// keepOnlyFlag
			if("S".equals(individualInformationRequest.getHousehold().getShop().getEnrollmentType().name())){
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside validate keepOnlyFlag---->", null, false);
				if(individualInformationRequest.getHousehold().getKeepOnlyFlag() == null) {
					throw new GIException(GhixErrors.INVALID_KeepOnlyFlag);
				}
			}
			isSpecialEnrollment = isSpecialEnrollment(individualInformationRequest.getHousehold().getShop().getEnrollmentType());
			enrollmentVal = individualInformationRequest.getHousehold().getShop().getEnrollmentType();
		}
		
		Date coverageStartDate = null;
		if(individualInformationRequest.getHousehold().getShop()!=null) {
			coverageStartDate = DateUtil.StringToDate(individualInformationRequest.getHousehold().getShop().getCoverageStartDate(), "MM/dd/yyyy");
		} else {
			coverageStartDate = DateUtil.StringToDate(individualInformationRequest.getHousehold().getIndividual().getCoverageStartDate(), "MM/dd/yyyy");
		}
		
		this.validateMembers(individualInformationRequest.getHousehold().getMembers(), coverageStartDate,isSpecialEnrollment,individualInformationRequest.getHousehold().getProductType(), enrollmentVal);
		
		//If disenroll members are present.
		if(individualInformationRequest.getHousehold().getDisenrollMembers() != null){
			this.validateDisenrollMembers(individualInformationRequest.getHousehold().getDisenrollMembers());
		}
	}

	private void validateDisenrollMembers(DisenrollMembers disenrollMembers) throws GIException {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside validateDisenrollMembers---->", null, false);
		for(DisenrollMember disEnrollMember : disenrollMembers.getDisenrollMember()){
			if("03".equals(disEnrollMember.getTerminationReasonCode())){
				String deathDate = disEnrollMember.getDeathDate();
				Date currentDate = new TSDate();
				if(deathDate == null)
				{
					throw new GIException(GhixErrors.INVALID_DEATH_DATE);
				}
				Date deathDateDt = DateUtil.StringToDate(deathDate, "MM/dd/yyyy");
				if (!(currentDate.compareTo(deathDateDt) >= 0)) {
					throw new GIException(GhixErrors.INVALID_DEATH_DATE);
				}
			}
		}
	}
	
	private void validateSpouse(String spouseId, Set<String> memberIds) throws GIException {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside validateSpouse---->", null, false);
		if(!memberIds.contains(spouseId)) {
			throw new GIException(GhixErrors.INVALID_SPOUSE_ID);
		}
	}
	
	private void validateEmployee(String employeeId, Set<String> memberIds) throws GIException {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside validateEmployee---->", null, false);
		if(!memberIds.contains(employeeId)) {
			throw new GIException(GhixErrors.INVALID_EMPLOYEE_ID);
		}
	}
	
	private void validateMembers(Members members, Date coverageStartDate, boolean isSpecialEnrollment,ProductType productType, EnrollmentVal enrollmentVal) throws GIException {
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		for(Member member : members.getMember()) {
			if(!(isSpecialEnrollment && YesNoVal.N.equals(member.getNewPersonFLAG()))) {
				this.validateDOB(member,coverageStartDate);
			}			
			//county code validation
			if(member.getCountyCode()!= null && !("".equals(member.getCountyCode()))) {
				//HIX-119187 MN - Bypass check for out of state scenarios
				boolean byPassValidationForCounty = "MN".equalsIgnoreCase(stateCode) && "999".equalsIgnoreCase(member.getCountyCode());
				if (!(zipCodeService.validateZipAndCountyCode(member.getHomeZip(), member.getCountyCode())) && !byPassValidationForCounty) {
					throw new GIException(GhixErrors.INVALID_COUNTY_CODE);
				}				 
			}
			
			this.validateCustodialParent(member);
			
			if (member.getSecondaryPhone() != null && member.getPrimaryPhone() == null)	{
				throw new GIException(GhixErrors.INVALID_PRIMARY_PHONE);
			} 
			
			try {
				validateElement(member.getCustodialParentFirstName(), member.getCustodialParentId());
				validateElement(member.getCustodialParentLastName(), member.getCustodialParentId());
			
				if(!"CA".equalsIgnoreCase(stateCode)){
					validateElement(member.getCustodialParentSsn(), member.getCustodialParentId());
					validateElement(member.getCustodialParentPrimaryPhone(), member.getCustodialParentId());
				}				
				validateElement(member.getCustodialParentHomeAddress1(), member.getCustodialParentId());
				validateElement(member.getCustodialParentHomeCity(), member.getCustodialParentId());
				validateElement(member.getCustodialParentHomeState(), member.getCustodialParentId());
				validateElement(member.getCustodialParentHomeZip(), member.getCustodialParentId());
			} catch(GIException giException) {
				giException.setErrorMsg("CustodialParentId missing");
				throw giException;
			}
			
			if (isSpecialEnrollment) {
				if (member.getMaintenanceReasonCode() == null || "".equals(member.getMaintenanceReasonCode().trim())) {
					GIException giException = new GIException();
					giException.setErrorCode(108);
					giException.setErrorMsg("Missing Maintenance Reason Code.");
					throw giException;
				}
			}
			if (isSpecialEnrollment || EnrollmentVal.A.equals(enrollmentVal)) 
			{
				if(ProductType.H.equals(productType) && StringUtils.isNotBlank(member.getExistingSADPEnrollmentID()))
				{
					throw new GIException(GhixErrors.INVALID_EXISTING_SADP_ENROLLMENTID_FOR_PRODUCTTYPE);
					/*GIException giException = new GIException();
					giException.setErrorCode(108);
					giException.setErrorMsg("Invalid ExistingSADPEnrollmentID for ProductType H");
					throw giException;*/
				}
				else if(ProductType.D.equals(productType) && StringUtils.isNotBlank(member.getExistingMedicalEnrollmentID()))
				{
					throw new GIException(GhixErrors.INVALID_EXISTING_MEDICAL_ENROLLMENTID_FOR_PRODUCTTYPE);
				}
			}
		}
	}

	private boolean isSpecialEnrollment(EnrollmentVal enrollmentVal) {
		return EnrollmentVal.S.equals(enrollmentVal);
	}
	
	private void validateElement(String custodialParentElement, String custodialParentId) throws GIException {
		if (StringUtils.isNotBlank(custodialParentElement) && StringUtils.isBlank(custodialParentId)) {
			throw new GIException(GhixErrors.MISSING_CUSTODIAL_ELEMENT);
		}
	}

	private void validateCustodialParent(Member member) throws GIException {
		String parentId = member.getCustodialParentId();
		if (StringUtils.isNotBlank(parentId)) {
			this.validateCustodialElementsPresent(member);
		}
	}
	private void validateCustodialElementsPresent(Member member) throws GIException	{
		GIException giException = new GIException(GhixErrors.MISSING_CUSTODIAL_ELEMENT);
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		
		if (StringUtils.isBlank(member.getCustodialParentFirstName())) {
			giException.setErrorMsg("CustodialParentFirstName missing");
			throw giException;
		} else if (StringUtils.isBlank(member.getCustodialParentLastName())) {
			giException.setErrorMsg("CustodialParentLastName missing");
			throw giException;
		} else if (StringUtils.isBlank(member.getCustodialParentSsn()) && !"CA".equalsIgnoreCase(stateCode)) {
			giException.setErrorMsg("CustodialParentSsn missing");
			throw giException;
		} else if (StringUtils.isBlank(member.getCustodialParentPrimaryPhone()) && !"CA".equalsIgnoreCase(stateCode)) {
			giException.setErrorMsg("CustodialParentPrimaryPhone missing");
			throw giException;
		} else if (StringUtils.isBlank(member.getCustodialParentHomeAddress1())) {
			giException.setErrorMsg("CustodialParentHomeAddress1 missing");
			throw giException;
		} else if (StringUtils.isBlank(member.getCustodialParentHomeCity())) {
			giException.setErrorMsg("CustodialParentHomeCity missing");
			throw giException;
		} else if (StringUtils.isBlank(member.getCustodialParentHomeState())) {
			giException.setErrorMsg("CustodialParentHomeState missing");
			throw giException;
		} else if (StringUtils.isBlank(member.getCustodialParentHomeZip())) {
			giException.setErrorMsg("CustodialParentHomeZip missing");
			throw giException;
		}		
	}

	private void validateDOB(Member member, Date coverageStartDate) throws GIException {
		Calendar coverageStart = TSCalendar.getInstance();
		coverageStart.setTime(coverageStartDate);
	    Calendar dob = TSCalendar.getInstance();
	    dob.setTime(DateUtil.StringToDate(member.getDob(), "MM/dd/yyyy"));
	    Integer age = PlanDisplayUtil.getAgeFromCoverageDate(coverageStart.getTime(), dob.getTime());
	    
	    if (age < 0){
	    	throw new GIException(GhixErrors.INVALID_DATE_OF_BIRTH);
	    }
	}
	
}
