package com.getinsured.plandisplay.util;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.springframework.stereotype.Component;
/**
 * This class will copy all the properties of a class to
 * destination object except nested model.
 * This class overides org.apache.commons.beanutils.BeanUtilsBean API to skip nested model properties.
 * 
 * 
 */

@Component("createDTOBean")
public class CreateDTOBean extends BeanUtilsBean{

	@Override
	public void copyProperty(Object dest, String name, Object value) throws IllegalAccessException, InvocationTargetException {
		if(value == null ){
			return;
		}

		if (value instanceof Collection ||
				("com.getinsured.hix.model".equalsIgnoreCase(value.getClass().getPackage().getName()) && !value.getClass().isEnum())) 
		{
			return;
		}

		super.copyProperty(dest, name, value);
	}

}