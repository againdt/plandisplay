package com.getinsured.plandisplay.util;

import java.util.HashMap;
import java.util.Map;

 public final class PlanDisplayConstants {
	private PlanDisplayConstants(){
		
	}
	
	public static final String ZIP = "zip";
	public static final String ID = "id";
	public static final String DOB = "dob";
	public static final String RELATION = "relation";
	public static final String SELF = "self";
	public static final String SPOUSE = "spouse";
	public static final String CHILD = "child";
	public static final String IS_COVERED = "isCovered";
	public static final String DEPENDENT = "dependent";
	public static final String CONTENT_TYPE = "application/json";
	public static final String NAMESPACE = "hixws/schemas";
	public static final String BENCHMARK_PREMIUM_WSDL_URL = "services/getBenchmarkPlan.wsdl";
	public static final String COVERAGE_START_DATE_FORMAT = "yyyy-MM-dd";
	public static final String DOB_DATE_FORMAT = "yyyy-MM-dd";
	public static final String DOB_DATE_FORMAT_REQUIRED = "MM/dd/yyyy";
	public static final float CHILD_AGE=19.00f;
	public static final int MAX_MONTH_VALUE=12;
	public static final String DEDUCTIBLE_INTG_MED_DRUG = "DEDUCTIBLE_INTG_MED_DRUG";
	public static final String IN_NETWORK_FLY = "inNetworkFly";
	public static final String COMBINED_INOUTNETWORK_FLY = "combinedInOutNetworkFly";
	public static final String DEDUCTIBLE_MEDICAL = "DEDUCTIBLE_MEDICAL";
	public static final String DEDUCTIBLE_DRUG = "DEDUCTIBLE_DRUG";
	public static final String IN_NETWORK_IND = "inNetworkInd";
	public static final String COMBINED_INOUTNETWORK_IND = "combinedInOutNetworkInd";
	public static final String HYPHEN = " - ";
	public static final String COLON = " : ";
	public static final String MODULE_NAME= "PD";
	public static final String DENTAL= "DENTAL";
	public static final String HEALTH= "HEALTH";
	public static final String ERR_MSG= "Invalid input: inputs cannot be Null or empty";
	
	public static final String PARAMGROUP = "group";
	public static final String PARAMINDIVIDUAL = "INDIVIDUAL";
	public static final String PARAMCURRENTGRPID = "CURRENT_GROUP_ID";
	public static final String PARAMHOUSEHOLDID = "HOUSEHOLD_ID";
	public static final String PARAMGRPID = "groupId";
	public static final String PERSON_DATA_OBJ="personDataObj";
	public static final String YES = "YES";
	public static final String NO = "NO";
	public static final String PLAN_ID = "planId";
	public static final String COVERAGE_START_DATE = "coverageStartDate";
	public static final String MINIMIZE_PLAN_DATA = "minimizePlanData";
	public static final String ANONYMOUS_INDIVIDUAL_TYPE = "ANONYMOUS_INDIVIDUAL_TYPE";
	public static final String PREMIUM = "PREMIUM";
	public static final String PREMIUM_BEFORE_CREDIT = "PREMIUM_BEFORE_CREDIT";
	public static final String QUOTING_DATA = "QUOTING_DATA";
	public static final String CONTRIBUTION = "CONTRIBUTION";
	
	public static final String MEMBER_ID = "memberId";
	public static final String PARAMCOUNTYCODE = "countyCode";
	public static final String EXISTING_QHP_ENROLLMENT_ID = "existingQhpEnrollmentId";
	public static final String EXISTING_SADP_ENROLLMENT_ID = "existingSadpEnrollmentId";
	public static final String EXISTING_ENROLLED_QHP_PLANID = "existingEnrolledQhpPlanId";
	public static final String EXISTING_ENROLLED_SADP_PLANID = "existingEnrolledSadpPlanId";
	public static final String EMPTY="";
	public static final String STM_DEDUCTIBLE = "DEDUCTIBLE";
	public static final String STM_IN_NETWORK_IND = "IN_NETWORK_IND";
	public static final String STM_IN_NETWORK_FLY = "IN_NETWORK_FLY";
	public static final String AME_DEDUCTIBLE = "DEDUCTIBLE";

	public static final String VERYHIGH = "VERYHIGH";
	public static final String HIGH = "HIGH";
	public static final String MEDIUM = "MEDIUM";
	public static final String LOW = "LOW";
	public static final String COVERED = "Covered";
	public static final String AND = " AND ";
	public static final String OR = " OR ";
	public static final String COMMA = ",";
	public static final String REGION = "region";
	public static final String MEMBER_PREMIUM = "premium";
	public static final String ERRORCODE1 = "101";
	public static final String ERRORCODE2 = "102";
	public static final String ERRORCODE3 = "103";
	public static final String ERRORCODE4 = "104";
	public static final String ERRORCODE5 = "105";
	public static final String SUCCESS_CODE = "200";
	public static final String SUCCESS = "SUCCESS";
	public static final String FAILED = "FAILED";
	public static final String ENROLLMENTRECORD_NOTFOUND = "Enrollment record for one of the enrollment ids not found.";
	public static final String HEALTH_AND_DENTAL_PLAN_NOTFOUND = "Error occurred while executing the request. Plans may not be available for provided enrollment Ids.";
	public static final String HEALTH_PLAN_NOTFOUND = "Error occurred while executing the request. Health plan may not be available for provided enrollment id.";
	public static final String DENTAL_PLAN_NOTFOUND = "Error occurred while executing the request. Dental Plan may not be available for provided enrollment id.";
	public static final String TECHNICAL_ISSUE_FOUND = "Technical issue occurred while executing the provided request.";
	public static final String ENROLLMENT_ID_NOTFOUND = "Enrollment id not found.";
	public static final String STATUS = "status";
	public static String FAILURE_REASON = "FailureReason";
	public static String CALCULATOR_REQ_NOT_VALID= "costCalculatorRequest is not valid";
	public static String HOUSEHOLD_ID_NOT_EMPTY = "householdId cannot be empty";
	public static String PROVIDER_NOT_EMPTY = "Providers cannot be empty";
	public static String PROVIDER_ID_NOT_EMPTY = "providers id cannot be empty";
	public static String PROVIDER_SPECIALTY_NOT_EMPTY = "providers specialty cannot be empty";
	public static String PROVIDER_NAME_NOT_EMPTY = "providers name cannot be empty";
	public static String PROVIDER_CITY_NOT_EMPTY = "providers city cannot be empty";
	public static String PROVIDER_STATE_NOT_VALID = "providers state is not valid";
	public static String INVALID_ZIP_CODE = "Invalid Zip Code";
	public static String HOUSEHOLD_INFO_NOT_EMPTY = "Household information cannot be empty";
	public static String NOOFMEMBERS_NOT_VALID = "NoOfMembers cannot be less than 1 and more than 30";
	public static String COVERAGE_YEAR_NOT_VALID = "CoverageYear is not valid";
	public static String PLANPREMIUM_NOT_EMPTY = "PlanPremiumList cannot be empty";
	public static String HIOS_ID_NOT_VALID = "HiosId is not valid";
	public static String NET_PREMIUM_NOT_NEGATIVE = "NetPremium cannot be negative";
	public static String REG_FOR_ZIP = "^\\d{5}";
	public static String REG_FOR_HIOSID = "^[A-Z0-9]{16}";
	public static String REG_FOR_STATE = "^[a-zA-Z]{2}";
	public static String MESSAGE = "message";
	public static String  BLANK_REQUEST = "Blank Request";
	public static String PREFERENCES_NOT_FOUND = "Preferences Not Found";
	public static String HOUSEHOLD_NOT_FOUND = "Household not found for the given household id";
	public static String PLANS_NOT_AVAILABLE = "Plans not available";
	public static String AGE = "age";
	public static String PERSON_COVG_DATE = "personCovgDate";
	
	public static final Map<String,String> relationships;
	static {
		relationships = new HashMap<String, String>();
		relationships.put("01", SPOUSE);
		relationships.put("03", DEPENDENT);
		relationships.put("04", DEPENDENT);
		relationships.put("05", DEPENDENT);
		relationships.put("06", DEPENDENT);
		relationships.put("07", DEPENDENT);
		relationships.put("08", DEPENDENT);
		relationships.put("09", CHILD);
		relationships.put("10", DEPENDENT);
		relationships.put("11", DEPENDENT);
		relationships.put("12", DEPENDENT);
		relationships.put("13", DEPENDENT);
		relationships.put("14", DEPENDENT);
		relationships.put("15", DEPENDENT);
		relationships.put("16", DEPENDENT);
		relationships.put("17", CHILD);
		relationships.put("18", SELF);
		relationships.put("19", CHILD);
		relationships.put("23", DEPENDENT);
		relationships.put("24", DEPENDENT);
		relationships.put("25", DEPENDENT);
		relationships.put("26", DEPENDENT);
		relationships.put("31", DEPENDENT);
		relationships.put("38", DEPENDENT);
		relationships.put("53", DEPENDENT);
		relationships.put("60", DEPENDENT);
		relationships.put("D2", DEPENDENT);
		relationships.put("G8", DEPENDENT);
		relationships.put("G9", DEPENDENT);
	}
	
	public static final Map<String, String> benefitsMapKey;
	static
	{
		benefitsMapKey = new HashMap<String, String>();
		benefitsMapKey.put("Adult dental", "MAJOR_DENTAL_CARE_ADULT");
		benefitsMapKey.put("Pediatric dental", "MAJOR_DENTAL_CARE_CHILD");
		benefitsMapKey.put("Chiropractic care", "CHIROPRACTIC");
		benefitsMapKey.put("Occupational and physical therapy", "REHABILITATIVE_PHYSICAL_THERAPY");
		benefitsMapKey.put("Acupuncture", "ACUPUNCTURE");
		benefitsMapKey.put("Hearing aids", "HEARING_AIDS");
		benefitsMapKey.put("Nutritional counseling", "NUTRITIONAL_COUNSELING");
		benefitsMapKey.put("Weight loss programs", "WEIGHT_LOSS");
	}
		
	public static final Map<String, String> benefitsKeyReveseMap;
	static
	{
		benefitsKeyReveseMap = new HashMap<String, String>();
		benefitsKeyReveseMap.put("MAJOR_DENTAL_CARE_ADULT", "Adult dental");
		benefitsKeyReveseMap.put("MAJOR_DENTAL_CARE_CHILD", "Pediatric dental");
		benefitsKeyReveseMap.put("ACUPUNCTURE", "Acupuncture");
		benefitsKeyReveseMap.put("CHIROPRACTIC", "Chiropractic care");
		benefitsKeyReveseMap.put("REHABILITATIVE_PHYSICAL_THERAPY", "Occupational and physical therapy");
		benefitsKeyReveseMap.put("HEARING_AIDS", "Hearing aids");
		benefitsKeyReveseMap.put("NUTRITIONAL_COUNSELING", "Nutritional counseling");
		benefitsKeyReveseMap.put("WEIGHT_LOSS", "Weight loss programs");
	}

	public static final Map<String, Double> benefitsPriceMapKey;
	static
	{
		benefitsPriceMapKey = new HashMap<String, Double>();
		benefitsPriceMapKey.put("MAJOR_DENTAL_CARE_ADULT", 100.0);
		benefitsPriceMapKey.put("MAJOR_DENTAL_CARE_CHILD", 100.0);
		benefitsPriceMapKey.put("ACUPUNCTURE", 100.0);
		benefitsPriceMapKey.put("CHIROPRACTIC", 65.0);
		benefitsPriceMapKey.put("REHABILITATIVE_PHYSICAL_THERAPY", 150.0);
		benefitsPriceMapKey.put("HEARING_AIDS", 3000.0);
		benefitsPriceMapKey.put("NUTRITIONAL_COUNSELING", 65.0);
		benefitsPriceMapKey.put("WEIGHT_LOSS", 100.0);
	}
	
	public enum ErrorCodes{
		 REQUEST_IS_NULL(201, "Invalid input: Request is Null."),
	 	 REQUEST_ID_IS_NULL(202, "Invalid input: Id is Null or Empty."),
	 	 FAILED_TO_GET_PREFERENCES(203,"Error occurred while processing getPreferences()."),
		 FAILED_TO_EXECUTE_CHECK_ITEM_IN_CART(205,"Error occurred while executing checkItemInCart() "),
		 FAILED_TO_EXECUTE_FIND_GROUP_BY_HOUSEHOLD_ID(206,"Error occurred while executing findGroupsByHousehold()"),
		 FAILED_TO_EXECUTE_FIND_PLD_HOUSEHOLDBY_ID(207," Error occurred while executing findPldHouseholdById()"),
		 FAILED_TO_EXECUTE_FIND_PLDHOUSEHOLD_BY_SHOPPING_ID(208,"Error occurred while executing findPldHouseholdByShoppingId()"),
		 FAILED_TO_EXECUTE_FIND_PLDHOUSEHOLD_PERSON(209,"Error occurred while executing findPldHouseholdPerson() "),
		 FAILED_TO_FIND_PLDHOUSEHOLD_PERSON(210,"Error occurred while executing findPldHouseholdPerson() "),
		 FAILED_TO_EXECUTE_POST_SPLITHOUSEHOLD(211,"Error occurred while executing postSplitHousehold() "),
		 INVALID_HOUSEHOLD_ID(212,"Invalid Household id."),
		 FAILED_TO_EXECUTE_GET_SUBSCRIBER_DATA(213,"Error occurred while executing getSubscriberData() "),
		 FAILED_TO_EXECUTE_SAVE_PREFERENCES(214,"Error occurred while executing savePreferences() "),
		 FAILED_TO_EXECUTE_FIND_PLDGROUP_BY_ID(215,"Error occurred while executing findPldGroupById()"),
		 FAILED_TO_EXECUTE_GET_PLANS(216,"Error occurred while executing getplans()"),
		 FAILED_TO_EXECUTE_SAVE_CART_ITEMS(217,"Error occurred while executing saveCartItems()"),
		 FAILED_TO_EXECUTE_DELETE_ORDER_ITEM(218,"Error occurred while executing deleteOrderItem()"),
		 FAILED_TO_EXECUTE_GET_CART_ITEMS(219,"Error occurred while executing getCartItems()"),
		 FAILED_TO_EXECUTE_KEEP_PLAN(220,"Error occurred while executing keepPlan()"),
		 FAILED_TO_EXECUTE_SHOWING_CART(221,"Error occurred while executing showingCart()"),
		 FAILED_TO_EXECUTE_GET_PALN_INFO(222,"Error occurred while executing getplanInfo()"),
		 FAILED_TO_EXECUTE_GET_PLAN_BENEFIT_COST_DATA(223,"Error occurred while executing getPlanBenefitCostData()"),
		 FAILED_TO_EXECUTE_FIND_PLD_ORDER_ITEM_BY_ID(224,"Error occurred while executing findPldOrderItemById()"),
		 FAILED_TO_EXECUTE_SPECIAL_ENROLLMENT_PERSONDATA_UPDATE(225,"Error occurred while executing specialEnrollmentPersonDataUpdate() "),
		 FAILED_TO_EXECUTE_GET_ORDER_ID_BY_HOUSEDHOLD_ID_AND_STATUS(226,"Error occurred while executing getOrderIdByHouseholdIdandStatus()"),
		 FAILED_TO_EXECUTE_FIND_PLDORDER_ITEM_BY_PLD_HOUSEDHOLD_ID(227,"Error occurred while executing findPldOrderItemByPldHouseholdId"),
		 FAILED_TO_EXECUTE_SAVE_INDIVIDUAL_PHIX(228,"Error occurred while executing saveIndividualPHIX()"),
		 FAILED_TO_EXECUTE_FIND_HOUSEHOLD_ID_BY_LEAD_ID(229,"Error occurred while executing findHouseholdIdByLeadId()"),
		 FAILED_TO_EXECUTE_SAVE_APTC(230,"Error occurred while executing saveAptc() "),
		 FAILED_TO_EXECUTE_SAVE_ORDER_ITEM(231,"Error occurred while executing saveOrderItem() "),
		 FAILED_TO_EXECUTE_SAVE_MISSING_SOLR_DOCS(232,"Error occurred while processing saveMissingSolrDocs()"),
		 FAILED_TO_EXECUTE_SEND_ADMIN_UPDATE(233,"Error occurred while executing sendAdminUpdate()"),
		 FAILED_TO_EXECUTE_EXTRACT_PERSON_DATA(234,"Error occurred while processing extractPersonData()"),
		 FAILED_TO_EXECUTE_UPDATE_ORDER_STATUS(235,"Error occurred while executing updateOrderStatus()"),
		 FAILED_TO_EXECUTE_UPDATE_CONTRIBUTION_PER_PERSON(236,"Error occurred while executing updateContributionPerPerson()"),
		 FAILED_TO_EXECUTE_FIND_AVAILABLE_PLAN(237,"Error occurred while executing findavailablePlan()"),
		 FAILED_TO_EXECUTE_UPDATE_PLAN_DATA_SOLR(238,"Error occurred while executing updatePlanBenefitsAndCostInSOLR()"),
		 FAILED_TO_EXECUTE_UPDATE_SEEK_COVERAGE_DENTAL(239,"Error occurred while executing updateSeekCoverageDental()"),
		 FAILED_TO_EXECUTE_SAVE_PREFERENCES_BY_HOUSEHOLD_ID(240,"Error occurred while executing savePreferences() "),
		 FAILED_TO_EXECUTE_SAVE_CART_ITEM(241,"Error occurred while executing saveCartItem()"),
		 FAILED_TO_EXECUTE_DELETE_CART_ITEM(242,"Error occurred while executing deleteCartItem()"),
		 FAILED_TO_EXECUTE_FIND_ORDERITEM_IDS_BY_HOUSEHOLDID(243,"Error occurred while executing findOrderItemIdsByHouseholdId()"),
		 FAILED_TO_EXECUTE_UPDATE_PERSON_SUBSIDY(244,"Error occurred while executing updatePersonSubsidy()"),
		 FAILED_TO_EXECUTE_UPDATE_HOUSEHOLD_SUBSIDY(245,"Error occurred while executing updateHouseholdSubsidy()"),
		 FAILED_TO_SAVE_APPLICATION_ELIGIBILITY_DATA(246, "Error occoured while saving applicant eligibility data"),
		 FAILED_TO_FIND_HOUSEHOLD_BY_SSAPIDANDREQTYPE(247, "Error occured while finding household by leadId and request type"),
		 FAILED_TO_GET_NON_ELIGIBLE_MEMBERS(248, "Error occoured while getNonEligibleMemberList"),
		 FAILED_TO_EXECUTE_UPDATE_PERSON_TOBACCO(249, "Error occoured while updatePersonTobacco"),
		 FAILED_TO_SAVE_PDHOUSHOLD(250, "Error occured while saving pdHousehold"),
		 FAILED_TO_FIND_PDORDERITEM_BY_SSAPAPPLICATIONID(251, "Error occured while finding ssapApplicatioinId in PdOrderItem"),
		 FAILED_TO_FIND_PDORDERITEM_BY_LEADID(252, "Error occured while finding PdOrderItems by lead id"),
		 FAILED_TO_FIND_CART_ITEMS(253, "Error occured while finding CartItems"),
		 FAILED_TO_EXECUTE_UPDATE_APPLICATIONID_IN_ORDERITEM(254, "Error occurred while executing updateApplicationIdInOrderItem()"),	
		 FAILED_TO_EXECUTE_APPEND_HIOSPLANID_WITH_CSR(255, "Error occurred while executing appendHiosPlanIdWithCSR()"),
		 FAILED_TO_FIND_PDHOUSHOLD(256, "Error occured while finding PdHousehold by lead id And CoverageYear"),
		 FAILED_TO_FIND_SUBSCRIBER_ZIP(257, "Error occured while getSubscriberZip");
		 
		private int id;
		private String message;
		private ErrorCodes(int id,String message){
			this.id=id;
			this.message=message;
		}
		
		public int getId() {
			return id;
		}
		
		public String getMessage() {
			return message;
		}
	}

}
