package com.getinsured.plandisplay.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;

import freemarker.template.Configuration;
import freemarker.template.Template;


public class GenerateAsciiDoc {

	private static final Logger LOGGER = LoggerFactory.getLogger(GenerateAsciiDoc.class);
	private static String NOT_PROVIDED = "Not Provided";
	public static void main(String[] args) {

		String fileName = "src/main/resources/ToAsciiDoc.xlsx";
		String directory = "src/main/asciidoc";
		String asciidoc="src/main/asciidoc/api-guide.asciidoc";
		
		try 
		{
			File dir=new File(directory) ;
			boolean chk = false;
			if(!dir.exists())
			{
				chk = dir.mkdir();
				if( chk )
				{
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "Directory Created", null, false);
				}
			}
			
			File file = new File(asciidoc);
			
		    if (!file.exists()) 
		    {
		 	    chk = file.createNewFile();
		 	    if( chk )
		 	    {
		 	    	PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "Asciidoc File Created", null, false);
		 	    }
	        }
			
			
			FileWriter fileWriter=new FileWriter(file);
			
			Template template=loadTemplateForDoc("src/main/resources/asciiInitDoc.txt");
			Writer writerinit = new StringWriter();
			template.process(null, writerinit);
			fileWriter.write(writerinit.toString());
			template=loadTemplateForDoc("src/main/resources/asciidoc.txt");

			FileInputStream myInput = new FileInputStream(fileName);	
			XSSFWorkbook wb = new XSSFWorkbook(myInput);
			XSSFSheet mySheet = wb.getSheetAt(0);
			XSSFRow xssfRow;
			
			for (int row = 1; row <= mySheet.getLastRowNum(); row++) {
				xssfRow = mySheet.getRow(row);
				if( xssfRow != null)
				{
					Map<String,Object> replaceableVal = new HashMap<String,Object>();
					replaceableVal.put("heading",xssfRow.getCell(0)!=null ? xssfRow.getCell(0).getStringCellValue().length()!=0 ? xssfRow.getCell(0) : xssfRow.getCell(6)  :xssfRow.getCell(6));
					replaceableVal.put("url", xssfRow.getCell(6)!=null ? xssfRow.getCell(6).getStringCellValue().length()!=0? "["+xssfRow.getCell(6)+"]" : NOT_PROVIDED : NOT_PROVIDED);
					replaceableVal.put("input", xssfRow.getCell(1)!=null ? xssfRow.getCell(1).getStringCellValue().length()!=0?xssfRow.getCell(1) : NOT_PROVIDED : NOT_PROVIDED);
					replaceableVal.put("output", xssfRow.getCell(2)!=null ? xssfRow.getCell(2).getStringCellValue().length()!=0?xssfRow.getCell(2) : NOT_PROVIDED : NOT_PROVIDED);
					replaceableVal.put("method", xssfRow.getCell(3)!=null ? xssfRow.getCell(3).getStringCellValue().length()!=0?xssfRow.getCell(3) : NOT_PROVIDED : NOT_PROVIDED);
					replaceableVal.put("owner",xssfRow.getCell(4)!=null? xssfRow.getCell(4).getStringCellValue().length()!=0 ? xssfRow.getCell(4) : NOT_PROVIDED : NOT_PROVIDED);
					replaceableVal.put("description", xssfRow.getCell(5)!=null ? xssfRow.getCell(5).getStringCellValue().length()!=0?xssfRow.getCell(5) : NOT_PROVIDED : NOT_PROVIDED);
					replaceableVal.put("linkUrl",xssfRow.getCell(6)!=null ? xssfRow.getCell(6).getStringCellValue().length()!=0?xssfRow.getCell(6) : NOT_PROVIDED : NOT_PROVIDED);
					replaceableVal.put("responseFormat", xssfRow.getCell(7)!=null? xssfRow.getCell(7).getStringCellValue().length()!=0 ? xssfRow.getCell(7) :"JSON" :"JSON");
					replaceableVal.put("requireAuth", xssfRow.getCell(8)!=null? xssfRow.getCell(8).getStringCellValue().length()!=0 ? xssfRow.getCell(8) : "NO"  : "NO" );
					replaceableVal.put("sampleRequest", xssfRow.getCell(9)!=null? xssfRow.getCell(9).getStringCellValue().length()!=0 ? xssfRow.getCell(9) :NOT_PROVIDED :NOT_PROVIDED);
					replaceableVal.put("sampleResponse", xssfRow.getCell(10)!=null? xssfRow.getCell(10).getStringCellValue().length()!=0 ? xssfRow.getCell(10) :NOT_PROVIDED :NOT_PROVIDED );
					replaceableVal.put("inputParameters", xssfRow.getCell(11)!=null? xssfRow.getCell(11).getStringCellValue().length()!=0 ? getInputParameters(xssfRow.getCell(11)) :"| " + NOT_PROVIDED : "| " +  NOT_PROVIDED );
					
					Writer out = new OutputStreamWriter(System.out);
		            template.process(replaceableVal, out);
		            out.flush();
		           
		            Writer writer = new StringWriter();
		            template.process(replaceableVal,writer);
		            String sample = writer.toString();
		            fileWriter.write(sample);
				}
			}
			
			 fileWriter.close();

		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "ERROR", e, false);
		}
	}

	private static  Object getInputParameters(XSSFCell  xssfCell ) {
		
		String paramater = xssfCell.getStringCellValue();
		List<String> paramList = null;
		StringBuilder sb = new StringBuilder();
		if(paramater.contains(";"))
		{
			String paramaterArr [] = paramater.split(";");
			paramList = Arrays.asList(paramaterArr);
			for( String param : paramList)
			{
				if (sb.length() != 0) {
			        sb.append(",");
			        sb.append(System.lineSeparator());
			    }
				sb.append("| ");
				sb.append(param);
			}
		}
		else
		{
			sb.append("| ");
			sb.append(paramater);
		}
		return sb;
	} 

	private static  Template loadTemplateForDoc(String filename) {
		Configuration cfg = new Configuration();
		Template template = null;
		 try {
			 template = cfg.getTemplate(filename);
			
			
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "Error in loading Template", e, false);
		}
		 return template;
		
	} 
}