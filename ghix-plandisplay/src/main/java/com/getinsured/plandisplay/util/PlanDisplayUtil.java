package com.getinsured.plandisplay.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.util.HtmlUtils;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapHeaderElement;
import org.w3c.dom.Document;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.plandisplay.APTCCalculatorMemberData;
import com.getinsured.hix.dto.plandisplay.APTCCalculatorRequest;
import com.getinsured.hix.dto.plandisplay.APTCCalculatorResponse;
import com.getinsured.hix.dto.plandisplay.APTCPlanRequestDTO;
import com.getinsured.hix.dto.plandisplay.APTCPlanResponseDTO;
import com.getinsured.hix.dto.plandisplay.DrugDTO;
import com.getinsured.hix.dto.plandisplay.PdHouseholdDTO;
import com.getinsured.hix.dto.plandisplay.PdPersonDTO;
import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;
import com.getinsured.hix.dto.plandisplay.PlanDentalBenefitDTO;
import com.getinsured.hix.dto.plandisplay.PlanDentalCostDTO;
import com.getinsured.hix.dto.plandisplay.PlanHealthBenefitDTO;
import com.getinsured.hix.dto.plandisplay.PlanHealthCostDTO;
import com.getinsured.hix.dto.plandisplay.PlanPremiumDTO;
import com.getinsured.hix.dto.plandisplay.dst.SaveHouseholdRequest;
import com.getinsured.hix.dto.planmgmt.AmePlan;
import com.getinsured.hix.dto.planmgmt.AmePlanRequestDTO;
import com.getinsured.hix.dto.planmgmt.AmePlanResponseDTO;
import com.getinsured.hix.dto.planmgmt.LifePlanDTO;
import com.getinsured.hix.dto.planmgmt.LifePlanRequestDTO;
import com.getinsured.hix.dto.planmgmt.LifePlanResponseDTO;
import com.getinsured.hix.dto.planmgmt.Member;
import com.getinsured.hix.dto.planmgmt.PlanRateBenefitRequest;
import com.getinsured.hix.dto.planmgmt.PlanRateBenefitResponse;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.dto.planmgmt.SbcScenarioDTO;
import com.getinsured.hix.dto.planmgmt.SbcScenarioRequestDTO;
import com.getinsured.hix.dto.planmgmt.SbcScenarioResponseDTO;
import com.getinsured.hix.dto.planmgmt.VisionPlan;
import com.getinsured.hix.dto.planmgmt.VisionPlanRequestDTO;
import com.getinsured.hix.dto.planmgmt.VisionPlanResponseDTO;
import com.getinsured.hix.dto.prescription.PrescriptionSearchRequest;
import com.getinsured.hix.dto.shop.EmployerEnrollmentDTO;
import com.getinsured.hix.dto.shop.ShopResponse;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.EmployerEnrollmentItem;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.model.IndividualPlan;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.PdHousehold;
import com.getinsured.hix.model.PdOrderItem;
import com.getinsured.hix.model.PdPerson;
import com.getinsured.hix.model.PdPreferences;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanRateBenefit;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.consumer.FFMMemberResponse;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.plandisplay.FFMPerson;
import com.getinsured.hix.model.plandisplay.PdNetworkCompareRequest;
import com.getinsured.hix.model.plandisplay.PdQuoteRequest;
import com.getinsured.hix.model.plandisplay.PdSaveProviderPrefRequest;
import com.getinsured.hix.model.plandisplay.PdWSResponse;
import com.getinsured.hix.model.plandisplay.PlanAvailabilityForDrugResponse;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentFlowType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ExchangeType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.Gender;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.MedicalProcedure;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.PlanTab;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.PreferencesLevel;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.Relationship;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ShoppingType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;
import com.getinsured.hix.model.plandisplay.PlanPreferences;
import com.getinsured.hix.model.plandisplay.ProviderBean;
import com.getinsured.hix.model.plandisplay.SessionData.ProgramType;
import com.getinsured.hix.model.ssap.ConsumerApplicant.BooleanFlag;
import com.getinsured.hix.planmgmt.controller.AmePlanRestController;
import com.getinsured.hix.planmgmt.controller.PlanRateBenefitRestController;
import com.getinsured.hix.planmgmt.controller.PlanRestController;
import com.getinsured.hix.planmgmt.controller.STMPlanRateBenefitRestController;
import com.getinsured.hix.planmgmt.controller.VisionPlanRestController;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.config.PlanDisplayConfiguration;
import com.getinsured.hix.platform.enums.OnOffEnum;
import com.getinsured.hix.platform.enums.YesNoEnum;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.logging.filter.RequestCorrelationContext;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.PlanMgmtEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.ShopEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIExceptionHandlerFactory;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.APTCEligibilityType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.AddressTypeCodeType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.ApplicantEligibilityResponseType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.CSREligibilityType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InitialEnrollmentPeriodEligibilityType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicationType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.PersonType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.ProgramEligibilitySnapshotType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.ProgramEligibilityStatusCodeType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.ProgramEligibilityType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.ResponseType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.SSFPrimaryContactType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.SpecialEnrollmentPeriodEligibilityType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_core.PersonAssociationType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_core.PersonContactInformationAssociationType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_types.InsurancePlanVariantCategoryNumericCodeType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.AddressType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.ContactInformationType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.DateRangeType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.FullTelephoneNumberType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.structures._2.ReferenceType;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.DisenrollMembers;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.DisenrollMembers.DisenrollMember;
import com.getinsured.plandisplay.service.prescriptionSearch.PrescriptionData;
import com.getinsured.plandisplay.service.prescriptionSearch.PrescriptionFactory;
import com.getinsured.plandisplay.service.prescriptionSearch.PrescriptionSearch;
import com.getinsured.plandisplay.service.providerSearch.ProviderFactory;
import com.getinsured.plandisplay.service.providerSearch.ProviderSearch;
import com.getinsured.plandisplay.service.rules.BenefitCoverageCalculator;
import com.getinsured.plandisplay.service.rules.gps.GPSCalculatorService;
import com.getinsured.plandisplay.service.rules.gps.GPSIronMan;
import com.getinsured.plandisplay.service.rules.gps.GPSSuperMan;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TimeShifterUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.thoughtworks.xstream.XStream;

@Component
@DependsOn("giExceptionHandler")
public class PlanDisplayUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(PlanDisplayUtil.class);
	
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private PlanRateBenefitRestController planRateBenefitController;
	@Autowired private STMPlanRateBenefitRestController stmPlanRateBenefitRestController;
	@Autowired private AmePlanRestController amePlanRestController;
	@Autowired private VisionPlanRestController visionPlanRestController;
	@Autowired private PlanRestController planRestController;
	@Autowired private ZipCodeService zipCodeService;
	@Autowired private GIWSPayloadService giWSPayloadService;
	@Autowired private GIExceptionHandler giExceptionHandler;
	@Autowired private UserService userService;
	@Autowired private PlanDisplayAsyncUtil planDisplayAsyncUtil;
	@Autowired private Gson platformGson;
	@Autowired private ProviderFactory providerFactory;
	@Autowired private PrescriptionFactory prescriptionFactory;
	
	//Autowiring ModuleClassLoaderHook here, just to force its bean creation. 
	@Autowired private ModuleClassLoaderHook moduleClassLoaderHook;
	
	public static final String PREMIUM="premium";
	public static final String CONTRIBUTION="contribution";
	private static Map<String, CostSharing> FFM_CSR_MAPPING;
	
	public static final String EMPTY_STRING = StringUtils.EMPTY;
	public static final String USER_NAME = "userName";
	public static final String LOG_CORRELATION_ID = "LOG_CORRELATION_ID";
	public static final String LOG_GLOBAL_ID = "LOG_GLOBAL_ID";
	public static final String ZIP = "zip";
	public static final String GENDER = "gender";
	public static final String AGE = "age";
	public static final String MALE = "Male";
	public static final String GINS = "GINS";
	public static final String TOBACCO = "tobacco";
	public static final String COUNTY_CODE = "countycode";
	public static final String YYYY_MM_DD = "yyyy-MM-dd";
	
	public static final String COVERED = "Covered";
	public static final String NOT_COVERED = "Not covered";
	public static final String N = "N";
	public static final String U = "U";
	public static final String Y = "Y";
	public static final String STATUS = "status";
	public static final String ERROR = "error";
	public static final String BLANK_SPACE_STRING = " ";
	public static final String INDIVIDUAL="individual";
	public static final String DOCTOR="doctor";
	public static final String HOSPITAL="hospital";
	
	private static final String[] ameBenefitAttributeList = new String[] { "displayVal" };
		
	private static final String DISPLAY_VAL = "displayVal".intern();
	
	
	private static final String NETWORK_T1_DISP = "netWkT1Disp".intern();
	private static final String NETWORK_T1_TILE_DISP = "netWkT1TileDisp".intern();
	private static final String TILE_DISPLAY_VAL = "tileDisplayVal".intern();
	private static final String NETWORK_T2_DISP = "netWkT2Disp".intern();
	private static final String OUT_NETORK_DISP = "outNetWkDisp".intern();
	private static final String SUB_TO_NET_DEDUCT ="subToNetDeduct".intern();
	private static final String SUB_TO_NON_NET_DEDUCT ="subToNonNetDeduct".intern();
	private static final String LIMIT_EXCEP_DISP ="limitExcepDisp".intern();
	private static final String EXPLANATION ="explanation".intern();
	private static final String AFTER_DEDUCTIBLE = "after deductible".intern();
	private static final String COPAY = "copay".intern();
	private static final String NO_CHARGE = "no charge".intern();

    private static final String TENANT_GINS = "GINS";
    public static final String OUT_NETWORK = "outNetWork";
    public static final String IN_NETWORK = "inNetWork";
    
    private static final List<String> drugCoveredArray = new ArrayList<>(Arrays.asList("generic", "preferred_brand", "non_preferred_brand", "specialty"));
    private static final String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
    private static final String isStateSubsidyEnabled = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_SUBSIDY);
    private static final String exchangeTypeConfig = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE);
    
    private static final String PERSON_COVG_DATE = "personCovgDate";
    private static final int HIOS_ISSUER_ID_LEN = 5;
    public static final String SHORT_DATE_FORMAT = "MM/dd/yyyy";
    
	static {
		FFM_CSR_MAPPING = new HashMap<String, CostSharing>();
		FFM_CSR_MAPPING.put("01", CostSharing.CS1);
		FFM_CSR_MAPPING.put("02", CostSharing.CS2);
		FFM_CSR_MAPPING.put("03", CostSharing.CS3);
		FFM_CSR_MAPPING.put("04", CostSharing.CS4);
		FFM_CSR_MAPPING.put("05", CostSharing.CS5);
		FFM_CSR_MAPPING.put("06", CostSharing.CS6);
	}
	
	public PlanDisplayUtil() {
		// TODO Auto-generated constructor stub
	}
	
	@PostConstruct
	public void custom_init(){
		GIExceptionHandlerFactory.init(giExceptionHandler);
	}
	
	public static String getDefaultPref() {
		PdPreferencesDTO pdPreferencesDTO = getDefaultPreferencesDTO();
		
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(PdPreferencesDTO.class);
			return writer.writeValueAsString(pdPreferencesDTO);
		} catch (JsonParseException e) {
			putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			return null;
		} catch (JsonMappingException e) {
			putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			return null;
		} catch (IOException e) {
			putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			return null;
		}
	}


	private static PdPreferencesDTO getDefaultPreferencesDTO() {
		PdPreferencesDTO pdPreferencesDTO = new PdPreferencesDTO();
		
		boolean isPHIXProfile = "PHIX".equalsIgnoreCase(exchangeTypeConfig);
		if(isPHIXProfile || "CA".equalsIgnoreCase(stateCode) || "WA".equalsIgnoreCase(stateCode) || "CT".equalsIgnoreCase(stateCode) || "MN".equalsIgnoreCase(stateCode)){
			pdPreferencesDTO.setMedicalUse(PreferencesLevel.LEVEL2);
			pdPreferencesDTO.setPrescriptionUse(PreferencesLevel.LEVEL2);
		}else{
			pdPreferencesDTO.setMedicalUse(PreferencesLevel.LEVEL1);
			pdPreferencesDTO.setPrescriptionUse(PreferencesLevel.LEVEL1);
		}
				
		List<String> benefits = new ArrayList<String>();
		pdPreferencesDTO.setBenefits(benefits);
		return pdPreferencesDTO;
	}
	
	public List<PdPersonDTO> getSeekingCovgPersonList(List<PdPersonDTO> pdPersonList) {
		List<PdPersonDTO> dentalPersonDataList = new ArrayList<PdPersonDTO>();
		for(PdPersonDTO pdPersonDTO : pdPersonList) {
			if(YorN.Y.equals(pdPersonDTO.getSeekCoverage())){
				dentalPersonDataList.add(pdPersonDTO);
			}
		}		
		return dentalPersonDataList;
	}
	
	public String getDentalConfig(Date covgStartDate) 
	{
		 String dentalConfig  = "ON";
		 if(!DynamicPropertiesUtil.getPropertyValue("planSelection.DentalPlanSelection").isEmpty()) {
			 dentalConfig = DynamicPropertiesUtil.getPropertyValue("planSelection.DentalPlanSelection");
		 }
		
		 /*final String stateExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);					
		 if("Individual".equalsIgnoreCase(stateExchangeType)){
			 String configChangeDate = "01-01-2016"; // FIXME why this is hard-coded?!
			 DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
			 Date formattedConfigChangeDate;
			 try 
			 {
				 String programType = DynamicPropertiesUtil.getPropertyValue("global.StateExchangeType");
				 if (programType != null && programType.equalsIgnoreCase(ProgramType.INDIVIDUAL.toString())) {
					 formattedConfigChangeDate = dateFormat.parse(configChangeDate);
					 if(covgStartDate != null && covgStartDate.before(formattedConfigChangeDate)) {
						 return "PO";
					 } else {
						 return "ON";
					 }
				 } 
				 else {
					 return dentalConfig;
				 }
			 }
			 catch (ParseException e) 
			 {
			   // No PII could leak from this exception, no need to encrypt anything here.
			   if(LOGGER.isErrorEnabled())
			   {
			     LOGGER.error("<----Error while Date formatted Change---->", e);
			   }
			 } 
		 }*/		 
		 return dentalConfig;
	}
	
	public static List<Map<String,String>> getMemberListForQuoting(List<PdPersonDTO> pdPersonList, Map<String,String> subscriberData, Date coverageStartDate, EnrollmentType enrollmentType, EnrollmentFlowType specialEnrollmentFlowType, InsuranceType insuranceType) 
	{
		List<Map<String,String>> censusList = new ArrayList<Map<String,String>>();		

		for(PdPersonDTO pdPersonDTO : pdPersonList){
			Map<String,String> censusData = new HashMap<String,String>();
			censusData.put("id", pdPersonDTO.getExternalId().toString());
			censusData.put("relation", pdPersonDTO.getRelationship().toString());
			censusData.put(ZIP, subscriberData.get(GhixConstants.SUBSCRIBER_MEMBER_ZIP));
			censusData.put("countycode", subscriberData.get(GhixConstants.SUBSCRIBER_MEMBER_COUNTY));
			censusData.put("dateOfBirth", DateUtil.dateToString(pdPersonDTO.getBirthDay(), "MM/dd/yyyy"));
			
			Date coverageDateforAge = getPersonCoverageDate(enrollmentType, specialEnrollmentFlowType, insuranceType, pdPersonDTO, coverageStartDate);
			censusData.put(PERSON_COVG_DATE, DateUtil.dateToString(coverageDateforAge, "MM/dd/yyyy"));
			
			Integer personAge = getAgeFromCoverageDate(coverageDateforAge, pdPersonDTO.getBirthDay());
			censusData.put(AGE, personAge.toString());
			
			if(pdPersonDTO.getTobacco() != null){
				censusData.put(TOBACCO,pdPersonDTO.getTobacco().toString());
			}
			
			if(pdPersonDTO.getGender() != null){
				censusData.put(GENDER,Gender.F.equals(pdPersonDTO.getGender()) ? "Female" : MALE);
			}
					
			censusList.add(censusData);
		}
		return censusList;
	}
	
	public static Date getPersonCoverageDate(EnrollmentType enrollmentType, EnrollmentFlowType specialEnrollmentFlowType, InsuranceType insuranceType, PdPersonDTO pdPersonDTO, Date coverageStartDate) {
		Date coverageDateforAge = null;		
		if (EnrollmentType.S.equals(enrollmentType) && EnrollmentFlowType.KEEP.equals(specialEnrollmentFlowType)){
			if(InsuranceType.HEALTH.equals(insuranceType)){
				coverageDateforAge = pdPersonDTO.getHealthCoverageStartDate();
			}else{
				coverageDateforAge = pdPersonDTO.getDentalCoverageStartDate();
			}
			if (coverageDateforAge == null){
				coverageDateforAge = coverageStartDate;
			}			
			
		}else {
			coverageDateforAge = coverageStartDate;
		}
		return coverageDateforAge;
	}
	
	public static Integer getPersonAge(EnrollmentType enrollmentType, EnrollmentFlowType specialEnrollmentFlowType, InsuranceType insuranceType, PdPersonDTO pdPersonDTO, Date coverageStartDate) {
		Date coverageDateforAge = getPersonCoverageDate(enrollmentType, specialEnrollmentFlowType, insuranceType, pdPersonDTO, coverageStartDate);
		Integer personAge = getAgeFromCoverageDate(coverageDateforAge, pdPersonDTO.getBirthDay());
		return personAge;
	}

	public static IndividualPlan buildPlanDetails(PlanResponse planResponse)
	{
		IndividualPlan individualPlan = new IndividualPlan();
		individualPlan.setId(planResponse.getPlanId());
		individualPlan.setIssuerPlanNumber(planResponse.getIssuerPlanNumber());
		individualPlan.setPlanId(planResponse.getPlanId());
		individualPlan.setName(planResponse.getPlanName());
		individualPlan.setIssuer(planResponse.getIssuerName());
		individualPlan.setIssuerLogo(planResponse.getIssuerLogo());
		individualPlan.setIssuerId(planResponse.getIssuerId());
		if(Plan.PlanLevel.EXPANDEDBRONZE.toString().equals(planResponse.getPlanLevel()))
		{
			individualPlan.setLevel(Plan.PlanLevel.BRONZE.toString());
		}
		else
		{
			individualPlan.setLevel(planResponse.getPlanLevel());
		}
		individualPlan.setPlanType(planResponse.getInsuranceType());
		individualPlan.setNetworkType(planResponse.getNetworkType());
		individualPlan.setCostSharing(planResponse.getCostSharing());
		individualPlan.setSbcUrl(planResponse.getSbcDocUrl());
		individualPlan.setPlanBrochureUrl(planResponse.getBrochure());
		individualPlan.setFormularyUrl(planResponse.getFormularyUrl());
		
		if (Plan.PlanInsuranceType.HEALTH.toString().equals(planResponse.getInsuranceType()) && planResponse.getTier2util()!=null && !planResponse.getTier2util().isEmpty() && !"0%".equals(planResponse.getTier2util())) {
			individualPlan.setTier2Coverage(Y);
		}	
		if(Plan.PlanInsuranceType.HEALTH.toString().equals(planResponse.getInsuranceType()) && planResponse.getMaxCoinsForSpecialtyDrugs()!= null && !(EMPTY_STRING.equals(planResponse.getMaxCoinsForSpecialtyDrugs()))){
			individualPlan.setMaxCoinseForSpecialtyDrugs(planResponse.getMaxCoinsForSpecialtyDrugs());
		}
		
		if (Plan.PlanInsuranceType.STM.toString().equals(planResponse.getInsuranceType())) {
			individualPlan.setPolicyLength(planResponse.getPolicyLength());
			individualPlan.setCoinsurance(planResponse.getCoinsurance());
			individualPlan.setSeparateDrugDeductible(planResponse.getSeparateDrugDeductible());
			individualPlan.setOutOfNetwkCoverage(planResponse.getOutOfNetwkCoverage());
			individualPlan.setOutOfCountyCoverage(planResponse.getOutOfCountyCoverage());
			individualPlan.setPolicyLimit(planResponse.getPolicyLimit());
			individualPlan.setPolicyLengthUnit(planResponse.getPolicyLengthUnit());
			individualPlan.setOopMaxFamily(planResponse.getOopMaxFamily());
			individualPlan.setOopMaxIndDesc(planResponse.getOopMaxAttr());
			float stmOopMax = planResponse.getOopMax() != null ? Float.parseFloat(planResponse.getOopMax().toString()) : new Float(0);
			individualPlan.setOopMax(stmOopMax);
		}

		if (Plan.PlanInsuranceType.AME.toString().equals(planResponse.getInsuranceType())) {
			Map<String, Map<String, String>> planCosts = new HashMap<String, Map<String, String>>();

			Map<String, String> planCost = new HashMap<String, String>();

			planCost.put("deductibleVal", planResponse.getDeductible());
			planCost.put("deductibleAttrib", planResponse.getDeductibleAttrib());

			planCosts.put(PlanDisplayConstants.AME_DEDUCTIBLE, planCost);

			planCost = new HashMap<String, String>();
			planCost.put("maxBenefitVal", planResponse.getMaxBenefitVal());
			planCost.put("maxBenefitAttrib", planResponse.getMaxBenefitAttrib());
			planCosts.put("MAX_BENEFIT", planCost);

			planCost = new HashMap<String, String>();
			planCost.put("limitationsAndExclusions",planResponse.getLmitationAndExclusions());
			planCosts.put("LIMITATIONS", planCost);
			individualPlan.setPlanCosts(planCosts);

			individualPlan.setPlanBrochureUrl(planResponse.getBrochure());

		}
		
		if (Plan.PlanInsuranceType.VISION.toString().equals(planResponse.getInsuranceType())) {
			Map<String, Map<String, String>> planCosts = new HashMap<String, Map<String, String>>();
			individualPlan.setPlanCosts(planCosts);

			individualPlan.setPlanBrochureUrl(planResponse.getBrochure());
			individualPlan.setOutOfNetwkCoverage(planResponse.getOutOfNetwkCoverage());
			individualPlan.setPolicyLength(planResponse.getPremiumPayment());
			individualPlan.setPlanTier1(planResponse.getPlanBenefits());
		}
		
		if("CT".equalsIgnoreCase(stateCode)) {
			if(StringUtils.isNotBlank(planResponse.getEocDocUrl())) {
				individualPlan.setProviderLink(planResponse.getEocDocUrl());
			}
		}else if (planResponse.getProviderUrl() != null && !(StringUtils.EMPTY.equals(planResponse.getProviderUrl()))) {
			individualPlan.setProviderLink(planResponse.getProviderUrl());
		}
		
		String insuranceType = planResponse.getInsuranceType() != null ? planResponse.getInsuranceType() : StringUtils.EMPTY;
		if(Plan.PlanInsuranceType.HEALTH.toString().equalsIgnoreCase(insuranceType) && planResponse.getEhbPercentage() != null && !planResponse.getEhbPercentage().isEmpty()) {
			individualPlan.setEhbPrecentage(Float.parseFloat(planResponse.getEhbPercentage()));
		}		
		if(Plan.PlanInsuranceType.DENTAL.toString().equalsIgnoreCase(insuranceType) && planResponse.getPediatricDentalComponent() != null && !planResponse.getPediatricDentalComponent().isEmpty()) {
			String pediatricDentalComponent = planResponse.getPediatricDentalComponent();
			pediatricDentalComponent = pediatricDentalComponent.replace("$", StringUtils.EMPTY);
			individualPlan.setEhbPrecentage(Float.parseFloat(pediatricDentalComponent));
		}
		
		if(null!=planResponse.getPlanCosts()){
			calculateDeductible(individualPlan, planResponse.getPlanCosts(), 1);
		}
		
		if(planResponse.getExchangeType()!= null && !(StringUtils.EMPTY.equals(planResponse.getExchangeType()))){
			individualPlan.setExchangeType(planResponse.getExchangeType());
		}else{
			individualPlan.setExchangeType("ON");
		}
		
		if (null!=planResponse.getPlanBenefits() && !Plan.PlanInsuranceType.VISION.toString().equalsIgnoreCase(planResponse.getInsuranceType()))
		{
			String[] props = GhixConstants.helathBenefitList;
			if(Plan.PlanInsuranceType.DENTAL.toString().equalsIgnoreCase(insuranceType)){
				props = GhixConstants.dentalBenefitList;
			} else if (Plan.PlanInsuranceType.STM.toString().equalsIgnoreCase(insuranceType)) {
				props = GhixConstants.stmBenefitList;
			} else if (Plan.PlanInsuranceType.AME.toString().equalsIgnoreCase(insuranceType)) {
				props = GhixConstants.ameBenefitList;
			}
			
			//List<Map<String, Map<String,String>>> planBenefitsDisplay = parsePlanBenefits(PlanDisplayEnum.InsuranceType.valueOf(planResponse.getInsuranceType()), planResponse.getPlanBenefits(), individualPlan);
			if (Plan.PlanInsuranceType.AME.toString().equalsIgnoreCase(insuranceType)) {	
				individualPlan.setPlanTier1(parsePlanBenefitsAME(planResponse.getPlanBenefits(), individualPlan, props));
			}else{
				individualPlan.setPlanTier1(parsePlanBenefitsTier1(planResponse.getPlanBenefits(), individualPlan, props));
			}
			individualPlan.setPlanTier2(parsePlanBenefitsTier2(planResponse.getPlanBenefits(), props));
			individualPlan.setPlanOutNet(parsePlanBenefitsOutNet(planResponse.getPlanBenefits(), props));
			individualPlan.setPlanAppliesToDeduct(parseAppliesToDeduct(planResponse.getPlanBenefits(), props));
			individualPlan.setNetwkException(parseNetwkExcp(PlanDisplayEnum.InsuranceType.valueOf(planResponse.getInsuranceType()), planResponse.getPlanBenefits(), props));
			individualPlan.setMissingDentalCovg(parseBenefitCovg(planResponse.getPlanBenefits(), props));
			individualPlan.setBenefitExplanation(parseBenefitExpl(planResponse.getPlanBenefits(), props));
		}
		
		if(null!=planResponse.getPlanCosts() && !Plan.PlanInsuranceType.AME.toString().equals(planResponse.getInsuranceType())
				&& !Plan.PlanInsuranceType.VISION.toString().equals(planResponse.getInsuranceType())){
			Map<String, Map<String, String>> planCosts = new HashMap<String, Map<String, String>>();
			if(planResponse.getPlanCosts() != null) {
				planCosts = planResponse.getPlanCosts();
			}
			if(Plan.PlanInsuranceType.STM.toString().equals(planResponse.getInsuranceType())) {
				Map<String, String> planCost = new HashMap<String, String>();
				planCost.put("limitationsAndExclusions",planResponse.getLmitationAndExclusions());
				planCosts.put("LIMITATIONS", planCost);
			}
			individualPlan.setPlanCosts(planCosts);
			individualPlan.setOptionalDeductible(planResponse.getOptionalDeductible());
		}
		
		individualPlan.setHsa(planResponse.getHsa());
		if(planResponse.getIssuerQualityRating() != null){
			individualPlan.setQualityRating(planResponse.getIssuerQualityRating().get("QualityRating"));
			individualPlan.setIssuerQualityRating(planResponse.getIssuerQualityRating());
			individualPlan.setOverAllQuality(planResponse.getIssuerQualityRating().get("GlobalRating"));
		}
		if(planResponse.getSbcScenarioDTO() != null)
		{
			SbcScenarioDTO sbcDto = planResponse.getSbcScenarioDTO();
			Integer havingBabySBC = getSBCScenarioValues(sbcDto.getBabyCoinsurance(), sbcDto.getBabyCopay(), sbcDto.getBabyDeductible(), sbcDto.getBabyLimit());
			Integer havingDiabetesSBC = getSBCScenarioValues(sbcDto.getDiabetesCoinsurance(), sbcDto.getDiabetesCopay(), sbcDto.getDiabetesDeductible(), sbcDto.getDiabetesLimit());
			Integer simpleFractureSBC = getSBCScenarioValues(sbcDto.getFractureCoinsurance(), sbcDto.getFractureCopay(), sbcDto.getFractureDeductible(), sbcDto.getFractureLimit());
			individualPlan.setHavingBabySBC(havingBabySBC);
			individualPlan.setHavingDiabetesSBC(havingDiabetesSBC);
			individualPlan.setSimpleFractureSBC(simpleFractureSBC);
		}
		return individualPlan;
	}
	
	public List<IndividualPlan> buildPlan(List<PlanRateBenefit> plans, List<Map<String,String>> censusList, InsuranceType insuranceType, BigDecimal aptc, BigDecimal stateSubsidy, Map<Long,Double> costCompareData, String minPremiumPerMember, boolean minimizePlanData, List<ProviderBean> providers, List<PdPersonDTO> memberList, Date coverageStartDate, String[] props, ExchangeType exchangeType, PdPreferencesDTO pdPreferencesDTO,Map<String, Future<Map<String, Object>>> futureProviderResponseListMap, Map<String, List<DrugDTO>> planDrugListMap)
	{	
		long startTimeBuildPlan = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "----buildPlan will be started----", null, false);
		
		List<IndividualPlan> individualPlans = new ArrayList<IndividualPlan>();
		int planCount = 0;
		boolean isDental2017 = isDental2017(insuranceType,coverageStartDate);
		IndividualPlan individualPlan = null;
		for(PlanRateBenefit planMap : plans) {
			planCount++;
			individualPlan = new IndividualPlan();
			individualPlan.setId(planCount);
			individualPlan.setPlanId(planMap.getId());
			individualPlan.setIssuerPlanNumber(planMap.getHiosPlanNumber());
			individualPlan.setPlanType(insuranceType.toString());
			individualPlan.setExchangeType(planMap.getExchangeType());
			individualPlan.setName(planMap.getName());
			if(Plan.PlanLevel.EXPANDEDBRONZE.toString().equals(planMap.getLevel()))
			{
				individualPlan.setLevel(Plan.PlanLevel.BRONZE.toString());
			}
			else
			{
				individualPlan.setLevel(planMap.getLevel());
			}
			individualPlan.setPremium(planMap.getPremium());
			individualPlan.setIssuer(planMap.getIssuer());
			individualPlan.setIssuerText(planMap.getIssuerText());
			individualPlan.setNetworkType(planMap.getNetworkType());
			individualPlan.setPlanBrochureUrl(planMap.getPlanBrochureUrl());
			individualPlan.setSbcUrl(planMap.getSbcDocUrl());
			individualPlan.setIssuerId(planMap.getIssuerId());
			individualPlan.setPrimaryCareCostSharingAfterSetNumberVisits(planMap.getPrimaryCareCostSharingAfterSetNumberVisits());
			individualPlan.setPrimaryCareDeductOrCoinsAfterSetNumberCopays(planMap.getPrimaryCareDeductOrCoinsAfterSetNumberCopays());
			individualPlan.setIssuerLogo(planMap.getIssuerLogo());
			individualPlan.setIsPuf(planMap.getIsPuf());
			individualPlan.setDentalGuarantee(planMap.getGuaranteedVsEstimatedRate());
			individualPlan.setCostSharing(planMap.getCostSharing());				
			setPlanDetailsByMember(planMap.getPlanDetailsByMember(), censusList);
			individualPlan.setPlanDetailsByMember(planMap.getPlanDetailsByMember());
			individualPlan.setPolicyLength(planMap.getPolicyLength());
			individualPlan.setCoinsurance(planMap.getCoinsurance());
			individualPlan.setSeparateDrugDeductible(planMap.getSeparateDrugDeductible());
			individualPlan.setOutOfNetwkCoverage(planMap.getOutOfNetwkCoverage());
			individualPlan.setOutOfCountyCoverage(planMap.getOutOfCountyCoverage());
			individualPlan.setPolicyLimit(planMap.getPolicyLimit());
			individualPlan.setPolicyLengthUnit(planMap.getPolicyLengthUnit());
			individualPlan.setOopMaxFamily(planMap.getOopMaxFamily());
			individualPlan.setOopMaxIndDesc(planMap.getOopMaxAttr());
			individualPlan.setNetworkKey(planMap.getNetworkKey());
			individualPlan.setNetworkTransparencyRating(planMap.getNetworkTransparencyRating());
			if(InsuranceType.HEALTH.equals(insuranceType) && planMap.getTier2util()!=null && !planMap.getTier2util().isEmpty() && !"0%".equals(planMap.getTier2util())) {
				individualPlan.setTier2Coverage(Y);
			}
			if(InsuranceType.HEALTH.equals(insuranceType) && planMap.getMaxCoinseForSpecialtyDrugs()!= null && !(EMPTY_STRING.equals(planMap.getMaxCoinseForSpecialtyDrugs()))){
				individualPlan.setMaxCoinseForSpecialtyDrugs(planMap.getMaxCoinseForSpecialtyDrugs());
			}
			
			if(!minimizePlanData ){
				calculateDeductible(individualPlan, planMap.getPlanCosts(), censusList.size()); 
			}
			
			if(planMap.getHsa()!= null && !(StringUtils.EMPTY.equals(planMap.getHsa()))){
				individualPlan.setHsa(planMap.getHsa());
			}
									
			if(planMap.getFormularyUrl()!= null && !(StringUtils.EMPTY.equals(planMap.getFormularyUrl()))){
				individualPlan.setFormularyUrl(planMap.getFormularyUrl());
			}
			if("CT".equalsIgnoreCase(stateCode)) {
				if(StringUtils.isNotBlank(planMap.getEocDocUrl())) {
					individualPlan.setProviderLink(planMap.getEocDocUrl());
				}
			}else if(planMap.getProviderLink()!= null && !(StringUtils.EMPTY.equals(planMap.getProviderLink()))){
				individualPlan.setProviderLink(planMap.getProviderLink());
			}
			
			if(planMap.getIssuerQualityRating() != null){
				individualPlan.setQualityRating(planMap.getIssuerQualityRating().get("QualityRating"));
				individualPlan.setIssuerQualityRating(planMap.getIssuerQualityRating());
				individualPlan.setOverAllQuality(planMap.getIssuerQualityRating().get("GlobalRating"));
			}
			
			List<Map<String, String>> providersList = new ArrayList<Map<String, String>>();
			if (planMap.getProviders() != null) {
				providersList = planMap.getProviders();
			}
						
			List<HashMap<String, Object>> doctors = prepareProviderDataMap(providers, providersList, individualPlan, futureProviderResponseListMap);
			Map<String, String> docSupport = getProvidersSupportedString(doctors);
			individualPlan.setDoctors(doctors);
			individualPlan.setDoctorsCount(Integer.parseInt(docSupport.get("count")));
			individualPlan.setDoctorsSupported(docSupport.get("supported"));
						
			int totalProvidersCount = doctors.size();
			individualPlan.setTotalProvidersCount(totalProvidersCount);

			int docfacilityCount = individualPlan.getDoctorsCount();
			individualPlan.setDocfacilityCount(docfacilityCount);
			
			if(null!=planMap.getPlanBenefits()){
				//List<Map<String, Map<String,String>>> planBenefitsDisplay = parsePlanBenefits(PlanDisplayEnum.InsuranceType.valueOf(planResponse.getInsuranceType()), planResponse.getPlanBenefits(), individualPlan);
				
				if (PlanDisplayEnum.InsuranceType.AME.equals(insuranceType)) {	
					individualPlan.setPlanTier1(parsePlanBenefitsAME(planMap.getPlanBenefits(), individualPlan, props));
				}else{
					individualPlan.setPlanTier1(parsePlanBenefitsTier1(planMap.getPlanBenefits(), individualPlan, props));
				}
				individualPlan.setPlanTier2(parsePlanBenefitsTier2(planMap.getPlanBenefits(), props));
				individualPlan.setPlanOutNet(parsePlanBenefitsOutNet(planMap.getPlanBenefits(), props));
				individualPlan.setPlanAppliesToDeduct(parseAppliesToDeduct(planMap.getPlanBenefits(), props));
				individualPlan.setNetwkException(parseNetwkExcp(insuranceType, planMap.getPlanBenefits(), props));
				individualPlan.setMissingDentalCovg(parseBenefitCovg(planMap.getPlanBenefits(), props));
				individualPlan.setBenefitExplanation(parseBenefitExpl(planMap.getPlanBenefits(), props));
			}
			
			BigDecimal premiumBeforeCredit = new BigDecimal(Float.toString(planMap.getPremium()));
			individualPlan.setPremiumBeforeCredit(planMap.getPremium());
			BigDecimal annualPremiumBeforeCredit = premiumBeforeCredit.multiply(BigDecimal.valueOf(12));
			individualPlan.setAnnualPremiumBeforeCredit(annualPremiumBeforeCredit.floatValue());
			BigDecimal premiumAfterCredit = new BigDecimal(Float.toString(planMap.getPremium()));
			BigDecimal minPremiumPerHousehold = BigDecimal.valueOf(censusList.size()).multiply(new BigDecimal(minPremiumPerMember));

			BigDecimal applicablePremium = BigDecimal.ZERO;
			BigDecimal ehbPercentage = null;

			if(planMap.getEhbPercentage() != null && !planMap.getEhbPercentage().isEmpty()) {
				ehbPercentage = new BigDecimal(planMap.getEhbPercentage());
			}
			if(InsuranceType.HEALTH.equals(insuranceType) || isDental2017) {
				BigDecimal defaultEHB = BigDecimal.valueOf(1.0);
				if(isDental2017)
				{
					defaultEHB = BigDecimal.valueOf(0.0);
				}
				ehbPercentage = ehbPercentage == null ? defaultEHB  : ehbPercentage;
				applicablePremium = premiumBeforeCredit.multiply(ehbPercentage);
				if(InsuranceType.DENTAL.equals(insuranceType))
				{
					if(!("MN".equalsIgnoreCase(stateCode))){
						BigDecimal childPremiumBeforeCredit = calculateChildPremium(planMap.getPlanDetailsByMember());
						applicablePremium = childPremiumBeforeCredit.multiply(ehbPercentage);
					}
					
				}
			}
			else {
				ehbPercentage = ehbPercentage == null ? BigDecimal.valueOf(0.0) : ehbPercentage;
				applicablePremium = ehbPercentage;
			}

			BigDecimal ehbMinimum = premiumBeforeCredit.subtract(applicablePremium);

			BigDecimal actualMinimum = minPremiumPerHousehold.max(ehbMinimum);

			if(!"CATASTROPHIC".equalsIgnoreCase(planMap.getLevel())) {
				if (aptc.compareTo(BigDecimal.ZERO) > 0) {
					individualPlan.setAptc(aptc.floatValue());
					BigDecimal appliedAptc = aptc;

					if (InsuranceType.DENTAL.equals(insuranceType)) {
						actualMinimum = BigDecimal.ZERO;
					}
					if (appliedAptc.compareTo(applicablePremium) > 0) {
						appliedAptc = applicablePremium.setScale(2, BigDecimal.ROUND_HALF_UP);
					}
					if (premiumBeforeCredit.subtract(appliedAptc).compareTo(actualMinimum) < 0) {
						appliedAptc = premiumBeforeCredit.subtract(actualMinimum).setScale(2, BigDecimal.ROUND_HALF_UP);
						premiumAfterCredit = actualMinimum;
					} else {
						premiumAfterCredit = premiumBeforeCredit.subtract(appliedAptc).setScale(2, BigDecimal.ROUND_HALF_UP);

					}
					individualPlan.setAptc(appliedAptc.setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
				}
				if (stateSubsidy != null &&
						stateSubsidy.compareTo(BigDecimal.ZERO) >= 0) {

					if (premiumAfterCredit.compareTo(actualMinimum) > 0) {
						BigDecimal appliedStateSubsidy = stateSubsidy;

						//State Subsidy larger than premium
						if (appliedStateSubsidy.compareTo(premiumAfterCredit) > 0) {
							//Applied State Subsidy is premium after State Subsidy
							appliedStateSubsidy = premiumAfterCredit.setScale(2, BigDecimal.ROUND_HALF_UP);
						}
						//New premium is lower than minimum premium per household
						if (premiumAfterCredit.subtract(appliedStateSubsidy).compareTo(actualMinimum) < 0) {
							appliedStateSubsidy = premiumAfterCredit.subtract(actualMinimum).setScale(2, BigDecimal.ROUND_HALF_UP);
							premiumAfterCredit = actualMinimum;
						}
						//Enough left over premium to apply State Subsidy
						else {
							premiumAfterCredit = premiumAfterCredit.subtract(appliedStateSubsidy).setScale(2, BigDecimal.ROUND_HALF_UP);
						}

						individualPlan.setStateSubsidy(appliedStateSubsidy.setScale(2, BigDecimal.ROUND_HALF_UP));
					} else {
						individualPlan.setStateSubsidy(new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP));
					}
				}
			}
			else if(memberList != null && !memberList.isEmpty())
			{
				BigDecimal totalPremium = BigDecimal.ZERO;
				BigDecimal totalContribution = BigDecimal.ZERO;
					
				final List<Map<String,String>> memberInfo = planMap.getPlanDetailsByMember();
				final Map<String,Float> contributionData = new HashMap<String,Float>();
				
				for(int i = 0; i < memberInfo.size(); i++)
	            {
					PdPersonDTO pdPersonDTO = this.getMemberByMemberId(memberInfo.get(i).get("id"), memberList);
					BigDecimal adjustedPremium = BigDecimal.ZERO;
					BigDecimal adjustedContribution;
					BigDecimal mlPremium = new BigDecimal(memberInfo.get(i).get(PREMIUM));
					BigDecimal mlContribution;
					if(InsuranceType.HEALTH.equals(insuranceType)) {
						mlContribution = pdPersonDTO != null ? new BigDecimal(Float.toString(pdPersonDTO.getHealthSubsidy())) : BigDecimal.ZERO;
					} else {
						mlContribution = pdPersonDTO != null ? new BigDecimal(Float.toString(pdPersonDTO.getDentalSubsidy())) : BigDecimal.ZERO;
					}
					if(mlPremium.compareTo(mlContribution) > 0) {
						adjustedPremium = mlPremium.subtract(mlContribution).setScale(2,BigDecimal.ROUND_HALF_UP);
						adjustedContribution = mlContribution;
                    } else {
                    	adjustedContribution = mlPremium;
                    }	
					adjustedContribution = adjustedContribution.setScale(2,BigDecimal.ROUND_HALF_UP);
					totalPremium = totalPremium.add(adjustedPremium);
					totalContribution = totalContribution.add(adjustedContribution);
					contributionData.put(memberInfo.get(i).get("id"), adjustedContribution.floatValue());
	            }  
				
				premiumAfterCredit = totalPremium;

				individualPlan.setAptc(totalContribution.floatValue());
				individualPlan.setTotalContribution(totalContribution.floatValue());
				individualPlan.setContributionData(contributionData);
			}

			if("CATASTROPHIC".equalsIgnoreCase(planMap.getLevel()) && "Y".equalsIgnoreCase(isStateSubsidyEnabled) && stateSubsidy != null &&
					stateSubsidy.compareTo(BigDecimal.ZERO) >= 0){
				individualPlan.setStateSubsidy(BigDecimal.ZERO);
			}
			
			BigDecimal annualPremiumAfterCredit = premiumAfterCredit.multiply(BigDecimal.valueOf(12));
			
			individualPlan.setPremiumAfterCredit(premiumAfterCredit.floatValue());
			individualPlan.setAnnualPremiumAfterCredit(annualPremiumAfterCredit.floatValue());
						
			String encodedPremium = getEncodedPremium(planMap.getId(),premiumBeforeCredit.floatValue(),premiumAfterCredit.floatValue(),planMap.getPlanDetailsByMember());
			if(encodedPremium != null){
				individualPlan.setEncodedPremium(encodedPremium);
			}
			
			String netwkType = "inNetworkInd".intern();
			String combinedInOutNetType="combinedInOutNetworkInd".intern();
			if(censusList.size() > 1){
				netwkType = "inNetworkFly".intern();
				combinedInOutNetType="combinedInOutNetworkFly".intern();
			}
			Map<String, Map<String, String>> planCost = planMap.getPlanCosts();
			
			if(InsuranceType.HEALTH.equals(insuranceType) ){
				setOopMaxForHealth(planCost, netwkType, individualPlan);
			}else if (PlanDisplayEnum.InsuranceType.STM.equals(insuranceType)) {
				float stmOopMax = planMap.getOopMax() != null ? Float.parseFloat(planMap.getOopMax().toString()) : new Float(0);
				individualPlan.setOopMax(stmOopMax);
			}else{
				if(planCost !=null){
					if(planCost.get("MAX_OOP_MEDICAL") != null && planCost.get("MAX_OOP_MEDICAL").get(netwkType) !=null && !(StringUtils.EMPTY.equals(planCost.get("MAX_OOP_MEDICAL").get(netwkType)))){
						individualPlan.setOopMax(Float.parseFloat(planCost.get("MAX_OOP_MEDICAL").get(netwkType)));
					}else if(planCost.get("MAX_OOP_MEDICAL") != null && planCost.get("MAX_OOP_MEDICAL").get(combinedInOutNetType) !=null && !(StringUtils.EMPTY.equals(planCost.get("MAX_OOP_MEDICAL").get(combinedInOutNetType)))){
						individualPlan.setOopMax(Float.parseFloat(planCost.get("MAX_OOP_MEDICAL").get(combinedInOutNetType)));
					}
				}
			}
			if(PlanDisplayEnum.InsuranceType.HEALTH.equals(insuranceType) && individualPlan.getOopMax() == null && minimizePlanData == false){
				continue;
			}
			
			setSbcCost(planMap.getSbcScenarioDTO(), individualPlan);
			
			PrescriptionSearch availabilityObject = prescriptionFactory.getObject();
			List<DrugDTO> planDrugList = availabilityObject.getPlanDrugList(planDrugListMap, individualPlan.getIssuerPlanNumber());

			individualPlan.setPrescriptionResponseList(planDrugList);
			
			Float oopMaxTemp = individualPlan.getOopMax() == null ?  0.0f : individualPlan.getOopMax() ;
			individualPlan.setEstimatedTotalHealthCareCost(BigDecimal.valueOf(individualPlan.getAnnualPremiumAfterCredit()).add(BigDecimal.valueOf(oopMaxTemp)).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue());
			if(PlanDisplayEnum.InsuranceType.HEALTH.equals(insuranceType)){
				Double estimatedOop = costCompareData.get(Long.valueOf(planMap.getId()));
				try{
					if (estimatedOop != null){
						estimatedOop = calculateEstimatedOopForMedicalProcedure(pdPreferencesDTO, individualPlan, estimatedOop);
												
						estimatedOop = calculateEstamatedOopForDrugList(planDrugList, estimatedOop);
												
						individualPlan.setEstimatedTotalHealthCareCost(BigDecimal.valueOf(individualPlan.getAnnualPremiumAfterCredit()).add(BigDecimal.valueOf(estimatedOop)).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue());
					}
				}catch(Exception ex){
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error while calculating EstimatedTotalHealthCareCost for PlanId ---->"+individualPlan.getPlanId()+" Calculated Expense estimate: "+estimatedOop+" Annual Premium: "+individualPlan.getAnnualPremiumAfterCredit(), ex, false);
				}
			}
			
			String showPufPremium = DynamicPropertiesUtil.getPropertyValue(PlanDisplayConfiguration.PlanDisplayConfigurationEnum.SHOW_PUF_PREMIUM);
			if (Y.equalsIgnoreCase(planMap.getIsPuf())) {
				individualPlan.setEstimatedTotalHealthCareCost(99999);
				individualPlan.setDeductible(99999);
				if ("OFF".equalsIgnoreCase(showPufPremium)) {
					individualPlan.setPremiumAfterCredit(99999);
					individualPlan.setPremium(99999f);
				}
			}
			
			Map<String, Map<String, String>> planCosts = new HashMap<String, Map<String, String>>();
			if(planMap.getPlanCosts() != null) {
				planCosts = planMap.getPlanCosts();
			}
			if (PlanDisplayEnum.InsuranceType.STM.equals(insuranceType)) {				
				Map<String, String> planCostNew = new HashMap<String, String>();
				planCostNew.put("limitationsAndExclusions", planMap.getExclusionsAndLimitations());
				planCosts.put("LIMITATIONS", planCostNew);
			}
			individualPlan.setPlanCosts(planCosts);
			
			individualPlan.setOptionalDeductible(planMap.getOptionalDeductible());
			
			if (InsuranceType.DENTAL.equals(insuranceType)) {
				if (individualPlan.getPlanCosts() != null && individualPlan.getPlanCosts().get("ANNUAL_MAXIMUM_BENEFIT") != null && individualPlan.getPlanCosts().get("ANNUAL_MAXIMUM_BENEFIT").get("inNetworkInd") != null) {
					individualPlan.setAnnualLimit(Integer.valueOf(individualPlan.getPlanCosts().get("ANNUAL_MAXIMUM_BENEFIT").get("inNetworkInd")));
				}
			}
			
			if(ExchangeType.ON.equals(exchangeType)){
				individualPlan.setPlanTab(PlanTab.TAX_CREDIT);
			}else{
				individualPlan.setPlanTab(PlanTab.FULL_PRICE);
			}

			individualPlans.add(individualPlan);
		}
		
		long endTimeBuildPlan = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "buildPlan ended in " + (endTimeBuildPlan - startTimeBuildPlan) + " ms", null, false);
		
		return individualPlans;
	}	

	private BigDecimal calculateChildPremium(List<Map<String, String>> planDetailsByMember) 
	{
		BigDecimal childPremium = new BigDecimal("0");
		for(Map<String,String> memberinfo : planDetailsByMember)
		{
			String age = memberinfo.get(PlanDisplayUtil.AGE);
			if(Float.parseFloat(age) < PlanDisplayConstants.CHILD_AGE)
			{
				childPremium = childPremium.add(new BigDecimal(memberinfo.get("premium")));
			}
		}
		return childPremium;
	}

	private void setPlanDetailsByMember(List<Map<String, String>> memberInfoList, List<Map<String, String>> censusList) {
		if(memberInfoList != null){
			for(int i = 0; i < memberInfoList.size(); i++) {
				Map<String,String> memberInfo = memberInfoList.get(i);	
				if(memberInfo != null){
					for(Map<String,String> member : censusList) {
						String externalId = member.get("id");
						if(memberInfo.get("id").equalsIgnoreCase(externalId)) {							
							memberInfo.put(PERSON_COVG_DATE, member.get(PERSON_COVG_DATE));
							memberInfo.put(PlanDisplayUtil.AGE, member.get(PlanDisplayUtil.AGE));
						}
					}
				}				
			}
		}	
	}


	private Double calculateEstamatedOopForDrugList(List<DrugDTO> planDrugList, Double estimatedOop) {
		if(planDrugList != null)
		{
			for (DrugDTO drugDTO : planDrugList)
			{
				boolean addFairPrice = false;
				if (!"Y".equalsIgnoreCase(drugDTO.getIsDrugCovered()) && StringUtils.isNotBlank(drugDTO.getDrugFairPrice()))
				{
					addFairPrice = true;
				}
				if (StringUtils.isNotBlank(drugDTO.getGenericNdc()) && "Y".equalsIgnoreCase(drugDTO.getIsGenericCovered()))
				{
					addFairPrice = false;
				}
				if (addFairPrice)
				{
					estimatedOop += Double.parseDouble(drugDTO.getDrugFairPrice());
				}
			}
		}

		return estimatedOop;
	}


	private Double calculateEstimatedOopForMedicalProcedure(PdPreferencesDTO pdPreferencesDTO, IndividualPlan individualPlan, Double estimatedOop) {
		if(pdPreferencesDTO.getMedicalProcedure() != null){
			Float oopMaxTemp = individualPlan.getOopMax() == null ?  0.0f : individualPlan.getOopMax() ;
			Integer sbcCost = 0;
			if(MedicalProcedure.PREGNANCY.equals(pdPreferencesDTO.getMedicalProcedure())){
				sbcCost = individualPlan.getHavingBabySBC();
			}else if(MedicalProcedure.FRACTURE.equals(pdPreferencesDTO.getMedicalProcedure())){
				sbcCost = individualPlan.getSimpleFractureSBC();
			}
			Double estimatedOOPwithSbc = estimatedOop + sbcCost;
			if(oopMaxTemp < estimatedOOPwithSbc){
				estimatedOop = oopMaxTemp.doubleValue();
			}else{
				estimatedOop = estimatedOOPwithSbc;
			}
		}
		return estimatedOop;
	}


	private void setSbcCost(SbcScenarioDTO sbcScenarioDTO, IndividualPlan individualPlan) {
		if(sbcScenarioDTO != null){
			SbcScenarioDTO sbcDto = sbcScenarioDTO;
			Integer havingBabySBC = getSBCScenarioValues(sbcDto.getBabyCoinsurance(), sbcDto.getBabyCopay(), sbcDto.getBabyDeductible(), sbcDto.getBabyLimit());
			Integer havingDiabetesSBC = getSBCScenarioValues(sbcDto.getDiabetesCoinsurance(), sbcDto.getDiabetesCopay(), sbcDto.getDiabetesDeductible(), sbcDto.getDiabetesLimit());
			Integer simpleFractureSBC = getSBCScenarioValues(sbcDto.getFractureCoinsurance(), sbcDto.getFractureCopay(), sbcDto.getFractureDeductible(), sbcDto.getFractureLimit());
			individualPlan.setHavingBabySBC(havingBabySBC);
			individualPlan.setHavingDiabetesSBC(havingDiabetesSBC);
			individualPlan.setSimpleFractureSBC(simpleFractureSBC);
		}		
	}


	private void setOopMaxForHealth(Map<String, Map<String, String>> planCost, String netwkType, IndividualPlan individualPlan) {
		if(planCost!=null){
			if(planCost.get("MAX_OOP_INTG_MED_DRUG") != null && planCost.get("MAX_OOP_INTG_MED_DRUG").get(netwkType) !=null && !(StringUtils.EMPTY.equals(planCost.get("MAX_OOP_INTG_MED_DRUG").get(netwkType)))){
				individualPlan.setIntgMediDrugOopMax(Float.parseFloat(planCost.get("MAX_OOP_INTG_MED_DRUG").get(netwkType)));
			}
			if(individualPlan.getIntgMediDrugOopMax() == null){
				if(planCost.get("MAX_OOP_MEDICAL") != null && planCost.get("MAX_OOP_MEDICAL").get(netwkType) !=null && !(StringUtils.EMPTY.equals(planCost.get("MAX_OOP_MEDICAL").get(netwkType)))){
					individualPlan.setMedicalOopMax(Float.parseFloat(planCost.get("MAX_OOP_MEDICAL").get(netwkType)));
				}
				if(planCost.get("MAX_OOP_DRUG") != null && planCost.get("MAX_OOP_DRUG").get(netwkType) !=null && !(StringUtils.EMPTY.equals(planCost.get("MAX_OOP_DRUG").get(netwkType)))){
					individualPlan.setDrugOopMax(Float.parseFloat(planCost.get("MAX_OOP_DRUG").get(netwkType)));
				}
				float medicalOopMax = individualPlan.getMedicalOopMax()!=null?individualPlan.getMedicalOopMax():0;
				float drugOopMax = individualPlan.getDrugOopMax()!=null?individualPlan.getDrugOopMax():0;
				individualPlan.setOopMax(new BigDecimal(Float.toString(medicalOopMax)).add(new BigDecimal(Float.toString(drugOopMax))).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue());
			}else{
				individualPlan.setOopMax(individualPlan.getIntgMediDrugOopMax());
			}
		}
	}


	public DrugDTO updatePlanAvailabilityCoverage(Map<String, Future<PrescriptionData>> futurePlanAvailabilityListMap,
			DrugDTO drugResponseDTO, String hiosPlanNumber, String drugNdc, boolean isGeneridNdc) {
		if(futurePlanAvailabilityListMap.containsKey(drugNdc)){
			Map<String, Object> planAvailabilityResponse = null;
			Future<PrescriptionData> futurePlanAvailabilityResponse = futurePlanAvailabilityListMap.get(drugNdc);
			try {
				PrescriptionData data=futurePlanAvailabilityResponse.get();
				planAvailabilityResponse = data.getVericredPrescriotions();
			} catch (InterruptedException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			} catch (ExecutionException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			}	
			if(planAvailabilityResponse!=null && planAvailabilityResponse.get("drug_coverages") != null){
				@SuppressWarnings("unchecked")
				List<Map<String,String>> vericredPlanAvailabilityList = (List<Map<String,String>>) planAvailabilityResponse.get("drug_coverages");			
				if(vericredPlanAvailabilityList != null){
					for(Map<String,String> vericredPlanAvailability : vericredPlanAvailabilityList){
						if(hiosPlanNumber.equalsIgnoreCase(vericredPlanAvailability.get("plan_id"))){
							if(isGeneridNdc)
							{
								if(StringUtils.isNotBlank(vericredPlanAvailability.get("tier"))){
									drugResponseDTO.setGenericTier(vericredPlanAvailability.get("tier"));
									if(drugCoveredArray.contains(drugResponseDTO.getGenericTier())){
										drugResponseDTO.setIsGenericCovered("Y");
									}else if("not_covered".equalsIgnoreCase(drugResponseDTO.getGenericTier())){
										drugResponseDTO.setIsGenericCovered("N");
									}else{
										drugResponseDTO.setIsGenericCovered("U");
									}
								}else{
									drugResponseDTO.setGenericTier("not_listed");
									drugResponseDTO.setIsGenericCovered("U");
								}
							}
							else
							{
								if(StringUtils.isNotBlank(vericredPlanAvailability.get("tier"))){
									drugResponseDTO.setDrugTier(vericredPlanAvailability.get("tier"));
									if(drugCoveredArray.contains(drugResponseDTO.getDrugTier())){
										drugResponseDTO.setIsDrugCovered("Y");
									}else if("not_covered".equalsIgnoreCase(drugResponseDTO.getDrugTier())){
										drugResponseDTO.setIsDrugCovered("N");
									}else{
										drugResponseDTO.setIsDrugCovered("U");
									}
								}else{
									drugResponseDTO.setDrugTier("not_listed");
									drugResponseDTO.setIsDrugCovered("U");
								}
							}
							return drugResponseDTO;
						}
					}
				}
			}
		}
		return drugResponseDTO;
	}

	public List<DrugDTO> parsePrescriptionJson(String prescriptionJson) {
		List<DrugDTO> drugList = new ArrayList<DrugDTO>();
		try{
			if (prescriptionJson != null) {
				drugList = platformGson.fromJson(prescriptionJson, new TypeToken<List<DrugDTO>>() {}.getType());
			}
		}catch(Exception ex){}
		
		return drugList;
	}
	
	public List<String> prepareNdcList(List<DrugDTO> drugList) {
		List<String> drugNdcList = new ArrayList<String>();
		if(drugList != null && drugList.size() > 0){
			for(DrugDTO drugDTO : drugList){
				drugNdcList.add(drugDTO.getDrugNdc());
				if(StringUtils.isNotBlank(drugDTO.getGenericNdc())){
					drugNdcList.add(drugDTO.getGenericNdc());
				}
			}
		}
		return drugNdcList;
	}
	
	public List<String> prepareRxCodeList(List<DrugDTO> drugList) {
		List<String> drugRxCodeList = new ArrayList<String>();
		if(drugList != null && drugList.size() > 0){
			for(DrugDTO drugDTO : drugList){
				drugRxCodeList.add(drugDTO.getDrugRxCode());
				if(StringUtils.isNotBlank(drugDTO.getGenericRxCode())){
					drugRxCodeList.add(drugDTO.getGenericRxCode());
				}
			}
		}
		return drugRxCodeList;
	}


	private static Integer getSBCScenarioValues(Integer coinsurance, Integer copay, Integer deductible, Integer limit)
	{
		Integer scenarioValue = null;
		if (coinsurance != null)
		{
			scenarioValue = coinsurance;
		}
		if (copay != null)
		{
			if (scenarioValue != null)
			{
				scenarioValue = scenarioValue.intValue() + copay.intValue();
			}
			else
			{
				scenarioValue = copay;
			}
		}
		if (deductible != null)
		{
			if (scenarioValue != null)
			{
				scenarioValue = scenarioValue.intValue() + deductible.intValue();
			}
			else
			{
				scenarioValue = deductible;
			}
		}
		if (limit != null)
		{
			if (scenarioValue != null)
			{
				scenarioValue = scenarioValue.intValue() + limit.intValue();
			}
			else
			{
				scenarioValue = limit;
			}
		}
		return scenarioValue;
	}
	
	private boolean isDental2017(InsuranceType insuranceType, Date coverageStartDate) {
		final String stateExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);					
		 if("Individual".equalsIgnoreCase(stateExchangeType) && InsuranceType.DENTAL.equals(insuranceType)){
			 String configChangeDate = "01-01-2017";
			 DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
			 Date formattedConfigChangeDate;
			 try 
			 {
				 String programType = DynamicPropertiesUtil.getPropertyValue("global.StateExchangeType");
				 if (programType != null && programType.equalsIgnoreCase(ProgramType.INDIVIDUAL.toString())) {
					 formattedConfigChangeDate = dateFormat.parse(configChangeDate);
					 if(coverageStartDate != null && coverageStartDate.before(formattedConfigChangeDate)) {
						 return false;
					 } else {
						 return true;
					 }
				 } 
				 else {
					 return false;
				 }
			 }
			 catch (ParseException e) {
				 putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error while Date formatted Change---->", e, false);
			 } 
		 }
		return false;
	}


	private PdPersonDTO getMemberByMemberId(String memberId, List<PdPersonDTO> personDTOList) {
		for(PdPersonDTO pdPersonDTO : personDTOList) {
			if(pdPersonDTO.getExternalId().equals(memberId)) {
				return pdPersonDTO;
			}
		}
		return null;
	}
	
	private List<HashMap<String, Object>> prepareProviderDataMap(List<ProviderBean> providersBeanList,List<Map<String, String>> providersInPlan, IndividualPlan individualPlan, Map<String, Future<Map<String, Object>>> futureProviderResponseListMap) {
		List<HashMap<String, Object>> providerDataList = new ArrayList<>();
		for (ProviderBean providerBean : providersBeanList) {
			HashMap<String, Object> data = new HashMap<>();

			data.put("providerId", providerBean.getId());
			data.put("providerName", providerBean.getName());
			data.put("providerCity", providerBean.getCity());
			data.put("providerState", providerBean.getState());
			data.put("providerSpecialty", providerBean.getSpecialty());
			ProviderSearch providerSearch = providerFactory.getObject();
			data.put("networkStatus", providerSearch.getNetworkStatus(futureProviderResponseListMap, individualPlan, providerBean, providersInPlan));
			
			data.put("planSupported", StringUtils.EMPTY);
			data.put("providerType", providerBean.getProviderType());//By default this should come as doctor as set in ProviderBean Shared Module.
			data.put("providerAddress", providerBean.getAddress());
			data.put("providerZip", providerBean.getZip());
			data.put("providerPhone", providerBean.getPhone());
			data.put("groupKey", providerBean.getGroupKey());
			data.put("locationId", providerBean.getLocationId());

			providerDataList.add(data);
		}

		return providerDataList;
	}
	
	public Map<String, Future<PrescriptionData>> prepareDrugHiosMap(List<String> drugNdcList, String state){		
		HttpHeaders headers = new HttpHeaders();
		String vericredUrl = DynamicPropertiesUtil.getPropertyValue("planSelection.vericredUrl");
		String vericredApiKey = DynamicPropertiesUtil.getPropertyValue("planSelection.vericredApiKey");
		headers.set("VERICRED_API_KEY", vericredApiKey);
		@SuppressWarnings({ "rawtypes", "unchecked" })
		HttpEntity entity = new HttpEntity(headers);
		
		Map<String, Future<PrescriptionData>> futurePlanAvailabilityListMap = new HashMap<String, Future<PrescriptionData>>();
		if(drugNdcList != null){
		for (String drugNdc : drugNdcList) {
			if(StringUtils.isNotBlank(drugNdc) && drugNdc.length() == 11){
				String formattedNdc = drugNdc.substring(0, 5) + "-" + drugNdc.substring(5, 9) + "-" + drugNdc.substring(9, 11);
				String sourceUrl = vericredUrl+"/drug_packages/"+formattedNdc+"/coverages?audience=individual&state_code="+state;
				Future<PrescriptionData> futurePlanAvailabilityResponse = planDisplayAsyncUtil.callPrescriptionAPIAsync(sourceUrl, entity);
				futurePlanAvailabilityListMap.put(drugNdc, futurePlanAvailabilityResponse);
			}
		}
		}
		return futurePlanAvailabilityListMap;
	}
	
	/**
	 * @deprecated This method is not in use.
	 * @param drugNdcList
	 * @param planHiosList
	 * @param futurePlanAvailabilityListMap
	 * @return
	 */
	@Deprecated
	public List<PlanAvailabilityForDrugResponse> prepareDrugPlanMap(List<String> drugNdcList, List<String> planHiosList, Map<String, Future<Map<String, Object>>> futurePlanAvailabilityListMap) {
		List<PlanAvailabilityForDrugResponse> planAvailabilityForDrugResponseList = new ArrayList<PlanAvailabilityForDrugResponse>();
		for (String drugNdc : drugNdcList) {
			PlanAvailabilityForDrugResponse planAvailabilityForDrugResponse = new PlanAvailabilityForDrugResponse();
			planAvailabilityForDrugResponse.setDrugNdc(drugNdc);
			List<Map<String, String>> plans = new ArrayList<Map<String, String>>();
			for (String planHiosId : planHiosList) {
				Map<String, String> planAvailability = new HashMap<String, String>();
				planAvailability.put("planHiosId", planHiosId);
				planAvailability.put("tier", "not_covered");
				plans.add(planAvailability);
			}
			planAvailabilityForDrugResponse.setPlans(plans);
			
			if(futurePlanAvailabilityListMap.containsKey(drugNdc)){
				Future<Map<String, Object>> futurePlanAvailabilityResponse = futurePlanAvailabilityListMap.get(drugNdc);
				Map<String, Object> planAvailabilityResponse = null;
				try {
					planAvailabilityResponse = futurePlanAvailabilityResponse.get();
				} catch (InterruptedException e) {
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				} catch (ExecutionException e) {
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				}			
			
				if(planAvailabilityResponse!=null && planAvailabilityResponse.get("drug_coverages") != null){
					@SuppressWarnings("unchecked")
					List<Map<String,String>> vericredPlanAvailabilityList = (List<Map<String,String>>) planAvailabilityResponse.get("drug_coverages");			
					if(vericredPlanAvailabilityList != null){
						for(Map<String, String> plan : plans){
							for(Map<String,String> vericredPlanAvailability : vericredPlanAvailabilityList){
								if(plan.get("planHiosId").equalsIgnoreCase(vericredPlanAvailability.get("plan_id"))){
									plan.put("tier", (String)vericredPlanAvailability.get("tier"));
									break;
								}
							}
						}
					}
				}
			}
			planAvailabilityForDrugResponse.setPlans(plans);
			planAvailabilityForDrugResponseList.add(planAvailabilityForDrugResponse);
		}
		return planAvailabilityForDrugResponseList;
	}
	
	public static String create14DigitNum(String hiosPlanNumber){
		String hiosPlanId = StringUtils.EMPTY;
		if(hiosPlanNumber != null){
			hiosPlanId = hiosPlanNumber.substring(0, 14);
		}
		return hiosPlanId;
	}
	
	private static Map<String, String> getProvidersSupportedString(List<HashMap<String, Object>> providers) {
		String providerSupported = StringUtils.EMPTY;
		int supported = 0;
		Boolean tier2Present = false;
		int providerSize = providers.size();

		for (Map<String, Object> provider : providers) {
			String planSupported = (String) provider.get("planSupported");
			if ("tier2".equals(planSupported)) {
				tier2Present = true;
			} else if (Boolean.parseBoolean(planSupported)) {
				supported++;
			}
		}

		if (tier2Present) {
			providerSupported = "half-supported";
		} else if (supported == 0) {
			providerSupported = "not-supported";
		} else if (supported == providerSize) {
			providerSupported = "supported";
		} else if (supported < providerSize) {
			providerSupported = "half-supported";
		}

		Map<String, String> providerSupport = new HashMap<String, String>();
		providerSupport.put("count", StringUtils.EMPTY + supported);
		providerSupport.put("supported", providerSupported);
		return providerSupport;
	}
	
	public static void calculateDeductible(IndividualPlan individualPlan, Map<String, Map<String, String>> planCost,int  personCount){
		try {
			
			if (PlanDisplayEnum.InsuranceType.STM.equals(PlanDisplayEnum.InsuranceType.valueOf(individualPlan.getPlanType()))) {
				String inNetworkIndv = PlanDisplayConstants.STM_IN_NETWORK_IND;
				Map<String, String> stmDeductible = planCost.get(PlanDisplayConstants.STM_DEDUCTIBLE);

				if (stmDeductible != null && (stmDeductible.get(inNetworkIndv) != null && !(StringUtils.EMPTY.equals(stmDeductible.get(inNetworkIndv)))) ) {
					//if (stmDeductible.get(inNetworkIndv) != null && !(stmDeductible.get(inNetworkIndv).equals(StringUtils.EMPTY))) {
						individualPlan.setDeductible(Integer.parseInt(stmDeductible.get(inNetworkIndv)));
					//}
				}
				return;
			}
			if (PlanDisplayEnum.InsuranceType.AME.equals(PlanDisplayEnum.InsuranceType.valueOf(individualPlan.getPlanType()))) {
				Map<String, String> ameDeductible = planCost.get(PlanDisplayConstants.AME_DEDUCTIBLE);
				if (ameDeductible != null && (ameDeductible.get("deductibleVal") != null && !("".equals(ameDeductible.get("deductibleVal")))) ) {
						individualPlan.setDeductible(Integer.parseInt(ameDeductible.get("deductibleVal") ));
				}
				return;
			}
			
			Map<String, String> deductibleIntgMedDrug = planCost.get(PlanDisplayConstants.DEDUCTIBLE_INTG_MED_DRUG);
			Map<String, String> deductibleMedical = planCost.get(PlanDisplayConstants.DEDUCTIBLE_MEDICAL);
			Map<String, String> deductibleDrug = planCost.get(PlanDisplayConstants.DEDUCTIBLE_DRUG);
			
			String inNetworkFlyOrIndv = personCount> 1?PlanDisplayConstants.IN_NETWORK_FLY:PlanDisplayConstants.IN_NETWORK_IND;
			String combinedNetworkFlyOrIndv = personCount> 1?PlanDisplayConstants.COMBINED_INOUTNETWORK_FLY:PlanDisplayConstants.COMBINED_INOUTNETWORK_IND;
			
			if (PlanDisplayEnum.InsuranceType.DENTAL.equals(PlanDisplayEnum.InsuranceType.valueOf(individualPlan.getPlanType())) && !"CA".equalsIgnoreCase(stateCode)) {
				inNetworkFlyOrIndv = PlanDisplayConstants.IN_NETWORK_IND;
				combinedNetworkFlyOrIndv = PlanDisplayConstants.COMBINED_INOUTNETWORK_IND;
			}
			
			if(deductibleIntgMedDrug !=null){
				if(deductibleIntgMedDrug.get(inNetworkFlyOrIndv) !=null && !(StringUtils.EMPTY.equals(deductibleIntgMedDrug.get(inNetworkFlyOrIndv)))){
					individualPlan.setIntgMediDrugDeductible(Integer.parseInt(deductibleIntgMedDrug.get(inNetworkFlyOrIndv)));
				}else if(deductibleIntgMedDrug.get(combinedNetworkFlyOrIndv) !=null && !(StringUtils.EMPTY.equals(deductibleIntgMedDrug.get(combinedNetworkFlyOrIndv)))){
					individualPlan.setIntgMediDrugDeductible(Integer.parseInt(deductibleIntgMedDrug.get(combinedNetworkFlyOrIndv)));
				}
			}
			if(individualPlan.getIntgMediDrugDeductible()==null){					
				if(deductibleMedical !=null){
					if(deductibleMedical.get(inNetworkFlyOrIndv) !=null && !(StringUtils.EMPTY.equals(deductibleMedical.get(inNetworkFlyOrIndv)) )){
						individualPlan.setMedicalDeductible(Integer.parseInt(deductibleMedical.get(inNetworkFlyOrIndv)));
					}else if(deductibleMedical.get(combinedNetworkFlyOrIndv) !=null && !(StringUtils.EMPTY.equals(deductibleMedical.get(combinedNetworkFlyOrIndv)) )){
						individualPlan.setMedicalDeductible(Integer.parseInt(deductibleMedical.get(combinedNetworkFlyOrIndv)));
					}
				}					
				if(deductibleDrug !=null){
					if(deductibleDrug.get(inNetworkFlyOrIndv) !=null && !(StringUtils.EMPTY.equals(deductibleDrug.get(inNetworkFlyOrIndv)))){
						individualPlan.setDrugDeductible(Integer.parseInt(deductibleDrug.get(inNetworkFlyOrIndv)));
					}else if(deductibleDrug.get(combinedNetworkFlyOrIndv) !=null && !(StringUtils.EMPTY.equals(deductibleDrug.get(combinedNetworkFlyOrIndv)) )){
						individualPlan.setDrugDeductible(Integer.parseInt(deductibleDrug.get(combinedNetworkFlyOrIndv)));
					}
				}
				int medicalDeductible = individualPlan.getMedicalDeductible()!=null?individualPlan.getMedicalDeductible():0;
				int drugDeductible = individualPlan.getDrugDeductible()!=null?individualPlan.getDrugDeductible():0;
				individualPlan.setDeductible(medicalDeductible+drugDeductible);
			}else{
				individualPlan.setDeductible(individualPlan.getIntgMediDrugDeductible() + 1);
			}

		} catch (Exception e) {
			putLoggerStmt(LOGGER, LogLevel.ERROR, "<----In calculateDeductible method---->"+e.getMessage(), e, false);
		}
	}
	
	
	private static Map<String, Map<String, String>> initializeAppliesValues(String[] props) {
		Map<String, Map<String, String>> appliesToDeduct = new HashMap<String, Map<String, String>>();
		Map<String, String> applies = null;
		for (String prop : props) {
			applies = new HashMap<String, String>(2);
			applies.put("AppliesNetwk".intern(), "Not Applicable".intern());
			applies.put("AppliesNonNetwk".intern(), "Not Applicable".intern());

			appliesToDeduct.put(prop, applies);
		}

		return appliesToDeduct;

	}

	
	private static Map<String, Map<String, String>> initializeNetworkExcpValues(String[] props) {
		Map<String, Map<String, String>> netwkException = new HashMap<String, Map<String, String>>();
		Map<String,String> netwkExce = null;
		for (String prop : props) {
			netwkExce = new HashMap<String,String>(3);
			netwkExce.put("NetwkExce".intern(), StringUtils.EMPTY);
			netwkExce.put("NetwkLimit".intern(), StringUtils.EMPTY);
			netwkExce.put("NetwkLimitAttrib".intern(), StringUtils.EMPTY);
			netwkException.put(prop, netwkExce);
		}
		return netwkException;
	}
	
	private static Map<String, Map<String, String>> initializeBenefitCoverageValues(String[] props) {
		Map<String, Map<String, String>> benefitCoverage = new HashMap<String, Map<String, String>>();
		Map<String,String> coverage = null;
		for (String prop : props) {
			coverage = new HashMap<String,String>(1);
			coverage.put(PlanDisplayConstants.IS_COVERED, NOT_COVERED);
			benefitCoverage.put(prop, coverage);
		}
		return benefitCoverage;
	}
	
	public static Map<String, Map<String,String>> parsePlanBenefitsTier1(Map<String, Map<String, String>> planBenefits,IndividualPlan individualPlan, String[] props){
		Map<String, Map<String, String>> tierBenefits = new HashMap<String, Map<String, String>>();
		for(String prop : props){
			Map<String, String> benefitAttrib = planBenefits.get(prop);
			if(benefitAttrib != null){
				Map<String, String> benefitData = new HashMap<String, String>();
				
				benefitData.put(DISPLAY_VAL, benefitAttrib.get(NETWORK_T1_DISP));
				benefitData.put(TILE_DISPLAY_VAL, benefitAttrib.get(NETWORK_T1_TILE_DISP));
				//isCovered  is needed for calculating the plan score
				benefitData.put(PlanDisplayConstants.IS_COVERED, benefitAttrib.get(PlanDisplayConstants.IS_COVERED));
				if(individualPlan != null && !"WA".equalsIgnoreCase(stateCode)){
					/*  copay for office visits  filter*/
					if("PRIMARY_VISIT".equalsIgnoreCase(prop)){
						String deductibleVal = getDeductibleVal(benefitAttrib, benefitAttrib.get(NETWORK_T1_DISP));
						individualPlan.setCopayOfficeVisit(deductibleVal);
					}
					/*  copay for genric drug filter*/
					if("generic".equalsIgnoreCase(prop)){
						String deductibleVal = getDeductibleVal(benefitAttrib, benefitAttrib.get(NETWORK_T1_DISP));
						individualPlan.setCopayGenericDrug(deductibleVal);
					}
				}
				tierBenefits.put(prop, benefitData);
			}
		}
		
		return tierBenefits;
	}
	
	public static Map<String, Map<String,String>> parsePlanBenefitsAME(Map<String, Map<String, String>> planBenefits,IndividualPlan individualPlan, String[] props){
		Map<String, Map<String,String>> tierBenefits = new HashMap<String, Map<String, String>>();
		
		for(String prop : props){
			Map<String, String> benefitData = null;
			Map<String, String> benefitAttrib = planBenefits.get(prop);
			if (benefitAttrib != null) {
				for (String ameBenefitAttribute : ameBenefitAttributeList) {
					benefitData = new HashMap<String, String>();
					benefitData.put(DISPLAY_VAL, benefitAttrib.get(ameBenefitAttribute));
				}
				tierBenefits.put(prop, benefitData);
			}
		}
		
		return tierBenefits;
	}
	
	public static Map<String, Map<String,String>> parsePlanBenefitsTier2(Map<String, Map<String, String>> planBenefits, String[] props){
		Map<String, Map<String,String>> tierBenefits = new HashMap<String, Map<String, String>>();

		for(String prop : props){
			Map<String, String> benefitAttrib = planBenefits.get(prop);
			if(benefitAttrib != null){
				Map<String, String> benefitData = new HashMap<String, String>();
				
				benefitData.put(DISPLAY_VAL, benefitAttrib.get(NETWORK_T2_DISP));
				tierBenefits.put(prop, benefitData);	
			}
		}
		
		return tierBenefits;
	}
	
	public static Map<String, Map<String,String>> parsePlanBenefitsOutNet(Map<String, Map<String, String>> planBenefits, String[] props){
		Map<String, Map<String,String>> tierBenefits = new HashMap<String, Map<String, String>>();

		for(String prop : props){
			Map<String, String> benefitAttrib = planBenefits.get(prop);
			if(benefitAttrib != null){
				Map<String, String> benefitData = new HashMap<String, String>();
				
				benefitData.put(DISPLAY_VAL, benefitAttrib.get(OUT_NETORK_DISP));
				tierBenefits.put(prop, benefitData);
			}
		}
		
		return tierBenefits;
	}
	
	public static Map<String, Map<String,String>> parseAppliesToDeduct(Map<String, Map<String, String>> planBenefits, String[] props){
		Map<String, Map<String,String>> appliesToDeduct = initializeAppliesValues(props);

		for(String prop : props){
			Map<String, String> benefitAttrib = planBenefits.get(prop);
			if(benefitAttrib != null){
				Map<String, String> benefitData = new HashMap<String, String>(2);
				
				if(benefitAttrib.get(SUB_TO_NET_DEDUCT) != null){
					benefitData.put("AppliesNetwk",benefitAttrib.get(SUB_TO_NET_DEDUCT));
				}
				if(benefitAttrib.get(SUB_TO_NON_NET_DEDUCT) != null){
					benefitData.put("AppliesNonNetwk",benefitAttrib.get(SUB_TO_NON_NET_DEDUCT));
				}
				
				appliesToDeduct.put(prop, benefitData);
			}
		}
		
		return appliesToDeduct;
	}
	
	public static Map<String, Map<String,String>> parseNetwkExcp(PlanDisplayEnum.InsuranceType insuranceType, Map<String, Map<String, String>> planBenefits, String[] props){
		Map<String, Map<String,String>> netwkException =  initializeNetworkExcpValues(props);

		for(String prop : props){
			Map<String, String> benefitAttrib = planBenefits.get(prop);
			if(benefitAttrib != null){
				Map<String, String> benefitData = new HashMap<String, String>(1);
				
				if(StringUtils.isNotEmpty(benefitAttrib.get(LIMIT_EXCEP_DISP))){
					StringBuilder netwkExceptionDispStr = new StringBuilder(benefitAttrib.get(LIMIT_EXCEP_DISP));
					String networkException = replaceUnterminatString(netwkExceptionDispStr.toString());
					benefitData.put(DISPLAY_VAL, networkException);
					netwkException.put(prop, benefitData);
				}
			}
		}
		
		return netwkException;
	}
	
	public static Map<String, Map<String,String>> parseBenefitCovg(Map<String, Map<String, String>> planBenefits, String[] props){
		Map<String, Map<String,String>> benefitCoverage = initializeBenefitCoverageValues(props);

		for(String prop : props){
			Map<String, String> benefitAttrib = planBenefits.get(prop);
			if(benefitAttrib != null){
				Map<String, String> benefitData = new HashMap<String, String>(1);
				
				if(benefitAttrib.get(PlanDisplayConstants.IS_COVERED) != null){
					benefitData.put(PlanDisplayConstants.IS_COVERED, benefitAttrib.get(PlanDisplayConstants.IS_COVERED));
					benefitCoverage.put(prop, benefitData);
				}
			}
		}
		
		return benefitCoverage;
	}
	
	public static Map<String, Map<String,String>> parseBenefitExpl(Map<String, Map<String, String>> planBenefits, String[] props){
		Map<String, Map<String,String>> benefitExpl = new HashMap<String, Map<String,String>>(); 

		for(String prop : props){
			Map<String, String> benefitAttrib = planBenefits.get(prop);
			if(benefitAttrib != null){
				Map<String,String> benefitData = new HashMap<String,String>(1);
				if(benefitAttrib.get(EXPLANATION) != null) {
					String explanation = replaceUnterminatString(benefitAttrib.get(EXPLANATION));
					benefitData.put(DISPLAY_VAL, explanation);					
				}
				benefitExpl.put(prop, benefitData);
			}
		}
		
		return benefitExpl;
	}
	
	private static String replaceUnterminatString(String input){
		String output = input.replaceAll("\n", "<br/>");
		 output = HtmlUtils.htmlEscape(output);
		return  output;
	}
	
	private static String getDeductibleVal(Map<String, String> benefitAttrib,
			String dispVal) {
		String deductibleVal = null;
		if(dispVal != null && !dispVal.equals(StringUtils.EMPTY)){
			if(dispVal.toLowerCase().contains(AFTER_DEDUCTIBLE)){
				deductibleVal = "No";
			}
			else{
				if(dispVal.toLowerCase().contains(COPAY) || dispVal.toLowerCase().contains(NO_CHARGE)){
					deductibleVal = "Yes";
				}
				else{
					deductibleVal = "No";
				}
			}
		}
		return deductibleVal;
	}


	
    public static int getAgeFromCoverageDate(Date coverageStartDate, Date dateOfBirth) {
		Calendar today = TSCalendar.getInstance();
		today.setTime(coverageStartDate);
		Calendar dob = TSCalendar.getInstance();
		dob.setTime(dateOfBirth);
		Integer age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
		if(dob.get(Calendar.MONTH) > today.get(Calendar.MONTH) 
			|| (dob.get(Calendar.MONTH) == today.get(Calendar.MONTH) && dob.get(Calendar.DATE) > today.get(Calendar.DATE))){
			age--;
		}
		
		return age;
	}
	
	public List<PlanRateBenefit> getHouseholdPlanRateBenefits(List<Map<String,String>> memberList, List<PdPersonDTO> seekingCovgPersonList, Date effectiveDate, PdQuoteRequest pdQuoteRequest, PdHouseholdDTO pdHouseholdDTO , boolean minimizePlanData, List<PrescriptionSearchRequest> prescriptionList)
	{		
		InsuranceType insuranceType = pdQuoteRequest.getInsuranceType()!=null?pdQuoteRequest.getInsuranceType():InsuranceType.HEALTH;
		CostSharing costSharing = pdHouseholdDTO.getCostSharing();
		String planLevel = StringUtils.EMPTY;
		if(ShoppingType.EMPLOYEE.equals(pdHouseholdDTO.getShoppingType()) && InsuranceType.HEALTH.equals(insuranceType)){
			planLevel = pdQuoteRequest.getEmployerPlanTier() != null ? pdQuoteRequest.getEmployerPlanTier() : StringUtils.EMPTY;
		}
			
		ShoppingType shoppingType = pdHouseholdDTO.getShoppingType();
		ExchangeType exchangeType = pdQuoteRequest.getExchangeType();
		
		boolean showCatastrophicPlan = pdQuoteRequest.getShowCatastrophicPlan();
		List<String> planIdList = pdQuoteRequest.getPlanIdList();
		String tenant = TENANT_GINS;
		if(TenantContextHolder.getTenant() != null && StringUtils.isNotBlank(TenantContextHolder.getTenant().getCode())){
			tenant = TenantContextHolder.getTenant().getCode();
		}
		
		boolean checkEnrollmentAvailability = true;
		EnrollmentType enrollmentType = pdHouseholdDTO.getEnrollmentType();
		YorN keepOnly = pdHouseholdDTO.getKeepOnly();
		String initialPlanId = null;
		String initialCmsPlanId = null;
		String isSpecialEnrollment = "NO";
   		String currentHiosIssuerID = null;
   		YorN isPlanChange = YorN.N;

		if(EnrollmentType.S.equals(enrollmentType)) {
			isSpecialEnrollment = "YES";
			if(YorN.Y.equals(pdHouseholdDTO.getKeepOnly())) {
				for(PdPersonDTO pdPersonDTO : seekingCovgPersonList){
					String maintenanceReasonCode = pdPersonDTO.getMaintenanceReasonCode();
					if(maintenanceReasonCode != null && !StringUtils.EMPTY.equals(maintenanceReasonCode) && ("02".equals(maintenanceReasonCode) || "05".equals(maintenanceReasonCode))) {
						keepOnly = YorN.N;
					}
				}
				
			}
			
			boolean hasNewPerson = false;
			for(PdPersonDTO pdPersonDTO : seekingCovgPersonList){
				YorN newPersonFlag = pdPersonDTO.getNewPersonFlag(); 
				if (YorN.Y.equals(newPersonFlag)) {
					hasNewPerson = true;
					break;
				}
			}
			
			if (EnrollmentFlowType.KEEP.equals(pdQuoteRequest.getSpecialEnrollmentFlowType()) && !hasNewPerson) {
				checkEnrollmentAvailability = false;
			}
			
			if (EnrollmentFlowType.NEW.equals(pdQuoteRequest.getSpecialEnrollmentFlowType())
					&& null != pdQuoteRequest.getInitialCmsPlanId()
					&& HIOS_ISSUER_ID_LEN <= pdQuoteRequest.getInitialCmsPlanId().trim().length()) {
				currentHiosIssuerID = pdQuoteRequest.getInitialCmsPlanId().trim().substring(0, HIOS_ISSUER_ID_LEN);
				isPlanChange = YorN.Y;
			}
			
			Calendar cal = TSCalendar.getInstance();
		    cal.setTime(pdHouseholdDTO.getCoverageStartDate());
		    int coverageYear = cal.get(Calendar.YEAR);
		    boolean isInsideOEEnrollmentWindow = false;
			try {
				isInsideOEEnrollmentWindow = (ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL + "eligibility/indportal/isInsideOEEnrollment/"+coverageYear, 
						GhixConstants.USER_NAME_EXCHANGE, 
						HttpMethod.GET, 
						MediaType.APPLICATION_JSON, 
						Boolean.class,
						null)).getBody();
				
			} catch (Exception e) {
				String errorMsg = "Exception occurred while checking if coverageYear Is Inside OE Enrollment Window fro coverageYear - " + coverageYear;
				LOGGER.error(errorMsg);
				throw new GIRuntimeException(errorMsg);
			}			
		    
	   		if(pdQuoteRequest.getInitialPlanIdToEliminate() != null && !isInsideOEEnrollmentWindow){
	   			initialPlanId = pdQuoteRequest.getInitialPlanIdToEliminate().toString();
	   		}
		}
		
		if (YorN.N.equals(isPlanChange) && pdQuoteRequest.getInitialCmsPlanId() != null) {
			initialCmsPlanId = pdQuoteRequest.getInitialCmsPlanId();
		}
		
		String issuerId = pdQuoteRequest.getIssuerId();
		boolean issuerVerifiedFlag = true;
		if(issuerId != null && !("0".equals(issuerId))){
			issuerVerifiedFlag = false;
		}
		
		String marketType = Plan.PlanMarket.INDIVIDUAL.toString();
		if(ShoppingType.EMPLOYEE.equals(shoppingType)){
			marketType = Plan.PlanMarket.SHOP.toString();
		}
		
		Map<String, Object> requestParameters = new HashMap<String, Object>();
   		requestParameters.put("memberList", memberList);
   		requestParameters.put("insType", insuranceType.toString());
   		requestParameters.put("effectiveDate", DateUtil.dateToString(effectiveDate, YYYY_MM_DD));
   		String csr = StringUtils.EMPTY;
   		if(costSharing != null){
   			csr = costSharing.toString();
   		}
   		String planIdStr = StringUtils.EMPTY;
   		if(planIdList != null){
   			planIdStr = StringUtils.join(planIdList, ',');
   		}
   		
   		boolean isPHIXProfile = "PHIX".equalsIgnoreCase(exchangeTypeConfig);
   		List<String> providersList = new ArrayList<String>();  
   		List<ProviderBean> providerObjectList = new ArrayList<ProviderBean>();
   		PdPreferencesDTO pdPreferencesDTO = pdQuoteRequest.getPdPreferencesDTO();
   		if(pdPreferencesDTO != null) {
   			String providerJson = pdPreferencesDTO.getProviders();
			if(providerJson!=null){
				providerObjectList = platformGson.fromJson(providerJson,new TypeToken<List<ProviderBean>>() {}.getType());
				if(!isPHIXProfile && !"CA".equalsIgnoreCase(stateCode)){
					for(ProviderBean providerBean:providerObjectList) {
						providersList.add(providerBean.getId());
					}
				}
			}
   		}
   		
   		OnOffEnum affiliateIssuerHiosIdRestricted = OnOffEnum.ON;
   		if(pdQuoteRequest.getAffiliateIssuerHiosIdRestricted() !=null){
   			affiliateIssuerHiosIdRestricted = pdQuoteRequest.getAffiliateIssuerHiosIdRestricted();
   		}
   		
   		YesNoEnum showPufPlans = YesNoEnum.NO;
   		if(pdQuoteRequest.getShowPufPlans() !=null){
   			showPufPlans = pdQuoteRequest.getShowPufPlans();
   		}
   		
   		List<Map<String, List<String>>> providerMapList = new ArrayList<Map<String, List<String>>>();
   		
		if("CA".equalsIgnoreCase(stateCode) && providerObjectList != null && providerObjectList.size() > 0){
			for(ProviderBean providerBean:providerObjectList) {
				Map<String, List<String>> providerMap = new HashMap<String, List<String>>();
				providerMap.put(providerBean.getId(), providerBean.getNetworkIdList());
				providerMapList.add(providerMap);
			}
		}
   		
   		requestParameters.put("costSharing", csr);
   		requestParameters.put("planIdStr", planIdStr);
   		requestParameters.put("showCatastrophicPlan", showCatastrophicPlan);   		
   		requestParameters.put("planLevel", planLevel);
   		requestParameters.put("marketType", marketType);
   		requestParameters.put("exchangeType", exchangeType);
   		requestParameters.put("tenant", tenant);
   		requestParameters.put("isSpecialEnrollment", isSpecialEnrollment);
   		requestParameters.put("eliminatePlanIds", initialPlanId);
   		requestParameters.put("issuerId", issuerId);
   		requestParameters.put("issuerVerifiedFlag", issuerVerifiedFlag);
   		requestParameters.put("checkEnrollmentAvailability", checkEnrollmentAvailability);
   		requestParameters.put("keepOnly", keepOnly);
   		requestParameters.put("minimizePlanData",minimizePlanData);
   		requestParameters.put("ehbCovered", StringUtils.EMPTY);
   		requestParameters.put("providers", providerMapList);
		requestParameters.put("strenussIdList", providersList);
		requestParameters.put("groupId", 0);
   		requestParameters.put("hiosPlanNumber", initialCmsPlanId);
   		requestParameters.put("restrictHiosIds", affiliateIssuerHiosIdRestricted.name());
   		requestParameters.put("showPufPlans", showPufPlans.name());
   		requestParameters.put("currentHiosIssuerID", currentHiosIssuerID);
   		requestParameters.put("isPlanChange", isPlanChange);

   		PlanRateBenefitRequest planRateBenefitRequest = new PlanRateBenefitRequest();
   		if (InsuranceType.STM.equals(insuranceType)) {
   			try {
   				planRateBenefitRequest.setRequestParameters(requestParameters);
   				List<PlanRateBenefit> planRateBenefitList = new ArrayList<PlanRateBenefit>();
   				planRateBenefitList = stmPlanRateBenefitRestController.getStmPlanRateBenefitsHelper(planRateBenefitRequest);
   				return planRateBenefitList;
   			} catch (Exception e) {
   				putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
   			}
   		} else {
   			/*if (pdPreferencesDTO != null) {
				String prescriptionJson = pdPreferencesDTO.getPrescriptions();
				List<PrescriptionSearchRequest> prescriptionRequestList = new ArrayList<PrescriptionSearchRequest>();
				if (prescriptionJson != null) {
					prescriptionRequestList = platformGson.fromJson(prescriptionJson,
							new TypeToken<List<PrescriptionSearchRequest>>() {
							}.getType());
				}
				if (prescriptionRequestList != null && !prescriptionRequestList.isEmpty()) {
					List<PrescriptionSearchRequest> prescriptionList = new ArrayList<PrescriptionSearchRequest>();
					for(PrescriptionSearchRequest prescriptionSearchRequest:prescriptionRequestList){
						prescriptionList.add(prescriptionSearchRequest);
						if(prescriptionSearchRequest.getAssociatedDrugs()!=null && !prescriptionSearchRequest.getAssociatedDrugs().isEmpty()){
							for(PrescriptionSearchRequest associatedDrugsSearchRequest:prescriptionSearchRequest.getAssociatedDrugs()){
								prescriptionList.add(associatedDrugsSearchRequest);
							}
						}						
					}
					planRateBenefitRequest.setPrescriptionRequestList(prescriptionList);
				}
			}*/
   			if(prescriptionList != null && prescriptionList.size() > 0)
   			{
   				planRateBenefitRequest.setPrescriptionRequestList(prescriptionList);
   			}
			planRateBenefitRequest.setUrl(PlanMgmtEndPoints.GET_HOUSEHOLD_PLAN_RATE_URL);
	   		planRateBenefitRequest.setRequestMethod(PlanRateBenefitRequest.RequestMethod.POST);
	   		planRateBenefitRequest.setRequestParameters(requestParameters);
	   		
	   		try {
	   			// No PII, no need encryption
	   		    if(LOGGER.isInfoEnabled()) { 
	   			  LOGGER.info("<----QUOTING API CALL START---->");
	   			}
	   			
	            List<PlanRateBenefit> planRateBenefitList = planRateBenefitController.getHouseholdPlanRateBenefitsHelper(planRateBenefitRequest);
	            
	            if(LOGGER.isInfoEnabled()) { 
                  LOGGER.info("<----QUOTING API CALL END---->");
                }
	            
	            return planRateBenefitList;
	        } catch (Exception e) {
	            putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
	        }	
   		}
   		return null;
	}
	
	/**
	 * @deprecated This method is not in use.
	 * @param externalEmployerId
	 * @param coverageStartDate
	 * @return
	 * @throws GIException
	 */
	@Deprecated
	public Map<String, String> getEmployerEnrollment(String externalEmployerId, Date coverageStartDate) throws GIException {
		String coverageStart = DateUtil.dateToString(coverageStartDate, GhixConstants.REQUIRED_DATE_FORMAT);
		
		Map<String, String> responseData  = new HashMap<String, String>();
		String response = null;
		
		try {
			response = ghixRestTemplate.getForObject(ShopEndPoints.GET_EMPLOYER_ENROLLMENT+"?externalEmployerId="+externalEmployerId+"&coverageStartDate="+coverageStart, String.class);
		} catch (Exception e) {
			putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		ShopResponse  shopResponse = (ShopResponse) xstream.fromXML(response);
		
		if(shopResponse.getErrCode()==1){
			responseData.put("Error", shopResponse.getErrMsg());
			return responseData;
		}
		
		EmployerEnrollmentDTO employerEnrollmentDTO = (EmployerEnrollmentDTO) shopResponse.getResponseData().get("EmployerEnrollment");
		Set<EmployerEnrollmentItem> employerEnrollmentItems = employerEnrollmentDTO.getEmployerEnrollmentItems();
		Iterator<EmployerEnrollmentItem> itr = employerEnrollmentItems.iterator();
		StringBuilder planTier = new StringBuilder(StringUtils.EMPTY);
		//For now the enrollmentItem set may contain only one element but future can be more than one as discussed with enrollment team.
		while(itr.hasNext()){
			if(StringUtils.EMPTY.equals(planTier.toString())){
				planTier.append(itr.next().getPlanTier().toString());
			}else{
				planTier = planTier.append(",").append(itr.next().getPlanTier().toString());
			}
		}
		 
		responseData.put("Error", StringUtils.EMPTY);
		responseData.put("plan_tier", planTier.toString());
		responseData.put("dependent_contribution", String.valueOf(employerEnrollmentDTO.getDependentContribution()));
		responseData.put("employee_contribution", String.valueOf(employerEnrollmentDTO.getEmployeeContribution()));
	
		return responseData;
	}
		
	public IndividualPlan getPlanInfo(String planId, String coverageStartDate, boolean mininmizeData){
		PlanResponse planResponse = getPlanDetails(planId, coverageStartDate, mininmizeData);
		IndividualPlan individualPlan = null;
		if(planResponse != null) {
			individualPlan = buildPlanDetails(planResponse);
		}            
        return individualPlan;
	}
	
	public PlanResponse getPlanDetails(String planId, String coverageStartDate, boolean mininmizeData){
		PlanRequest planRequest = new PlanRequest();
        planRequest.setEffectiveDate(coverageStartDate);
        planRequest.setPlanId(planId);
        planRequest.setMinimizePlanData(mininmizeData);
        PlanResponse planResponse = null;
		try {
			planResponse = planRestController.getPlanDetailsHelper(planRequest);
		} catch (GIException e) {
		  if(LOGGER.isErrorEnabled())
		  {
			putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		  }
		}
        return planResponse;
	}

	public List<PlanRateBenefit> getIndividualPlanBenefitsAndCost(List<Integer> planIds, String insuranceType,String coverageStartDate) {
		Map<String, Object> requestParameters = new HashMap<String, Object>();
		List<PlanRateBenefit> responseData = null;
		requestParameters.put("planIds", planIds);
		requestParameters.put("insuranceType", insuranceType);
		requestParameters.put("coverageStartDate", coverageStartDate);

		PlanRateBenefitRequest planRateBenefitRequest = new PlanRateBenefitRequest();
		planRateBenefitRequest.setRequestParameters(requestParameters);
		try {
			responseData = planRateBenefitController.getBenefitsAndCostHelper(planRateBenefitRequest);
		} catch (Exception e) {
			putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Exception in getIndividualPlanBenefitsAndCost---->", e, false);
		}
		return responseData;
	}

	public Map<String, Object> getIndividualPlanBenefitsAndCostByPlanid(List<Integer> planIds, String insuranceType) {
		Map<String, Object> requestParameters = new HashMap<String, Object>();
		Map<String, Object> responseData = new HashMap<String, Object>();
		requestParameters.put("planIds", planIds);
		requestParameters.put("insuranceType", insuranceType);
		
		PlanRateBenefitRequest planRateBenefitRequest = new PlanRateBenefitRequest();
		planRateBenefitRequest.setRequestParameters(requestParameters);
		try	{
			responseData = planRateBenefitController.getBenefitsAndCostByPlanIdHelper(planRateBenefitRequest);
			if ("HEALTH".equals(insuranceType))	{
				responseData.put("healthBenefits", (List<PlanHealthBenefitDTO>) responseData.get("healthBenefits"));
				responseData.put("healthCosts",(List<PlanHealthCostDTO>) responseData.get("healthCosts"));
			} else if ( "DENTAL".equals(insuranceType) ) {
			   responseData.put("dentalBenefits",(List<PlanDentalBenefitDTO>) responseData.get("dentalBenefits"));
			   responseData.put("dentalCosts",(List<PlanDentalCostDTO>) responseData.get("dentalCosts")); 
		    }
		} catch (Exception e) {
		  if(LOGGER.isErrorEnabled())
		  {
			putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Exception in getIndividualPlanBenefitsAndCostByPlanid---->", e, false);
		  }
		}
		return responseData;
	}
		
	private String getEncodedPremium(int planId,Float premiumBeforeCredit,Float premiumAfterCredit,List<Map<String, String>> planDetailsByMember) {
		String encodedPremium = null;
		ArrayList<String> encodedPremiumList = new ArrayList<String>();
		encodedPremiumList.add(Integer.toString(planId));
		encodedPremiumList.add(Float.toString(premiumBeforeCredit));
		encodedPremiumList.add(Float.toString(premiumAfterCredit));

		if(planDetailsByMember!=null && !planDetailsByMember.isEmpty()){			
			for(Map<String, String> planDetailsByMemberMap : planDetailsByMember){
				encodedPremiumList.add(planDetailsByMemberMap.get("premium"));
			}
		}
		try {
			encodedPremium = GhixUtils.getGhixSecureCheckSum(encodedPremiumList);
		} catch (NoSuchAlgorithmException e) {
		  if(LOGGER.isErrorEnabled())
		  {
			putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occur while converting encoded premium---->", e, false);
		  }
			return encodedPremium;
		}
		return encodedPremium;
	}
	
	public PdOrderItem calculateSubsidy(PdOrderItem pdOrderItem, String coverageStartDate, BigDecimal subsidy, int memberCount, IndividualPlan individualPlan)	{
		BigDecimal premiumBeforeCredit = new BigDecimal(Float.toString(pdOrderItem.getPremiumBeforeCredit()));
		String minPremiumPerMember = DynamicPropertiesUtil.getPropertyValue(PlanDisplayConfiguration.PlanDisplayConfigurationEnum.MIN_PREMIUM_PER_MEMBER);
		
		BigDecimal ehbPercentage;
		BigDecimal applicablePremium = premiumBeforeCredit;
		BigDecimal minPremiumPerHousehold = BigDecimal.ZERO;
		Date covStartDate = DateUtil.StringToDate(coverageStartDate, "yyyy-MM-dd");
		boolean isDental2017 = isDental2017(pdOrderItem.getInsuranceType(),covStartDate);
		boolean isCAProfile = stateCode != null ? stateCode.equalsIgnoreCase("CA") : false;
		if(InsuranceType.HEALTH.equals(pdOrderItem.getInsuranceType()) || (isDental2017 && !isCAProfile) ){
			if(!"CATASTROPHIC".equalsIgnoreCase(individualPlan.getLevel()) || (isDental2017 && !isCAProfile))
			{
				BigDecimal defaultEHB = BigDecimal.valueOf(1.0);
            	if(isDental2017)
            	{
            		defaultEHB = BigDecimal.valueOf(0.0);
            	}
            	
	        	ehbPercentage = individualPlan.getEhbPrecentage() == null ? defaultEHB : new BigDecimal(Float.toString(individualPlan.getEhbPrecentage()));
	        	applicablePremium = premiumBeforeCredit.multiply(ehbPercentage);
	        	minPremiumPerHousehold = BigDecimal.valueOf(memberCount).multiply(new BigDecimal(minPremiumPerMember));
	        	if(InsuranceType.DENTAL.equals(pdOrderItem.getInsuranceType())) {
					minPremiumPerHousehold = BigDecimal.ZERO;
				}
			}
			else
			{
				applicablePremium = premiumBeforeCredit;
				subsidy = BigDecimal.ZERO;
			}
        } else if(!isCAProfile) {        	
        	ehbPercentage = individualPlan.getEhbPrecentage() == null ? BigDecimal.valueOf(0.0) : BigDecimal.valueOf(memberCount).multiply(new BigDecimal(Float.toString(individualPlan.getEhbPrecentage())));
        	applicablePremium = ehbPercentage;
        }
		
		BigDecimal premiumAfterCredit = new BigDecimal(Float.toString(pdOrderItem.getPremiumBeforeCredit()));
		BigDecimal aptc = null;
		if(subsidy != null){
			aptc = subsidy;
			if(aptc.compareTo(applicablePremium) > 0) {
				aptc = applicablePremium; 
			}
			
			BigDecimal preAftCre = premiumBeforeCredit.subtract(aptc).setScale(2,BigDecimal.ROUND_HALF_UP);
			if (minPremiumPerHousehold.compareTo(preAftCre) > 0) {
				aptc = premiumBeforeCredit.subtract(minPremiumPerHousehold).setScale(2,BigDecimal.ROUND_HALF_UP);
				premiumAfterCredit = minPremiumPerHousehold;
			} else {
				premiumAfterCredit = preAftCre;
			}
			pdOrderItem.setMonthlySubsidy(aptc.floatValue());
		}else{
			pdOrderItem.setMonthlySubsidy(null);
		}
				
		pdOrderItem.setPremiumAfterCredit(premiumAfterCredit.floatValue());
		
		return pdOrderItem;
	}
	
	public static int getNoOfDoctorsInPlan(
			List<HashMap<String, Object>> providersInPlan) {
		int noOfDoctorsInPlanCount = 0;
		if (providersInPlan != null && providersInPlan.size() > 0) {
			for (Map<String, Object> map : providersInPlan) {

				if (map.get("networkStatus") instanceof String) {
					String networkStatus = (String) map.get("networkStatus");

					if ("inNetWork".equals(networkStatus)) {
						noOfDoctorsInPlanCount++;
					}
				}
			}
		}
		return noOfDoctorsInPlanCount;
	}
	
	public static int getNoOfDrugsInPlan(List<DrugDTO> drugList) {
		int noOfDrugsInPlanCount = 0;
		if (drugList != null && drugList.size() > 0) {
			for (DrugDTO drugDTO : drugList) {
				if ("Y".equalsIgnoreCase(drugDTO.getIsDrugCovered()) || "Y".equalsIgnoreCase(drugDTO.getIsGenericCovered())) {
					noOfDrugsInPlanCount++;
				}
			}
		}
		return noOfDrugsInPlanCount;
	}
	
	public List<IndividualPlan> getAMEPlans(List<Map<String, String>> memberList, Date effectiveDate) {
		AmePlanRequestDTO amePlanRequest = new AmePlanRequestDTO();
		List<IndividualPlan> individualPlans = null;

		String tenant = TENANT_GINS;
		if(TenantContextHolder.getTenant() != null && StringUtils.isNotBlank(TenantContextHolder.getTenant().getCode())){
			 tenant = TenantContextHolder.getTenant().getCode();
		}
		amePlanRequest.setMemberList(memberList);
		amePlanRequest.setEffectiveDate(DateUtil.dateToString(effectiveDate, YYYY_MM_DD));
		amePlanRequest.setTenantCode(tenant);

		String ameplanResponseStr = ghixRestTemplate.postForObject(PlanMgmtEndPoints.GET_AME_PLANS, amePlanRequest, String.class);
		
		if(LOGGER.isDebugEnabled())
		{
		putLoggerStmt(LOGGER, LogLevel.DEBUG, "<----GET_AME_PLANS response str---->"+ameplanResponseStr, null, true);
		}
		
		if (!StringUtils.isEmpty(ameplanResponseStr)) {
			AmePlanResponseDTO ameplanResponse = (AmePlanResponseDTO) platformGson.fromJson(ameplanResponseStr, AmePlanResponseDTO.class);
			if (!GhixConstants.RESPONSE_FAILURE.equalsIgnoreCase(ameplanResponse.getStatus())) {
				individualPlans = buildAMEPlans(ameplanResponse);
			}

		}

		return individualPlans;
	}
	
	public List<IndividualPlan> getVisionPlans(List<Map<String, String>> memberList, Date effectiveDate) {
		VisionPlanRequestDTO visionPlanRequest = new VisionPlanRequestDTO();
		List<IndividualPlan> individualPlans = null;

		String tenant = TENANT_GINS;
		if(TenantContextHolder.getTenant() != null && StringUtils.isNotBlank(TenantContextHolder.getTenant().getCode())){
			 tenant = TenantContextHolder.getTenant().getCode();
		}
		
		visionPlanRequest.setMemberList(memberList);
		visionPlanRequest.setEffectiveDate(DateUtil.dateToString(effectiveDate, YYYY_MM_DD));
		visionPlanRequest.setTenant(tenant);
		
		String visionPlanResponseStr = ghixRestTemplate.postForObject(PlanMgmtEndPoints.GET_VISION_PLAN, visionPlanRequest, String.class);
		
		if(LOGGER.isDebugEnabled())
		{
		putLoggerStmt(LOGGER, LogLevel.DEBUG, "<----GET_VISION_PLAN response str---->"+visionPlanResponseStr, null, true);
		}
		
		if (!StringUtils.isEmpty(visionPlanResponseStr)) {
			VisionPlanResponseDTO visionPlanResponse = (VisionPlanResponseDTO) platformGson
					.fromJson(visionPlanResponseStr, VisionPlanResponseDTO.class);
			if (!GhixConstants.RESPONSE_FAILURE
					.equalsIgnoreCase(visionPlanResponse.getStatus())) {
				individualPlans = buildVisionPlans(visionPlanResponse);
			}
		}

		return individualPlans;
	}
	
	public List<IndividualPlan> getLifePlans(List<Map<String, String>> memberList, Date effectiveDate) {
		List<IndividualPlan> individualPlans = null;		
		Member memberInfo = new Member();
		String countyCode = null;
		String zip = null;
		if(memberList != null && !memberList.isEmpty()){
			Map<String, String> member = memberList.get(0);
			memberInfo.setAge(Integer.parseInt(member.get(AGE)));
			String gender = StringUtils.isNotEmpty(member.get(GENDER)) && member.get(GENDER).equalsIgnoreCase(MALE)?Gender.M.toString():Gender.F.toString();
			memberInfo.setGender(gender);
			memberInfo.setTobacco(member.get(TOBACCO));
			countyCode = member.get(COUNTY_CODE);
			zip = member.get(ZIP);
		}
		
		String tenant = GINS;
		if(TenantContextHolder.getTenant() != null && StringUtils.isNotBlank(TenantContextHolder.getTenant().getCode())){
			 tenant = TenantContextHolder.getTenant().getCode();
		}
		
		LifePlanRequestDTO lifePlanRequestDTO = new LifePlanRequestDTO();		
		ZipCode zipCode =  zipCodeService.getZipcodeByZipAndCountyCode(zip, countyCode);
		if(zipCode != null){
			lifePlanRequestDTO.setStateCode(zipCode.getState());
		}						
		lifePlanRequestDTO.setMemberInfo(memberInfo);
		lifePlanRequestDTO.setEffectiveDate(DateUtil.dateToString(effectiveDate, YYYY_MM_DD));
		lifePlanRequestDTO.setTenantCode(tenant);
		
		String lifePlanResponseStr = ghixRestTemplate.postForObject(PlanMgmtEndPoints.GET_LIFE_PLANS, lifePlanRequestDTO, String.class);
		putLoggerStmt(LOGGER, LogLevel.DEBUG, "<----GET_LIFE_PLANS response str---->"+lifePlanResponseStr, null, true);
		if (!StringUtils.isEmpty(lifePlanResponseStr)) {
			LifePlanResponseDTO lifePlanResponseDTO = (LifePlanResponseDTO) platformGson.fromJson(lifePlanResponseStr, LifePlanResponseDTO.class);
			if (!GhixConstants.RESPONSE_FAILURE.equalsIgnoreCase(lifePlanResponseDTO.getStatus())) {
				individualPlans = buildLifePlans(lifePlanResponseDTO);
			}
		}

		return individualPlans;
	}
	
	public List<IndividualPlan> buildLifePlans(LifePlanResponseDTO lifePlanResponseDTO) {
		List<IndividualPlan> individualPlans = new ArrayList<IndividualPlan>();
		int planCount = 0;

		for (LifePlanDTO lifePlanDTO : lifePlanResponseDTO.getLifePlanList()) {
			IndividualPlan individualPlan = new IndividualPlan();
			individualPlan.setId(++planCount);
			individualPlan.setPlanId(lifePlanDTO.getPlanId());
			individualPlan.setName(lifePlanDTO.getName());
			individualPlan.setIssuerLogo(lifePlanDTO.getIssuerLogo());
			individualPlan.setIssuer(lifePlanDTO.getIssuerName());
			individualPlan.setIssuerId(lifePlanDTO.getIssuerId());
			individualPlan.setIssuerText(lifePlanDTO.getIssuerName());
			individualPlan.setPolicyTerm(lifePlanDTO.getPolicyTerm());
			individualPlan.setBenefitUrl(lifePlanDTO.getBenefitUrl());
			individualPlan.setCoverageAmount(lifePlanDTO.getCoverageAmount());	
			individualPlan.setPremium(lifePlanDTO.getPremium());
			
			individualPlan.setPremiumAfterCredit(lifePlanDTO.getPremium());
			individualPlan.setPremiumBeforeCredit(lifePlanDTO.getPremium());
			individualPlan.setPlanType(Plan.PlanInsuranceType.LIFE.toString());			
			String encodedPremium = getEncodedPremium(lifePlanDTO.getPlanId(), lifePlanDTO.getPremium(), lifePlanDTO.getPremium(), null);
			individualPlan.setEncodedPremium(encodedPremium);
			individualPlan.setPlanTab(PlanTab.FULL_PRICE);
			individualPlans.add(individualPlan);
		}

		return individualPlans;
	}
	
	public List<IndividualPlan> buildAMEPlans(AmePlanResponseDTO amePlanResponseDTO) {
		List<IndividualPlan> individualPlans = new ArrayList<IndividualPlan>();
		int planCount = 0;

		for (AmePlan amePlan : amePlanResponseDTO.getAmePlanList()) {
			IndividualPlan individualPlan = new IndividualPlan();
			individualPlan.setId(++planCount);
			individualPlan.setPlanId(amePlan.getPlanId());
			individualPlan.setName(amePlan.getName());

			individualPlan.setIssuerLogo(amePlan.getIssuerLogo());
			individualPlan.setIssuer(amePlan.getIssuerName());
			individualPlan.setIssuerId(amePlan.getIssuerId());
			individualPlan.setIssuerText(amePlan.getIssuerName());

			individualPlan.setPlanBrochureUrl(amePlan.getBrochure());

			individualPlan.setPlanType(Plan.PlanInsuranceType.AME.toString());
			individualPlan.setNetworkType(amePlan.getNetworkType());
			individualPlan.setPremiumAfterCredit(amePlan.getPremium());
			individualPlan.setPremiumBeforeCredit(amePlan.getPremium());
			individualPlan.setPremium(amePlan.getPremium());
			String encodedPremium = getEncodedPremium(amePlan.getPlanId(), amePlan.getPremium(), amePlan.getPremium(), null);
			individualPlan.setEncodedPremium(encodedPremium);
			//individualPlan.setEncryptedPremiumAfterCredit(ghixJasyptEncrytorUtil.encryptStringByJasypt(Float.toString(amePlan.getPremium())));	
			//individualPlan.setEncryptedPremiumBeforeCredit(ghixJasyptEncrytorUtil.encryptStringByJasypt(Float.toString(amePlan.getPremium())));	
			try{
				individualPlan.setDeductible(!StringUtils.isEmpty(amePlan.getDeductibleVal()) ? Integer.valueOf(amePlan.getDeductibleVal()) : 0);
			}catch (NumberFormatException e){
				individualPlan.setDeductible(0);
			}

			try{
				individualPlan.setMaxBenefits(!StringUtils.isEmpty(amePlan.getMaxBenefitVal()) ? Integer.valueOf(amePlan.getMaxBenefitVal()) : 0);
			}catch (NumberFormatException e){
				individualPlan.setMaxBenefits(0);
			}
			
			individualPlan.setPlanTier1(amePlan.getPlanBenefits());

			Map<String, Map<String, String>> planCosts = new HashMap<String, Map<String, String>>();

			Map<String, String> planCost = new HashMap<String, String>();

			planCost.put("deductibleVal", amePlan.getDeductibleVal());
			planCost.put("deductibleAttrib", amePlan.getDeductibleAttrib());

			planCosts.put(PlanDisplayConstants.AME_DEDUCTIBLE, planCost);

			planCost = new HashMap<String, String>();
			planCost.put("maxBenefitVal", amePlan.getMaxBenefitVal());
			planCost.put("maxBenefitAttrib", amePlan.getMaxBenefitAttrib());
			planCosts.put("MAX_BENEFIT", planCost);

			planCost = new HashMap<String, String>();
			planCost.put("limitationsAndExclusions",
					amePlan.getLmitationAndExclusions());
			planCosts.put("LIMITATIONS", planCost);

			individualPlan.setPlanCosts(planCosts);
			individualPlan.setPlanTab(PlanTab.FULL_PRICE);

			individualPlans.add(individualPlan);
		}

		return individualPlans;
	}
	
	public List<IndividualPlan> buildVisionPlans(VisionPlanResponseDTO visionPlanResponseDTO) {
		List<IndividualPlan> individualPlans = new ArrayList<IndividualPlan>();
		int planCount = 0;

		for (VisionPlan visionPlan : visionPlanResponseDTO.getVisionPlanList()) {
			IndividualPlan individualPlan = new IndividualPlan();
			individualPlan.setId(++planCount);
			individualPlan.setPlanId(visionPlan.getId());
			individualPlan.setName(visionPlan.getName());

			individualPlan.setIssuerLogo(visionPlan.getIssuerLogo());
			individualPlan.setIssuer(visionPlan.getIssuerName());
			individualPlan.setIssuerId(visionPlan.getIssuerId());
			individualPlan.setIssuerText(visionPlan.getIssuerName());

			individualPlan.setPlanBrochureUrl(visionPlan.getBrochureUrl());
			individualPlan.setProviderLink(visionPlan.getProviderNetworkUrl());
			individualPlan.setPolicyLength(visionPlan.getPremiumPayment());
			individualPlan.setOutOfNetwkCoverage(visionPlan.getOutOfNetwkCoverage());

			individualPlan.setPlanType(Plan.PlanInsuranceType.VISION.toString());
			individualPlan.setPremiumAfterCredit(visionPlan.getPremium());
			individualPlan.setPremiumBeforeCredit(visionPlan.getPremium());
			individualPlan.setPremium(visionPlan.getPremium());
			String encodedPremium = getEncodedPremium(visionPlan.getId(), visionPlan.getPremium(), visionPlan.getPremium(), null);
			individualPlan.setEncodedPremium(encodedPremium);
			
			individualPlan.setPlanTier1(visionPlan.getBenefits());
			
			try{
				individualPlan.setVisionEyeExam(!StringUtils.isEmpty(visionPlan.getBenefits().get("EYE_EXAM").get("copayVal")) ? Float.valueOf(visionPlan.getBenefits().get("EYE_EXAM").get("copayVal")) : 0f);
			}catch (NumberFormatException e){
				individualPlan.setVisionEyeExam(0f);
			}
			try{
				individualPlan.setVisionGlasses(!StringUtils.isEmpty(visionPlan.getBenefits().get("GLASSES").get("copayVal")) ? Float.valueOf(visionPlan.getBenefits().get("GLASSES").get("copayVal")) : 0f);
			}catch (NumberFormatException e){
				individualPlan.setVisionGlasses(0f);
			}
			try{
				individualPlan.setVisionContacts(!StringUtils.isEmpty(visionPlan.getBenefits().get("CONTACT_LENS").get("copayVal")) ? Float.valueOf(visionPlan.getBenefits().get("CONTACT_LENS").get("copayVal")) : 0f);
			}catch (NumberFormatException e){
				individualPlan.setVisionContacts(0f);
			}
			
			Map<String, Map<String, String>> planCosts = new HashMap<String, Map<String, String>>();
			individualPlan.setPlanCosts(planCosts);
			individualPlan.setPlanTab(PlanTab.FULL_PRICE);

			individualPlans.add(individualPlan);
		}

		return individualPlans;
	}
	
	public CostSharing getCostSharingFromFFM(InsuranceApplicationType insuranceApplication) {
		CostSharing costSharing = null;
		com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType eligibleInsuranceApplicant = null;
		
		List<JAXBElement<? extends InsuranceApplicantType>> insuranceApplicantList = insuranceApplication.getInsuranceApplicant();
		for(JAXBElement<? extends InsuranceApplicantType> insuranceApplicantElement:insuranceApplicantList){
			com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType insuranceApplicant = (com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType) insuranceApplicantElement.getValue();
			boolean isEligible = true;
			InitialEnrollmentPeriodEligibilityType initialEnrollmentPeriodEligibility = insuranceApplicant.getInitialEnrollmentPeriodEligibility();
			
			if(!initialEnrollmentPeriodEligibility.getEligibilityIndicator().isValue()){
				SpecialEnrollmentPeriodEligibilityType specialEnrollmentPeriodEligibility = insuranceApplicant.getSpecialEnrollmentPeriodEligibility();
				if(!specialEnrollmentPeriodEligibility.getEligibilityIndicator().isValue()){
					isEligible = false;
				}	
			}
			
			if(isEligible){
				eligibleInsuranceApplicant = insuranceApplicant;
			}
		}
		
		if(eligibleInsuranceApplicant != null){
			ProgramEligibilitySnapshotType activeProgramEligibilitySnapshot = this.getActiveEligibility(eligibleInsuranceApplicant);
			if (activeProgramEligibilitySnapshot != null) {
				CSREligibilityType cSREligibility = activeProgramEligibilitySnapshot.getCSREligibility();
					if (null != cSREligibility && null != cSREligibility.getInsurancePlanVariantCategoryCode()) {
						List<JAXBElement<?>> insurancePlanVariantCategoryCodeList = cSREligibility.getInsurancePlanVariantCategoryCode();
						for (JAXBElement<?> insurancePlanVariantCategoryCode : insurancePlanVariantCategoryCodeList) {
							InsurancePlanVariantCategoryNumericCodeType insurancePlanVariantCategoryNumericCodeType = (InsurancePlanVariantCategoryNumericCodeType) insurancePlanVariantCategoryCode.getValue();
							String csr = insurancePlanVariantCategoryNumericCodeType.getValue();
							if (csr != null && !(StringUtils.EMPTY.equals(csr)) && FFM_CSR_MAPPING.containsKey(csr)) {
								costSharing = FFM_CSR_MAPPING.get(csr);
								break;
							}
						}
					}
			}
		}

		return costSharing;
	}
	
	public ProgramEligibilitySnapshotType getActiveEligibility(com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType insuranceApplicant) {

		ProgramEligibilitySnapshotType activeProgramEligibilitySnapshot = null;
		XMLGregorianCalendar ffmCoverageDate = null;
		
		boolean isOep = true;
		InitialEnrollmentPeriodEligibilityType initialEnrollmentPeriodEligibility = insuranceApplicant.getInitialEnrollmentPeriodEligibility();
		SpecialEnrollmentPeriodEligibilityType specialEnrollmentPeriodEligibility = insuranceApplicant.getSpecialEnrollmentPeriodEligibility();
		
		if(specialEnrollmentPeriodEligibility != null && specialEnrollmentPeriodEligibility.getEligibilityIndicator().isValue()){
			isOep = false;
		}
		if(initialEnrollmentPeriodEligibility != null && initialEnrollmentPeriodEligibility.getEligibilityIndicator().isValue()){
			isOep = true;
		}
		if(isOep){
			ffmCoverageDate = getFfmCoverageDate(initialEnrollmentPeriodEligibility);
		}else{
			ffmCoverageDate = getFfmCoverageDate(specialEnrollmentPeriodEligibility);
		}
		
		List<ProgramEligibilitySnapshotType> programEligibilitySnapshotList = insuranceApplicant.getProgramEligibilitySnapshot();
		if (programEligibilitySnapshotList != null && !programEligibilitySnapshotList.isEmpty()) {
			for (ProgramEligibilitySnapshotType programEligibilitySnapshot : programEligibilitySnapshotList) {
				if(specialEnrollmentPeriodEligibility != null && specialEnrollmentPeriodEligibility.getEligibilityIndicator() != null && !specialEnrollmentPeriodEligibility.getEligibilityIndicator().isValue() && initialEnrollmentPeriodEligibility != null && !initialEnrollmentPeriodEligibility.getEligibilityIndicator().isValue()){
					// Both initial and special eligibility indicator is false
					activeProgramEligibilitySnapshot = programEligibilitySnapshot;
				}else{
					if(ffmCoverageDate == null){
						activeProgramEligibilitySnapshot = programEligibilitySnapshot;
					}
					if(ffmCoverageDate != null && (DatatypeConstants.GREATER == ffmCoverageDate.compare(programEligibilitySnapshot.getActivityDate().get(0).getDate().getValue())	
							|| DatatypeConstants.EQUAL == ffmCoverageDate.compare(programEligibilitySnapshot.getActivityDate().get(0).getDate().getValue()))) {
						activeProgramEligibilitySnapshot = programEligibilitySnapshot;
					}
				}
			}
		}
		return activeProgramEligibilitySnapshot;
	}
	
	private XMLGregorianCalendar getFfmCoverageDate(InitialEnrollmentPeriodEligibilityType initialEnrollmentPeriodEligibility) {
		XMLGregorianCalendar ffmCoverageDate = null;
		if(initialEnrollmentPeriodEligibility.getEligibilityIndicator().isValue()){
			if (initialEnrollmentPeriodEligibility.getEarliestQHPEffectiveDate() != null && initialEnrollmentPeriodEligibility.getEarliestQHPEffectiveDate().getDate() != null) {
				ffmCoverageDate = initialEnrollmentPeriodEligibility.getEarliestQHPEffectiveDate().getDate().getValue();
			} else {
				ffmCoverageDate = initialEnrollmentPeriodEligibility.getEligibilityDateRange().getStartDate().getDate().getValue();
			}
		}

		return ffmCoverageDate;
	}
	
	private XMLGregorianCalendar getFfmCoverageDate(SpecialEnrollmentPeriodEligibilityType specialEnrollmentPeriodEligibility) {
		XMLGregorianCalendar ffmCoverageDate = null;
		if(specialEnrollmentPeriodEligibility.getEarliestQHPEffectiveDate()!=null && specialEnrollmentPeriodEligibility.getEarliestQHPEffectiveDate().getDate() != null){
			ffmCoverageDate = specialEnrollmentPeriodEligibility.getEarliestQHPEffectiveDate().getDate().getValue();
		}else{
			ffmCoverageDate = specialEnrollmentPeriodEligibility.getEligibilityDateRange().getStartDate().getDate().getValue();
		}
		
		return ffmCoverageDate;
	}
	
	public Date getCoverageStartDateFromFFM(InsuranceApplicationType insuranceApplication) {
		XMLGregorianCalendar ffmCoverageDate = null;
		com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType eligibleInsuranceApplicant = null;
		List<JAXBElement<? extends InsuranceApplicantType>> insuranceApplicantList = insuranceApplication.getInsuranceApplicant();
		for(JAXBElement<? extends InsuranceApplicantType> insuranceApplicantElement:insuranceApplicantList){
			com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType insuranceApplicant = (com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType) insuranceApplicantElement.getValue();
			boolean isEligible = true;
			InitialEnrollmentPeriodEligibilityType initialEnrollmentPeriodEligibility = insuranceApplicant.getInitialEnrollmentPeriodEligibility();
			
			if(!initialEnrollmentPeriodEligibility.getEligibilityIndicator().isValue()){
				SpecialEnrollmentPeriodEligibilityType specialEnrollmentPeriodEligibility = insuranceApplicant.getSpecialEnrollmentPeriodEligibility();
				if(!specialEnrollmentPeriodEligibility.getEligibilityIndicator().isValue()){
					isEligible = false;
				}	
			}
			
			if(isEligible){
				eligibleInsuranceApplicant = insuranceApplicant;
			}
		}
		if(eligibleInsuranceApplicant != null){
			boolean isOep = true;
			InitialEnrollmentPeriodEligibilityType initialEnrollmentPeriodEligibility = eligibleInsuranceApplicant.getInitialEnrollmentPeriodEligibility();
			SpecialEnrollmentPeriodEligibilityType specialEnrollmentPeriodEligibility = eligibleInsuranceApplicant.getSpecialEnrollmentPeriodEligibility();
			
			if(specialEnrollmentPeriodEligibility != null && specialEnrollmentPeriodEligibility.getEligibilityIndicator().isValue()){
				isOep = false;
			}
			if(initialEnrollmentPeriodEligibility != null && initialEnrollmentPeriodEligibility.getEligibilityIndicator().isValue()){
				isOep = true;
			}
			
			if(isOep){
				ffmCoverageDate = getFfmCoverageDate(initialEnrollmentPeriodEligibility);
			}else{
				ffmCoverageDate = getFfmCoverageDate(specialEnrollmentPeriodEligibility);
			}
			String coverageDate = ffmCoverageDate.toString();
			return DateUtil.StringToDate(coverageDate, YYYY_MM_DD);
		}else{
			return null;
		}
	}
	
	public Float calculateMonthlyAptcFromFFM(InsuranceApplicationType insuranceApplication) {
		List<JAXBElement<? extends InsuranceApplicantType>> insuranceApplicantList = insuranceApplication.getInsuranceApplicant();
		BigDecimal aptc = BigDecimal.ZERO;
		for (JAXBElement<? extends InsuranceApplicantType> insuranceApplicantElement : insuranceApplicantList) {
			if (insuranceApplicantElement.getValue() != null) {
				com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType insuranceApplicant = (com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType) insuranceApplicantElement.getValue();
				boolean isEligible = true;
				InitialEnrollmentPeriodEligibilityType initialEnrollmentPeriodEligibility = insuranceApplicant.getInitialEnrollmentPeriodEligibility();
				
				if(!initialEnrollmentPeriodEligibility.getEligibilityIndicator().isValue()){
					SpecialEnrollmentPeriodEligibilityType specialEnrollmentPeriodEligibility = insuranceApplicant.getSpecialEnrollmentPeriodEligibility();
					if(!specialEnrollmentPeriodEligibility.getEligibilityIndicator().isValue()){
						isEligible = false;
					}	
				}
				
				if(isEligible){
					ProgramEligibilitySnapshotType activeProgramEligibilitySnapshot = this.getActiveEligibility(insuranceApplicant);
					BigDecimal monthlyAptc = BigDecimal.ZERO;
					if (activeProgramEligibilitySnapshot != null) {
						String eligibilityVal = checkEligibility(activeProgramEligibilitySnapshot);
						if ("true".equals(eligibilityVal)) {
							APTCEligibilityType aPTCEligibility = activeProgramEligibilitySnapshot.getAPTCEligibility();
								if (null != aPTCEligibility && null != aPTCEligibility.getMonthlyAPTCAmount()) {
									monthlyAptc = aPTCEligibility.getMonthlyAPTCAmount().getValue();
									//monthlyAptc = monthlyAptcAmount;
								}
						}
					}
					aptc = aptc.add(monthlyAptc);
				}
			}
		}
		return aptc.floatValue();
	}
	
	public String checkEligibility(ProgramEligibilitySnapshotType programEligibilitySnapshot) {
		boolean exchangeEligibility = false;
		if (programEligibilitySnapshot.getExchangeEligibility() != null) {
			exchangeEligibility = programEligibilitySnapshot.getExchangeEligibility().getEligibilityIndicator().isValue();
		}

		boolean chipEligibility = false;
		if (programEligibilitySnapshot.getCHIPEligibility() != null) {
			chipEligibility = programEligibilitySnapshot.getCHIPEligibility().getEligibilityIndicator().isValue();
		}

		boolean aptcEligibility = false;
		if(programEligibilitySnapshot.getAPTCEligibility() != null)	{
			aptcEligibility = programEligibilitySnapshot.getAPTCEligibility().getEligibilityIndicator().isValue();
		}
		
		boolean medicaidEligibility = false;
		if(!aptcEligibility && programEligibilitySnapshot.getMedicaidEligibility() != null 
				&& programEligibilitySnapshot.getMedicaidEligibility().getEligibilityDateRange() != null
				&& programEligibilitySnapshot.getMedicaidEligibility().getEligibilityDateRange().getStartDate() != null
				&& programEligibilitySnapshot.getMedicaidEligibility().getEligibilityDateRange().getStartDate().getDate() != null 
				&& programEligibilitySnapshot.getMedicaidEligibility().getEligibilityDateRange().getStartDate().getDate().getValue() != null
				&& programEligibilitySnapshot.getMedicaidEligibility().getEligibilityDateRange().getStartDate().getDate().getValue().getYear() == 2015)	{
			medicaidEligibility = programEligibilitySnapshot.getMedicaidEligibility().getEligibilityIndicator().isValue();
		}

		String value = "Commercial Plan Eligible";
		if (exchangeEligibility) {
			if (!(chipEligibility || medicaidEligibility)) {
				value = "true";
			} else if (medicaidEligibility) {
				value = "Medicaid eligible";
			} else if (chipEligibility) {
				value = "CHIP eligible";
			}
		} else {
			if (medicaidEligibility) {
				value = "Medicaid eligible";
			} else if (chipEligibility) {
				value = "CHIP eligible";
			}
		}
		return value;
	}
	
	public Map<String, String> getRelationship(SSFPrimaryContactType sSFPrimaryContact) {
		Map<String, String> relationMap = new HashMap<String, String>();
		PersonType person = (PersonType) sSFPrimaryContact.getRoleOfPersonReference().getRef();
		List<PersonAssociationType> personAssociationList = person.getPersonAugmentation().getPersonAssociation();
		for (PersonAssociationType personAssociation : personAssociationList) {
			ReferenceType refPersonObj = personAssociation.getPersonReference().get(0);
			if (null != personAssociation.getFamilyRelationshipCode()) {
				String relationshipCode = personAssociation.getFamilyRelationshipCode().getValue();
				String memberId = ((com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.PersonType) refPersonObj.getRef()).getId();
				relationMap.put(memberId, relationshipCode);
			}
		}
		return relationMap;
	}
	
	public Map<String, Object> getPersonDataFromFFM(com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType insuranceApplicant,	ApplicantEligibilityResponseType applicantEligibilityResponse) {
		Map<String, Object> personMap = new HashMap<String, Object>();
		personMap.put("insuranceApplicantId", insuranceApplicant.getId());
		
		IdentificationType identificationType = insuranceApplicant.getExchangeAssignedInsuranceApplicantIdentification();
			if (identificationType != null && identificationType.getIdentificationID() != null) {
				String id = identificationType.getIdentificationID().getValue();
				personMap.put("exchangeAssignedInsuranceApplicantIdentificationId",	id);
			}
		
		ReferenceType referenceType = insuranceApplicant.getRoleOfPersonReference();
			if (referenceType != null && null != referenceType.getRef() && referenceType.getRef() instanceof PersonType) {
				PersonType person = (PersonType) referenceType.getRef();
				createIndividualPersonMap(person, personMap, applicantEligibilityResponse);
			}
		ProgramEligibilitySnapshotType activeProgramEligibilitySnapshot = this.getActiveEligibility(insuranceApplicant);

		if (activeProgramEligibilitySnapshot != null) {
			CSREligibilityType cSREligibility = activeProgramEligibilitySnapshot.getCSREligibility();
			if (null != cSREligibility) {
				Map<String, Object> cSREligibilityMap = new HashMap<String, Object>();
				if (null != cSREligibility.getInsurancePlanVariantCategoryCode()) {
					List<JAXBElement<?>> insurancePlanVariantCategoryCodeList = cSREligibility.getInsurancePlanVariantCategoryCode();
					if (insurancePlanVariantCategoryCodeList != null && !insurancePlanVariantCategoryCodeList.isEmpty()) {
						for (JAXBElement<?> insurancePlanVariantCategoryCode : insurancePlanVariantCategoryCodeList) {
							InsurancePlanVariantCategoryNumericCodeType insurancePlanVariantCategoryNumericCodeType = (InsurancePlanVariantCategoryNumericCodeType) insurancePlanVariantCategoryCode.getValue();
							String costSharing = insurancePlanVariantCategoryNumericCodeType.getValue();
							cSREligibilityMap.put("costSharing", costSharing);
							break;
						}
					}
				}
				getEligibilityIndicator(cSREligibility.getEligibilityIndicator(), cSREligibilityMap);
				getStartEndDate(cSREligibility.getEligibilityDateRange(), cSREligibilityMap);
				personMap.put("cSREligibilityMap", cSREligibilityMap);
			}
			
			APTCEligibilityType aPTCEligibility = activeProgramEligibilitySnapshot.getAPTCEligibility();
			if (null != aPTCEligibility) {
				Map<String, Object> aPTCEligibilityMap = new HashMap<String, Object>();
				if (null != aPTCEligibility.getMonthlyAPTCAmount()) {
					String monthlyAptcAmount = aPTCEligibility.getMonthlyAPTCAmount().getValue().toString();
					aPTCEligibilityMap.put("monthlyAptcAmount",	monthlyAptcAmount);
				}
				getEligibilityIndicator(aPTCEligibility.getEligibilityIndicator(), aPTCEligibilityMap);
				getStartEndDate(aPTCEligibility.getEligibilityDateRange(), aPTCEligibilityMap);
				personMap.put("aPTCEligibilityMap", aPTCEligibilityMap);
			}
			// populate Medicaid Eligibility
            if(activeProgramEligibilitySnapshot.getMedicaidEligibility() != null 
                    && activeProgramEligibilitySnapshot.getMedicaidEligibility().getEligibilityDateRange() != null
                    && activeProgramEligibilitySnapshot.getMedicaidEligibility().getEligibilityDateRange().getStartDate() != null
                    && activeProgramEligibilitySnapshot.getMedicaidEligibility().getEligibilityDateRange().getStartDate().getDate() != null 
                    && activeProgramEligibilitySnapshot.getMedicaidEligibility().getEligibilityDateRange().getStartDate().getDate().getValue() != null
                    && activeProgramEligibilitySnapshot.getMedicaidEligibility().getEligibilityDateRange().getStartDate().getDate().getValue().getYear() == 2015) {
				
				personMap.put("medicaidEligibility", activeProgramEligibilitySnapshot.getMedicaidEligibility().getEligibilityIndicator());
			}

			// populate CHIP eligibility
			ProgramEligibilityType chipEligibility = activeProgramEligibilitySnapshot.getCHIPEligibility();
			if (null != chipEligibility) {
				personMap.put("chipEligibility", chipEligibility.getEligibilityIndicator());
			}
			ProgramEligibilityType exchangeEligibility = activeProgramEligibilitySnapshot.getExchangeEligibility();
			if (null != exchangeEligibility) {
				Map<String, Object> exchangeEligibilityMap = new HashMap<String, Object>();
				getProgramEligibilityStatusCode(exchangeEligibility.getProgramEligibilityStatusCode(), exchangeEligibilityMap);
				getStartEndDate(exchangeEligibility.getEligibilityDateRange(), exchangeEligibilityMap);
				personMap.put("exchangeEligibility", exchangeEligibility.getEligibilityIndicator());
				personMap.put("exchangeEligibilityMap", exchangeEligibilityMap);
			}
		}

		
		return personMap;
	}
	
	
	private void createIndividualPersonMap(PersonType person, Map<String, Object> personMap, ApplicantEligibilityResponseType applicantEligibilityResponseObj) {
		personMap.put(PlanDisplayConstants.MEMBER_ID, person.getId());
		personMap.put("personId", person.getId());
		if (null != person.getPersonBirthDate()) {
			String dob = DateUtil.changeFormat(person.getPersonBirthDate().getDate().getValue().toString(),	PlanDisplayConstants.DOB_DATE_FORMAT, PlanDisplayConstants.DOB_DATE_FORMAT_REQUIRED);
			personMap.put("dob", dob);
		}
		if (null != person.getPersonName()) {
			if (null != person.getPersonName().getPersonGivenName()) {
				String firstName = person.getPersonName().getPersonGivenName().getValue();
				personMap.put("firstName", firstName);
			}
			if (null != person.getPersonName().getPersonMiddleName()) {
				String middleName = person.getPersonName().getPersonMiddleName().getValue();
				personMap.put("middleName", middleName);
			}
			if (null != person.getPersonName().getPersonNameSuffixText()) {
				String suffixName = person.getPersonName().getPersonNameSuffixText().getValue();
				personMap.put("suffixName", suffixName);
			}
			if (null != person.getPersonName().getPersonSurName()) {
				String lastName = person.getPersonName().getPersonSurName().getValue();
				personMap.put("lastName", lastName);
			}
		}

		if (null != person.getPersonSexCode()) {
			String sexCode = person.getPersonSexCode().getValue().value();
			personMap.put("sexCode", sexCode);
		}
		if (null != person.getPersonSSNIdentification()) {
			String ssn = person.getPersonSSNIdentification().getIdentificationID().getValue();
			personMap.put("ssn", ssn);
		}
		if (null != person.getPersonMarriedIndicator()) {
			String maritalStatusCode = Boolean.toString(person.getPersonMarriedIndicator().isValue());
			personMap.put("maritalStatusCode", maritalStatusCode);
		}

		if (null != person.getPersonAugmentation()) {
			List<PersonAssociationType> personAssociationList = person.getPersonAugmentation().getPersonAssociation();
			List<Map<String, String>> relationshipList = new ArrayList<Map<String, String>>();
			// personMap.put("relationshipCode", StringUtils.EMPTY);
			for (PersonAssociationType personAssociation : personAssociationList) {
				Map<String, String> relationshipMap = new HashMap<String, String>();
				ReferenceType refPersonObj = personAssociation.getPersonReference().get(0);

				if (null != personAssociation.getFamilyRelationshipCode()) {
					String relationshipCode = personAssociation.getFamilyRelationshipCode().getValue();
					personMap.put("familyRelationshipCode", relationshipCode);
					com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.PersonType pessonType = (com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.PersonType) refPersonObj.getRef();
					
					if (applicantEligibilityResponseObj != null) {
						List<JAXBElement<? extends com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType>> applicantList = (ArrayList<JAXBElement<? extends com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType>>) applicantEligibilityResponseObj.getInsuranceApplication().getInsuranceApplicant();
						for (JAXBElement<? extends com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType> applicant : applicantList) {
							com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType insuranceApplicant = (com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType) applicant.getValue();
							PersonType person1 = (PersonType) insuranceApplicant.getRoleOfPersonReference().getRef();
							boolean relationShipPresent = false;
							if(!relationshipList.isEmpty()){
								for(Map<String, String> existingRelationshipMap : relationshipList){
									if(existingRelationshipMap.get(PlanDisplayConstants.MEMBER_ID).equalsIgnoreCase(person1.getId())){
										relationShipPresent = true;
									}
								}
							}
							if (!relationShipPresent && pessonType.getId().equalsIgnoreCase(person1.getId())) {
								ProgramEligibilitySnapshotType activeProgramEligibilitySnapshot = this.getActiveEligibility(insuranceApplicant);
								String eligibilityVal = checkEligibility(activeProgramEligibilitySnapshot);
								if ("true".equals(eligibilityVal)) {
									relationshipMap.put(PlanDisplayConstants.MEMBER_ID,	person1.getId());// Integer.toString(memId++));
									IdentificationType identificationType = insuranceApplicant.getExchangeAssignedInsuranceApplicantIdentification();
									String exchangeAssignedApplicantId = null;
									if (identificationType != null && identificationType.getIdentificationID() != null) {
										exchangeAssignedApplicantId = identificationType.getIdentificationID().getValue();
									}
									relationshipMap.put("exchangeAssignedApplicantId", exchangeAssignedApplicantId);
									relationshipMap.put("relationshipCode", relationshipCode);
									relationshipList.add(relationshipMap);
								}
							}
						}
					}
						
				}
				// relationshipList.add(relationshipMap);
			}
			if (relationshipList != null && !relationshipList.isEmpty()) {
				personMap.put("relationInfoList", relationshipList);
			}

			List<PersonContactInformationAssociationType> personContactInformationAssociationList = person.getPersonAugmentation().getPersonContactInformationAssociation();
			for (PersonContactInformationAssociationType personContactInformationAssociationType : personContactInformationAssociationList) {
				com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.PersonContactInformationAssociationType personContactInformationAssociation = (com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.PersonContactInformationAssociationType) personContactInformationAssociationType;
				
				FFMPerson ffmPerson = new FFMPerson();
				boolean homeIndicator = false;
				if (null != personContactInformationAssociation.getContactInformationIsHomeIndicator()) {
					homeIndicator = personContactInformationAssociation.getContactInformationIsHomeIndicator().isValue();
					ffmPerson.setHomeIndicator(Boolean.toString(homeIndicator));
				}
				if (homeIndicator) {
					if (personContactInformationAssociation.getContactInformationReference() != null) {
						ContactInformationType contactInformationType = (com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.ContactInformationType) personContactInformationAssociation.getContactInformationReference().getRef();
						if (contactInformationType != null) {
							createIndividualContactInfoMap(contactInformationType, ffmPerson);
							personMap.put("personContactInfoMap", ffmPerson);
						}
					}
				}

				if (personContactInformationAssociation.getContactInformationReference() != null) {
					for (AddressTypeCodeType addressType : personContactInformationAssociation.getAddressTypeCode()) {
						if ("Mailing".equals(addressType.getValue())) {
							ContactInformationType contactInformationType = (com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.ContactInformationType) personContactInformationAssociation.getContactInformationReference().getRef();
							if (contactInformationType != null) {
								FFMPerson ffmMailingAddress = new FFMPerson();
								createIndividualContactInfoMap(contactInformationType, ffmMailingAddress);
								personMap.put("personMailingAddress", ffmMailingAddress);
							}
						}
					}
				}
				
				if (personContactInformationAssociation.getContactInformationReference() != null) {
					ContactInformationType contactInformationType = (com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.ContactInformationType) personContactInformationAssociation.getContactInformationReference().getRef();
					if(contactInformationType.getContactMainTelephoneNumber()!=null){
						List<FullTelephoneNumberType> fullTelephoneNumberTypeList = contactInformationType.getContactMainTelephoneNumber().getFullTelephoneNumber();
						if(fullTelephoneNumberTypeList!=null && !fullTelephoneNumberTypeList.isEmpty()){
							for(FullTelephoneNumberType fullTelephoneNumber:fullTelephoneNumberTypeList){
								if(fullTelephoneNumber.getTelephoneNumberFullID()!=null){
									String telephoneNumber =  fullTelephoneNumber.getTelephoneNumberFullID().getValue();
									personMap.put("telephoneNumber", telephoneNumber);
								}
								break;
							}
						}
					}
				}
				
				
				// personMap.put("personContactInfoMap", personContactInfoMap);
				// break;
			}
		}
	}
	
	private void createIndividualContactInfoMap(ContactInformationType contactInformation, FFMPerson ffmPerson) {
		if (null != contactInformation.getContactEmailID()) {
			String email = contactInformation.getContactEmailID().getValue();
			ffmPerson.setEmail(email);
		}

		List<AddressType> addressTypeList = contactInformation.getContactMailingAddress();
		if (addressTypeList != null && !addressTypeList.isEmpty()) {
			for (AddressType address : addressTypeList) {
				if (address.getStructuredAddress() != null) {
					if (null != address.getStructuredAddress().getLocationStreet()) {
						String streetName = address.getStructuredAddress().getLocationStreet().getStreetFullText().getValue();
						ffmPerson.setStreetName(streetName);
					}

					if (null != address.getStructuredAddress().getLocationCityName()) {
						String cityName = address.getStructuredAddress().getLocationCityName().getValue();
						ffmPerson.setCityName(cityName);
					}

					if (null != address.getStructuredAddress().getLocationPostalCode()) {
						String zipCode = address.getStructuredAddress().getLocationPostalCode().getValue().substring(0, 5);
						ffmPerson.setZipCode(zipCode);
					}
					if (null != address.getStructuredAddress().getLocationStateUSPostalServiceCode()) {
						String stateCode = address.getStructuredAddress().getLocationStateUSPostalServiceCode().getValue().value();
						ffmPerson.setStateCode(stateCode);
					}

					if (null != address.getStructuredAddress().getLocationCountyCode()) {
						String countyCode = address.getStructuredAddress().getLocationCountyCode().getValue();
						String stateCode = ffmPerson.getStateCode();
						String zipCode = ffmPerson.getZipCode();
						if (countyCode.length() < 5 && zipCode != null && stateCode != null) {
							List<ZipCode> zipCodeList = zipCodeService.findByZip(zipCode);
							if (zipCodeList != null) {
								for (ZipCode zipCodeObj : zipCodeList) {
									if (zipCodeObj.getCountyFips().equals(countyCode) && zipCodeObj.getState().equalsIgnoreCase(stateCode)) {
										countyCode = zipCodeObj.getStateFips().concat(zipCodeObj.getCountyFips());
									}
								}
							}
						}
						ffmPerson.setCountyCode(countyCode);
					}
				}
				break;
			}
		}		
	}
	
	private void getProgramEligibilityStatusCode(ProgramEligibilityStatusCodeType programEligibilityStatusCode,	Map<String, Object> eligibilityMap) {
		if (programEligibilityStatusCode != null) {
			String prgEligibilityStatusCode = programEligibilityStatusCode.getValue();
			eligibilityMap.put("programEligibilityStatusCode", prgEligibilityStatusCode);
		}
	}

	private void getEligibilityIndicator(com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.proxy.xsd._2.Boolean eligibilityIndicator, Map<String, Object> eligibilityMap) {
		if (eligibilityIndicator != null) {
			Boolean eligibilityIndicatorVal = eligibilityIndicator.isValue();
			eligibilityMap.put("eligibilityIndicator",	Boolean.toString(eligibilityIndicatorVal));
		}
	}

	private void getStartEndDate(DateRangeType dataRangeType,Map<String, Object> eligibilityMap) {
		if (dataRangeType != null) {
			DateType startDate = dataRangeType.getStartDate();
			if (startDate != null) {
				String startDateVal = startDate.getDate().getValue().toString();
				eligibilityMap.put("startDate", startDateVal);
			}
			DateType endDate = dataRangeType.getEndDate();
			if (endDate != null) {
				String endDateVal = endDate.getDate().getValue().toString();
				eligibilityMap.put("endDate", endDateVal);
			}
		}
	}

	public PdHousehold getLatestHousehold(List<PdHousehold> householdList, String requestType) {
		PdHousehold latestHousehold = null;
		if (householdList != null && !householdList.isEmpty()) {
			if (!(StringUtils.EMPTY.equals(requestType))) {
				for (PdHousehold household : householdList) {
					if (household.getRequestType().toString().equalsIgnoreCase(requestType)) {
						latestHousehold = household;
						break;
					}
				}
			} else {
				latestHousehold = householdList.get(0);
			}
		}
		return latestHousehold;
	}
	
	public ApplicantEligibilityResponseType getEligiblityResponse(Long giWsPayloadId){
		ApplicantEligibilityResponseType applicantEligibilityResponse = new ApplicantEligibilityResponseType();
		
		GIWSPayload giWSPayload = giWSPayloadService.findById(giWsPayloadId.intValue());

        try {
        	JAXBContext jaxbContext = JAXBContext.newInstance(
					com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.exchange._1.ObjectFactory.class,
					com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.structures._2.ObjectFactory.class,
					com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_core.ObjectFactory.class,
					com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_pm.ObjectFactory.class);
        	
        	Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        	DocumentBuilderFactory dbf = GhixUtils.getDocumentBuilderFactoryInstance();
			dbf.setExpandEntityReferences(false);
			dbf.setNamespaceAware(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document document = db.parse(new ByteArrayInputStream(giWSPayload.getResponsePayload().getBytes("UTF-8")));
	        @SuppressWarnings("unchecked")
			JAXBElement<ResponseType> jaxbApplicantEligibilityResponse = (JAXBElement<ResponseType>)jaxbUnmarshaller.unmarshal(document);
	        applicantEligibilityResponse = ((ResponseType)jaxbApplicantEligibilityResponse.getValue()).getApplicantEligibilityResponse();
        } catch (Exception e) {
          if(LOGGER.isErrorEnabled())
          {
        	putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Exception in PlanDisplayUtil.getEligiblityResponse()---->", e, false);
          }
        } 
        
        return applicantEligibilityResponse;
	}
	
	public List<PdPersonDTO> getNonEligibleMemberList(ApplicantEligibilityResponseType applicantEligibilityResponse) {	
		List<PdPersonDTO> nonEligibleMemberList = new ArrayList<PdPersonDTO>();

		if (applicantEligibilityResponse != null) {
			InsuranceApplicationType insuranceApplication = applicantEligibilityResponse.getInsuranceApplication();
			List<JAXBElement<? extends InsuranceApplicantType>> insuranceApplicantList = insuranceApplication.getInsuranceApplicant();
			for (JAXBElement<? extends InsuranceApplicantType> insuranceApplicantElement : insuranceApplicantList) {
				com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType insuranceApplicant = (com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType) insuranceApplicantElement.getValue();
				ReferenceType referenceType = insuranceApplicant.getRoleOfPersonReference();
					if (referenceType != null && null != referenceType.getRef() && referenceType.getRef() instanceof PersonType) {
						PersonType person = (PersonType) referenceType.getRef();
						PdPersonDTO pdPersonDTO = new PdPersonDTO();
						if (null != person.getPersonName()) {
							ProgramEligibilitySnapshotType activeProgramEligibilitySnapshot = this.getActiveEligibility(insuranceApplicant);
							if (activeProgramEligibilitySnapshot != null) {
								String eligibilityVal = checkEligibility(activeProgramEligibilitySnapshot);
								if (!"true".equals(eligibilityVal)) {
									if (null != person.getPersonName().getPersonGivenName()) {
										pdPersonDTO.setFirstName(person.getPersonName().getPersonGivenName().getValue());
									}
									if (null != person.getPersonName().getPersonSurName()) {
										pdPersonDTO.setLastName(person.getPersonName().getPersonSurName().getValue());
									}
									if (null != person.getPersonBirthDate().getDate().getValue()) {
										pdPersonDTO.setBirthDay(DateUtil.StringToDate(person.getPersonBirthDate().getDate().getValue().toString(), PlanDisplayConstants.DOB_DATE_FORMAT));
									}
									nonEligibleMemberList.add(pdPersonDTO);
								}
							}
						}
					}
			}
		}
		return nonEligibleMemberList;
	}
	
	public FFMPerson getHouseholdContact(Long giWsPayloadId) {
		ApplicantEligibilityResponseType applicantEligibilityResponse = this.getEligiblityResponse(giWsPayloadId);
		FFMPerson ffmPerson = new FFMPerson();
		if (applicantEligibilityResponse != null) {
			SSFPrimaryContactType sSFPrimaryContact = applicantEligibilityResponse.getSSFPrimaryContact();
			if (null != sSFPrimaryContact.getContactEmailID()) {
				String primaryContactEmail = sSFPrimaryContact.getContactEmailID().getValue();
				ffmPerson.setHouseHoldContactEmail(primaryContactEmail);
			}
			if (null != sSFPrimaryContact.getContactMobileTelephoneNumber()	
					&& null != sSFPrimaryContact.getContactMobileTelephoneNumber().getFullTelephoneNumber()	
					&& !sSFPrimaryContact.getContactMobileTelephoneNumber().getFullTelephoneNumber().isEmpty()) {
				if (null != sSFPrimaryContact.getContactMobileTelephoneNumber().getFullTelephoneNumber().get(0).getTelephoneNumberFullID()) {
					String primaryContactMobileNo = sSFPrimaryContact.getContactMobileTelephoneNumber().getFullTelephoneNumber().get(0).getTelephoneNumberFullID().getValue();
					ffmPerson.setHouseHoldContactMobileNo(primaryContactMobileNo);
				}
				if (null != sSFPrimaryContact.getContactMobileTelephoneNumber().getFullTelephoneNumber().get(0).getTelephoneSuffixID()) {
					String primaryContactMobileExt = sSFPrimaryContact.getContactMobileTelephoneNumber().getFullTelephoneNumber().get(0).getTelephoneSuffixID().getValue();
					ffmPerson.setHouseHoldContactMobileExt(primaryContactMobileExt);
				}
			}

			PersonType ssfPrimaryContactPerson = (PersonType) sSFPrimaryContact.getRoleOfPersonReference().getRef();
			
			String houseHoldContactFirstName = ssfPrimaryContactPerson.getPersonName().getPersonGivenName() != null ? ssfPrimaryContactPerson.getPersonName().getPersonGivenName().getValue()	: StringUtils.EMPTY;
			ffmPerson.setHouseHoldContactFirstName(houseHoldContactFirstName);
			
			String houseHoldContactMiddleName = ssfPrimaryContactPerson.getPersonName().getPersonMiddleName() != null ? ssfPrimaryContactPerson.getPersonName().getPersonMiddleName().getValue() : StringUtils.EMPTY;
			ffmPerson.setHouseHoldContactMiddleName(houseHoldContactMiddleName);
			
			String houseHoldContactLastName = ssfPrimaryContactPerson.getPersonName().getPersonSurName() != null ? ssfPrimaryContactPerson.getPersonName().getPersonSurName().getValue() : StringUtils.EMPTY;
			ffmPerson.setHouseHoldContactLastName(houseHoldContactLastName);
			
			String houseHoldContactSuffix = ssfPrimaryContactPerson.getPersonName().getPersonNameSuffixText() != null ? ssfPrimaryContactPerson.getPersonName().getPersonNameSuffixText().getValue() : StringUtils.EMPTY;
			ffmPerson.setHouseHoldContactSuffix(houseHoldContactSuffix);
			
			String houseHoldContactPersonID = ssfPrimaryContactPerson.getId();
			ffmPerson.setHouseHoldContactPersonID(houseHoldContactPersonID);
	
			
			List<PersonContactInformationAssociationType> personContactInformationAssociationList = ssfPrimaryContactPerson.getPersonAugmentation().getPersonContactInformationAssociation();
			for (PersonContactInformationAssociationType personContactInformationAssociationType : personContactInformationAssociationList) {
				com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.PersonContactInformationAssociationType personContactInformationAssociation = (com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.PersonContactInformationAssociationType) personContactInformationAssociationType;

				boolean homeIndicator = personContactInformationAssociation.getContactInformationIsHomeIndicator() != null ? personContactInformationAssociation.getContactInformationIsHomeIndicator().isValue() : false;
				if (homeIndicator) {
					if (personContactInformationAssociation.getContactInformationReference() != null) {
						ContactInformationType contactInformationType = (com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.ContactInformationType) personContactInformationAssociation.getContactInformationReference().getRef();
						if (contactInformationType != null) {
							createIndividualContactInfoMap(contactInformationType, ffmPerson);
						}
					}
				}

			}
		}
		return ffmPerson;
	}
	
	public List<FFMMemberResponse> getFFMMemberResponseList(Long giWsPayloadId) {
		List<FFMMemberResponse> fFMMemberResponseList = new ArrayList<FFMMemberResponse>();
		
		ApplicantEligibilityResponseType applicantEligibilityResponse = this.getEligiblityResponse(giWsPayloadId);
		
		if (applicantEligibilityResponse != null) {
			Map<String,String> relationMap = getRelationship(applicantEligibilityResponse.getSSFPrimaryContact());
			InsuranceApplicationType insuranceApplication = applicantEligibilityResponse.getInsuranceApplication();
			List<JAXBElement<? extends InsuranceApplicantType>> insuranceApplicantList = insuranceApplication.getInsuranceApplicant();
			for (JAXBElement<? extends InsuranceApplicantType> insuranceApplicantElement : insuranceApplicantList) {
				com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType insuranceApplicant = (com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType) insuranceApplicantElement.getValue();
				boolean isEligible = true;
				InitialEnrollmentPeriodEligibilityType initialEnrollmentPeriodEligibility = insuranceApplicant.getInitialEnrollmentPeriodEligibility();
				
				if(!initialEnrollmentPeriodEligibility.getEligibilityIndicator().isValue()){
					SpecialEnrollmentPeriodEligibilityType specialEnrollmentPeriodEligibility = insuranceApplicant.getSpecialEnrollmentPeriodEligibility();
					if(!specialEnrollmentPeriodEligibility.getEligibilityIndicator().isValue()){
						isEligible = false;
					}	
				}
				
				if(isEligible){
					ProgramEligibilitySnapshotType activeProgramEligibilitySnapshot = getActiveEligibility(insuranceApplicant);
					if(activeProgramEligibilitySnapshot!=null){
						String eligibilityVal = checkEligibility(activeProgramEligibilitySnapshot);
						if ("true".equals(eligibilityVal)) {
							Map<String, Object> personData = getPersonDataFromFFM(insuranceApplicant, null);
							FFMMemberResponse fFMMemberResponse = new FFMMemberResponse();
							String memberId = (String) personData.get(PlanDisplayConstants.MEMBER_ID);
							fFMMemberResponse.setPersonId(memberId);
							fFMMemberResponse.setSsn((String)personData.get("ssn"));

							String dob = (String)personData.get("dob");
							Date dateOfBirth = dob!=null?DateUtil.StringToDate(dob,PlanDisplayConstants.DOB_DATE_FORMAT_REQUIRED):null;
							String firstName = (String)personData.get("firstName");
							String middleName = (String)personData.get("middleName");
							String lastName = (String)personData.get("lastName");
							String sexCode = (String)personData.get("sexCode");
							com.getinsured.hix.model.ssap.ConsumerApplicant.Gender gender = null;
							if(sexCode!=null){
								gender = "F".equalsIgnoreCase(sexCode)?com.getinsured.hix.model.ssap.ConsumerApplicant.Gender.F:com.getinsured.hix.model.ssap.ConsumerApplicant.Gender.M;
							}
							String insuranceApplicantId = (String)personData.get("insuranceApplicantId");
							String relationShipCode = relationMap.get(memberId);
							Relationship relationship = Relationship.valueOf(PlanDisplayConstants.relationships.get(relationShipCode).toUpperCase());
														
							String maritalStatusCode = (String)personData.get("maritalStatusCode");
							com.getinsured.hix.model.ssap.ConsumerApplicant.BooleanFlag maritalFlag = null;
							if(maritalStatusCode!=null){
								maritalFlag = "false".equalsIgnoreCase(maritalStatusCode)?com.getinsured.hix.model.ssap.ConsumerApplicant.BooleanFlag.NO:com.getinsured.hix.model.ssap.ConsumerApplicant.BooleanFlag.YES;
							}
							String suffix = (String)personData.get("suffixName");
							String ffeApplicantId= personData.get("exchangeAssignedInsuranceApplicantIdentificationId")!=null?personData.get("exchangeAssignedInsuranceApplicantIdentificationId").toString():"0";
														
							fFMMemberResponse.setBirthDate(dateOfBirth);
							fFMMemberResponse.setFirstName(firstName);
							fFMMemberResponse.setMiddleName(middleName);
							fFMMemberResponse.setFamilyRelationshipCode(String.valueOf(relationship));
							fFMMemberResponse.setGender(gender);
							fFMMemberResponse.setLastName(lastName);
							fFMMemberResponse.setMaritalStatus(maritalFlag);
							fFMMemberResponse.setInsuranceApplicantId(insuranceApplicantId);
							fFMMemberResponse.setSuffix(suffix);
							fFMMemberResponse.setFfeApplicantId(Long.parseLong(ffeApplicantId));
							
							if(personData.get("cSREligibilityMap")!=null){
								Map cSREligibilityMap = (Map)personData.get("cSREligibilityMap");
								String csr = cSREligibilityMap.get("costSharing")!=null?cSREligibilityMap.get("costSharing").toString():null;
								fFMMemberResponse.setCsrLevel(csr);
								if(cSREligibilityMap.get("eligibilityIndicator")!=null){
									fFMMemberResponse.setCsrEligibilityIndicator("false".equalsIgnoreCase(cSREligibilityMap.get("eligibilityIndicator").toString())?com.getinsured.hix.model.ssap.ConsumerApplicant.BooleanFlag.NO:com.getinsured.hix.model.ssap.ConsumerApplicant.BooleanFlag.YES);
								}
								if(cSREligibilityMap.get("startDate")!=null){
									String startDate = cSREligibilityMap.get("startDate").toString();
									fFMMemberResponse.setCsrEligibilityStartDate(DateUtil.StringToDate(startDate, YYYY_MM_DD));
								}
								if(cSREligibilityMap.get("endDate")!=null){
									String endDate = cSREligibilityMap.get("endDate").toString();
									fFMMemberResponse.setCsrEligibilityEndDate(DateUtil.StringToDate(endDate, YYYY_MM_DD));
								}								
							}
							if(personData.get("aPTCEligibilityMap")!=null){
								Map aPTCEligibilityMap = (Map)personData.get("aPTCEligibilityMap");
								if(aPTCEligibilityMap.get("eligibilityIndicator")!=null){
									fFMMemberResponse.setAptcEligibilityIndicator("false".equalsIgnoreCase(aPTCEligibilityMap.get("eligibilityIndicator").toString())?BooleanFlag.NO:BooleanFlag.YES);
								}
								if(aPTCEligibilityMap.get("startDate")!=null){
									String startDate = aPTCEligibilityMap.get("startDate").toString();
									fFMMemberResponse.setAptcEligibilityStartDate(DateUtil.StringToDate(startDate, YYYY_MM_DD));
								}
								if(aPTCEligibilityMap.get("endDate")!=null){
									String endDate = aPTCEligibilityMap.get("endDate").toString();
									fFMMemberResponse.setAptcEligibilityEndDate(DateUtil.StringToDate(endDate, YYYY_MM_DD));
								}
								
							}
							
							if(personData.get("chipEligibility")!=null){
								com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.proxy.xsd._2.Boolean chipEligibilityIndicator = (com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.proxy.xsd._2.Boolean) personData.get("chipEligibility");
								if(chipEligibilityIndicator.isValue())
								{
									fFMMemberResponse.setChipEligibilityIndicator(BooleanFlag.YES);
								}
							}
							if(personData.get("medicaidEligibility")!=null){
								com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.proxy.xsd._2.Boolean medicaidEligibilityIndicator = (com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.proxy.xsd._2.Boolean) personData.get("medicaidEligibility");
								if(medicaidEligibilityIndicator.isValue())
								{
									fFMMemberResponse.setMedicaidEligibilityIndicator(BooleanFlag.YES);
								}
							}
							
							if(personData.get("exchangeEligibility")!=null){
								com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.proxy.xsd._2.Boolean exchangeEligibilityIndicator = (com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.proxy.xsd._2.Boolean) personData.get("exchangeEligibility");
								if(exchangeEligibilityIndicator.isValue())
								{
									fFMMemberResponse.setExchangeEligibilityIndicator(BooleanFlag.YES);
								}
							}
									
							if(personData.get("exchangeEligibilityMap")!=null){
								Map exchangeEligibilityMap = (Map)personData.get("exchangeEligibilityMap");
								if(exchangeEligibilityMap.get("programEligibilityStatusCode")!=null){
									fFMMemberResponse.setEligibilityStatus("NEW".equalsIgnoreCase(exchangeEligibilityMap.get("programEligibilityStatusCode").toString())?com.getinsured.hix.model.ssap.ConsumerApplicant.EligibilityStateValues.INITIAL_ELIGIBILITY_NEEDED:com.getinsured.hix.model.ssap.ConsumerApplicant.EligibilityStateValues.INITIAL_ELIGIBILITY_COMPLETE);
								}
								if(exchangeEligibilityMap.get("startDate")!=null){
									String startDate = exchangeEligibilityMap.get("startDate").toString();
									fFMMemberResponse.setEligibilityStartDate(DateUtil.StringToDate(startDate, YYYY_MM_DD));
								}
								if(exchangeEligibilityMap.get("endDate")!=null){
									String endDate = exchangeEligibilityMap.get("endDate").toString();
									fFMMemberResponse.setEligibilityEndDate(DateUtil.StringToDate(endDate, YYYY_MM_DD));
								}
								
							}
							
							FFMPerson personContactInfo = (FFMPerson) personData.get("personContactInfoMap");
							if(personContactInfo!=null){
								Location homeLocation=new Location();
								String zip = personContactInfo.getZipCode();								
								String streetName = personContactInfo.getStreetName();
								String cityName = personContactInfo.getCityName();
								String stateCode = personContactInfo.getStateCode();
								String countyCode = personContactInfo.getCountyCode();
								homeLocation.setZip(zip);
								homeLocation.setAddress1(streetName);
								homeLocation.setCity(cityName);
								homeLocation.setState(stateCode);
								homeLocation.setCountycode(countyCode);
								fFMMemberResponse.setHomeAddress(homeLocation);
							}				
							
							FFMPerson personMailingAddress = (FFMPerson) personData.get("personMailingAddress");
							if(personMailingAddress!=null){
								Location mailingLocation=new Location();
								String zip = personMailingAddress.getZipCode();								
								String streetName = personMailingAddress.getStreetName();
								String cityName = personMailingAddress.getCityName();
								String stateCode = personMailingAddress.getStateCode();
								String countyCode = personMailingAddress.getCountyCode();
								mailingLocation.setZip(zip);
								mailingLocation.setAddress1(streetName);
								mailingLocation.setCity(cityName);
								mailingLocation.setState(stateCode);
								mailingLocation.setCountycode(countyCode);
								fFMMemberResponse.setMailingAddress(mailingLocation);
							}
												
							fFMMemberResponseList.add(fFMMemberResponse);
						}
					}
				}
			}
		}
		
		return fFMMemberResponseList;
	}
	
	public Map<String, Object> getMaxBenefitAmePlan(String coverageStartDate, List<Map<String, String>> censusList){		
		AmePlanRequestDTO amePlanRequestDTO = new AmePlanRequestDTO();
		amePlanRequestDTO.setMemberList(censusList);
		amePlanRequestDTO.setEffectiveDate(coverageStartDate);
		
		String tenant = TENANT_GINS;
		if(TenantContextHolder.getTenant() != null && StringUtils.isNotBlank(TenantContextHolder.getTenant().getCode())){
			 tenant = TenantContextHolder.getTenant().getCode();
		}
		amePlanRequestDTO.setTenantCode(tenant);
		Map<String, Object> result = null;		
		try{
			AmePlanResponseDTO amePlanResponseDTO = amePlanRestController.getMaxBenefitAmePlan(amePlanRequestDTO);
			if(GhixConstants.RESPONSE_SUCCESS.equals(amePlanResponseDTO.getStatus()) && !amePlanResponseDTO.getAmePlanList().isEmpty()){
				AmePlan amePlan = amePlanResponseDTO.getAmePlanList().get(0);
				result = new HashMap<String, Object>();
				result.put("planId", String.valueOf(amePlan.getPlanId()));
				result.put("planPremium", amePlan.getPremium().toString());
				result.put("name", amePlan.getName());
				result.put("issuerLogo", amePlan.getIssuerLogo());
			}
		}catch (Exception e){
		  if(LOGGER.isErrorEnabled())
		  {
			putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		  }
		}
		return result;
	}
	
	public Map<String, Object> getLowestDentalPlan(String coverageStartDate, List<Map<String, String>> censusList, String tenant, String exchangeType){
		Map<String, Object> requestParameters = new HashMap<String, Object>();
		requestParameters.put("memberList", censusList);
		requestParameters.put("effectiveDate", coverageStartDate);
		requestParameters.put("exchangeType", exchangeType);
		tenant = TENANT_GINS;
		if(TenantContextHolder.getTenant() != null && StringUtils.isNotBlank(TenantContextHolder.getTenant().getCode())){
			tenant = TenantContextHolder.getTenant().getCode();
		}
		requestParameters.put("tenant", tenant);
		
		PlanRateBenefitRequest planRateBenefitRequest = new PlanRateBenefitRequest();
		Map<String, Object> result = null;
		planRateBenefitRequest.setRequestParameters(requestParameters);
		try 
        {
			PlanRateBenefitResponse planRateBenefitResponse = planRateBenefitController.getLowestDentalPremium(planRateBenefitRequest);
            if(GhixConstants.RESPONSE_SUCCESS.equals(planRateBenefitResponse.getStatus()) && !planRateBenefitResponse.getPlanRateBenefitList().isEmpty()){
            	PlanRateBenefit planRateBenefit = planRateBenefitResponse.getPlanRateBenefitList().get(0);
            	result = new HashMap<String, Object>();
            	result.put("planId", String.valueOf(planRateBenefit.getId()));
            	result.put("planPremium", planRateBenefit.getPremium().toString());
            	result.put("name", planRateBenefit.getName());
            	result.put("issuerLogo", planRateBenefit.getIssuerLogo());
            }
            
        }catch (Exception e){
          if(LOGGER.isErrorEnabled())
          {
            putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
          }
        }
		
		return result;
	}
	
	public Map<String, Object> getLowestVisionPlan(String coverageStartDate, List<Map<String, String>> censusList){
		VisionPlanRequestDTO visionPlanRequestDTO = new VisionPlanRequestDTO();
		visionPlanRequestDTO.setEffectiveDate(coverageStartDate);
		visionPlanRequestDTO.setMemberList(censusList);
		Map<String, Object> result = null;
		String tenant = TENANT_GINS;
		if(TenantContextHolder.getTenant() != null && StringUtils.isNotBlank(TenantContextHolder.getTenant().getCode())){
			tenant = TenantContextHolder.getTenant().getCode();
		}
		
		visionPlanRequestDTO.setTenant(tenant);
		try 
		{
			VisionPlanResponseDTO visonPlanResponseDTO = visionPlanRestController.getLowestVisionPlan(visionPlanRequestDTO);
			if(GhixConstants.RESPONSE_SUCCESS.equals(visonPlanResponseDTO.getStatus()) && !visonPlanResponseDTO.getVisionPlanList().isEmpty())
			{
				VisionPlan visionPlan = visonPlanResponseDTO.getVisionPlanList().get(0);
				result = new HashMap<String, Object>();
				result.put("planId", String.valueOf(visionPlan.getId()));
				result.put("planPremium", visionPlan.getPremium().toString());
				result.put("name", visionPlan.getName());
				result.put("issuerLogo", visionPlan.getIssuerLogo());
			}
		}catch (Exception e){
		  if(LOGGER.isErrorEnabled())
		  {
			putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		  }
		}

		return result;
	}
	
	/*
	 * When IND 19 member loop only has new members, and Enrollment Type = S, retrieve existing Enrollment ID from the termination loop and set this enrollment id into pdPerson.
	 */
	public void getEnrollmentIdFromDisenrollMembers(List<PdPerson> persons, DisenrollMembers disenrollMembers, Date coverageDate) {
		
		Long healthEnrollmentID = null;
		Long dentalEnrollmentID = null;	
		for(PdPerson pdPerson :persons){
			if(YorN.N.equals(pdPerson.getNewPersonFlag())){
				if(healthEnrollmentID == null) {
					healthEnrollmentID = pdPerson.getHealthEnrollmentId();
				}
				if(dentalEnrollmentID == null) {
					dentalEnrollmentID = pdPerson.getDentalEnrollmentId();
				}
			}
		}
		
		if(healthEnrollmentID==null || dentalEnrollmentID==null){
						
			Long disEnrollmentId1 = null;
			Long disEnrollmentId2 = null;
			for(DisenrollMember disenrollMember:disenrollMembers.getDisenrollMember()){
				List<String> enrollmentIds = disenrollMember.getEnrollments().getEnrollmentId();
				for(String enrollmentId : enrollmentIds){
					Long disEnrollmentId = Long.valueOf(enrollmentId);
					if(disEnrollmentId1==null){
						disEnrollmentId1 = disEnrollmentId;
					}
					if(disEnrollmentId2==null && !disEnrollmentId1.equals(disEnrollmentId)){
						disEnrollmentId2 = disEnrollmentId;
					}				
				}
			}
						
			if(healthEnrollmentID==null && dentalEnrollmentID!=null){
				if(dentalEnrollmentID.equals(disEnrollmentId1)){
					healthEnrollmentID = disEnrollmentId2;
				}else{
					healthEnrollmentID = disEnrollmentId1;
				}				
				setPersonEnrollmentId(persons,  healthEnrollmentID, dentalEnrollmentID);
			}else if(healthEnrollmentID!=null && dentalEnrollmentID==null){
				if(healthEnrollmentID.equals(disEnrollmentId1)){
					dentalEnrollmentID = disEnrollmentId2;
				}else{
					dentalEnrollmentID = disEnrollmentId1;
				}	
				setPersonEnrollmentId(persons,  healthEnrollmentID, dentalEnrollmentID);
			}else{
				if(disEnrollmentId1!=null){
					EnrollmentResponse enrollmentResponse = findPlanIdAndOrderItemIdByEnrollmentId(disEnrollmentId1.toString());
					Integer planId = enrollmentResponse.getPlanId();					
					Plan plan = getPlanInfoByPlanId(planId.longValue(), coverageDate);
					if(InsuranceType.HEALTH.toString().equals(plan.getInsuranceType())){							
							setPersonEnrollmentId(persons,  disEnrollmentId1, null);
					}else{							
						setPersonEnrollmentId(persons,  null, disEnrollmentId1);
					}					
				}
				if(disEnrollmentId2!=null){
					if(persons.get(0).getHealthEnrollmentId()==null){
						setPersonEnrollmentId(persons,  disEnrollmentId2, disEnrollmentId1);
					}else{
						setPersonEnrollmentId(persons,  disEnrollmentId1, disEnrollmentId2);
					}
				}
			}			
		}
	}
	
	private void setPersonEnrollmentId(List<PdPerson> persons,  Long healthEnrollmentID, Long dentalEnrollmentID){
		for(PdPerson pdPerson :persons){
			if(YorN.Y.equals(pdPerson.getNewPersonFlag())){
				pdPerson.setHealthEnrollmentId(healthEnrollmentID);
				pdPerson.setDentalEnrollmentId(dentalEnrollmentID);
			}
		}
	}
	
	public EnrollmentResponse findPlanIdAndOrderItemIdByEnrollmentId(String id){		
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		enrollmentRequest.setEnrollmentId(Integer.parseInt(id));
		String postResp = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_PLANID_ORDERITEM_ID_BY_ENROLLMENT_ID_JSON, GhixConstants.USER_NAME_EXCHANGE, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollmentRequest)).getBody();
		return platformGson.fromJson(postResp, EnrollmentResponse.class);
	}
	
	private Plan getPlanInfoByPlanId(Long planId, Date coverageDate) {
		PlanResponse planResponse = getPlanDetails(planId.toString(), DateUtil.dateToString(coverageDate, YYYY_MM_DD), true);
		Plan plan = new Plan();
		plan.setId(planResponse.getPlanId());
		plan.setName(planResponse.getPlanName());
		plan.setInsuranceType(planResponse.getInsuranceType());
		return plan;
	}
	
	public static void putLoggerStmt(Logger loggerSource, LogLevel logLevel, String logMessage, Exception excep, boolean isSanitizeForLoggingOn) 
	{
		 if(LogLevel.INFO.equals(logLevel) && loggerSource.isInfoEnabled()) {
			 String msg = isSanitizeForLoggingOn ? SecurityUtil.sanitizeForLogging(logMessage) : logMessage;
			 loggerSource.info(msg);
		 } 
		 else if(LogLevel.DEBUG.equals(logLevel) && loggerSource.isDebugEnabled()) {
			 String msg = isSanitizeForLoggingOn ? SecurityUtil.sanitizeForLogging(logMessage) : logMessage;
			 loggerSource.debug(msg);
		 }
		 else if(LogLevel.ERROR.equals(logLevel) && loggerSource.isErrorEnabled()) {
			 if(excep == null) {
				 String msg = isSanitizeForLoggingOn ? SecurityUtil.sanitizeForLogging(logMessage) : logMessage;
				 loggerSource.error(msg);
			 } else {
				 String msg = isSanitizeForLoggingOn ? SecurityUtil.sanitizeForLogging(logMessage) : logMessage;
				 loggerSource.error(msg,excep);
				 /*GIExceptionHandlerFactory.recordCriticalException(
								com.getinsured.hix.platform.util.exception.GIRuntimeException.Component.PLANDISPLAY, // Component name
								null, //ghixErrorCode
								msg, //errorMessage
								excep //Exception
								);*/
			 }
		 }else if(LogLevel.WARN.equals(logLevel) && loggerSource.isWarnEnabled()) {
			 String msg = isSanitizeForLoggingOn ? SecurityUtil.sanitizeForLogging(logMessage) : logMessage;
			 loggerSource.warn(msg);
		 } 
	}
	
	public EligLead fetchEligLeadRecord(long leadId){
		try{
			if (leadId > 0) {
				String response = ghixRestTemplate.postForObject(GhixEndPoints.PhixEndPoints.GET_LEAD, "\""+leadId+"\"",String.class); // FIXME: get rid of quotes from both ends!
				ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(EligLead.class);
				return reader.readValue(response);
			}else {
				return null;
			}
		} catch(Exception e){
		  if(LOGGER.isErrorEnabled())
		  {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Exception occured while fetching record from ELIG_LEAD table---->", e, false);
		  }
			return null;
		}
	}
	  
	  public PdPreferencesDTO getPdPreferencesDTO(PdPreferences pdPreferences)
	  {
		PdPreferencesDTO pdPreferencesDTO = new PdPreferencesDTO();
		if(StringUtils.isNotBlank(pdPreferences.getPreferences())){
			pdPreferencesDTO = (PdPreferencesDTO)platformGson.fromJson(pdPreferences.getPreferences(), PdPreferencesDTO.class);
		    pdPreferencesDTO.setEligLeadId(pdPreferences.getEligLeadId());
		    pdPreferencesDTO.setPdHouseholdId(pdPreferences.getPdHouseholdId());
	    }
		if(StringUtils.isNotBlank(pdPreferences.getDoctors())){
			 pdPreferencesDTO.setProviders(pdPreferences.getDoctors());
		}
		if(StringUtils.isNotBlank(pdPreferences.getPrescriptions())){
			 String prescription = pdPreferences.getPrescriptions();
			 if(prescription != null && prescription.contains("drugDosage")){
			    	pdPreferencesDTO.setPrescriptions(prescription);
			 }	    
		}
	    return pdPreferencesDTO;
	  }

	private String extractUserIdFromCorrelationId(String correlationId) {
		String[] correlationIds = StringUtils.splitByWholeSeparator(correlationId, ",");
		for (String pair : correlationIds) {
			String[] entry = pair.split("=");
			if (entry != null && entry.length > 0) {
				String key = entry[0];
				if ("userId".equals(key)) {
					return entry[1];
				}
			}
		}
		return null;
	}
	
	  private String getLoggedInUser(){
		String userName = null;
		String userId = extractUserIdFromCorrelationId(RequestCorrelationContext.getCurrent().getCorrelationId());
		int usrId;
		if (StringUtils.isNotBlank(userId))
		{
			try {
				usrId = Integer.parseInt(userId);
				AccountUser loggedInUser = userService.findById(usrId);
				if (loggedInUser != null) {
					userName = loggedInUser.getEmail();
				}
			} catch (NumberFormatException e) {
			  if(LOGGER.isErrorEnabled())
			  {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----getLoggedInUser from CorrelationId---->" + userId, null, false);
			  }
			}
		}
		else
		{			
			  try {
				  AccountUser loggedInUser = userService.getLoggedInUser();
				  if(loggedInUser!=null){
					  userName = loggedInUser.getEmail();
				  }
			  } catch (InvalidUserException ex) {
			    if(LOGGER.isErrorEnabled())
			    {
				  PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----InvalidUserException---->", ex, false);
			    }
			  }
		}
		return userName;
	  }
	  
	  private String getUserFromRequestHeader(SoapHeader header){
		  String userName = null;
		  if(header!=null){
			  Iterator<SoapHeaderElement> headerElements  = header.examineAllHeaderElements();
			  if(headerElements!=null){
				  while (headerElements.hasNext()){
					  SoapHeaderElement soapHeaderElement = headerElements.next();
					  if(soapHeaderElement!=null && soapHeaderElement.getName()!=null && USER_NAME.equalsIgnoreCase(soapHeaderElement.getName().getLocalPart())){
						  userName = soapHeaderElement.getText();
						  if(LOGGER.isDebugEnabled())
						  {
						  PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "<----UserFromRequestHeader---->"+userName, null, false);
					  }	            
				  }
			  }
		  }
		  }
		  return userName;
	  }
	  
	  public Map<String,String> getGlobalIdAndCorrelationId(SoapHeader header){
		  String globalId = null;
		  String correlationId = null;
		  if(header!=null){
			  Iterator<SoapHeaderElement> headerElements  = header.examineAllHeaderElements();
			  if(headerElements!=null){
				  while (headerElements.hasNext()){
					  SoapHeaderElement soapHeaderElement = headerElements.next();
					  if(soapHeaderElement!=null && soapHeaderElement.getName()!=null && LOG_GLOBAL_ID.equalsIgnoreCase(soapHeaderElement.getName().getLocalPart())){
						  globalId = soapHeaderElement.getText();
						  if(LOGGER.isDebugEnabled()){
							  PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "<----LOG_GLOBAL_ID FromRequestHeader---->"+globalId, null, false);
						  }	            
					  }
					  if(soapHeaderElement!=null && soapHeaderElement.getName()!=null && LOG_CORRELATION_ID.equalsIgnoreCase(soapHeaderElement.getName().getLocalPart())){
						  correlationId = soapHeaderElement.getText();
						  if(LOGGER.isDebugEnabled()){
							  PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "<----LOG_CORRELATION_ID FromRequestHeader---->"+globalId, null, false);
						  }	            
					  }
				  }
			  }
		  }
		  
		  Map<String,String> globalIdAndCorrelationId = new HashMap<String,String>();
		  globalIdAndCorrelationId.put("globalId",globalId);
		  globalIdAndCorrelationId.put("correlationId",correlationId);
		  return globalIdAndCorrelationId;
	  }
	  
	  public String getCurrentUser(SoapHeader header){
		  String loggedInUser = getLoggedInUser();
		  if(StringUtils.isNotBlank(loggedInUser)){
			  return loggedInUser;
		  }
		  
		  String userFromRequestHeader = getUserFromRequestHeader(header);
		  if(StringUtils.isNotBlank(userFromRequestHeader)){
			  return userFromRequestHeader;
		  }	
		  
		  return GhixConstants.USER_NAME_EXCHANGE;
	  }	    

  	public static PlanPreferences getDefaultPlanPreferences() {
  		PdPreferencesDTO preferences = getDefaultPreferencesDTO();
		PlanPreferences planPreferences = new PlanPreferences();
		planPreferences.setMedicalUse(preferences.getMedicalUse());
		planPreferences.setPrescriptionUse(preferences.getPrescriptionUse());
		planPreferences.setMedicalCondition(preferences.getMedicalCondition());
		planPreferences.setMedicalProcedure(preferences.getMedicalProcedure());
		return planPreferences;
	}
  	
  	public YorN setSeekCoverage(Date dentalCovgDate, String dob) {
		YorN seekCoverage = YorN.Y;
		String dentalPlanSelection = getDentalConfig(dentalCovgDate);
		
		if("PO".equals(dentalPlanSelection)){
			int age = PlanDisplayUtil.getAgeFromCoverageDate(dentalCovgDate, DateUtil.StringToDate(dob, "MM/dd/yyyy"));
			if(age < PlanDisplayConstants.CHILD_AGE){
				seekCoverage = YorN.Y;
			}else{
				seekCoverage = YorN.N;
			}
		}
		return seekCoverage;
	}
  	
  	public String validatePreferencesRequest(PdSaveProviderPrefRequest pdSaveProviderPrefRequest){
		if(pdSaveProviderPrefRequest == null){
			return "Blank Request";
		}
		if(pdSaveProviderPrefRequest.getProviders() == null){
			return PlanDisplayConstants.PROVIDER_NOT_EMPTY;
		}
		if(pdSaveProviderPrefRequest.getProviders().size() > 5){
			return "Maximum 5 Providers can be saved";
		}
		
		for(ProviderBean providerBean : pdSaveProviderPrefRequest.getProviders()){
			if(StringUtils.isBlank(providerBean.getId())){
				return PlanDisplayConstants.PROVIDER_ID_NOT_EMPTY;
			}
			if(StringUtils.isBlank(providerBean.getName())){
				return PlanDisplayConstants.PROVIDER_NAME_NOT_EMPTY;
			}
			if(StringUtils.isBlank(providerBean.getCity())){
				return PlanDisplayConstants.PROVIDER_CITY_NOT_EMPTY;
			}
			if(StringUtils.isBlank(providerBean.getState())){
				return "Providers state can not be empty";
			}
			if(!isValidState(providerBean.getState())){
				return PlanDisplayConstants.PROVIDER_STATE_NOT_VALID;
			}
			if(StringUtils.isBlank(providerBean.getProviderType())){
				return "ProviderType can not be empty";
			}
			if(!isValidProviderType(providerBean.getProviderType())){
				return "Invalid Provider type";
			}
			if(StringUtils.isBlank(providerBean.getLocationId())){
				return "LocationId can not be empty";
			}
		}
		return "VALID";
  	}
  	
  	/**
  	 * @deprecated	This method is not in use.
  	 * @param pdNetworkCompareRequest
  	 * @return
  	 */
  	@Deprecated
  	public String validateNetworkCompareRequest(PdNetworkCompareRequest pdNetworkCompareRequest){
		if(pdNetworkCompareRequest == null){
			return "Blank Request";
		}
		if(pdNetworkCompareRequest.getHiosIdList() == null || pdNetworkCompareRequest.getHiosIdList().isEmpty()){
			return "Empty Hios id list";
		}
		for(String hiosId : pdNetworkCompareRequest.getHiosIdList()){
			if(StringUtils.isBlank(hiosId) || !isValidHiosId(hiosId)){
				return PlanDisplayConstants.HIOS_ID_NOT_VALID+hiosId;
			}
		}
		if(pdNetworkCompareRequest.getZipCode() != null && !isValidZip(pdNetworkCompareRequest.getZipCode())){
			return PlanDisplayConstants.INVALID_ZIP_CODE;
		}
		if(pdNetworkCompareRequest.getRadius() != null && pdNetworkCompareRequest.getRadius() < 0){
			return "Invalid Radius";
		}
		return "VALID";
  	}
  	

	public String validateSaveHouseholdRequest(SaveHouseholdRequest saveHouseholdRequest) {
  		if(saveHouseholdRequest == null){
			return "Blank Request";
		}
		if(saveHouseholdRequest.getZipCode() == null){
			return "ZipCode is missing";
		}
		if(saveHouseholdRequest.getNoOfMembers() == null){
			return "NoOfMembers is missing";
		}
		
		/**
		 * Return error message when both coverageYear and coverageStartDate is passed in request. 
		 * Also return error when invalid coverageStartDate is passed.
		 */
		if(StringUtils.isNotBlank(saveHouseholdRequest.getCoverageStartDate()) && saveHouseholdRequest.getCoverageYear() != null) {
			return "Cannot pass both coverageYear and coverageStartDate in request";
		}
		
		if(saveHouseholdRequest.getCoverageYear() == null && StringUtils.isBlank(saveHouseholdRequest.getCoverageStartDate())){
			return "Please pass either coverageYear or coverageStartDate in request";
		}
		
		if (StringUtils.isNotBlank(saveHouseholdRequest.getCoverageStartDate())
				&& !DateUtil.isValidDate(saveHouseholdRequest.getCoverageStartDate(), "yyyy-MM-dd")) {
			return "Invalid coverageStartDate passed";
		}		
		
		if(!isValidZip(saveHouseholdRequest.getZipCode())){
			return PlanDisplayConstants.INVALID_ZIP_CODE;
		}
		if(!isValidNoOfMembers(saveHouseholdRequest.getNoOfMembers())){
			return PlanDisplayConstants.NOOFMEMBERS_NOT_VALID;
		}
		
		if(saveHouseholdRequest.getCoverageYear() != null && !isValidCoverageYear(saveHouseholdRequest.getCoverageYear())){
			return PlanDisplayConstants.COVERAGE_YEAR_NOT_VALID;
		}
		
		if(saveHouseholdRequest.getPlanPremiumList() == null || saveHouseholdRequest.getPlanPremiumList().isEmpty() ){
			return PlanDisplayConstants.PLANPREMIUM_NOT_EMPTY;
		}
		
		for(PlanPremiumDTO planPremiumDTO : saveHouseholdRequest.getPlanPremiumList()){
			if(StringUtils.isBlank(planPremiumDTO.getHiosId()) || !isValidHiosId(planPremiumDTO.getHiosId())){
				return PlanDisplayConstants.HIOS_ID_NOT_VALID+planPremiumDTO.getHiosId();
			}
			
			if(planPremiumDTO.getNetPremium() == null || planPremiumDTO.getNetPremium() < 0){
				return PlanDisplayConstants.NET_PREMIUM_NOT_NEGATIVE+planPremiumDTO.getNetPremium();
			}else if(planPremiumDTO.getNetPremium().isInfinite()){
				return "Invalid Premium";
			}
		}
		return "VALID";
		
  	}
    
  	private boolean isValidHiosId(String hiosId) {
    	Pattern pattern = Pattern.compile(PlanDisplayConstants.REG_FOR_HIOSID);
        Matcher matcher = pattern.matcher(hiosId);
        return matcher.matches();
    }

  	private boolean isValidZip(String zipCode) {
    	Pattern pattern = Pattern.compile(PlanDisplayConstants.REG_FOR_ZIP);
        Matcher matcher = pattern.matcher(zipCode);
        if(matcher.matches()){
        	ZipCode zipCodeObj = zipCodeService.findByZipCode(zipCode);
        	if(zipCodeObj != null){
        		return true;
        	}
        }
        return false;
    }
    
    private boolean isValidNoOfMembers(Integer memberCount){
		if(memberCount != null && memberCount >= 1 && memberCount <= 30){
			return true;
		}
		return false;
	}
    
    private boolean isValidCoverageYear(Integer year){    	
    	return year != null && year >= 2017 ? true : false;
    }
    
    private boolean isValidState(String state) {
    	Pattern pattern = Pattern.compile(PlanDisplayConstants.REG_FOR_STATE);
        Matcher matcher = pattern.matcher(state);
        return matcher.matches();
    }
    
    private boolean isValidProviderType(String providerType) {
		if(providerType.equalsIgnoreCase("doctor") || providerType.equalsIgnoreCase("hospital")){
			return true;
		}
		return false;
	}
  	
  	public List<PlanRateBenefit> getPlanRateBenefitsByplanId(List<String> hiosPlanIds, String insuranceType, String coverageStartDate) {
  		long start = TimeShifterUtil.currentTimeMillis();
		Map<String, Object> requestParameters = new HashMap<String, Object>();
		List<PlanRateBenefit> responseData = null;
		requestParameters.put("hiosPlanIds", hiosPlanIds);
		requestParameters.put("insuranceType", insuranceType);
		requestParameters.put("coverageStartDate", coverageStartDate);
		PlanRateBenefitRequest planRateBenefitRequest = new PlanRateBenefitRequest();
		planRateBenefitRequest.setRequestParameters(requestParameters);
		try {
			
			responseData = planRateBenefitController.getBenefitsAndCostHelperNew(planRateBenefitRequest);
		} catch (Exception e) {
			putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Exception in getPlanRateBenefitsByplanId---->", e, false);
		}
		long end = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "planSelection ended : "+ (end - start), null, false);

		return responseData;
	}
  	
  	/**
	 * We check if either co-insurance or co-pay is present, this benefit is
	 * covered in the plan
	 * 
	 * @param benefitMap
	 * @return
	 */
  	private boolean isBenefitPresent(Map<String, String> benefitMap) {
		if (benefitMap != null) {
			String isCovered = benefitMap.get("isCovered");
			if (isCovered != null && PlanDisplayConstants.COVERED.equalsIgnoreCase(isCovered)) {
				return true;
			}
		}
		return false;
	}
  	
  	private boolean isUsageModerateOrLow(Map<String, Integer> usage) {
		if (usage.containsKey(PlanDisplayConstants.HIGH) || usage.containsKey(PlanDisplayConstants.VERYHIGH)) {
			if (0 < usage.get(PlanDisplayConstants.HIGH) || 0 < usage.get(PlanDisplayConstants.VERYHIGH)) {
				return false;
			}
		}

		return true;
	}
  	  	
  	public void calculateSmartScore(List<String> prefBenefits, List<IndividualPlan> individualPlans,
			Map<String, Integer> medicalUsage, Map<String, Integer> drugUsage, Integer doctorsSelected, Integer drugsSelected) {
		String key;
		List<String> items = new ArrayList<String>();
		if (prefBenefits != null) {
			items = prefBenefits;
		}
		
		if (items != null) {
			Map<Integer, Double> planOop = new HashMap<Integer, Double>();
			Map<Integer, Double> deductible = new HashMap<Integer, Double>();
			Map<Integer, Integer> planBenefitScore = new HashMap<Integer, Integer>();
			Map<Integer, Integer> doctorsInNetwork = new HashMap<Integer, Integer>();
			Map<Integer, Integer> availableDrugs = new HashMap<Integer, Integer>();

			for (IndividualPlan individualPlan : individualPlans) {
				if ("N".equals(individualPlan.getIsPuf()) && individualPlan.getEstimatedTotalHealthCareCost() != 0) {
					planOop.put(individualPlan.getPlanId(), new Double(individualPlan.getEstimatedTotalHealthCareCost()));
					deductible.put(individualPlan.getPlanId(), new Double(individualPlan.getDeductible()));
					doctorsInNetwork.put(individualPlan.getPlanId(), getNoOfDoctorsInPlan(individualPlan.getDoctors()));
					availableDrugs.put(individualPlan.getPlanId(), getNoOfDrugsInPlan(individualPlan.getPrescriptionResponseList()));
					int counter = 0;
					for (String benefit : items) {
						key = PlanDisplayConstants.benefitsMapKey.get(benefit);
						if (individualPlan.getPlanTier1().containsKey(key)
								&& isBenefitPresent(individualPlan.getPlanTier1().get(key))) {
							counter++;
						}
					}
					planBenefitScore.put(individualPlan.getPlanId(), counter);
				}
			}

			boolean doWeAccountDeductible = isUsageModerateOrLow(medicalUsage) && isUsageModerateOrLow(drugUsage);
			Map<Integer, Double> gpsScore = new HashMap<Integer, Double>();
			if (!planOop.isEmpty()) {
				if("WA".equalsIgnoreCase(stateCode)){
					GPSIronMan gpsCalculator = new GPSIronMan();
					gpsScore = gpsCalculator.computeRawScore(planOop,
							doWeAccountDeductible ? deductible : null, planBenefitScore, items.size(), doctorsInNetwork,
							doctorsSelected, availableDrugs, drugsSelected);
				}else{
					GPSCalculatorService gpsCalculator = new GPSSuperMan();
					gpsScore = gpsCalculator.calculateUsingDeductibleAndDoctorSearch(planOop,
							doWeAccountDeductible ? deductible : null, planBenefitScore, items.size(), doctorsInNetwork,
							doctorsSelected);
				}
			}

			if (gpsScore != null) {
				for (IndividualPlan individualPlan : individualPlans) {
					if ("N".equals(individualPlan.getIsPuf()) && individualPlan.getEstimatedTotalHealthCareCost() != 0) {
						individualPlan.setSmartScore(gpsScore.get(individualPlan.getPlanId()).intValue());
					} else {
						individualPlan.setSmartScore(0);
					}
				}
			}
		}
	}

	public String getCoverageDate(int coverageYear) {
		return coverageYear+"-01-01";
	}

	// iso date format: yyyy-MM-dd
	public static int extractYearFromDate(String isoDate) {
		if (DateUtil.isValidDate(isoDate, "yyyy-MM-dd")) {
			String year = isoDate.split("-")[0];
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(year);
			}
			return Integer.parseInt(year);
		} else {
			throw new DateTimeException("Date format is not valid!");
		}
	}

	public static int getRemainingMonthsForCoverage(String coverageStartDate, String state) {
		if(state.equalsIgnoreCase("CA")) {
		    return 12;	
		}
		
		if(!DateUtil.isValidDate(coverageStartDate, "yyyy-MM-dd")) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.info("Coverage start date is not valid");
			}
			return 12;
		}
		
		// compare with month, remove leading zero in case of "08", "09" etc
		String coverageStrMonth = coverageStartDate.split("-")[1].replaceFirst("^0+(?!$)", "");
		
		int coverageMonth = Integer.parseInt(coverageStrMonth);
		
		int remainingMonth = 12 - coverageMonth;
		
		// 9 - 11 return 12
		if (remainingMonth >= 9 && remainingMonth <= 11) {
			return 12;
		}
		// 6 - 8 return 9
		else if (remainingMonth >= 6 && remainingMonth <= 8) {
			return 9;
		}
		// 3 - 5 return 6
		else if (remainingMonth >= 3 && remainingMonth <= 5) {
			return 6;
		}
		// 0 - 2 return 3
		else if (remainingMonth >= 0 && remainingMonth <= 2) {
			return 3;
		}
		
		return 12;
	}

	public List<IndividualPlan> getEstimatedCosts(List<PlanRateBenefit> planRateBenefitList, Map<Long, Double> costCompareData, Map<String, List<DrugDTO>> planDrugListMap, PdPreferencesDTO pdPreferencesDTO, InsuranceType insuranceType, int memberCount, List<PlanPremiumDTO> planPremiumList, List<ProviderBean> providers, Map<String, Future<Map<String, Object>>> futureProviderResponseListMap, String[] props, String coverageStartDate) {
		List<IndividualPlan> individualPlans = new ArrayList<IndividualPlan>();
		
		if(planPremiumList != null){
			for(PlanPremiumDTO planPremium : planPremiumList){						
				for(PlanRateBenefit planMap : planRateBenefitList){
					String hiosId = planMap.getHiosPlanNumber();
					if(StringUtils.isNotEmpty(planPremium.getHiosId()) && planPremium.getHiosId().equals(hiosId)){	
					IndividualPlan individualPlan = new IndividualPlan();	
					individualPlan.setPlanId(planMap.getId());
					
					if(planPremium.getPlanTab() != null){
						individualPlan.setPlanTab(planPremium.getPlanTab());						
					}
					
						
					individualPlan.setIssuerPlanNumber(hiosId);
					individualPlan.setIsPuf("N");
					individualPlan.setPlanType(InsuranceType.HEALTH.toString());
					
					individualPlan.setNetworkName(planMap.getNetworkName());
					
					int coverageYear = extractYearFromDate(coverageStartDate);
					
					individualPlan.setApplicableYear(coverageYear);
					List<HashMap<String, Object>> doctors = prepareProviderDataMap(providers, null, individualPlan, futureProviderResponseListMap);
					individualPlan.setDoctors(doctors);
					
					individualPlan.setPrescriptionResponseList(planDrugListMap.get(hiosId));
					
					if(null!=planMap.getPlanCosts()){
						calculateDeductible(individualPlan, planMap.getPlanCosts(), memberCount);
					}
					
					if(null!=planMap.getPlanBenefits()){
						individualPlan.setPlanTier1(parsePlanBenefitsTier1(planMap.getPlanBenefits(), individualPlan, props));
					}
						
						String netwkType = memberCount > 1 ? "inNetworkFly".intern() : "inNetworkInd".intern();				
						
						if(PlanDisplayEnum.InsuranceType.HEALTH.equals(insuranceType) ){
							setOopMaxForHealth(planMap.getPlanCosts(), netwkType, individualPlan);
						}
						
						setSbcCost(planMap.getSbcScenarioDTO(), individualPlan);
						
						Float netPremium = planPremium.getNetPremium();
						// remainingMonths = no of months user needs to pay premium
						int remainingMonths = 13 - Integer.parseInt(coverageStartDate.split("-")[1]);
						individualPlan.setAnnualPremiumAfterCredit(netPremium * remainingMonths);
						
						if(PlanDisplayEnum.InsuranceType.HEALTH.equals(insuranceType)){
							Double estimatedOop = costCompareData.get(Long.valueOf(planMap.getId()));
							try{
								if (estimatedOop != null){							
									estimatedOop = calculateEstimatedOopForMedicalProcedure(pdPreferencesDTO, individualPlan, estimatedOop);							
									//estimatedOop = calculateEstamatedOopForDrugList(planDrugList, estimatedOop);	
									BigDecimal estimatedTotalHealthCareCost = new BigDecimal(Float.toString(individualPlan.getAnnualPremiumAfterCredit())).add(new BigDecimal(Double.toString(estimatedOop))).setScale(2,BigDecimal.ROUND_HALF_UP);
									individualPlan.setEstimatedTotalHealthCareCost(estimatedTotalHealthCareCost.floatValue());
								}
							}catch(Exception ex){
								individualPlan.setEstimatedTotalHealthCareCost(0);
								PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error while calculating Estimated cost: "+estimatedOop+" for PlanId ---->"+individualPlan.getPlanId(), ex, false);
							}
						}
						individualPlans.add(individualPlan);	
					}													
				}
			}
		}
		return individualPlans;
	}

	public String generatePlanScoreResponse(List<IndividualPlan> individualPlans, List<ProviderBean> providers, List<String> benefitList, List<String> hiosPlanIds) {
		List<PdWSResponse> pdWSResponseList = new ArrayList<PdWSResponse>();
		List<String> validHiosPlanIds = new ArrayList<String>();
		boolean phixOnlyFlag = "PHIX".equalsIgnoreCase(exchangeTypeConfig);
		for(IndividualPlan individualPlan : individualPlans){
			validHiosPlanIds.add(individualPlan.getIssuerPlanNumber());
			
			PdWSResponse pdWSResponse = new PdWSResponse();
			pdWSResponse.setHiosId(individualPlan.getIssuerPlanNumber());
			pdWSResponse.setEstimatedCost(individualPlan.getEstimatedTotalHealthCareCost());
			pdWSResponse.setScore(individualPlan.getSmartScore());
			
			if(individualPlan.getPlanTab() != null){
				pdWSResponse.setPlanTab(individualPlan.getPlanTab());
			}			
			
			if(individualPlan.getDoctors() != null && !individualPlan.getDoctors().isEmpty()){
				List<PdWSResponse.Provider> providerList= new  ArrayList<PdWSResponse.Provider>();
				for (Map<String, Object> doctor : individualPlan.getDoctors()) {
					PdWSResponse.Provider providerObj = pdWSResponse.new Provider();
					String networkStatus = (String) doctor.get("networkStatus");
					String coveredByPlan = "N";
					if (PlanDisplayUtil.IN_NETWORK.equals(networkStatus)) {
						coveredByPlan = "Y";
					} 
					providerObj.setId((String) doctor.get("providerId"));
					providerObj.setName((String) doctor.get("providerName"));
					providerObj.setCoveredByPlan(coveredByPlan);
					providerObj.setLocationId((String) doctor.get("locationId"));
					providerObj.setProviderType((String) doctor.get("providerType"));
					providerObj.setState((String) doctor.get("providerState"));
					providerObj.setCity((String) doctor.get("providerCity"));
					
					providerList.add(providerObj);
				}			
				pdWSResponse.setProviders(providerList);
			}
			
			if(individualPlan.getPrescriptionResponseList() != null && !individualPlan.getPrescriptionResponseList().isEmpty()){
				List<PdWSResponse.Drug> drugList= new  ArrayList<PdWSResponse.Drug>();
				for(DrugDTO drug:individualPlan.getPrescriptionResponseList()){
					PdWSResponse.Drug drugObj = pdWSResponse.new Drug();
					drugObj.setId(drug.getDrugRxCode() != null ? drug.getDrugRxCode() : drug.getDrugID());
					drugObj.setName(StringUtils.substring(drug.getDrugName(), 0, 200));
					drugObj.setType(drug.getDrugType());
					drugObj.setCoveredByPlan(drug.getIsDrugCovered());
					drugObj.setFairPrice(drug.getDrugFairPrice());
					drugObj.setDosage(StringUtils.substring(drug.getDrugDosage(), 0, 200));
					drugObj.setCost(drug.getDrugCost());
					drugObj.setGenericFairPrice(drug.getGenericFairPrice());
					drugObj.setGenericDosage(StringUtils.substring(drug.getGenericDosage(), 0, 200));
					drugObj.setGenericCost(drug.getGenericCost());
					drugObj.setGenericCoveredByPlan(drug.getIsGenericCovered());
					//Set these drug data only for PHIX.
					if(phixOnlyFlag){
						drugObj.setTier(drug.getDrugTier());
						drugObj.setGenericId(drug.getGenericID());
						drugObj.setGenericTier(drug.getGenericTier());
					}
					drugList.add(drugObj);
				}
				pdWSResponse.setDrugs(drugList);
			}
			
			if(benefitList != null && !benefitList.isEmpty()){
				List<PdWSResponse.Benefit> benefits = new ArrayList<PdWSResponse.Benefit>();
				Map<String, String> benefitsCoverage = individualPlan.getBenefitsCoverage();
				for(String benefit : benefitList){
					PdWSResponse.Benefit benefitObj = pdWSResponse.new Benefit();
					String coveredByPlan = "N";
					if("GOOD".equals(benefitsCoverage.get(benefit))){
						coveredByPlan = "Y";
					}
					benefitObj.setName(benefit);
					benefitObj.setCoveredByPlan(coveredByPlan);
					benefits.add(benefitObj);
				}
				pdWSResponse.setBenefits(benefits);
			}
			//Set Expense Estimate in pdWSResponse only for PHIX.
			if(phixOnlyFlag){
				pdWSResponse.setExpenseEstimate(individualPlan.getExpenseEstimate());
			}
			
			pdWSResponseList.add(pdWSResponse);
		}
		
		for(String hiosPlanId : hiosPlanIds){
			if(!validHiosPlanIds.contains(hiosPlanId)){
				PdWSResponse pdWSResponse = new PdWSResponse();
				pdWSResponse.setHiosId(hiosPlanId);
				pdWSResponse.setEstimatedCost(0);
				pdWSResponse.setScore(0);
				pdWSResponseList.add(pdWSResponse);
			}
		}
		
		return platformGson.toJson(pdWSResponseList);
	}


	public Map<String, SbcScenarioDTO> getSbcCost(List<String> hiosPlanIdList, String insuranceType, Integer applicableYear) {
		SbcScenarioRequestDTO sbcScenarioRequestDTO = new SbcScenarioRequestDTO();
		sbcScenarioRequestDTO.setHiosPlanIdList(hiosPlanIdList);
		sbcScenarioRequestDTO.setApplicableYear(applicableYear);
		try {
			String sbcScenarioResponseStr = ghixRestTemplate.postForObject(GhixEndPoints.PlanMgmtEndPoints.GET_SBC_SCENARIO_BY_HIOS_PLAN_ID, sbcScenarioRequestDTO, String.class);			
			putLoggerStmt(LOGGER, LogLevel.DEBUG, "<----GET_SBC_SCENARIO_BY_HIOS_PLAN_ID str---->"+sbcScenarioResponseStr, null, true);
			
			if (!StringUtils.isEmpty(sbcScenarioResponseStr)) {
				SbcScenarioResponseDTO sbcScenarioResponseDTO = (SbcScenarioResponseDTO) platformGson.fromJson(sbcScenarioResponseStr, SbcScenarioResponseDTO.class);
				if (sbcScenarioResponseDTO != null) {
					return sbcScenarioResponseDTO.getSbcScenario();
				}
			}
		} catch (Exception e) {
			putLoggerStmt(LOGGER, LogLevel.ERROR, "----Exception occured while fetching record from getSbcCost----", e, false);
			
		}
		return Collections.emptyMap();
	}


	public void setBenefitCoverage(List<IndividualPlan> individualPlans, List<PlanRateBenefit> plans) {
		Map<String, Map<Integer, Double>> phixPlanBenefits = getBenefitCoverageData(plans);
		BenefitCoverageCalculator benefitCoverageCalculator = new BenefitCoverageCalculator(PlanDisplayConstants.benefitsMapKey);
		Map<Integer, Map<String, String>> planBenefitsCoverageOutput = benefitCoverageCalculator.calculateBenefitCoverage(phixPlanBenefits);
		for (IndividualPlan individualPlan : individualPlans) {
			Map<String, String> benefitsCoverageOutput = planBenefitsCoverageOutput.get(individualPlan.getPlanId());
			Map<String, String> benefitsCoverage = new HashMap<String, String>();
			if (benefitsCoverageOutput != null && !benefitsCoverageOutput.isEmpty()) {
				for (Map.Entry<String, String> entry : benefitsCoverageOutput.entrySet()) {
					benefitsCoverage.put(PlanDisplayConstants.benefitsKeyReveseMap.get(entry.getKey()), entry.getValue());
				}
			}			
			individualPlan.setBenefitsCoverage(benefitsCoverage);		
		}
	}
	
	public Map<String, Map<Integer, Double>> getBenefitCoverageData(List<PlanRateBenefit> plans) {
		Map<String, Map<Integer, Double>> phixPlanBenefits = new HashMap<String, Map<Integer, Double>>();
		
		for (Entry phixBenefit : PlanDisplayConstants.benefitsMapKey.entrySet()) {
			Map<Integer, Double> phixPlanBenefitCost = new HashMap<Integer, Double>();
			for (PlanRateBenefit planRateBenefits : plans) {
				if (planRateBenefits.getPlanBenefits().containsKey(phixBenefit.getValue())) {
					Map<String, String> benefit = planRateBenefits.getPlanBenefits().get(phixBenefit.getValue());
					CalculatorUtil calcUtil = new CalculatorUtil(PlanDisplayConstants.benefitsPriceMapKey);
					String paymentType = calcUtil.getChargeType(benefit);
					double cost = calcUtil.getBenefitPaymentCost(benefit, paymentType,
							phixBenefit.getValue().toString());
					
					if (calcUtil.isBenefitCovered(benefit)) {
						phixPlanBenefitCost.put(planRateBenefits.getId(), cost);
					}
				}
			}

			phixPlanBenefits.put(phixBenefit.getValue().toString(), phixPlanBenefitCost);
		}

		return phixPlanBenefits;
	}
	
	public Map<String, String> getRxCodesFromNdc(List<String> drugNdcList) {
		Map<String,String> rxCodesNdcMap = new HashMap<String,String>();
		Map<String,Future<Map<String,Object>>> ndcFutureRxNorms = new HashMap<String,Future<Map<String,Object>>>();
		for(String ndc : drugNdcList)
		{
			Future<Map<String,Object>> rxNormResponseForNdc = planDisplayAsyncUtil.getRxNormResponseForNdc(ndc);
			ndcFutureRxNorms.put(ndc, rxNormResponseForNdc);
		}
		
		for(Map.Entry<String,Future<Map<String,Object>>> rxNormResponseObjFutureEntry : ndcFutureRxNorms.entrySet())
		{
			Future<Map<String,Object>> rxNormResponseObjFuture	= rxNormResponseObjFutureEntry.getValue();
			Map<String, Object> rxNormResponseObj = null;
			try {
				rxNormResponseObj = rxNormResponseObjFuture.get();
			} catch (InterruptedException | ExecutionException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "Error while calling getRxCodesFromNdc", e, false);
			}
			if(rxNormResponseObj != null){
				Map idGroupMap = (Map<String, Object>) rxNormResponseObj.get("idGroup");
				if(idGroupMap!=null && idGroupMap.get("rxnormId") != null){
					List<String> rxnormIdList =  (List<String>) idGroupMap.get("rxnormId");
					if(rxnormIdList!=null && rxnormIdList.size() > 0){
						rxCodesNdcMap.put(rxNormResponseObjFutureEntry.getKey(),rxnormIdList.get(0));
					}
				}
			}
		}
		return rxCodesNdcMap;
	}

	public Map<String, Object> callMarketPlaceDrugSearch(String name) {
		try {
			String marketPlaceAPIUrl = DynamicPropertiesUtil.getPropertyValue("global.MarketPlaceAPI.Url");
			String marketPlaceAPIKey = DynamicPropertiesUtil.getPropertyValue("global.MarketPlaceAPI.Key");
			
			String response =  ghixRestTemplate.getForObject(marketPlaceAPIUrl + "/drugs/search?apikey="+marketPlaceAPIKey+"&q="+name, String.class);
			ObjectReader objreader = JacksonUtils.getJacksonObjectReaderForHashMap(String.class, Object.class);
			Map<String, Object> marketPlaceAPIResponseObj = objreader.readValue(response);
			return marketPlaceAPIResponseObj;
		} catch (Exception ex) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "Exception while calling MarketPlaceDrugSearch", ex, false);
		}
		return null;
	}
	
	public Map<String, Object> getRxNormResponseForNdc(String ndc) {
		Future<Map<String,Object>> rxNormResponseForNdc = planDisplayAsyncUtil.getRxNormResponseForNdc(ndc);
		try {
			return rxNormResponseForNdc.get();
		} catch (InterruptedException | ExecutionException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "Error while calling getRxNormResponseForNdc", e, false);
		}
		return null;
	}
	
	public void calculateExpenseEstimate(List<IndividualPlan> individualPlans) {
		float minimumEstimatedTotalCost = 0;
		float estimateCostRange = 0;
		
		HashMap<String,Float> minAndMaxEstimatedCost = this.getMinAndMaxEstimatedCost(individualPlans);
		minimumEstimatedTotalCost = minAndMaxEstimatedCost.get("MIN_COST".intern());
		float maximumEstimatedTotalCost = minAndMaxEstimatedCost.get("MAX_COST".intern());
		estimateCostRange = new BigDecimal(Float.toString(maximumEstimatedTotalCost)).subtract(new BigDecimal(Float.toString(minimumEstimatedTotalCost))).divide(BigDecimal.valueOf(3.0), 2, BigDecimal.ROUND_HALF_UP).floatValue();
		
		for (IndividualPlan individualPlan : individualPlans) {
			if ("N".equalsIgnoreCase(individualPlan.getIsPuf())) {
				if (estimateCostRange > 0) {
					if (individualPlan.getEstimatedTotalHealthCareCost() > 0) {
						individualPlan.setExpenseEstimate(fromInteger(
								(int) Math.ceil(new BigDecimal(Float.toString(individualPlan.getEstimatedTotalHealthCareCost()))
										.subtract(new BigDecimal(Float.toString(minimumEstimatedTotalCost)))
										.divide(new BigDecimal(Float.toString(estimateCostRange)), 2, BigDecimal.ROUND_HALF_UP)
										.floatValue())));
					} else {
						individualPlan.setExpenseEstimate("Pricey");
					}
				} else {
					individualPlan.setExpenseEstimate("Lower Cost");
				}
			} else {
				individualPlan.setExpenseEstimate("Pricey");
			}
		}		
	}
	
	public String fromInteger(int x) {
		switch (x) {
        case 0:
        case 1:
            return "Lower Cost";
        case 2:
            return "Average";
        case 3:
            return "Pricey";
        }
        return null;
    }
	
	public HashMap<String, Float> getMinAndMaxEstimatedCost(List<IndividualPlan> individualPlans){
		float maximumEstimatedTotalCost = 0;
		float minimumEstimatedTotalCost = 99999;
		float planCost = 0.0f;
		HashMap<String, Float> minAndMax = new HashMap<>();
		for (IndividualPlan individualPlan : individualPlans) {
			if ("N".equalsIgnoreCase(individualPlan.getIsPuf())
					&& individualPlan.getEstimatedTotalHealthCareCost() > 0) {
				planCost = individualPlan.getEstimatedTotalHealthCareCost();
				if (planCost > maximumEstimatedTotalCost) {
					maximumEstimatedTotalCost = individualPlan.getEstimatedTotalHealthCareCost();
				}
				if (planCost < minimumEstimatedTotalCost) {
					minimumEstimatedTotalCost = individualPlan.getEstimatedTotalHealthCareCost();
				}
			}
		}
		minAndMax.put("MIN_COST".intern(), minimumEstimatedTotalCost);
		minAndMax.put("MAX_COST".intern(), maximumEstimatedTotalCost);
		return minAndMax;
	}
	
	public void getMemberCoverageDateforHealthDental(Map<String, String> data, String enrollmentId, String userName, String memberId, String type, String changeEffectiveDate) {
		if(enrollmentId != null) {
			EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
			enrollmentRequest.setEnrollmentId(Integer.parseInt(enrollmentId));
			enrollmentRequest.setChangeEffectiveDate(changeEffectiveDate);
			String enrollmentResponseStr = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_ENROLLEE_COVERAGE_START_DATA_BY_ENROLLMENT_ID_AND_CHANGE_DATE, GhixConstants.USER_NAME_EXCHANGE, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollmentRequest)).getBody();
			
			XStream xstream = GhixUtils.getXStreamStaxObject();
			EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
			enrollmentResponse = (EnrollmentResponse) xstream.fromXML(enrollmentResponseStr);
			if (enrollmentResponse != null && enrollmentResponse.getIsEnrollmentExists())
			{
				List<Enrollee> enrollies = enrollmentResponse.getEnrolleeList();
				for (Enrollee enrollee : enrollies){
					if (memberId.equals(enrollee.getExchgIndivIdentifier())){
						data.put(type+memberId, enrollee.getQuotingDate()!=null?DateUtil.dateToString(enrollee.getQuotingDate(), "MM/dd/yyyy"):null);
					}
				}
			}
		}		
	}

	public boolean isAdultMember(String coverageStartDate, String dob) {
		if (coverageStartDate != null && !("".equals(coverageStartDate))) {
			Date formattedCoverageStartDate = DateUtil.StringToDate(coverageStartDate, "MM/dd/yyyy");
			int age = PlanDisplayUtil.getAgeFromCoverageDate(formattedCoverageStartDate, DateUtil.StringToDate(dob, "MM/dd/yyyy"));
			if(age >= PlanDisplayConstants.CHILD_AGE) {
				return true;
			}
		}
		return false;
	}
	
	private APTCPlanResponseDTO calculateSubsidy(APTCPlanRequestDTO planRequestDTO, BigDecimal availableAptc, BigDecimal currentStateSubsidy, int memberCount, boolean isCAProfile, List<APTCCalculatorMemberData> membersData) {
		APTCPlanResponseDTO planResponseDTO = new APTCPlanResponseDTO();
		planResponseDTO.setInsuranceType(planRequestDTO.getInsuranceType());
				
		BigDecimal premiumBeforeCredit = new BigDecimal(Float.toString(planRequestDTO.getPremium()));
		BigDecimal premiumAfterCredit = premiumBeforeCredit;
		BigDecimal applicablePremium = premiumBeforeCredit;
		BigDecimal ehbPercentage = planRequestDTO.getEhbPercentage() != null ? new BigDecimal(Float.toString(planRequestDTO.getEhbPercentage())) : null;
		BigDecimal minPremiumPerHousehold = BigDecimal.ZERO;

		if(InsuranceType.HEALTH.toString().equalsIgnoreCase(planRequestDTO.getInsuranceType())) {
			if("CATASTROPHIC".equalsIgnoreCase(planRequestDTO.getLevel())){
				applicablePremium = premiumBeforeCredit;
				availableAptc = BigDecimal.ZERO;
			}else{
				applicablePremium = calculateApplicablePremium(premiumBeforeCredit, ehbPercentage);
				minPremiumPerHousehold = calculateMinPremiumPerHoushold(memberCount);
			}
		}else if(InsuranceType.DENTAL.toString().equalsIgnoreCase(planRequestDTO.getInsuranceType())) {
			if(isCAProfile) {
				applicablePremium = premiumBeforeCredit;
				availableAptc = BigDecimal.ZERO;
			}else {
				applicablePremium = calculateApplicablePremium(premiumBeforeCredit, ehbPercentage);
				BigDecimal childPremium = new BigDecimal("0");
				if(membersData != null){
					for(APTCCalculatorMemberData member : membersData)
					{
						if(member.getAge() < PlanDisplayConstants.CHILD_AGE)
						{
						childPremium = childPremium.add(new BigDecimal(Float.toString(member.getPremium())));
						}
					}
				}				
				applicablePremium = childPremium;
			}
		}

		if(availableAptc != null && availableAptc.compareTo(applicablePremium) > 0) {
			availableAptc = applicablePremium.setScale(2,BigDecimal.ROUND_HALF_UP);
		}

		BigDecimal ehbMinimum = premiumBeforeCredit.subtract(applicablePremium);

		BigDecimal actualMinimum = minPremiumPerHousehold.max(ehbMinimum);

		if(availableAptc != null){
			if(availableAptc.compareTo(BigDecimal.ZERO) > 0) {
				BigDecimal appliedAptc = availableAptc;
	
				if (premiumBeforeCredit.subtract(appliedAptc).compareTo(actualMinimum) < 0) {
					appliedAptc = premiumBeforeCredit.subtract(actualMinimum).setScale(2, BigDecimal.ROUND_HALF_UP);
					premiumAfterCredit = actualMinimum;
				} else {
					premiumAfterCredit = premiumBeforeCredit.subtract(appliedAptc).setScale(2, BigDecimal.ROUND_HALF_UP);
				}
	
				planResponseDTO.setAppliedAptc(appliedAptc.floatValue());
			}else{
				planResponseDTO.setAppliedAptc(0.0f);
			}
		}

		if(currentStateSubsidy != null && currentStateSubsidy.compareTo(BigDecimal.ZERO) > 0){
			BigDecimal appliedStateSubsidy = currentStateSubsidy;
			if (premiumAfterCredit.subtract(appliedStateSubsidy).compareTo(actualMinimum) < 0) {
				appliedStateSubsidy = premiumAfterCredit.subtract(actualMinimum).setScale(2,BigDecimal.ROUND_HALF_UP);
				premiumAfterCredit = actualMinimum;
			}
			else {
				premiumAfterCredit = premiumAfterCredit.subtract(appliedStateSubsidy).setScale(2,BigDecimal.ROUND_HALF_UP);
			}

			planResponseDTO.setAppliedStateSubsidy(appliedStateSubsidy.setScale(2, BigDecimal.ROUND_HALF_UP));
		}

		planResponseDTO.setPremiumAfterCredit(premiumAfterCredit.floatValue());
		planResponseDTO.setPremiumBeforeCredit(premiumBeforeCredit.floatValue());
		
		return planResponseDTO;
	}
	
	public BigDecimal calculateMinPremiumPerHoushold(int memberCount) {
		BigDecimal minPremiumPerHousehold = BigDecimal.ZERO;
		if(StringUtils.isNotBlank(DynamicPropertiesUtil.getPropertyValue(PlanDisplayConfiguration.PlanDisplayConfigurationEnum.MIN_PREMIUM_PER_MEMBER))){
			String minPremiumPerMember = DynamicPropertiesUtil.getPropertyValue(PlanDisplayConfiguration.PlanDisplayConfigurationEnum.MIN_PREMIUM_PER_MEMBER);
			minPremiumPerHousehold = BigDecimal.valueOf(memberCount).multiply(new BigDecimal(minPremiumPerMember));
		}		
		return minPremiumPerHousehold;
	}

	public BigDecimal calculateApplicablePremium(BigDecimal premiumBeforeCredit, BigDecimal ehbPercentage) {
		BigDecimal defaultEhb = BigDecimal.valueOf(1.0);
		ehbPercentage = ehbPercentage == null ? defaultEhb : ehbPercentage;
		BigDecimal applicablePremium = premiumBeforeCredit.multiply(ehbPercentage);
		return applicablePremium;
	}

	public APTCCalculatorResponse calculateSliderAptc(APTCCalculatorRequest aptcCalculatorRequest){
		APTCCalculatorResponse aptcCalculatorResponse = new APTCCalculatorResponse();
		int memberCount = aptcCalculatorRequest.getMemberCount();
		Boolean isChildPresent = aptcCalculatorRequest.getIsChildPresent() != null ? aptcCalculatorRequest.getIsChildPresent() : false ;
		
		List<APTCPlanRequestDTO> planRequestList = aptcCalculatorRequest.getPlans();
		
		BigDecimal currentAptc = aptcCalculatorRequest.getCurrentAptc() != null ? new BigDecimal(Float.toString(aptcCalculatorRequest.getCurrentAptc())) : null;
		BigDecimal currentStateSubsidy = aptcCalculatorRequest.getCurrentStateSubsidy();

		List<APTCPlanResponseDTO> planResponseList = new ArrayList<>();
		
		if(planRequestList != null && !planRequestList.isEmpty()){
			boolean isCAProfile = stateCode != null ? stateCode.equalsIgnoreCase("CA") : false;
			APTCPlanResponseDTO healthPlanDTO = null;
			APTCPlanResponseDTO dentalPlanDTO = null;
			
			for(APTCPlanRequestDTO planRequestDTO : planRequestList){
				String insuranceType = planRequestDTO.getInsuranceType();
				String level = planRequestDTO.getLevel();
								
				if(InsuranceType.HEALTH.toString().equalsIgnoreCase(insuranceType)){
					healthPlanDTO = calculateSubsidy(planRequestDTO, currentAptc, currentStateSubsidy, memberCount, isCAProfile, null);
					planResponseList.add(healthPlanDTO);
				}
				if(InsuranceType.DENTAL.toString().equalsIgnoreCase(insuranceType)){
					BigDecimal remainingAptc = BigDecimal.ZERO;
					BigDecimal remainingStateSubsidy = null;
										
					if(healthPlanDTO != null && !"CATASTROPHIC".equalsIgnoreCase(level) && !isCAProfile && isChildPresent && currentAptc != null){
						remainingAptc = currentAptc.subtract(new BigDecimal(Float.toString(healthPlanDTO.getAppliedAptc()))).setScale(2,BigDecimal.ROUND_HALF_UP);
					}
					dentalPlanDTO = calculateSubsidy(planRequestDTO, remainingAptc, remainingStateSubsidy, memberCount, isCAProfile, aptcCalculatorRequest.getMemberData());
					planResponseList.add(dentalPlanDTO);
				}			
			}
		}		
		aptcCalculatorResponse.setPlans(planResponseList);		
		return aptcCalculatorResponse;
	}
	private BigDecimal calculateChildPredmium(List<Map<String, String>> planDetailsByMember) 
	{
		BigDecimal childPremium = new BigDecimal("0");
		for(Map<String,String> memberinfo : planDetailsByMember)
		{
			String age = memberinfo.get(PlanDisplayUtil.AGE);
			if(Float.parseFloat(age) < PlanDisplayConstants.CHILD_AGE)
			{
				childPremium = childPremium.add(BigDecimal.valueOf(Float.parseFloat(memberinfo.get("premium"))));
			}
		}
		return childPremium;
	}
	public APTCPlanRequestDTO createPlanRequestDTO(String insuranceType, Float premium, String level, Float ehbAmount) {
		APTCPlanRequestDTO planRequestDTO = new APTCPlanRequestDTO();
		planRequestDTO.setInsuranceType(insuranceType);
		planRequestDTO.setPremium(premium);
		planRequestDTO.setLevel(level);
		planRequestDTO.setEhbPercentage(ehbAmount);
		return planRequestDTO;
	}
	
	public static IndividualPlan buildEnrolledPlanDetails(PlanResponse planResponse, EnrollmentDataDTO enrollmentdata, List<ProviderBean> providers, List<DrugDTO> drugList)
	{
		IndividualPlan individualPlan = new IndividualPlan();
		individualPlan.setId(0);
		individualPlan.setIssuerPlanNumber(planResponse.getIssuerPlanNumber());
		individualPlan.setPlanId(0);
		individualPlan.setName(planResponse.getPlanName());
		individualPlan.setIssuer(planResponse.getIssuerName());
		individualPlan.setIssuerLogo(planResponse.getIssuerLogo());
		individualPlan.setIssuerId(planResponse.getIssuerId());
		if(Plan.PlanLevel.EXPANDEDBRONZE.toString().equals(planResponse.getPlanLevel()))
		{
			individualPlan.setLevel(Plan.PlanLevel.BRONZE.toString());
		}
		else
		{
			individualPlan.setLevel(planResponse.getPlanLevel());
		}
		individualPlan.setPlanType(planResponse.getInsuranceType());
		individualPlan.setNetworkType(planResponse.getNetworkType());
		individualPlan.setCostSharing(planResponse.getCostSharing());
		individualPlan.setSbcUrl(planResponse.getSbcDocUrl());
		individualPlan.setPlanBrochureUrl(planResponse.getBrochure());
		individualPlan.setFormularyUrl(planResponse.getFormularyUrl());
		
		//********
		if(enrollmentdata.getGrossPremiumAmt()!=null) {
			individualPlan.setPremium(enrollmentdata.getGrossPremiumAmt());
		}
		if(enrollmentdata.getPlanLevel()!=null) {
			individualPlan.setLevel(enrollmentdata.getPlanLevel());
		}
		if(enrollmentdata.getCMSPlanID()!=null) {
			individualPlan.setIssuerPlanNumber(enrollmentdata.getCMSPlanID());
		}
		if(enrollmentdata.getCsrLevel()!=null) {
			individualPlan.setCostSharing(enrollmentdata.getCsrLevel());
		}
		if(enrollmentdata.getAptcAmount()!=null) {
			individualPlan.setAptc(enrollmentdata.getAptcAmount());
		}
		
		BigDecimal premiumBeforeCredit = new BigDecimal(Float.toString(enrollmentdata.getGrossPremiumAmt()));
		individualPlan.setPremiumBeforeCredit(enrollmentdata.getGrossPremiumAmt());
		BigDecimal annualPremiumBeforeCredit = premiumBeforeCredit.multiply(BigDecimal.valueOf(12));
		individualPlan.setAnnualPremiumBeforeCredit(annualPremiumBeforeCredit.floatValue());
		
		BigDecimal premiumAfterCredit = new BigDecimal(Float.toString(enrollmentdata.getNetPremiumAmt()));
		BigDecimal annualPremiumAfterCredit = premiumAfterCredit.multiply(BigDecimal.valueOf(12));
		individualPlan.setPremiumAfterCredit(enrollmentdata.getNetPremiumAmt());
		individualPlan.setAnnualPremiumAfterCredit(annualPremiumAfterCredit.floatValue());
		
		Float oopMaxTemp = individualPlan.getOopMax() == null ?  0.0f : individualPlan.getOopMax() ;
		individualPlan.setEstimatedTotalHealthCareCost(BigDecimal.valueOf(individualPlan.getAnnualPremiumAfterCredit()).
				add(BigDecimal.valueOf(oopMaxTemp)).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue());
		
		//***Benefit Coverage Start ***
		Map<String, Map<Integer, Double>> phixPlanBenefits = new HashMap<String, Map<Integer, Double>>();
		for (Entry phixBenefit : PlanDisplayConstants.benefitsMapKey.entrySet()) {
			Map<Integer, Double> phixPlanBenefitCost = new HashMap<Integer, Double>();
			if (planResponse.getPlanBenefits().containsKey(phixBenefit.getValue())) {
				Map<String, String> benefit = planResponse.getPlanBenefits().get(phixBenefit.getValue());
				CalculatorUtil calcUtil = new CalculatorUtil(PlanDisplayConstants.benefitsPriceMapKey);
				String paymentType = calcUtil.getChargeType(benefit);
				double cost = calcUtil.getBenefitPaymentCost(benefit, paymentType,
						phixBenefit.getValue().toString());
				if (calcUtil.isBenefitCovered(benefit)) {
						phixPlanBenefitCost.put(planResponse.getPlanId(), cost);
					}
				}
			

			phixPlanBenefits.put(phixBenefit.getValue().toString(), phixPlanBenefitCost);
		}
		BenefitCoverageCalculator benefitCoverageCalculator = new BenefitCoverageCalculator(PlanDisplayConstants.benefitsMapKey);
		Map<Integer, Map<String, String>> planBenefitsCoverageOutput = benefitCoverageCalculator.calculateBenefitCoverage(phixPlanBenefits);

		Map<String, String> benefitsCoverageOutput = planBenefitsCoverageOutput.get(individualPlan.getPlanId());
		Map<String, String> benefitsCoverage = new HashMap<String, String>();
		if (benefitsCoverageOutput != null && !benefitsCoverageOutput.isEmpty()) {
			for (Map.Entry<String, String> entry : benefitsCoverageOutput.entrySet()) {
				benefitsCoverage.put(PlanDisplayConstants.benefitsKeyReveseMap.get(entry.getKey()), entry.getValue());
			}
		}

		if("Yes".equalsIgnoreCase(individualPlan.getHsa()))
		{
			benefitsCoverage.put("HSA Eligible", "GOOD");
		}
		individualPlan.setBenefitsCoverage(benefitsCoverage);		
		//***Benefit Coverage End ***
		
		//***OOPMAX START
		String netwkType = "inNetworkInd".intern();
		String combinedInOutNetType="combinedInOutNetworkInd".intern();
		if(enrollmentdata.getEnrolleeDataDtoList().size() > 1){
			netwkType = "inNetworkFly".intern();
			combinedInOutNetType="combinedInOutNetworkFly".intern();
		}
		Map<String, Map<String, String>> planCst = planResponse.getPlanCosts();
		
		if(InsuranceType.HEALTH.name().equals(planResponse.getInsuranceType())){
			//setOopMaxForHealth(planCst, netwkType, individualPlan);
			if(planCst!=null){
				if(planCst.get("MAX_OOP_INTG_MED_DRUG") != null && planCst.get("MAX_OOP_INTG_MED_DRUG").get(netwkType) !=null && !(StringUtils.EMPTY.equals(planCst.get("MAX_OOP_INTG_MED_DRUG").get(netwkType)))){
					individualPlan.setIntgMediDrugOopMax(Float.parseFloat(planCst.get("MAX_OOP_INTG_MED_DRUG").get(netwkType)));
				}
				if(individualPlan.getIntgMediDrugOopMax() == null){
					if(planCst.get("MAX_OOP_MEDICAL") != null && planCst.get("MAX_OOP_MEDICAL").get(netwkType) !=null && !(StringUtils.EMPTY.equals(planCst.get("MAX_OOP_MEDICAL").get(netwkType)))){
						individualPlan.setMedicalOopMax(Float.parseFloat(planCst.get("MAX_OOP_MEDICAL").get(netwkType)));
					}
					if(planCst.get("MAX_OOP_DRUG") != null && planCst.get("MAX_OOP_DRUG").get(netwkType) !=null && !(StringUtils.EMPTY.equals(planCst.get("MAX_OOP_DRUG").get(netwkType)))){
						individualPlan.setDrugOopMax(Float.parseFloat(planCst.get("MAX_OOP_DRUG").get(netwkType)));
					}
					float medicalOopMax = individualPlan.getMedicalOopMax()!=null?individualPlan.getMedicalOopMax():0;
					float drugOopMax = individualPlan.getDrugOopMax()!=null?individualPlan.getDrugOopMax():0;
					individualPlan.setOopMax(new BigDecimal(Float.toString(medicalOopMax)).add(new BigDecimal(Float.toString(drugOopMax))).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue());
				}else{
					individualPlan.setOopMax(individualPlan.getIntgMediDrugOopMax());
				}
			}
		}else if (PlanDisplayEnum.InsuranceType.STM.equals(planResponse.getInsuranceType())) {
			float stmOopMax = planResponse.getOopMax() != null ? Float.parseFloat(planResponse.getOopMax().toString()) : new Float(0);
			individualPlan.setOopMax(stmOopMax);
		}else{
			if(planCst !=null){
				if(planCst.get("MAX_OOP_MEDICAL") != null && planCst.get("MAX_OOP_MEDICAL").get(netwkType) !=null && !(StringUtils.EMPTY.equals(planCst.get("MAX_OOP_MEDICAL").get(netwkType)))){
					individualPlan.setOopMax(Float.parseFloat(planCst.get("MAX_OOP_MEDICAL").get(netwkType)));
				}else if(planCst.get("MAX_OOP_MEDICAL") != null && planCst.get("MAX_OOP_MEDICAL").get(combinedInOutNetType) !=null && !(StringUtils.EMPTY.equals(planCst.get("MAX_OOP_MEDICAL").get(combinedInOutNetType)))){
					individualPlan.setOopMax(Float.parseFloat(planCst.get("MAX_OOP_MEDICAL").get(combinedInOutNetType)));
				}
			}
		}
		
		//***OOPMax END
		
		
		//***DOCTOR start
		List<HashMap<String, Object>> providerDataList = new ArrayList<>();
		for (ProviderBean providerBean : providers) {
			HashMap<String, Object> data = new HashMap<>();

			data.put("providerId", providerBean.getId());
			data.put("providerName", providerBean.getName());
			data.put("providerCity", providerBean.getCity());
			data.put("providerState", providerBean.getState());
			data.put("providerSpecialty", providerBean.getSpecialty());
			data.put("networkStatus", "Not Applicable"); //No provider search for previous year enrolled plan
			
			data.put("planSupported", StringUtils.EMPTY);
			data.put("providerType", providerBean.getProviderType());//By default this should come as doctor as set in ProviderBean Shared Module.
			data.put("providerAddress", providerBean.getAddress());
			data.put("providerZip", providerBean.getZip());
			data.put("providerPhone", providerBean.getPhone());
			data.put("groupKey", providerBean.getGroupKey());
			data.put("locationId", providerBean.getLocationId());

			providerDataList.add(data);
		}
		
		individualPlan.setDoctors(providerDataList);
		individualPlan.setDoctorsCount(0);
		individualPlan.setDoctorsSupported("Not Applicable");
		//***DOC End
		
		//**Prescription Start
		if(drugList!=null) {
			for(DrugDTO drug : drugList) {
				drug.setIsDrugCovered("U");   //No prescription search for previous year enrolled plan
				drug.setIsGenericCovered("U"); 
			}
			individualPlan.setPrescriptionResponseList(drugList);
		}
		
		//***Prescription End
		//*********
		
		if (Plan.PlanInsuranceType.HEALTH.toString().equals(planResponse.getInsuranceType()) && planResponse.getTier2util()!=null && !planResponse.getTier2util().isEmpty() && !"0%".equals(planResponse.getTier2util())) {
			individualPlan.setTier2Coverage(Y);
		}	
		if(Plan.PlanInsuranceType.HEALTH.toString().equals(planResponse.getInsuranceType()) && planResponse.getMaxCoinsForSpecialtyDrugs()!= null && !(EMPTY_STRING.equals(planResponse.getMaxCoinsForSpecialtyDrugs()))){
			individualPlan.setMaxCoinseForSpecialtyDrugs(planResponse.getMaxCoinsForSpecialtyDrugs());
		}
		
		if (Plan.PlanInsuranceType.STM.toString().equals(planResponse.getInsuranceType())) {
			individualPlan.setPolicyLength(planResponse.getPolicyLength());
			individualPlan.setCoinsurance(planResponse.getCoinsurance());
			individualPlan.setSeparateDrugDeductible(planResponse.getSeparateDrugDeductible());
			individualPlan.setOutOfNetwkCoverage(planResponse.getOutOfNetwkCoverage());
			individualPlan.setOutOfCountyCoverage(planResponse.getOutOfCountyCoverage());
			individualPlan.setPolicyLimit(planResponse.getPolicyLimit());
			individualPlan.setPolicyLengthUnit(planResponse.getPolicyLengthUnit());
			individualPlan.setOopMaxFamily(planResponse.getOopMaxFamily());
			individualPlan.setOopMaxIndDesc(planResponse.getOopMaxAttr());
			float stmOopMax = planResponse.getOopMax() != null ? Float.parseFloat(planResponse.getOopMax().toString()) : new Float(0);
			individualPlan.setOopMax(stmOopMax);
		}

		if (Plan.PlanInsuranceType.AME.toString().equals(planResponse.getInsuranceType())) {
			Map<String, Map<String, String>> planCosts = new HashMap<String, Map<String, String>>();

			Map<String, String> planCost = new HashMap<String, String>();

			planCost.put("deductibleVal", planResponse.getDeductible());
			planCost.put("deductibleAttrib", planResponse.getDeductibleAttrib());

			planCosts.put(PlanDisplayConstants.AME_DEDUCTIBLE, planCost);

			planCost = new HashMap<String, String>();
			planCost.put("maxBenefitVal", planResponse.getMaxBenefitVal());
			planCost.put("maxBenefitAttrib", planResponse.getMaxBenefitAttrib());
			planCosts.put("MAX_BENEFIT", planCost);

			planCost = new HashMap<String, String>();
			planCost.put("limitationsAndExclusions",planResponse.getLmitationAndExclusions());
			planCosts.put("LIMITATIONS", planCost);
			individualPlan.setPlanCosts(planCosts);

			individualPlan.setPlanBrochureUrl(planResponse.getBrochure());

		}
		
		if (Plan.PlanInsuranceType.VISION.toString().equals(planResponse.getInsuranceType())) {
			Map<String, Map<String, String>> planCosts = new HashMap<String, Map<String, String>>();
			individualPlan.setPlanCosts(planCosts);

			individualPlan.setPlanBrochureUrl(planResponse.getBrochure());
			individualPlan.setOutOfNetwkCoverage(planResponse.getOutOfNetwkCoverage());
			individualPlan.setPolicyLength(planResponse.getPremiumPayment());
			individualPlan.setPlanTier1(planResponse.getPlanBenefits());
		}
		
		if("CT".equalsIgnoreCase(stateCode)) {
			if(StringUtils.isNotBlank(planResponse.getEocDocUrl())) {
				individualPlan.setProviderLink(planResponse.getEocDocUrl());
			}
		}else if (planResponse.getProviderUrl() != null && !(StringUtils.EMPTY.equals(planResponse.getProviderUrl()))) {
			individualPlan.setProviderLink(planResponse.getProviderUrl());
		}
		
		String insuranceType = planResponse.getInsuranceType() != null ? planResponse.getInsuranceType() : StringUtils.EMPTY;
		if(Plan.PlanInsuranceType.HEALTH.toString().equalsIgnoreCase(insuranceType) && planResponse.getEhbPercentage() != null && !planResponse.getEhbPercentage().isEmpty()) {
			individualPlan.setEhbPrecentage(Float.parseFloat(planResponse.getEhbPercentage()));
		}		
		if(Plan.PlanInsuranceType.DENTAL.toString().equalsIgnoreCase(insuranceType) && planResponse.getPediatricDentalComponent() != null && !planResponse.getPediatricDentalComponent().isEmpty()) {
			String pediatricDentalComponent = planResponse.getPediatricDentalComponent();
			pediatricDentalComponent = pediatricDentalComponent.replace("$", StringUtils.EMPTY);
			individualPlan.setEhbPrecentage(Float.parseFloat(pediatricDentalComponent));
		}
		
		if(null!=planResponse.getPlanCosts()){
			calculateEnrolledPlanDeductible(individualPlan, planResponse.getPlanCosts(), enrollmentdata.getEnrolleeDataDtoList().size());
		}
		
		if(planResponse.getExchangeType()!= null && !(StringUtils.EMPTY.equals(planResponse.getExchangeType()))){
			individualPlan.setExchangeType(planResponse.getExchangeType());
		}else{
			individualPlan.setExchangeType("ON");
		}
		
		if (null!=planResponse.getPlanBenefits() && !Plan.PlanInsuranceType.VISION.toString().equalsIgnoreCase(planResponse.getInsuranceType()))
		{
			String[] props = GhixConstants.helathBenefitList;
			if(Plan.PlanInsuranceType.DENTAL.toString().equalsIgnoreCase(insuranceType)){
				props = GhixConstants.dentalBenefitList;
			} else if (Plan.PlanInsuranceType.STM.toString().equalsIgnoreCase(insuranceType)) {
				props = GhixConstants.stmBenefitList;
			} else if (Plan.PlanInsuranceType.AME.toString().equalsIgnoreCase(insuranceType)) {
				props = GhixConstants.ameBenefitList;
			}
			
			//List<Map<String, Map<String,String>>> planBenefitsDisplay = parsePlanBenefits(PlanDisplayEnum.InsuranceType.valueOf(planResponse.getInsuranceType()), planResponse.getPlanBenefits(), individualPlan);
			if (Plan.PlanInsuranceType.AME.toString().equalsIgnoreCase(insuranceType)) {	
				individualPlan.setPlanTier1(parsePlanBenefitsAME(planResponse.getPlanBenefits(), individualPlan, props));
			}else{
				individualPlan.setPlanTier1(parsePlanBenefitsTier1(planResponse.getPlanBenefits(), individualPlan, props));
			}
			individualPlan.setPlanTier2(parsePlanBenefitsTier2(planResponse.getPlanBenefits(), props));
			individualPlan.setPlanOutNet(parsePlanBenefitsOutNet(planResponse.getPlanBenefits(), props));
			individualPlan.setPlanAppliesToDeduct(parseAppliesToDeduct(planResponse.getPlanBenefits(), props));
			individualPlan.setNetwkException(parseNetwkExcp(PlanDisplayEnum.InsuranceType.valueOf(planResponse.getInsuranceType()), planResponse.getPlanBenefits(), props));
			individualPlan.setMissingDentalCovg(parseBenefitCovg(planResponse.getPlanBenefits(), props));
			individualPlan.setBenefitExplanation(parseBenefitExpl(planResponse.getPlanBenefits(), props));
		}
		
		if(null!=planResponse.getPlanCosts() && !Plan.PlanInsuranceType.AME.toString().equals(planResponse.getInsuranceType())
				&& !Plan.PlanInsuranceType.VISION.toString().equals(planResponse.getInsuranceType())){
			Map<String, Map<String, String>> planCosts = new HashMap<String, Map<String, String>>();
			if(planResponse.getPlanCosts() != null) {
				planCosts = planResponse.getPlanCosts();
			}
			if(Plan.PlanInsuranceType.STM.toString().equals(planResponse.getInsuranceType())) {
				Map<String, String> planCost = new HashMap<String, String>();
				planCost.put("limitationsAndExclusions",planResponse.getLmitationAndExclusions());
				planCosts.put("LIMITATIONS", planCost);
			}
			individualPlan.setPlanCosts(planCosts);
			individualPlan.setOptionalDeductible(planResponse.getOptionalDeductible());
		}
		
		individualPlan.setHsa(planResponse.getHsa());
		if(planResponse.getIssuerQualityRating() != null){
			individualPlan.setQualityRating(planResponse.getIssuerQualityRating().get("QualityRating"));
			individualPlan.setIssuerQualityRating(planResponse.getIssuerQualityRating());
			individualPlan.setOverAllQuality(planResponse.getIssuerQualityRating().get("GlobalRating"));
		}
		if(planResponse.getSbcScenarioDTO() != null)
		{
			SbcScenarioDTO sbcDto = planResponse.getSbcScenarioDTO();
			Integer havingBabySBC = getSBCScenarioValues(sbcDto.getBabyCoinsurance(), sbcDto.getBabyCopay(), sbcDto.getBabyDeductible(), sbcDto.getBabyLimit());
			Integer havingDiabetesSBC = getSBCScenarioValues(sbcDto.getDiabetesCoinsurance(), sbcDto.getDiabetesCopay(), sbcDto.getDiabetesDeductible(), sbcDto.getDiabetesLimit());
			Integer simpleFractureSBC = getSBCScenarioValues(sbcDto.getFractureCoinsurance(), sbcDto.getFractureCopay(), sbcDto.getFractureDeductible(), sbcDto.getFractureLimit());
			individualPlan.setHavingBabySBC(havingBabySBC);
			individualPlan.setHavingDiabetesSBC(havingDiabetesSBC);
			individualPlan.setSimpleFractureSBC(simpleFractureSBC);
		}
		return individualPlan;
	}
	
	
	public static void calculateEnrolledPlanDeductible(IndividualPlan individualPlan, Map<String, Map<String, String>> planCost,int  personCount){
		try {
			
			if (PlanDisplayEnum.InsuranceType.STM.equals(PlanDisplayEnum.InsuranceType.valueOf(individualPlan.getPlanType()))) {
				String inNetworkIndv = PlanDisplayConstants.STM_IN_NETWORK_IND;
				Map<String, String> stmDeductible = planCost.get(PlanDisplayConstants.STM_DEDUCTIBLE);

				if (stmDeductible != null && (stmDeductible.get(inNetworkIndv) != null && !(StringUtils.EMPTY.equals(stmDeductible.get(inNetworkIndv)))) ) {
					//if (stmDeductible.get(inNetworkIndv) != null && !(stmDeductible.get(inNetworkIndv).equals(StringUtils.EMPTY))) {
						individualPlan.setDeductible((int)Double.parseDouble(stmDeductible.get(inNetworkIndv)));
					//}
				}
				return;
			}
			if (PlanDisplayEnum.InsuranceType.AME.equals(PlanDisplayEnum.InsuranceType.valueOf(individualPlan.getPlanType()))) {
				Map<String, String> ameDeductible = planCost.get(PlanDisplayConstants.AME_DEDUCTIBLE);
				if (ameDeductible != null && (ameDeductible.get("deductibleVal") != null && !("".equals(ameDeductible.get("deductibleVal")))) ) {
						individualPlan.setDeductible((int)Double.parseDouble(ameDeductible.get("deductibleVal") ));
				}
				return;
			}
			
			Map<String, String> deductibleIntgMedDrug = planCost.get(PlanDisplayConstants.DEDUCTIBLE_INTG_MED_DRUG);
			Map<String, String> deductibleMedical = planCost.get(PlanDisplayConstants.DEDUCTIBLE_MEDICAL);
			Map<String, String> deductibleDrug = planCost.get(PlanDisplayConstants.DEDUCTIBLE_DRUG);
			
			String inNetworkFlyOrIndv = personCount> 1?PlanDisplayConstants.IN_NETWORK_FLY:PlanDisplayConstants.IN_NETWORK_IND;
			String combinedNetworkFlyOrIndv = personCount> 1?PlanDisplayConstants.COMBINED_INOUTNETWORK_FLY:PlanDisplayConstants.COMBINED_INOUTNETWORK_IND;
			
			if (PlanDisplayEnum.InsuranceType.DENTAL.equals(PlanDisplayEnum.InsuranceType.valueOf(individualPlan.getPlanType())) && !"CA".equalsIgnoreCase(stateCode)) {
				inNetworkFlyOrIndv = PlanDisplayConstants.IN_NETWORK_IND;
				combinedNetworkFlyOrIndv = PlanDisplayConstants.COMBINED_INOUTNETWORK_IND;
			}
			
			if(deductibleIntgMedDrug !=null){
				if(deductibleIntgMedDrug.get(inNetworkFlyOrIndv) !=null && !(StringUtils.EMPTY.equals(deductibleIntgMedDrug.get(inNetworkFlyOrIndv)))){
					individualPlan.setIntgMediDrugDeductible((int)Double.parseDouble(deductibleIntgMedDrug.get(inNetworkFlyOrIndv)));
				}else if(deductibleIntgMedDrug.get(combinedNetworkFlyOrIndv) !=null && !(StringUtils.EMPTY.equals(deductibleIntgMedDrug.get(combinedNetworkFlyOrIndv)))){
					individualPlan.setIntgMediDrugDeductible((int)Double.parseDouble(deductibleIntgMedDrug.get(combinedNetworkFlyOrIndv)));
				}
			}
			if(individualPlan.getIntgMediDrugDeductible()==null){					
				if(deductibleMedical !=null){
					if(deductibleMedical.get(inNetworkFlyOrIndv) !=null && !(StringUtils.EMPTY.equals(deductibleMedical.get(inNetworkFlyOrIndv)) )){
						individualPlan.setMedicalDeductible((int)Double.parseDouble(deductibleMedical.get(inNetworkFlyOrIndv)));
					}else if(deductibleMedical.get(combinedNetworkFlyOrIndv) !=null && !(StringUtils.EMPTY.equals(deductibleMedical.get(combinedNetworkFlyOrIndv)) )){
						individualPlan.setMedicalDeductible((int)Double.parseDouble(deductibleMedical.get(combinedNetworkFlyOrIndv)));
					}
				}					
				if(deductibleDrug !=null){
					if(deductibleDrug.get(inNetworkFlyOrIndv) !=null && !(StringUtils.EMPTY.equals(deductibleDrug.get(inNetworkFlyOrIndv)))){
						individualPlan.setDrugDeductible((int)Double.parseDouble(deductibleDrug.get(inNetworkFlyOrIndv)));
					}else if(deductibleDrug.get(combinedNetworkFlyOrIndv) !=null && !(StringUtils.EMPTY.equals(deductibleDrug.get(combinedNetworkFlyOrIndv)) )){
						individualPlan.setDrugDeductible((int)Double.parseDouble(deductibleDrug.get(combinedNetworkFlyOrIndv)));
					}
				}
				int medicalDeductible = individualPlan.getMedicalDeductible()!=null?individualPlan.getMedicalDeductible():0;
				int drugDeductible = individualPlan.getDrugDeductible()!=null?individualPlan.getDrugDeductible():0;
				individualPlan.setDeductible(medicalDeductible+drugDeductible);
			}else{
				individualPlan.setDeductible(individualPlan.getIntgMediDrugDeductible() + 1);
			}

		} catch (Exception e) {
			putLoggerStmt(LOGGER, LogLevel.ERROR, "<----In calculateEnrolledPlanDeductible method---->"+e.getMessage(), e, false);
		}
	}
	
				
}
