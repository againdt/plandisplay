package com.getinsured.plandisplay.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.UriUtils;

import com.getinsured.plandisplay.service.GoodRxService;

public final class GoodRxUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GoodRxUtil.class);
	private static final String ARGS_SEP = "&";
	private static final String KEYVAL_SEP = "=";
	private static final String HMAC_SHA_256 = "HmacSHA256";
	
	private GoodRxUtil() {
		super();
	}

	/**
	 * Splits a String into key value pairs
	 * 
	 * @param args
	 * @return
	 */
	public static String[] parseArgs(String args) {
		if(args==null || args.isEmpty()){
			return null;
		}
		String[] result = args.split(ARGS_SEP);
		
		return result;
	}
	
	/**
	 * Splits key value pair
	 * @param args
	 * @return
	 */
	public static String[] parseKeyVal(String args){
		if(args==null || args.isEmpty()) {
			return null;
		}
		
		String[] keyVal = args.split(KEYVAL_SEP);
		
		if(keyVal.length!=2) {
			return null;
		}
		
		try {
			keyVal[1] = UriUtils.encodeQueryParam(keyVal[1], "UTF-8");
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("Exception occured while encoding query parameters",e);
		}
		return keyVal;
	}
	
	/**
	 * Encrypt query string
	 * @param queryStr
	 * @return
	 */
	public static byte[] encryptQueryString(String queryStr){
		return hmacSHA256(queryStr, GoodRxService.API_SECRET);
	}
	
	/**
	 * 
	 * @param value
	 * @param key
	 * @return
	 */
	public static byte[] hmacSHA256(String value, String key){
		try {
            byte[] keyBytes = key.getBytes();           
            SecretKeySpec signingKey = new SecretKeySpec(keyBytes, HMAC_SHA_256);

            Mac mac = Mac.getInstance(HMAC_SHA_256);
            mac.init(signingKey);

            byte[] rawHmac = mac.doFinal(value.getBytes());

            return rawHmac;
            //byte[] hexBytes = new Hex().encode(rawHmac);

            //return new String(hexBytes, "UTF-8");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
	}
	
	/**
	 * 
	 * @param queryStr
	 * @return
	 */
	public static String encodeBase64(byte[] queryStr){
		byte[] encodedBytes = Base64.encodeBase64(queryStr);
		String encodedBuffer = new String(encodedBytes);
		encodedBuffer = encodedBuffer.replace("+", "_");
		encodedBuffer = encodedBuffer.replace("/", "_");
		try {
			return URLEncoder.encode(encodedBuffer, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("ERROR encoding :" + queryStr);
			return null;
		}
	}
	
	/**
	 * 
	 * @param queryStr
	 * @return
	 */
	public static String createSignature(String queryStr){
		byte[] signedStr = encryptQueryString(queryStr);
		return encodeBase64(signedStr);
	}
}
