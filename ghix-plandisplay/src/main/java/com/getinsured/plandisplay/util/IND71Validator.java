package com.getinsured.plandisplay.util;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.getinsured.hix.dto.plandisplay.ind71g.IND71Enums;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71Enums.EnrollmentType;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71Enums.RoleType;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GEnrollment;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GHouseHoldContact;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GMember;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GResponsiblePerson;
import com.getinsured.hix.dto.plandisplay.ind71g.Ind71GRequest;

@Component
public class IND71Validator extends IND71BaseValidator implements Validator {
	
	@Override
	public boolean supports(Class<?> clazz) {
		
		return Ind71GRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Ind71GRequest request = null;
		if(Ind71GRequest.class.isAssignableFrom(target.getClass()))
		{
			request = (Ind71GRequest) target;
		}
		validateRequestElements(errors, request);
		
		validateResponsiblePerson(request.getResponsiblePerson(),errors);
		
		validateHouseholdContact(request.getHouseHoldContact(),errors);
		Set<String> enrollmentIds = new HashSet<String>(3);
		Set<String> healthMemberIds = new HashSet<String>(5);
		
		int dentalCounter = 0;
		int enrollmentCounter = 0;
		int memberCounter = 0;
		
		for(IND71GEnrollment enrollment : request.getEnrollments())
		{
			//iND71GEnrollmentValidator.validate(enrollment, errors);
			int memCount = 0;
			validateEnrollment(enrollment,errors,enrollmentCounter);
			for(IND71GMember member : enrollment.getMembers())
			{
				validateMember(member,errors,enrollmentCounter,memCount);
				memCount++;
			}
			if(!enrollmentIds.add(enrollment.getEnrollmentId()))
			{
				errors.rejectValue("enrollments["+enrollmentCounter+"].enrollmentId", "enrollments["+enrollmentCounter+"].enrollmentId","Duplicate enrollmentIds present.");
			}
			if("Health".equals(enrollment.getInsuranceType()))
			{
				for(IND71GMember member : enrollment.getMembers())
				{
					if(!healthMemberIds.add(member.getMemberId()))
					{
						errors.rejectValue("enrollments["+enrollmentCounter+"].members["+memberCounter+"].memberId", "enrollments["+enrollmentCounter+"].members["+memberCounter+"].memberId","Duplicate memberIds present for health enrollments.");
					}
					validateCustodialParent(member,errors, enrollmentCounter, memberCounter);
					memberCounter++;
				}
				memberCounter = 0;
			}
			else
			{
				dentalCounter++;
			}
			
		}
		if(dentalCounter>1)
		{
			errors.rejectValue("enrollments["+enrollmentCounter+"].enrollmentId", "enrollments["+enrollmentCounter+"].enrollmentId","More than one dental enrollments present.");
		}
		enrollmentCounter++;
	}

	private void validateMember(IND71GMember member, Errors errors, int enrollmentCounter, int memCount) {
		String fieldName = null;
		if(StringUtils.isEmpty(member.getMemberId()))
		{
			errors.rejectValue("enrollments["+enrollmentCounter+"].members["+memCount+"].memberId", "enrollments["+enrollmentCounter+"].members["+memCount+"].memberId","enrollmentType"+IND71Enums.REQUIRED_FIELD);
		}
		if(!StringUtils.isEmpty(member.getMaintenanceReasonCode()) && !getMrcRegex().matcher(member.getMaintenanceReasonCode()).matches())
		{
			fieldName = "enrollments["+enrollmentCounter+"].members["+memCount+"].maintenanceReasonCode";
			errors.rejectValue(fieldName, fieldName,fieldName + IND71Enums.INVALID_REGEX_ERROR);
		}
		if(!StringUtils.isEmpty(member.getNewPersonFlag()) && !getYesNoRegex().matcher(member.getNewPersonFlag()).matches())
		{
			fieldName = "enrollments["+enrollmentCounter+"].members["+memCount+"].newPersonFlag";
			errors.rejectValue(fieldName, fieldName,fieldName + IND71Enums.INVALID_REGEX_ERROR);
		}
		if(!StringUtils.isEmpty(member.getDisenrollMemberFlag()) && !getYesNoRegex().matcher(member.getDisenrollMemberFlag()).matches())
		{
			fieldName = "enrollments["+enrollmentCounter+"].members["+memCount+"].disenrollMemberFlag";
			errors.rejectValue(fieldName, fieldName,fieldName + IND71Enums.INVALID_REGEX_ERROR);
		}
		if(!StringUtils.isEmpty(member.getDeathDate()) && !getDateRegex().matcher(member.getDeathDate()).matches())
		{
			fieldName = "enrollments["+enrollmentCounter+"].members["+memCount+"].deathDate";
			errors.rejectValue(fieldName, fieldName,fieldName + IND71Enums.INVALID_REGEX_ERROR);
		}
		if(!StringUtils.isEmpty(member.getTerminationDate()) && !getDateRegex().matcher(member.getTerminationDate()).matches())
		{
			fieldName = "enrollments["+enrollmentCounter+"].members["+memCount+"].terminationDate";
			errors.rejectValue(fieldName, fieldName,fieldName + IND71Enums.INVALID_REGEX_ERROR);
		}
		if(!StringUtils.isEmpty(member.getTerminationReasonCode()) && !getMrcRegex().matcher(member.getTerminationReasonCode()).matches())
		{
			fieldName = "enrollments["+enrollmentCounter+"].members["+memCount+"].terminationReasonCode";
			errors.rejectValue(fieldName, fieldName,fieldName + IND71Enums.INVALID_REGEX_ERROR);
		}
		if(!StringUtils.isEmpty(member.getResponsiblePersonRelationship()) && !getRelationRegex().matcher(member.getResponsiblePersonRelationship()).matches())
		{
			fieldName = "enrollments["+enrollmentCounter+"].members["+memCount+"].responsiblePersonRelationship";
			errors.rejectValue(fieldName, fieldName,fieldName + IND71Enums.INVALID_REGEX_ERROR);
		}
		if(!StringUtils.isEmpty(member.getDob()) && !getDateRegex().matcher(member.getDob()).matches())
		{
			fieldName = "enrollments["+enrollmentCounter+"].members["+memCount+"].dob";
			errors.rejectValue(fieldName, fieldName,fieldName + IND71Enums.INVALID_REGEX_ERROR);
		}
		if(!StringUtils.isEmpty(member.getTobacco()) && !getYesNoRegex().matcher(member.getTobacco()).matches())
		{
			fieldName = "enrollments["+enrollmentCounter+"].members["+memCount+"].tobacco";
			errors.rejectValue(fieldName, fieldName,fieldName + IND71Enums.INVALID_REGEX_ERROR);
		}
		validateStateAndLength(errors, member.getHomeState(), "enrollments["+enrollmentCounter+"].members["+memCount+"].homeState");
		validateZipAndLength(errors, member.getHomeZip(), "enrollments["+enrollmentCounter+"].members["+memCount+"].homeZip");
		if(!StringUtils.isEmpty(member.getCountyCode()))
		{
			fieldName = "enrollments["+enrollmentCounter+"].members["+memCount+"].countyCode";
			validateLength(member.getCountyCode(), fieldName, IND71Enums.COUNTY_MIN_LENGTH, IND71Enums.COUNTY_MAX_LENGTH, errors);
			if(!getCountyRegex().matcher(member.getCountyCode()).matches())
			{
				errors.rejectValue(fieldName, fieldName,fieldName + IND71Enums.INVALID_REGEX_ERROR);
			}
		}
		validateNameAndLength(errors, member.getFirstName(), "enrollments["+enrollmentCounter+"].members["+memCount+"].firstName");
		validateNameAndLength(errors, member.getMiddleName(), "enrollments["+enrollmentCounter+"].members["+memCount+"].middleName");
		validateNameAndLength(errors, member.getLastName(), "enrollments["+enrollmentCounter+"].members["+memCount+"].lastName");

		if(!StringUtils.isEmpty(member.getSuffix()))
		{
			validateLength(member.getSuffix(), "enrollments["+enrollmentCounter+"].members["+memCount+"].suffix", IND71Enums.SUFFIX_MIN_LENGTH, IND71Enums.SUFFIX_MAX_LENGTH, errors);
		}
		if(!StringUtils.isEmpty(member.getSsn()))
		{
			fieldName = "enrollments["+enrollmentCounter+"].members["+memCount+"].ssn";
			validateLength(member.getSsn(), fieldName, IND71Enums.SSN_MIN_LENGTH, IND71Enums.SSN_MAX_LENGTH, errors);
			if(!getSsnRegex().matcher(member.getSsn()).matches())
			{
				errors.rejectValue(fieldName, fieldName, fieldName + IND71Enums.INVALID_REGEX_ERROR);
			}
		}
		validatePhoneAndLength(errors, member.getPrimaryPhone(), "enrollments["+enrollmentCounter+"].members["+memCount+"].primaryPhone");
		validatePhoneAndLength(errors, member.getSecondaryPhone(), "enrollments["+enrollmentCounter+"].members["+memCount+"].secondaryPhone");
		validatePhoneAndLength(errors, member.getPreferredPhone(), "enrollments["+enrollmentCounter+"].members["+memCount+"].preferredPhone");
		validateEmail(errors, member.getPreferredEmail(), "enrollments["+enrollmentCounter+"].members["+memCount+"].preferredEmail");
		validateAdddressAndLength(errors, member.getHomeAddress1(), "enrollments["+enrollmentCounter+"].members["+memCount+"].homeAddress1");
		validateAdddressAndLength(errors, member.getHomeAddress2(), "enrollments["+enrollmentCounter+"].members["+memCount+"].homeAddress2");
		validateCityAndLength(errors, member.getHomeCity(), "enrollments["+enrollmentCounter+"].members["+memCount+"].homeCity");
		if(!StringUtils.isEmpty(member.getGenderCode()))
		{
			fieldName = "enrollments["+enrollmentCounter+"].members["+memCount+"].genderCode";
			if(!getGenderRegex().matcher(member.getGenderCode()).matches())
			{
				errors.rejectValue(fieldName, fieldName, fieldName + IND71Enums.INVALID_REGEX_ERROR);
			}
		}
		if(!StringUtils.isEmpty(member.getMaritalStatusCode()))
		{
			fieldName = "enrollments["+enrollmentCounter+"].members["+memCount+"].maritalStatusCode";
			if(!getMaritalStatusRegex().matcher(member.getMaritalStatusCode()).matches())
			{
				errors.rejectValue(fieldName, fieldName, fieldName + IND71Enums.INVALID_REGEX_ERROR);
			}
		}
		if(!StringUtils.isEmpty(member.getCitizenshipStatusCode()))
		{
			fieldName = "enrollments["+enrollmentCounter+"].members["+memCount+"].citizenshipStatusCode";
			if(!getCitizenShpRegex().matcher(member.getCitizenshipStatusCode()).matches())
			{
				errors.rejectValue(fieldName, fieldName, fieldName + IND71Enums.INVALID_REGEX_ERROR);
			}
		}
		if(!StringUtils.isEmpty(member.getSpokenLanguageCode()))
		{
			fieldName = "enrollments["+enrollmentCounter+"].members["+memCount+"].spokenLanguageCode";
			if(!getSpkLangRegex().matcher(member.getSpokenLanguageCode()).matches())
			{
				errors.rejectValue(fieldName, fieldName, fieldName + IND71Enums.INVALID_REGEX_ERROR);
			}
		}
		if(!StringUtils.isEmpty(member.getWrittenLanguageCode()))
		{
			fieldName = "enrollments["+enrollmentCounter+"].members["+memCount+"].writtenLanguageCode";
			if(!getWrtLangRegex().matcher(member.getWrittenLanguageCode()).matches())
			{
				errors.rejectValue(fieldName, fieldName, fieldName + IND71Enums.INVALID_REGEX_ERROR);
			}
		}
		validateAdddressAndLength(errors, member.getMailingAddress1(), "enrollments["+enrollmentCounter+"].members["+memCount+"].mailingAddress1");
		validateAdddressAndLength(errors, member.getMailingAddress2(), "enrollments["+enrollmentCounter+"].members["+memCount+"].mailingAddress2");
		validateCityAndLength(errors, member.getMailingCity(), "enrollments["+enrollmentCounter+"].members["+memCount+"].mailingCity");
		validateStateAndLength(errors, member.getMailingState(), "enrollments["+enrollmentCounter+"].members["+memCount+"].mailingState");
		validateZipAndLength(errors, member.getMailingZip(), "enrollments["+enrollmentCounter+"].members["+memCount+"].mailingZip");

		if(!StringUtils.isEmpty(member.getCatastrophicEligible()) && !getYesNoRegex().matcher(member.getCatastrophicEligible()).matches())
		{
			fieldName = "enrollments["+enrollmentCounter+"].members["+memCount+"].catastrophicEligible";
			errors.rejectValue(fieldName, fieldName,fieldName + IND71Enums.INVALID_REGEX_ERROR);
		}
		validateNameAndLength(errors, member.getCustodialParentFirstName(), "enrollments["+enrollmentCounter+"].members["+memCount+"].custodialParentFirstName");
		validateNameAndLength(errors, member.getCustodialParentMiddleName(), "enrollments["+enrollmentCounter+"].members["+memCount+"].custodialParentMiddleName");
		validateNameAndLength(errors, member.getCustodialParentLastName(), "enrollments["+enrollmentCounter+"].members["+memCount+"].custodialParentLastName");
		if(!StringUtils.isEmpty(member.getCustodialParentSuffix()))
		{
			validateLength(member.getCustodialParentSuffix(), "enrollments["+enrollmentCounter+"].members["+memCount+"].custodialParentSuffix", IND71Enums.SUFFIX_MIN_LENGTH, IND71Enums.SUFFIX_MAX_LENGTH, errors);
		}

		if(!StringUtils.isEmpty(member.getCustodialParentSsn()))
		{
			validateLength(member.getCustodialParentSsn(), "enrollments["+enrollmentCounter+"].members["+memCount+"].custodialParentSsn", IND71Enums.SSN_MIN_LENGTH, IND71Enums.SSN_MAX_LENGTH, errors);
			if(!getSsnRegex().matcher(member.getCustodialParentSsn()).matches())
			{
				errors.rejectValue("enrollments["+enrollmentCounter+"].members["+memCount+"].custodialParentSsn", "enrollments["+enrollmentCounter+"].members["+memCount+"].custodialParentSsn","enrollments["+enrollmentCounter+"].members["+memCount+"].custodialParentSsn" + IND71Enums.INVALID_REGEX_ERROR);
			}
		}
		validatePhoneAndLength(errors, member.getCustodialParentPrimaryPhone(), "enrollments["+enrollmentCounter+"].members["+memCount+"].custodialParentPrimaryPhone");
		validatePhoneAndLength(errors, member.getCustodialParentSecondaryPhone(), "enrollments["+enrollmentCounter+"].members["+memCount+"].custodialParentSecondaryPhone");
		validatePhoneAndLength(errors, member.getCustodialParentPreferredPhone(), "enrollments["+enrollmentCounter+"].members["+memCount+"].custodialParentPreferredPhone");
		validateEmail(errors, member.getCustodialParentPreferredEmail(), "enrollments["+enrollmentCounter+"].members["+memCount+"].custodialParentPreferredEmail");
		validateAdddressAndLength(errors, member.getCustodialParentHomeAddress1(), "enrollments["+enrollmentCounter+"].members["+memCount+"].custodialParentHomeAddress1");
		validateAdddressAndLength(errors, member.getCustodialParentHomeAddress2(), "enrollments["+enrollmentCounter+"].members["+memCount+"].custodialParentHomeAddress2");
		validateCityAndLength(errors, member.getCustodialParentHomeCity(), "enrollments["+enrollmentCounter+"].members["+memCount+"].custodialParentHomeCity");
		validateStateAndLength(errors, member.getCustodialParentHomeState(), "enrollments["+enrollmentCounter+"].members["+memCount+"].custodialParentHomeState");
		validateZipAndLength(errors, member.getCustodialParentHomeZip(), "enrollments["+enrollmentCounter+"].members["+memCount+"].custodialParentHomeZip");

		if(!StringUtils.isEmpty(member.getFinancialHardshipExemption()) && !getYesNoRegex().matcher(member.getFinancialHardshipExemption()).matches())
		{
			fieldName = "enrollments["+enrollmentCounter+"].members["+memCount+"].financialHardshipExemption";
			errors.rejectValue(fieldName, fieldName,fieldName + IND71Enums.INVALID_REGEX_ERROR);
		}

		if(!StringUtils.isEmpty(member.getChildOnlyPlanEligibile()) && !getYesNoRegex().matcher(member.getChildOnlyPlanEligibile()).matches())
		{
			fieldName = "enrollments["+enrollmentCounter+"].members["+memCount+"].childOnlyPlanEligibile";
			errors.rejectValue(fieldName, fieldName,fieldName + IND71Enums.INVALID_REGEX_ERROR);
		}
	}

	private void validateHouseholdContact(IND71GHouseHoldContact houseHoldContact, Errors errors) {
		validateLength(houseHoldContact.getHouseHoldContactId(), "houseHoldContact.houseHoldContactId", 1, 80, errors);
		validateNameAndLength(errors, houseHoldContact.getHouseHoldContactFirstName(), "houseHoldContact.houseHoldContactFirstName");
		validateNameAndLength(errors, houseHoldContact.getHouseHoldContactMiddleName(), "houseHoldContact.houseHoldContactMiddleName");
		validateNameAndLength(errors, houseHoldContact.getHouseHoldContactLastName(), "houseHoldContact.houseHoldContactLastName");
		if(!StringUtils.isEmpty(houseHoldContact.getHouseHoldContactSuffix()))
		{
			validateLength(houseHoldContact.getHouseHoldContactSuffix(), "houseHoldContact.houseHoldContactSuffix", IND71Enums.SUFFIX_MIN_LENGTH, IND71Enums.SUFFIX_MAX_LENGTH, errors);
		}
		if(!StringUtils.isEmpty(houseHoldContact.getHouseHoldContactFederalTaxIdNumber()))
		{
			validateLength(houseHoldContact.getHouseHoldContactFederalTaxIdNumber(), "houseHoldContact.getHouseHoldContactFederalTaxIdNumber", IND71Enums.SSN_MIN_LENGTH, IND71Enums.SSN_MAX_LENGTH, errors);
			if(!getSsnRegex().matcher(houseHoldContact.getHouseHoldContactFederalTaxIdNumber()).matches())
			{
				errors.rejectValue("houseHoldContact.getHouseHoldContactFederalTaxIdNumber", "houseHoldContact.getHouseHoldContactFederalTaxIdNumber","houseHoldContact.getHouseHoldContactFederalTaxIdNumber" + IND71Enums.INVALID_REGEX_ERROR);
			}
		}
		validatePhoneAndLength(errors, houseHoldContact.getHouseHoldContactPrimaryPhone(), "houseHoldContact.getHouseHoldContactPrimaryPhone");
		validatePhoneAndLength(errors, houseHoldContact.getHouseHoldContactSecondaryPhone(), "houseHoldContact.getHouseHoldContactSecondaryPhone");
		validatePhoneAndLength(errors, houseHoldContact.getHouseHoldContactPreferredPhone(), "houseHoldContact.getHouseHoldContactPreferredPhone");
		validateEmail(errors, houseHoldContact.getHouseHoldContactPreferredEmail(), "houseHoldContact.getHouseHoldContactPreferredEmail");
		validateAdddressAndLength(errors, houseHoldContact.getHouseHoldContactHomeAddress1(), "houseHoldContact.getHouseHoldContactHomeAddress1");
		validateAdddressAndLength(errors, houseHoldContact.getHouseHoldContactHomeAddress2(), "houseHoldContact.getHouseHoldContactHomeAddress2");
		validateCityAndLength(errors, houseHoldContact.getHouseHoldContactHomeCity(), "houseHoldContact.getHouseHoldContactHomeCity");
		validateStateAndLength(errors, houseHoldContact.getHouseHoldContactHomeState(), "houseHoldContact.getHouseHoldContactHomeState");
		validateZipAndLength(errors, houseHoldContact.getHouseHoldContactHomeZip(), "houseHoldContact.getHouseHoldContactHomeZip");
	}

	public void validateRequestElements(Errors errors, Ind71GRequest request) {
		if(!StringUtils.isEmpty(request.getUserRoleType()) && !EnumUtils.isValidEnum(RoleType.class, request.getUserRoleType()))
		{
			errors.rejectValue("userRoleType", "userRoleType","Invalid or blank UserRoleType.");
		}

		if(!StringUtils.isEmpty(request.getUserRoleType()))
		{
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userRoleId", "error.userRoleId", "UserRoleType is provided but UserRoleId is absent.");
		}
		if(!StringUtils.isEmpty(request.getEnrollmentType()) && !EnumUtils.isValidEnum(EnrollmentType.class, request.getEnrollmentType()))
		{
			errors.rejectValue("enrollmentType", "enrollmentType","enrollmentType"+IND71Enums.REQUIRED_FIELD);
		}
		if(!StringUtils.isEmpty(request.getAllowChangePlan()) && !getYesNoRegex().matcher(request.getAllowChangePlan()).matches() )
		{
			errors.rejectValue("allowChangePlan", "allowChangePlan","allowChangePlan"+IND71Enums.INVALID_REGEX_ERROR);
		}

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "applicationId", "error.applicationId", "applicationId"+IND71Enums.REQUIRED_FIELD);
		String applicationId = request.getApplicationId();
		validateLength(applicationId,"applicationId", 1, 10, errors);
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "householdCaseId", "error.householdCaseId", "householdCaseId"+IND71Enums.REQUIRED_FIELD);
	}

	private void validateResponsiblePerson(IND71GResponsiblePerson responsiblePerson, Errors errors) {
		if(!StringUtils.isEmpty(responsiblePerson.getResponsiblePersonId()))
		{
			validateLength(responsiblePerson.getResponsiblePersonId(), "responsiblePerson.responsiblePersonId", IND71Enums.RESPONSIBLE_PERSON_MIN_LENGTH, IND71Enums.RESPONSIBLE_PERSON_MAX_LENGTH, errors);
			if(!getRespPersonRegex().matcher(responsiblePerson.getResponsiblePersonId()).matches())
			{
				errors.rejectValue("responsiblePerson.responsiblePersonId", "responsiblePersonId","responsiblePersonId" + IND71Enums.INVALID_REGEX_ERROR);
			}
		}
		validateNameAndLength(errors, responsiblePerson.getResponsiblePersonFirstName(), "responsiblePerson.responsiblePersonFirstName");
		validateNameAndLength(errors, responsiblePerson.getResponsiblePersonMiddleName(), "responsiblePerson.responsiblePersonMiddleName");
		validateNameAndLength(errors, responsiblePerson.getResponsiblePersonMiddleName(), "responsiblePerson.responsiblePersonMiddleName");
		validateNameAndLength(errors, responsiblePerson.getResponsiblePersonLastName(), "responsiblePersonLastName");
		validateLength(responsiblePerson.getResponsiblePersonSuffix(), "responsiblePerson.responsiblePersonSuffix", IND71Enums.SUFFIX_MIN_LENGTH, IND71Enums.SUFFIX_MAX_LENGTH, errors);
		validateLength(responsiblePerson.getResponsiblePersonSsn(), "responsiblePerson.responsiblePersonSsn", IND71Enums.SSN_MIN_LENGTH, IND71Enums.SSN_MAX_LENGTH, errors);
		if(!StringUtils.isEmpty(responsiblePerson.getResponsiblePersonSsn()))
		{
			if(!getSsnRegex().matcher(responsiblePerson.getResponsiblePersonSsn()).matches())
			{
				errors.rejectValue("responsiblePerson.responsiblePersonSsn", "responsiblePerson.responsiblePersonSsn","responsiblePersonSsn" + IND71Enums.INVALID_REGEX_ERROR);
			}
		}
		validatePhoneAndLength(errors, responsiblePerson.getResponsiblePersonPrimaryPhone(), "responsiblePerson.responsiblePersonPrimaryPhone");
		validatePhoneAndLength(errors, responsiblePerson.getResponsiblePersonSecondaryPhone(), "responsiblePerson.responsiblePersonSecondaryPhone");
		validatePhoneAndLength(errors, responsiblePerson.getResponsiblePersonPreferredPhone(), "responsiblePerson.responsiblePersonPreferredPhone");
		validateEmail(errors, responsiblePerson.getResponsiblePersonPreferredEmail(), "responsiblePerson.responsiblePersonPreferredEmail");
		validateAdddressAndLength(errors, responsiblePerson.getResponsiblePersonHomeAddress1(), "responsiblePerson.responsiblePersonHomeAddress1");
		validateAdddressAndLength(errors, responsiblePerson.getResponsiblePersonHomeAddress2(), "responsiblePerson.responsiblePersonHomeAddress2");
		validateCityAndLength(errors, responsiblePerson.getResponsiblePersonHomeCity(), "responsiblePerson.responsiblePersonHomeCity");
		validateStateAndLength(errors, responsiblePerson.getResponsiblePersonHomeState(), "responsiblePerson.responsiblePersonHomeState");
		validateZipAndLength(errors, responsiblePerson.getResponsiblePersonHomeZip(), "responsiblePerson.responsiblePersonHomeZip");
		if(!StringUtils.isEmpty(responsiblePerson.getResponsiblePersonType()))
		{
			if(!getRespPersonRegex().matcher(responsiblePerson.getResponsiblePersonType()).matches())
			{
				errors.rejectValue("responsiblePerson.responsiblePersonType", "responsiblePerson.responsiblePersonType","responsiblePersonType" + IND71Enums.INVALID_REGEX_ERROR);
			}
		}
	}

	private void validateEnrollment(IND71GEnrollment enrollment, Errors errors, int enrollmentCounter) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "enrollments["+enrollmentCounter+"].enrollmentId", "error.enrollmentId", "enrollmentId" + IND71Enums.REQUIRED_FIELD);
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "enrollments["+enrollmentCounter+"].insuranceType", "error.insuranceType", "insuranceType" + IND71Enums.REQUIRED_FIELD);
		if(!getInsTypeRegex().matcher(enrollment.getInsuranceType()).matches())
		{
			errors.rejectValue("enrollments["+enrollmentCounter+"].insuranceType", "enrollments["+enrollmentCounter+"].insuranceType","Invalid insuranceType.");
		}
		if(!StringUtils.isEmpty(enrollment.getCsr()) && !getCsrRegex().matcher(enrollment.getCsr()).matches())
		{
			errors.rejectValue("enrollments["+enrollmentCounter+"].csr", "enrollments["+enrollmentCounter+"].csr","Invalid csr.");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "enrollments["+enrollmentCounter+"].coverageDate", "error.coverageDate", "coverageDate" + IND71Enums.REQUIRED_FIELD);

		if(!getDateRegex().matcher(enrollment.getCoverageDate()).matches())
		{
			errors.rejectValue("enrollments["+enrollmentCounter+"].coverageDate", "coverageDate","Invalid coverageDate.");
		}
		if(!StringUtils.isEmpty(enrollment.getFinancialEffectiveDate())) {
		if(!getDateRegex().matcher(enrollment.getFinancialEffectiveDate()).matches())
		{
			errors.rejectValue("enrollments["+enrollmentCounter+"].financialEffectiveDate", "financialEffectiveDate","Invalid financialEffectiveDate.");
		}
		}
	}

	private void validateCustodialParent(IND71GMember member, Errors errors, int enrollmentCounter, int memberCounter) {
		if(!StringUtils.isEmpty(member.getCustodialParentId()))
		{
			validateElementPresent(member.getCustodialParentFirstName(), "CustodialParentFirstName", errors, enrollmentCounter, memberCounter);
			validateElementPresent(member.getCustodialParentLastName(), "CustodialParentLastName", errors, enrollmentCounter, memberCounter);
			validateElementPresent(member.getCustodialParentHomeAddress1(), "CustodialParentHomeAddress1", errors, enrollmentCounter, memberCounter);
			validateElementPresent(member.getCustodialParentHomeCity(), "CustodialParentHomeCity", errors, enrollmentCounter, memberCounter);
			validateElementPresent(member.getCustodialParentHomeState(), "CustodialParentHomeState", errors, enrollmentCounter, memberCounter);
			validateElementPresent(member.getCustodialParentHomeZip(), "CustodialParentHomeZip", errors, enrollmentCounter, memberCounter);
		}
		else
		{
			validateCustodialParentIdPresent(member.getCustodialParentFirstName(), "CustodialParentFirstName", member.getCustodialParentId(), errors, enrollmentCounter, memberCounter);
			validateCustodialParentIdPresent(member.getCustodialParentLastName(), "CustodialParentLastName", member.getCustodialParentId(), errors, enrollmentCounter, memberCounter);
			validateCustodialParentIdPresent(member.getCustodialParentHomeAddress1(), "CustodialParentHomeAddress1", member.getCustodialParentId(), errors, enrollmentCounter, memberCounter);
			validateCustodialParentIdPresent(member.getCustodialParentHomeCity(), "CustodialParentHomeCity", member.getCustodialParentId(), errors, enrollmentCounter, memberCounter);
			validateCustodialParentIdPresent(member.getCustodialParentHomeState(), "CustodialParentHomeState", member.getCustodialParentId(), errors, enrollmentCounter, memberCounter);
			validateCustodialParentIdPresent(member.getCustodialParentHomeZip(), "CustodialParentHomeZip", member.getCustodialParentId(), errors, enrollmentCounter, memberCounter);
		}
	}
	
	
}
