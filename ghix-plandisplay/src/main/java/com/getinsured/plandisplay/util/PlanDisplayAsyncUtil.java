package com.getinsured.plandisplay.util;

import java.io.IOException;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.hix.dto.planmgmt.PrescriptionSearchByHIOSIdRequestDTO;
import com.getinsured.hix.dto.planmgmt.PrescriptionSearchByHIOSIdResponseDTO;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.PlanMgmtEndPoints;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.plandisplay.service.prescriptionSearch.PrescriptionData;

@Async
@Component
public class PlanDisplayAsyncUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlanDisplayAsyncUtil.class);
	//private static ObjectMapper mapper = new ObjectMapper();

	@Autowired private RestTemplate restTemplate;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private GIWSPayloadService giwsPayloadService;

	public Future<Map<String, Object>> callProviderAPIAsync(String sourceUrl, @SuppressWarnings("rawtypes") HttpEntity entity) {
		Map<String, Object> providerResponse = new HashMap<String, Object>();
		try {
			HttpEntity<String> responseEntity = restTemplate.exchange(sourceUrl, HttpMethod.GET, entity, String.class);
			String response = responseEntity.getBody();
			//TypeReference<HashMap<String, Object>> typeRef = new TypeReference<HashMap<String, Object>>() {};
			//providerResponse = mapper.readValue(response, typeRef);
			ObjectReader reader = JacksonUtils.getJacksonObjectReaderForHashMap(String.class, Object.class);
			providerResponse = reader.readValue(response);

		} catch (Exception e) {
			Throwable ex = e.getCause();
			if(ex instanceof HttpStatusCodeException){
				HttpStatusCodeException statCodeExcp = (HttpStatusCodeException) ex;
				String responseBody = statCodeExcp.getResponseBodyAsString();
				if(responseBody != null && !responseBody.contains("Unable to find a record with") && !responseBody.contains("We were unable to")){
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "Error while calling callProviderAPIAsync", statCodeExcp, false);
				}
			}else{
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "Error while calling callProviderAPIAsync", e, false);
			}
		}
		return new AsyncResult<Map<String, Object>>(providerResponse);
	}
	
	public Future<PrescriptionData> callPrescriptionAPIAsync(String sourceUrl, @SuppressWarnings("rawtypes") HttpEntity entity) {
		Map<String, Object> providerResponse = new HashMap<String, Object>();
		try {
			HttpEntity<String> responseEntity = restTemplate.exchange(sourceUrl, HttpMethod.GET, entity, String.class);
			String response = responseEntity.getBody();
			//TypeReference<HashMap<String, Object>> typeRef = new TypeReference<HashMap<String, Object>>() {};
			//providerResponse = mapper.readValue(response, typeRef);
			ObjectReader reader = JacksonUtils.getJacksonObjectReaderForHashMap(String.class, Object.class);
			providerResponse = reader.readValue(response);

		} catch (Exception e) {
			Throwable ex = e.getCause();
			if(ex instanceof HttpStatusCodeException){
				HttpStatusCodeException statCodeExcp = (HttpStatusCodeException) ex;
				String responseBody = statCodeExcp.getResponseBodyAsString();
				if(responseBody != null && !responseBody.contains("Unable to find a record with")){
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "Error while calling callPrescriptionAPIAsync", statCodeExcp, false);
				}
			}else{
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "Error while calling callPrescriptionAPIAsync", e, false);
			}
		}
		PrescriptionData responseObj = new PrescriptionData();
		responseObj.setVericredPrescriotions(providerResponse);
		return new AsyncResult<PrescriptionData>(responseObj);
	}
	
	public Future<PrescriptionData> callAPIAsyncPOST(PrescriptionSearchByHIOSIdRequestDTO dto)
	{
		PrescriptionSearchByHIOSIdResponseDTO responseDTO =null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(PrescriptionSearchByHIOSIdRequestDTO.class);
			String postParams = writer.writeValueAsString(dto);
			String postRespPlanMgmt = (ghixRestTemplate.exchange(PlanMgmtEndPoints.PRESCRIPTION_SERACH_BY_HIOS_ID_URL, GhixConstants.USER_NAME_EXCHANGE ,HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, postParams)).getBody();
			ObjectReader objectReader = JacksonUtils.getJacksonObjectReaderForJavaType(PrescriptionSearchByHIOSIdResponseDTO.class);			
			responseDTO =  objectReader.readValue(postRespPlanMgmt);
		} catch (Exception e) {
			Throwable ex = e.getCause();
			if(ex instanceof HttpStatusCodeException){
				HttpStatusCodeException statCodeExcp = (HttpStatusCodeException) ex;
				String responseBody = statCodeExcp.getResponseBodyAsString();
				if(responseBody != null && !responseBody.contains("Unable to find a record with")){
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "Error while calling callAPIAsyncPOST", statCodeExcp, false);
				}
			}else{
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "Error while calling callAPIAsyncPOST", e, false);
			}
		}
		PrescriptionData responseObj = new PrescriptionData();
		responseObj.setGiPrescriptions(responseDTO.getPrescriptionList());
		return new AsyncResult<PrescriptionData>(responseObj);
	}

	public Future<Map<String, Object>> getRxNormResponseForNdc(String ndc)
	{
		Map<String, Object> rxNormResponseObj = null;
		try {
			String rxNormServiceUrl = DynamicPropertiesUtil.getPropertyValue("global.RxNorm.ServiceUrl");
			HttpHeaders headers = new HttpHeaders();
			HttpEntity entity = new HttpEntity(headers);
			HttpEntity<String> responseEntity =  restTemplate.exchange(rxNormServiceUrl + "/REST/rxcui.json?idtype=NDC&id="+ndc, HttpMethod.GET, entity, String.class);
			String rxNormResponse = responseEntity.getBody();
			ObjectReader objreader = JacksonUtils.getJacksonObjectReaderForHashMap(String.class, Object.class);
			rxNormResponseObj = objreader.readValue(rxNormResponse);
		} catch (JsonParseException |JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		return new AsyncResult<Map<String, Object>>(rxNormResponseObj);
	}
	
	public void populateGiWSPayload(String requestXML, String responseXML, String endpointUrl, String endpointOperationName, String status) {

		GIWSPayload newGiWSPayload = new GIWSPayload();

		newGiWSPayload.setCreatedTimestamp(new TSDate());
		newGiWSPayload.setEndpointUrl(endpointUrl);
		newGiWSPayload.setRequestPayload(requestXML);
		newGiWSPayload.setResponsePayload(responseXML);
		newGiWSPayload.setEndpointOperationName(endpointOperationName);
		newGiWSPayload.setStatus(status);
		
		giwsPayloadService.save(newGiWSPayload);;
	}
}
