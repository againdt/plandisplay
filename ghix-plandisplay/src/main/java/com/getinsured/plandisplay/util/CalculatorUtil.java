package com.getinsured.plandisplay.util;

import java.util.Map;

public class CalculatorUtil {
	
	private Map<String, Double> benefitsPriceMapKey;
	
	public CalculatorUtil(Map<String, Double> benefitsPriceMapKey) {
		this.benefitsPriceMapKey = benefitsPriceMapKey;
	}

	/** Determines if the calculation is based on COPAY or COINSURANCE
	 * @param Map<String,String> benefit
	 * @return
	 */
	public String getChargeType(Map<String,String> benefit) {
		String attributeType="";
		String networkCopayAttribute= (benefit.get("tier1CopayAttrib")==null)?"":benefit.get("tier1CopayAttrib");
		String networkCoinsuranceAttribute = (benefit.get("tier1CoinsAttrib")==null)?"":benefit.get("tier1CoinsAttrib");

		if("No Charge".equalsIgnoreCase(networkCopayAttribute)) {
			networkCopayAttribute = "0";
		}

		if("No Charge".equalsIgnoreCase(networkCoinsuranceAttribute)) {
			networkCoinsuranceAttribute = "0";
		}

		if("0".equals(networkCopayAttribute) && "0".equals(networkCoinsuranceAttribute)) {
			attributeType = "NO COST";
		}
		else if("0".equals(networkCopayAttribute) && !("0".equals(networkCoinsuranceAttribute))) {
			attributeType = "COINSURANCE";
		}
		else if(!"0".equals(networkCopayAttribute) && "0".equals(networkCoinsuranceAttribute)) {
			attributeType = "COPAY";
		}

		else if(!"0".equals(networkCopayAttribute) && !"0".equals(networkCoinsuranceAttribute)) {
			//attributeType = "COPAY "; Suneel
			attributeType = getNonZeroPaymentAttribute(benefit);
		}
		else {
			attributeType = "COINSURANCE";
		}
		return attributeType;
	}
	
	/** get the attribute which is non-zero. THe output is COPAY or COINSURANCE
	 * @param Map<String,String> benefit
	 * @return String processType
	 */
	private String getNonZeroPaymentAttribute(Map<String,String> benefit) {
		Double benefitCoinsValue = Double.parseDouble((benefit.get("tier1CoinsVal")==null || "".equals(benefit.get("tier1CoinsVal")))?"0.0":benefit.get("tier1CoinsVal"));
		Double benefitCopayValue = Double.parseDouble((benefit.get("tier1CopayVal")==null || "".equals(benefit.get("tier1CopayVal")))?"0.0":benefit.get("tier1CopayVal"));
		String processType = "COPAY";
		if(benefitCoinsValue > 0 && benefitCopayValue > 0) {
			processType = "COPAY";
		}
		else if(benefitCoinsValue > 0 && benefitCopayValue <= 0 ){
			processType = "COINSURANCE";
		}
		else if(benefitCoinsValue <= 0 && benefitCopayValue > 0 ){
			processType = "COPAY";
		}
		else {
		}

		return processType;
	}
	
	/** Calculate the cost for the given benefit
	 * For copay the cost is copay and for coinsurance the cost is list price * cosinsurance
	 * @param Map<String,String> benefit
	 * @param String paymentType
	 * @param String benefitName
	 * @return double cost
	 */
	public double getBenefitPaymentCost(Map<String,String> benefit, String paymentType, String benefitName) {
		String coInsuranceVal = (benefit.get("tier1CoinsVal")==null || "".equals(benefit.get("tier1CoinsVal")) )?"0.0":benefit.get("tier1CoinsVal");
		String copayVal = (benefit.get("tier1CopayVal")==null || "".equals(benefit.get("tier1CopayVal")))?"0.0":benefit.get("tier1CopayVal");
		double cost = 0.0;
		
		if("COPAY".equalsIgnoreCase(paymentType)) {
			cost = Double.parseDouble(copayVal);
		}
		else if("COINSURANCE".equalsIgnoreCase(paymentType)){
			cost = Double.parseDouble(coInsuranceVal) * benefitsPriceMapKey.get(benefitName)/100 ;
		}
		else {
			cost = 0.0; // should this be 1 for logarithm to work??
		}
		return cost;
	}
	
	public boolean isBenefitCovered(Map<String,String> benefit) {
		if(benefit!=null){
			String isCovered = benefit.get("isCovered");
			if(isCovered!=null&&"Covered".equalsIgnoreCase(isCovered)){
				return true;
			}
		}
		return false;
	}

}
