package com.getinsured.plandisplay.util;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.getinsured.hix.platform.util.ModuleAwareClassLoader;
import com.getinsured.hix.platform.util.ModuleContextProvider;


public class ModuleClassLoaderHook implements ApplicationContextAware {
	private Logger logger = LoggerFactory.getLogger(ModuleClassLoaderHook.class);
	@Autowired private ModuleAwareClassLoader platformClassLoader;
	@Autowired private ModuleContextProvider moduleContextProvider;
	private ApplicationContext appContext;
	
	@PostConstruct
	public void register(){
		logger.info("Registering Plan Display Class loader with Platform");
		platformClassLoader.registerClassLoader(this.getClass().getClassLoader(),"Plan Display".intern());
		moduleContextProvider.setModuleContext(appContext);
	}

	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		this.appContext = context;
		
	}

}
