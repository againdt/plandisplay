package com.getinsured.plandisplay.util;

import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;

import com.getinsured.hix.dto.plandisplay.ind71g.IND71Enums;

@Component
public class IND71BaseValidator {
	private Pattern nameRegex;
	private Pattern relationRegex;
	private Pattern dateRegex;
	private Pattern ssnRegex;
	private Pattern addressRegex;
	private Pattern stateRegex;
	private Pattern zipRegex;
	private Pattern countyRegex;
	private Pattern phoneRegex;
	private Pattern cityRegex;
	private Pattern emailRegex;
	private Pattern yesNoRegex;
	private Pattern mrcRegex;
	private Pattern respPersonTypeRegex;
	private Pattern respPersonRegex;
	private Pattern genderRegex;
	private Pattern maritalStatusRegex;
	private Pattern citizenShpRegex;
	private Pattern spkLangRegex;
	private Pattern wrtLangRegex;
	private Pattern yesRegex;
	private Pattern csrRegex;
	private Pattern insTypeRegex;
	

	public Pattern getNameRegex() {
		return nameRegex;
	}

	public Pattern getRelationRegex() {
		return relationRegex;
	}

	public Pattern getDateRegex() {
		return dateRegex;
	}

	public Pattern getSsnRegex() {
		return ssnRegex;
	}

	public Pattern getAddressRegex() {
		return addressRegex;
	}

	public Pattern getStateRegex() {
		return stateRegex;
	}

	public Pattern getZipRegex() {
		return zipRegex;
	}

	public Pattern getCountyRegex() {
		return countyRegex;
	}

	public Pattern getPhoneRegex() {
		return phoneRegex;
	}

	public Pattern getCityRegex() {
		return cityRegex;
	}

	public Pattern getEmailRegex() {
		return emailRegex;
	}

	public Pattern getYesNoRegex() {
		return yesNoRegex;
	}

	public Pattern getMrcRegex() {
		return mrcRegex;
	}

	public Pattern getRespPersonTypeRegex() {
		return respPersonTypeRegex;
	}

	public Pattern getRespPersonRegex() {
		return respPersonRegex;
	}
	public Pattern getGenderRegex() {
		return genderRegex;
	}

	public Pattern getMaritalStatusRegex() {
		return maritalStatusRegex;
	}

	public Pattern getCitizenShpRegex() {
		return citizenShpRegex;
	}

	public Pattern getSpkLangRegex() {
		return spkLangRegex;
	}

	public Pattern getWrtLangRegex() {
		return wrtLangRegex;
	}

	public Pattern getYesRegex() {
		return yesRegex;
	}

	public Pattern getCsrRegex() {
		return csrRegex;
	}

	public Pattern getInsTypeRegex() {
		return insTypeRegex;
	}
	@PostConstruct
	public void init()
	{
		nameRegex = Pattern.compile(IND71Enums.NAME_REGEX);
		relationRegex = Pattern.compile(IND71Enums.RELATION_REGEX);
		dateRegex = Pattern.compile(IND71Enums.DATE_REGEX);
		ssnRegex = Pattern.compile(IND71Enums.SSN_REGEX);
		addressRegex = Pattern.compile(IND71Enums.ADDRESS_REGEX);
		stateRegex = Pattern.compile(IND71Enums.STATE_REGEX);
		zipRegex = Pattern.compile(IND71Enums.ZIP_REGEX);
		countyRegex = Pattern.compile(IND71Enums.COUNTY_REGEX);
		phoneRegex = Pattern.compile(IND71Enums.PHONE_REGEX);
		cityRegex = Pattern.compile(IND71Enums.CITY_REGEX);
		emailRegex = Pattern.compile(IND71Enums.EMAIL_REGEX);
		yesNoRegex = Pattern.compile(IND71Enums.YES_NO_REGEX);
		mrcRegex = Pattern.compile(IND71Enums.MRC_REGEX);
		respPersonTypeRegex = Pattern.compile(IND71Enums.RESPONSIBLE_PERSON_TYPE);
		respPersonRegex = Pattern.compile(IND71Enums.RESPONSIBLE_PERSON_REGEX);
		genderRegex = Pattern.compile(IND71Enums.GENDER_REGEX);
		maritalStatusRegex = Pattern.compile(IND71Enums.MARITAL_STATUS_REGEX);
		citizenShpRegex = Pattern.compile(IND71Enums.CITIZENSHIP_STATUS_REGEX);
		spkLangRegex = Pattern.compile(IND71Enums.SPOKEN_LANGUAGE_CODE_REGEX);
		wrtLangRegex = Pattern.compile(IND71Enums.WRITTEN_LANGUAGE_CODE_REGEX);
		yesRegex = Pattern.compile(IND71Enums.YES_REGEX);
		csrRegex = Pattern.compile(IND71Enums.CSR_REGEX);
		insTypeRegex = Pattern.compile(IND71Enums.INSURANCE_TYPE_REGEX);
	}
	

	protected void validateElementPresent(String elementValue,String elementName, Errors errors, int enrollmentCounter, int memberCounter)
	{
		if (StringUtils.isEmpty(elementValue)) {
			errors.rejectValue("enrollments["+enrollmentCounter+"].members["+memberCounter+"]."+elementName, "enrollments["+enrollmentCounter+"].members["+memberCounter+"]."+elementName,elementName +IND71Enums.CUSTODIAL_PARENT_ERROR);
		}
	}
	
	protected void validateCustodialParentIdPresent(String custodialParentElement,String custodialParentElementName,String custodialParentId, Errors errors, int enrollmentCounter, int memberCounter)
	{
		if (!StringUtils.isEmpty(custodialParentElement) && StringUtils.isEmpty(custodialParentId)) {
			errors.rejectValue("enrollments["+enrollmentCounter+"].members["+memberCounter+"]."+custodialParentElementName, "enrollments["+enrollmentCounter+"].members["+memberCounter+"]."+custodialParentElementName,custodialParentElementName +IND71Enums.CUSTODIAL_PARENT_ID_ERROR);
		}
	}

	
	protected void validateZipAndLength(Errors errors, String zip, String fieldName) {
		if(!StringUtils.isEmpty(zip))
		{
			validateLength(zip, fieldName, IND71Enums.ZIP_MIN_LENGTH, IND71Enums.ZIP_MAX_LENGTH, errors);
			if(!getZipRegex().matcher(zip).matches())
			{
				errors.rejectValue(fieldName, fieldName,fieldName + IND71Enums.INVALID_REGEX_ERROR);
			}
		}
	}
	
	protected void validateStateAndLength(Errors errors, String state, String fieldName) {
		if(!StringUtils.isEmpty(state))
		{
			validateLength(state, fieldName, IND71Enums.STATE_MIN_LENGTH, IND71Enums.STATE_MAX_LENGTH, errors);
			if(!getStateRegex().matcher(state).matches())
			{
				errors.rejectValue(fieldName, fieldName,fieldName + IND71Enums.INVALID_REGEX_ERROR);
			}
		}
	}

	protected void validateCityAndLength(Errors errors, String city, String fieldName) {
		if(!StringUtils.isEmpty(city))
		{
			validateLength(city, fieldName, IND71Enums.CITY_MIN_LENGTH, IND71Enums.CITY_MAX_LENGTH, errors);
			if(!getCityRegex().matcher(city).matches())
			{
				errors.rejectValue(fieldName, fieldName,fieldName + IND71Enums.INVALID_REGEX_ERROR);
			}
		}
	}
	
	protected void validateAdddressAndLength(Errors errors, String address, String fieldName) {
		if(!StringUtils.isEmpty(address))
		{
			validateLength(address, fieldName, IND71Enums.ADDRESS_MIN_LENGTH, IND71Enums.ADDRESS_MAX_LENGTH, errors);
			if(!getAddressRegex().matcher(address).matches())
			{
				errors.rejectValue(fieldName, fieldName,fieldName + IND71Enums.INVALID_REGEX_ERROR);
			}
		}
	}
	
	protected void validateEmail(Errors errors, String email, String fieldName) {
		if(!StringUtils.isEmpty(email))
		{
			if(!getEmailRegex().matcher(email).matches())
			{
				errors.rejectValue(fieldName, fieldName,fieldName + IND71Enums.INVALID_EMAIL_ERROR);
			}
		}
	}
	
	protected void validatePhoneAndLength(Errors errors, String phone, String fieldName) {
		if(!StringUtils.isEmpty(phone))
		{
			validateLength(phone, fieldName, IND71Enums.PHONE_LENGTH, IND71Enums.PHONE_LENGTH, errors);
			if(!getPhoneRegex().matcher(phone).matches())
			{
				errors.rejectValue(fieldName, fieldName,phone + IND71Enums.INVALID_REGEX_ERROR);
			}
		}
	}
	
	protected void validateNameAndLength(Errors errors, String name, String fieldName) {
		if(!StringUtils.isEmpty(name))
		{
			validateLength(name, fieldName, IND71Enums.NAME_MIN_LENGTH, IND71Enums.NAME_MAX_LENGTH, errors);
			if(!getNameRegex().matcher(name).matches())
			{
				errors.rejectValue(fieldName, fieldName,fieldName + IND71Enums.INVALID_REGEX_ERROR);
			}
		}
	}


	protected void validateLength(String valueToCheck, String fieldName, int minLength, int maxLength, Errors errors) {
		if(!StringUtils.isEmpty(valueToCheck))
		{
			int applicationIdLength = valueToCheck.length();
			
			if (applicationIdLength < minLength || applicationIdLength > maxLength)
			{
				errors.rejectValue(fieldName, fieldName, fieldName+IND71Enums.LENGTH_ERROR);
			}
		}
	}
}
