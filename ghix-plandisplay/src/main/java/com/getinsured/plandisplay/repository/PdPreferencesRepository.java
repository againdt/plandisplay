package com.getinsured.plandisplay.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PdPreferences;

public interface PdPreferencesRepository extends JpaRepository<PdPreferences, Serializable>{
	
	@Query("SELECT pdPreferences FROM PdPreferences pdPreferences where pdHouseholdId = :pdHouseholdId")
	public PdPreferences findPreferencesByPdHouseholdId(@Param("pdHouseholdId") Long pdHouseholdId);
	
	@Query("SELECT pdPreferences FROM PdPreferences pdPreferences where eligLeadId = :eligLeadId order by pdPreferences.id desc")
	public List<PdPreferences> findPreferencesByEligLeadId(@Param("eligLeadId") Long eligLeadId);

}
