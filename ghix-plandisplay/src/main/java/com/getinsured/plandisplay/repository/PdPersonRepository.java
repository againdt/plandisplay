package com.getinsured.plandisplay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PdPerson;

public interface PdPersonRepository extends JpaRepository<PdPerson, Long>
{
	@Query("SELECT person FROM PdPerson person WHERE pdHousehold.id = :householdId)")
	public List<PdPerson> findPersonListByHouseholdId(@Param("householdId") Long householdId);
}