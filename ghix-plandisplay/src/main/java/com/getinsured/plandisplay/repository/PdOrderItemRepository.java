package com.getinsured.plandisplay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.PdOrderItem;
import com.getinsured.hix.model.PdOrderItem.Status;

@Transactional(readOnly = true)
public interface PdOrderItemRepository extends JpaRepository<PdOrderItem, Long> {
	
	@Query("SELECT id FROM PdOrderItem pdOrderItem WHERE pdHousehold.id = :householdId and (status !='DELETED' or status is null)")
	public List<Long> findOrderItemIdsByHouseholdId(@Param("householdId") long householdId);

	@Query("SELECT pdOrderItem FROM PdOrderItem pdOrderItem WHERE pdHousehold.id = :householdId and (status !='DELETED' or status is null)")
	public List<PdOrderItem> findOrderItemsByHouseholdId(@Param("householdId") long householdId);
	
	@Query("SELECT planId FROM PdOrderItem pdOrderItem WHERE pdHousehold.id = :householdId and (status !='DELETED' or status is null)")
    public List<Long> findPlanIdsByHouseholdId(@Param("householdId") long householdId);
	
	@Query("SELECT pdOrderItem FROM PdOrderItem pdOrderItem WHERE ssapApplicationId = :applicationId and (status !='DELETED' or status is null)")
	public List<PdOrderItem> findOrderItemsByApplicationId(@Param("applicationId") long applicationId);

	@Modifying
	@Transactional(readOnly=false)
	@Query("Update PdOrderItem pdOrderItem set status = :status where pdOrderItem.id = :id")
	public int updateOrderItemStatus(@Param("id") Long id, @Param("status") Status status);
	
	@Query("FROM PdOrderItem pdOrderItem WHERE pdHousehold.id = :householdId")
	public List<PdOrderItem> findAllOrderItemsByHouseholdId(@Param("householdId") long householdId);
}