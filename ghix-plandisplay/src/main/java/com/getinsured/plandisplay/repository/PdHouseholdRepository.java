package com.getinsured.plandisplay.repository;

import java.io.Serializable;
import java.util.List;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PdHousehold;

public interface PdHouseholdRepository extends JpaRepository<PdHousehold, Serializable> {

	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	@Query("SELECT household FROM PdHousehold household where shoppingId = :shoppingId")
	public PdHousehold findHouseholdByShoppingId(@Param("shoppingId") String shoppingId);
	
	@Query("SELECT household FROM PdHousehold household where externalId = null and eligLeadId = :leadId order by household.id desc")
	public List<PdHousehold> findPreShoppingHousehold(@Param("leadId") Long leadId);
	
	@Query("SELECT household FROM PdHousehold household where applicationId = :ssapApplicationId order by household.id desc")
	public List<PdHousehold> getHouseholdByApplicationId(@Param("ssapApplicationId") Long ssapApplicationId);

}
