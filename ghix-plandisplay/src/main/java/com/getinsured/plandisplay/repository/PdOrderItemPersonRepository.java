package com.getinsured.plandisplay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.PdOrderItemPerson;

@Transactional(readOnly = true)
public interface PdOrderItemPersonRepository extends JpaRepository<PdOrderItemPerson, Long> {

	@Query("SELECT pdOrderItemPerson FROM PdOrderItemPerson pdOrderItemPerson WHERE pdOrderItem.id = :orderItemId")
	public List<PdOrderItemPerson> findOrderItemPersonsByOrderItemId(@Param("orderItemId") long orderItemId);
	
	@Modifying
	@Transactional
	@Query("DELETE FROM PdOrderItemPerson pdOrderItemPerson WHERE pdOrderItem.id = :orderItemId")
	public Integer deleteByOrderItemId(@Param("orderItemId") long orderItemId);	
	
	@Query("SELECT count(*) FROM PdOrderItemPerson pdOrderItemPerson WHERE pdOrderItem.id = :orderItemId")
	public Long countOrderItemPersonsByOrderItemId(@Param("orderItemId") long orderItemId);
	
}