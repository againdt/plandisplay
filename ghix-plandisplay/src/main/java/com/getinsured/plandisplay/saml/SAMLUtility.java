package com.getinsured.plandisplay.saml;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.opensaml.Configuration;
import org.opensaml.DefaultBootstrap;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.Response;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.io.Unmarshaller;
import org.opensaml.xml.io.UnmarshallerFactory;
import org.opensaml.xml.schema.XSString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.plandisplay.util.PlanDisplayUtil;

public class SAMLUtility{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SAMLUtility.class);
	
	public String prepareSAMLOutput(Map<String, String> configParams, Map<String, String> inputData,String postReturnUrl,String keepAliveUrl)
	{
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside the method SAMLUtility.prepareSAMLOutput---->", null, false);
		
		SAMLInputContainer samlInput = this.prepareInputForSAMLGeneration(configParams, inputData,postReturnUrl,keepAliveUrl);

		String encodedSAMLRespone = SAMLGenerator.generateEncodedSAML(samlInput, configParams);
		
		return encodedSAMLRespone;
	}
	
	public Map<String, String> parseSAMLOutput(String samlResponse)
	{
		Assertion assertion = null;
		Map<String, String> attributeValues = new HashMap<String, String>();
		ByteArrayInputStream is = new ByteArrayInputStream(samlResponse.getBytes());
		try
		{
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			documentBuilderFactory.setNamespaceAware(true);
			DocumentBuilder docBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document = docBuilder.parse(is);
			Element element = document.getDocumentElement();
			UnmarshallerFactory unmarshallerFactory = null;
			try 
			{
				DefaultBootstrap.bootstrap();
				unmarshallerFactory = Configuration.getUnmarshallerFactory();
				Unmarshaller unmarshaller = unmarshallerFactory.getUnmarshaller(element);
				XMLObject responseXmlObj = unmarshaller.unmarshall(element);
				Response response = (Response)responseXmlObj;		
				assertion = response.getAssertions().get(0);
				List<Attribute> attributes = assertion.getAttributeStatements().get(0).getAttributes();
				
				for(Attribute attribute : attributes){
					if("Partner Assigned Consumer ID".equalsIgnoreCase(attribute.getName())){
						attributeValues.put("partnerAssignedConsumerID", attribute.getAttributeValues().get(0).getDOM().getTextContent());
					}
					if("FFE Assigned Consumer ID".equalsIgnoreCase(attribute.getName())){
						attributeValues.put("ffmAssignedConsumerID", attribute.getAttributeValues().get(0).getDOM().getTextContent());
					}
					if("FFE User ID".equalsIgnoreCase(attribute.getName())){
						attributeValues.put("ffmUserID", attribute.getAttributeValues().get(0).getDOM().getTextContent());
					}
					if("Transfer Type".equalsIgnoreCase(attribute.getName())){
						attributeValues.put("transferType", attribute.getAttributeValues().get(0).getDOM().getTextContent());
					}
					if("State Exchange Code".equalsIgnoreCase(attribute.getName())){
						attributeValues.put("stateExchangeCode", attribute.getAttributeValues().get(0).getDOM().getTextContent());
					}
				}
			} catch (Exception ce) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, ce.getLocalizedMessage(), ce, false);
			}
		} catch(Exception pce){
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, pce.getLocalizedMessage(), pce, false);
		}
		
		return attributeValues;
	}
	
	private SAMLInputContainer prepareInputForSAMLGeneration(Map<String, String> configParams, Map<String, String> inputData,String postReturnUrl,String keepAliveUrl)
	{
		SAMLInputContainer samlInput = new SAMLInputContainer();
		
		samlInput.setStrIssuer("GetInsured");
		samlInput.setStrNameID(configParams.get("ffmPartnerId"));
		samlInput.setStrNameQualifier("");
		samlInput.setSessionId("abcdedf1234567");
		
		Map<String, String> customAttributes = new HashMap<String, String>();
		customAttributes.put("State Exchange Code", inputData.get("State")+"0");
		customAttributes.put("Partner Assigned Consumer ID", inputData.get("PartnerAssignedConsumerID"));
		customAttributes.put("User Type", inputData.get("UserType"));
		customAttributes.put("Transfer Type", "Direct Enrollment");
		customAttributes.put("Return URL", postReturnUrl);
		customAttributes.put("Keep Alive URL", keepAliveUrl);
		
		if("Consumer".equals(inputData.get("UserType"))){
			if(inputData.get("FFMAssignedConsumerID") != null){
				customAttributes.put("FFE Assigned Consumer ID", inputData.get("FFMAssignedConsumerID"));
			}
		}else{
			customAttributes.put("FFE User ID", inputData.get("FfmUserId"));
		}

		samlInput.setAttributes(customAttributes);
		
		return samlInput;
	}
}