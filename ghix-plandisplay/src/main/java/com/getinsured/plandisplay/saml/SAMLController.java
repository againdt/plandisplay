package com.getinsured.plandisplay.saml;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.model.plandisplay.PlanDisplayResponse;
import com.getinsured.plandisplay.util.PlanDisplayUtil;
import com.google.gson.Gson;

/**
 * 
 * @author krishna_prasad
 *
 */

@Controller
@RequestMapping(value = "/saml")
public class SAMLController
{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SAMLController.class);
	private static final String STATUS_SUCCESS = "SUCCESS";
	private static final String STATUS_FAILED = "FAILED";
	private static final String POST_RETURN_URL = "postFfm/postReturnUrl";
	private static final String KEEP_ALIVE_URL = "postFfm/keepAliveUrl";
	
	@Value("#{configProp}")
	private Properties properties;
	@Autowired private Gson platformGson;
	
	@RequestMapping(value="/returnSAMLOutput", method = RequestMethod.POST)
	public @ResponseBody String returnSAMLOutput(@RequestBody Map<String, String> inputData)
	{
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside SAMLController---->", null, false);
		String response = "";
		String id ="";
		PlanDisplayResponse planDisplayResponse = new PlanDisplayResponse();

		try {
			planDisplayResponse.startResponse();
			Map<String, String> configParams = this.prepareConfigParams(properties);
			String keepAliveUrl = inputData.get("keepAliveUrl");
			String postReturnUrl = inputData.get("postReturnUrl");
			String appUrl = inputData.get("appUrl");
			if(StringUtils.isBlank(postReturnUrl) || StringUtils.isBlank(keepAliveUrl)){
				keepAliveUrl = appUrl + KEEP_ALIVE_URL;
				postReturnUrl = appUrl + POST_RETURN_URL;
			}
			id = new SAMLUtility().prepareSAMLOutput(configParams, inputData,postReturnUrl,keepAliveUrl);
			
			if (id == null) {
				planDisplayResponse.setStatus(STATUS_FAILED);
				planDisplayResponse.setErrMsg("Invalid input: Id cannot be null");
				planDisplayResponse.endResponse();
				response = platformGson.toJson(planDisplayResponse);
				return response;
			}

			planDisplayResponse.setStatus(STATUS_SUCCESS);
			Map<String,Object> outputData = new HashMap<String, Object>();
			outputData.put("id", id);
			planDisplayResponse.setOutputData(outputData);
			planDisplayResponse.endResponse();					

		} catch (Exception e) {
			 PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occured while finding PldHousehold by Id---->", e, false);
		}
		return id;
	}
	
	@RequestMapping(value="/returnSAMLResponseAttributes", method = RequestMethod.POST)
	public @ResponseBody Map<String, String> returnSAMLResponseAttributes(@RequestBody String samlResponse)
	{
		return new SAMLUtility().parseSAMLOutput(samlResponse);
	}
	
	private Map<String, String> prepareConfigParams(Properties properties)
	{
		Map<String, String> inputParams = new HashMap<String, String>();
		String keystoreFileLocation = properties.getProperty("platform.keystore.location") + "/" + properties.getProperty("platform.keystore.file");
		inputParams.put("keystoreFileLocation", keystoreFileLocation);
		inputParams.put("privateKeyAliasName", properties.getProperty("platform.privateKey.alias"));
		inputParams.put("storePassword", properties.getProperty("platform.keystore.pass"));
		inputParams.put("ffmPartnerId", properties.getProperty("ffmPartnerId"));
		return inputParams;
	}
	
}