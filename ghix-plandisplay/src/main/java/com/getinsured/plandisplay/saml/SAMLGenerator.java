package com.getinsured.plandisplay.saml;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;

import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import org.opensaml.Configuration;
import org.opensaml.DefaultBootstrap;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.common.SAMLVersion;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.AttributeValue;
import org.opensaml.saml2.core.AuthnContext;
import org.opensaml.saml2.core.AuthnContextClassRef;
import org.opensaml.saml2.core.AuthnStatement;
import org.opensaml.saml2.core.Condition;
import org.opensaml.saml2.core.Conditions;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.NameID;
import org.opensaml.saml2.core.OneTimeUse;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.core.Status;
import org.opensaml.saml2.core.StatusCode;
import org.opensaml.saml2.core.StatusMessage;
import org.opensaml.saml2.core.Subject;
import org.opensaml.saml2.core.SubjectConfirmation;
import org.opensaml.saml2.core.SubjectConfirmationData;
import org.opensaml.saml2.core.SubjectLocality;
import org.opensaml.saml2.core.impl.ResponseMarshaller;
import org.opensaml.saml2.core.impl.StatusBuilder;
import org.opensaml.saml2.core.impl.StatusCodeBuilder;
import org.opensaml.saml2.core.impl.StatusMessageBuilder;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.XMLObjectBuilder;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.schema.XSString;
import org.opensaml.xml.security.SecurityConfiguration;
import org.opensaml.xml.security.SecurityHelper;
import org.opensaml.xml.security.credential.Credential;
import org.opensaml.xml.security.keyinfo.KeyInfoGenerator;
import org.opensaml.xml.security.keyinfo.KeyInfoGeneratorFactory;
import org.opensaml.xml.security.keyinfo.KeyInfoGeneratorManager;
import org.opensaml.xml.security.keyinfo.KeyInfoHelper;
import org.opensaml.xml.security.keyinfo.NamedKeyInfoGeneratorManager;
import org.opensaml.xml.security.x509.BasicX509Credential;
import org.opensaml.xml.signature.KeyInfo;
import org.opensaml.xml.signature.Signature;
import org.opensaml.xml.signature.SignatureException;
import org.opensaml.xml.signature.Signer;
import org.opensaml.xml.signature.X509SubjectName;
import org.opensaml.xml.util.Base64;
import org.opensaml.xml.util.XMLHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.w3c.dom.Element;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.plandisplay.util.PlanDisplayUtil;

/**
 * This class creates a valid SAML 2.0 Assertion.
 */
public class SAMLGenerator 
{

	private final static Logger logger = LoggerFactory.getLogger(SAMLGenerator.class);
	
	private static XMLObjectBuilderFactory builderFactory;	
	private static Credential signingCredential = null;
	public static String generateEncodedSAML(SAMLInputContainer samlInput, Map<String, String> keyStoreParams) 
	{
		String encodedSAMLRespone = null; 
		try 
		{
			Assertion assertion = SAMLGenerator.buildDefaultAssertion(samlInput);
			
			SAMLGenerator sign = new SAMLGenerator();
			sign.intializeCredentials(keyStoreParams);
			Signature signature = null;
			try {
				DefaultBootstrap.bootstrap();
			} catch (ConfigurationException e) {
				PlanDisplayUtil.putLoggerStmt(logger, LogLevel.ERROR, "<----ConfigurationException in  the method generateEncodedSAML---->", e, false);
			}
			
			signature = (Signature) Configuration.getBuilderFactory().getBuilder(Signature.DEFAULT_ELEMENT_NAME).buildObject(Signature.DEFAULT_ELEMENT_NAME);
			signature.setSigningCredential(signingCredential);

			// This is also the default if a null SecurityConfiguration is specified
			SecurityConfiguration secConfig = Configuration.getGlobalSecurityConfiguration();
			// If null this would result in the default KeyInfoGenerator being used

			try {
				SecurityHelper.prepareSignatureParams(signature, signingCredential, secConfig, null);
			} catch (SecurityException e) {
				PlanDisplayUtil.putLoggerStmt(logger, LogLevel.ERROR, "<----SecurityException in  the method generateEncodedSAML---->", e, false);
			} catch (org.opensaml.xml.security.SecurityException e) {
				PlanDisplayUtil.putLoggerStmt(logger, LogLevel.ERROR, "<----SecurityException in  the method generateEncodedSAML---->", e, false);
			}

			// Adding KeyInfo
			try {
				KeyInfo aKeyInfo = SAMLGenerator.getKeyInfo(signingCredential);
				// aKeyInfo.getX509Datas().size() + "\n\n\n\n");
				X509SubjectName x509SubjectName = KeyInfoHelper.buildX509SubjectName("CN=GetInsured, OU=GetInsured, O=GetInsured, L=Palo Alto, ST=California, C=US");
				aKeyInfo.getX509Datas().get(0).getX509SubjectNames().add(x509SubjectName);
				signature.setKeyInfo(aKeyInfo);
			} catch (SecurityException e1) {
				PlanDisplayUtil.putLoggerStmt(logger, LogLevel.ERROR, "<----SecurityException in  the method generateEncodedSAML---->", e1, false);
			} catch (org.opensaml.xml.security.SecurityException e1) {
				PlanDisplayUtil.putLoggerStmt(logger, LogLevel.ERROR, "<----SecurityException in  the method generateEncodedSAML---->", e1, false);
			}

			Response resp = SAMLGenerator.buildResponse();
			assertion.setSignature(signature);
			resp.getAssertions().add(assertion);

			try {
				Configuration.getMarshallerFactory().getMarshaller(resp).marshall(resp);
			} catch (MarshallingException e) {
				PlanDisplayUtil.putLoggerStmt(logger, LogLevel.ERROR, "<----MarshallingException in  the method generateEncodedSAML---->", e, false);
			}

			try {
				Signer.signObject(signature);
			} catch (SignatureException e) {
				PlanDisplayUtil.putLoggerStmt(logger, LogLevel.ERROR, "<----SignatureException in  the method generateEncodedSAML---->", e, false);
			}

			ResponseMarshaller respMarshaller = new ResponseMarshaller();
			Element plain = respMarshaller.marshall(resp);
			String samlResponse = XMLHelper.nodeToString(plain);
			
			PlanDisplayUtil.putLoggerStmt(logger, LogLevel.DEBUG, "<----SAML Response---->"+samlResponse, null, true);
			
			// Now Base64 Encode
			encodedSAMLRespone = new String(Base64.encodeBytes(samlResponse.getBytes(), Base64.DONT_BREAK_LINES));
			
			PlanDisplayUtil.putLoggerStmt(logger, LogLevel.DEBUG, "<----Encoded SAML Response---->"+encodedSAMLRespone, null, true);

		} catch (MarshallingException e) {
			PlanDisplayUtil.putLoggerStmt(logger, LogLevel.ERROR, "<----MarshallingException in  the method generateEncodedSAML---->", e, false);
		}		
		return encodedSAMLRespone;
	}

	public static KeyInfo getKeyInfo(Credential credential)	throws SecurityException, org.opensaml.xml.security.SecurityException 
	{
		SecurityConfiguration secConfiguration = Configuration.getGlobalSecurityConfiguration();
		NamedKeyInfoGeneratorManager namedKeyInfoGeneratorManager = secConfiguration.getKeyInfoGeneratorManager();
		KeyInfoGeneratorManager keyInfoGeneratorManager = namedKeyInfoGeneratorManager.getDefaultManager();
		KeyInfoGeneratorFactory factory = keyInfoGeneratorManager.getFactory(credential);
		KeyInfoGenerator generator = factory.newInstance();
		return generator.generate(credential);
	}

	private void intializeCredentials(Map<String, String> keyStoreParams) 
	{
		final String storePassword = keyStoreParams.get("storePassword");
		final String privateKeyAliasName = keyStoreParams.get("privateKeyAliasName");
		Resource resource = new FileSystemResource(keyStoreParams.get("keystoreFileLocation"));
		
		KeyStore ks = null;
		InputStream is = null;
		char[] password = storePassword.toCharArray();

		// Get Default Instance of KeyStore
		try {
			ks = KeyStore.getInstance(KeyStore.getDefaultType());
			is = resource.getInputStream();
			ks.load(is, password);
		}
		catch (KeyStoreException e) {
			PlanDisplayUtil.putLoggerStmt(logger, LogLevel.ERROR, "<----Error while Intializing Keystore---->", e, false);
		} 
		catch (IOException e1) {
			PlanDisplayUtil.putLoggerStmt(logger, LogLevel.ERROR, e1.getLocalizedMessage(), e1, false);
		}//this.getClass().getClassLoader().getResourceAsStream("ffm_gi_keystore.jks");
		catch (NoSuchAlgorithmException e) {
			PlanDisplayUtil.putLoggerStmt(logger, LogLevel.ERROR, "<----Failed to Load the KeyStore---->", e, false);
		}
		catch (CertificateException e) {
			PlanDisplayUtil.putLoggerStmt(logger, LogLevel.ERROR, "<----Failed to Load the KeyStore---->", e, false);
		}

		// Close InputFileStream
		try {
				if(is != null){
					is.close();
				}
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(logger, LogLevel.ERROR, "<----Failed to close file stream---->", e, false);
		}

		// Get Private Key Entry From Certificate
		KeyStore.PrivateKeyEntry pkEntry = null;
		try {
			if(ks != null){
				pkEntry = (KeyStore.PrivateKeyEntry) ks.getEntry(privateKeyAliasName, new KeyStore.PasswordProtection(password));
			}
		} catch (NoSuchAlgorithmException e) {
			PlanDisplayUtil.putLoggerStmt(logger, LogLevel.ERROR, "<----Failed to Get Private Entry From the keystore---->", e, false);
		} catch (UnrecoverableEntryException e) {
			PlanDisplayUtil.putLoggerStmt(logger, LogLevel.ERROR, "<----Failed to Get Private Entry From the keystore---->", e, false);
		} catch (KeyStoreException e) {
			PlanDisplayUtil.putLoggerStmt(logger, LogLevel.ERROR, "<----Failed to Get Private Entry From the keystore---->", e, false);
		}
			
		if(pkEntry != null){
			PrivateKey pk = pkEntry.getPrivateKey();
			X509Certificate certificate = (X509Certificate) pkEntry.getCertificate();
			BasicX509Credential credential = new BasicX509Credential();
			credential.setEntityCertificate(certificate);
			credential.setPrivateKey(pk);
			signingCredential = credential;
		}
	}

	public static XMLObjectBuilderFactory getSAMLBuilder() throws ConfigurationException 
	{
		if (builderFactory == null) {
			DefaultBootstrap.bootstrap();
			builderFactory = Configuration.getBuilderFactory();
		}
		return builderFactory;
	}

	/**
	 * Builds a SAML Attribute of type String
	 * 
	 * @param name
	 * @param value
	 * @param builderFactory
	 * @return
	 * @throws ConfigurationException
	 */
	public static Attribute buildStringAttribute(String name, String value)	throws ConfigurationException 
	{
		SAMLObjectBuilder<?> attrBuilder = (SAMLObjectBuilder<?>) getSAMLBuilder().getBuilder(Attribute.DEFAULT_ELEMENT_NAME);
		Attribute attrFirstName = (Attribute) attrBuilder.buildObject();
		attrFirstName.setName(name);

		// Set custom Attributes
		XMLObjectBuilder<?> stringBuilder = getSAMLBuilder().getBuilder(XSString.TYPE_NAME);
		XSString attrValueFirstName = (XSString) stringBuilder.buildObject(AttributeValue.DEFAULT_ELEMENT_NAME, XSString.TYPE_NAME);
		attrValueFirstName.setValue(value);

		attrFirstName.getAttributeValues().add(attrValueFirstName);
		return attrFirstName;
	}

	/**
	 * Helper method which includes some basic SAML fields which are part of
	 * almost every SAML Assertion.
	 * 
	 * @param input
	 * @return
	 */
	public static Assertion buildDefaultAssertion(SAMLInputContainer input) 
	{
		try {
			// Create the NameIdentifier
			SAMLObjectBuilder<?> nameIdBuilder = (SAMLObjectBuilder<?>) SAMLGenerator.getSAMLBuilder().getBuilder(NameID.DEFAULT_ELEMENT_NAME);
			NameID nameId = (NameID) nameIdBuilder.buildObject();
			nameId.setValue(input.getStrNameID());
			nameId.setNameQualifier(input.getStrNameQualifier());
			nameId.setFormat(NameID.UNSPECIFIED);

			// Create the SubjectConfirmation

			SAMLObjectBuilder<?> confirmationMethodBuilder = (SAMLObjectBuilder<?>) SAMLGenerator.getSAMLBuilder().getBuilder(SubjectConfirmationData.DEFAULT_ELEMENT_NAME);
			SubjectConfirmationData confirmationMethod = (SubjectConfirmationData) confirmationMethodBuilder.buildObject();
			DateTime now = TSDateTime.getInstance();
			confirmationMethod.setNotBefore(now);
			confirmationMethod.setNotOnOrAfter(now.plusMinutes(2));

			SAMLObjectBuilder<?> subjectConfirmationBuilder = (SAMLObjectBuilder<?>) SAMLGenerator.getSAMLBuilder().getBuilder(SubjectConfirmation.DEFAULT_ELEMENT_NAME);
			SubjectConfirmation subjectConfirmation = (SubjectConfirmation) subjectConfirmationBuilder.buildObject();
			// subjectConfirmation.setSubjectConfirmationData(confirmationMethod);
			subjectConfirmation.setMethod("urn:oasis:names:tc:SAML:2.0:cm:sender-vouches");
			NameID subConfirmNameId = (NameID) nameIdBuilder.buildObject();
			subConfirmNameId.setValue("CN=GetInsured, OU=GetInsured, O=GetInsured, L=Palo Alto, ST=California, C=US");
			nameId.setNameQualifier(input.getStrNameQualifier());
			nameId.setFormat(NameID.UNSPECIFIED);
			subjectConfirmation.setNameID(subConfirmNameId);

			// Create the Subject
			SAMLObjectBuilder<?> subjectBuilder = (SAMLObjectBuilder<?>) SAMLGenerator.getSAMLBuilder().getBuilder(Subject.DEFAULT_ELEMENT_NAME);
			Subject subject = (Subject) subjectBuilder.buildObject();
			subject.setNameID(nameId);
			subject.getSubjectConfirmations().add(subjectConfirmation);

			// Create Authentication Statement
			SAMLObjectBuilder<?> authStatementBuilder = (SAMLObjectBuilder<?>) SAMLGenerator.getSAMLBuilder().getBuilder(AuthnStatement.DEFAULT_ELEMENT_NAME);
			AuthnStatement authnStatement = (AuthnStatement) authStatementBuilder.buildObject();
			// authnStatement.setSubject(subject);
			// authnStatement.setAuthenticationMethod(strAuthMethod);
			DateTime now2 = TSDateTime.getInstance();
			authnStatement.setAuthnInstant(now2);
			authnStatement.setSessionIndex(input.getSessionId());
			authnStatement.setSessionNotOnOrAfter(now2.plus(input.getMaxSessionTimeoutInMinutes()));

			SAMLObjectBuilder<?> authContextBuilder = (SAMLObjectBuilder<?>) SAMLGenerator.getSAMLBuilder().getBuilder(AuthnContext.DEFAULT_ELEMENT_NAME);
			AuthnContext authnContext = (AuthnContext) authContextBuilder.buildObject();

			SAMLObjectBuilder<?> authContextClassRefBuilder = (SAMLObjectBuilder<?>) SAMLGenerator.getSAMLBuilder().getBuilder(AuthnContextClassRef.DEFAULT_ELEMENT_NAME);
			AuthnContextClassRef authnContextClassRef = (AuthnContextClassRef) authContextClassRefBuilder.buildObject();
			authnContextClassRef.setAuthnContextClassRef("urn:oasis:names:tc:SAML:2.0:ac:classes:Password"); 
			authnContext.setAuthnContextClassRef(authnContextClassRef);

			// Now let us create subject locality
			SAMLObjectBuilder<SubjectLocality> subjectLocalityBuilder = (SAMLObjectBuilder<SubjectLocality>) builderFactory.getBuilder(SubjectLocality.DEFAULT_ELEMENT_NAME);
			SubjectLocality subjectLocality = subjectLocalityBuilder.buildObject();
			//subjectLocality.setDNSName(IP_ADDRESS);
			//subjectLocality.setAddress("1234 Fishy LN, PEORIA, IL 20190");
			authnStatement.setSubjectLocality(subjectLocality);
			authnStatement.setAuthnContext(authnContext);

			// Builder Attributes
			SAMLObjectBuilder<?> attrStatementBuilder = (SAMLObjectBuilder<?>) SAMLGenerator.getSAMLBuilder().getBuilder(AttributeStatement.DEFAULT_ELEMENT_NAME);
			AttributeStatement attrStatement = (AttributeStatement) attrStatementBuilder.buildObject();

			// Create the attribute statement
			Map<String,String> attributes = input.getAttributes();
			if(attributes != null)
			{
				for (Map.Entry<String,String> entry : attributes.entrySet()) {
					Attribute attrFirstName = buildStringAttribute(entry.getKey(), entry.getValue());
					attrStatement.getAttributes().add(attrFirstName);
				}
			}

			// Create the do-not-cache condition
			SAMLObjectBuilder<?> doNotCacheConditionBuilder = (SAMLObjectBuilder<?>) SAMLGenerator.getSAMLBuilder().getBuilder(OneTimeUse.DEFAULT_ELEMENT_NAME);
			Condition condition = (Condition) doNotCacheConditionBuilder.buildObject();

			SAMLObjectBuilder<?> conditionsBuilder = (SAMLObjectBuilder<?>) SAMLGenerator.getSAMLBuilder().getBuilder(Conditions.DEFAULT_ELEMENT_NAME);
			Conditions conditions = (Conditions) conditionsBuilder.buildObject();	
			conditions.getConditions().add(condition);

			// Create Issuer
			SAMLObjectBuilder<?> issuerBuilder = (SAMLObjectBuilder<?>) SAMLGenerator.getSAMLBuilder().getBuilder(Issuer.DEFAULT_ELEMENT_NAME);
			Issuer issuer = (Issuer) issuerBuilder.buildObject();
			issuer.setFormat("urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified");
			issuer.setValue(input.getStrIssuer());

			// Create the assertion
			SAMLObjectBuilder<?> assertionBuilder = (SAMLObjectBuilder<?>) SAMLGenerator.getSAMLBuilder().getBuilder(Assertion.DEFAULT_ELEMENT_NAME);
			Assertion assertion = (Assertion) assertionBuilder.buildObject();
			assertion.setIssuer(issuer);
			assertion.setIssueInstant(now);
			assertion.setVersion(SAMLVersion.VERSION_20);
			assertion.setID("SamlAssertion-25171a8736ed098dde8659e5ba250b5f");
			assertion.setSubject(subject);
			assertion.getAuthnStatements().add(authnStatement);
			assertion.getAttributeStatements().add(attrStatement);
			assertion.setConditions(conditions);

			return assertion;
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(logger, LogLevel.ERROR, "<----Exception in  the method buildDefaultAssertion---->", e, false);
		}
		return null;
	}

	static Response buildResponseWithAssertion(Assertion anAssertion) 
	{
		Response resp = (Response) Configuration.getBuilderFactory().getBuilder(Response.DEFAULT_ELEMENT_NAME).buildObject(Response.DEFAULT_ELEMENT_NAME);
		resp.setID("413be1b7-ac2d-4324-a359-998935f11a66");
		resp.setStatus(SAMLGenerator.buildStatus("Success", null));
		resp.getAssertions().add(anAssertion);
		return resp;
	}

	static Response buildResponse() 
	{
		Response resp = (Response) Configuration.getBuilderFactory().getBuilder(Response.DEFAULT_ELEMENT_NAME).buildObject(Response.DEFAULT_ELEMENT_NAME);
		resp.setID("413be1b7-ac2d-4324-a359-998935f11a66");
		resp.setStatus(SAMLGenerator.buildStatus("urn:oasis:names:tc:SAML:2.0:status:Success", null));
		return resp;
	}

	private static Status buildStatus(String status, String statMsg) 
	{
		Status stat = new StatusBuilder().buildObject();
		// Set the status code
		StatusCode statCode = new StatusCodeBuilder().buildObject();
		statCode.setValue(status);
		stat.setStatusCode(statCode);
		// Set the status Message
		if (statMsg != null) 
		{
			StatusMessage statMesssage = new StatusMessageBuilder().buildObject();
			statMesssage.setMessage(statMsg);
			stat.setStatusMessage(statMesssage);
		}
		return stat;
	}

}
