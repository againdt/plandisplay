package com.getinsured.plandisplay.service.prescriptionSearch;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import com.getinsured.hix.dto.plandisplay.DrugDTO;

public interface PrescriptionSearch {
	
	String getSearchType();


	default Map<String, Future<PrescriptionData>> prepareFuturePlanAvailabilityMap(PrescriptionRequest planAvailabilityForDrugRequest)
	{
		return null;
	}

	default Map<String, List<DrugDTO>> prepareDrugListMap(List<DrugDTO> drugDTOs, List<String> hiosPlanIds, Map<String, Future<PrescriptionData>> futurePlanAvailabilityListMap)
	{
		return null;
	}	
	
	List<DrugDTO> getPlanDrugList(Map<String, List<DrugDTO>> planDrugListMap, String issuerPlanNumber);
}
