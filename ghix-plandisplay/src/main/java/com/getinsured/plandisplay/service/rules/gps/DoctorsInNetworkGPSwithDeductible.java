package com.getinsured.plandisplay.service.rules.gps;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.plandisplay.util.PlanDisplayUtil;

/**
 * There can be 3 entry points to this class.
 * 1. 
 * @author root
 *
 */
public class DoctorsInNetworkGPSwithDeductible extends
		SmartChoiceWithDeductibleGPSImpl {
	
	private static final Integer EXPENSE_WEIGHT_DEDUCTIBLE = 40;
	private static final Integer DEDUCTIBLE_WEIGHT = 10;
	private static final Double SCORE_OUT_OF = 98.0;
	protected static final Integer BENEFIT_WEIGHT = 10;
	private static final Integer DOCTOR_WEIGHT = 10;
	private static final Logger LOGGER = LoggerFactory.getLogger(DoctorsInNetworkGPSwithDeductible.class);
	
	@Override
	public Map<Integer, Double> calculateUsingDeductibleAndDoctorSearch(
			Map<Integer, Double> expense, Map<Integer, Double> deductible,
			Map<Integer, Integer> benefits, Integer benefitsSelected,
			Map<Integer, Integer> doctorsInNetwork, Integer doctorsSelected) {
		
		Map<Integer, Double> scores = null;
		
		/**
		 * if deductible is included go the other route
		 * Let's assume deductible is included for a while
		 */
		
		
		
		if(doctorsSelected>0){
			scores = computeRawScore(expense, deductible, benefits, benefitsSelected, doctorsInNetwork, doctorsSelected);
		}else{
			throw new UnsupportedOperationException();
		}
		
		consoleDebug("GPS: Raw scores - before scaling to 100", scores);
		scores = disperseScoresBelowThreshold(scores);
		consoleDebug("GPS: final scores", scores);
		return scores;
		
	}
	
	/**
	 * This method takes deductible into account.
	 * @param expense
	 * @param deductible
	 * @param benefit
	 * @param benefitsSelected
	 * @return
	 */
	private Map<Integer, Double> computeRawScore(Map<Integer, Double> expense, Map<Integer, Double> deductible, Map<Integer, Integer> benefit, Integer benefitsSelected, Map<Integer, Integer> doctorsInNW, Integer doctorsSelected){
		Double idealPlanScore = EXPENSE_WEIGHT_DEDUCTIBLE + DEDUCTIBLE_WEIGHT +BENEFIT_WEIGHT*benefitsSelected*1.0 + DOCTOR_WEIGHT*doctorsSelected*1.0;
		
		Map<Integer, Double> expenseScore = computeScoreFromExpense(expense);
		Map<Integer, Double> deductibleScore = computeScoreFromDeductible(deductible);
		consoleDebug("GPS: Scores from expenses", expenseScore);
		consoleDebug("GPS: Scores from deductible", deductibleScore);
		Map<Integer, Integer> benefitScore = computeScoreFromBenefits(benefit);
		consoleDebugBenefits("GPS: Scores from benefits", benefitScore);
		Map<Integer, Integer> doctorsScore = computeScoreFromDoctorsAvailability(doctorsInNW);
		consoleDebugBenefits("GPS: doctors scores", doctorsScore);
		
		
		for(Map.Entry<Integer, Double> entry : expenseScore.entrySet()){
			entry.setValue(entry.getValue() + deductibleScore.get(entry.getKey()) + benefitScore.get(entry.getKey()) + doctorsScore.get(entry.getKey()));
			//Normalization with idealPlanScore
			entry.setValue((entry.getValue()/idealPlanScore)*SCORE_OUT_OF);
			entry.setValue(Double.valueOf(SCORE_FORMAT.format(Math.round(entry.getValue()))));
		}
		
		return expenseScore;
	}
	
	/**
	 * 
	 */
	private Map<Integer, Double> computeScoreFromExpense(Map<Integer, Double> expense){
		Map<Integer, Double> expenseScore = computeScaledRatio(expense);
		for(Map.Entry<Integer, Double> entry : expenseScore.entrySet()){
			entry.setValue(entry.getValue()*EXPENSE_WEIGHT_DEDUCTIBLE);
		}
		
		return expenseScore;
	}
	
	/**
	 * Compute scores we get from doctors presence 
	 * @param doctorsPresence
	 * @return
	 */
	private Map<Integer, Integer> computeScoreFromDoctorsAvailability(Map<Integer, Integer> doctorsPresence){
		Map<Integer, Integer> doctorsScore = new HashMap<Integer, Integer>(doctorsPresence);
		for(Map.Entry<Integer, Integer> entry : doctorsScore.entrySet()){
			entry.setValue(entry.getValue()*DOCTOR_WEIGHT);
		}
		
		return doctorsScore;
	}
	
	public static void main(String args[]){
		GPSCalculatorService gps = new DoctorsInNetworkGPSwithDeductible();
		Map<Integer, Double> expense = new HashMap<Integer, Double>();
		Map<Integer, Double> deductible = new HashMap<Integer, Double>();
		Map<Integer, Integer> benefits = new HashMap<Integer, Integer>();
		Map<Integer, Integer> doctorsInNW = new HashMap<Integer, Integer>();
		
		expense.put(203, 3822.7);
		expense.put(206, 3354.0);
		expense.put(241, 4143.0);
		
		deductible.put(203, 3250.0);
		deductible.put(206, 2500.0);
		deductible.put(241, 6300.0);
		
		benefits.put(203, 1);
		benefits.put(206, 2);
		benefits.put(241, 0);
		
		doctorsInNW.put(203, 1);
		doctorsInNW.put(206, 2);
		doctorsInNW.put(241, 0);
		
		
		
		Map<Integer, Double> scores = gps.calculateUsingDeductibleAndDoctorSearch(expense, deductible, benefits, 2, doctorsInNW, 2);
		for(Map.Entry<Integer, Double> score : scores.entrySet()){
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "#" + score.getKey() + " - " + score.getValue() + "%", null, true);
		}
		
		
		
		
	}
	
	

}
