package com.getinsured.plandisplay.service;

import java.util.List;

import com.getinsured.hix.model.PdOrderItem;
import com.getinsured.hix.model.PdOrderItemPerson;
import com.getinsured.hix.model.plandisplay.PdSaveCartRequest;

public interface PdOrderItemPersonService {
	
	void saveItemPerson(PdOrderItem newPdOrderItem, PdSaveCartRequest pdSaveCartRequest);
	List<PdOrderItemPerson> getByOrderItemId(Long orderItemId);
	void deleteByOrderItemId(Long orderItemId);
	Long countOrderItemPersonsByOrderItemId(Long orderItemId);
	
}
