package com.getinsured.plandisplay.service.providerSearch;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;

@Component
public class ProviderFactory implements ApplicationContextAware {

	/*@Value("#{configProp['providerSearchSource']}")
	private String providerName = null;*/
	
	private ProviderSearch providerSearch;
	private static ApplicationContext applicationContext;
	private void initProviderSearch()
	{
		String providerName = DynamicPropertiesUtil.getPropertyValue("planSelection.providerSearchAPI");
		if(StringUtils.isBlank(providerName))
		{
			providerName = "defaultProvider";
		}
		providerSearch = (ProviderSearch) applicationContext.getBean(providerName);
	}

	public ProviderSearch getObject() {
		if (providerSearch == null)
		{
			initProviderSearch();
		}
		return providerSearch;
	}
	
	@Override
	public void setApplicationContext(ApplicationContext appContext) throws BeansException {
		applicationContext = appContext;
		
	}
	
	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}
}
