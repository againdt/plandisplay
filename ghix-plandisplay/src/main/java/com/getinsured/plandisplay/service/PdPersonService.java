package com.getinsured.plandisplay.service;

import java.util.List;

import com.getinsured.hix.model.PdPerson;

public interface PdPersonService {
	PdPerson savePerson(PdPerson person);
	List<PdPerson> savePerson(List<PdPerson> personList);
	PdPerson findById(Long id);
	List<PdPerson> getPersonListByHouseholdId(Long householdId);
}
