package com.getinsured.plandisplay.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.plandisplay.DrugDataDTO;
import com.getinsured.hix.dto.plandisplay.DrugDataRequestDTO;
import com.getinsured.hix.dto.plandisplay.DrugDataResponseDTO;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.plandisplay.util.PlanDisplayUtil;

/**
 * Class is used to provide service for Prescription Drug Data.
 * 
 * @since October 23, 2018
 */
@Service("pdDrugService")
public class PdDrugServiceImpl implements PdDrugService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PdDrugServiceImpl.class);
	@PersistenceUnit private EntityManagerFactory emf;

	/**
	 * Method is used to get Drug Data Response by RXCUI List.
	 */
	@Override
	public void getDrugDataResponseByRxcuiList(DrugDataRequestDTO requestDTO, DrugDataResponseDTO responseDTO, HttpServletResponse response) {

		LOGGER.debug("getDrugDataResponseByRxcuiList() Start");

		if (CollectionUtils.isEmpty(requestDTO.getRxcuiList())) {
			response.setHeader("FailureReason", "Empty input parameters");
			response.setStatus(HttpStatus.PRECONDITION_FAILED.value());
			return;
		}

		EntityManager entityManager = null;

		try {

			String prescriptionSearchConfig = DynamicPropertiesUtil.getPropertyValue("planSelection.prescriptionSearch");

			if (StringUtils.isNotBlank(prescriptionSearchConfig) && "ON".equalsIgnoreCase(prescriptionSearchConfig)) {

				entityManager = emf.createEntityManager();
				int totalRecordCount = getRecordCountForDrugDataByRxcuiList(entityManager, requestDTO, response);
				responseDTO.setTotalRecordCount(totalRecordCount);

				if (0 < totalRecordCount) {

					List<DrugDataDTO> drugDataList = getDrugDataByRxcuiList(entityManager, requestDTO, response);

					if (CollectionUtils.isNotEmpty(drugDataList)) {
						responseDTO.setDrugDataList(drugDataList);
						responseDTO.setCurrentRecordCount(drugDataList.size());
					}
				}
				responseDTO.setPageSize(requestDTO.getPageSize());
			}
			else {
				LOGGER.error("Prescription Search is Not Implemeted for configuration: " + prescriptionSearchConfig);
				response.setHeader("FailureReason", "Not configured");
				response.setStatus(HttpStatus.NOT_IMPLEMENTED.value());
			}
		}
		catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "----Exception occured while getting Drug Data Response by RXCUI List ----", e, false);
			response.setHeader("FailureReason", e.getMessage());
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		finally {

			if (entityManager != null && entityManager.isOpen()) {
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
			LOGGER.debug("getDrugDataResponseByRxcuiList() End");
		}
	}

	/**
	 * Method is used to get Record Count for Drug Data by RXCUI List.
	 */
	private int getRecordCountForDrugDataByRxcuiList(EntityManager entityManager, DrugDataRequestDTO requestDTO, HttpServletResponse response) {

		LOGGER.debug("getRecordCountForDrugDataByRxcuiList() Start");
		int totalRecordCount = 0;

		try {

			StringBuffer queryStr = new StringBuffer("SELECT COUNT(drugData) ");
			queryStr.append("FROM DrugData AS drugData ");
			queryStr.append("WHERE drugData.rxcui IN (:rxcuiList)");

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Drug Data Count Query: " + queryStr);
			}

			Query query = entityManager.createQuery(queryStr.toString());
			query.setParameter("rxcuiList", requestDTO.getRxcuiList());

			Object objCount = query.getSingleResult();
			if (objCount != null) {
				totalRecordCount = Integer.parseInt(objCount.toString());
			}
			LOGGER.info("Total Record Count for Drug data by RXCUI List: " + totalRecordCount);
		}
		catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "----Exception occured while getting Total Record Count for Drug data by RXCUI List ----", e, false);
			response.setHeader("FailureReason", e.getMessage());
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		finally {
			LOGGER.debug("getRecordCountForDrugDataByRxcuiList() End");
		}
		return totalRecordCount;
	}

	/**
	 * Method is used to get Drug Data by RXCUI List.
	 */
	private List<DrugDataDTO> getDrugDataByRxcuiList(EntityManager entityManager, DrugDataRequestDTO requestDTO, HttpServletResponse response) {

		LOGGER.debug("getDrugDataByRxcuiList() Start");
		List<DrugDataDTO> drugDataList = null;

		try {

			StringBuffer queryStr = new StringBuffer("SELECT new com.getinsured.hix.dto.plandisplay.DrugDataDTO(drugData.rxcui, "
					+ "drugData.name, drugData.strength, drugData.route, drugData.fullName, drugData.rxTermDosageForm, drugData.rxNormDosageForm, "
					+ "drugData.type, drugData.genericName, drugData.genericRxcui, drugData.status, drugData.remappedTo) ");
			queryStr.append("FROM DrugData AS drugData ");
			queryStr.append("WHERE drugData.rxcui IN (:rxcuiList) ");
			queryStr.append("ORDER BY drugData.rxcui ASC");

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Drug Data Query: " + queryStr);
			}

			Query query = entityManager.createQuery(queryStr.toString());
			query.setParameter("rxcuiList", requestDTO.getRxcuiList());
			query.setFirstResult(requestDTO.getStartRecord());
			query.setMaxResults(requestDTO.getPageSize());
			drugDataList = query.getResultList();
		}
		catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "----Exception occured while getting Drug Data by RXCUI List ----", e, false);
			response.setHeader("FailureReason", e.getMessage());
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		finally {
			LOGGER.debug("getDrugDataByRxcuiList() End");
		}
		return drugDataList;
	}
}
