package com.getinsured.plandisplay.service.prescriptionSearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.plandisplay.DrugDTO;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.plandisplay.util.PlanDisplayUtil;

@Service(value="vericredPrescriptionSearch")
public class VericredPrescriptionSearch implements PrescriptionSearch {
	private static final Logger LOGGER = LoggerFactory.getLogger(VericredPrescriptionSearch.class);
	
	@Autowired PlanDisplayUtil planDisplayUtil;
	@Autowired ZipCodeService zipCodeService;

	@Override
	public String getSearchType() {
		return "VericredPrescriptionSearch";
	}
	
	@Override
	public Map<String, Future<PrescriptionData>> prepareFuturePlanAvailabilityMap(PrescriptionRequest prescriptionRequest)
	{
		if(prescriptionRequest != null )
		{
			List<String> drugList = planDisplayUtil.prepareNdcList(prescriptionRequest.getDrugDTOList());
			return planDisplayUtil.prepareDrugHiosMap(drugList,prescriptionRequest.getState());
		}
		return null;
	}

	public Map<String, List<DrugDTO>> prepareDrugListMap(List<DrugDTO> drugDTOs, List<String> hiosPlanIds, Map<String, Future<PrescriptionData>> futurePlanAvailabilityListMap)
	{
		Map<String, List<DrugDTO>> planDrugListMap = new HashMap<String, List<DrugDTO>>();
		for(String hiosPlanNumber : hiosPlanIds) {
			List<DrugDTO> planDrugList = processDrugListForMap(futurePlanAvailabilityListMap, drugDTOs, hiosPlanNumber);
			planDrugListMap.put(hiosPlanNumber, planDrugList);
		}
		return planDrugListMap;
	}	

	private List<DrugDTO> processDrugListForMap(Map<String, Future<PrescriptionData>> futurePlanAvailabilityListMap,
			List<DrugDTO> drugList, String hiosPlanNumber) {
		DrugDTO drugResponseDTO;
		List<DrugDTO> planDrugList = new ArrayList<DrugDTO>();
		hiosPlanNumber = PlanDisplayUtil.create14DigitNum(hiosPlanNumber);
		if(drugList != null && drugList.size() > 0){
			for (DrugDTO drugDTO : drugList){
				drugResponseDTO = drugDTO.clone();
				drugResponseDTO.setDrugTier("not_listed");
				drugResponseDTO.setIsDrugCovered("U");
				
				drugResponseDTO = planDisplayUtil.updatePlanAvailabilityCoverage(futurePlanAvailabilityListMap, drugResponseDTO, hiosPlanNumber, drugDTO.getDrugNdc(), false);
				if(StringUtils.isNotBlank(drugDTO.getGenericNdc())){
					drugResponseDTO.setGenericTier("not_listed");
					drugResponseDTO.setIsGenericCovered("U");
					drugResponseDTO = planDisplayUtil.updatePlanAvailabilityCoverage(futurePlanAvailabilityListMap, drugResponseDTO, hiosPlanNumber, drugDTO.getGenericNdc(), true);
				}
				planDrugList.add(drugResponseDTO);
			}
		}
		return planDrugList;
	}
		
	@Override
	public List<DrugDTO> getPlanDrugList(Map<String, List<DrugDTO>> planDrugListMap, String issuerPlanNumber) {
		List<DrugDTO> planDrugList = null;
		if (planDrugListMap != null){
			planDrugList = new ArrayList<DrugDTO>();
			if (planDrugListMap.get(issuerPlanNumber) != null && planDrugListMap.get(issuerPlanNumber).size() > 0 ){
				planDrugList = planDrugListMap.get(issuerPlanNumber);
			}
		}
		return planDrugList;
	}
}
