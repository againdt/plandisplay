package com.getinsured.plandisplay.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.PdPerson;
import com.getinsured.plandisplay.repository.PdPersonRepository;

@Service("pdPersonService")
public class PdPersonServiceImpl implements PdPersonService{

	@Autowired private PdPersonRepository pdPersonRepository;

	@Override @Transactional
	public PdPerson savePerson(PdPerson person) {
		return pdPersonRepository.save(person);
	}
	
	@Override @Transactional
	public List<PdPerson> savePerson(List<PdPerson> personList) {
		return pdPersonRepository.save(personList);
	}

	@Override
	public PdPerson findById(Long id) {
		if(pdPersonRepository.exists(id)){
			return pdPersonRepository.findOne(id);
		}
		return null;
	}

	@Override
	public List<PdPerson> getPersonListByHouseholdId(Long householdId){
		return pdPersonRepository.findPersonListByHouseholdId(householdId);
	}
}
