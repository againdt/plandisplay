package com.getinsured.plandisplay.service.prescriptionSearch;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;

@Component
public class PrescriptionFactory implements ApplicationContextAware{
	
	private PrescriptionSearch prescriptionSearch;
	private ApplicationContext applicationContext;
	
	private void initProviderSearch()
	{
		String prescriptionProvider = DynamicPropertiesUtil.getPropertyValue("planSelection.prescriptionSearchAPI");
		if(StringUtils.isBlank(prescriptionProvider))
		{
			prescriptionProvider = "GIPrescriptionSearch";
		}
			prescriptionSearch = (PrescriptionSearch) applicationContext.getBean(prescriptionProvider);
	}

	public PrescriptionSearch getObject() {
		if (prescriptionSearch == null)
		{
			initProviderSearch();
		}
		return prescriptionSearch;
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
		
	}
}
