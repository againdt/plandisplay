package com.getinsured.plandisplay.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.plandisplay.PdHouseholdDTO;
import com.getinsured.hix.dto.plandisplay.PdPersonDTO;
import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;
import com.getinsured.hix.model.IndividualPlan;
import com.getinsured.hix.model.plandisplay.PdQuoteRequest;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentFlowType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;

public interface IndividualPlanService {
	List<IndividualPlan> getPlans(PdQuoteRequest pdQuoteRequest);
	Map<String, Object> findPlanDetailsFromSolr(String planIds);
	Map<String,String> getSubscriberData(List<PdPersonDTO> seekingCovgPersonList, PdHouseholdDTO pdHouseholdDTO, String initialSubscriberId, EnrollmentFlowType specialEnrollmentFlowType, InsuranceType insuranceType);
	Map<String, Integer> formMedicalUsageMap(PdPreferencesDTO pdPreferencesDTO, int memberCount);
	Map<String, Integer> formDrugsUsageMap(PdPreferencesDTO pdPreferencesDTO, int memberCount);
	IndividualPlan getHouseholdEnrolledPlanInfo(PdQuoteRequest pdQuoteRequest);
}
