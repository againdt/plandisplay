/**
 * 
 */
package com.getinsured.plandisplay.service;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.methods.HttpGet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.getinsured.hix.dto.consumer.goodrx.GoodRxResponseDTO;
import com.getinsured.hix.platform.util.HIXHTTPClient;
import com.getinsured.plandisplay.util.GoodRxUtil;
import com.google.gson.Gson;

@Service("goodRxService")
public class GoodRxServiceImpl implements GoodRxService {
	@Autowired
	@Qualifier("hIXHTTPClient")
	private HIXHTTPClient hixhttpClient;
	private static final Logger LOGGER = LoggerFactory.getLogger(GoodRxServiceImpl.class);
	
	@Override
	public GoodRxResponseDTO getFairPriceByNDC(String ndc) {
		String queryString = createPairBasedQueryString("ndc="+ndc);
		String response  = getData(GoodRxService.FAIR_PRICE_SERVICE_ENDPOINT,queryString);
		GoodRxResponseDTO goodRxResp = null;
		if(StringUtils.isNotEmpty(response)){
			Gson gson = new Gson();
			LOGGER.info("Fair Price response: "+response);
			goodRxResp = gson.fromJson(response, GoodRxResponseDTO.class);
		}
		return goodRxResp;
	}
	
	@Override
	public GoodRxResponseDTO getFairPrice(String name) {
		String queryString = createPairBasedQueryString("name="+name);
		String response  = getData(GoodRxService.FAIR_PRICE_SERVICE_ENDPOINT,queryString);
		GoodRxResponseDTO goodRxResp = null;
		if(StringUtils.isNotEmpty(response)){
			Gson gson = new Gson();
			LOGGER.info("Fair Price response: "+response);
			goodRxResp = gson.fromJson(response, GoodRxResponseDTO.class);
		}
		return goodRxResp;
	}
	
	public String getData(String url, String queryString) {
		String response = null;
		try {
			response = hixhttpClient.getGETData(url +queryString);
			LOGGER.info("Response for the url: "+url +" with queryString: "+queryString+" is :"+response);
		} catch (RestClientException re) {
			LOGGER.error("Error connecting rest client:", re);
		} catch (Exception e) {
			LOGGER.error("Error reading enrollment record.", e);
		}

		return response;
	}

	/**
	 * append API key to queryString
	 * 
	 * @param queryStr
	 */
	public void appendAPIKey(StringBuffer queryStr) {
		if (queryStr != null) {
			if (queryStr.length() > 1) {
				queryStr.append("&");
			}
			queryStr.append("api_key=");
			queryStr.append(GoodRxService.API_KEY);
		}
	}

	/**
	 * Creates a query string where input is like paramname=valuestring
	 * 
	 * @param args
	 * @return
	 */
	public String createPairBasedQueryString(String args) {
		// break key val pairs
		String[] keyVal = GoodRxUtil.parseArgs(args);
		if (keyVal == null) {
			return null;
		}
		StringBuffer queryString = new StringBuffer();
		boolean first = true;
		String[] keyvalpair;
		for (String pair : keyVal) {
			keyvalpair = GoodRxUtil.parseKeyVal(pair);
			if (keyvalpair != null) {
				if (first) {
					first = false;
				} else {
					queryString.append("&");
				}
				queryString.append(keyvalpair[0]);
				queryString.append("=");
				queryString.append(keyvalpair[1]);
			} else {
				LOGGER.error("INVALID args: " + pair);
			}
		}

		// append api key
		appendAPIKey(queryString);

		return appendSignature(queryString.toString());
	}

	/**
	 * Create a query string where input is just a string for example input for
	 * drug-search api. Further we append signature and other apikey
	 * 
	 * @return
	 */
	public String createSimpleQueryString(String args) {
		StringBuffer queryString = new StringBuffer();
		if (args == null || args.isEmpty()) {
			return null;
		}
		/**
		 * add input string as query
		 */
		queryString.append("query=");
		queryString.append(args);

		// append api key
		appendAPIKey(queryString);

		return appendSignature(queryString.toString());
		//return queryString.toString();
	}

	public String appendSignature(String query) {
		StringBuffer queryString = new StringBuffer();
		queryString.append(query);
		// create signature
		String signature = GoodRxUtil.createSignature(query);

		queryString.append("&");
		queryString.append("sig");
		queryString.append("=");
		queryString.append(signature);
		return queryString.toString();
	}
	
	@Override
	public GoodRxResponseDTO getDrugNames(String drugSynonym) {
		String queryString = createPairBasedQueryString("query="+drugSynonym);
		String response  = getData(GoodRxService.DRUG_SEARCH_SERVICE_ENDPOINT,queryString);
		GoodRxResponseDTO goodRxResp = null;
		if(StringUtils.isNotEmpty(response)){
			Gson gson = new Gson();
			LOGGER.info("Drug Search response: "+response);
			goodRxResp = gson.fromJson(response, GoodRxResponseDTO.class);
		}
		return goodRxResp;
	}
}