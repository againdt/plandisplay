package com.getinsured.plandisplay.service.prescriptionSearch;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.planmgmt.PrescriptionDTO;

public class PrescriptionData {

	Map<String, Object> vericredPrescriotions;
	
	List<PrescriptionDTO> giPrescriptions;

	public Map<String, Object> getVericredPrescriotions() {
		return vericredPrescriotions;
	}

	public void setVericredPrescriotions(Map<String, Object> vericredPrescriotions) {
		this.vericredPrescriotions = vericredPrescriotions;
	}

	public List<PrescriptionDTO> getGiPrescriptions() {
		return giPrescriptions;
	}

	public void setGiPrescriptions(List<PrescriptionDTO> giPrescriptions) {
		this.giPrescriptions = giPrescriptions;
	}

}
