package com.getinsured.plandisplay.service.providerSearch;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.plandisplay.ProviderSearchRequest;
import com.getinsured.hix.model.IndividualPlan;
import com.getinsured.hix.model.plandisplay.ProviderBean;

@Service(value="defaultProvider")
public class DefaultProviderSearch implements ProviderSearch {
	
	@Override
	public String getSearchType() {
		return "defaultProvider";
	}
	
	@Override
	public Map<String, Future<Map<String, Object>>> prepareProviderHiosMap(List<ProviderBean> providers) {
		return null;
	}

	@Override
	public String getNetworkStatus(Map<String, Future<Map<String, Object>>> futureProviderResponseListMap, IndividualPlan individualPlan, ProviderBean providerBean, List<Map<String, String>> providersInPlan) {
		for (Map<String, String> map : providersInPlan) {
			if (providerBean.getId().equals(map.get("providerId"))){
				return map.get("networkStatus");
			}
		}
		return null;
	}
	
	@Override
	public Map<String,Object> searchProvider(ProviderSearchRequest providerSearchRequest) {
		return null;
	}
}
