package com.getinsured.plandisplay.service.rules.gps;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.plandisplay.util.PlanDisplayUtil;

/**
 * This algorithm is based on story as mentioned in HIX-24285
 * 
 * @since 12/30/2013
 * @author bhatt_s
 *
 */
public class SmartChoiceScoreAlgorithmGPSImpl implements GPSCalculatorService {
	
	private static final Integer EXPENSE_WEIGHT = 60;
	protected static final Integer BENEFIT_WEIGHT = 10;
	private static final Double MAX_POSSIBLE_SCORE = 98.0;
	private static final Double MIN_POSSIBLE_SCORE = 41.0;
	private static final Double LOW_CURVE_THRESHOLD = 50.0;
	/**
	 * We want max possible score to be out of 93 instead of 100;
	 */
	private static final Double SCORE_OUT_OF = 93.0;
	protected static final DecimalFormat SCORE_FORMAT = new DecimalFormat("#.#");
	private static final Logger LOGGER = LoggerFactory.getLogger(SmartChoiceScoreAlgorithmGPSImpl.class);

	/**
	 * Algorithm entry point
	 * @param expense - (planid, approximateExpense)
	 * @param benefits - (planid, countOfBenefitsAvailableInThePlan)
	 * @param benefitsSelected - countOfBenefits
	 */
	@Override
	public Map<Integer, Double> calculate(Map<Integer, Double> expense,
			Map<Integer, Integer> benefits, Integer benefitsSelected) {
		Map<Integer, Double> scores = computeRawScore(expense, benefits, benefitsSelected);
		//scores = avoidPerfectAndHorriblePlans(scores);
		consoleDebug("GPS: Raw scores - before scaling to 100", scores);
		scores = disperseScoresBelowThreshold(scores);
		consoleDebug("GPS: final scores", scores);
		return scores;
	}
	
	/**
	 * Put a min/max filter. 
	 * if score>MAX_POSSIBLE_VALUE set it to MAX_POSSIBLE_VALUE
	 * and similarly for MIN_POSSIBLE_VALUE
	 * @param rawScore
	 * @return
	 */
	/*private Map<Integer, Double> avoidPerfectAndHorriblePlans(Map<Integer, Double> rawScore){
		Map<Integer, Double> displayScores = new HashMap<Integer, Double>(rawScore);
		for(Map.Entry<Integer, Double> entry : displayScores.entrySet()){
			if(entry.getValue()>MAX_POSSIBLE_SCORE){
				entry.setValue(MAX_POSSIBLE_SCORE);
			}else if(entry.getValue()<MIN_POSSIBLE_SCORE){
				entry.setValue(MIN_POSSIBLE_SCORE);
			}
		}
		return displayScores;
	}*/
	
	/**
	 * computes raw score based on summation of expense and benefits
	 * Further normalized by ideal score
	 * @param expense
	 * @param benefit
	 * @param benefitsSelected
	 * @return
	 */
	private Map<Integer, Double> computeRawScore(Map<Integer, Double> expense, Map<Integer, Integer> benefit, Integer benefitsSelected){
		Double idealPlanScore = EXPENSE_WEIGHT + BENEFIT_WEIGHT*benefitsSelected*1.0;
		
		Map<Integer, Double> expenseScore = computeScoreFromExpense(expense);
		consoleDebug("GPS: Scores from expenses", expenseScore);
		Map<Integer, Integer> benefitScore = computeScoreFromBenefits(benefit);
		consoleDebugBenefits("GPS: Scores from benefits", benefitScore);
		for(Map.Entry<Integer, Double> entry : expenseScore.entrySet()){
			entry.setValue(entry.getValue() + benefitScore.get(entry.getKey()));
			//Normalization with idealPlanScore
			entry.setValue((entry.getValue()/idealPlanScore)*SCORE_OUT_OF);
			entry.setValue(Double.valueOf(SCORE_FORMAT.format(Math.round(entry.getValue()))));
		}
		
		return expenseScore;
	}
	
	/**
	 * Returns expenseRatio*EXPENSE_WEIGHT
	 * @param expense
	 * map of plan and approximate expense
	 * @return
	 */
	private Map<Integer, Double> computeScoreFromExpense(Map<Integer, Double> expense){
		Map<Integer, Double> expenseScore = computeScaledRatio(expense);
		for(Map.Entry<Integer, Double> entry : expenseScore.entrySet()){
			entry.setValue(entry.getValue()*EXPENSE_WEIGHT);
		}
		
		return expenseScore;
	}
	
	/**
	 * Returns Sum(BENEFIT_WEIGHT*count) for each plan
	 * @param benefits
	 * map of each plan and count of benefits covered
	 * @return
	 */
	protected Map<Integer, Integer> computeScoreFromBenefits(Map<Integer, Integer> benefits){
		Map<Integer, Integer> benefitScore = new HashMap<Integer, Integer>(benefits);
		for(Map.Entry<Integer, Integer> entry : benefitScore.entrySet()){
			entry.setValue(entry.getValue()*BENEFIT_WEIGHT);
		}
		
		return benefitScore;
	}
	
	/**
	 * For each plan BEGIN
	 * 	DO
	 * 		estimate = 1000/expense --------------(step 1)
	 * 		inverseScore = estimate/(max_across_all_plans(step 1))--------------(step 2)
	 * 		scaledRatio = inverseScore/Max(max_across_all_plans(step 2))
	 * 	DONE
	 * END	
	 * @param expense
	 * @return
	 */
	protected Map<Integer, Double> computeScaledRatio(Map<Integer, Double> expense){
		Map<Integer, Double> planValues = new HashMap<Integer, Double>(expense);
		consoleDebug("GPS: Before Computing Scaled Ratio", planValues);
		//calculate estimate
		for(Map.Entry<Integer, Double> entry : planValues.entrySet()){
			if(entry.getValue()>1){
				entry.setValue(1000/entry.getValue());
			}else{
				entry.setValue(0.0); //avoid division by zero
			}
			
		}
		
		//inverseScore
		Double maxEstimate = Collections.max(planValues.values());
		for(Map.Entry<Integer, Double> entry : planValues.entrySet()){
			entry.setValue(entry.getValue()/maxEstimate);
		}
		
		
		//scale the ratio
		Double maxInverseScore = Collections.max(planValues.values());
		for(Map.Entry<Integer, Double> entry : planValues.entrySet()){
			entry.setValue(entry.getValue()/maxInverseScore);
		}
		
		return planValues;
	}
	
	/**
	 * Disperse all scores below 50
	 * @param scores
	 * @return
	 */
	protected Map<Integer, Double> disperseScoresBelowThreshold(Map<Integer, Double> scores){
		Map<Integer, Double> result = new HashMap<Integer, Double>(scores);
		List<Integer> belowThresholdGuys = new ArrayList<Integer>();
		Double minVal=99.0;
		
		//find all below threshold score 50
		for(Map.Entry<Integer, Double> item : result.entrySet()){
			if(item.getValue()>MAX_POSSIBLE_SCORE){
				result.put(item.getKey(), MAX_POSSIBLE_SCORE);
			}
			if(item.getValue()<LOW_CURVE_THRESHOLD){
				belowThresholdGuys.add(item.getKey());
				if(minVal>item.getValue()){
					minVal = item.getValue();
				}
			}
		}
		
		//normalize all others
		/**
		 * lowest dude gets score of 41 and 49 for max guy
		 * and relatively others get dispersed in between
		 * 
		 * this algorithm triggers only if our min falls below 40
		 */
		if(minVal<MIN_POSSIBLE_SCORE){
			double base = LOW_CURVE_THRESHOLD - minVal; //actual dispersion
			double value;
			for(Integer key : belowThresholdGuys){
				value = result.get(key);
				value = MIN_POSSIBLE_SCORE + 9*((value - minVal)/base);
				result.put(key, Double.valueOf(SCORE_FORMAT.format(Math.round(value))));
			}
		}
		return result;
		
	}
	
	public static void main(String args[]){
		SmartChoiceScoreAlgorithmGPSImpl imp = new SmartChoiceScoreAlgorithmGPSImpl();
		Map<Integer, Double> planData = new HashMap<Integer, Double>();
		planData.put(1, 710.0);
		planData.put(2, 710.0);
		planData.put(3, 2200.0);
		planData.put(4, 1080.0);
		planData.put(5, 1080.0);
		planData.put(6, 3817.5);
		planData.put(7, 917.5);
		planData.put(8, 944.9);
		planData.put(9, 944.9);
		planData.put(10, 944.9);
		
		Map<Integer, Integer> benefits = new HashMap<Integer, Integer>();
		benefits.put(1, 2);
		benefits.put(2, 1);
		benefits.put(3, 2);
		benefits.put(4, 1);
		benefits.put(5, 2);
		benefits.put(6, 0);
		benefits.put(7, 0);
		benefits.put(8, 2);
		benefits.put(9, 2);
		benefits.put(10, 1);
		
		Map<Integer, Double> results = imp.calculate(planData, benefits, 2);
		imp.consoleDebug("final results", results);
	}
	
	public void consoleDebug(String message, Map<Integer, Double> result){
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "<----"+message+"---->", null, true);
		for(Map.Entry<Integer, Double> entry : result.entrySet()){
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, entry.getKey() + " : " + entry.getValue(), null, true);
		}
	}
	
	public void consoleDebugBenefits(String message, Map<Integer, Integer> result) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "<----"+message+"---->", null, true);
		for (Map.Entry<Integer, Integer> entry : result.entrySet()) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, entry.getKey() + " : " + entry.getValue(), null, true);
		}
	}

	@Override
	public Map<Integer, Double> calculateUsingDeductible(
			Map<Integer, Double> expense, Map<Integer, Double> deductible,
			Map<Integer, Integer> benefits, Integer benefitsSelected) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public Map<Integer, Double> calculateUsingDeductibleAndDoctorSearch(
			Map<Integer, Double> expense, Map<Integer, Double> deductible,
			Map<Integer, Integer> benefits, Integer benefitsSelected,
			Map<Integer, Integer> doctorsInNetwork, Integer doctorsSelected) {
		throw new UnsupportedOperationException();
	}

}
