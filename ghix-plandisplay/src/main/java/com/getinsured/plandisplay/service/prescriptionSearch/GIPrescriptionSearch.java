package com.getinsured.plandisplay.service.prescriptionSearch;

import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TimeShifterUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.plandisplay.DrugDTO;
import com.getinsured.hix.dto.planmgmt.PrescriptionDTO;
import com.getinsured.hix.dto.planmgmt.PrescriptionSearchByHIOSIdRequestDTO;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.plandisplay.util.PlanDisplayAsyncUtil;
import com.getinsured.plandisplay.util.PlanDisplayUtil;

@Service(value="GIPrescriptionSearch")
public class GIPrescriptionSearch implements PrescriptionSearch {
	private static final Logger LOGGER = LoggerFactory.getLogger(GIPrescriptionSearch.class);

	@Autowired
	PlanDisplayUtil planDisplayUtil;
	
	@Autowired
	PlanDisplayAsyncUtil planDisplayAsyncUtil;
	
	@Override
	public String getSearchType() {
		return "GIPrescriptionSearch";
	}
	
	@Override
	public Map<String, Future<PrescriptionData>> prepareFuturePlanAvailabilityMap(final PrescriptionRequest planAvailabilityForDrugRequest)
	{
		if(planAvailabilityForDrugRequest != null && planAvailabilityForDrugRequest.getPlanHiosList() != null)
		{	
			long startTime = TimeShifterUtil.currentTimeMillis();
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "----preparedrughiosmap started----", null, false);
			Map<String, Future<PrescriptionData>> responseObj = null;
			List<String> drugRxCodeList = null;
			if(!drugDtosHasRxCodes(planAvailabilityForDrugRequest.getDrugDTOList()))
			{
				drugRxCodeList = new ArrayList<>();
				Map<String, String> rxCodeNdcMapping = planDisplayUtil.getRxCodesFromNdc(planDisplayUtil.prepareNdcList(planAvailabilityForDrugRequest.getDrugDTOList()));
				updateDrugDtosWithRxCodes(planAvailabilityForDrugRequest.getDrugDTOList(),rxCodeNdcMapping);
				
			}
			drugRxCodeList = planDisplayUtil.prepareRxCodeList(planAvailabilityForDrugRequest.getDrugDTOList());
			responseObj = prepareDrugHiosMap(drugRxCodeList,planAvailabilityForDrugRequest.getPlanHiosList(), planAvailabilityForDrugRequest.getApplicableYear());
			long endTime = TimeShifterUtil.currentTimeMillis();
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO,"----preparedrughiosmap API END----"+(startTime-endTime),null,false);
			return responseObj;
		}
		return null;
	}
	
	private void updateDrugDtosWithRxCodes(List<DrugDTO> drugDTOList, Map<String, String> rxCodeNdcMapping) {
		for(DrugDTO drugDto : drugDTOList)
		{
			drugDto.setDrugRxCode(rxCodeNdcMapping.get(drugDto.getDrugNdc()));
			drugDto.setGenericRxCode(rxCodeNdcMapping.get(drugDto.getGenericNdc()));
		}		
	}

	private boolean drugDtosHasRxCodes(final List<DrugDTO> drugDTOList) {
		if(drugDTOList != null){
			for(DrugDTO drugDTO : drugDTOList){
				if(StringUtils.isEmpty(drugDTO.getDrugRxCode())){
					return false;
				}
			}
		}
		return true;
	}

	private Map<String, Future<PrescriptionData>> prepareDrugHiosMap(final List<String> drugRxCodeList, final List<String> hiosIds, int applicableYear){		
		Map<String, Future<PrescriptionData>> futurePlanAvailabilityListMap = new HashMap<String, Future<PrescriptionData>>();
		PrescriptionSearchByHIOSIdRequestDTO prescriptionSearchRequest = new PrescriptionSearchByHIOSIdRequestDTO();
		prescriptionSearchRequest.setApplicableYear(applicableYear);
		prescriptionSearchRequest.setHiosPlanIdList(hiosIds);
		prescriptionSearchRequest.setRxCodeList(drugRxCodeList);
		Future<PrescriptionData> futurePlanAvailabilityResponse = planDisplayAsyncUtil.callAPIAsyncPOST(prescriptionSearchRequest);
		futurePlanAvailabilityListMap.put("giPrescriptionData", futurePlanAvailabilityResponse);
		
		return futurePlanAvailabilityListMap;
	}
	
	public Map<String, List<DrugDTO>> prepareDrugListMap(final List<DrugDTO> drugDTOList, final List<String> hiosPlanIds, final Map<String, Future<PrescriptionData>> futurePlanAvailabilityListMap)
	{
		Map<String, List<DrugDTO>> planDrugListMap = new HashMap<String, List<DrugDTO>>();
		
		Future<PrescriptionData> prescriptionDataFutureObj = futurePlanAvailabilityListMap.get("giPrescriptionData");
		if(prescriptionDataFutureObj != null){
			try {
				PrescriptionData prescriptionData = prescriptionDataFutureObj.get();
	
				long startTime = TimeShifterUtil.currentTimeMillis();
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO,"---- inside prepareDrugListMap ---- ",null,false);
				List<PrescriptionDTO> prescriptionDTOList = prescriptionData.getGiPrescriptions();
				for(String hiosPlanId : hiosPlanIds)
				{
					List<DrugDTO> dtos = getRxCodesForHiosPlanId(prescriptionDTOList, hiosPlanId, drugDTOList);
					planDrugListMap.put(hiosPlanId, dtos);
				}
				long endTime = TimeShifterUtil.currentTimeMillis();
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO,"---- inside prepareDrugListMap ----"+(endTime-startTime),null,false);
			} catch (InterruptedException | ExecutionException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "Error while calling prepareDrugListMap", e, false);
			}
		}
		return planDrugListMap;
	}		

	private List<DrugDTO> getRxCodesForHiosPlanId(final List<PrescriptionDTO> prescriptionDTOList, final String hiosPlanId, final List<DrugDTO> drugDTOList) {
		DrugDTO drugResponseDTO;
		List<DrugDTO> planDrugList = new ArrayList<DrugDTO>();
		for(DrugDTO drugDTO : drugDTOList){
			drugResponseDTO = drugDTO.clone();
			drugResponseDTO.setIsDrugCovered("N");
			drugResponseDTO.setDrugFairPrice(drugDTO.getDrugFairPrice());
			Map<String, String> drugData = findPlanAvailabilityForDrug(prescriptionDTOList,drugDTO.getDrugRxCode(), hiosPlanId);
			if(drugData != null && StringUtils.isNotBlank(drugData.get("displayVal"))){
				drugResponseDTO.setIsDrugCovered("Y");
				drugResponseDTO.setDrugCost(drugData.get("displayVal"));
				if(drugData.get("authRequired") != null) {
					if("Y".equalsIgnoreCase(drugData.get("authRequired"))){
						drugResponseDTO.setAuthRequired("Yes");
					}else if("N".equalsIgnoreCase(drugData.get("authRequired"))){
						drugResponseDTO.setAuthRequired("No");
					}
				}
				if(drugData.get("stepTherapyRequired") != null) {
					if("Y".equalsIgnoreCase(drugData.get("stepTherapyRequired"))){
						drugResponseDTO.setStepTherapyRequired("Yes");
					}else if("N".equalsIgnoreCase(drugData.get("stepTherapyRequired"))){
						drugResponseDTO.setStepTherapyRequired("No");
					}
				}
			}
			
			if(StringUtils.isNotBlank(drugDTO.getGenericRxCode())){
				Map<String, String> genericDrugData = findPlanAvailabilityForDrug(prescriptionDTOList,drugDTO.getGenericRxCode(), hiosPlanId);
				drugResponseDTO.setIsGenericCovered("N");
				drugResponseDTO.setGenericFairPrice(drugDTO.getGenericFairPrice());
				if(genericDrugData != null && StringUtils.isNotBlank(genericDrugData.get("displayVal"))){
					drugResponseDTO.setIsGenericCovered("Y");
					drugResponseDTO.setGenericCost(genericDrugData.get("displayVal"));
					if(genericDrugData.get("authRequired") != null) {
						if("Y".equalsIgnoreCase(genericDrugData.get("authRequired"))){
							drugResponseDTO.setGenericAuthRequired("Yes");
						}else if("N".equalsIgnoreCase(genericDrugData.get("authRequired"))){
							drugResponseDTO.setGenericAuthRequired("No");
						}
					}
					if(genericDrugData.get("stepTherapyRequired") != null) {
						if("Y".equalsIgnoreCase(genericDrugData.get("stepTherapyRequired"))){
							drugResponseDTO.setGenericStepTherapyRequired("Yes");
						}else if("N".equalsIgnoreCase(genericDrugData.get("stepTherapyRequired"))){
							drugResponseDTO.setGenericStepTherapyRequired("No");
						}
					}
				}
			}
			planDrugList.add(drugResponseDTO);
		}
		
		return planDrugList;
	}
	
	private Map<String, String> findPlanAvailabilityForDrug(final List<PrescriptionDTO> prescriptionDTOList, final String rxCode, final String hiosPlanId) {
		if(prescriptionDTOList != null && StringUtils.isNotEmpty(hiosPlanId)){
			for(PrescriptionDTO prescriptionDto : prescriptionDTOList){
				if(prescriptionDto != null && rxCode.equalsIgnoreCase(prescriptionDto.getRxCode())  && prescriptionDto.getSupportedPlanList() != null){
					for(Map<String, String> supportPlan : prescriptionDto.getSupportedPlanList()){
						if(hiosPlanId.equals(supportPlan.get("hiosPlanId"))){
							if (StringUtils.isNotEmpty(supportPlan.get("displayVal"))) {
								return supportPlan;
							}
						}
					}
				}
			}
		}
		return null;
	}

	@Override
	public List<DrugDTO> getPlanDrugList(Map<String, List<DrugDTO>> planDrugListMap, String issuerPlanNumber) {
		List<DrugDTO> planDrugList = null;
		if (planDrugListMap != null){
			planDrugList = new ArrayList<DrugDTO>();
			if (planDrugListMap.get(issuerPlanNumber) != null && planDrugListMap.get(issuerPlanNumber).size() > 0 ){
				planDrugList = planDrugListMap.get(issuerPlanNumber);
			}
		}
		return planDrugList;
	}
}
