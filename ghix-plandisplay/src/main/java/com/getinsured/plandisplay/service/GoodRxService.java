package com.getinsured.plandisplay.service;

import com.getinsured.hix.dto.consumer.goodrx.GoodRxResponseDTO;

public interface GoodRxService {
	String API_KEY = "d6de803087";
	String API_SECRET = "pf8d4wma7SLmuPS31ZDsuw==";
	String FAIR_PRICE_SERVICE_ENDPOINT = "https://api.goodrx.com/fair-price?";
	String DRUG_SEARCH_SERVICE_ENDPOINT = "https://api.goodrx.com/drug-search?";
	
	GoodRxResponseDTO getFairPriceByNDC(String ndc);
	GoodRxResponseDTO getFairPrice(String args);
	GoodRxResponseDTO getDrugNames(String drugSynonym);

}