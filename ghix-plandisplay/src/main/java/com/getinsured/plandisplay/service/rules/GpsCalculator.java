/**
 * 
 */
package com.getinsured.plandisplay.service.rules;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.plandisplay.util.PlanDisplayUtil;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;

/**
 * @author karnam_s
 *
 */
public class GpsCalculator {
	private static final Logger LOGGER = LoggerFactory.getLogger(GpsCalculator.class);
	private int oopCostWeightFactor = 40;
	private int featPlanImp = 20;
	private int ehb = 40;
	private Double noPerfectPlanValue = 98.0;
	
	public Map<Integer, Double> calculateGpsScore(Map<Integer, Double> planOopCost, Map<Integer, Integer> planBenefitScore) {
		
		Map<Integer, Double> inversePlanOopCost = getPlanOopCostsInverseScore(planOopCost);
		Map<Integer, Double> planOopScaledRatio =  getPlanOopScaledRatio(inversePlanOopCost);
		Map<Integer, Double> planOopWeightedScoreMap = getPlanOopWeightedScore(planOopScaledRatio, oopCostWeightFactor);
		
		Map<Integer, Double>benefitScaledRatio = getBenefitScaledRatio(planBenefitScore);
		Map<Integer,Double>benefitsWeightedScoreMap = getBenefitsWeightedScore(benefitScaledRatio, featPlanImp);
		
		Map<Integer,Double> unadjustedInterimScoreMap = getUnadjustedInterimScore(benefitsWeightedScoreMap,planOopWeightedScoreMap);
		
		Map<Integer,Double> finalGpsScoreMap = setNoPerfectPlanValues(unadjustedInterimScoreMap, noPerfectPlanValue);
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----FInal GPS output---->"+finalGpsScoreMap, null, true);
		return finalGpsScoreMap;
	}
	
	public Map<Integer,Double> getPlanOopCostsInverseScore(Map<Integer, Double> planOopCost) {
		Iterator planOopEntries = planOopCost.entrySet().iterator();
		Map<Integer, Double> inversePlanOopCost = new HashMap<Integer, Double>();
		while (planOopEntries.hasNext()) {
			Entry thisEntry = (Entry) planOopEntries.next();
			Integer planId = (Integer)thisEntry.getKey();
			Double planOop = (Double)thisEntry.getValue();
			Double inversePlanOop = (planOop<1)?0.0:1/planOop*1000;
			inversePlanOopCost.put(planId, inversePlanOop);
		}
		printMap("inversePlanOopCost::",inversePlanOopCost);
		return inversePlanOopCost;
	}
	
	public Map<Integer, Double> getPlanOopScaledRatio(Map<Integer, Double> inversePlanOopCost) {
		
		Double inverseMaxOop = (inversePlanOopCost==null || inversePlanOopCost.isEmpty())? new Double(1) : Collections.max(inversePlanOopCost.values());
		Map<Integer, Double> planOopScaledRatio = new HashMap<Integer, Double>();
	 if(inversePlanOopCost != null){
		Iterator inversePlanOopEntries = inversePlanOopCost.entrySet().iterator();
		while (inversePlanOopEntries.hasNext()) {
			Entry thisEntry = (Entry) inversePlanOopEntries.next();
			Integer planId = (Integer)thisEntry.getKey();
			Double inversePlanOop = (Double)thisEntry.getValue();
			Double planOopWeightedRatioVal = inversePlanOop/inverseMaxOop;
			planOopScaledRatio.put(planId, planOopWeightedRatioVal);
		}
	 }	
		printMap("planOopScaledRatio::",planOopScaledRatio);
		return planOopScaledRatio;
	}
	
	public Map<Integer, Double> getPlanOopWeightedScore(Map<Integer, Double> planOopScaledRatio, int oopCostWeightFactor) {
		Iterator planOopScaledRatioEntries = planOopScaledRatio.entrySet().iterator();
		Map<Integer, Double> planOopWeightedScoreMap = new HashMap<Integer, Double>(); 
		while (planOopScaledRatioEntries.hasNext()) {
			Entry thisEntry = (Entry) planOopScaledRatioEntries.next();
			Integer planId = (Integer)thisEntry.getKey();
			Double planOopScaledRatioVal = (Double)thisEntry.getValue();
			Double adjustedVal = oopCostWeightFactor * planOopScaledRatioVal;
			planOopWeightedScoreMap.put(planId, adjustedVal);
		}
		printMap("planOopWeightedScoreMap::",planOopWeightedScoreMap);
		return planOopWeightedScoreMap;
	}
	
	// benefit related calculations
	public Map<Integer,Double> getBenefitScaledRatio(Map<Integer,Integer> planBenefitScoreMap) {
		Integer maxBenefitScore  = (planBenefitScoreMap==null || planBenefitScoreMap.isEmpty()) ? 1 : Collections.max(planBenefitScoreMap.values());
		Map<Integer,Double> benefitScaledRatioMap = new HashMap<Integer,Double>();
	 if(planBenefitScoreMap != null){
		Iterator planBenefitScoreEntries = planBenefitScoreMap.entrySet().iterator();
		while (planBenefitScoreEntries.hasNext()) {
			Entry thisEntry = (Entry) planBenefitScoreEntries.next();
			Integer planId = (Integer)thisEntry.getKey();
			Integer planBenefitScore = (Integer)thisEntry.getValue();
			Double benefitScaledRatioEntry = (maxBenefitScore==0)?0.0:(double)planBenefitScore/(double)maxBenefitScore;
			benefitScaledRatioMap.put(planId, benefitScaledRatioEntry);
		}
	  }	
		printMap("benefitScaledRatioMap::",benefitScaledRatioMap);
		return benefitScaledRatioMap;
	}
	
	public Map<Integer,Double> getBenefitsWeightedScore(Map<Integer,Double>benefitScaledRatioMap, int featPlanImp) {
		Map<Integer,Double> benefitsWeightedScoreMap = new HashMap<Integer, Double>();
		Iterator  benefitScaledRatioEntries = benefitScaledRatioMap.entrySet().iterator();
		while (benefitScaledRatioEntries.hasNext()) {
			Entry thisEntry = (Entry) benefitScaledRatioEntries.next();
			Integer planId = (Integer)thisEntry.getKey();
			Double benefitScaledRatioEntry = (Double)thisEntry.getValue();
			Double benefitWeightedScore =  benefitScaledRatioEntry * featPlanImp;
			benefitsWeightedScoreMap.put(planId, benefitWeightedScore);
		}
		return benefitsWeightedScoreMap;
	}
	
	public Map<Integer,Double> getUnadjustedInterimScore(Map<Integer,Double> benefitsWeightedScoreMap, Map<Integer,Double> planOopWeightedScoreMap) {
		Map<Integer,Double> unadjustedInterimScoreMap = new HashMap<Integer, Double>();
		Iterator  benefitsWeightedScoreEntries = benefitsWeightedScoreMap.entrySet().iterator();
		while (benefitsWeightedScoreEntries.hasNext()) {
			Entry thisEntry = (Entry) benefitsWeightedScoreEntries.next();
			Integer planId = (Integer)thisEntry.getKey();
			Double benefitsWeightedScore = (Double)thisEntry.getValue();
			Double planOopWeightedScoreEntry = (planOopWeightedScoreMap.get(planId)==null)?0.0:planOopWeightedScoreMap.get(planId);
			Double combinedAdjustedInterimScore = benefitsWeightedScore + planOopWeightedScoreEntry + ehb;
			unadjustedInterimScoreMap.put(planId, combinedAdjustedInterimScore);
		}
		return unadjustedInterimScoreMap;
	}
	
	public Map<Integer,Double> setNoPerfectPlanValues(Map<Integer,Double> unadjustedInterimScoreMap, Double noPerfectPlanValue) {
		Map<Integer,Double> finalGpsScoreMap = new HashMap<Integer, Double>();
		Iterator  unadjustedInterimScoreEntries = unadjustedInterimScoreMap.entrySet().iterator();
		while (unadjustedInterimScoreEntries.hasNext()) {
			Entry thisEntry = (Entry) unadjustedInterimScoreEntries.next();
			Integer planId = (Integer)thisEntry.getKey();
			Double unadjustedInterimEntryScore = (Double)thisEntry.getValue();
			Double finalScoreEntry = (unadjustedInterimEntryScore > noPerfectPlanValue)?noPerfectPlanValue:unadjustedInterimEntryScore;
			DecimalFormat twoDForm = new DecimalFormat("#.#");
			
			finalGpsScoreMap.put(planId, Double.valueOf(twoDForm.format(finalScoreEntry)));
		}
		printMap("finalGpsScoreMap::", finalGpsScoreMap);
		return finalGpsScoreMap;
	}
	public void printMap(String message, Map m) {
		for (int i=1; i<=10; i++) {
			//System.out.println(message + "::" + "plan _" + i + "::" + m.get(i));
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG,message + "::" + "plan _" + i + "::" + m.get(i),null, true);
		}	
	}
}
