package com.getinsured.plandisplay.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.Future;

import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.SolrPingResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.EnrollmentStatus;
import com.getinsured.hix.dto.plandisplay.DrugDTO;
import com.getinsured.hix.dto.plandisplay.PdHouseholdDTO;
import com.getinsured.hix.dto.plandisplay.PdPersonDTO;
import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;
import com.getinsured.hix.dto.plandisplay.PlanDentalBenefitDTO;
import com.getinsured.hix.dto.plandisplay.PlanDentalCostDTO;
import com.getinsured.hix.dto.plandisplay.PlanHealthBenefitDTO;
import com.getinsured.hix.dto.plandisplay.PlanHealthCostDTO;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.dto.prescription.PrescriptionSearchRequest;
import com.getinsured.hix.model.IndividualPlan;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Plan.PlanInsuranceType;
import com.getinsured.hix.model.PlanRateBenefit;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.plandisplay.PdQuoteRequest;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentFlowType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ExchangeType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.GPSVersion;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.PreferencesLevel;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.Relationship;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ShoppingType;
import com.getinsured.hix.model.plandisplay.ProviderBean;
import com.getinsured.hix.planmgmt.controller.PlanRestController;
import com.getinsured.hix.planmgmt.service.PlanRateBenefitServiceImpl.CostNames;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.config.PlanDisplayConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.getinsured.plandisplay.service.prescriptionSearch.PrescriptionData;
import com.getinsured.plandisplay.service.prescriptionSearch.PrescriptionFactory;
import com.getinsured.plandisplay.service.prescriptionSearch.PrescriptionRequest;
import com.getinsured.plandisplay.service.prescriptionSearch.PrescriptionSearch;
import com.getinsured.plandisplay.service.providerSearch.ProviderFactory;
import com.getinsured.plandisplay.service.providerSearch.ProviderSearch;
import com.getinsured.plandisplay.service.rules.BenefitCoverageCalculator;
import com.getinsured.plandisplay.util.PlanDisplayConstants;
import com.getinsured.plandisplay.util.PlanDisplayUtil;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TimeShifterUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Service("IndividualPlanService")
public class IndividualPlanServiceImpl implements IndividualPlanService {
	private static final Logger LOGGER = LoggerFactory.getLogger(IndividualPlanServiceImpl.class);

	@Autowired
	private PdHouseholdService pdHouseholdService;
	@Autowired
	private PdOrderItemService pdOrderItemService;
	@Autowired
	private PlanDisplayUtil planDisplayUtil;
	@Autowired
	private PlanSolrUpdateService planSolrUpdateService;
	@Autowired
	private GIExceptionHandler giExceptionHandler;
	@Autowired private ZipCodeService zipCodeService;
	
	@Value("#{configProp}")
	private Properties properties;

	@Autowired
	@Qualifier("solrPlanBenefitServer")
	private SolrServer solrPlanBenefitServer;
	
	@Autowired
	@Qualifier("solrHealthCostServer")
	private SolrServer solrHealthCostServer;

	@Autowired
	@Qualifier("solrDentalBenefitServer")
	private SolrServer solrDentalBenefitServer;
	
	@Autowired
	@Qualifier("solrDentalCostServer")
	private SolrServer solrDentalCostServer;

	private QueryResponse response = null;
	

	@Autowired private Gson platformGson;

	@Autowired private ProviderFactory providerFactory;
	@Autowired private PrescriptionFactory prescriptionFactory;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private PlanRestController planRestController;
	
	private static final String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
    private static final String exchangeTypeConfig = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE);
	

	private void setWorksiteZipAsZip(List<Map<String, String>> censusList, String employerWorksiteZip,
			String employerWorksiteCounty) {
		for (Map<String, String> member : censusList) {
			member.put("zip", employerWorksiteZip);
			member.put("countycode", employerWorksiteCounty);
		}
	}

	@Override
	public Map<String, String> getSubscriberData(List<PdPersonDTO> seekingCovgPersonList, PdHouseholdDTO pdHouseholdDTO,
			String initialSubscriberId, EnrollmentFlowType specialEnrollmentFlowType, InsuranceType insuranceType) {
		EnrollmentType enrollmentType = pdHouseholdDTO.getEnrollmentType();
		Date householdCoverageStartDate = pdHouseholdDTO.getCoverageStartDate();
		
		Map<String, String> subscriberData = new HashMap<String, String>();

		if (EnrollmentType.S.equals(enrollmentType) && initialSubscriberId != null) {
			for (PdPersonDTO pdPersonDTO : seekingCovgPersonList) {
				if (pdPersonDTO.getExternalId().equalsIgnoreCase(initialSubscriberId)) {
					subscriberData = formSubscriber(pdPersonDTO);
					break;
				}
			}
		}
		if (subscriberData.isEmpty()) {
			subscriberData = getSubscriberforSelf(seekingCovgPersonList);

			// If Child only shopping
			if (subscriberData == null) {
				subscriberData = getSubscriberforYoungestMember(seekingCovgPersonList, enrollmentType,
						householdCoverageStartDate, specialEnrollmentFlowType, insuranceType);
			}

			// if self is not present/below 18 -> get oldest member above 18
			if (subscriberData == null) {
				subscriberData = getSubscriberforOldestMember(seekingCovgPersonList, enrollmentType,
						householdCoverageStartDate, specialEnrollmentFlowType, insuranceType);
			}
		}
		return subscriberData;
	}
	
	private Map<String, String> getSubscriberforSelf(List<PdPersonDTO> pdPersonDTOList) {
		Map<String, String> subscriberData = null;
		for (PdPersonDTO pdPersonDTO : pdPersonDTOList) {
			if (Relationship.SELF.equals(pdPersonDTO.getRelationship())) {
				subscriberData = formSubscriber(pdPersonDTO);
				break;
			}
		}
		return subscriberData;
	}

	private Map<String, String> getSubscriberforOldestMember(List<PdPersonDTO> pdPersonDTOList,
			EnrollmentType enrollmentType, Date householdCoverageStartDate,
			EnrollmentFlowType specialEnrollmentFlowType, InsuranceType insuranceType) {
		Map<String, String> subscriberData = null;
		Integer ageInDays = null;
		PdPersonDTO oldestMemberDTO = null;
		for (PdPersonDTO pdPersonDTO : pdPersonDTOList) {
			Integer currAge = PlanDisplayUtil.getPersonAge(enrollmentType, specialEnrollmentFlowType, insuranceType, pdPersonDTO, householdCoverageStartDate);
			if (ageInDays == null || ageInDays <= currAge) {
				ageInDays = currAge;
				oldestMemberDTO = pdPersonDTO;
			}
		}
		if (oldestMemberDTO != null) {
			subscriberData = formSubscriber(oldestMemberDTO);
		}
		return subscriberData;
	}
	
	private Map<String, String> getSubscriberforYoungestMember(List<PdPersonDTO> pdPersonDTOList,
			EnrollmentType enrollmentType, Date householdCoverageStartDate,
			EnrollmentFlowType specialEnrollmentFlowType, InsuranceType insuranceType) {
		Map<String, String> subscriberData = null;
		boolean childOnly =  true;			
		for (PdPersonDTO pdPersonDTO : pdPersonDTOList) {
			if (!Relationship.CHILD.equals(pdPersonDTO.getRelationship())) {
				childOnly = false;
			}
		}
		
		if (childOnly) {
			Integer ageInDays = null;
			PdPersonDTO yongestMemberDTO = null;
			for (PdPersonDTO pdPersonDTO : pdPersonDTOList) {
				Integer currAge = PlanDisplayUtil.getPersonAge(enrollmentType, specialEnrollmentFlowType, insuranceType, pdPersonDTO, householdCoverageStartDate);
				if (ageInDays == null || ageInDays > currAge) {
					ageInDays = currAge;
					yongestMemberDTO = pdPersonDTO;
				}
			}
			if (yongestMemberDTO != null) {
				subscriberData = formSubscriber(yongestMemberDTO);
			}
		}

		return subscriberData;
	}
	
	private int getAgeInDays(Date dob, Date coverageStartDate) {
		int ageInDays = 0;
		DateTime dobJoda = new DateTime(dob);
		DateTime coverageStart = new DateTime(coverageStartDate);
		Days days = Days.daysBetween(dobJoda, coverageStart);
		ageInDays = days.getDays();
		return ageInDays;
	}

	private Map<String, String> formSubscriber(PdPersonDTO pdPersonDTO) {
		Map<String, String> subscriberData = new HashMap<String, String>();

		subscriberData.put(GhixConstants.SUBSCRIBER_MEMBER_COUNTY, pdPersonDTO.getCountyCode());
		subscriberData.put(GhixConstants.SUBSCRIBER_MEMBER_ID, pdPersonDTO.getExternalId() + "");
		subscriberData.put(GhixConstants.SUBSCRIBER_MEMBER_ZIP, pdPersonDTO.getZipCode());

		return subscriberData;
	}

	@Override
	public List<IndividualPlan> getPlans(PdQuoteRequest pdQuoteRequest) {	
		BigDecimal aptc = BigDecimal.ZERO;
		BigDecimal stateSubsidy = null;
		Map<String,String> subscriberData = new HashMap<String,String>();
		List<IndividualPlan> individualPlans = new ArrayList<IndividualPlan>();

		try{
			PdHouseholdDTO pdHouseholdDTO = pdQuoteRequest.getPdHouseholdDTO();
			boolean minimizePlanData = pdQuoteRequest.getMinimizePlanData();				

			List<PdPersonDTO> seekingCovgPersonList = pdQuoteRequest.getPdPersonDTOList();
			if(InsuranceType.DENTAL.equals(pdQuoteRequest.getInsuranceType())){
				seekingCovgPersonList = planDisplayUtil.getSeekingCovgPersonList(pdQuoteRequest.getPdPersonDTOList());
			}
			
			try{
				if(pdHouseholdDTO.getId() != null || pdQuoteRequest.getInitialSubscriberId() != null) {
					subscriberData = getSubscriberData(seekingCovgPersonList, pdHouseholdDTO, pdQuoteRequest.getInitialSubscriberId(), pdQuoteRequest.getSpecialEnrollmentFlowType(), pdQuoteRequest.getInsuranceType());
					String newuSubscriberId = subscriberData.get(GhixConstants.SUBSCRIBER_MEMBER_ID);
					if(pdHouseholdDTO.getId() != null){
						pdHouseholdService.setSubscriber(pdHouseholdDTO.getId(), newuSubscriberId, pdQuoteRequest.getInsuranceType());
					}
				}else { // For ANONYMOUS EMPLOYER and ISSUER VERIFICATION flow			
					subscriberData.put(GhixConstants.SUBSCRIBER_MEMBER_ZIP, pdQuoteRequest.getPdPersonDTOList().get(0).getZipCode());
					subscriberData.put(GhixConstants.SUBSCRIBER_MEMBER_COUNTY, pdQuoteRequest.getPdPersonDTOList().get(0).getCountyCode());
				}
			} catch (Exception ex) {
				int seekingCoveragePersonCount = seekingCovgPersonList != null ? seekingCovgPersonList.size() : 0;
				if(pdHouseholdDTO != null){
					Long householdId = pdHouseholdDTO.getId() != null ? pdHouseholdDTO.getId() : 0;
					String enrollmentTypeStr = pdHouseholdDTO.getEnrollmentType() != null ? pdHouseholdDTO.getEnrollmentType().toString() : "NULL";
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----No subScriber found for household----"+householdId+"--with person count--"+pdQuoteRequest.getPdPersonDTOList()+"--and seeking coverage person count---"+seekingCoveragePersonCount+"--- InsuranceType---"+pdQuoteRequest.getInsuranceType()+"-- Enrollment Type --"+enrollmentTypeStr, ex, true);
				}else{
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----No subScriber found for NULL household with person count--"+pdQuoteRequest.getPdPersonDTOList()+" with seeking coverage person count---"+seekingCoveragePersonCount+"--- InsuranceType---"+pdQuoteRequest.getInsuranceType(), ex, true);
				}
				throw ex;
			}

			Date coverageStartDateForAge = pdHouseholdDTO.getCoverageStartDate();
			List<Map<String,String>> censusList = PlanDisplayUtil.getMemberListForQuoting(seekingCovgPersonList, subscriberData, coverageStartDateForAge, pdHouseholdDTO.getEnrollmentType(), pdQuoteRequest.getSpecialEnrollmentFlowType(), pdQuoteRequest.getInsuranceType());

			if(InsuranceType.AME.equals(pdQuoteRequest.getInsuranceType())){
				individualPlans = planDisplayUtil.getAMEPlans(censusList, pdHouseholdDTO.getCoverageStartDate());
			}else if(InsuranceType.VISION.equals(pdQuoteRequest.getInsuranceType())){
				individualPlans = planDisplayUtil.getVisionPlans(censusList, pdHouseholdDTO.getCoverageStartDate());
			}else if(InsuranceType.LIFE.equals(pdQuoteRequest.getInsuranceType())){
				individualPlans = planDisplayUtil.getLifePlans(censusList, pdHouseholdDTO.getCoverageStartDate());
			}else{
				String readPlansFromSolrConf = properties.getProperty("readPlansFromSolr") != null ? properties.getProperty("readPlansFromSolr"):"NO";
				//Check availability of SOLR.
				boolean readDataFromSolr = "YES".equalsIgnoreCase(readPlansFromSolrConf) && (InsuranceType.HEALTH.equals(pdQuoteRequest.getInsuranceType())||InsuranceType.DENTAL.equals(pdQuoteRequest.getInsuranceType())) && checkSolrServerAvailability(solrPlanBenefitServer);
				boolean minimizePlanDataForQuoting = true;
				if(!minimizePlanData){
					minimizePlanDataForQuoting = readDataFromSolr ?  true : false;
				}

				Date quotingCoverageDate = pdHouseholdDTO.getCoverageStartDate();
				if(ShoppingType.EMPLOYEE.equals(pdHouseholdDTO.getShoppingType())){
					quotingCoverageDate = pdQuoteRequest.getEmployerCoverageStartDate();
				}

				
				PdPreferencesDTO pdPreferencesDTO = pdQuoteRequest.getPdPreferencesDTO();
				List<ProviderBean> providers = new ArrayList<ProviderBean>();
				//List<PrescriptionSearchRequest> prescriptionRequestList = new ArrayList<PrescriptionSearchRequest>();
				if(pdPreferencesDTO != null){
					String providerJson = pdPreferencesDTO.getProviders();			
					if(StringUtils.isNotBlank(providerJson)){
						providers = platformGson.fromJson(providerJson,new TypeToken<List<ProviderBean>>() {}.getType());
					}
				}
				ProviderSearch providerSearch = providerFactory.getObject();
				Map<String, Future<Map<String, Object>>> futureProviderResponseListMap = providerSearch.prepareProviderHiosMap(providers);
				
				List<PrescriptionSearchRequest> prescriptionRequests = null;
				
				//Call Quoting API to get Plans
				List<PlanRateBenefit> plans = planDisplayUtil.getHouseholdPlanRateBenefits(censusList, seekingCovgPersonList, quotingCoverageDate, pdQuoteRequest, pdHouseholdDTO, minimizePlanDataForQuoting, prescriptionRequests);

				//SHOP - if plans are not available for employee quoting zip then look for plans based on employer work site zipcode
				if(ShoppingType.EMPLOYEE.equals(pdHouseholdDTO.getShoppingType()) && plans.isEmpty() && pdQuoteRequest.getEmployerWorksiteZip() != null && pdQuoteRequest.getEmployerWorksiteCounty() != null) {
					setWorksiteZipAsZip(censusList, pdQuoteRequest.getEmployerWorksiteZip(), pdQuoteRequest.getEmployerWorksiteCounty());
					plans = planDisplayUtil.getHouseholdPlanRateBenefits(censusList, seekingCovgPersonList, quotingCoverageDate, pdQuoteRequest, pdHouseholdDTO, minimizePlanDataForQuoting, prescriptionRequests);
				}

				if(plans.isEmpty()){
					return individualPlans;
				}
				
				
				String strCoverageStartDate = DateUtil.dateToString(quotingCoverageDate, "yyyy-MM-dd");
				if(readDataFromSolr && !minimizePlanData) {
					getBenefitCostFromSolr(plans, pdQuoteRequest.getInsuranceType().toString(), strCoverageStartDate);
				}
				if (!minimizePlanData && (InsuranceType.HEALTH.equals(pdQuoteRequest.getInsuranceType()) || InsuranceType.DENTAL.equals(pdQuoteRequest.getInsuranceType()))) {
					List<PlanRateBenefit> plansWithNoBenefitsAndCost = new ArrayList<PlanRateBenefit>();
					for (PlanRateBenefit plan : plans) {
						if (!"Y".equalsIgnoreCase(plan.getIsPuf()) && plan.getPlanBenefits() == null || plan.getPlanCosts() == null) {
							plansWithNoBenefitsAndCost.add(plan);
							PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR,"<----Plan Benefit/Cost data missing for Plan id---->" + plan.getId(), null, true);
						}
					}
					plans.removeAll(plansWithNoBenefitsAndCost);
				}
				
				Map<String,Integer> personalUsageForMedical = formMedicalUsageMap(pdPreferencesDTO, censusList.size());
				Map<String,Integer> personalUsageForDrugs = formDrugsUsageMap(pdPreferencesDTO, censusList.size());
				String minPremiumPerMember = DynamicPropertiesUtil.getPropertyValue(PlanDisplayConfiguration.PlanDisplayConfigurationEnum.MIN_PREMIUM_PER_MEMBER);
				
				Map<String, Future<PrescriptionData>> futurePlanAvailabilityListMap = null;
				PrescriptionSearch availabilityObject = prescriptionFactory.getObject();
				Map<String, List<DrugDTO>> planDrugListMap = null;
				if(InsuranceType.HEALTH.equals(pdQuoteRequest.getInsuranceType()) && StringUtils.isNotBlank(pdPreferencesDTO.getPrescriptions())){
					List<String> hiosList = preparePlanHiosList(plans);
					List<DrugDTO> drugList = planDisplayUtil.parsePrescriptionJson(pdPreferencesDTO.getPrescriptions());
					if(censusList != null && censusList.size() > 0 && censusList.get(0).get("zip") != null && censusList.get(0).get("countycode") != null)
					{
						String zip =censusList.get(0).get("zip");
						String countyCode =censusList.get(0).get("countycode");
						if(zip != null && countyCode != null){
							ZipCode zipCode =  zipCodeService.getZipcodeByZipAndCountyCode(zip, countyCode);
							PrescriptionRequest planAvailabilityForDrugRequest = new PrescriptionRequest();
							Calendar cal = TSCalendar.getInstance();
						    cal.setTime(pdHouseholdDTO.getCoverageStartDate());
						    int applicableYear = cal.get(Calendar.YEAR);
							planAvailabilityForDrugRequest.setApplicableYear(applicableYear);
							planAvailabilityForDrugRequest.setState(zipCode.getState());
							planAvailabilityForDrugRequest.setDrugDTOList(drugList);
							planAvailabilityForDrugRequest.setPlanHiosList(hiosList);
							futurePlanAvailabilityListMap = availabilityObject.prepareFuturePlanAvailabilityMap(planAvailabilityForDrugRequest);
							planDrugListMap = availabilityObject.prepareDrugListMap(drugList, hiosList, futurePlanAvailabilityListMap);
						}
					}
				}
				
				Map<Long, Double> costCompareData = new HashMap<Long, Double>();
				if(InsuranceType.HEALTH.equals(pdQuoteRequest.getInsuranceType())){
					costCompareData = pdOrderItemService.getComparisonCosts(plans, personalUsageForMedical, personalUsageForDrugs, pdPreferencesDTO, DateUtil.dateToString(pdHouseholdDTO.getCoverageStartDate(), "yyyy-MM-dd"), null);
				}

				List<PdPersonDTO> memberList = null;
				if(ShoppingType.EMPLOYEE.equals(pdHouseholdDTO.getShoppingType())) {
					memberList = seekingCovgPersonList;
				}
				else {
					Float subsidyForCalculations = pdHouseholdDTO.getMaxSubsidy();
					if(EnrollmentType.S.equals(pdQuoteRequest.getPdHouseholdDTO().getEnrollmentType()) && pdQuoteRequest.getInitialCmsPlanId() != null && pdQuoteRequest.getPdHouseholdDTO().getAptcForKeep() != null)
					{
						subsidyForCalculations = pdHouseholdDTO.getAptcForKeep();
					}

					boolean isCAProfile = stateCode != null ? stateCode.equalsIgnoreCase("CA") : false;
					if(InsuranceType.HEALTH.equals(pdQuoteRequest.getInsuranceType()) && subsidyForCalculations != null){
						aptc = new BigDecimal(Float.toString(subsidyForCalculations));
					}
					if(InsuranceType.DENTAL.equals(pdQuoteRequest.getInsuranceType()) && pdHouseholdDTO.getDentalSubsidy() != null && subsidyForCalculations != null && !Float.valueOf(0).equals(subsidyForCalculations) && !isCAProfile){
						if(("MN".equalsIgnoreCase(stateCode)) || hasUnderAgeChildEnrolled(censusList))
						{
							Float healthSubsidy = pdHouseholdDTO.getHealthSubsidy() != null ? pdHouseholdDTO.getHealthSubsidy() : 0;
							aptc = new BigDecimal(Float.toString(subsidyForCalculations)).subtract(new BigDecimal(Float.toString(healthSubsidy))).setScale(2,BigDecimal.ROUND_HALF_UP);
						}
					}
				}

				if(pdHouseholdDTO.getMaxStateSubsidy() != null && InsuranceType.HEALTH.equals(pdQuoteRequest.getInsuranceType())){
					stateSubsidy = pdHouseholdDTO.getMaxStateSubsidy();
				}

				
				ExchangeType exchangeType = pdQuoteRequest.getExchangeType();
				String[] props = GhixConstants.helathBenefitList;
				if(PlanDisplayEnum.InsuranceType.DENTAL.equals(pdQuoteRequest.getInsuranceType())){
					props = GhixConstants.dentalBenefitList;
				} else if (PlanDisplayEnum.InsuranceType.STM.equals(pdQuoteRequest.getInsuranceType())) {
					props = GhixConstants.stmBenefitList;
				} else if (PlanDisplayEnum.InsuranceType.AME.equals(pdQuoteRequest.getInsuranceType())) {
					props = GhixConstants.ameBenefitList;
				}
				individualPlans = planDisplayUtil.buildPlan(plans, censusList, pdQuoteRequest.getInsuranceType(), aptc, stateSubsidy, costCompareData, minPremiumPerMember, minimizePlanData, providers, memberList, quotingCoverageDate, props, exchangeType, pdPreferencesDTO, futureProviderResponseListMap, planDrugListMap);
				if(!minimizePlanData) {
					this.expandPlanData(individualPlans, pdHouseholdDTO, pdPreferencesDTO, providers,censusList.size(), plans);
				}
			}
		}catch(Exception e){
			giExceptionHandler.recordFatalException(Component.PLANDISPLAY,"Failed to get plans ", e);
			throw e;
		}
		return individualPlans;

	}

	private List<String> preparePlanHiosList(List<PlanRateBenefit> plans) {
		List<String> hiosList = new ArrayList<String>(); 
		for(PlanRateBenefit plan : plans)
		{
			hiosList.add(plan.getHiosPlanNumber());
		}
		return hiosList;
	}

	private boolean hasUnderAgeChildEnrolled(List<Map<String, String>> censusList) {
		for (Map<String, String> member : censusList) {
			Integer age = Integer.parseInt(member.get(PlanDisplayUtil.AGE));
			String relation = member.get("relation");
			if(age < 19 && Relationship.CHILD.toString().equals(relation))
			{
				return true;
			}
		}
		return false;
	}

	private void expandPlanData(List<IndividualPlan> individualPlans, PdHouseholdDTO pdHouseholdDTO, PdPreferencesDTO pdPreferencesDTO, List<ProviderBean> providers, int censusListSize, List<PlanRateBenefit> plans) {
		int lowPlanCount = 0;
		int averagePlanCount = 0;
		int priceyPlanCount = 0;
		float minimumEstimatedTotalCost = 0;
		float estimateCostRange = 0;
		
		if("CA".equalsIgnoreCase(stateCode)){
			int planCount = individualPlans.size();
			if(planCount > 0){
				lowPlanCount = Math.round(planCount/3f);
				priceyPlanCount = Math.round(planCount/4f);
				averagePlanCount = planCount - lowPlanCount - priceyPlanCount;
			}
			Collections.sort(individualPlans, new Comparator<IndividualPlan>() {
		        public int compare(IndividualPlan individualPlan1, IndividualPlan individualPlan2) {
		            return individualPlan2.getEstimatedTotalHealthCareCost() < individualPlan1.getEstimatedTotalHealthCareCost() ? 1 : -1;
		        }
		    });
		}else{
			HashMap<String,Float> minAndMaxEstimatedCost = planDisplayUtil.getMinAndMaxEstimatedCost(individualPlans);
			minimumEstimatedTotalCost = minAndMaxEstimatedCost.get("MIN_COST".intern());
			float maximumEstimatedTotalCost = minAndMaxEstimatedCost.get("MAX_COST".intern());
			estimateCostRange = new BigDecimal(Float.toString(maximumEstimatedTotalCost)).subtract(new BigDecimal(Float.toString(minimumEstimatedTotalCost))).divide(BigDecimal.valueOf(3.0), 2, BigDecimal.ROUND_HALF_UP).floatValue();
		}
		
		String gpsVersion = properties.getProperty("GPSVersion.individual") != null ? properties.getProperty("GPSVersion.individual").trim() : null;
		if (ShoppingType.EMPLOYEE.equals(pdHouseholdDTO.getShoppingType())) { 
			gpsVersion = properties.getProperty("GPSVersion.employee").trim();
		}

		boolean isPHIXProfile = "PHIX".equalsIgnoreCase(exchangeTypeConfig);
		if (isPHIXProfile) {
			gpsVersion = GPSVersion.V1.toString();
		}

		if (gpsVersion == null || (!"Baseline".equals(gpsVersion) && !"V1".equals(gpsVersion))) {
			gpsVersion = "Baseline";
		}				

		if ("V1".equals(gpsVersion)) {
			Map<String, Integer> personalUsageForMedical = formMedicalUsageMap(pdPreferencesDTO, censusListSize);
			Map<String, Integer> personalUsageForDrugs = formDrugsUsageMap(pdPreferencesDTO, censusListSize);
			Integer doctorsSelected = providers.size();
			Integer drugsSelected = 0;
			planDisplayUtil.calculateSmartScore(pdPreferencesDTO.getBenefits(), individualPlans, personalUsageForMedical, personalUsageForDrugs, doctorsSelected, drugsSelected);
		}

		Map<String, Map<Integer, Double>> phixPlanBenefits = planDisplayUtil.getBenefitCoverageData(plans);
		BenefitCoverageCalculator benefitCoverageCalculator = new BenefitCoverageCalculator(PlanDisplayConstants.benefitsMapKey);
		Map<Integer, Map<String, String>> planBenefitsCoverageOutput = benefitCoverageCalculator.calculateBenefitCoverage(phixPlanBenefits);
		int i = 0;
		for (IndividualPlan individualPlan : individualPlans) {
			Map<String, String> benefitsCoverageOutput = planBenefitsCoverageOutput.get(individualPlan.getPlanId());
			Map<String, String> benefitsCoverage = new HashMap<String, String>();
			if (benefitsCoverageOutput != null && !benefitsCoverageOutput.isEmpty()) {
				for (Map.Entry<String, String> entry : benefitsCoverageOutput.entrySet()) {
					benefitsCoverage.put(PlanDisplayConstants.benefitsKeyReveseMap.get(entry.getKey()), entry.getValue());
				}
			}

			if("Yes".equalsIgnoreCase(individualPlan.getHsa()))
			{
				benefitsCoverage.put("HSA Eligible", "GOOD");
			}
			individualPlan.setBenefitsCoverage(benefitsCoverage);
			
			if ("N".equalsIgnoreCase(individualPlan.getIsPuf())) {
				if("CA".equalsIgnoreCase(stateCode)){
					if(i<lowPlanCount){
						individualPlan.setExpenseEstimate(planDisplayUtil.fromInteger(1));
					}else if(i < (lowPlanCount+averagePlanCount)){
						individualPlan.setExpenseEstimate(planDisplayUtil.fromInteger(2));
					}else{
						individualPlan.setExpenseEstimate(planDisplayUtil.fromInteger(3));
					}
					i++;
				}else{
					if (estimateCostRange > 0) {
						individualPlan.setExpenseEstimate(planDisplayUtil.fromInteger((int) Math.ceil(new BigDecimal(
								Float.toString(individualPlan.getEstimatedTotalHealthCareCost()))
								.subtract(new BigDecimal(Float.toString(minimumEstimatedTotalCost)))
								.divide(BigDecimal.valueOf(estimateCostRange), 2, BigDecimal.ROUND_HALF_UP).floatValue())));
					} else {
						individualPlan.setExpenseEstimate("Lower Cost");
					}
				}
			} else {
				individualPlan.setExpenseEstimate("Pricey");
			}
			cleanUpBenefitAttributes(individualPlan.getPlanTier1());
			cleanUpBenefitAttributes(individualPlan.getPlanTier2());
			cleanUpBenefitAttributes(individualPlan.getPlanOutNet());
			cleanUpBenefitAttributes(individualPlan.getNetwkException());
			
		}
	}

	private void getBenefitCostFromSolr(List<PlanRateBenefit> plans, String insuranceType, String coverageStartDate) {
		Map<String, Map<String, Map<String, String>>> planBenefitData = null;
		Map<String, Map<String, Map<String, String>>> planCostData = null;
		Set<Integer> missingPlanIds  = new HashSet<Integer>();
		if (PlanInsuranceType.HEALTH.toString().equals(insuranceType)
				|| PlanInsuranceType.DENTAL.toString().equals(insuranceType)) {
			List<String> planIdlist = getPlanIdlist(plans);
			if (PlanInsuranceType.HEALTH.toString().equals(insuranceType)) {
				planBenefitData = getOptimizedHealthPlanBenefits(planIdlist, coverageStartDate, missingPlanIds);
				planCostData = getOptimizedHealthPlanCosts(planIdlist, missingPlanIds);
			} else {
				planBenefitData = getOptimizedDentalPlanBenefits(planIdlist, coverageStartDate, missingPlanIds);
				planCostData = getOptimizedDentalPlanCosts(planIdlist, missingPlanIds);
			}
			List<Integer> missingPlans = new ArrayList<Integer>(missingPlanIds);
			setBenifitAndCosts(planBenefitData, planCostData, plans, insuranceType, coverageStartDate, missingPlans);
		}
	}

	public void setBenifitAndCosts(Map<String, Map<String, Map<String, String>>> planBenefitData,
			Map<String, Map<String, Map<String, String>>> healthCostData, List<PlanRateBenefit> plans,
			String insuranceType, String coverageStartDate, List<Integer> missingPlanIds) {
		for (PlanRateBenefit planRateBenefit : plans) {
			if (planRateBenefit.getIsPuf() != null && "N".equals(planRateBenefit.getIsPuf())) {
				Map<String, Map<String, String>> planBenefits = planBenefitData
						.get(String.valueOf(planRateBenefit.getId()));
				if (planBenefits != null) {
					planRateBenefit.setPlanBenefits(planBenefits);
				}
				Map<String, Map<String, String>> costMap = healthCostData.get(String.valueOf(planRateBenefit.getId()));
				if (costMap != null) {
					populateCostMap(planRateBenefit, costMap);
				}
			}
		}
		// ---fallback mechanism to fetch benefits and costs from DB again---
		if (!missingPlanIds.isEmpty()) {
			long startfallbackMechanism = TimeShifterUtil.currentTimeMillis();
			List<PlanRateBenefit> responseData = planDisplayUtil.getIndividualPlanBenefitsAndCost(missingPlanIds,
					insuranceType, coverageStartDate);
			if (responseData != null && !responseData.isEmpty()) {
				for (PlanRateBenefit planRateBenefit : plans) {
					Integer key = Integer.valueOf(planRateBenefit.getId());
					if (missingPlanIds.contains(key)) {
						for (PlanRateBenefit obj : responseData) {
							if (obj.getId() == planRateBenefit.getId()) {
								planRateBenefit.setPlanBenefits(obj.getPlanBenefits());
								Map<String, Map<String, String>> costMap = obj.getPlanCosts();
								populateCostMap(planRateBenefit, costMap);
							}
						}
					}
				}
			}

			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Before async step 1---->", null, false);
			planSolrUpdateService.saveMissingSolrDocs(missingPlanIds, insuranceType);
			long endfallbackMechanism = TimeShifterUtil.currentTimeMillis();
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO,
					"<----Time taken by fallback mechanism---->" + (endfallbackMechanism - startfallbackMechanism),
					null, false);
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----After async step 2---->", null, false);
		}
	}
	
	private void populateCostMap(PlanRateBenefit planRateBenefit, Map<String, Map<String, String>> costMap) {
		Map<String, Map<String, String>> planCostMap = new HashMap<String, Map<String, String>>();
		Map<String, Map<String, String>> optionalPlanCostMap = new HashMap<String, Map<String, String>>();
		List<String> costList = getCostNames();
		Set<Map.Entry<String, Map<String, String>>> entrySet = costMap.entrySet();
		for (Map.Entry<String, Map<String, String>> entry : entrySet) {
			if (costList.contains(entry.getKey())) {
				planCostMap.put(entry.getKey(), entry.getValue());
			} else {
				optionalPlanCostMap.put(entry.getKey(), entry.getValue());
			}
		}
		planRateBenefit.setPlanCosts(planCostMap);
		planRateBenefit.setOptionalDeductible(optionalPlanCostMap);
	}
	
	public List<String> getCostNames() {
		List<String> deductibles = new ArrayList<String>();
		deductibles.add(CostNames.MAX_OOP_MEDICAL.toString());
		deductibles.add(CostNames.MAX_OOP_DRUG.toString());
		deductibles.add(CostNames.MAX_OOP_INTG_MED_DRUG.toString());
		deductibles.add(CostNames.DEDUCTIBLE_MEDICAL.toString());
		deductibles.add(CostNames.DEDUCTIBLE_DRUG.toString());
		deductibles.add(CostNames.DEDUCTIBLE_INTG_MED_DRUG.toString());
		deductibles.add(CostNames.DEDUCTIBLE_BRAND_NAME_DRUG.toString());
		deductibles.add(CostNames.ANNUAL_MAXIMUM_BENEFIT.toString());
		return deductibles;
	}

	public List<String> getPlanIdlist(List<PlanRateBenefit> list) {
		 List<String> planIdlist = new ArrayList<String>();
		for (PlanRateBenefit planRateBenefit : list) {
			if (planRateBenefit.getIsPuf() != null && "N".equals(planRateBenefit.getIsPuf())) {
				 planIdlist.add(String.valueOf(planRateBenefit.getId()));
			 }
		 }
		return planIdlist;
	}
	
	public SolrQuery getSolrQuery(String commaSeparatedPlanIds, String commaSeparatedBenefitNames, String effectiveDate,
			Integer rows, Integer start) throws IllegalStateException {
		SolrQuery query = new SolrQuery();
		StringBuilder sb = new StringBuilder();
		if (commaSeparatedBenefitNames != null && !commaSeparatedBenefitNames.isEmpty()) {
			sb.append("(")
					.append(commaSeparatedBenefitNames.contains(PlanDisplayConstants.COMMA)
							? commaSeparatedBenefitNames.replaceAll(PlanDisplayConstants.COMMA, PlanDisplayConstants.OR)
							: commaSeparatedBenefitNames)
					.append(")");
		}
		
		if (commaSeparatedPlanIds != null && !commaSeparatedPlanIds.isEmpty()) {
			if (!sb.toString().isEmpty()) {
				sb.append(PlanDisplayConstants.AND);
			}
			sb.append("(")
					.append(commaSeparatedPlanIds.contains(PlanDisplayConstants.COMMA)
							? commaSeparatedPlanIds.replaceAll(PlanDisplayConstants.COMMA, PlanDisplayConstants.OR)
							: commaSeparatedPlanIds)
					.append(")");
		}
		
		if (effectiveDate != null && !effectiveDate.isEmpty()) {
			String upperLimit = getSolrDateRange("*", getSolrDateFormat(effectiveDate), "effEndDate");
			String lowerLimit = getSolrDateRange(getSolrDateFormat(effectiveDate), "*", "effStartDate");
			String dateSearchFilter = upperLimit + PlanDisplayConstants.AND + lowerLimit;
			
			if (!sb.toString().isEmpty()) {
				sb.append(PlanDisplayConstants.AND);
			}
			sb.append(dateSearchFilter);
		}
		
		query.add("q", sb.toString());
		query.setRows(rows);
		query.setStart(start);
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "<----Solr Query---->" + sb.toString(), null, true);
		return query;
	}
	
	public SolrDocumentList findPlanBenefits(SolrServer solrServer, List<String> planIds, List<String> benefitNames,
			String effectiveDatesearchType) {
		SolrDocumentList solrDocumentList = new SolrDocumentList();
		try {
			if (planIds != null && !planIds.isEmpty()) {
				Integer rowsToFetch = 100000;
				Integer start = 0;
				StringBuilder benefitNameSb = getCommaSeperatedValues(benefitNames, "name");
				StringBuilder healthIdSb = getCommaSeperatedValues(planIds, "planId");
				response = solrServer.query(getSolrQuery(healthIdSb.toString(), benefitNameSb.toString(),
						effectiveDatesearchType, rowsToFetch, start));
				if (response != null && response.getResults() != null) {
					solrDocumentList = response.getResults();
					if (solrDocumentList.getNumFound() > rowsToFetch) {
						start = rowsToFetch;
						// --- If there are more than 100000 records returned by
						// above query, execute again to fetch remaining records
						// ---
						rowsToFetch = ((int) (solrDocumentList.getNumFound() - rowsToFetch)) + 1;
						response = solrServer.query(getSolrQuery(healthIdSb.toString(), benefitNameSb.toString(),
								effectiveDatesearchType, rowsToFetch, start));
						solrDocumentList.addAll(response.getResults());
						PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO,
								"<----No of benefit records---->" + solrDocumentList.size(), null, false);
					}
				}
			}

		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Failed to execute search---->", e, false);
			solrDocumentList = new SolrDocumentList();
		}
		return solrDocumentList;
	}
	
	private boolean checkSolrServerAvailability(SolrServer solrServer) {
		boolean chk = false;
		try {
			SolrPingResponse res =  solrServer.ping();
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG,
					"<----Solr Server Ping response---->" + res.getResponseHeader(), null, true);
			if (res.getStatus() == 0) {
				chk = true;
			}
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Failed to ping solr server---->", e, false);
			giExceptionHandler.recordFatalException(Component.PLANDISPLAY, "Failed to ping solr server", e);
		}

		return chk;
	}
	
	public StringBuilder getCommaSeperatedValues(List<String> values, String columnName) {
		StringBuilder sb = new StringBuilder();
		if (values != null) {
			for (String name : values) {
				if ("".equals(sb.toString())) {
					sb.append(columnName).append(PlanDisplayConstants.COLON).append(name);
				} else {
					sb.append(PlanDisplayConstants.COMMA);
					sb.append(columnName).append(PlanDisplayConstants.COLON).append(name);
				}
			}
		}

		return sb;
	}
	
	public String getSolrDateRange(String upperLimit, String lowerLimit, String columnName) {
		StringBuilder sb = new StringBuilder();
		if (upperLimit != null && lowerLimit != null) {
			sb.append(columnName).append(PlanDisplayConstants.COLON).append("[").append(lowerLimit).append(" TO ")
					.append(upperLimit).append("]");
		}

		return sb.toString();
	}

	public String getSolrDateFormat(String dateStr) {
		String solrDate = null;
		DateFormat simpleDf = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat solrDf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		solrDf.setTimeZone(TimeZone.getTimeZone("UTC"));
		try {
			solrDate = solrDf.format(simpleDf.parse(dateStr));
		} catch (java.text.ParseException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		return solrDate;
	}
	
	public SolrDocumentList findPlanCosts(SolrServer solrCostServer, List<String> planIds) {
		SolrDocumentList solrDocumentList = null;
		try {
			if (planIds != null && !planIds.isEmpty()) {
				if (checkSolrServerAvailability(solrCostServer)) {
					Integer rowsToFetch = 100000;
					Integer start = 0;
					StringBuilder healthIdSb = getCommaSeperatedValues(planIds, "planId");
					response = solrCostServer
							.query(getSolrQuery(healthIdSb.toString(), null, null, rowsToFetch, start));
					solrDocumentList = response.getResults();
					if (solrDocumentList.getNumFound() > rowsToFetch) {
						start = rowsToFetch;
						// --- If there are more than 30000 records returned by
						// above query, execute again to fetch remaining records
						// ---
						rowsToFetch = ((int) (solrDocumentList.getNumFound() - rowsToFetch)) + 1;
						response = solrCostServer
								.query(getSolrQuery(healthIdSb.toString(), null, null, rowsToFetch, start));
						solrDocumentList.addAll(response.getResults());
						PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO,
								"<----No of records---->" + solrDocumentList.size(), null, false);
					}
				}
			} else {
				solrDocumentList = new SolrDocumentList();
			}
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Failed to execute search---->", e, false);
			solrDocumentList = new SolrDocumentList();
		}
		return solrDocumentList;
	}

	public String formatNumber(Number n) {
        NumberFormat format = DecimalFormat.getInstance();
        format.setRoundingMode(RoundingMode.FLOOR);
        format.setMinimumFractionDigits(0);
        format.setMaximumFractionDigits(0);
        format.setGroupingUsed(false);
        return format.format(n);
    }
	
	public Map<String, Map<String, Map<String, String>>> getOptimizedHealthPlanCosts(List<String> planIds,
			Set<Integer> missingPlanIds) {
		long startgetOptimizedHealthPlanCosts = TimeShifterUtil.currentTimeMillis();
		Map<String, Map<String, Map<String, String>>> planCostsDataMap = new HashMap<String, Map<String, Map<String, String>>>();
		List<PlanHealthCostDTO> list = getPlanHealthCostDTOList(findPlanCosts(solrHealthCostServer, planIds));
		List<String>  planIdList = new ArrayList<String>();
		planIdList.addAll(planIds);
		if (!list.isEmpty()) {
			for (PlanHealthCostDTO planHealthCostDTO : list) {
				Map<String, String> costAttrib = new HashMap<String, String>();
				String name = planHealthCostDTO.getName();
				String planId = String.valueOf(planHealthCostDTO.getPlanId());
				if (planIdList.contains(planId)) {
					planIdList.remove(planId);
				}
				costAttrib.put("inNetworkInd", (planHealthCostDTO.getInNetWorkInd() == null) ? ""
						: formatNumber(planHealthCostDTO.getInNetWorkInd()));
				costAttrib.put("inNetworkFly", (planHealthCostDTO.getInNetWorkFly() == null) ? ""
						: formatNumber(planHealthCostDTO.getInNetWorkFly()));
				costAttrib.put("inNetworkTier2Ind", (planHealthCostDTO.getInNetworkTier2Ind() == null) ? ""
						: formatNumber(planHealthCostDTO.getInNetworkTier2Ind()));
				costAttrib.put("inNetworkTier2Fly", (planHealthCostDTO.getInNetworkTier2Fly() == null) ? ""
						: formatNumber(planHealthCostDTO.getInNetworkTier2Fly()));
				costAttrib.put("outNetworkInd", (planHealthCostDTO.getOutNetworkInd() == null) ? ""
						: formatNumber(planHealthCostDTO.getOutNetworkInd()));
				costAttrib.put("outNetworkFly", (planHealthCostDTO.getOutNetworkFly() == null) ? ""
						: formatNumber(planHealthCostDTO.getOutNetworkFly()));
				costAttrib.put("combinedInOutNetworkInd", (planHealthCostDTO.getCombinedInOutNetworkInd() == null) ? ""
						: formatNumber(planHealthCostDTO.getCombinedInOutNetworkInd()));
				costAttrib.put("combinedInOutNetworkFly", (planHealthCostDTO.getCombinedInOutNetworkFly() == null) ? ""
						: formatNumber(planHealthCostDTO.getCombinedInOutNetworkFly()));
				costAttrib.put("combinedDefCoinsNetworkTier1", (planHealthCostDTO.getCombDefCoinsNetworkTier1() == null)
						? "" : formatNumber(planHealthCostDTO.getCombDefCoinsNetworkTier1()));
				costAttrib.put("combinedDefCoinsNetworkTier2", (planHealthCostDTO.getCombDefCoinsNetworkTier2() == null)
						? "" : formatNumber(planHealthCostDTO.getCombDefCoinsNetworkTier2()));
				costAttrib.put("limitExcepDisplay", (planHealthCostDTO.getLimitExcepDisplay() == null) ? ""
						: planHealthCostDTO.getLimitExcepDisplay());
				
				if (planCostsDataMap.get(planId) == null) {
					planCostsDataMap.put(planId, new HashMap<String, Map<String, String>>());
				}
				planCostsDataMap.get(planId).put(name, costAttrib);
			}
		}

		addMissingPlanIds(planIdList, missingPlanIds);

		long endgetOptimizedHealthPlanCosts = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "<----Time taken by getOptimizedHealthPlanCosts()---->"
				+ (endgetOptimizedHealthPlanCosts - startgetOptimizedHealthPlanCosts), null, false);
		return planCostsDataMap;
	}
	
	public Map<String, Map<String, Map<String, String>>> getOptimizedHealthPlanBenefits(List<String> planIds,
			String effectiveDate, Set<Integer> missingPlanIds) {
		// pull require health plan benefits only	
		long startgetOptimizedHealthPlanBenefits = TimeShifterUtil.currentTimeMillis();
		String[] helathBenefitListArr = GhixConstants.helathBenefitList;
		List<String> benefitNames = Arrays.asList(helathBenefitListArr);
		List<PlanHealthBenefitDTO> list = getPlanHealthBenefitDTOList(
				findPlanBenefits(solrPlanBenefitServer, planIds, benefitNames, effectiveDate));
		List<String>  planIdList = new ArrayList<String>();
		planIdList.addAll(planIds);
		Map<String, Map<String, Map<String, String>>> benefitDataMap = new HashMap<String, Map<String, Map<String, String>>>();
		
		for (String planId : planIds) {
			benefitDataMap.put(planId, new HashMap<String, Map<String, String>>());
		}
		
		if (!list.isEmpty()) {
			for (PlanHealthBenefitDTO planHealthBenefitDTO : list) {
				String benefitName = planHealthBenefitDTO.getName();
				String planId = String.valueOf(planHealthBenefitDTO.getPlanId());
				planIdList.remove(planId);
				Map<String, String> benefitAttrib = prepareBenefitVal(planHealthBenefitDTO);
				benefitDataMap.get(planId).put(benefitName, benefitAttrib);
			}
		}

		addMissingPlanIds(planIdList, missingPlanIds);
		Set<Map.Entry<String, Map<String, Map<String, String>>>> entrySet = benefitDataMap.entrySet();
		for (Map.Entry<String, Map<String, Map<String, String>>> entry : entrySet) {
			if (!("0".equals(entry.getKey()))) {
				for (String benefitName : benefitNames) {
					if (entry.getValue().get(benefitName) == null) {
						PlanHealthBenefitDTO planHealthBenefitDTO = new PlanHealthBenefitDTO();
						Map<String, String> benefitAttrib = prepareBenefitVal(planHealthBenefitDTO);
						entry.getValue().put(benefitName, benefitAttrib);
					}
				}
			}
		}
		long endgetOptimizedHealthPlanBenefits = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "<----Time taken by getOptimizedHealthPlanBenefits()---->"
				+ (endgetOptimizedHealthPlanBenefits - startgetOptimizedHealthPlanBenefits), null, false);
		return benefitDataMap;
	}
	
	private void addMissingPlanIds(List<String> planIds, Set<Integer> missingPlanIds) {
		for (String planId : planIds) {
			missingPlanIds.add(Integer.valueOf(planId));
		}
	}

	private Map<String, String> prepareBenefitVal(PlanHealthBenefitDTO planHealthBenefitDTO) {
		Map<String, String> benefitAttrib = new HashMap<String, String>();
		benefitAttrib.put("netWkT1Disp",
				(planHealthBenefitDTO.getNetworkT1display() == null) ? "" : planHealthBenefitDTO.getNetworkT1display());
		benefitAttrib.put("netWkT2Disp",
				(planHealthBenefitDTO.getNetworkT2display() == null) ? "" : planHealthBenefitDTO.getNetworkT2display());
		benefitAttrib.put("outNetWkDisp", (planHealthBenefitDTO.getOutOfNetworkDisplay() == null) ? ""
				: planHealthBenefitDTO.getOutOfNetworkDisplay());
		benefitAttrib.put("limitExcepDisp", (planHealthBenefitDTO.getLimitExcepDisplay() == null) ? ""
				: planHealthBenefitDTO.getLimitExcepDisplay());
		benefitAttrib.put("netWkT1TileDisp", (planHealthBenefitDTO.getNetworkT1TileDisplay() == null) ? ""
				: planHealthBenefitDTO.getNetworkT1TileDisplay());
		benefitAttrib.put("netwkLimit", (planHealthBenefitDTO.getNetworkLimitation() == null) ? ""
				: planHealthBenefitDTO.getNetworkLimitation());
		benefitAttrib.put("netwkLimitAttrib", (planHealthBenefitDTO.getNetworkLimitationAttribute() == null) ? ""
				: planHealthBenefitDTO.getNetworkLimitationAttribute());
		benefitAttrib.put("netwkExce", (planHealthBenefitDTO.getNetworkExceptions() == null) ? ""
				: planHealthBenefitDTO.getNetworkExceptions());
		benefitAttrib.put("tier1CopayVal", (planHealthBenefitDTO.getNetworkT1CopayVal() == null) ? ""
				: planHealthBenefitDTO.getNetworkT1CopayVal());
		benefitAttrib.put("tier1CopayAttrib", (planHealthBenefitDTO.getNetworkT1CopayAttr() == null) ? ""
				: planHealthBenefitDTO.getNetworkT1CopayAttr());
		benefitAttrib.put("tier1CoinsVal", (planHealthBenefitDTO.getNetworkT1CoinsurVal() == null) ? ""
				: planHealthBenefitDTO.getNetworkT1CoinsurVal());
		benefitAttrib.put("tier1CoinsAttrib", (planHealthBenefitDTO.getNetworkT1CoinsurAttr() == null) ? ""
				: planHealthBenefitDTO.getNetworkT1CoinsurAttr());
		benefitAttrib.put("tier2CopayVal", (planHealthBenefitDTO.getNetworkT2CopayVal() == null) ? ""
				: planHealthBenefitDTO.getNetworkT2CopayVal());
		benefitAttrib.put("tier2CopayAttrib", (planHealthBenefitDTO.getNetworkT2CopayAttr() == null) ? ""
				: planHealthBenefitDTO.getNetworkT2CopayAttr());
		benefitAttrib.put("tier2CoinsVal", (planHealthBenefitDTO.getNetworkT2CoinsurVal() == null) ? ""
				: planHealthBenefitDTO.getNetworkT2CoinsurVal());
		benefitAttrib.put("tier2CoinsAttrib", (planHealthBenefitDTO.getNetworkT2CoinsurAttr() == null) ? ""
				: planHealthBenefitDTO.getNetworkT2CoinsurAttr());
		benefitAttrib.put("outNetWkCopayVal", (planHealthBenefitDTO.getOutOfNetworkCopayVal() == null) ? ""
				: planHealthBenefitDTO.getOutOfNetworkCopayVal());
		benefitAttrib.put("outNetWkCopayAttrib", (planHealthBenefitDTO.getOutOfNetworkCopayAttr() == null) ? ""
				: planHealthBenefitDTO.getOutOfNetworkCopayAttr());
		benefitAttrib.put("outNetWkCoinsVal", (planHealthBenefitDTO.getOutOfNetworkCoinsurVal() == null) ? ""
				: planHealthBenefitDTO.getOutOfNetworkCoinsurVal());
		benefitAttrib.put("outNetWkCoinsAttrib", (planHealthBenefitDTO.getOutOfNetworkCoinsurAttr() == null) ? ""
				: planHealthBenefitDTO.getOutOfNetworkCoinsurAttr());
		benefitAttrib.put("subToNetDeduct", (planHealthBenefitDTO.getSubjectToInNetworkDuductible() == null) ? ""
				: planHealthBenefitDTO.getSubjectToInNetworkDuductible());
		benefitAttrib.put("subToNonNetDeduct", (planHealthBenefitDTO.getSubjectToOutNetworkDeductible() == null) ? ""
				: planHealthBenefitDTO.getSubjectToOutNetworkDeductible());
		benefitAttrib.put("isCovered",
				(planHealthBenefitDTO.getIsCovered() == null) ? "" : planHealthBenefitDTO.getIsCovered());
		benefitAttrib.put("explanation",
				(planHealthBenefitDTO.getExplanation() == null) ? "" : planHealthBenefitDTO.getExplanation());
		return benefitAttrib;
	}

	private List<PlanHealthCostDTO> getPlanHealthCostDTOList(SolrDocumentList solrDocumentList) {
		List<PlanHealthCostDTO> list =  new ArrayList<PlanHealthCostDTO>();
		for (SolrDocument solrDocument : solrDocumentList) {
			list.add(buildPlanHealthCostDTO(solrDocument));
		}
		return list;
	}

	public PlanHealthCostDTO buildPlanHealthCostDTO(SolrDocument result) {
		PlanHealthCostDTO planHealthCostDTO = new PlanHealthCostDTO();
		planHealthCostDTO.setName((String) result.get("name"));
		planHealthCostDTO.setId((Integer) result.get("id"));
		planHealthCostDTO.setPlanId((Integer) result.get("planId"));
		planHealthCostDTO.setPlanHealthId((Integer) result.get("planHealthId"));
		planHealthCostDTO.setCombDefCoinsNetworkTier1(getDoubleValue(result.get("combDefCoinsNetworkTier1")));
		planHealthCostDTO.setCombDefCoinsNetworkTier2(getDoubleValue(result.get("combDefCoinsNetworkTier2")));
		planHealthCostDTO.setCombinedInOutNetworkFly(getDoubleValue(result.get("combinedInOutNetworkFly")));
		planHealthCostDTO.setCombinedInOutNetworkInd(getDoubleValue(result.get("combinedInOutNetworkInd")));
		planHealthCostDTO.setInNetWorkFly(getDoubleValue(result.get("inNetworkFly")));
		planHealthCostDTO.setInNetWorkInd(getDoubleValue(result.get("inNetWorkInd")));
		planHealthCostDTO.setInNetworkTier2Fly(getDoubleValue(result.get("inNetworkTier2Fly")));
		planHealthCostDTO.setInNetworkTier2Ind(getDoubleValue(result.get("inNetworkTier2Ind")));
		planHealthCostDTO.setLimitExcepDisplay((String) result.get("limitExcepDisplay"));
		planHealthCostDTO.setOutNetworkFly(getDoubleValue(result.get("outNetworkFly")));
		planHealthCostDTO.setOutNetworkInd(getDoubleValue(result.get("outNetworkInd")));
		return planHealthCostDTO;
	}

	private List<PlanDentalCostDTO> getPlanDentalCostDTOList(SolrDocumentList solrDocumentList) {
		List<PlanDentalCostDTO> list =  new ArrayList<PlanDentalCostDTO>();
		for (SolrDocument solrDocument : solrDocumentList) {
			list.add(buildPlanDentalCostDTO(solrDocument));
		}
		return list;
	}

	public PlanDentalCostDTO buildPlanDentalCostDTO(SolrDocument result) {
		PlanDentalCostDTO planDentalCostDTO = new PlanDentalCostDTO();
		planDentalCostDTO.setName((String) result.get("name"));
		planDentalCostDTO.setId((Integer) result.get("id"));
		planDentalCostDTO.setPlanId((Integer) result.get("planId"));
		planDentalCostDTO.setPlanDentalId((Integer) result.get("planDentalId"));
		planDentalCostDTO.setCombDefCoinsNetworkTier1(getDoubleValue(result.get("combDefCoinsNetworkTier1")));
		planDentalCostDTO.setCombDefCoinsNetworkTier2(getDoubleValue(result.get("combDefCoinsNetworkTier2")));
		planDentalCostDTO.setCombinedInOutNetworkFly(getDoubleValue(result.get("combinedInOutNetworkFly")));
		planDentalCostDTO.setCombinedInOutNetworkInd(getDoubleValue(result.get("combinedInOutNetworkInd")));
		planDentalCostDTO.setInNetWorkFly(getDoubleValue(result.get("inNetworkFly")));
		planDentalCostDTO.setInNetWorkInd(getDoubleValue(result.get("inNetWorkInd")));
		planDentalCostDTO.setInNetworkTier2Fly(getDoubleValue(result.get("inNetworkTier2Fly")));
		planDentalCostDTO.setInNetworkTier2Ind(getDoubleValue(result.get("inNetworkTier2Ind")));
		planDentalCostDTO.setLimitExcepDisplay((String) result.get("limitExcepDisplay"));
		planDentalCostDTO.setOutNetworkFly(getDoubleValue(result.get("outNetworkFly")));
		planDentalCostDTO.setOutNetworkInd(getDoubleValue(result.get("outNetworkInd")));
		return planDentalCostDTO;
	}

	private List<PlanHealthBenefitDTO> getPlanHealthBenefitDTOList(SolrDocumentList solrDocumentList) {
		List<PlanHealthBenefitDTO> list =  new ArrayList<PlanHealthBenefitDTO>();
		for (SolrDocument solrDocument : solrDocumentList) {
			list.add(buildPlanHealthBenefitDTO(solrDocument));
		}
		return list;
	}
	
	public Map<String, Map<String, Map<String, String>>> getOptimizedDentalPlanBenefits(List<String> planIds,
			String effectiveDate, Set<Integer> missingPlanIds) {
		// pull require health plan benefits only	
		long startgetOptimizedDentalPlanBenefits = TimeShifterUtil.currentTimeMillis();
		String[] dentalBenefitListArr = GhixConstants.dentalBenefitList;
		List<String> benefitNames = Arrays.asList(dentalBenefitListArr);
		List<PlanDentalBenefitDTO> list = getPlanDentalBenefitDTOList(
				findPlanBenefits(solrDentalBenefitServer, planIds, benefitNames, effectiveDate));
		Map<String, Map<String, Map<String, String>>> benefitDataMap = new HashMap<String, Map<String, Map<String, String>>>();
		List<String> planIdList = new ArrayList<String>();
		planIdList.addAll(planIds);
		
		for (String planId : planIds) {
			benefitDataMap.put(planId, new HashMap<String, Map<String, String>>());
		}
		
		if (!list.isEmpty()) {
			for (PlanDentalBenefitDTO planDentalBenefitDTO : list) {
				String benefitName = planDentalBenefitDTO.getName();
				String planId = String.valueOf(planDentalBenefitDTO.getPlanId());
				if (planIdList.contains(planId)) {
					planIdList.remove(planId);
				}
				Map<String, String> benefitAttrib = prepareDentalBenefitVal(planDentalBenefitDTO);
				benefitDataMap.get(planId).put(benefitName, benefitAttrib);
			}
		}

		addMissingPlanIds(planIdList, missingPlanIds);
		
		Set<Map.Entry<String, Map<String, Map<String, String>>>> entrySet = benefitDataMap.entrySet();
		for (Map.Entry<String, Map<String, Map<String, String>>> entry : entrySet) {
			if (!("0".equals(entry.getKey()))) {
				for (String benefitName : benefitNames) {
					if (entry.getValue().get(benefitName) == null) {
						PlanDentalBenefitDTO planDentalBenefitDTO = new PlanDentalBenefitDTO();
						Map<String, String> benefitAttrib = prepareDentalBenefitVal(planDentalBenefitDTO);
						entry.getValue().put(benefitName, benefitAttrib);
					}
				}
			}
		}

		long endgetOptimizedDentalPlanBenefits = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "<----Time taken by getOptimizedDentalPlanBenefits()---->"
				+ (endgetOptimizedDentalPlanBenefits - startgetOptimizedDentalPlanBenefits), null, false);
		return benefitDataMap;
	}

	private Map<String, String> prepareDentalBenefitVal(PlanDentalBenefitDTO planDentalBenefitDTO) {
		Map<String, String> benefitAttrib = new HashMap<String, String>();

		benefitAttrib.put("netWkT1Disp",
				(planDentalBenefitDTO.getNetworkT1display() == null) ? "" : planDentalBenefitDTO.getNetworkT1display());
		benefitAttrib.put("netWkT2Disp",
				(planDentalBenefitDTO.getNetworkT2display() == null) ? "" : planDentalBenefitDTO.getNetworkT2display());
		benefitAttrib.put("outNetWkDisp", (planDentalBenefitDTO.getOutOfNetworkDisplay() == null) ? ""
				: planDentalBenefitDTO.getOutOfNetworkDisplay());
		benefitAttrib.put("limitExcepDisp", (planDentalBenefitDTO.getLimitExcepDisplay() == null) ? ""
				: planDentalBenefitDTO.getLimitExcepDisplay());
		benefitAttrib.put("netWkT1TileDisp", (planDentalBenefitDTO.getNetworkT1TileDisplay() == null) ? ""
				: planDentalBenefitDTO.getNetworkT1TileDisplay());
		benefitAttrib.put("netwkLimit", (planDentalBenefitDTO.getNetworkLimitation() == null) ? ""
				: planDentalBenefitDTO.getNetworkLimitation());
		benefitAttrib.put("netwkLimitAttrib", (planDentalBenefitDTO.getNetworkLimitationAttribute() == null) ? ""
				: planDentalBenefitDTO.getNetworkLimitationAttribute());
		benefitAttrib.put("netwkExce", (planDentalBenefitDTO.getNetworkExceptions() == null) ? ""
				: planDentalBenefitDTO.getNetworkExceptions());
		benefitAttrib.put("tier1CopayVal", (planDentalBenefitDTO.getNetworkT1CopayVal() == null) ? ""
				: planDentalBenefitDTO.getNetworkT1CopayVal());
		benefitAttrib.put("tier1CopayAttrib", (planDentalBenefitDTO.getNetworkT1CopayAttr() == null) ? ""
				: planDentalBenefitDTO.getNetworkT1CopayAttr());
		benefitAttrib.put("tier1CoinsVal", (planDentalBenefitDTO.getNetworkT1CoinsurVal() == null) ? ""
				: planDentalBenefitDTO.getNetworkT1CoinsurVal());
		benefitAttrib.put("tier1CoinsAttrib", (planDentalBenefitDTO.getNetworkT1CoinsurAttr() == null) ? ""
				: planDentalBenefitDTO.getNetworkT1CoinsurAttr());
		benefitAttrib.put("tier2CopayVal", (planDentalBenefitDTO.getNetworkT2CopayVal() == null) ? ""
				: planDentalBenefitDTO.getNetworkT2CopayVal());
		benefitAttrib.put("tier2CopayAttrib", (planDentalBenefitDTO.getNetworkT2CopayAttr() == null) ? ""
				: planDentalBenefitDTO.getNetworkT2CopayAttr());
		benefitAttrib.put("tier2CoinsVal", (planDentalBenefitDTO.getNetworkT2CoinsurVal() == null) ? ""
				: planDentalBenefitDTO.getNetworkT2CoinsurVal());
		benefitAttrib.put("tier2CoinsAttrib", (planDentalBenefitDTO.getNetworkT2CoinsurAttr() == null) ? ""
				: planDentalBenefitDTO.getNetworkT2CoinsurAttr());
		benefitAttrib.put("outNetWkCopayVal", (planDentalBenefitDTO.getOutOfNetworkCopayVal() == null) ? ""
				: planDentalBenefitDTO.getOutOfNetworkCopayVal());
		benefitAttrib.put("outNetWkCopayAttrib", (planDentalBenefitDTO.getOutOfNetworkCopayAttr() == null) ? ""
				: planDentalBenefitDTO.getOutOfNetworkCopayAttr());
		benefitAttrib.put("outNetWkCoinsVal", (planDentalBenefitDTO.getOutOfNetworkCoinsurVal() == null) ? ""
				: planDentalBenefitDTO.getOutOfNetworkCoinsurVal());
		benefitAttrib.put("outNetWkCoinsAttrib", (planDentalBenefitDTO.getOutOfNetworkCoinsurAttr() == null) ? ""
				: planDentalBenefitDTO.getOutOfNetworkCoinsurAttr());
		benefitAttrib.put("subToNetDeduct", (planDentalBenefitDTO.getSubjectToInNetworkDuductible() == null) ? ""
				: planDentalBenefitDTO.getSubjectToInNetworkDuductible());
		benefitAttrib.put("subToNonNetDeduct", (planDentalBenefitDTO.getSubjectToOutNetworkDeductible() == null) ? ""
				: planDentalBenefitDTO.getSubjectToOutNetworkDeductible());
		benefitAttrib.put("isCovered",
				(planDentalBenefitDTO.getIsCovered() == null) ? "" : planDentalBenefitDTO.getIsCovered());
		benefitAttrib.put("explanation",
				(planDentalBenefitDTO.getExplanation() == null) ? "" : planDentalBenefitDTO.getExplanation());
		return benefitAttrib;
	}

	private List<PlanDentalBenefitDTO> getPlanDentalBenefitDTOList(SolrDocumentList solrDocumentList) {
		List<PlanDentalBenefitDTO> list =  new ArrayList<PlanDentalBenefitDTO>();
		for (SolrDocument solrDocument : solrDocumentList) {
			list.add(buildPlanDentalBenefitDTO(solrDocument));
		}
		return list;
	}
	
	public PlanDentalBenefitDTO buildPlanDentalBenefitDTO(SolrDocument result) {
		PlanDentalBenefitDTO planDentalBenefitDTO = new PlanDentalBenefitDTO();
		planDentalBenefitDTO.setName((String) result.get("name"));
		planDentalBenefitDTO.setId((Integer) result.get("id"));
		planDentalBenefitDTO.setPlanId((Integer) result.get("planId"));
		planDentalBenefitDTO.setPlanDentalId((Integer) result.get("planDentalId"));
		planDentalBenefitDTO.setExcludedFromInNetworkMoop((String) result.get("excludedFromInNetworkMoop"));
		planDentalBenefitDTO.setExcludedFromOutOfNetworkMoop((String) result.get("excludedFromOutOfNetworkMoop"));
		planDentalBenefitDTO.setExplanation((String) result.get("explanation"));
		planDentalBenefitDTO.setIsCovered((String) result.get("isCovered"));
		planDentalBenefitDTO.setIsEHB((String) result.get("isEHB"));
		planDentalBenefitDTO.setLimitExcepDisplay((String) result.get("limitExcepDisplay"));
		planDentalBenefitDTO.setMinStay((String) result.get("minStay"));
		planDentalBenefitDTO.setNetworkExceptions((String) result.get("networkExceptions"));
		planDentalBenefitDTO.setNetworkLimitation((String) result.get("networkLimitation"));
		planDentalBenefitDTO.setNetworkLimitationAttribute((String) result.get("networkLimitationAttribute"));
		planDentalBenefitDTO.setNetworkT1CoinsurAttr((String) result.get("networkT1CoinsurAttr"));
		planDentalBenefitDTO.setNetworkT1CoinsurVal((String) result.get("networkT1CoinsurVal"));
		planDentalBenefitDTO.setNetworkT1CopayAttr((String) result.get("networkT1CopayAttr"));
		planDentalBenefitDTO.setNetworkT1CopayVal((String) result.get("networkT1CopayVal"));
		planDentalBenefitDTO.setNetworkT1display((String) result.get("networkT1display"));
		planDentalBenefitDTO.setNetworkT1TileDisplay((String) result.get("networkT1TileDisplay"));
		planDentalBenefitDTO.setNetworkT2CoinsurAttr((String) result.get("networkT2CoinsurAttr"));
		planDentalBenefitDTO.setNetworkT2CoinsurVal((String) result.get("networkT2CoinsurVal"));
		planDentalBenefitDTO.setNetworkT2CopayAttr((String) result.get("networkT2CopayAttr"));
		planDentalBenefitDTO.setNetworkT2CopayVal((String) result.get("networkT2CopayVal"));
		planDentalBenefitDTO.setNetworkT2display((String) result.get("networkT2display"));
		planDentalBenefitDTO.setOutOfNetworkCoinsurAttr((String) result.get("outOfNetworkCoinsurAttr"));
		planDentalBenefitDTO.setOutOfNetworkCoinsurVal((String) result.get("outOfNetworkCoinsurVal"));
		planDentalBenefitDTO.setOutOfNetworkCopayAttr((String) result.get("outOfNetworkCopayAttr"));
		planDentalBenefitDTO.setOutOfNetworkCopayVal((String) result.get("outOfNetworkCopayVal"));
		planDentalBenefitDTO.setOutOfNetworkDisplay((String) result.get("outOfNetworkDisplay"));
		planDentalBenefitDTO.setSubjectToInNetworkDuductible((String) result.get("subjectToInNetworkDuductible"));
		planDentalBenefitDTO.setSubjectToOutNetworkDeductible((String) result.get("subjectToOutNetworkDeductible"));
		return planDentalBenefitDTO;
	}
	
	public Map<String, Map<String, Map<String, String>>> getOptimizedDentalPlanCosts(List<String> planIds,
			Set<Integer> missingPlanIds) {
		long startgetOptimizedDentalPlanCosts = TimeShifterUtil.currentTimeMillis();
		Map<String, Map<String, Map<String, String>>> planDentalDataMap = new HashMap<String, Map<String, Map<String, String>>>();
		List<PlanDentalCostDTO> list = getPlanDentalCostDTOList(findPlanCosts(solrDentalCostServer, planIds));
		List<String>  planIdList = new ArrayList<String>();
		planIdList.addAll(planIds);
		if (!list.isEmpty()) {
			for (PlanDentalCostDTO planDentalCostDTO : list) {
				Map<String, String> costAttrib = new HashMap<String, String>();
				String name = planDentalCostDTO.getName();
				String planId = String.valueOf(planDentalCostDTO.getPlanId());
				if (planIdList.contains(planId)) {
					planIdList.remove(planId);
				}
				costAttrib.put("inNetworkInd", (planDentalCostDTO.getInNetWorkInd() == null) ? ""
						: formatNumber(planDentalCostDTO.getInNetWorkInd()));
				costAttrib.put("inNetworkFly", (planDentalCostDTO.getInNetWorkFly() == null) ? ""
						: formatNumber(planDentalCostDTO.getInNetWorkFly()));
				costAttrib.put("inNetworkTier2Ind", (planDentalCostDTO.getInNetworkTier2Ind() == null) ? ""
						: formatNumber(planDentalCostDTO.getInNetworkTier2Ind()));
				costAttrib.put("inNetworkTier2Fly", (planDentalCostDTO.getInNetworkTier2Fly() == null) ? ""
						: formatNumber(planDentalCostDTO.getInNetworkTier2Fly()));
				costAttrib.put("outNetworkInd", (planDentalCostDTO.getOutNetworkInd() == null) ? ""
						: formatNumber(planDentalCostDTO.getOutNetworkInd()));
				costAttrib.put("outNetworkFly", (planDentalCostDTO.getOutNetworkFly() == null) ? ""
						: formatNumber(planDentalCostDTO.getOutNetworkFly()));
				costAttrib.put("combinedInOutNetworkInd", (planDentalCostDTO.getCombinedInOutNetworkInd() == null) ? ""
						: formatNumber(planDentalCostDTO.getCombinedInOutNetworkInd()));
				costAttrib.put("combinedInOutNetworkFly", (planDentalCostDTO.getCombinedInOutNetworkFly() == null) ? ""
						: formatNumber(planDentalCostDTO.getCombinedInOutNetworkFly()));
				costAttrib.put("combinedDefCoinsNetworkTier1", (planDentalCostDTO.getCombDefCoinsNetworkTier1() == null)
						? "" : formatNumber(planDentalCostDTO.getCombDefCoinsNetworkTier1()));
				costAttrib.put("combinedDefCoinsNetworkTier2", (planDentalCostDTO.getCombDefCoinsNetworkTier2() == null)
						? "" : formatNumber(planDentalCostDTO.getCombDefCoinsNetworkTier2()));
				costAttrib.put("limitExcepDisplay", (planDentalCostDTO.getLimitExcepDisplay() == null) ? ""
						: planDentalCostDTO.getLimitExcepDisplay());
				
				if (planDentalDataMap.get(planId) == null) {
					planDentalDataMap.put(planId, new HashMap<String, Map<String, String>>());
				}
				planDentalDataMap.get(planId).put(name, costAttrib);
			}
		}

		addMissingPlanIds(planIdList, missingPlanIds);

		long endgetOptimizedHealthPlanCosts = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "<----Time taken by getOptimizedHealthPlanCosts()---->"
				+ (endgetOptimizedHealthPlanCosts - startgetOptimizedDentalPlanCosts), null, false);
		return planDentalDataMap;
	}

	private Double getDoubleValue(Object value) {
		Double d = null;
		if (value != null) {
			if (value instanceof Double) {
		        d = (Double) value;
			} else if (value instanceof String) {
				 d = Double.parseDouble((String) value);
			}
		}

		return d;
	}
	
	public PlanHealthBenefitDTO buildPlanHealthBenefitDTO(SolrDocument result) {
		PlanHealthBenefitDTO planHealthBenefitDTO = new PlanHealthBenefitDTO();
		planHealthBenefitDTO.setName((String) result.get("name"));
		planHealthBenefitDTO.setId((Integer) result.get("id"));
		planHealthBenefitDTO.setPlanId((Integer) result.get("planId"));
		planHealthBenefitDTO.setPlanHealthId((Integer) result.get("planHealthId"));
		planHealthBenefitDTO
				.setCombinedInAndOutNetworkAttribute((String) result.get("combinedInAndOutNetworkAttribute"));
		planHealthBenefitDTO.setCombinedInAndOutOfNetwork((String) result.get("combinedInAndOutOfNetwork"));
		planHealthBenefitDTO.setCombinedInOutNetworkFamily(getDoubleValue(result.get("combinedInOutNetworkFamily")));			
		planHealthBenefitDTO
				.setCombinedInOutNetworkIndividual(getDoubleValue(result.get("combinedInOutNetworkIndividual")));
		planHealthBenefitDTO.setEhbVarianceReason((String) result.get("ehbVarianceReason"));
		planHealthBenefitDTO.setExcludedFromInNetworkMoop((String) result.get("excludedFromInNetworkMoop"));
		planHealthBenefitDTO.setExcludedFromOutOfNetworkMoop((String) result.get("excludedFromOutOfNetworkMoop"));
		planHealthBenefitDTO.setExplanation((String) result.get("explanation"));
		planHealthBenefitDTO.setInNetworkFamily(getDoubleValue(result.get("inNetworkFamily")));					
		planHealthBenefitDTO.setInNetworkIndividual(getDoubleValue(result.get("inNetworkIndividual")));				
		planHealthBenefitDTO.setInNetworkTierTwoFamily(getDoubleValue(result.get("inNetworkTierTwoFamily")));				
		planHealthBenefitDTO.setInNetworkTierTwoIndividual(getDoubleValue(result.get("inNetworkTierTwoIndividual")));			
		planHealthBenefitDTO.setIsCovered((String) result.get("isCovered"));
		planHealthBenefitDTO.setIsEHB((String) result.get("isEHB"));
		planHealthBenefitDTO.setIsStateMandate((String) result.get("isStateMandate"));
		planHealthBenefitDTO.setLimitExcepDisplay((String) result.get("limitExcepDisplay"));
		planHealthBenefitDTO.setMinStay((String) result.get("minStay"));
		planHealthBenefitDTO.setNetworkExceptions((String) result.get("networkExceptions"));
		planHealthBenefitDTO.setNetworkLimitation((String) result.get("networkLimitation"));
		planHealthBenefitDTO.setNetworkLimitationAttribute((String) result.get("networkLimitationAttribute"));
		planHealthBenefitDTO.setNetworkT1CoinsurAttr((String) result.get("networkT1CoinsurAttr"));
		planHealthBenefitDTO.setNetworkT1CoinsurVal((String) result.get("networkT1CoinsurVal"));
		planHealthBenefitDTO.setNetworkT1CopayAttr((String) result.get("networkT1CopayAttr"));
		planHealthBenefitDTO.setNetworkT1CopayVal((String) result.get("networkT1CopayVal"));
		planHealthBenefitDTO.setNetworkT1display((String) result.get("networkT1display"));
		planHealthBenefitDTO.setNetworkT1TileDisplay((String) result.get("networkT1TileDisplay"));
		planHealthBenefitDTO.setNetworkT2CoinsurAttr((String) result.get("networkT2CoinsurAttr"));
		planHealthBenefitDTO.setNetworkT2CoinsurVal((String) result.get("networkT2CoinsurVal"));
		planHealthBenefitDTO.setNetworkT2CopayAttr((String) result.get("networkT2CopayAttr"));
		planHealthBenefitDTO.setNetworkT2CopayVal((String) result.get("networkT2CopayVal"));
		planHealthBenefitDTO.setNetworkT2display((String) result.get("networkT2display"));
		planHealthBenefitDTO.setOutOfNetworkCoinsurAttr((String) result.get("outOfNetworkCoinsurAttr"));
		planHealthBenefitDTO.setOutOfNetworkCoinsurVal((String) result.get("outOfNetworkCoinsurVal"));
		planHealthBenefitDTO.setOutOfNetworkCopayAttr((String) result.get("outOfNetworkCopayAttr"));
		planHealthBenefitDTO.setOutOfNetworkCopayVal((String) result.get("outOfNetworkCopayVal"));
		planHealthBenefitDTO.setOutOfNetworkDisplay((String) result.get("outOfNetworkDisplay"));
		planHealthBenefitDTO.setOutOfNetworkFamily(getDoubleValue(result.get("outOfNetworkFamily")));				
		planHealthBenefitDTO.setOutOfNetworkIndividual(getDoubleValue(result.get("outOfNetworkIndividual")));			
		planHealthBenefitDTO.setServiceLimit((String) result.get("serviceLimit"));
		planHealthBenefitDTO.setSubjectToInNetworkDuductible((String) result.get("subjectToInNetworkDuductible"));
		planHealthBenefitDTO.setSubjectToOutNetworkDeductible((String) result.get("subjectToOutNetworkDeductible"));
		return planHealthBenefitDTO;
	}
	
	private void cleanUpBenefitAttributes(Map<String, Map<String, String>> tier1Benifits) {
		Set<String> retainValues = new HashSet<String>();
		retainValues.add("displayVal");
		retainValues.add("tileDisplayVal");
		Set<Map.Entry<String, Map<String, String>>> entrySet = tier1Benifits.entrySet();
		for (Map.Entry<String, Map<String, String>> entry : entrySet) {
			// remove every thing except the displayVal from the attributes
			entry.getValue().keySet().retainAll(retainValues);
		}
	}

	
	
	
	@Override
	public Map<String, Integer> formDrugsUsageMap(PdPreferencesDTO pdPreferencesDTO, int memberCount) {
		Map<String, Integer> drugUseMap = new HashMap<String, Integer>();
		drugUseMap.put("TOTAL", memberCount);
		drugUseMap.put(PlanDisplayConstants.LOW, 0);
		drugUseMap.put(PlanDisplayConstants.MEDIUM, 0);
		drugUseMap.put(PlanDisplayConstants.HIGH, 0);
		drugUseMap.put(PlanDisplayConstants.VERYHIGH, 0);
		
		if (pdPreferencesDTO != null) {
			if("CA".equalsIgnoreCase(stateCode)){
				if (PreferencesLevel.LEVEL1.equals(pdPreferencesDTO.getPrescriptionUse())) {
					drugUseMap.put(PlanDisplayConstants.LOW, memberCount);
				} else if (PreferencesLevel.LEVEL2.equals(pdPreferencesDTO.getPrescriptionUse())) {
					drugUseMap.put(PlanDisplayConstants.MEDIUM, 1);
					if(memberCount > 1){
						drugUseMap.put(PlanDisplayConstants.LOW, (memberCount-1));
					}
				} else if (PreferencesLevel.LEVEL3.equals(pdPreferencesDTO.getPrescriptionUse())) {
					drugUseMap.put(PlanDisplayConstants.HIGH, 1);
					if(memberCount > 1){
						drugUseMap.put(PlanDisplayConstants.MEDIUM, 1);
					}
					if(memberCount > 2){
						drugUseMap.put(PlanDisplayConstants.LOW, (memberCount-2));
					}
				} else if (PreferencesLevel.LEVEL4.equals(pdPreferencesDTO.getPrescriptionUse())) {
					drugUseMap.put(PlanDisplayConstants.VERYHIGH, 1);
					if(memberCount > 1){
						drugUseMap.put(PlanDisplayConstants.MEDIUM, 1);
					}
					if(memberCount > 2){
						drugUseMap.put(PlanDisplayConstants.LOW, (memberCount-2));
					}
				}
			}else{
				if (PreferencesLevel.LEVEL1.equals(pdPreferencesDTO.getPrescriptionUse())) {
					drugUseMap.put(PlanDisplayConstants.LOW, memberCount);
				} else if (PreferencesLevel.LEVEL2.equals(pdPreferencesDTO.getPrescriptionUse())) {
					drugUseMap.put(PlanDisplayConstants.MEDIUM, memberCount);
				} else if (PreferencesLevel.LEVEL3.equals(pdPreferencesDTO.getPrescriptionUse())) {
					drugUseMap.put(PlanDisplayConstants.HIGH, memberCount);
				} else if (PreferencesLevel.LEVEL4.equals(pdPreferencesDTO.getPrescriptionUse())) {
					drugUseMap.put(PlanDisplayConstants.VERYHIGH, memberCount);
				}
			}
			
		}

		return drugUseMap;
	}

	@Override
	public Map<String, Integer> formMedicalUsageMap(PdPreferencesDTO pdPreferencesDTO, int memberCount) {
		Map<String, Integer> medicalUseMap = new HashMap<String, Integer>();
		medicalUseMap.put("TOTAL", memberCount);
		medicalUseMap.put(PlanDisplayConstants.LOW, 0);
		medicalUseMap.put(PlanDisplayConstants.MEDIUM, 0);
		medicalUseMap.put(PlanDisplayConstants.HIGH, 0);
		medicalUseMap.put(PlanDisplayConstants.VERYHIGH, 0);
		
		if (pdPreferencesDTO != null) {
			if("CA".equalsIgnoreCase(stateCode)){
				if (PreferencesLevel.LEVEL1.equals(pdPreferencesDTO.getMedicalUse())) {
					medicalUseMap.put(PlanDisplayConstants.LOW, memberCount);
				} else if (PreferencesLevel.LEVEL2.equals(pdPreferencesDTO.getMedicalUse())) {
					medicalUseMap.put(PlanDisplayConstants.MEDIUM, 1);
					if(memberCount > 1){
						medicalUseMap.put(PlanDisplayConstants.LOW, (memberCount-1));
					}
				} else if (PreferencesLevel.LEVEL3.equals(pdPreferencesDTO.getMedicalUse())) {
					medicalUseMap.put(PlanDisplayConstants.HIGH, 1);
					if(memberCount > 1){
						medicalUseMap.put(PlanDisplayConstants.MEDIUM, 1);
					}
					if(memberCount > 2){
						medicalUseMap.put(PlanDisplayConstants.MEDIUM, 2);
					}
					if(memberCount > 3){
						medicalUseMap.put(PlanDisplayConstants.LOW, (memberCount-3));
					}
				} else if (PreferencesLevel.LEVEL4.equals(pdPreferencesDTO.getMedicalUse())) {
					medicalUseMap.put(PlanDisplayConstants.VERYHIGH, 1);
					if(memberCount > 1){
						medicalUseMap.put(PlanDisplayConstants.MEDIUM, 1);
					}
					if(memberCount > 2){
						medicalUseMap.put(PlanDisplayConstants.MEDIUM, 2);
					}
					if(memberCount > 3){
						medicalUseMap.put(PlanDisplayConstants.LOW, (memberCount-3));
					}
				}
			}else{
				if (PreferencesLevel.LEVEL1.equals(pdPreferencesDTO.getMedicalUse())) {
					medicalUseMap.put(PlanDisplayConstants.LOW, memberCount);
				} else if (PreferencesLevel.LEVEL2.equals(pdPreferencesDTO.getMedicalUse())) {
					medicalUseMap.put(PlanDisplayConstants.MEDIUM, memberCount);
				} else if (PreferencesLevel.LEVEL3.equals(pdPreferencesDTO.getMedicalUse())) {
					medicalUseMap.put(PlanDisplayConstants.HIGH, memberCount);
				} else if (PreferencesLevel.LEVEL4.equals(pdPreferencesDTO.getMedicalUse())) {
					medicalUseMap.put(PlanDisplayConstants.VERYHIGH, memberCount);
				}
			}
		}

		return medicalUseMap;
	}

	
	@Override
	public Map<String, Object> findPlanDetailsFromSolr(String planId) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		List<String> planIds = new ArrayList<>();
		planIds.add(planId);

		dataMap.put("planId", planId);
		Plan plan = null;
		try {
			plan = pdOrderItemService.getPlanInfoByPlanId(Long.parseLong(planId));
		} catch (Exception e1) {
			dataMap.put("planId", "No plan found in db for id " + planId);
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----No plan found in db for id---->" + planId, null, true);
		}
			
		if (plan != null) {
			SolrServer solrBenefitServer = null;
			SolrServer solrCostServer = null;
			SolrDocumentList solrDocumentList = null;
				
			if (PlanDisplayConstants.HEALTH.equals(plan.getInsuranceType())) {
				solrBenefitServer = solrPlanBenefitServer;
				solrCostServer = solrHealthCostServer;
			} else {
				solrBenefitServer = solrDentalBenefitServer;
				solrCostServer = solrDentalCostServer;
			}

			try {
				if (checkSolrServerAvailability(solrBenefitServer)) {
					Integer rowsToFetch = 100000;
					Integer start = 0;
					response = solrBenefitServer.query(getSolrQuery(
							getCommaSeperatedValues(planIds, "planId").toString(), "", null, rowsToFetch, start));
					solrDocumentList = response.getResults();
					dataMap.put("benefits", solrDocumentList);

					response = solrCostServer.query(getSolrQuery(getCommaSeperatedValues(planIds, "planId").toString(),
							"", null, rowsToFetch, start));
					solrDocumentList = response.getResults();
					dataMap.put("costs", solrDocumentList);
				} else {
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Solr Server not available---->", null, false);
				}
			} catch (Exception e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Failed to execute search---->", e, false);
			}
		}

		return dataMap;
	}
	
	
public IndividualPlan getHouseholdEnrolledPlanInfo(PdQuoteRequest pdQuoteRequest) {
		
		String householdId = pdQuoteRequest.getPdHouseholdDTO().getExternalId();
		
		IndividualPlan enrolledPlan = null;
		String enrollmentResponse = null;
		EnrollmentResponse response = null;
		EnrollmentRequest enrRequest = new EnrollmentRequest();
		List<String> householdList = new ArrayList<>();
		EnrollmentStatus enrollmentStatus[] = { EnrollmentStatus.CONFIRM, EnrollmentStatus.TERM, EnrollmentStatus.PENDING };
		
		householdList.add(householdId);
		enrRequest.setHouseHoldCaseIdList(householdList);
		enrRequest.setEnrollmentStatusList(Arrays.asList(enrollmentStatus));
		enrRequest.setPremiumDataRequired(false);

		try {
			if(LOGGER.isInfoEnabled()) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "Calling getEnrollmentDataByListOfHouseholdcaseids for household id " + householdId, null, false);
			}
			enrollmentResponse = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_ENROLLMENT_DATA_BY_LIST_OF_HOUSEHOLDCASEIDS,
							GhixConstants.USER_NAME_EXCHANGE, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,
							platformGson.toJson(enrRequest))
					.getBody();
			response = platformGson.fromJson(enrollmentResponse, EnrollmentResponse.class);
		} catch (Exception e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR,
					"<----Exception occured while calling Enrollment getEnrollmentdataByHouseholdCaseId api---->", e,
					false);
		}

		

		PdPreferencesDTO pdPreferencesDTO = pdQuoteRequest.getPdPreferencesDTO();
		List<ProviderBean> providers = new ArrayList<ProviderBean>();
		if(pdPreferencesDTO != null){
			String providerJson = pdPreferencesDTO.getProviders();			
			if(StringUtils.isNotBlank(providerJson)){
				providers = platformGson.fromJson(providerJson,new TypeToken<List<ProviderBean>>() {}.getType());
			}
		}
		List<DrugDTO> drugList = null;
		if(InsuranceType.HEALTH.equals(pdQuoteRequest.getInsuranceType()) && StringUtils.isNotBlank(pdPreferencesDTO.getPrescriptions())){
			drugList = planDisplayUtil.parsePrescriptionJson(pdPreferencesDTO.getPrescriptions());
		}
		
		if (response != null && response.getEnrollmentDataDtoListMap() != null) {
			List<EnrollmentDataDTO> enrollemntDataList = response.getEnrollmentDataDtoListMap().get(householdId);
			Map<Integer, EnrollmentDataDTO> enrolmentDataMap = new HashMap<>();

			if (enrollemntDataList != null) {
				List<Integer> planIds = new ArrayList<>();
				for (EnrollmentDataDTO enrlmentdata : enrollemntDataList) {
					if (enrlmentdata.getPlanType().equalsIgnoreCase(pdQuoteRequest.getInsuranceType().name())) {
						planIds.add(enrlmentdata.getPlanId());
						enrolmentDataMap.put(enrlmentdata.getPlanId(), enrlmentdata);
					} else {
						if(LOGGER.isInfoEnabled()) {
							PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO,
								"Existing enrollment Type not same as requested " + enrlmentdata.getPlanType(), null, false);
						}
					}
				}

				if (planIds.size() == 1) { // Dont add already enrolled plan info, if member enrolled in more than one health or dental plan
					PlanRequest planRequest = new PlanRequest();
					planRequest.setPlanIds(planIds);
					List<PlanResponse> planResponse = null;
					try {
						if(LOGGER.isInfoEnabled()) {
							PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO,"Calling PlanMetaDataHelper for plan Id " + planIds, null, false);
						}
						planResponse = planRestController.getPlanMetaDataHelper(planRequest);
					} catch (GIException e) {
							PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR,
									"Error while get Plan Info for already enrolled plans ", e, false);
					}
					for (PlanResponse planResp : planResponse) {
						enrolledPlan = PlanDisplayUtil.buildEnrolledPlanDetails(planResp, enrolmentDataMap.get(planResp.getPlanId()), providers, drugList);
						PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "Enrolled IndividualPlan added to List ",null, false);
						return enrolledPlan;
					}
				}

			}
		} else {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR,
					"Reponse from getEnrollmentdataByHouseholdCaseId " + enrollmentResponse, null, false);
		}
		return enrolledPlan;

	}
	
	
}
