package com.getinsured.plandisplay.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.ws.soap.SoapHeader;

import com.getinsured.hix.dto.enrollment.EnrollmentAutoRenewalDTO;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GEnrollment;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71Response;
import com.getinsured.hix.dto.plandisplay.ind71g.Ind71GRequest;
import com.getinsured.hix.model.PdHousehold;
import com.getinsured.hix.model.PdOrderItem;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest;
import com.getinsured.hix.webservice.plandisplay.autorenewal.ProductType;

public interface AutoRenewalService
{
	Long createOrderForAutoRenewal(Long householdId, String renewalPlanId,InsuranceType insurancetype, ProductType productType) throws GIException;
	Long createOrderForSpecialEnrollment(Long householdId, InsuranceType insurancetype, ProductType productType) throws GIException;
	EnrollmentResponse createEnrollmentForGivenOrder(Long householdId, AutoRenewalRequest autoRenewalRequest, SoapHeader header) throws GIException;
	PdHousehold saveEligibility(AutoRenewalRequest autoRenewalRequest);
	PdHousehold saveEligibility(Ind71GRequest ind71GRequest, IND71GEnrollment enrollment, Float remainingSubsidy, BigDecimal remainingStateSubsidy);
	IND71Response processIND71G(Ind71GRequest ind71GRequest, IND71Response response);
}