package com.getinsured.plandisplay.service.rules;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.plandisplay.util.PlanDisplayUtil;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;

public class BenefitCoverageCalculator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BenefitCoverageCalculator.class);
	private static Double GOOD_THRESHOLD_BASIS = 2.0;
	private static Double OK_THRESHOLD_BASIS = 5.0;
	private static Double ROOT = 0.5;
	private  Map<String, String> benefitsMapKey;
	
	public BenefitCoverageCalculator(Map<String, String> benefitsMapKey) {
		this.benefitsMapKey = benefitsMapKey;
	}
	
	/** Calculates the coverage for benefits. For each benefit the values can be GOOD, OK, POOR, NA
	 * @param Map<String, Map<Integer,Double>> phixPlanBenefits
	 * @return Map<Integer,Map<String,String>> planBenefitsCoverageOutput
	 */
	public Map<Integer,Map<String,String>> calculateBenefitCoverage(Map<String, Map<Integer,Double>> phixPlanBenefits) {
		//TODO: Check with AtulZ : below method call looks unused hence commented.
		//Map<String,Map<String,Double>> benefitThresholds = calculateThresholds(phixPlanBenefits);
		
		Map<Integer,Map<String,String>> planBenefitsCoverage = new HashMap<Integer,Map<String,String>>();
		for(Entry phixBenefit:phixPlanBenefits.entrySet()) {
			String benefitName = phixBenefit.getKey().toString();
			Map<Integer,Double> planBenefitCosts = (Map<Integer,Double>)phixBenefit.getValue();
			
			for(Entry planBenefit:planBenefitCosts.entrySet()) {
				Integer planId = (Integer)planBenefit.getKey();
				
				
				/**
				 * PATCH work: HIX-26869
				 * The different color levels (good, ok, poor) are 
				 * confusing. So for now we are saying they are
				 * either present or not.
				 */
				
				if(planBenefitsCoverage.containsKey(planId)) {
					planBenefitsCoverage.get(planId).put(benefitName, "GOOD");
				}
				else {
					planBenefitsCoverage.put(planId, new HashMap<String,String>());
					planBenefitsCoverage.get(planId).put(benefitName, "GOOD");
				}
				
				
				
				
				/**
				 * Following is original snippet 
				 * come back here once we figure out a meaningful
				 * way to convey the message.
				 * 
				
				
				if(benefitCost <=benefitGoodThreshold) {
					if(planBenefitsCoverage.containsKey(planId)) {
						planBenefitsCoverage.get(planId).put(benefitName, "GOOD");
					}
					else {
						planBenefitsCoverage.put(planId, new HashMap<String,String>());
						planBenefitsCoverage.get(planId).put(benefitName, "GOOD");
					}
				}
				else if(benefitCost > benefitGoodThreshold && benefitCost <=benefitOKThreshold) {
					if(planBenefitsCoverage.containsKey(planId)) {
						planBenefitsCoverage.get(planId).put(benefitName, "OK");
					}
					else {
						planBenefitsCoverage.put(planId, new HashMap<String,String>());
						planBenefitsCoverage.get(planId).put(benefitName, "OK");
					}
				}
				else if(benefitCost > benefitOKThreshold) {
					if(planBenefitsCoverage.containsKey(planId)) {
						planBenefitsCoverage.get(planId).put(benefitName, "POOR");
					}
					else {
						planBenefitsCoverage.put(planId, new HashMap<String,String>());
						planBenefitsCoverage.get(planId).put(benefitName, "POOR");
					}
				}
				*/
				
				
			}
		}
		Map<Integer,Map<String,String>> planBenefitsCoverageOutput = updateMissingBenefits(planBenefitsCoverage);
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "<----planBenefitsCoverageOutput---->"+planBenefitsCoverageOutput, null, true);
		
		return planBenefitsCoverageOutput;
	}
	
	/** Calculate the thresholds for GOOD, OK and POOR
	 * @param Map<String, Map<Integer,Double>> phixPlanBenefits
	 * @return Map<String,Map<String,Double>> benefitThresholds benefitThresholds
	 */
	/*private Map<String,Map<String,Double>> calculateThresholds(Map<String, Map<Integer,Double>> phixPlanBenefits) {
		// 1.First Calculate the Minimum cost for each benefit
		// 2. Logarithm of minimum benefit cost * GOOD_THRESHOLD_BASIS
		
		Map<String,Map<String,Double>> benefitThresholds = new HashMap<String,Map<String,Double>>();
		for(Entry phixBenefit:phixPlanBenefits.entrySet()) {
			String benefitName = phixBenefit.getKey().toString();
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----benefitName---->"+benefitName, null);
			Map<Integer,Double> benefitCosts = (Map<Integer,Double>)phixBenefit.getValue();
			Double minBenefitCost = (benefitCosts.isEmpty())? 1.0 : Collections.min(benefitCosts.values());
			Double thresholdGOOD = getGOODThreshold(minBenefitCost>0?minBenefitCost:1);
			Double thresholdOK = getOKThreshold(minBenefitCost>0?minBenefitCost:1);
			Map<String,Double> thresholdMap = new HashMap<String,Double>();
			thresholdMap.put("GOOD", thresholdGOOD);
			thresholdMap.put("OK", thresholdOK);
			LOGGER.debug("Thresholds for " + benefitName + ":" + thresholdMap);
			benefitThresholds.put(benefitName, thresholdMap);
		}
		
		return benefitThresholds;
	}*/
	
	/** 
	 * @deprecated This method is not in use.
	 * Get the threshold for the benefit to be OK
	 * @param Double minBenefitCost
	 * If the benefit is zero cost it is treated as 1 as we use log base 10
	 * @return Double thresholdOK
	 */
	@Deprecated
	private Double getOKThreshold(Double minBenefitCost) {
		Double logMinBenefitCost = Math.log10(minBenefitCost);
		Double multiplier = Math.pow(ROOT, logMinBenefitCost);
		Double newTarget = (multiplier * OK_THRESHOLD_BASIS) + 1;
		Double thresholdOK = newTarget * minBenefitCost;
		
		return thresholdOK;
	}
	
	/** 
	 * @deprecated This method is not in use.
	 * get the threshold for the benefit to be considered as good
	 * @param minBenefitCost
	 * If the benefit is zero cost it is treated as 1 as we use log base 10
	 * @return Double thresholdGOOD
	 */
	@Deprecated
	private Double getGOODThreshold(Double minBenefitCost) {
		Double logMinBenefitCost = Math.log10(minBenefitCost);
		Double multiplier = Math.pow(ROOT, logMinBenefitCost);
		Double newTarget = (multiplier * GOOD_THRESHOLD_BASIS) + 1;
		Double thresholdGOOD = newTarget * minBenefitCost;
		
		return thresholdGOOD;
	}
	
	/** 
	 * @deprecated This method is not in use.
	 * set benefit coverage to NA for the missing benefits in a plan 
	 * @param Map<Integer,Map<String,String>> planBenefitsCoverage
	 * @return Map<Integer,Map<String,String>> planBenefitsCoverage
	 */
	@Deprecated
	private Map<Integer,Map<String,String>> updateMissingBenefits(Map<Integer,Map<String,String>> planBenefitsCoverage) {
		for(Entry planBenefitCoverage:planBenefitsCoverage.entrySet()) {
			Map <String,String> planCoverage = (HashMap<String,String>)planBenefitCoverage.getValue();
			for(Entry phixBenefit:benefitsMapKey.entrySet()) {
				if(!planCoverage.containsKey(phixBenefit.getValue())) {
					planCoverage.put(phixBenefit.getValue().toString(), "NA");
				}
				
			}
		}
		return planBenefitsCoverage;
	}

}
