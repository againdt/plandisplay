package com.getinsured.plandisplay.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.ws.soap.SoapHeader;

import com.getinsured.hix.dto.enrollment.EnrollmentAutoRenewalDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentCustomGroupDTO;
import com.getinsured.hix.dto.plandisplay.PdHouseholdDTO;
import com.getinsured.hix.dto.plandisplay.PdPersonDTO;
import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GEnrollment;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GMember;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71Response;
import com.getinsured.hix.dto.plandisplay.ind71g.Ind71GRequest;
import com.getinsured.hix.dto.plandisplay.planavailability.PdOrderItemPersonResponse;
import com.getinsured.hix.dto.plandisplay.planavailability.PdOrderItemResponse;
import com.getinsured.hix.dto.plandisplay.planavailability.PdPersonRequest;
import com.getinsured.hix.dto.plandisplay.planavailability.PdRequestPlanAvailability;
import com.getinsured.hix.dto.plandisplay.planavailability.PdResponsePlanAvailability;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.model.IndividualPlan;
import com.getinsured.hix.model.PdHousehold;
import com.getinsured.hix.model.PdOrderItem;
import com.getinsured.hix.model.PdPerson;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.plandisplay.PdOrderResponse;
import com.getinsured.hix.model.plandisplay.PdQuoteRequest;
import com.getinsured.hix.model.plandisplay.PdSaveCartRequest;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ExchangeType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.Relationship;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ShoppingType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIException.GhixErrors;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Individual;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Members;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Members.Member;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalResponse;
import com.getinsured.hix.webservice.plandisplay.autorenewal.EnrollmentVal;
import com.getinsured.hix.webservice.plandisplay.autorenewal.ProductType;
import com.getinsured.hix.webservice.plandisplay.autorenewal.YesNoVal;
import com.getinsured.plandisplay.controller.PlanDisplaySvcHelper;
import com.getinsured.plandisplay.util.CreateDTOBean;
import com.getinsured.plandisplay.util.PlanDisplayConstants;
import com.getinsured.plandisplay.util.PlanDisplayUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * @author Krishna_Prasad
 * 
 */

@Service("autoRenewalService")
public class AutoRenewalServiceImpl implements AutoRenewalService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(AutoRenewalServiceImpl.class);

	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private PdHouseholdService pdHouseholdService;
	@Autowired private IndividualPlanService individualPlanService;
	@Autowired private PdOrderItemService pdOrderItemService;
	@Autowired private PdPersonService pdPersonService;
	@Autowired private PlanDisplaySvcHelper planDisplaySvcHelper;
	@Autowired private CreateDTOBean createDTOBean;	
	@Autowired private PlanDisplayUtil planDisplayUtil;
	@Autowired private Gson platformGson;
			
	@Override
	public Long createOrderForAutoRenewal(Long householdId, String renewalPlanId,InsuranceType insurancetype, ProductType productType) throws GIException
	{
		PdHousehold pdHousehold = pdHouseholdService.findById(householdId);	
		PdHouseholdDTO pdHouseholdDTO = new PdHouseholdDTO();
		try {
			createDTOBean.copyProperties(pdHouseholdDTO, pdHousehold);
		} catch (Exception e) {
			throw new GIException(GhixErrors.ERROR_WHILE_COPY);
		}
		List<PdPerson> pdPersonList = pdPersonService.getPersonListByHouseholdId(pdHousehold.getId());
		List<PdPersonDTO> pdPersonDTOList = new ArrayList<PdPersonDTO>();
		for(PdPerson pdPerson : pdPersonList) {
			PdPersonDTO pdPersonDTO = new PdPersonDTO();
			try {
				createDTOBean.copyProperties(pdPersonDTO, pdPerson);
			} catch (Exception e) {
				throw new GIException(GhixErrors.ERROR_WHILE_COPY);
			}
			pdPersonDTOList.add(pdPersonDTO);
		}
		List<IndividualPlan> plans = null;
		String zipCode = null;
		String countyCode = null;
		String isEligibleForCatastrophic = planDisplaySvcHelper.getCatastrophicEligibility(pdPersonDTOList);
		Map<String,String> subscriberData = individualPlanService.getSubscriberData(pdPersonDTOList, pdHouseholdDTO, null, null, insurancetype);
		if (subscriberData != null) {
			zipCode = subscriberData.get(GhixConstants.SUBSCRIBER_MEMBER_ZIP);
			countyCode = subscriberData.get(GhixConstants.SUBSCRIBER_MEMBER_COUNTY);
		}
		
		if(zipCode != null && countyCode != null){
			String coverageDate = DateUtil.dateToString(pdHouseholdDTO.getCoverageStartDate(), "yyyy-MM-dd");
			String csrValue = (pdHouseholdDTO.getCostSharing() != null && InsuranceType.HEALTH.equals(insurancetype)) ? pdHouseholdDTO.getCostSharing().toString() : "";
			
			long time = System.currentTimeMillis();
			String issuerPlanId = planDisplaySvcHelper.getCrossWalkIssuerPlanId(zipCode, countyCode, coverageDate, isEligibleForCatastrophic,renewalPlanId, csrValue);
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.WARN, "received getCrossWalkIssuerPlanId response for SSAP-Application " + pdHousehold.getApplicationId()+ " in " + (System.currentTimeMillis() - time) + " ms", null, false);
			
			String newCostSharing = issuerPlanId.substring((issuerPlanId.length() - 2), issuerPlanId.length());
			CostSharing costSharingChange = planDisplaySvcHelper.getCostSharing(newCostSharing);
			if(InsuranceType.HEALTH.equals(insurancetype) && !costSharingChange.equals(pdHouseholdDTO.getCostSharing())){
				if(householdId != null){
						pdHouseholdService.updateCostSharing(householdId, costSharingChange);
				}
				pdHouseholdDTO.setCostSharing(costSharingChange);
			}
			PdQuoteRequest pdQuoteRequest = new PdQuoteRequest();		
			pdQuoteRequest.setPdHouseholdDTO(pdHouseholdDTO);
			pdQuoteRequest.setPdPersonDTOList(pdPersonDTOList);
			pdQuoteRequest.setExchangeType(ExchangeType.ON);
			pdQuoteRequest.setShowCatastrophicPlan("Y".equals(isEligibleForCatastrophic) ? true : false);
			pdQuoteRequest.setInsuranceType(insurancetype);
			pdQuoteRequest.setMinimizePlanData(false);
			pdQuoteRequest.setIssuerId(null);
			pdQuoteRequest.setInitialCmsPlanId(issuerPlanId);
			
			String defaultPref = planDisplayUtil.getDefaultPref();
			PdPreferencesDTO defaultPrefDTO = platformGson.fromJson(defaultPref, new TypeToken<PdPreferencesDTO>() {}.getType());
			pdQuoteRequest.setPdPreferencesDTO(defaultPrefDTO);
			
			long startTime = System.currentTimeMillis();
			plans = individualPlanService.getPlans(pdQuoteRequest);
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.WARN, "received getPlans response for SSAP-Application " + pdHousehold.getApplicationId()+ " in " + (System.currentTimeMillis() - startTime) + " ms", null, false);
		}
		
		Long pdOrderItemId = null;
		List<Long> pdOrderItemIds = null;
		if (plans != null && !plans.isEmpty()) {
			IndividualPlan individualPlan = plans.get(0);
			PdSaveCartRequest pdSaveCartRequest = new PdSaveCartRequest();
			pdSaveCartRequest.setInsuranceType(insurancetype);
			pdSaveCartRequest.setHouseholdId(pdHouseholdDTO.getId());
			Float monthlySubsidy = new BigDecimal(Float.toString(individualPlan.getAptc())).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue();
			boolean hasChild = false;
			if(ProductType.A.equals(productType)){
				if(InsuranceType.HEALTH.equals(insurancetype)){
					calculateAutoRenewalSubsidy(pdPersonList,
												pdHousehold,
												individualPlan.getAptc(),
												individualPlan.getStateSubsidy(),
												hasChild);
				}else{
					monthlySubsidy = pdHouseholdDTO.getDentalSubsidy();
				}
			}

			pdSaveCartRequest.setMonthlySubsidy(monthlySubsidy);
			pdSaveCartRequest.setMonthlyStateSubsidy(individualPlan.getStateSubsidy());
			pdSaveCartRequest.setPlanId(individualPlan.getPlanId().longValue());
			pdSaveCartRequest.setPremiumAfterCredit(new BigDecimal(Float.toString(individualPlan.getPremiumAfterCredit())).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue());
			pdSaveCartRequest.setPremiumBeforeCredit(new BigDecimal(Float.toString(individualPlan.getPremiumBeforeCredit())).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue());
			pdSaveCartRequest.setPremiumData(individualPlan.getPlanDetailsByMember());
			pdSaveCartRequest.setSubsidyData(individualPlan.getContributionData());
			pdSaveCartRequest.setSsapApplicationId(pdHouseholdDTO.getApplicationId());
			if(InsuranceType.HEALTH.equals(insurancetype))
			{
				pdSaveCartRequest.setPlanPreferences(PlanDisplayUtil.getDefaultPlanPreferences());
			}
			pdOrderItemIds = pdOrderItemService.saveItem(pdSaveCartRequest);
			pdOrderItemId = pdOrderItemIds != null && !pdOrderItemIds.isEmpty() ? pdOrderItemIds.get(0) : null;
		} else {
			throw new GIException(GhixErrors.PLAN_NOT_RETURNED);
		}
		
		return pdOrderItemId;
	}

	private void calculateAutoRenewalSubsidy(List<PdPerson> pdPersonList,
											 PdHousehold pdHousehold,
											 float aptc,
											 BigDecimal stateSubsidy,
											 boolean hasChild) {
		Float maxSubsidy = pdHousehold.getMaxSubsidy();
		if(aptc != 0 && maxSubsidy != null && !Float.valueOf(0).equals(maxSubsidy)){
			
			float remainingAptc = BigDecimal.valueOf(maxSubsidy).subtract(BigDecimal.valueOf(aptc)).floatValue();
			pdHousehold.setHealthSubsidy(new BigDecimal(Float.toString(aptc)).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue());

			if (pdHousehold.getMaxStateSubsidy() != null && stateSubsidy != null) {
				BigDecimal remaininingStateSubsidy = pdHousehold.getMaxStateSubsidy().subtract(stateSubsidy);
				pdHousehold.setStateSubsidy(remaininingStateSubsidy);
			}

			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			if(!"CA".equalsIgnoreCase(stateCode)){
				pdHousehold.setDentalSubsidy(new BigDecimal(Float.toString(remainingAptc)).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue());
			}else{
				pdHousehold.setDentalSubsidy(0.0f);
			}
			pdHouseholdService.save(pdHousehold);
		}
	}
	
	@Override
	public Long createOrderForSpecialEnrollment(Long householdId, InsuranceType insurancetype, ProductType productType) throws GIException
	{
		PdHousehold pdHousehold = pdHouseholdService.findById(householdId);	
		List<PdPerson> pdPersonList = pdPersonService.getPersonListByHouseholdId(pdHousehold.getId());
		Long pdOrderItemId = null;
		if(pdPersonList != null && pdPersonList.size() > 0) {
			PdRequestPlanAvailability pdRequestPlanAvailability = new PdRequestPlanAvailability();
			pdRequestPlanAvailability.setCostSharing(pdHousehold.getCostSharing());
			pdRequestPlanAvailability.setCoverageDate(DateUtil.dateToString(pdHousehold.getCoverageStartDate(), "MM/dd/yyyy"));
			pdRequestPlanAvailability.setEnrollmentType(EnrollmentType.S);
			pdRequestPlanAvailability.setHouseholdGUID(Long.parseLong(pdHousehold.getExternalId()));
			Float subsidy = pdHousehold.getAptcForKeep() != null ? pdHousehold.getAptcForKeep() : pdHousehold.getMaxSubsidy();
			pdRequestPlanAvailability.setMaxStateSubsidy(pdHousehold.getMaxStateSubsidy());
			pdRequestPlanAvailability.setMaxSubsidy(subsidy);
			pdRequestPlanAvailability.setShoppingType(ShoppingType.INDIVIDUAL);
			pdRequestPlanAvailability.setHouseholdId(householdId);
			
			List<PdPersonRequest> pdPersonCRDTOList = new ArrayList<PdPersonRequest>();
			for(PdPerson pdPerson : pdPersonList){
				PdPersonRequest pdPersonRequest = new PdPersonRequest();
				pdPersonRequest.setId(pdPerson.getId());
				pdPersonRequest.setCountyCode(pdPerson.getCountyCode());
				pdPersonRequest.setDentalEnrollmentId(pdPerson.getDentalEnrollmentId());
				pdPersonRequest.setHealthEnrollmentId(pdPerson.getHealthEnrollmentId());
				pdPersonRequest.setDob(DateUtil.dateToString(pdPerson.getBirthDay(), "MM/dd/yyyy"));
				pdPersonRequest.setMemberGUID(pdPerson.getExternalId());
				pdPersonRequest.setRelationship(pdPerson.getRelationship());
				pdPersonRequest.setTobacco(pdPerson.getTobacco());
				pdPersonRequest.setZipCode(pdPerson.getZipCode());
				pdPersonRequest.setCatastrophicEligible(pdPerson.getCatastrophicEligible());
				
				pdPersonCRDTOList.add(pdPersonRequest);
			}
			
			pdRequestPlanAvailability.setPdPersonCRDTOList(pdPersonCRDTOList);
			PdResponsePlanAvailability pdResponsePlanAvailability = planDisplaySvcHelper.getPlanAvailability(pdRequestPlanAvailability);
			if(PlanDisplayConstants.SUCCESS.equals(pdResponsePlanAvailability.getResponse()) && PlanDisplayConstants.SUCCESS_CODE.equals(pdResponsePlanAvailability.getErrCode())){
				List<PdOrderItemResponse> pdOrderItemResponseList = pdResponsePlanAvailability.getPdOrderItemResponse();
				if(pdOrderItemResponseList != null && pdOrderItemResponseList.size() > 0){
					for(PdOrderItemResponse pdOrderItemResponse : pdOrderItemResponseList){
						if(insurancetype.equals(pdOrderItemResponse.getInsuranceType())){
							PdSaveCartRequest pdSaveCartRequest = new PdSaveCartRequest();
							pdSaveCartRequest.setInsuranceType(insurancetype);
							pdSaveCartRequest.setHouseholdId(householdId);
							Float monthlySubsidy = null;
							if(pdOrderItemResponse.getMonthlySubsidy() != null){
								monthlySubsidy = new BigDecimal(Float.toString(pdOrderItemResponse.getMonthlySubsidy())).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue();
							}
							if(ProductType.A.equals(productType)){
								if(InsuranceType.HEALTH.equals(insurancetype)){
									calculateAutoRenewalSubsidy(pdPersonList,
																pdHousehold,
																pdOrderItemResponse.getMonthlySubsidy(),
																pdOrderItemResponse.getMonthlyStateSubsidy(),
																false);
								}
							}
	
							pdSaveCartRequest.setMonthlySubsidy(monthlySubsidy);
							pdSaveCartRequest.setMonthlyStateSubsidy(pdOrderItemResponse.getMonthlyStateSubsidy());
							pdSaveCartRequest.setPlanId(pdOrderItemResponse.getPlanId().longValue());
							pdSaveCartRequest.setPremiumAfterCredit(new BigDecimal(Float.toString(pdOrderItemResponse.getPremiumAfterCredit())).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue());
							pdSaveCartRequest.setPremiumBeforeCredit(new BigDecimal(Float.toString(pdOrderItemResponse.getPremiumBeforeCredit())).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue());
							List<Map<String,String>> premiumDataList = new ArrayList<Map<String,String>>();
							for(PdOrderItemPersonResponse pdOrderItemPersonResponse : pdOrderItemResponse.getPdOrderItemPersonResponseList()){
								Map<String,String> premiumData = new HashMap<String,String>();
								premiumData.put("id", pdOrderItemPersonResponse.getMemberGUID());
								premiumData.put("premium", pdOrderItemPersonResponse.getPremium().toString());
								premiumData.put("region", pdOrderItemPersonResponse.getRegion().toString());
								if(pdOrderItemPersonResponse.getCoverageStartDate() != null){
									premiumData.put("personCovgDate", pdOrderItemPersonResponse.getCoverageStartDate());
								}							
								if(YorN.Y.equals(pdOrderItemPersonResponse.getSubscriberFlag())){
									if(InsuranceType.HEALTH.equals(insurancetype)){
										pdHousehold.setHealthSubscriberId(pdOrderItemPersonResponse.getMemberGUID()+"");
										pdHouseholdService.save(pdHousehold);
									}
									if(InsuranceType.DENTAL.equals(insurancetype)){
										pdHousehold.setDentalSubscriberId(pdOrderItemPersonResponse.getMemberGUID()+"");
										pdHouseholdService.save(pdHousehold);
									}
								}
								premiumDataList.add(premiumData);
							}
							pdSaveCartRequest.setPremiumData(premiumDataList);
							pdSaveCartRequest.setSsapApplicationId(pdHousehold.getApplicationId());
							if(InsuranceType.HEALTH.equals(insurancetype))
							{
								pdSaveCartRequest.setPlanPreferences(PlanDisplayUtil.getDefaultPlanPreferences());
							}
							List<Long> pdOrderItemIds = pdOrderItemService.saveItem(pdSaveCartRequest);
							pdOrderItemId = pdOrderItemIds != null && !pdOrderItemIds.isEmpty() ? pdOrderItemIds.get(0) : null;
						}
					}
				}else{
					throw new GIException(GhixErrors.PLAN_NOT_RETURNED);
				}
			}else{
				throw new GIException(Integer.valueOf(pdResponsePlanAvailability.getErrCode()), pdResponsePlanAvailability.getResponse(), "ERROR");
			}
		}
		
		return pdOrderItemId;
	}
	
	@Override
	public EnrollmentResponse createEnrollmentForGivenOrder(Long householdId, AutoRenewalRequest autoRenewalRequest, SoapHeader header) throws GIException
	{		
		String userName = planDisplayUtil.getCurrentUser(header);

		PdHousehold pdHousehold = pdHouseholdService.findById(householdId);
		List<PdOrderItem> pdOrderItems = pdOrderItemService.findByHouseholdId(householdId);
		EnrollmentAutoRenewalDTO enrollmentAutoRenewalDTO = formEnrollmentAutoRenewalDTO(pdHousehold, pdOrderItems,autoRenewalRequest, header);
		
		String enrollmentResp = null;
		try {
			enrollmentResp = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.CREATE_AUTO_RENEWAL_JSON, userName, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollmentAutoRenewalDTO)).getBody();
		}
		catch(Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Exception occured while calling Enrollment createautorenewal api---->", e, false);
			throw new GIException(GhixErrors.ERROR_WHILE_CALLING_CREATE_AUTO_RENEWAL);
		}
		
		
		return platformGson.fromJson(enrollmentResp, EnrollmentResponse.class);
	}

	public EnrollmentAutoRenewalDTO formEnrollmentAutoRenewalDTO(PdHousehold pdHousehold, List<PdOrderItem> pdOrderItems, AutoRenewalRequest autoRenewalRequest, SoapHeader header) {
		PdOrderResponse pdOrderResponse = new PdOrderResponse();
		planDisplaySvcHelper.createPdOrderResponseForEnrollment(pdHousehold, pdOrderItems, pdOrderResponse, false, null);				
		Map<String,String> globalIdAndCorrelationId = planDisplayUtil.getGlobalIdAndCorrelationId(header);
		
		EnrollmentAutoRenewalDTO enrollmentAutoRenewalDTO = new EnrollmentAutoRenewalDTO();
		enrollmentAutoRenewalDTO.setPdOrderResponse(pdOrderResponse);
		enrollmentAutoRenewalDTO.setOrderid(Long.toString(pdHousehold.getId()));
		enrollmentAutoRenewalDTO.setEnrollmentType(pdHousehold.getEnrollmentType().toString());
		enrollmentAutoRenewalDTO.setAutoRenewalRequest(autoRenewalRequest);
		enrollmentAutoRenewalDTO.setGlobalId(globalIdAndCorrelationId.get("globalId"));
		enrollmentAutoRenewalDTO.setCorrelationId(globalIdAndCorrelationId.get("correlationId"));
		return enrollmentAutoRenewalDTO;
	}
	
	@Override
	@Transactional
	public PdHousehold saveEligibility(AutoRenewalRequest autoRenewalRequest) {
		boolean isDental = ProductType.D.equals(autoRenewalRequest.getHousehold().getProductType());
		PdHousehold pdHousehold = saveHousehold(autoRenewalRequest,isDental);
		List<PdPerson> persons = savePerson(autoRenewalRequest, pdHousehold, isDental);
		
		if(autoRenewalRequest.getHousehold().getIndividual() != null && autoRenewalRequest.getHousehold().getIndividual().getAptcForKeep() != null)
		{
			com.getinsured.hix.webservice.plandisplay.individualinformation.ProductType productType = null;
			if(ProductType.H.equals(autoRenewalRequest.getHousehold().getProductType()))
			{
				productType = com.getinsured.hix.webservice.plandisplay.individualinformation.ProductType.H;
			}else if(ProductType.D.equals(autoRenewalRequest.getHousehold().getProductType()))
			{
				productType = com.getinsured.hix.webservice.plandisplay.individualinformation.ProductType.D;
			}
			
			long time = System.currentTimeMillis();
			boolean oldSubscriberPresent = pdHouseholdService.isOldSubscriberPresent(pdHousehold,persons,productType);
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.WARN, "received oldSubscriberPresent response for SSAP-Application " + autoRenewalRequest.getHousehold().getApplicationId()+ " in " + (System.currentTimeMillis() - time) + " ms", null, false);
			
			if(!oldSubscriberPresent)
			{
				BigDecimal monthlySubsidy = new BigDecimal(Float.toString(autoRenewalRequest.getHousehold().getIndividual().getAptc())).setScale(2,BigDecimal.ROUND_HALF_UP);
				pdHousehold.setAptcForKeep(monthlySubsidy.floatValue());
				pdHouseholdService.save(pdHousehold);
			}
			
		}
		return pdHousehold;
	}

	@Transactional
	public PdHousehold saveEligibility(Ind71GRequest ind71GRequest, IND71GEnrollment enrollment, Float remainingSubsidy, BigDecimal remainingStateSubsidy) {
		PdHousehold pdHousehold = saveHousehold(ind71GRequest, enrollment, remainingSubsidy, remainingStateSubsidy);
		savePerson(ind71GRequest, enrollment, pdHousehold);
		return pdHousehold;
	}
	
	private PdHousehold saveHousehold(AutoRenewalRequest autoRenewalRequest,boolean isDental) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Preparing the houlsehold object from autoRenewalRequest---->", null, false);
		if(autoRenewalRequest != null && autoRenewalRequest.getHousehold() != null && autoRenewalRequest.getHousehold().getIndividual() != null) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "Case Id: "+autoRenewalRequest.getHousehold().getIndividual().getHouseholdCaseId(), null, false);
		}
		PdHousehold pdHousehold = new PdHousehold();
		AutoRenewalRequest.Household household = autoRenewalRequest.getHousehold();
				
		pdHousehold.setApplicationId(household.getApplicationId());
		if(isDental)
		{
			pdHousehold.setHealthSubsidy(0f);
			if(household.getIndividual() != null && household.getIndividual().getAptc() != null){
				BigDecimal monthlySubsidy = new BigDecimal(Float.toString(household.getIndividual().getAptc())).setScale(2,BigDecimal.ROUND_HALF_UP);
				pdHousehold.setDentalSubsidy(monthlySubsidy.floatValue());
			}
		}
		if (household.getIndividual() != null) {
			setHouseholdForIndividual(household, pdHousehold);
		} 
		
		pdHousehold = pdHouseholdService.save(pdHousehold);
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Persisted pdhousehold object---->", null, false);
		if(autoRenewalRequest != null && autoRenewalRequest.getHousehold() != null && autoRenewalRequest.getHousehold().getIndividual() != null) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "Case Id: "+autoRenewalRequest.getHousehold().getIndividual().getHouseholdCaseId(), null, false);
		}
		return pdHousehold;
	}
	

	private PdHousehold saveHousehold(Ind71GRequest ind71GRequest, IND71GEnrollment enrollment, Float remainingSubsidy, BigDecimal remainingStateSubsidy) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Preparing the houlsehold object from ind71GRequest---->", null, false);
		if(ind71GRequest != null ) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "Case Id: "+ind71GRequest.getHouseholdCaseId(), null, false);
		}
		PdHousehold pdHousehold = new PdHousehold();
				
		pdHousehold.setApplicationId(new Long(ind71GRequest.getApplicationId()));
		setHouseholdForIndividual(ind71GRequest,enrollment, pdHousehold,remainingSubsidy, remainingStateSubsidy);
		
		pdHousehold = pdHouseholdService.save(pdHousehold);
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Persisted pdhousehold object---->", null, false);
		return pdHousehold;
	}
	
	private void setHouseholdForIndividual(Ind71GRequest ind71gRequest, IND71GEnrollment enrollment, PdHousehold pdHousehold, Float remainingSubsidy, BigDecimal remainingStateSubsidy) {

		//--- set CSR ---
		if(StringUtils.isEmpty(enrollment.getCsr()) || StringUtils.isBlank(enrollment.getCsr())){
			CostSharing csr = CostSharing.valueOf("CS1");
			pdHousehold.setCostSharing(csr);
		} else {
			CostSharing csr = CostSharing.valueOf(enrollment.getCsr());
			pdHousehold.setCostSharing(csr);
		}
		
		//--- set APTC ---
		if("Health".equals(enrollment.getInsuranceType()) && enrollment.getAptc() != null){
			BigDecimal monthlySubsidy = new BigDecimal(Float.toString(enrollment.getAptc())).setScale(2,BigDecimal.ROUND_HALF_UP);
			if(remainingSubsidy != null)
			{
				monthlySubsidy = monthlySubsidy.add(new BigDecimal(remainingSubsidy));
			}
			pdHousehold.setMaxSubsidy(monthlySubsidy.floatValue());
		}
		
		if("Dental".equals(enrollment.getInsuranceType()) && remainingSubsidy != null){
			pdHousehold.setMaxSubsidy(remainingSubsidy);
		}

		if("Health".equals(enrollment.getInsuranceType()) && enrollment.getStateSubsidy() != null){
			BigDecimal monthlyStateSubsidy = enrollment.getStateSubsidy().setScale(2, BigDecimal.ROUND_HALF_UP);
			if(remainingStateSubsidy != null){
				monthlyStateSubsidy = monthlyStateSubsidy.add(remainingStateSubsidy);
			}
			pdHousehold.setMaxStateSubsidy(monthlyStateSubsidy);
		}
		
		if("Dental".equals(enrollment.getInsuranceType()) && remainingStateSubsidy != null){
			pdHousehold.setMaxStateSubsidy(remainingStateSubsidy);
		}
		
		pdHousehold.setExternalId(ind71gRequest.getHouseholdCaseId()+"");
		pdHousehold.setCoverageStartDate(DateUtil.StringToDate(enrollment.getCoverageDate(),"MM/dd/yyyy"));
		if(ind71gRequest.getEnrollmentType()!=null && EnrollmentVal.S.toString().equals(ind71gRequest.getEnrollmentType())){
			pdHousehold.setEnrollmentType(EnrollmentType.S);
			pdHousehold.setAutoRenewal(YorN.N);
		}else{
			pdHousehold.setEnrollmentType(EnrollmentType.A);
			pdHousehold.setAutoRenewal(YorN.Y);
		}
		pdHousehold.setShoppingType(ShoppingType.INDIVIDUAL);
	
		
	}

	private void setHouseholdForIndividual(Household household, PdHousehold pdHousehold) {
		Individual individual = household.getIndividual();
		if(StringUtils.isEmpty(individual.getCsr()) || StringUtils.isBlank(individual.getCsr())){
			CostSharing csr = CostSharing.valueOf("CS1");
			pdHousehold.setCostSharing(csr);
		} else {
			CostSharing csr = CostSharing.valueOf(individual.getCsr());
			pdHousehold.setCostSharing(csr);
		}
		if(individual.getAptc() != null){
			BigDecimal monthlySubsidy = new BigDecimal(Float.toString(individual.getAptc())).setScale(2,BigDecimal.ROUND_HALF_UP);
			pdHousehold.setMaxSubsidy(monthlySubsidy.floatValue());
		}
		if(individual.getAptcForKeep() != null){
			BigDecimal monthlySubsidy = new BigDecimal(Float.toString(individual.getAptcForKeep())).setScale(2,BigDecimal.ROUND_HALF_UP);
			pdHousehold.setAptcForKeep(monthlySubsidy.floatValue());
		}
		if(individual.getStateSubsidy() != null){
			pdHousehold.setMaxStateSubsidy(individual.getStateSubsidy());
		}
		pdHousehold.setExternalId(individual.getHouseholdCaseId()+"");
		pdHousehold.setCoverageStartDate(DateUtil.StringToDate(individual.getCoverageStartDate(),"MM/dd/yyyy"));
		if(individual.getEnrollmentType()!=null && EnrollmentVal.S.equals(individual.getEnrollmentType())){
			pdHousehold.setEnrollmentType(EnrollmentType.S);
			pdHousehold.setAutoRenewal(YorN.N);
		}else{
			pdHousehold.setEnrollmentType(EnrollmentType.A);
			pdHousehold.setAutoRenewal(YorN.Y);
		}
		pdHousehold.setShoppingType(ShoppingType.INDIVIDUAL);
	}

	
	private List<PdPerson> savePerson(Ind71GRequest ind71gRequest, IND71GEnrollment enrollment, PdHousehold pdHousehold) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Forming of PdPerson object---->", null, false);
		if(ind71gRequest != null ) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "Case Id: "+ind71gRequest.getHouseholdCaseId(), null, false);
		}
		List<PdPerson> persons = new ArrayList<PdPerson>();
		for (IND71GMember member : enrollment.getMembers()) {
			if(StringUtils.isEmpty(member.getDisenrollMemberFlag()) || "N".equalsIgnoreCase(member.getDisenrollMemberFlag()))
			{
				PdPerson pdPerson = formPersonObject(member, pdHousehold, ("Dental".equals(enrollment.getInsuranceType())?true:false), enrollment);
				persons.add(pdPerson);
			}
		}
		pdPersonService.savePerson(persons);
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Persisted PdPerson object---->", null, false);
		return persons;
	}
	
	private List<PdPerson> savePerson(AutoRenewalRequest autoRenewalRequest, PdHousehold pdHousehold, boolean isDental) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Forming of PdPerson object---->", null, false);
		if(autoRenewalRequest != null && autoRenewalRequest.getHousehold() != null && autoRenewalRequest.getHousehold().getIndividual() != null) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "Case Id: "+autoRenewalRequest.getHousehold().getIndividual().getHouseholdCaseId(), null, false);
		}
		List<PdPerson> persons = new ArrayList<PdPerson>();
		ProductType productType = autoRenewalRequest.getHousehold().getProductType();
		Members members = autoRenewalRequest.getHousehold().getMembers();
		for (Member member : members.getMember()) {
			PdPerson pdPerson = formPersonObject(member, pdHousehold, isDental, productType);
			persons.add(pdPerson);
		}
		pdPersonService.savePerson(persons);
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Persisted PdPerson object---->", null, false);
		if(autoRenewalRequest != null && autoRenewalRequest.getHousehold() != null && autoRenewalRequest.getHousehold().getIndividual() != null) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "Case Id: "+autoRenewalRequest.getHousehold().getIndividual().getHouseholdCaseId(), null, false);
		}
		return persons;
	}
	

	private PdPerson formPersonObject(IND71GMember member, PdHousehold pdHousehold,boolean isDental, IND71GEnrollment enrollment) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside formPersonObject : Forming PdPerson with memberId---->"+member.getMemberId(), null, false);

		PdPerson pdPerson = new PdPerson();
		pdPerson.setPdHousehold(pdHousehold);
		pdPerson.setExternalId(String.valueOf(member.getMemberId()));
		pdPerson.setFirstName(member.getFirstName());
		pdPerson.setLastName(member.getLastName());

		if (member.getNewPersonFlag() != null)
		{
			YorN newPersonFLAG = YorN.valueOf(member.getNewPersonFlag().toString());
			pdPerson.setNewPersonFlag(newPersonFLAG);
		}
		String strRelationship = PlanDisplayConstants.relationships.get(member.getResponsiblePersonRelationship());
		
		Relationship relationship = Relationship.valueOf(strRelationship.toUpperCase());
		pdPerson.setRelationship(relationship);	
		pdPerson.setZipCode(member.getHomeZip());
		pdPerson.setCountyCode(member.getCountyCode());		
		pdPerson.setBirthDay(DateUtil.StringToDate(member.getDob(),"MM/dd/yyyy"));
		if(isDental)
		{
			pdPerson.setSeekCoverage(YorN.Y);
			pdPerson.setDentalEnrollmentId(Long.parseLong(enrollment.getEnrollmentId()));
		}
		else
		{
			pdPerson.setHealthEnrollmentId(Long.parseLong(enrollment.getEnrollmentId()));
		}
		if(YesNoVal.Y.toString().equals(member.getCatastrophicEligible())) {
			pdPerson.setCatastrophicEligible(YorN.Y);
		} else {
			pdPerson.setCatastrophicEligible(YorN.N);
		}
		
		if (member.getTobacco() != null) {
			if(YesNoVal.Y.toString().equals(member.getTobacco())) {
				pdPerson.setTobacco(YorN.Y);
			} else {
				pdPerson.setTobacco(YorN.N);
			}
		}
		
		/*if(member.getExistingMedicalEnrollmentID() != null && !("".equals(member.getExistingMedicalEnrollmentID()))){
			pdPerson.setHealthEnrollmentId(Long.parseLong(member.getExistingMedicalEnrollmentID()));
		}
		
		if(member.getExistingSADPEnrollmentID() != null && !("".equals(member.getExistingSADPEnrollmentID()))){
			pdPerson.setDentalEnrollmentId(Long.parseLong(member.getExistingSADPEnrollmentID()));
		}*/
		
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----formPersonObject : Forming PdPerson with memberId---->"+member.getMemberId() +" Complete", null, false);
		return pdPerson;
	}
	private PdPerson formPersonObject(Member member, PdHousehold pdHousehold,boolean isDental, ProductType productType) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside formPersonObject : Forming PdPerson with memberId---->"+member.getMemberId(), null, false);

		PdPerson pdPerson = new PdPerson();
		pdPerson.setPdHousehold(pdHousehold);
		pdPerson.setExternalId(String.valueOf(member.getMemberId()));
		pdPerson.setFirstName(member.getFirstName());
		pdPerson.setLastName(member.getLastName());

		if (member.getNewPersonFLAG() != null)
		{
			YorN newPersonFLAG = YorN.valueOf(member.getNewPersonFLAG().toString());
			pdPerson.setNewPersonFlag(newPersonFLAG);
		}
		String strRelationship = PlanDisplayConstants.relationships.get(member.getResponsiblePersonRelationship());
		
		Relationship relationship = Relationship.valueOf(strRelationship.toUpperCase());
		pdPerson.setRelationship(relationship);	
		pdPerson.setZipCode(member.getHomeZip());
		pdPerson.setCountyCode(member.getCountyCode());		
		pdPerson.setBirthDay(DateUtil.StringToDate(member.getDob(),"MM/dd/yyyy"));
		if(isDental)
		{
			pdPerson.setSeekCoverage(YorN.Y);
		}
		if(ProductType.A.equals(productType)){
			YorN seekCoverage = planDisplayUtil.setSeekCoverage(pdHousehold.getCoverageStartDate(), member.getDob());
			pdPerson.setSeekCoverage(seekCoverage);
		}
		if(YesNoVal.Y.equals(member.getCatastrophicEligible())) {
			pdPerson.setCatastrophicEligible(YorN.Y);
		} else {
			pdPerson.setCatastrophicEligible(YorN.N);
		}
		
		if (member.getTobacco() != null) {
			if(YesNoVal.Y.equals(member.getTobacco())) {
				pdPerson.setTobacco(YorN.Y);
			} else {
				pdPerson.setTobacco(YorN.N);
			}
		}
		
		if(member.getExistingMedicalEnrollmentID() != null && !("".equals(member.getExistingMedicalEnrollmentID()))){
			pdPerson.setHealthEnrollmentId(Long.parseLong(member.getExistingMedicalEnrollmentID()));
		}
		
		if(member.getExistingSADPEnrollmentID() != null && !("".equals(member.getExistingSADPEnrollmentID()))){
			pdPerson.setDentalEnrollmentId(Long.parseLong(member.getExistingSADPEnrollmentID()));
		}
		
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----formPersonObject : Forming PdPerson with memberId---->"+member.getMemberId() +" Complete", null, false);
		return pdPerson;
	}
	

	public IND71Response processIND71G(Ind71GRequest ind71GRequest, IND71Response response) {
		
		Float remainingSubsidy = null;
		BigDecimal remainingStateSubsidy = null;
		IND71GEnrollment dentalEnrollment = null;
		List<PdOrderItem> orderItems = new ArrayList<PdOrderItem>();
		Map<Long,String> orderItemToEnrollment = new HashMap<Long,String>();
		boolean hasFailures = false;
		for(IND71GEnrollment enrollment : ind71GRequest.getEnrollments())
		{
			try {
				if ("Health".equals(enrollment.getInsuranceType()))
				{
					PdHousehold household = saveEligibility(ind71GRequest, enrollment, remainingSubsidy, remainingStateSubsidy);
					Long pdOrderItemId = createOrderForSpecialEnrollment(household.getId(),InsuranceType.HEALTH, ProductType.A);
					
					if(pdOrderItemId != null) {
						PdOrderItem orderItem = pdOrderItemService.findById(pdOrderItemId);
						BigDecimal remSubsidyBigDecimal = null;
						if(household.getMaxSubsidy() != null && orderItem.getMonthlySubsidy() != null)
						{
							remSubsidyBigDecimal = new BigDecimal(household.getMaxSubsidy()).subtract(new BigDecimal(orderItem.getMonthlySubsidy()).setScale(2,BigDecimal.ROUND_HALF_UP));
							remainingSubsidy = remSubsidyBigDecimal.floatValue();
						}
						if(household.getMaxStateSubsidy() != null && orderItem.getMonthlyStateSubsidy() != null){
							remainingStateSubsidy = household.getMaxStateSubsidy().subtract(orderItem.getMonthlyStateSubsidy()).setScale(2, BigDecimal.ROUND_HALF_UP);
						}
						orderItem.setPdHousehold(household);
						orderItems.add(orderItem);
						orderItemToEnrollment.put(pdOrderItemId, enrollment.getEnrollmentId());
					}
				}
				else
				{
					dentalEnrollment = enrollment;
				}
			} catch (GIException e) {
				response.setResponseCode(Integer.toString(e.getErrorCode()));
				response.setResponseDescription("Error while processing Enrollment. EnrollmentId : "+enrollment.getEnrollmentId() + " Error Details : "+e.getErrorMsg() );
				hasFailures = true;
				break;
			}catch (Exception e) {
				response.setResponseCode("501");
				response.setResponseDescription("Error while processing Enrollment. EnrollmentId : "+enrollment.getEnrollmentId() );
				hasFailures = true;
				break;
			}
		}

		if(dentalEnrollment != null && hasFailures == false)
		{
			try 
			{
				PdHousehold household = saveEligibility(ind71GRequest, dentalEnrollment, remainingSubsidy, remainingStateSubsidy);
				Long pdOrderItemId = createOrderForSpecialEnrollment(household.getId(),InsuranceType.DENTAL, ProductType.A);
				if(pdOrderItemId != null) {
					PdOrderItem orderItem = pdOrderItemService.findById(pdOrderItemId);
					orderItem.setPdHousehold(household);
					orderItems.add(orderItem);
					orderItemToEnrollment.put(pdOrderItemId, dentalEnrollment.getEnrollmentId());
				}
			}
			catch (GIException e) {
				response.setResponseCode(Integer.toString(e.getErrorCode()));
				response.setResponseDescription("Error while processing Enrollment. EnrollmentId : "+dentalEnrollment.getEnrollmentId() + " Error Details : "+e.getErrorMsg() );
				hasFailures = true;
			}catch (Exception e) {
				response.setResponseCode("501");
				response.setResponseDescription("Error while processing Enrollment. EnrollmentId : "+dentalEnrollment.getEnrollmentId() );
				hasFailures = true;
			}
		}

		if (hasFailures == false)
		{
			EnrollmentResponse enrollmentResponse = generateIND71GResponse(orderItems, orderItemToEnrollment, ind71GRequest);
			if("SUCCESS".equals(enrollmentResponse.getStatus()))
			{
				response.setResponseCode("200");
				response.setResponseDescription("Enrollments Created Successfully.");
			}
			else if("FAILURE".equals(enrollmentResponse.getStatus()))
			{
				response.setResponseCode(Integer.toString(enrollmentResponse.getErrCode()));
				response.setResponseDescription(enrollmentResponse.getErrMsg());
			}
		}
		//String successReponse = platformGson.toJson(response);
		return response;
	}

	private EnrollmentResponse generateIND71GResponse(List<PdOrderItem> orderItems, Map<Long, String> orderItemToEnrollment, Ind71GRequest ind71GRequest) {

		String enrollmentResp = "";
		EnrollmentResponse response  = new EnrollmentResponse();
		try {
			String userName = planDisplayUtil.getCurrentUser(null);
			PdOrderResponse pdOrderResponse = new PdOrderResponse();
			if(orderItems!=null && orderItems.size() > 0) {
				planDisplaySvcHelper.createPdOrderResponseForEnrollment(null, orderItems, pdOrderResponse, true, orderItemToEnrollment);
			}
			planDisplaySvcHelper.addDisenrolledGroups(orderItems, pdOrderResponse, ind71GRequest);
			EnrollmentCustomGroupDTO enrollmentCustomGroupDTO = new EnrollmentCustomGroupDTO();
			enrollmentCustomGroupDTO.setPdOrderResponse(pdOrderResponse);
			enrollmentCustomGroupDTO.setInd71GRequest(ind71GRequest);
			
			enrollmentResp = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.ENROLLMENT_CUSTOM_GROUPING, userName, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollmentCustomGroupDTO)).getBody();
			if(!StringUtils.isEmpty(enrollmentResp))
			{
				response = platformGson.fromJson(enrollmentResp, EnrollmentResponse.class);
			}
		}
		catch(Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Exception occured while calling Enrollment createautorenewal api---->", e, false);
			response.setStatus("FAILURE");
			response.setErrCode(126);
			response.setErrMsg("Error while calling Enrollment Api"+e.getMessage());
		}
		
		
		return response;
	}
}