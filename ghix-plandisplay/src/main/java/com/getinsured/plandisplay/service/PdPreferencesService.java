package com.getinsured.plandisplay.service;

import com.getinsured.hix.model.PdPreferences;

public interface PdPreferencesService {
	PdPreferences save(PdPreferences pdPreferences);
	PdPreferences findPreferencesByPdHouseholdId(Long pdHouseholdId);
	PdPreferences findPreferencesByEligLeadId(Long eligLeadId);
	void savePdPreferencesByHouseholdId(PdPreferences pdPreferences, Long pdHouseholdId);
	void savePdPreferencesByEligLeadId(Long eligLeadId);	
}
