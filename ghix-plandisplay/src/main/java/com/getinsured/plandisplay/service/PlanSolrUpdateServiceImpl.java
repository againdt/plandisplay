package com.getinsured.plandisplay.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.response.SolrPingResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.plandisplay.PlanDentalBenefitDTO;
import com.getinsured.hix.dto.plandisplay.PlanDentalCostDTO;
import com.getinsured.hix.dto.plandisplay.PlanHealthBenefitDTO;
import com.getinsured.hix.dto.plandisplay.PlanHealthCostDTO;
import com.getinsured.hix.model.plandisplay.PdResponse;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.plandisplay.controller.PlanDisplaySvcHelper;
import com.getinsured.plandisplay.util.PlanDisplayConstants;
import com.getinsured.plandisplay.util.PlanDisplayUtil;
import com.getinsured.timeshift.TimeShifterUtil;

@EnableAsync
@Service("planSolrUpdateService")
public class PlanSolrUpdateServiceImpl implements PlanSolrUpdateService{
	private static final Logger LOGGER = LoggerFactory.getLogger(PlanSolrUpdateServiceImpl.class);
	
	@Autowired private PlanDisplayUtil planDisplayUtil;
	@Autowired private PlanDisplaySvcHelper planDisplaySvcHelper; 
	
	@Autowired
	@Qualifier("solrPlanBenefitServer")
	private SolrServer solrPlanBenefitServer;
	
	
	@Autowired
	@Qualifier("solrHealthCostServer")
	private SolrServer solrHealthCostServer;
	
	@Autowired
	@Qualifier("solrDentalBenefitServer")
	private SolrServer solrDentalBenefitServer;
	
	
	@Autowired
	@Qualifier("solrDentalCostServer")
	private SolrServer solrDentalCostServer;
	
	
	private DateFormat simpleDf = new SimpleDateFormat("yyyy-MM-dd");
	private final static String INSURANCE_TYPE_HEALTH="HEALTH";
	private final static String INSURANCE_TYPE_DENTAL="DENTAL";
	
	@Async
	@Override
	public void saveMissingSolrDocs( List<Integer> planIds , String insuranceType){
		saveMissingSolrCoreLogic(planIds, insuranceType);
	}

	private List<SolrInputDocument> createPlanHealthCostSolrDocs(List<PlanHealthCostDTO> list ){
		List<SolrInputDocument> docList = new ArrayList<SolrInputDocument>();
		for( PlanHealthCostDTO obj : list){
			SolrInputDocument solrDoc = new SolrInputDocument();
			
			solrDoc.addField("name", obj.getName());
			solrDoc.addField("id", obj.getId());
			solrDoc.addField("planHealthId",obj.getPlanHealthId());
			solrDoc.addField("planId",obj.getPlanId());
			solrDoc.addField("inNetWorkInd", obj.getInNetWorkInd());
			solrDoc.addField("inNetworkFly", obj.getInNetWorkFly());
			solrDoc.addField("inNetworkTier2Ind", obj.getInNetworkTier2Ind());			
			solrDoc.addField("inNetworkTier2Fly", obj.getInNetworkTier2Fly());   
			solrDoc.addField("outNetworkInd", obj.getOutNetworkInd());					 		
			solrDoc.addField("outNetworkFly", obj.getOutNetworkFly());						
			solrDoc.addField("combinedInOutNetworkInd", obj.getCombinedInOutNetworkInd());					
			solrDoc.addField("combinedInOutNetworkFly", obj.getCombinedInOutNetworkFly());			
			solrDoc.addField("combDefCoinsNetworkTier1", obj.getCombDefCoinsNetworkTier1());    
			solrDoc.addField("combDefCoinsNetworkTier2", obj.getCombDefCoinsNetworkTier2());
			solrDoc.addField("limitExcepDisplay", obj.getLimitExcepDisplay());
			docList.add(solrDoc);
		}
		return docList;
	}
	
	private List<SolrInputDocument> createPlanDentalCostSolrDocs(List<PlanDentalCostDTO> list ){
		List<SolrInputDocument> docList = new ArrayList<SolrInputDocument>();
		for( PlanDentalCostDTO obj : list){
			SolrInputDocument solrDoc = new SolrInputDocument();
			
			solrDoc.addField("name", obj.getName());
			solrDoc.addField("id", obj.getId());
			solrDoc.addField("planDentalId",obj.getPlanDentalId());
			solrDoc.addField("planId",obj.getPlanId());
			solrDoc.addField("inNetWorkInd", obj.getInNetWorkInd());
			solrDoc.addField("inNetworkFly", obj.getInNetWorkFly());
			solrDoc.addField("inNetworkTier2Ind", obj.getInNetworkTier2Ind());			
			solrDoc.addField("inNetworkTier2Fly", obj.getInNetworkTier2Fly());   
			solrDoc.addField("outNetworkInd", obj.getOutNetworkInd());					 		
			solrDoc.addField("outNetworkFly", obj.getOutNetworkFly());						
			solrDoc.addField("combinedInOutNetworkInd", obj.getCombinedInOutNetworkInd());					
			solrDoc.addField("combinedInOutNetworkFly", obj.getCombinedInOutNetworkFly());			
			solrDoc.addField("combDefCoinsNetworkTier1", obj.getCombDefCoinsNetworkTier1());    
			solrDoc.addField("combDefCoinsNetworkTier2", obj.getCombDefCoinsNetworkTier2());
			solrDoc.addField("limitExcepDisplay", obj.getLimitExcepDisplay());
			docList.add(solrDoc);
		}
		return docList;
	}
	
	private List<SolrInputDocument> createPlanHealthBenefitSolrDocs(List<PlanHealthBenefitDTO> list ){
		List<SolrInputDocument> docList = new ArrayList<SolrInputDocument>();
		for( PlanHealthBenefitDTO obj : list){
			SolrInputDocument solrDoc = new SolrInputDocument();
			solrDoc.addField("name", obj.getName());
			solrDoc.addField("id", obj.getId());
			solrDoc.addField("planHealthId",obj.getPlanHealthId());
			solrDoc.addField("planId",obj.getPlanId());
			solrDoc.addField("combinedInAndOutNetworkAttribute", obj.getCombinedInAndOutNetworkAttribute());
			solrDoc.addField("combinedInAndOutOfNetwork", obj.getCombinedInAndOutOfNetwork());
			solrDoc.addField("combinedInOutNetworkFamily", obj.getCombinedInOutNetworkFamily());			
			solrDoc.addField("combinedInOutNetworkIndividual", obj.getCombinedInOutNetworkIndividual());   
			try {
				solrDoc.addField("effEndDate", simpleDf.parse(obj.getEffectiveEndDate()));					 		
				solrDoc.addField("effStartDate", simpleDf.parse(obj.getEffectiveStartDate()));
			} catch (ParseException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			}						
			solrDoc.addField("ehbVarianceReason", obj.getEhbVarianceReason());					
			solrDoc.addField("excludedFromInNetworkMoop", obj.getExcludedFromInNetworkMoop());			
			solrDoc.addField("excludedFromOutOfNetworkMoop", obj.getExcludedFromOutOfNetworkMoop());     
			solrDoc.addField("explanation", obj.getExplanation());						
			solrDoc.addField("inNetworkFamily", obj.getInNetworkFamily());					
			solrDoc.addField("inNetworkIndividual", obj.getInNetworkIndividual());				
			solrDoc.addField("inNetworkTierTwoFamily", obj.getInNetworkTierTwoFamily());				
			solrDoc.addField("inNetworkTierTwoIndividual", obj.getInNetworkTierTwoIndividual());			
			solrDoc.addField("isCovered", obj.getIsCovered());					 		
			solrDoc.addField("isEHB", obj.getIsEHB());					 			
			solrDoc.addField("isStateMandate", obj.getIsStateMandate());						
			solrDoc.addField("limitExcepDisplay", obj.getLimitExcepDisplay());					
			solrDoc.addField("minStay", obj.getMinStay());				 			
			solrDoc.addField("networkExceptions", obj.getNetworkExceptions());					
			solrDoc.addField("networkLimitation", obj.getNetworkLimitation());					
			solrDoc.addField("networkLimitationAttribute", obj.getNetworkLimitationAttribute());       
			solrDoc.addField("networkT1CoinsurAttr", obj.getNetworkT1CoinsurAttr());				
			solrDoc.addField("networkT1CoinsurVal", obj.getNetworkT1CoinsurVal());				
			solrDoc.addField("networkT1CopayAttr", obj.getNetworkT1CopayAttr());			 		
			solrDoc.addField("networkT1CopayVal", obj.getNetworkT1CopayVal());					
			solrDoc.addField("networkT1display", obj.getNetworkT1display());					
			solrDoc.addField("networkT1TileDisplay", obj.getNetworkT1TileDisplay());				
			solrDoc.addField("networkT2CoinsurAttr", obj.getNetworkT2CoinsurAttr());			
			solrDoc.addField("networkT2CoinsurVal", obj.getNetworkT2CoinsurVal());			
			solrDoc.addField("networkT2CopayAttr", obj.getNetworkT2CopayAttr());				
			solrDoc.addField("networkT2CopayVal", obj.getNetworkT2CopayVal());				
			solrDoc.addField("networkT2display", obj.getNetworkT2display());				
			solrDoc.addField("outOfNetworkCoinsurAttr", obj.getOutOfNetworkCoinsurAttr());		
			solrDoc.addField("outOfNetworkCoinsurVal", obj.getOutOfNetworkCoinsurVal());			
			solrDoc.addField("outOfNetworkCopayAttr", obj.getOutOfNetworkCopayAttr());			
			solrDoc.addField("outOfNetworkCopayVal", obj.getOutOfNetworkCopayVal());			
			solrDoc.addField("outOfNetworkDisplay", obj.getOutOfNetworkDisplay());			
			solrDoc.addField("outOfNetworkFamily", obj.getOutOfNetworkFamily());				
			solrDoc.addField("outOfNetworkIndividual", obj.getOutOfNetworkIndividual());			
			solrDoc.addField("serviceLimit", obj.getServiceLimit());					
			solrDoc.addField("subjectToInNetworkDuductible", obj.getSubjectToInNetworkDuductible());       
			solrDoc.addField("subjectToOutNetworkDeductible", obj.getSubjectToOutNetworkDeductible());
			docList.add(solrDoc);
		}
		
		return docList;
	}
	
	
	private List<SolrInputDocument> createPlanDentalBenefitSolrDocs(List<PlanDentalBenefitDTO> list ) {
		List<SolrInputDocument> docList = new ArrayList<SolrInputDocument>();
		for( PlanDentalBenefitDTO obj : list) {
			SolrInputDocument solrDoc = new SolrInputDocument();
			solrDoc.addField("name", obj.getName());
			solrDoc.addField("id", obj.getId());
			solrDoc.addField("planDentalId",obj.getPlanDentalId());
			solrDoc.addField("planId",obj.getPlanId());
			try {
				solrDoc.addField("effEndDate", simpleDf.parse(obj.getEffectiveEndDate()));					 		
				solrDoc.addField("effStartDate", simpleDf.parse(obj.getEffectiveStartDate()));
			} catch (ParseException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			}						
			solrDoc.addField("excludedFromInNetworkMoop", obj.getExcludedFromInNetworkMoop());			
			solrDoc.addField("excludedFromOutOfNetworkMoop", obj.getExcludedFromOutOfNetworkMoop());     
			solrDoc.addField("explanation", obj.getExplanation());						
			solrDoc.addField("isCovered", obj.getIsCovered());					 		
			solrDoc.addField("isEHB", obj.getIsEHB());					 			
			solrDoc.addField("limitExcepDisplay", obj.getLimitExcepDisplay());					
			solrDoc.addField("minStay", obj.getMinStay());				 			
			solrDoc.addField("networkExceptions", obj.getNetworkExceptions());					
			solrDoc.addField("networkLimitation", obj.getNetworkLimitation());					
			solrDoc.addField("networkLimitationAttribute", obj.getNetworkLimitationAttribute());       
			solrDoc.addField("networkT1CoinsurAttr", obj.getNetworkT1CoinsurAttr());				
			solrDoc.addField("networkT1CoinsurVal", obj.getNetworkT1CoinsurVal());				
			solrDoc.addField("networkT1CopayAttr", obj.getNetworkT1CopayAttr());			 		
			solrDoc.addField("networkT1CopayVal", obj.getNetworkT1CopayVal());					
			solrDoc.addField("networkT1display", obj.getNetworkT1display());					
			solrDoc.addField("networkT1TileDisplay", obj.getNetworkT1TileDisplay());				
			solrDoc.addField("networkT2CoinsurAttr", obj.getNetworkT2CoinsurAttr());			
			solrDoc.addField("networkT2CoinsurVal", obj.getNetworkT2CoinsurVal());			
			solrDoc.addField("networkT2CopayAttr", obj.getNetworkT2CopayAttr());				
			solrDoc.addField("networkT2CopayVal", obj.getNetworkT2CopayVal());				
			solrDoc.addField("networkT2display", obj.getNetworkT2display());				
			solrDoc.addField("outOfNetworkCoinsurAttr", obj.getOutOfNetworkCoinsurAttr());		
			solrDoc.addField("outOfNetworkCoinsurVal", obj.getOutOfNetworkCoinsurVal());			
			solrDoc.addField("outOfNetworkCopayAttr", obj.getOutOfNetworkCopayAttr());			
			solrDoc.addField("outOfNetworkCopayVal", obj.getOutOfNetworkCopayVal());			
			solrDoc.addField("outOfNetworkDisplay", obj.getOutOfNetworkDisplay());			
			solrDoc.addField("subjectToInNetworkDuductible", obj.getSubjectToInNetworkDuductible());       
			solrDoc.addField("subjectToOutNetworkDeductible", obj.getSubjectToOutNetworkDeductible());
			docList.add(solrDoc);
		}
		
		return docList;
	}
	
	
	public boolean savesolrDocs(List<SolrInputDocument> items , SolrServer solrServer) {
		boolean isSuccessful = false;
		long start = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "Received "+items.size()+" document for solr export", null, false);
		try {
			SolrPingResponse res =  solrServer.ping();
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "<----Solr Server Ping response---->"+res.getResponseHeader(), null, false);
			if( res.getStatus() == 0) {
				solrServer.add(items);
				UpdateResponse response = solrServer.commit();
				if( response.getStatus() == 0) {
					isSuccessful = true;
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "Posted "+items.size()+" Records in "+(TimeShifterUtil.currentTimeMillis()-start)+" ms", null, false);
				}
			}
		} catch(Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Failed to communicate with solr server---->", e, false);
		}
		return isSuccessful;
	}

	@Override
	public PdResponse saveMissingSolrDocsSynch(List<Integer> planIds,String insuranceType) throws GIRuntimeException {
		return saveMissingSolrCoreLogic(planIds,insuranceType);
	}
	
	private PdResponse saveMissingSolrCoreLogic(List<Integer> planIds, String insuranceType) {

		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside async step 3---->", null, false);
		long startSaveMissingSolrDocs = TimeShifterUtil.currentTimeMillis();
		
		try { 
			if(planIds==null || planIds.isEmpty()  || (!INSURANCE_TYPE_HEALTH.equals(insuranceType) && !INSURANCE_TYPE_DENTAL.equals(insuranceType))){
				return planDisplaySvcHelper.sendNull();
			}
			
			PdResponse pdResponse = new PdResponse();
			pdResponse.startResponse();
			
			Map<String, Object> data = planDisplayUtil.getIndividualPlanBenefitsAndCostByPlanid(planIds , insuranceType);
			boolean isSuccessful=false;
			if( "HEALTH".equals(insuranceType) ){
				List<PlanHealthBenefitDTO> benefitlist = (List<PlanHealthBenefitDTO>) data.get("healthBenefits");
				List<PlanHealthCostDTO> costlist = (List<PlanHealthCostDTO>) data.get("healthCosts");
				isSuccessful = savesolrDocs(createPlanHealthBenefitSolrDocs(benefitlist), solrPlanBenefitServer);
				if( isSuccessful ){
					isSuccessful = savesolrDocs(createPlanHealthCostSolrDocs(costlist), solrHealthCostServer);
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Saving solr docs successful---->", null, false);
				}
			} else if ( "DENTAL".equals(insuranceType) ) {
				List<PlanDentalBenefitDTO> benefitlist = (List<PlanDentalBenefitDTO>) data.get("dentalBenefits");
				List<PlanDentalCostDTO> costlist = (List<PlanDentalCostDTO>) data.get("dentalCosts");
				isSuccessful = savesolrDocs(createPlanDentalBenefitSolrDocs(benefitlist), solrDentalBenefitServer);
				if( isSuccessful ) {
					isSuccessful = savesolrDocs(createPlanDentalCostSolrDocs(costlist), solrDentalCostServer);
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Saving solr docs successful---->", null, false);
				}
			}
		
			if(isSuccessful) {
				long endSaveMissingSolrDocs = TimeShifterUtil.currentTimeMillis();
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Time taken by saveMissingSolrDocs---->"+(endSaveMissingSolrDocs - startSaveMissingSolrDocs), null, false);
				return planDisplaySvcHelper.sendSuccess(pdResponse);
			} else {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occurred while executing updatePlanBenefitsAndCostInSOLR() method---->"+pdResponse.getErrMsg()+PlanDisplayConstants.HYPHEN+pdResponse.getNestedStackTrace(), null, false);
				return planDisplaySvcHelper.sendFailure("Failed to update SOLD docs");
			}
		} catch(Exception e) {
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_EXECUTE_SAVE_MISSING_SOLR_DOCS,e);
		}
	}
}
