package com.getinsured.plandisplay.service.rules;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.model.PlanRateBenefit;
import com.getinsured.plandisplay.model.costcalculator.BenefitMeta;
import com.getinsured.plandisplay.model.costcalculator.PlanCost;
import com.getinsured.plandisplay.model.costcalculator.Usage;
import com.getinsured.plandisplay.util.PlanDisplayUtil;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;

/**
 * @author karnam_s
 *
 */

public class CostCalculator {
	private static final Logger LOGGER = LoggerFactory.getLogger(CostCalculator.class);
	private static final Double ZERO = new Double(0.0);

	public CostCalculator() {
    }

    public CostCalculator(PlanCost planCost) {
            this.planCost = planCost;
    }

	private PlanCost planCost =new PlanCost();
	private int familyCountLowUsage = 0;
	private int familyCountMediumUsage=0;
	private int familyCountHighUsage=0;
	private int familyCountVeryHighUsage=0;
	private int prescriptionCountLowUsage = 0;
	private int prescriptionCountMediumUsage=0;
	private int prescriptionCountHighUsage=0;
	private int prescriptionCountVeryHighUsage=0;
	private int totalFamilyCount=0;
    private Double currentIndividualDrugDeductibale = 0.0;
    private Double currentFamilyDrugDeductibale = 0.0;
    private Double currentIndividualdrugDeductibale=0.0;
    private Double currentIndividualMedicalDeductibale = 0.0;
    private Double currentFamilyMedicalDeductibale=0.0;
    private Double currentIndividualMedicalOOPCost=0.0;
    private Double currentFamilyMedicalOOPCost=0.0;
    private Double currentIndividualDrugOOPCost=0.0;
    private Double currentFamilyDrugOOPCost=0.0;
    private Double currentIndividualOOPCost=0.0;
    private Double currentFamilyOOPCost=0.0;
    private Double maxPlanDrugDeductibleIndividual = 0.0;
    private Double maxPlanDrugDeductibleFamily = 0.0;
    private Double maxPlanMedicalDeductibleIndividual = 0.0;
    private Double maxPlanMedicalDeductibleFamily = 0.0;
    private Double maxPlanIntegratedDeductibleIndividual = 0.0;
    private Double maxPlanIntegratedDeductibleFamily = 0.0;
    private Double maxOOPMedicalCostIndividual = 0.0;
    private Double maxOOPMedicalCostFamily = 0.0;
    private Double maxOOPDrugCostIndividual = 0.0;
    private Double maxOOPDrugCostFamily = 0.0;
    private Double maxOOPIntegratedCostIndividual = 0.0;
    private Double maxOOPIntegratedCostFamily = 0.0;
    private Double currentIndividualIntegratedDeductibale = 0.0;
    private Double currentFamilyIntegratedDeductibale = 0.0;
	private String isMedicalDeductiblaeCrossed;
	private String isDrugDeductiblecrossed;
	private String isIntegratedDeductiblecrossed;
	private String isIntegratedModelApplied="N";
	private String stateCode;
	private Map<String,Double> averagePrescrionUsageMap;

	/**
	 * @param plan
	 * @param familyUsageCount
	 * @param prescriptionUsage
	 * @return
	 */
	public Double calculatePlanCost(PlanRateBenefit plan, Map<String, Integer> familyUsageCount, Map<String, Integer> prescriptionUsage, String state) {
		Double finalAdjustedCost = 0.0;
		try {
			plan.getPlanDetailsByMember();

			familyCountLowUsage = Integer.parseInt(getMapValue(familyUsageCount, "LOW", "0"));
			familyCountMediumUsage = Integer.parseInt(getMapValue(familyUsageCount, "MEDIUM", "0"));
			familyCountHighUsage = Integer.parseInt(getMapValue(familyUsageCount, "HIGH", "0"));
			familyCountVeryHighUsage = Integer.parseInt(getMapValue(familyUsageCount, "VERYHIGH", "0"));
			isMedicalDeductiblaeCrossed = "N";
			isDrugDeductiblecrossed = "N";
			isIntegratedDeductiblecrossed = "N";
			this.stateCode = state;

			totalFamilyCount = familyCountLowUsage + familyCountMediumUsage + familyCountHighUsage + familyCountVeryHighUsage;
			int houseHoldCount = Integer.parseInt(getMapValue(familyUsageCount, "TOTAL", "0"));
			if(totalFamilyCount == 0) {
				familyCountLowUsage = houseHoldCount;
			}
			else {
				if (houseHoldCount -totalFamilyCount > 0) {
					familyCountLowUsage += houseHoldCount - totalFamilyCount;
				}
			}

			prescriptionCountLowUsage = Integer.parseInt(getMapValue(prescriptionUsage, "LOW", "0"));
			prescriptionCountMediumUsage = Integer.parseInt(getMapValue(prescriptionUsage, "MEDIUM", "0"));
			prescriptionCountHighUsage = Integer.parseInt(getMapValue(prescriptionUsage, "HIGH", "0"));
			prescriptionCountVeryHighUsage = Integer.parseInt(getMapValue(prescriptionUsage, "VERYHIGH", "0"));
			//setPrescriptionUsage(totalFamilyCount, prescriptionUsage);

			//loadMetaData(plan.getId());
            if( plan.getPlanCosts() != null && plan.getPlanBenefits() != null){
            		setPlanHealthCosts(plan.getPlanCosts());
            		calculate(plan);
            }
			finalAdjustedCost = calculateTotalFinalCost(plan.getId());
		}
		catch(Exception ex) {
			finalAdjustedCost = 0.0;
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR,"CostCalculator::calculatePlanCost() exception caught", ex, false);
		}
		return finalAdjustedCost;
	}


	/** Calculates the final oop for each plan based on the costs and usage
	 *
	 * @param planId
	 * @return annualHealthcareCost
	 */
	private Double calculateTotalFinalCost( int planId) {
		Double maxOOPAllowedIndividual = 0.0;
		Double maxOOPAllowedFamily = 0.0;
		Double netOOPAllowed = 0.0;
		Double annualHealthcareCost = 0.0;
		Double oopCost = 0.0;

		if(maxOOPIntegratedCostIndividual.equals(ZERO)) {
			// that means the OOP is at the medical and drugs level
			// we add them to get the OOP
			maxOOPAllowedIndividual = maxOOPMedicalCostIndividual + maxOOPDrugCostIndividual;
			maxOOPAllowedFamily = maxOOPMedicalCostFamily + maxOOPDrugCostFamily;
		}
		else {
			maxOOPAllowedIndividual = maxOOPIntegratedCostIndividual;
			maxOOPAllowedFamily = maxOOPIntegratedCostFamily;
		}

		if("Y".equalsIgnoreCase(isIntegratedModelApplied)) {
			oopCost = currentFamilyOOPCost;
		}
		else {
			oopCost = currentFamilyMedicalOOPCost + currentFamilyDrugOOPCost;
		}
		netOOPAllowed = (totalFamilyCount > 1) ? maxOOPAllowedFamily:maxOOPAllowedIndividual;
		//double adjustedOOP = adjustFinalCost(oopCost);
		double adjustedOOP = oopCost;
		annualHealthcareCost = (adjustedOOP > netOOPAllowed) ? netOOPAllowed  : adjustedOOP;

		//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "OOP for Plan with id: "+ planId +" = "+annualHealthcareCost, null, false);

		return annualHealthcareCost;

	}

	/** Initialize the variables
	 *
	 */
	private void resetPlanHealthCosts() {
		maxOOPMedicalCostIndividual = 0.0;
		maxOOPMedicalCostFamily = 0.0;
		maxOOPDrugCostIndividual = 0.0;
		maxOOPDrugCostFamily = 0.0;
		maxOOPIntegratedCostIndividual = 0.0;
		maxOOPIntegratedCostFamily = 0.0;
		maxPlanMedicalDeductibleIndividual = 0.0;
		maxPlanMedicalDeductibleFamily = 0.0;
		maxPlanDrugDeductibleIndividual = 0.0;
		maxPlanDrugDeductibleFamily = 0.0;
		maxPlanIntegratedDeductibleIndividual = 0.0;
		maxPlanIntegratedDeductibleFamily = 0.0;

		currentFamilyMedicalOOPCost = 0.0;
		currentFamilyDrugOOPCost = 0.0;
		currentFamilyOOPCost = 0.0;
		currentIndividualDrugDeductibale = 0.0;
		currentFamilyDrugDeductibale = 0.0;
		currentIndividualdrugDeductibale = 0.0;
		currentIndividualMedicalDeductibale = 0.0;
		currentFamilyMedicalDeductibale=0.0;
		currentIndividualMedicalOOPCost=0.0;
		currentFamilyMedicalOOPCost=0.0;
		currentIndividualDrugOOPCost=0.0;
		currentFamilyDrugOOPCost=0.0;
	}

	/** Loads the various costs associated with the plan
	 *
	 * @param planHealthCosts
	 */
	//Fix this after PlanManagement changes the method getPlanCosts to pass the Map object instyea dof HashMap
	private void setPlanHealthCosts(Map <String, Map<String, String>> planHealthCosts) {

		resetPlanHealthCosts();
		try {
			Iterator<Entry<String, Map<String, String>>> entries = planHealthCosts.entrySet().iterator();
			while (entries.hasNext()) {
				Entry<String, Map<String, String>> thisEntry = (Entry<String, Map<String, String>>) entries.next();
				Map<String, String> currCost = (Map<String, String>)thisEntry.getValue();
				String name = (String) thisEntry.getKey();

				String inNetworkIndCost = currCost.get("inNetworkInd");
				String inNetworkFlyCost = currCost.get("inNetworkFly");
				double inNetworkIndVal = 0;
				if(inNetworkIndCost!=null && !StringUtils.isEmpty(inNetworkIndCost)){
					inNetworkIndVal = Double.parseDouble(inNetworkIndCost);
				}
				
				double inNetworkFlyVal = 0;
				if(inNetworkFlyCost!=null && !StringUtils.isEmpty(inNetworkFlyCost)){
					inNetworkFlyVal = Double.parseDouble(inNetworkFlyCost);
				}
				
				if("MAX_OOP_MEDICAL".equalsIgnoreCase(name)) {
					maxOOPMedicalCostIndividual = inNetworkIndVal;
					maxOOPMedicalCostFamily = inNetworkFlyVal;
				}

				else if("MAX_OOP_DRUG".equalsIgnoreCase(name)) {
					maxOOPDrugCostIndividual = inNetworkIndVal;
					maxOOPDrugCostFamily = inNetworkFlyVal;
				}

				else if("MAX_OOP_INTG_MED_DRUG".equalsIgnoreCase(name)) {
					maxOOPIntegratedCostIndividual =  inNetworkIndVal;
					maxOOPIntegratedCostFamily = inNetworkFlyVal;
				}

				else if("DEDUCTIBLE_MEDICAL".equalsIgnoreCase(name)) {
					maxPlanMedicalDeductibleIndividual = inNetworkIndVal;
					maxPlanMedicalDeductibleFamily = inNetworkFlyVal;

				}

				else if("DEDUCTIBLE_DRUG".equalsIgnoreCase(name)) {
					//maxPlanDrugDeductibleIndividual = Double.parseDouble(currCost.get("inNetworkInd"));
					//maxPlanDrugDeductibleFamily = Double.parseDouble(currCost.get("inNetworkFly"));
					String drugDeductibleIndividual = inNetworkIndCost;
					String drugDeductibleFamily = inNetworkFlyCost;
					maxPlanDrugDeductibleIndividual = (StringUtils.isEmpty(drugDeductibleIndividual)) ? 0.0:Double.parseDouble(drugDeductibleIndividual);
					maxPlanDrugDeductibleFamily = (StringUtils.isEmpty(drugDeductibleFamily)) ? 0.0: Double.parseDouble(drugDeductibleFamily);

				}

				else if("DEDUCTIBLE_INTG_MED_DRUG".equalsIgnoreCase(name)) {
					maxPlanIntegratedDeductibleIndividual = inNetworkIndVal;
					maxPlanIntegratedDeductibleFamily = inNetworkFlyVal;

					if(maxPlanIntegratedDeductibleIndividual > 0 || maxPlanIntegratedDeductibleFamily > 0 ) {
						isIntegratedModelApplied = "Y";
					}
				}

			}
		}
		catch (Exception ex) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "CostCalculator::setPlanHealthCosts() exception caught", ex, false);
		}
	}

	/** Calculate the cost for each usage level. The cost can be integrated or separate for medical and drugs
	 *
	 * @param pHealth
	 */
	private void calculate(PlanRateBenefit pHealth) {
		try {
			if("Y".equalsIgnoreCase(isIntegratedModelApplied)) {
				calculateIntegratedCosts(pHealth, familyCountLowUsage, "LOW", "MEDICAL");
				calculateIntegratedCosts(pHealth, familyCountMediumUsage, "MEDIUM", "MEDICAL");
				calculateIntegratedCosts(pHealth, familyCountHighUsage, "HIGH", "MEDICAL");
				calculateIntegratedCosts(pHealth, familyCountVeryHighUsage, "VERYHIGH", "MEDICAL");
				
				calculateIntegratedCosts(pHealth, prescriptionCountLowUsage, "LOW", "DRUG");
				calculateIntegratedCosts(pHealth, prescriptionCountMediumUsage, "MEDIUM", "DRUG");
				calculateIntegratedCosts(pHealth, prescriptionCountHighUsage, "HIGH", "DRUG");
				calculateIntegratedCosts(pHealth, prescriptionCountVeryHighUsage, "VERYHIGH", "DRUG");

			}
			else {
				//calculateSepartecosts
				calculatMedicalAndDrugBenefitCosts(pHealth, familyCountLowUsage, "LOW", "MEDICAL");
				calculatMedicalAndDrugBenefitCosts(pHealth, familyCountMediumUsage, "MEDIUM", "MEDICAL");
				calculatMedicalAndDrugBenefitCosts(pHealth, familyCountHighUsage, "HIGH", "MEDICAL");
				calculatMedicalAndDrugBenefitCosts(pHealth, familyCountVeryHighUsage, "VERYHIGH", "MEDICAL");
				
				calculatMedicalAndDrugBenefitCosts(pHealth, prescriptionCountLowUsage, "LOW", "DRUG");
				calculatMedicalAndDrugBenefitCosts(pHealth, prescriptionCountMediumUsage, "MEDIUM", "DRUG");
				calculatMedicalAndDrugBenefitCosts(pHealth, prescriptionCountHighUsage, "HIGH", "DRUG");
				calculatMedicalAndDrugBenefitCosts(pHealth, prescriptionCountVeryHighUsage, "VERYHIGH", "DRUG");
			}
		}
		catch (Exception ex) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "CostCalculator::calculate() exception caught", ex, false);
		}
	}

	/** Sets the number of units for prescribed and non-prescribed drugs
	 * Default value of low is picked if none of the preferences is specified
	 *
	 * @param totalFamilyCount
	 * @param personalUsageForDrugsMap
	 */
	private void setPrescriptionUsage(int totalFamilyCount, Map<String,Integer> personalUsageForDrugsMap) {

		Map<String,Double> prefferedDrugsUsageMapping = new HashMap<String,Double>();
		prefferedDrugsUsageMapping.put("LOW", 0.2);
		prefferedDrugsUsageMapping.put("MEDIUM", 3.6);
		prefferedDrugsUsageMapping.put("HIGH", 7.2);
		prefferedDrugsUsageMapping.put("VERYHIGH", 12.0);

		Map<String,Double> nonPrefferedDrugsUsageMapping = new HashMap<String,Double>();
		nonPrefferedDrugsUsageMapping.put("LOW", 0.1);
		nonPrefferedDrugsUsageMapping.put("MEDIUM", 1.8);
		nonPrefferedDrugsUsageMapping.put("HIGH", 3.6);
		nonPrefferedDrugsUsageMapping.put("VERYHIGH", 6.0);

		Map<String,Double> genericDrugsUsageMapping = new HashMap<String,Double>();
		genericDrugsUsageMapping.put("LOW", 0.7);
		genericDrugsUsageMapping.put("MEDIUM", 12.6);
		genericDrugsUsageMapping.put("HIGH", 25.2);
		genericDrugsUsageMapping.put("VERYHIGH", 42.0);


		Double totalPrefferedPrescription = 0.0;
		Double totalNonPrefferedPrescription= 0.0;
		Double totalGenericPrescription = 0.0;

		try {
			if(personalUsageForDrugsMap != null) {
				Set<Map.Entry<String,Integer>> entrySet = personalUsageForDrugsMap.entrySet();
				for(Map.Entry<String,Integer> entry : entrySet) {
					if(!"TOTAL".equalsIgnoreCase(entry.getKey())) {
						int personCount = entry.getValue() == null ? 0 : entry.getValue();
						if(personCount > 0) {
							Double preferredDrugsCount = personCount * (prefferedDrugsUsageMapping.get(entry.getKey()) == null ? 0.0 : prefferedDrugsUsageMapping.get(entry.getKey()));
							totalPrefferedPrescription += preferredDrugsCount;
							Double nonPreferredDrugsCount = personCount * (nonPrefferedDrugsUsageMapping.get(entry.getKey()) == null ? 0.0 : nonPrefferedDrugsUsageMapping.get(entry.getKey()));
							totalNonPrefferedPrescription += nonPreferredDrugsCount;
							Double genericDrugsCount = personCount * (genericDrugsUsageMapping.get(entry.getKey()) == null ? 0.0 : genericDrugsUsageMapping.get(entry.getKey()));
							totalGenericPrescription += genericDrugsCount;
						}
					}
				}
			}

			if(totalPrefferedPrescription.equals(ZERO)) {
				//totalPrefferedPrescription = prefferedDrugsUsageMapping.get("LOW");
				totalPrefferedPrescription = prefferedDrugsUsageMapping.get("LOW") * totalFamilyCount;
			}
			if(totalNonPrefferedPrescription.equals(ZERO)) {
				//totalNonPrefferedPrescription = nonPrefferedDrugsUsageMapping.get("LOW");
				totalNonPrefferedPrescription = nonPrefferedDrugsUsageMapping.get("LOW") * totalFamilyCount;
			}
			if(totalGenericPrescription.equals(ZERO)) {
				//totalGenericPrescription = genericDrugsUsageMapping.get("LOW");
				totalGenericPrescription = genericDrugsUsageMapping.get("LOW") * totalFamilyCount;
			}
			averagePrescrionUsageMap = new HashMap<String,Double>();
			if(totalFamilyCount > 0) {
				Double averagePreferredDrugsCount = totalPrefferedPrescription/totalFamilyCount;
				Double averageNonPreferredDrugsCount = totalNonPrefferedPrescription/totalFamilyCount;
				Double averageGenericDrugsCount = totalGenericPrescription/totalFamilyCount;
				averagePrescrionUsageMap.put("PREFERRED_BRAND", averagePreferredDrugsCount);
				averagePrescrionUsageMap.put("NON_PREFERRED_BRAND", averageNonPreferredDrugsCount);
				averagePrescrionUsageMap.put("GENERIC", averageGenericDrugsCount);
			}
			else {
				averagePrescrionUsageMap.put("PREFERRED_BRAND", 0.0);
				averagePrescrionUsageMap.put("NON_PREFERRED_BRAND", 0.0);
				averagePrescrionUsageMap.put("GENERIC", 0.0);
			}
		}
		catch (Exception ex) {
			averagePrescrionUsageMap.put("PREFERRED_BRAND", 0.0);
			averagePrescrionUsageMap.put("NON_PREFERRED_BRAND", 0.0);
			averagePrescrionUsageMap.put("GENERIC", 0.0);

			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "Error in setting perscription usage", ex, false);
		}
	}

	/** Calculate the combined costs for all the benefits configured in the meta data xml
	 * for medical and drug
	 *
	 * @param pHealth
	 * @param familyCount
	 * @param usageType
	 */
	private void calculateIntegratedCosts(PlanRateBenefit pHealth, int familyCount, String usageType, String costType) {
		//09242013 suneel
		currentFamilyIntegratedDeductibale = 0.0;
		currentFamilyDrugDeductibale = 0.0;
		currentFamilyMedicalDeductibale = 0.0;
		currentFamilyIntegratedDeductibale = 0.0;

		for(int i=0; i<familyCount; i++) {
			currentIndividualDrugDeductibale = 0.0;
			currentIndividualdrugDeductibale = 0.0;
			currentIndividualMedicalDeductibale = 0.0;

			currentIndividualIntegratedDeductibale = 0.0;
			currentIndividualOOPCost = 0.0;
			currentIndividualDrugOOPCost = 0.0;
			currentIndividualMedicalOOPCost = 0.0;

			resetDeductibleFlags();

			if(planCost != null && planCost.getBenefitList() != null){
				List<BenefitMeta> benefitMetaList = planCost.getBenefitList().getBenefitMeta();				
				int noOfBenefits=benefitMetaList.size();
	
				//For catostrophic
				String planLevel = pHealth.getLevel()==null?"":pHealth.getLevel();
	
				for (int executeIndx=0;executeIndx<noOfBenefits;executeIndx++) {
					BenefitMeta currBenefitMeta = getBenefitMetaByexecuteIdx(executeIndx);
					Map<String,String> benefit =pHealth.getPlanBenefits().get(currBenefitMeta.getName());
	
					if(benefit == null)
					{
						if(currBenefitMeta != null)
						{
							//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "Missing benefit in the plan :"+ currBenefitMeta.getName(), null, false);
						}
					}else{
					//	LOGGER.info("Benefit Name :"+benefit.getName()+", Value :"+benefit.getNetworkValue()+", NW Attr : "+benefit.getNetworkAttribute());
						String deductableType = currBenefitMeta.getDeductableType();
						if("MEDICAL".equalsIgnoreCase(deductableType) && "MEDICAL".equalsIgnoreCase(costType)) {
							//CalculateIntegratedBenefitCost(benefit, currBenefitMeta, usageType, "MEDICAL");
							calculateVisitCosts(benefit, currBenefitMeta, usageType, "MEDICAL", planLevel);
						}
						else if("DRUG".equalsIgnoreCase(deductableType) && "DRUG".equalsIgnoreCase(costType)){
							//CalculateIntegratedBenefitCost(benefit, currBenefitMeta, usageType, "DRUG");
							calculateVisitCosts(benefit, currBenefitMeta, usageType, "DRUG", planLevel);
						}
					}
				}
			}
		}
	}

	/** calculate the costs at medical and drugs separately for the benefits in the meta data xml
	 *
	 * @param pHealth
	 * @param familyCount
	 * @param usageType
	 */
	private void calculatMedicalAndDrugBenefitCosts(PlanRateBenefit pHealth, int familyCount, String usageType, String costType) {

		for(int i=0; i<familyCount; i++) {
			currentIndividualMedicalDeductibale = 0.0;
			currentIndividualOOPCost = 0.0;
			currentIndividualDrugOOPCost = 0.0;
			currentIndividualMedicalOOPCost = 0.0;

			if(planCost != null && planCost.getBenefitList() != null){
				List<BenefitMeta> benefitMetaList = planCost.getBenefitList().getBenefitMeta();
				int noOfBenefits = benefitMetaList.size();
				String planLevel = pHealth.getLevel()==null?"":pHealth.getLevel();
	
				for (int executeIndx=0;executeIndx<noOfBenefits;executeIndx++) {
					BenefitMeta currBenefitMeta = getBenefitMetaByexecuteIdx(executeIndx);
					Map<String,String> benefit = pHealth.getPlanBenefits().get(currBenefitMeta.getName());
					if(benefit == null)
					{
						if(currBenefitMeta != null)
						{
							//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "Missing benefit in the plan: "+ currBenefitMeta.getName(), null, false);
						}
					}
					else{
						String deductableType = currBenefitMeta.getDeductableType();
						if("MEDICAL".equalsIgnoreCase(deductableType) && "MEDICAL".equalsIgnoreCase(costType)) {
							calculateVisitCosts(benefit, currBenefitMeta, usageType, "MEDICAL", planLevel);
						}
						else if("DRUG".equalsIgnoreCase(deductableType) && "DRUG".equalsIgnoreCase(costType)){
							calculateVisitCosts(benefit, currBenefitMeta, usageType, "DRUG", planLevel);
						}
					}
				}
			}
		}
	}

	/** gets the benefitmeta object which has service units, list price and usage for each benefit
	 * @param executeIdx
	 * @return
	 */
	private BenefitMeta getBenefitMetaByexecuteIdx(int executeIdx){
		BenefitMeta resultBenefitMeta = null;
		List<BenefitMeta> benefitMetaList = planCost.getBenefitList().getBenefitMeta();

		if(!benefitMetaList.isEmpty()){

			resultBenefitMeta = benefitMetaList.get(executeIdx);

			if(resultBenefitMeta.getExecuteIdx().intValue()==executeIdx){
				return resultBenefitMeta;
			}else{

				Iterator<BenefitMeta> itrBenefitMeta =benefitMetaList.iterator();
				while(itrBenefitMeta.hasNext()){
					resultBenefitMeta =itrBenefitMeta.next();
					if(resultBenefitMeta.getExecuteIdx().intValue()==executeIdx){
						return resultBenefitMeta;
					}
				}
			}
		}

		if(resultBenefitMeta == null)
		{
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "Error BenefitMeta :: BenefitMeta not defined for the executeIdx: "+ executeIdx, null, false);
		}

		return resultBenefitMeta;
	}

	/** get the usage type for Low, Medium, High and Very High
	 *
	 * @param benefitMeta
	 * @param usageType
	 * @return
	 */
	private Usage getBenefitMetaUsage(BenefitMeta benefitMeta,String usageType){
		Usage resultUsage = null;
		Iterator<Usage> itrUsage =benefitMeta.getUsageUnits().getUsage().iterator();

		while(itrUsage.hasNext()){
			resultUsage =itrUsage.next();
			if(resultUsage.getType().equalsIgnoreCase(usageType)){

				return resultUsage;
			}
		}

		if(resultUsage == null)
		{
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "Error BenefitMeta :: BenefitMeta not defined for the Usage: "+ usageType, null, false);
		}

		return resultUsage;
	}

	/** Calculates the balance amount to reach the medical deductible considering
	 *  the family costs and individual costs which ever is closer to the maximum allowed
	 *
	 * 
	 * @return
	 */
	private double getUnmetMedicalDeductibleAmount() {
		double unmetMedicalDeductibleAmount = 0.0;
		if(maxPlanMedicalDeductibleFamily - currentFamilyMedicalDeductibale >= maxPlanMedicalDeductibleIndividual - currentIndividualMedicalDeductibale) {
			unmetMedicalDeductibleAmount = maxPlanMedicalDeductibleIndividual - currentIndividualMedicalDeductibale;
		}
		else {
			unmetMedicalDeductibleAmount = maxPlanMedicalDeductibleFamily - currentFamilyMedicalDeductibale;
		}
		return unmetMedicalDeductibleAmount;
	}

	/** Calculates the balance amount to reach the drug deductible considering
	 *  the family costs and individual costs which ever is closer to the maximum allowed
	 *
	 * @param tempCost
	 * @return
	 */
	private double getUnmetDrugDeductibleAmount() {
		double unmetDrugDeductibleAmount = 0.0;
		if(maxPlanDrugDeductibleFamily - currentFamilyDrugDeductibale >= maxPlanDrugDeductibleIndividual - currentIndividualdrugDeductibale) {
			unmetDrugDeductibleAmount = maxPlanDrugDeductibleIndividual - currentIndividualdrugDeductibale;
		}
		else {
			unmetDrugDeductibleAmount = maxPlanDrugDeductibleFamily - currentFamilyDrugDeductibale;
		}
		return unmetDrugDeductibleAmount;
	}

	/** Calculates the balance amount to reach the combined deductible considering
	 *  the family costs and individual costs which ever is closer to the maximum allowed
	 *
	 * @param tempCost
	 * @return
	 */
	private double getUnmetIntegratedDeductibleAmount() {
		double unmetIntegratedDeductibleAmount = 0.0;
		if(maxPlanIntegratedDeductibleFamily - currentFamilyIntegratedDeductibale >= maxPlanIntegratedDeductibleIndividual - currentIndividualIntegratedDeductibale) {
			unmetIntegratedDeductibleAmount = maxPlanIntegratedDeductibleIndividual - currentIndividualIntegratedDeductibale;
		}
		else {
			unmetIntegratedDeductibleAmount = maxPlanIntegratedDeductibleFamily - currentFamilyIntegratedDeductibale;
		}
		return unmetIntegratedDeductibleAmount;
	}

	/** prevent null value problems
	 *
	 * @param m
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	private String getMapValue(Map m, String key, String defaultValue) {
		String returnVal;
		if(m == null || m.get(key) == null) {
			returnVal = defaultValue;
		}
		else {
			returnVal = m.get(key).toString();
		}

		return returnVal;
	}

	//***************
	/** Calculate the cost for each benefit for copay/coinsurance\
	 *
	 * @param benefit
	 * @param benefitMeta
	 * @param usageType
	 * @param deductibleType
	 */
	private void calculateVisitCosts(Map<String,String> benefit, BenefitMeta benefitMeta, String usageType, String deductibleType, String planLevel) {
		String paymentAttributeType = getChargeType(benefit);

		//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "paymentAttributeType: "+paymentAttributeType+" for "+benefitMeta.getName(), null, false);

		if("COPAY".equalsIgnoreCase(paymentAttributeType)) {
			calculatePaymentsForCopay(benefit, benefitMeta, usageType, deductibleType, planLevel);
		}
		else if("COINSURANCE".equalsIgnoreCase(paymentAttributeType)) {
			calculatePaymentsForCoInsurance(benefit, benefitMeta, usageType, deductibleType, planLevel);
		}

	}

	/** Determine the type of cost whether copay/coinsurance depending upon the attributes of the benefit
	 *
	 * @param benefit
	 * @return
	 */
	private String getChargeType(Map<String,String> benefit) {
		String attributeType="";
		String networkCopayAttribute= (benefit.get("tier1CopayAttrib")==null)?"":benefit.get("tier1CopayAttrib");
		String networkCoinsuranceAttribute = (benefit.get("tier1CoinsAttrib")==null)?"":benefit.get("tier1CoinsAttrib");

		if("No Charge".equalsIgnoreCase(networkCopayAttribute) || "Not Applicable".equalsIgnoreCase(networkCopayAttribute)) {
			networkCopayAttribute = "0";
		}

		if("No Charge".equalsIgnoreCase(networkCoinsuranceAttribute) || "Not Applicable".equalsIgnoreCase(networkCoinsuranceAttribute)) {
			networkCoinsuranceAttribute = "0";
		}

		if("0".equals(networkCopayAttribute) && "0".equals(networkCoinsuranceAttribute)) {
			attributeType = "NO COST";
		}
		else if("0".equals(networkCopayAttribute) && !("0".equals(networkCoinsuranceAttribute))) {
			attributeType = "COINSURANCE";
		}
		else if(!"0".equals(networkCopayAttribute) && "0".equals(networkCoinsuranceAttribute)) {
			attributeType = "COPAY";
		}

		else if(!"0".equals(networkCopayAttribute) && !"0".equals(networkCoinsuranceAttribute)) {
			//attributeType = "COPAY "; Suneel
			attributeType = getNonZeroPaymentAttribute(benefit);
		}
		else {
			attributeType = "COINSURANCE";
		}
		return attributeType;
	}

	/** Calculate the copay costs for each type of copay attribute for each benefit
	 *
	 * @param benefit
	 * @param benefitMeta
	 * @param usageType
	 * @param deductibleType
	 */
	private void calculatePaymentsForCopay(Map<String,String> benefit, BenefitMeta benefitMeta, String usageType, String deductibleType, String planLevel) {

		double benfitCost=0.0;
		String copayAttribute = StringUtils.isEmpty(benefit.get("tier1CopayAttrib"))?"":benefit.get("tier1CopayAttrib");
		String copayVal = StringUtils.isEmpty(benefit.get("tier1CopayVal"))?"0.0":benefit.get("tier1CopayVal");
		String isDeductibleApplied = (benefit.get("subToNetDeduct")==null)?"No":benefit.get("subToNetDeduct");

		double dCopay = 0.0;
		try {
			dCopay = Double.parseDouble(copayVal);
		}
		catch (Exception e) {
			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "Exception in copay and resetting to 0", null, false);
			dCopay = 0.0;
		}
		Usage usage = getBenefitMetaUsage(benefitMeta, usageType);
		//double serviceUnits = (deductibleType.equalsIgnoreCase("MEDICAL")) ? usage.getUsageLimit().intValue() : drugUsageValue;
		//double serviceUnits = (deductibleType.equalsIgnoreCase("MEDICAL")) ? usage.getUsageLimit().intValue() : averagePrescrionUsageMap.get(benefitMeta.getName());
		double serviceUnits = usage.getUsageLimit().doubleValue();
		/*if(!"CA".equalsIgnoreCase(stateCode) && "DRUG".equalsIgnoreCase(deductibleType)){
			serviceUnits = averagePrescrionUsageMap.get(benefitMeta.getName());
		}*/
		
		double listPrice = usage.getListedCost().doubleValue();
		  if(listPrice < dCopay){
		   dCopay = listPrice;
		  }
		//put code here
		//serviceUnits = readjustServiceUnitsForFreeVisits(benefit, benefitMeta, serviceUnits, planLevel);

		// 09302013
		boolean isAdjustmentForfreeVisistsRequired = isAdjustmentForFreeVsists(benefit, benefitMeta,planLevel);
		Double tempAdjustedCost = 0.0;
		if(isAdjustmentForfreeVisistsRequired) {
			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "Case for first 3 Primary visits for copay", null, false);

			if(serviceUnits <=3) {
				tempAdjustedCost = serviceUnits * dCopay;
				serviceUnits = 0.0; // prevent to recalculate

				//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "Service units <= 3 for payment type copay. serviceUnits for payment type copay: "+serviceUnits+", dCopay: "+dCopay+", Cost for first 3 primary visits for copay: "+ tempAdjustedCost, null, false);
			}
			else {
				tempAdjustedCost = 3 * dCopay;
				serviceUnits = serviceUnits - 3.0; // reduce the service units by 3 as first 3 visits are free

				//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "Service units > 3 for payment type copay. New Service units adjusted after first 3 primary visits for copay: "+serviceUnits+", dCopay: "+dCopay+", Cost for first 3 primary visits for copay: "+ tempAdjustedCost, null, false);
			}

			if("YES".equalsIgnoreCase(isDeductibleApplied)) {
				updateDeudctibles(tempAdjustedCost, deductibleType);
			}

		}

		//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "serviceUnits after any adjustments: "+serviceUnits+", dCopay: "+ dCopay, null, false);

		if(copayAttribute == null) {
			benfitCost = 0.0;
		}

		else if("".equalsIgnoreCase(copayAttribute) || "No Charge".equalsIgnoreCase(copayAttribute) || "0.0".equalsIgnoreCase(copayAttribute)) {

			benfitCost = 0.0; // do nothing

			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "benfitCost for No Charge or 0.0: "+ benfitCost, null, false);
		}

		else if("No Charge after deductible".equalsIgnoreCase(copayAttribute)) {
			benfitCost = getvariableCostsForCopay(deductibleType, serviceUnits, listPrice, listPrice, benefit, "FREE" );

			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "benfitCost for No Charge after deductible: "+ benfitCost, null, false);
		}

		else if("Dollar Amount copay".equalsIgnoreCase(copayAttribute) || "Copay with deductible".equalsIgnoreCase(copayAttribute)) {
			benfitCost = dCopay * serviceUnits;

			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "benfitCost for Dollar Amount copay: "+ benfitCost, null, false);

			if("YES".equalsIgnoreCase(isDeductibleApplied)) {
				updateDeudctibles(benfitCost, deductibleType);
			}
		}

		else if("Copay after deductible".equalsIgnoreCase(copayAttribute)) {
			benfitCost = getvariableCostsForCopay(deductibleType, serviceUnits, dCopay, listPrice, benefit, "COPAY" );
			
			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "benfitCost for Copay after deductible: "+ benfitCost, null, false);
		}

		else if("Copay before deductible".equalsIgnoreCase(copayAttribute)) {
			benfitCost = dCopay * serviceUnits;

			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "benfitCost for  Copay before deductible: "+ benfitCost, null, false);

			if("YES".equalsIgnoreCase(isDeductibleApplied)) {
				updateDeudctibles(benfitCost, deductibleType);
			}
		}

		else if("Copay Per Stay".equalsIgnoreCase(copayAttribute)) {
			benfitCost = dCopay * serviceUnits;

			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "benfitCost for Copay Per Stay: "+ benfitCost, null, false);

			if("YES".equalsIgnoreCase(isDeductibleApplied)) {
				updateDeudctibles(benfitCost, deductibleType);
			}
		}
		else if("Copay Per Day".equalsIgnoreCase(copayAttribute) || "copay per day before deductible".equalsIgnoreCase(copayAttribute)) {
			benfitCost = dCopay * serviceUnits;

			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "benfitCost for Copay Per Day: "+ benfitCost, null, false);

			if("YES".equalsIgnoreCase(isDeductibleApplied)) {
				updateDeudctibles(benfitCost, deductibleType);
			}
		}

		//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "Final benefitCost: "+ (benfitCost + tempAdjustedCost), null, false);

		updateOOPTrackers(benfitCost + tempAdjustedCost, deductibleType);
	}

	/** calculates the copay costs for integrated, medical and drugs
	 *
	 * @param deductibleType
	 * @param serviceUnits
	 * @param dCopay
	 * @param listPrice
	 * @param benefit
	 * @param postDeductibleCost
	 * @return
	 */
	private double getvariableCostsForCopay(String deductibleType, double serviceUnits, double dCopay, double listPrice, Map<String, String> benefit, String postDeductibleCost) {
		double benefitCost = 0.0;
		if("Y".equalsIgnoreCase(isIntegratedModelApplied)) {
			benefitCost = getIntegratedbenefitCostsForCopay(dCopay, serviceUnits, listPrice, benefit, postDeductibleCost);
		}
		else {
			// separate costs for drug and medical
			// medical case
			if("MEDICAL".equalsIgnoreCase(deductibleType)) {
				benefitCost = getMedicalBenfitCostsForCopay(dCopay, serviceUnits, listPrice, benefit, postDeductibleCost);
			}
			else {// Drugs related benefit
				benefitCost = getDrugsBenfitCostsForCopay(dCopay, serviceUnits, listPrice, benefit, postDeductibleCost);
			}
		}

		return benefitCost;
	}

	/** calculate the copay combined costs for medical and drugs before and after deductible
	 * @param dCopay
	 * @param serviceUnits
	 * @param listPrice
	 * @param benefit
	 * @param postDeductibleCost
	 * @return benefitCost
	 */
	private double getIntegratedbenefitCostsForCopay (double dCopay, double serviceUnits, double listPrice, Map<String, String> benefit, String postDeductibleCost) {
		double benefitCost = 0.0;

		//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "currentIndividualIntegratedDeductibale: "+currentIndividualIntegratedDeductibale+", maxPlanIntegratedDeductibleIndividual: "+maxPlanIntegratedDeductibleIndividual+", isIntegratedDeductiblecrossed: "+isIntegratedDeductiblecrossed, null, false);

		if("Y".equalsIgnoreCase(isIntegratedDeductiblecrossed)){
			if("FREE".equalsIgnoreCase(postDeductibleCost)) {
				benefitCost = 0.0;
			}
			else {
				benefitCost = dCopay * serviceUnits;
			}
		}
		else {

			double tempCost = listPrice * serviceUnits;
			double unmetIntegratedDeductibleAmount = 0.0;
			String isDeductibleApplied = (benefit.get("subToNetDeduct")==null)?"NO":benefit.get("subToNetDeduct");

			/*PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "***** isDeductibleApplied: "+isDeductibleApplied
						+ "; [copay] currentIndividualIntegratedDeductibale:  "+currentIndividualIntegratedDeductibale +
						", maxPlanIntegratedDeductibleIndividual:  "+maxPlanIntegratedDeductibleIndividual +
						", tempCost: "+tempCost+
						", currentFamilyIntegratedDeductibale: "+ currentFamilyIntegratedDeductibale+
						", maxPlanIntegratedDeductibleFamily:  "+ maxPlanIntegratedDeductibleFamily+
						", tempCost + currentIndividualIntegratedDeductibale: ,"+ (tempCost + currentIndividualIntegratedDeductibale) +
						", tempCost + currentFamilyIntegratedDeductibale: "+(tempCost + currentFamilyIntegratedDeductibale),null, false
				);*/

			if((tempCost + currentIndividualIntegratedDeductibale >= maxPlanIntegratedDeductibleIndividual) || (tempCost + currentFamilyIntegratedDeductibale >= maxPlanIntegratedDeductibleFamily)) {
				isIntegratedDeductiblecrossed = "Y"; // just crossed the deductible
				unmetIntegratedDeductibleAmount = getUnmetIntegratedDeductibleAmount();
				benefitCost = unmetIntegratedDeductibleAmount + (serviceUnits-(listPrice > 0 ? (unmetIntegratedDeductibleAmount/listPrice) : 0)) * dCopay;

				//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "deductible crossed for copay (integrated) and cost: "+ benefitCost, null, false);

				if("YES".equalsIgnoreCase(isDeductibleApplied)) {
					updateDeudctibles(unmetIntegratedDeductibleAmount, "INTG");
				}
			}
			else {
				benefitCost = tempCost;
				if("YES".equalsIgnoreCase(isDeductibleApplied)) {
					updateDeudctibles(tempCost, "INTG");
				}
			}
		}

		return benefitCost;
	}

	/** calculate the copay medical costs for medical and drugs before and after deductible
	 *
	 * @param dCopay
	 * @param serviceUnits
	 * @param listPrice
	 * @param benefit
	 * @param postDeductibleCost
	 * @return
	 */
	private double getMedicalBenfitCostsForCopay (double dCopay, double serviceUnits, double listPrice, Map<String, String> benefit, String postDeductibleCost) {
		double benfitCost = 0.0;
		if("Y".equalsIgnoreCase(isMedicalDeductiblaeCrossed)) {
			if("FREE".equalsIgnoreCase(postDeductibleCost)) {
				//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "The cost is free", null, false);

				benfitCost = 0.0;
			}
			else {
				benfitCost = dCopay * serviceUnits;
			}

		}
		else {
			double tempCost = listPrice * serviceUnits;
			double unmetMedicalDeductibleAmount = 0.0;

			String isDeductibleApplied = (benefit.get("subToNetDeduct")==null)?"NO":benefit.get("subToNetDeduct");

			/*PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG,"***** isDeductibleApplied: "+isDeductibleApplied
						+ "; [copay] currentIndividualIntegratedDeductibale:  "+currentIndividualIntegratedDeductibale +
								", maxPlanIntegratedDeductibleIndividual:  "+maxPlanIntegratedDeductibleIndividual +
								", tempCost: "+ tempCost+
								", currentFamilyIntegratedDeductibale: "+currentFamilyIntegratedDeductibale +
								", maxPlanIntegratedDeductibleFamily: "+maxPlanIntegratedDeductibleFamily +
								", tempCost + currentIndividualIntegratedDeductibale: "+(tempCost + currentIndividualIntegratedDeductibale) +
								", tempCost + currentFamilyIntegratedDeductibale: "+(tempCost + currentFamilyIntegratedDeductibale), null, false
						
				);
			*/
			if((tempCost + currentIndividualMedicalDeductibale >= maxPlanMedicalDeductibleIndividual) || (tempCost + currentFamilyMedicalDeductibale >= maxPlanMedicalDeductibleFamily)) {
				isMedicalDeductiblaeCrossed = "Y";
				double medicalCost =  0.0;
				double tempMaxPlanMedicalDeductibleIndividual = maxPlanMedicalDeductibleIndividual;
				int serviceUnitsUsed=0;
				while(tempMaxPlanMedicalDeductibleIndividual > 0 || serviceUnitsUsed < serviceUnits){
					if(tempMaxPlanMedicalDeductibleIndividual >= listPrice){
						medicalCost = medicalCost + listPrice;
						tempMaxPlanMedicalDeductibleIndividual = tempMaxPlanMedicalDeductibleIndividual - listPrice;
					}else{
						medicalCost = medicalCost + tempMaxPlanMedicalDeductibleIndividual;
						if(listPrice - tempMaxPlanMedicalDeductibleIndividual > dCopay){
							medicalCost = medicalCost + dCopay;
						}else{
							medicalCost = medicalCost + (listPrice - tempMaxPlanMedicalDeductibleIndividual);
						}
						tempMaxPlanMedicalDeductibleIndividual = 0;
					}
					serviceUnitsUsed++;
				}
				benfitCost = medicalCost + (serviceUnits - serviceUnitsUsed) * dCopay;
				unmetMedicalDeductibleAmount = getUnmetMedicalDeductibleAmount();

				//benfitCost = unmetMedicalDeductibleAmount + (serviceUnits-unmetMedicalDeductibleAmount/listPrice) * dCopay;

				//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "deductible crossed for copay (Medical) and cost: "+ benfitCost, null, false);

				if("YES".equalsIgnoreCase(isDeductibleApplied)) {
					updateDeudctibles(unmetMedicalDeductibleAmount, "MEDICAL");
				}
			}
			else {
				// still didn't meet the deductible
				benfitCost = tempCost;
				if("YES".equalsIgnoreCase(isDeductibleApplied)) {
					updateDeudctibles(tempCost, "MEDICAL");
				}
			}

		}
		return benfitCost;
	}

	/** calculate the copay drug costs for medical and drugs before and after deductible
	 *
	 * @param dCopay
	 * @param serviceUnits
	 * @param listPrice
	 * @param benefit
	 * @param postDeductibleCost
	 * @return
	 */
	private double getDrugsBenfitCostsForCopay (double dCopay, double serviceUnits, double listPrice, Map<String,String> benefit, String postDeductibleCost) {
		double benfitCost = 0.0;
		if("Y".equalsIgnoreCase(isDrugDeductiblecrossed)) {
			if("FREE".equalsIgnoreCase(postDeductibleCost)) {
				benfitCost = 0.0;
			}
			else {
				benfitCost = dCopay * serviceUnits;
			}

		}
		else {
			double tempCost = listPrice * serviceUnits;
			double unmetDrugDeductibleAmount = 0.0;
			String isDeductibleApplied = (benefit.get("subToNetDeduct")==null)?"NO":benefit.get("subToNetDeduct");

			/*PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "***** isDeductibleApplied: "+isDeductibleApplied +
						 " [copay] currentIndividualIntegratedDeductibale: "+ currentIndividualIntegratedDeductibale +
								"maxPlanIntegratedDeductibleIndividual:  "+ maxPlanIntegratedDeductibleIndividual +
								"tempCost: "+ tempCost+
								"currentFamilyIntegratedDeductibale: "+currentFamilyIntegratedDeductibale +
								"maxPlanIntegratedDeductibleFamily: "+maxPlanIntegratedDeductibleFamily +
								"tempCost + currentIndividualIntegratedDeductibale: "+(tempCost + currentIndividualIntegratedDeductibale) +
								"tempCost + currentFamilyIntegratedDeductibale: "+(tempCost + currentFamilyIntegratedDeductibale), null, false
						
				);
			*/
			//if(tempCost + currentIndividualDrugOOPCost >= maxPlanDrugDeductibleIndividual || tempCost + currentFamilyDrugOOPCost >= maxPlanDrugDeductibleFamily) {
			if((tempCost + currentIndividualDrugDeductibale >= maxPlanDrugDeductibleIndividual) || (tempCost + currentFamilyDrugDeductibale >= maxPlanDrugDeductibleFamily)) {
				isDrugDeductiblecrossed = "Y";
				double drugCost =  0.0;
				double tempMaxPlanDrugDeductibleIndividual = maxPlanDrugDeductibleIndividual;
				int serviceUnitsUsed=0;
				while(tempMaxPlanDrugDeductibleIndividual > 0 || serviceUnitsUsed < serviceUnits){
					if(tempMaxPlanDrugDeductibleIndividual >= listPrice){
						drugCost = drugCost + listPrice;
						tempMaxPlanDrugDeductibleIndividual = tempMaxPlanDrugDeductibleIndividual - listPrice;
					}else{
						drugCost = drugCost + tempMaxPlanDrugDeductibleIndividual;
						if(listPrice - tempMaxPlanDrugDeductibleIndividual > dCopay){
							drugCost = drugCost + dCopay;
						}else{
							drugCost = drugCost + (listPrice - tempMaxPlanDrugDeductibleIndividual);
						}
						tempMaxPlanDrugDeductibleIndividual = 0;
					}
					serviceUnitsUsed++;
				}
				benfitCost = drugCost + (serviceUnits - serviceUnitsUsed) * dCopay;
				unmetDrugDeductibleAmount = getUnmetDrugDeductibleAmount();
				
				//benfitCost = unmetDrugDeductibleAmount + (serviceUnits-unmetDrugDeductibleAmount/listPrice) * dCopay;

				//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "deductible crossed for copay (Drugs) and cost: "+ benfitCost, null, false);

				if("YES".equalsIgnoreCase(isDeductibleApplied)) {
					//currentIndividualDrugDeductibale += unmetDrugDeductibleAmount;
					//currentFamilyDrugDeductibale += unmetDrugDeductibleAmount;
					updateDeudctibles(unmetDrugDeductibleAmount, "DRUG");
				}
			}
			else {
				// Still didn't meet the deductible
				benfitCost = listPrice * serviceUnits;

				//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "benfitCost "+ benfitCost, null, false);

				if("YES".equalsIgnoreCase(isDeductibleApplied)) {
					updateDeudctibles(tempCost, "DRUG");
				}
			}
		}

		return benfitCost;
	}

	/** Calculate the coinsurance costs for each type of coinsurance attribute for each benefit
	 *
	 * @param benefit
	 * @param benefitMeta
	 * @param usageType
	 * @param deductibleType
	 */
	private void calculatePaymentsForCoInsurance(Map<String,String> benefit, BenefitMeta benefitMeta, String usageType, String deductibleType, String planLevel) {
		double benfitCost=0.0;
		String coInsuranceAttribute = StringUtils.isEmpty(benefit.get("tier1CoinsAttrib"))?"":benefit.get("tier1CoinsAttrib");
		String coInsuranceVal = StringUtils.isEmpty(benefit.get("tier1CoinsVal"))?"0.0":benefit.get("tier1CoinsVal");
		double dCoInsurance=0.0;
		try {
			dCoInsurance = Double.parseDouble(coInsuranceVal);
		}
		catch (Exception e) {
			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "Exception in settign coinsurance. Value set to 0.0: "+ e.getMessage(), null, false);

			dCoInsurance = 0.0;
		}

		Usage usage = getBenefitMetaUsage(benefitMeta, usageType);
		//double serviceUnits = (deductibleType.equalsIgnoreCase("MEDICAL")) ? usage.getUsageLimit().intValue() : drugUsageValue;
		//double serviceUnits = (deductibleType.equalsIgnoreCase("MEDICAL")) ? usage.getUsageLimit().intValue() : averagePrescrionUsageMap.get(benefitMeta.getName());
		double serviceUnits = usage.getUsageLimit().doubleValue();
		/*if(!"WA".equalsIgnoreCase(stateCode) && !"CA".equalsIgnoreCase(stateCode) && "DRUG".equalsIgnoreCase(deductibleType)){
			serviceUnits = averagePrescrionUsageMap.get(benefitMeta.getName());
		}*/
		double listPrice = usage.getListedCost().doubleValue();

		String isDeductibleApplied = (benefit.get("subToNetDeduct")==null)?"NO":benefit.get("subToNetDeduct");
		//put code here
		//serviceUnits = readjustServiceUnitsForFreeVisits(benefit, benefitMeta, serviceUnits, planLevel);

		//09302013
		boolean isAdjustmentForfreeVisistsRequired = isAdjustmentForFreeVsists(benefit, benefitMeta,planLevel);
		Double tempAdjustedCost = 0.0;
		if(isAdjustmentForfreeVisistsRequired) {
			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "Case for first 3 Primary visits for coinsurance", null, false);

			if(serviceUnits <=3) {
				tempAdjustedCost = serviceUnits * dCoInsurance/100;
				serviceUnits = 0.0; // prevent to recalculate
			}
			else {
				tempAdjustedCost = 3 * dCoInsurance/100;
				serviceUnits = serviceUnits - 3.0; // reduce the service units by 3 as first 3 visits are free
			}

			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "serviceUnits for payment type coinsurance: "+serviceUnits+", dCoInsurance: "+dCoInsurance+", Cost for first 3 primary visits for coinsurance: "+tempAdjustedCost, null, false);

			if("YES".equalsIgnoreCase(isDeductibleApplied)) {
				updateDeudctibles(tempAdjustedCost, deductibleType);
			}

		}

		//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "coinsurance serviceUnits after any adjustments: "+serviceUnits+", dCoInsurance: "+ dCoInsurance, null, false);

		if(coInsuranceAttribute==null) {
			benfitCost = 0.0;
			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "benfitCost "+benfitCost+", coInsuranceAttribute is null", null, false);
		}
		else if("Coinsurance per Stay".equalsIgnoreCase(coInsuranceAttribute)) {
			benfitCost = dCoInsurance * serviceUnits * listPrice/100;

			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "benfitCost for Coinsurance per Stay"+ benfitCost, null, false);

			if("YES".equalsIgnoreCase(isDeductibleApplied)) {
				updateDeudctibles(benfitCost, deductibleType);
			}
		}

		else if("Coinsurance after deductible".equalsIgnoreCase(coInsuranceAttribute)) {
			benfitCost = getvariableCostsForCoInsurance(deductibleType, serviceUnits, dCoInsurance, listPrice, benefit, "COINSURANCE" );

			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "benfitCost for Coinsurance after deductible: "+ benfitCost, null, false);
		}

		else if("Coinsurance before deductible".equalsIgnoreCase(coInsuranceAttribute)) {
			benfitCost = dCoInsurance * serviceUnits * listPrice/100;

			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "benfitCost for Coinsurance before deductible: "+ benfitCost, null, false);

			if("Y".equalsIgnoreCase(isDeductibleApplied)) {
				updateDeudctibles(benfitCost, deductibleType);
			}
		}

		else if("Percent co-insurance".equalsIgnoreCase(coInsuranceAttribute)) {
			benfitCost = dCoInsurance * serviceUnits * listPrice/100;

			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "benfitCost for Percent co-insurance: "+ benfitCost, null, false);

			if("YES".equalsIgnoreCase(isDeductibleApplied)) {
				updateDeudctibles(benfitCost, deductibleType);
			}
		}

		else if("".equalsIgnoreCase(coInsuranceAttribute) || "No Charge".equalsIgnoreCase(coInsuranceAttribute) || "0.0".equalsIgnoreCase(coInsuranceAttribute)) {
			benfitCost = 0.0; // do nothing

			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "benfitCost for No Charge is 0.0", null, false);
		}
		else if("No Charge after deductible".equalsIgnoreCase(coInsuranceAttribute)) {
			benfitCost = getvariableCostsForCoInsurance(deductibleType, serviceUnits, 100, listPrice, benefit, "FREE" );

			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "benfitCost for No Charge after deductible: "+ benfitCost, null, false);
		}

		else if("No Charge before deductible".equalsIgnoreCase(coInsuranceAttribute)) {
			//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "benfitCost for No Charge before deductible: "+ benfitCost, null, false);
			benfitCost = 0.0;
		}

		else {
			benfitCost = 0.0;
		}

		//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "final coinsurance benefitCost: "+ benfitCost+tempAdjustedCost, null, false);

		updateOOPTrackers(benfitCost + tempAdjustedCost, deductibleType);

	}

	/** calculates the coinsurance costs for integrated, medical and drugs
	 *
	 * @param deductibleType
	 * @param serviceUnits
	 * @param dCoInsurance
	 * @param listPrice
	 * @param benefit
	 * @param postDeductibleCost
	 * @return benefitCost
	 */
	private double getvariableCostsForCoInsurance(String deductibleType, double serviceUnits, double dCoInsurance, double listPrice, Map<String,String> benefit, String postDeductibleCost) {
		double benefitCost = 0.0;
		if("Y".equalsIgnoreCase(isIntegratedModelApplied)) {
			benefitCost = getIntegratedbenefitCostsForCoInsurance(dCoInsurance, serviceUnits, listPrice, benefit, postDeductibleCost);
		}
		else {
			// separate costs for drug and medical
			// medical case
			if("MEDICAL".equalsIgnoreCase(deductibleType)) {
				benefitCost = getMedicalBenfitCostsForCoInsurance(dCoInsurance, serviceUnits, listPrice, benefit, postDeductibleCost);
			}
			else {// Drugs related benefit
				benefitCost = getDrugsBenfitCostsForCoInsurance(dCoInsurance, serviceUnits, listPrice, benefit, postDeductibleCost);
			}
		}

		return benefitCost;
	}

	/** calculate the coinsurance combined costs for medical and drugs before and after deductible
	 *
	 * @param dCoInsurance
	 * @param serviceUnits
	 * @param listPrice
	 * @param benefit
	 * @param postDeductibleCost
	 * @return benefitCost
	 */
	private double getIntegratedbenefitCostsForCoInsurance (double dCoInsurance, double serviceUnits, double listPrice, Map<String,String> benefit, String postDeductibleCost) {
		double benefitCost = 0.0;

		/*PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "currentIndividualIntegratedDeductibale: "+currentIndividualIntegratedDeductibale
					+ ", maxPlanIntegratedDeductibleIndividual: "+maxPlanIntegratedDeductibleIndividual
					+ ", isIntegratedDeductiblecrossed: "+isIntegratedDeductiblecrossed, null, false);
		*/
		if("Y".equalsIgnoreCase(isIntegratedDeductiblecrossed)){
			if("FREE".equalsIgnoreCase(postDeductibleCost)) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "The cost is FREE", null, false);
				benefitCost = 0.0;
			}
			else {
				benefitCost = dCoInsurance * serviceUnits * listPrice/100;
			}
		}
		else {

			double tempCost = 0.0;
			tempCost = listPrice * serviceUnits;
			double unmetIntegratedDeductibleAmount = 0.0;
			String isDeductibleApplied = (benefit.get("subToNetDeduct")==null)?"NO":benefit.get("subToNetDeduct");

			/*PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "[coninsurance] isDeductibleApplied: "+isDeductibleApplied +
								", currentIndividualIntegratedDeductibale: "+currentIndividualIntegratedDeductibale +
								", maxPlanIntegratedDeductibleIndividual: "+maxPlanIntegratedDeductibleIndividual +
								", tempCost: " +tempCost+
								", currentFamilyIntegratedDeductibale: "+currentFamilyIntegratedDeductibale +
								", maxPlanIntegratedDeductibleFamily: "+maxPlanIntegratedDeductibleFamily +
								", tempCost + currentIndividualIntegratedDeductibale: "+(tempCost + currentIndividualIntegratedDeductibale) +
								", tempCost + currentFamilyIntegratedDeductibale: "+ (tempCost + currentFamilyIntegratedDeductibale), null, false);
			*/
			if((tempCost + currentIndividualIntegratedDeductibale >= maxPlanIntegratedDeductibleIndividual) || (tempCost + currentFamilyIntegratedDeductibale >= maxPlanIntegratedDeductibleFamily)) {
				isIntegratedDeductiblecrossed = "Y";
				unmetIntegratedDeductibleAmount = getUnmetIntegratedDeductibleAmount();
				benefitCost = unmetIntegratedDeductibleAmount + (serviceUnits-(listPrice > 0 ? (unmetIntegratedDeductibleAmount/listPrice) : 0)) * (dCoInsurance/100) * listPrice ;

				/*PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "currentFamilyIntegratedDeductibale: "+currentFamilyIntegratedDeductibale
							+ ", listPrice: "+listPrice
							+ ", serviceUnits: "+serviceUnits
							+ ", tempCost: "+tempCost
							+ ", deductible crossed for coinsurance (integrated) and cost: "+benefitCost, null, false);
				*/
				if("YES".equalsIgnoreCase(isDeductibleApplied)) {
					updateDeudctibles(unmetIntegratedDeductibleAmount, "INTG");
				}
			}
			else{
				// didn't meet the deductible
				benefitCost = tempCost;

				//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "SK_didnt meet decutible, benefitCost: "+ benefitCost, null, false);

				if("YES".equalsIgnoreCase(isDeductibleApplied)) {
					updateDeudctibles(tempCost, "INTG");
				}
			}

		}

		return benefitCost;
	}

	/** calculate the coinsurance medical costs before and after deductible
	 *
	 * @param dCoInsurance
	 * @param serviceUnits
	 * @param listPrice
	 * @param benefit
	 * @param postDeductibleCost
	 * @return benfitCost
	 */
	private double getMedicalBenfitCostsForCoInsurance (double dCoInsurance, double serviceUnits, double listPrice, Map<String, String> benefit, String postDeductibleCost) {
		double benfitCost = 0.0;
		if("Y".equalsIgnoreCase(isMedicalDeductiblaeCrossed)) {
			if("FREE".equalsIgnoreCase(postDeductibleCost)) {
				benfitCost = 0.0;
			}
			else {
				benfitCost = dCoInsurance * serviceUnits * listPrice/100;
			}

		}
		else {
			double tempCost = listPrice * serviceUnits;
			double unmetMedicalDeductibleAmount = 0.0;
			String isDeductibleApplied = (benefit.get("subToNetDeduct")==null)?"NO":benefit.get("subToNetDeduct");

			/*PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "[coninsurance] " +
								", currentIndividualMedicalDeductibale: "+currentIndividualMedicalDeductibale +
								", maxPlanIntegratedDeductibleIndividual: "+maxPlanMedicalDeductibleIndividual +
								", tempCost: "+tempCost +
								", currentFamilyMedicalDeductibale: "+currentFamilyMedicalDeductibale +
								", maxPlanMedicalDeductibleFamily: "+maxPlanMedicalDeductibleFamily +
								", tempCost + currentIndividualIntegratedDeductibale: "+(tempCost + currentIndividualMedicalDeductibale) +
								", tempCost + currentFamilyIntegratedDeductibale: "+(tempCost + currentFamilyMedicalDeductibale), null, false);
			*/
			if((tempCost + currentIndividualMedicalDeductibale >= maxPlanMedicalDeductibleIndividual) || (tempCost + currentFamilyMedicalDeductibale >= maxPlanMedicalDeductibleFamily)) {
				isMedicalDeductiblaeCrossed = "Y";
				unmetMedicalDeductibleAmount = getUnmetMedicalDeductibleAmount();
				benfitCost = unmetMedicalDeductibleAmount + (serviceUnits-(listPrice > 0 ? (unmetMedicalDeductibleAmount/listPrice) : 0)) * (dCoInsurance/100) * listPrice ;

				//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "deductible crossed for coinsurance (Medical) and cost: "+ benfitCost, null, false);

				if("YES".equalsIgnoreCase(isDeductibleApplied)) {
					//updateOOPTrackers(unmetMedicalDeductibleAmount, "MEDICAL"); // Not required here
					updateDeudctibles(unmetMedicalDeductibleAmount, "MEDICAL");
				}

			}
			else {
				//didnt meet the deductible
				benfitCost = tempCost;
				if("YES".equalsIgnoreCase(isDeductibleApplied)) {
					//updateOOPTrackers(tempCost, "MEDICAL");
					updateDeudctibles(tempCost, "MEDICAL");
				}

			}

		}
		return benfitCost;
	}

	/** calculate the coinsurance drug costs before and after deductible
	 *
	 * @param dCoInsurance
	 * @param serviceUnits
	 * @param listPrice
	 * @param benefit
	 * @param postDeductibleCost
	 * @return benfitCost
	 */
	private double getDrugsBenfitCostsForCoInsurance (double dCoInsurance, double serviceUnits, double listPrice, Map<String, String> benefit, String postDeductibleCost) {
		double benfitCost = 0.0;
		if("Y".equalsIgnoreCase(isDrugDeductiblecrossed)) {
			if("FREE".equalsIgnoreCase(postDeductibleCost)) {
				benfitCost = 0.0;
			}
			else {
				benfitCost = dCoInsurance * serviceUnits * listPrice/100;
			}

		}
		else {
			double tempCost = listPrice * serviceUnits;
			double unmetDrugDeductibleAmount = 0.0;
			String isDeductibleApplied = (benefit.get("subToNetDeduct")==null)?"NO":benefit.get("subToNetDeduct");

			/*PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "[coninsurance] " +
								"currentIndividualDrugDeductibale: "+currentIndividualDrugDeductibale +
								"maxPlanDrugDeductibleIndividual: "+maxPlanDrugDeductibleIndividual +
								"tempCost: "+tempCost +
								"currentFamilyDrugDeductibale: "+currentFamilyDrugDeductibale +
								"maxPlanDrugDeductibleFamily: "+maxPlanDrugDeductibleFamily +
								"tempCost + currentIndividualDrugDeductibale: "+(tempCost + currentIndividualMedicalDeductibale) +
								"tempCost + currentFamilyDrugDeductibale: "+ (tempCost + currentFamilyDrugDeductibale), null, false);
			*/
			if((tempCost + currentIndividualDrugDeductibale >= maxPlanDrugDeductibleIndividual) || (tempCost + currentFamilyDrugDeductibale >= maxPlanDrugDeductibleFamily)) {
				isDrugDeductiblecrossed = "Y";
				unmetDrugDeductibleAmount = getUnmetDrugDeductibleAmount();
				benfitCost = unmetDrugDeductibleAmount + (serviceUnits-(listPrice > 0 ? (unmetDrugDeductibleAmount/listPrice) : 0)) * (dCoInsurance/100) * listPrice ;

				//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "deductible crossed for coinsurance (Drug) and cost: "+ benfitCost, null, false);

				if("YES".equalsIgnoreCase(isDeductibleApplied)) {
					//updateOOPTrackers(unmetDrugDeductibleAmount, "DRUG");// not required here
					updateDeudctibles(unmetDrugDeductibleAmount, "DRUG");
				}
			}
			else {
				// didnt cross the deductible
				benfitCost = tempCost;

				//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "SK_didnt meet decutible, benefitCost "+ benfitCost, null, false);

				if("YES".equalsIgnoreCase(isDeductibleApplied)) {
					//updateOOPTrackers(tempCost, "DRUG");// Not required here
					updateDeudctibles(tempCost, "DRUG");
				}
			}

		}

		return benfitCost;
	}

	/** update the OOP trackers for combined/medical and drug benefits
	 *
	 * @param benefitCost
	 * @param deductibleType
	 */
	private void updateOOPTrackers(double benefitCost, String deductibleType) {
		if("Y".equalsIgnoreCase(isIntegratedModelApplied)){
			currentIndividualOOPCost += benefitCost;
			currentFamilyOOPCost += benefitCost;
		}
		else {
			if("MEDICAL".equalsIgnoreCase(deductibleType)) {
				currentIndividualMedicalOOPCost += benefitCost;
				currentFamilyMedicalOOPCost += benefitCost; // ??????
			}
			else {
				currentIndividualDrugOOPCost += benefitCost;
				currentFamilyDrugOOPCost += benefitCost; // ??????
			}

		}
	}

	/** update the deductible trackers for combined/medical and drug benefits
	 *
	 * @param cost
	 * @param benefitType
	 */
	private void updateDeudctibles(double cost, String benefitType) {
		if("Y".equalsIgnoreCase(isIntegratedModelApplied)) {
			currentIndividualIntegratedDeductibale += cost;
			currentFamilyIntegratedDeductibale += cost;

		}
		else {
			if("DRUG".equalsIgnoreCase(benefitType)) {
				currentIndividualDrugDeductibale += cost;
				currentFamilyDrugDeductibale += cost;

			}
			else {
				currentIndividualMedicalDeductibale += cost;
				currentFamilyMedicalDeductibale += cost;
			}
		}
	}

	/** round the cost to the enarst hundreds
	 *
	 * @param cost
	 * @return costInteger
	 */
	private int adjustFinalCost(double cost) {
		int costInteger = (int)cost;
		if(costInteger > 100) {
			int reminder = costInteger%100;
			costInteger = costInteger - reminder;

			if(reminder > 50) {
				costInteger += 100;
			}
		}
		else {
			if(costInteger > 0) {
				costInteger = 50;
			}
		}
		return costInteger;
	}

	private String getNonZeroPaymentAttribute(Map<String,String> benefit) {
		Double benefitCoinsValue = Double.parseDouble((StringUtils.isEmpty(benefit.get("tier1CoinsVal"))?"0":benefit.get("tier1CoinsVal")));
		Double benefitCopayValue = Double.parseDouble((StringUtils.isEmpty(benefit.get("tier1CopayVal"))?"0":benefit.get("tier1CopayVal")));
		String processType = "COPAY";
		if(benefitCoinsValue > 0 && benefitCopayValue > 0) {
			processType = "COPAY";
		}
		else if(benefitCoinsValue > 0 && benefitCopayValue <= 0 ){
			processType = "COINSURANCE";
		}
		else if(benefitCoinsValue <= 0 && benefitCopayValue > 0 ){
			processType = "COPAY";
		}
		return processType;
	}

	private boolean isAdjustmentForFreeVsists(Map<String,String> benefit, BenefitMeta benefitMeta, String planLevel) {
		boolean adjustmentRequired = false;
		String benefitName = benefitMeta.getName();
		//String stateCode = PlanDisplayConstants.STATE_CODE;
		//String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);//combinedConfig.getString(PlanDisplayConstants.STATE_CODE);
		//LOGGER.debug("**** stateCode: " + stateCode);
		if(!("CA".equalsIgnoreCase(this.stateCode))) {
			return false; // This rule is only for CA
		}
		//Double benefitCoinsValue = Double.parseDouble((benefit.get("tier1CoinsVal")==null)?"0":benefit.get("tier1CoinsVal"));
		Double benefitCopayValue = Double.parseDouble((StringUtils.isEmpty(benefit.get("tier1CopayVal"))?"0":benefit.get("tier1CopayVal")));

		String copayAtributeName = benefit.get("tier1CopayAttrib")==null?"":benefit.get("tier1CopayAttrib");
		String coInsuranceAtributeName = benefit.get("tier1CoinsAttrib")==null?"":benefit.get("tier1CoinsAttrib");

		if("no charge".equalsIgnoreCase(copayAtributeName) || "0".equalsIgnoreCase(copayAtributeName)) {
			copayAtributeName = "0";
		}

		if("no charge".equalsIgnoreCase(coInsuranceAtributeName) || "0".equalsIgnoreCase(coInsuranceAtributeName)) {
			coInsuranceAtributeName = "0";
		}

		if(benefitCopayValue.intValue() == 60 && "PRIMARY_VISIT".equalsIgnoreCase(benefitName)) {
			adjustmentRequired = true;
		}
		else if ("0".equals(copayAtributeName) && "0".equals(coInsuranceAtributeName) &&  "PRIMARY_VISIT".equalsIgnoreCase(benefitName)) {
			adjustmentRequired = true;
		}
		else if("CATASTROPHIC".equalsIgnoreCase(planLevel) && "PRIMARY_VISIT".equalsIgnoreCase(benefitName)) {
			adjustmentRequired = true;
		}
//		if(adjustmentRequired) {
		//PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, " Adjutment required for first 3 visits: "+ adjustmentRequired, null, false);
//		}
		return adjustmentRequired;

	}

	public boolean checkIsDeductibleCrossed(double tempCost) {
		boolean deductibleCrossed = false;
		if("Y".equals(isIntegratedModelApplied)) {
			if(tempCost + currentIndividualIntegratedDeductibale >= maxPlanIntegratedDeductibleIndividual ||
					tempCost + currentFamilyIntegratedDeductibale >= maxPlanIntegratedDeductibleFamily) {

				isIntegratedDeductiblecrossed = "Y"; // just crossed the deductible
				deductibleCrossed = true;

			}
		}
		return deductibleCrossed;
	}

	/**
	 * If the family deductible is met the flag is set for all the family members
	 * The flag is set to N if just individual deductible is met and not the family
	 */
	private void resetDeductibleFlags() {

		if(currentFamilyIntegratedDeductibale >= maxPlanIntegratedDeductibleFamily) {
			isIntegratedDeductiblecrossed = "Y";
		}
		else {
			isIntegratedDeductiblecrossed = "N";
		}

		if(currentFamilyMedicalDeductibale >= maxPlanMedicalDeductibleFamily) {
			isMedicalDeductiblaeCrossed = "Y";
		}
		else {
			isMedicalDeductiblaeCrossed = "N";
		}

		if(currentFamilyDrugDeductibale >= maxPlanDrugDeductibleFamily) {
			isDrugDeductiblecrossed = "Y";
		}
		else {
			isDrugDeductiblecrossed = "N";
		}
	}

}
