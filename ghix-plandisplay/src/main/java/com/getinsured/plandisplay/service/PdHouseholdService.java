package com.getinsured.plandisplay.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.ws.soap.SoapHeader;

import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;
import com.getinsured.hix.model.PdHousehold;
import com.getinsured.hix.model.PdPerson;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.plandisplay.ApplicantEligibilityData;
import com.getinsured.hix.model.plandisplay.GroupData;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest;
import com.getinsured.hix.webservice.plandisplay.individualinformation.ProductType;

public interface PdHouseholdService {
	PdHousehold findById(Long id);
	PdHousehold save(PdHousehold household);
	PdHousehold saveEligibility(IndividualInformationRequest individualInformationRequest, SoapHeader header);
	PdHousehold saveEligibility(EligLead eligLead, InsuranceType insuranceType, Date coverageStartDate);
	PdHousehold saveEligibility(GroupData groupData, InsuranceType insuranceType, Date coverageStartDate);
	PdHousehold findByShoppingId(String shoppingId);
	PdHousehold findPreshoppingHousehold(String leadId, String year);
	void savePreferences(PdPreferencesDTO pdPreferencesDTO ,Long householdId);
	void setSubscriber(Long householdId, String subscriberId, PlanDisplayEnum.InsuranceType insuranceType);
	public void updateSubsidies(PdHousehold pdHousehold, Float subsidy, BigDecimal stateSubsidy, PlanDisplayEnum.InsuranceType insuranceType);
	PdHousehold saveApplicantEligibilityDetails(ApplicantEligibilityData inputData);
	PdHousehold getHouseholdByApplicationId(Long applicationId, String requestType);
	List<PdHousehold> findPdHouseholdsByLeadId(String leadId);
	boolean isOldSubscriberPresent(PdHousehold householdO,List<PdPerson> persons, ProductType productType );
	void updateCostSharing(Long householdId, CostSharing costSharing);
}
