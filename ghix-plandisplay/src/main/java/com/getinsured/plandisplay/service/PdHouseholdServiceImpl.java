package com.getinsured.plandisplay.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.soap.SoapHeader;

import com.fasterxml.jackson.databind.ObjectReader;
import com.getinsured.affiliate.enums.AffiliateAncillary;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;
import com.getinsured.hix.model.PdHousehold;
import com.getinsured.hix.model.PdPerson;
import com.getinsured.hix.model.PdPreferences;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.estimator.mini.MemberRelationships;
import com.getinsured.hix.model.estimator.mini.MiniEstimatorResponse;
import com.getinsured.hix.model.plandisplay.ApplicantEligibilityData;
import com.getinsured.hix.model.plandisplay.FFMPerson;
import com.getinsured.hix.model.plandisplay.GroupData;
import com.getinsured.hix.model.plandisplay.PersonData;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.Gender;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.Relationship;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.RequestType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ShoppingType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.ApplicantEligibilityResponseType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InitialEnrollmentPeriodEligibilityType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicationType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.ProgramEligibilitySnapshotType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.SpecialEnrollmentPeriodEligibilityType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Individual;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Members;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Members.Member;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Shop;
import com.getinsured.hix.webservice.plandisplay.individualinformation.ProductType;
import com.getinsured.plandisplay.repository.PdHouseholdRepository;
import com.getinsured.plandisplay.util.PlanDisplayConstants;
import com.getinsured.plandisplay.util.PlanDisplayUtil;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

@Service("pdHouseholdService")
public class PdHouseholdServiceImpl implements PdHouseholdService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PdHouseholdServiceImpl.class);
	
	@Autowired private PdHouseholdRepository pdHouseholdRepository;
	@Autowired private PdPersonService pdPersonService;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private PlanDisplayUtil planDisplayUtil;
	@Autowired private PdPreferencesService pdPreferencesService;
	@Autowired private Gson platformGson;
		
	boolean isPHIXProfile = "PHIX".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE));
	String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	
	@Override
	public PdHousehold findById(Long id) {
		if(pdHouseholdRepository.exists(id)){
			return pdHouseholdRepository.findOne(id);
		}
		return null;
	}

	@Override
	@Transactional
	public PdHousehold save(PdHousehold household) {
		return pdHouseholdRepository.saveAndFlush(household);
	}
	
	@Override
	public PdHousehold findByShoppingId(String shoppingId) {
		return pdHouseholdRepository.findHouseholdByShoppingId(shoppingId);
	}
	
	@Override
	public PdHousehold findPreshoppingHousehold(String leadId , String year) {
		PdHousehold pdHousehold = null;
		List<PdHousehold> pdHouseholdList = pdHouseholdRepository.findPreShoppingHousehold(Long.parseLong(leadId));
		if(pdHouseholdList != null && !pdHouseholdList.isEmpty())
		{
			if( year != null && (StringUtils.isNotBlank(year) || StringUtils.isNotEmpty(year))){
				for( PdHousehold pdHouse : pdHouseholdList)
				{
					Calendar cal = TSCalendar.getInstance();
					if(pdHouse!=null && pdHouse.getCoverageStartDate() != null){
						cal.setTime(pdHouse.getCoverageStartDate());
						if(StringUtils.equals(year, String.valueOf(cal.get(Calendar.YEAR)))) {
							return pdHouse;
						}
					}
				}
			}
			else{
				pdHousehold = pdHouseholdList.get(0);
			}
		}
		
		return pdHousehold;
	}

	@Override
	@Transactional
	public PdHousehold saveEligibility(IndividualInformationRequest individualInformationRequest, SoapHeader header) {
		PdHousehold pdHousehold = saveHousehold(individualInformationRequest);
		List<PdPerson> persons= savePerson(individualInformationRequest, pdHousehold, header);
		
		if(individualInformationRequest.getHousehold().getIndividual() != null && individualInformationRequest.getHousehold().getIndividual().getAptcForKeep() != null)
		{
			boolean oldSubscriberPresent = isOldSubscriberPresent(pdHousehold,persons,individualInformationRequest.getHousehold().getProductType());
			if(!oldSubscriberPresent)
			{
				pdHousehold.setAptcForKeep(pdHousehold.getMaxSubsidy());
				save(pdHousehold);
			}
			
		}
		return pdHousehold;
	}

	@Override
	@Transactional
	public PdHousehold saveEligibility(EligLead eligLead, InsuranceType insuranceType, Date coverageStartDate) {
		PdHousehold pdHousehold = saveHousehold(eligLead, insuranceType, coverageStartDate);
		savePerson(eligLead, pdHousehold, insuranceType);
		
		return pdHousehold;
	}
	
	@Override
	@Transactional
	public PdHousehold saveEligibility(GroupData groupData, InsuranceType insuranceType, Date coverageStartDate) {
		PdHousehold pdHousehold = saveHousehold(groupData, insuranceType, coverageStartDate);
		savePerson(groupData, pdHousehold, insuranceType);
		
		return pdHousehold;
	}
	
	private void savePerson(GroupData groupData, PdHousehold pdHousehold, InsuranceType insuranceType) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Forming of PdPerson object---->", null, false);
		List<PdPerson> persons = new ArrayList<PdPerson>();
		for(PersonData personData : groupData.getPersonDataList()) {
			PdPerson pdPerson = formPersonObjectfromPersonData(personData, pdHousehold, groupData);
			persons.add(pdPerson);
		}
		pdPersonService.savePerson(persons);
	}
	
	private void savePerson(EligLead eligLead, PdHousehold pdHousehold, InsuranceType insuranceType) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Forming of PdPerson object---->", null, false);
		List<PdPerson> persons = new ArrayList<PdPerson>();
		
		boolean isCTProfile = "CT".equalsIgnoreCase(stateCode);
		boolean isMNProfile = "MN".equalsIgnoreCase(stateCode);
		
		//ObjectMapper mapper = new ObjectMapper();
		MiniEstimatorResponse miniEstimatorResponse = null;
		List<com.getinsured.hix.model.estimator.mini.Member> members = null;
		try {			
			if(InsuranceType.STM.equals(insuranceType) || InsuranceType.AME.equals(insuranceType) 
					|| InsuranceType.DENTAL.equals(insuranceType) || InsuranceType.VISION.equals(insuranceType) || InsuranceType.LIFE.equals(insuranceType)){
					ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaTypeList(ArrayList.class, com.getinsured.hix.model.estimator.mini.Member.class);
					members = reader.readValue(eligLead.getMemberData());
				 //members = mapper.readValue(eligLead.getMemberData(), new TypeReference<List<com.getinsured.hix.model.estimator.mini.Member>>() { });
			}else{
				ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(MiniEstimatorResponse.class);
				//miniEstimatorResponse = mapper.readValue(eligLead.getApiOutput(), new TypeReference<MiniEstimatorResponse>() { });
				miniEstimatorResponse = reader.readValue(eligLead.getApiOutput());
				members = miniEstimatorResponse.getMembers();
			}
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occured while reading members from eligLead APIOutput---->", e, false);
		}
		
		if(members != null){
			for(com.getinsured.hix.model.estimator.mini.Member member : members) {
				boolean addMember = false;
				if("Y".equals(member.getIsSeekingCoverage())){
					addMember = true;
				}
				if(isPHIXProfile && addMember){
					/*addMember = false;
					String memberEligibility = member.getMemberEligibility();
					if("APTC".equalsIgnoreCase(memberEligibility) || "APTC/CSR".equals(memberEligibility) || "NA".equals(memberEligibility) || "Ineligible".equals(memberEligibility)){
						addMember = true;
					}*/
					if(addMember == true && InsuranceType.HEALTH.equals(insuranceType)){
						// If age > 65 set addMember false
						int age = PlanDisplayUtil.getAgeFromCoverageDate(pdHousehold.getCoverageStartDate(), DateUtil.StringToDate(member.getDateOfBirth(), "MM/dd/yyyy"));
						if(age >= 65){
							addMember = false;
						}
						List<String> productSeekingCoverrage = member.getSeekingCoverageProducts();
						if(productSeekingCoverrage != null && productSeekingCoverrage.size() > 0 && !productSeekingCoverrage.contains(AffiliateAncillary.HEALTH.toString())){
							addMember = false;
						}
					}
				}
				if((isMNProfile || isCTProfile) && addMember){
					addMember = true;
					String memberEligibility = member.getMemberEligibility();
					if("CHIP".equalsIgnoreCase(memberEligibility) || "Medicare".equalsIgnoreCase(memberEligibility) || "Medicaid".equalsIgnoreCase(memberEligibility)){
						addMember = false;
					}
					int age = PlanDisplayUtil.getAgeFromCoverageDate(pdHousehold.getCoverageStartDate(), DateUtil.StringToDate(member.getDateOfBirth(), "MM/dd/yyyy"));
					if(age >= 65){
						addMember = false;
					}
				}
				if(addMember){
					PdPerson pdPerson = formPersonObject(eligLead, member, pdHousehold);
					if(pdPerson != null){
						persons.add(pdPerson);
					}
				}
			}
		}
		
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Total persons formed---->"+persons.size(), null, false);
		if (persons.size() > 0)
		{
			pdPersonService.savePerson(persons);
		}
		
	}
	
	private PdPerson formPersonObjectfromPersonData(PersonData personData, PdHousehold pdHousehold,GroupData groupData) {
		PdPerson pdPerson = new PdPerson();
		pdPerson.setPdHousehold(pdHousehold);
		pdPerson.setRelationship(Relationship.valueOf(personData.getRelationship().toUpperCase()));
		
		Date dob = DateUtil.StringToDate(personData.getDob(), GhixConstants.REQUIRED_DATE_FORMAT);

		YorN seekCoverage = planDisplayUtil.setSeekCoverage(pdHousehold.getCoverageStartDate(), personData.getDob());
		pdPerson.setSeekCoverage(seekCoverage);
		pdPerson.setExternalId(personData.getPersonId());
		pdPerson.setZipCode(groupData.getZipcode());
		pdPerson.setCountyCode(groupData.getCountycode());
		pdPerson.setBirthDay(dob);
		pdPerson.setCatastrophicEligible(YorN.N);
		if(StringUtils.isNotBlank(personData.getGender())){
			pdPerson.setGender(("Female".equalsIgnoreCase(personData.getGender()) ||"F".equalsIgnoreCase(personData.getGender())) ? Gender.F : Gender.M);
		}
		pdPerson.setTobacco(YorN.N);
		return pdPerson;
	}
	
	private PdPerson formPersonObject(EligLead eligLead, com.getinsured.hix.model.estimator.mini.Member member, PdHousehold pdHousehold) {
		PdPerson pdPerson = new PdPerson();
		pdPerson.setPdHousehold(pdHousehold);
		MemberRelationships memberRelationship = member.getRelationshipToPrimary();
		
		if(MemberRelationships.DEFAULT.equals(memberRelationship)){
			pdPerson.setRelationship(Relationship.DEPENDENT);
		}else{
			if(memberRelationship != null){
				pdPerson.setRelationship(Relationship.valueOf(memberRelationship.toString()));
			}
			else
			{
				return null;
			}
		}
		
		YorN seekCoverage = planDisplayUtil.setSeekCoverage(pdHousehold.getCoverageStartDate(), member.getDateOfBirth());
		pdPerson.setSeekCoverage(seekCoverage);
		
		pdPerson.setExternalId(String.valueOf(member.getMemberNumber()));
		pdPerson.setZipCode(eligLead.getZipCode());
		pdPerson.setCountyCode(eligLead.getCountyCode());
		pdPerson.setBirthDay(DateUtil.StringToDate(member.getDateOfBirth(),"MM/dd/yyyy"));
		pdPerson.setCatastrophicEligible(YorN.N);
		if(StringUtils.isNotBlank(member.getGender())){
			pdPerson.setGender(("Female".equalsIgnoreCase(member.getGender()) ||"F".equalsIgnoreCase(member.getGender())) ? Gender.F : Gender.M);
		}
		
		if (member.getIsTobaccoUser() != null) {
			if(member.getIsTobaccoUser().equalsIgnoreCase(com.getinsured.hix.webservice.plandisplay.individualinformation.YesNoVal.Y.toString())) {
				pdPerson.setTobacco(YorN.Y);
			} else {
				pdPerson.setTobacco(YorN.N);
			}
		}

		return pdPerson;
	}

	private PdHousehold saveHousehold(EligLead eligLead, InsuranceType insuranceType, Date coverageStartDate) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Preparing the houlsehold object from request---->", null, false);
		PdHousehold pdHousehold = new PdHousehold();

		if(InsuranceType.HEALTH.equals(insuranceType)){
			CostSharing csr = null;
			if(eligLead.getCsr() != null && StringUtils.isNotBlank(eligLead.getCsr()) && !("N/A".equalsIgnoreCase(eligLead.getCsr()))){
				csr = CostSharing.valueOf(eligLead.getCsr());
				pdHousehold.setCostSharing(csr);
			}
			
			if(eligLead.getAptc() != null && StringUtils.isNotBlank(eligLead.getAptc()) && !("N/A".equalsIgnoreCase(eligLead.getAptc()))){
				String aptc = eligLead.getAptc().replace("$","");
				pdHousehold.setMaxSubsidy(Float.parseFloat(aptc));
			}
		}
			
		pdHousehold.setEligLeadId(eligLead.getId());
		pdHousehold.setCoverageStartDate(coverageStartDate);
		pdHousehold.setEnrollmentType(EnrollmentType.I);
		pdHousehold.setShoppingType(ShoppingType.INDIVIDUAL);
		
		pdPreferencesService.savePdPreferencesByEligLeadId(eligLead.getId());
		
		pdHousehold = save(pdHousehold);		
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Persisted pdhousehold object---->", null, false);
		return pdHousehold;
	}
	
	private PdHousehold saveHousehold(GroupData groupData, InsuranceType insuranceType, Date coverageStartDate) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Preparing the houlsehold object from request---->", null, false);
		PdHousehold pdHousehold = new PdHousehold();
		if(InsuranceType.HEALTH.equals(insuranceType)){
			CostSharing csr = null;
			if(groupData.getCsr() != null && StringUtils.isNotBlank(groupData.getCsr()) && !("N/A".equalsIgnoreCase(groupData.getCsr()))){
				csr = CostSharing.valueOf(groupData.getCsr());
				pdHousehold.setCostSharing(csr);
			}
				
			if(groupData.getAptc() != null){
				pdHousehold.setMaxSubsidy(groupData.getAptc());
			}

			if(null != groupData.getStateSubsidy()){
				pdHousehold.setMaxStateSubsidy(groupData.getStateSubsidy());
			}
		}
			
		pdHousehold.setCoverageStartDate(coverageStartDate);
		pdHousehold.setEnrollmentType(EnrollmentType.I);
		pdHousehold.setShoppingType(ShoppingType.INDIVIDUAL);
		
		pdHousehold = save(pdHousehold);
		
		pdPreferencesService.savePdPreferencesByHouseholdId(null,pdHousehold.getId());
		
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Persisted pdhousehold object---->", null, false);
		return pdHousehold;
	}

	private PdHousehold saveHousehold(IndividualInformationRequest individualInformationRequest) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Preparing the household object from request---->", null, false);
		if(individualInformationRequest != null && individualInformationRequest.getHousehold() != null && individualInformationRequest.getHousehold().getIndividual() != null) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "Case Id: "+individualInformationRequest.getHousehold().getIndividual().getHouseholdCaseId(), null, false);
		}
		PdHousehold pdHousehold = new PdHousehold();
		IndividualInformationRequest.Household household=individualInformationRequest.getHousehold();
		pdHousehold.setApplicationId(household.getApplicationId());
		if(household.getAutoRenewal()!=null){
			YorN yesNo = YorN.valueOf(household.getAutoRenewal().toString());
			pdHousehold.setAutoRenewal(yesNo);
		}
		
		if (household.getIndividual() != null) {
			setHouseholdForIndividual(household.getIndividual(), pdHousehold, household.getProductType());
		} else {
			setHouseholdForEmployee(household.getShop(), pdHousehold);
		}
		
		if(EnrollmentType.S.equals(pdHousehold.getEnrollmentType())){
			YorN yesNo = YorN.N;
			if(household.getKeepOnlyFlag() != null)
			{
				yesNo = YorN.valueOf(household.getKeepOnlyFlag().toString());
			}
			pdHousehold.setKeepOnly(yesNo);
		}	
		pdHousehold.setPreviewId(household.getPreviewId());
		
		pdHousehold = save(pdHousehold);

		if(household.getPreviewId() != null){
			PdHousehold preshoppingHousehold = findByShoppingId(household.getPreviewId());
			if(preshoppingHousehold != null){
				PdPreferences preshoppingPreferencesObj = pdPreferencesService.findPreferencesByPdHouseholdId(preshoppingHousehold.getId());
				PdPreferences pdPreferences = new PdPreferences();
				String preshoppingPreferences = preshoppingPreferencesObj.getPreferences();	
				String preshoppingDoctors = preshoppingPreferencesObj.getDoctors();	
				String preshoppingPrescriptions = preshoppingPreferencesObj.getPrescriptions();
				pdPreferences.setPdHouseholdId(pdHousehold.getId());
				pdPreferences.setPreferences(preshoppingPreferences);
				pdPreferences.setDoctors(preshoppingDoctors);
				pdPreferences.setPrescriptions(preshoppingPrescriptions);
				pdPreferencesService.savePdPreferencesByHouseholdId(pdPreferences, pdHousehold.getId());
			}else{
				pdPreferencesService.savePdPreferencesByHouseholdId(null, pdHousehold.getId());
			}
		}else{
			PdPreferences pdPreferences = pdPreferencesService.findPreferencesByPdHouseholdId(pdHousehold.getId());
			pdPreferencesService.savePdPreferencesByHouseholdId(pdPreferences, pdHousehold.getId());		
		}
		
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Persisted pdhousehold object---->", null, false);
		if(individualInformationRequest != null && individualInformationRequest.getHousehold() != null && individualInformationRequest.getHousehold().getIndividual() != null) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "Case Id: "+individualInformationRequest.getHousehold().getIndividual().getHouseholdCaseId(), null, false);
		}
		return pdHousehold;
	}
	
	
	public boolean isOldSubscriberPresent(PdHousehold householdO,List<PdPerson> persons, ProductType productType ) {

		Long medicalEnrollmentId = null;
		String oldSubscriber = null;
		for(PdPerson person : persons)
		{
			if (ProductType.H.equals(productType) || ProductType.A.equals(productType)|| productType== null)
			{
				if(person.getHealthEnrollmentId() != null){
					medicalEnrollmentId = person.getHealthEnrollmentId();
					break;
				}
			}
			else if (ProductType.D.equals(productType))
			{
				if(person.getDentalEnrollmentId() != null){
					medicalEnrollmentId = person.getDentalEnrollmentId();
					break;
				}
			}
		}
		if(medicalEnrollmentId != null) {
			EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
			enrollmentRequest.setEnrollmentId(medicalEnrollmentId.intValue());
			/*XStream xstream = GhixUtils.getXStreamStaxObject();
			String enrollmentResponseStr = ghixRestTemplate.postForObject(GhixEndPoints.EnrollmentEndPoints.GET_PLANID_AND_ORDERITEMID_BY_ENROLLMENT_ID_URL, xstream.toXML(enrollmentRequest),String.class);*/
			String enrollmentResponseStr = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_PLANID_ORDERITEM_ID_BY_ENROLLMENT_ID_JSON, GhixConstants.USER_NAME_EXCHANGE, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollmentRequest)).getBody();
			EnrollmentResponse enrollmentResponse =  platformGson.fromJson(enrollmentResponseStr, EnrollmentResponse.class);
			if (enrollmentResponse != null)
			{
				oldSubscriber = enrollmentResponse.getSubscriberMemberId();
			}
		}	
		if(oldSubscriber == null)
		{
			return false;
		}
		else
		{
			for(PdPerson member : persons)
			{
				if(oldSubscriber.equals(member.getExternalId()))
				{
					return true;
				}
			}
			return false;
		}
	}

	private void setHouseholdForIndividual(Individual individual, PdHousehold pdHousehold, ProductType productType) {
		EnrollmentType enrollmentType = EnrollmentType.valueOf(individual.getEnrollmentType().toString());
		
		if(StringUtils.isEmpty(individual.getCsr()) || StringUtils.isBlank(individual.getCsr())){
			CostSharing csr = CostSharing.valueOf("CS1");
			pdHousehold.setCostSharing(csr);
		} else {
			CostSharing csr = CostSharing.valueOf(individual.getCsr());
			pdHousehold.setCostSharing(csr);
		}

		if(individual.getAptc() != null){
			pdHousehold.setMaxSubsidy(new BigDecimal(Float.toString(individual.getAptc())).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue());
			if(ProductType.D.equals(productType)){
				pdHousehold.setDentalSubsidy(new BigDecimal(Float.toString(individual.getAptc())).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue());
				pdHousehold.setHealthSubsidy(0.0f);
				pdHousehold.setStateSubsidy(null);
			}
		}

		if(individual.getStateSubsidy() != null){
			pdHousehold.setMaxStateSubsidy(individual.getStateSubsidy());
			if(ProductType.D.equals(productType)){
				pdHousehold.setDentalSubsidy(new BigDecimal(Float.toString(individual.getAptc())).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue());
				pdHousehold.setHealthSubsidy(0.0f);
				pdHousehold.setStateSubsidy(null);
			}
		}

		if(individual.getAptcForKeep() != null){
			pdHousehold.setAptcForKeep(new BigDecimal(Float.toString(individual.getAptcForKeep())).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue());
		}
		
		pdHousehold.setExternalId(individual.getHouseholdCaseId()+"");
		pdHousehold.setCoverageStartDate(DateUtil.StringToDate(individual.getCoverageStartDate(),"MM/dd/yyyy"));
		if(individual.getFinancialEffectiveDate() != null)
		{
			pdHousehold.setFinancialEffectiveDate(DateUtil.StringToDate(individual.getFinancialEffectiveDate(),"MM/dd/yyyy"));
		}
		pdHousehold.setEnrollmentType(enrollmentType);
		pdHousehold.setShoppingType(ShoppingType.INDIVIDUAL);
	}
	
	private void setHouseholdForEmployee(Shop shop, PdHousehold pdHousehold) {
		EnrollmentType enrollmentType = EnrollmentType.valueOf(shop.getEnrollmentType().toString());
		
		pdHousehold.setCoverageStartDate(DateUtil.StringToDate(shop.getCoverageStartDate(),"MM/dd/yyyy"));
		pdHousehold.setEnrollmentType(enrollmentType);
		pdHousehold.setExternalId(shop.getHouseholdCaseId()+"");
		pdHousehold.setShoppingType(ShoppingType.EMPLOYEE);
		pdHousehold.setEmployerId(Long.valueOf(shop.getEmployerId()));
	}

	private List<PdPerson> savePerson(IndividualInformationRequest individualInformationRequest, PdHousehold pdHousehold, SoapHeader header) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Forming of PdPerson object---->", null, false);
		if(individualInformationRequest != null && individualInformationRequest.getHousehold() != null && individualInformationRequest.getHousehold().getIndividual() != null) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "Case Id: "+individualInformationRequest.getHousehold().getIndividual().getHouseholdCaseId(), null, false);
		}
		List<PdPerson> persons = new ArrayList<PdPerson>();
		
		Members members = individualInformationRequest.getHousehold().getMembers();
		Map<String,String> memberCoverageDataMap = null;
		boolean isAdultInExistingMembers = false;
		if (EnrollmentType.S.equals(pdHousehold.getEnrollmentType())){
			memberCoverageDataMap = getMemberCoverageDate(members.getMember(), header, DateUtil.dateToString(pdHousehold.getCoverageStartDate(), "MM/dd/yyyy"));
			isAdultInExistingMembers = isAdultPresentInExistingMemberList(members.getMember(), memberCoverageDataMap);
		}
		for (Member member : members.getMember()) {
			PdPerson pdPerson = formPersonObject(member, individualInformationRequest.getHousehold(), pdHousehold, isAdultInExistingMembers, memberCoverageDataMap);
			persons.add(pdPerson);
		}
		
		if (EnrollmentType.S.equals(pdHousehold.getEnrollmentType()) || EnrollmentType.A.equals(pdHousehold.getEnrollmentType())) {
			this.setPersonSeekCoverageDental(pdHousehold, persons);
		}
		
		final String stateExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);
		if("Individual".equalsIgnoreCase(stateExchangeType) && EnrollmentType.S.equals(pdHousehold.getEnrollmentType()) && individualInformationRequest.getHousehold().getDisenrollMembers()!=null){
			planDisplayUtil.getEnrollmentIdFromDisenrollMembers(persons, individualInformationRequest.getHousehold().getDisenrollMembers(), pdHousehold.getCoverageStartDate());			
		}
		pdPersonService.savePerson(persons);
		
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Total PdPerson records inserted---->"+persons.size(), null, false);
		if(individualInformationRequest != null && individualInformationRequest.getHousehold() != null && individualInformationRequest.getHousehold().getIndividual() != null) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "Case Id: "+individualInformationRequest.getHousehold().getIndividual().getHouseholdCaseId(), null, false);
		}
		return persons;
	}

	private Map<String,String> getMemberCoverageDate(List<Member> members, SoapHeader header, String changeEffectiveDate)
	{
		String userName = planDisplayUtil.getCurrentUser(header);
		Map<String,String> data = new HashMap<String,String>();
		for(Member member : members)
		{
			String medicalEnrollmentId  = null;
			String sadpEnrollmentId  = null;
			if(member.getExistingMedicalEnrollmentID() != null && !("".equals(member.getExistingMedicalEnrollmentID()))){
				medicalEnrollmentId = member.getExistingMedicalEnrollmentID();
			} 
			if(member.getExistingSADPEnrollmentID() != null && !("".equals(member.getExistingSADPEnrollmentID()))){
				sadpEnrollmentId = member.getExistingSADPEnrollmentID();
			}
			if(medicalEnrollmentId != null) {
				planDisplayUtil.getMemberCoverageDateforHealthDental(data, medicalEnrollmentId, userName, member.getMemberId(), "H", changeEffectiveDate);
			}
			
			if(sadpEnrollmentId != null) {
				planDisplayUtil.getMemberCoverageDateforHealthDental(data, sadpEnrollmentId, userName, member.getMemberId(), "D", changeEffectiveDate);
			}			
		}
		return data;
	}
	
	private boolean isAdultPresentInExistingMemberList(List<Member> members, Map<String,String> memberCoverageDateMap) 
	{
		boolean isAdult = false;
		for(Member member : members)
		{			
			if ("N".equals(member.getNewPersonFLAG().value())) 
			{
				String coverageStartDate = null;
				if (member.getExistingMedicalEnrollmentID() != null && !("".equals(member.getExistingMedicalEnrollmentID()))) {
					coverageStartDate = memberCoverageDateMap.get("H"+member.getMemberId());
				}
				if (member.getExistingSADPEnrollmentID() != null && !("".equals(member.getExistingSADPEnrollmentID()))) {
					coverageStartDate = memberCoverageDateMap.get("D"+member.getMemberId());
			    }
				
				if(isAdult == false){
					isAdult = planDisplayUtil.isAdultMember(coverageStartDate, member.getDob());
				}
				if(isAdult == true){
					return true;
				}
					
			}			
		}
		return isAdult;
	}

	private PdPerson formPersonObject(Member member, Household indHousehold, PdHousehold pdHousehold, boolean isAdultInExistingMembers, Map<String,String> memberCoverageDateMap) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside formPersonObject : Forming PdPerson with memberId---->"+member.getMemberId(), null, false);

		PdPerson pdPerson = new PdPerson();
		pdPerson.setPdHousehold(pdHousehold);
		pdPerson.setExternalId(String.valueOf(member.getMemberId()));
		pdPerson.setFirstName(member.getFirstName());
		pdPerson.setLastName(member.getLastName());
		
		String strRelationship = PlanDisplayConstants.relationships.get(member.getResponsiblePersonRelationship());
		
		Relationship relationship = Relationship.valueOf(strRelationship.toUpperCase());
		pdPerson.setRelationship(relationship);
		
		if(ShoppingType.EMPLOYEE.equals(pdHousehold.getShoppingType())){
			pdPerson.setZipCode(indHousehold.getShop().getEmployeeQuotingZip()); 
			pdPerson.setCountyCode(indHousehold.getShop().getEmployeeQuotingCountyCode());
		}else{
			pdPerson.setZipCode(member.getHomeZip());
			pdPerson.setCountyCode(member.getCountyCode());
		}
		
		pdPerson.setBirthDay(DateUtil.StringToDate(member.getDob(),"MM/dd/yyyy"));

		if(com.getinsured.hix.webservice.plandisplay.individualinformation.YesNoVal.Y.equals(member.getCatastrophicEligible())) {
			pdPerson.setCatastrophicEligible(YorN.Y);
		} else {
			pdPerson.setCatastrophicEligible(YorN.N);
		}
		
		if (member.getTobacco() != null) {
			if(com.getinsured.hix.webservice.plandisplay.individualinformation.YesNoVal.Y.equals(member.getTobacco())) {
				pdPerson.setTobacco(YorN.Y);
			} else {
				pdPerson.setTobacco(YorN.N);
			}
		}
		
		if (EnrollmentType.S.equals(pdHousehold.getEnrollmentType()) || EnrollmentType.A.equals(pdHousehold.getEnrollmentType())) {
			setSpcialEnrollmentData(member, pdHousehold, isAdultInExistingMembers, pdPerson, memberCoverageDateMap);
		} else {
			YorN seekCoverage = planDisplayUtil.setSeekCoverage(pdHousehold.getCoverageStartDate(), member.getDob());
			if(stateCode.equalsIgnoreCase("CA")){
				ProductType productType = indHousehold.getProductType();
				if(ProductType.H.equals(productType)){
					seekCoverage = YorN.N;
				}
			}
			pdPerson.setSeekCoverage(seekCoverage);
		}
		if (member.getNewPersonFLAG() != null)
		{
			YorN newPersonFLAG = YorN.valueOf(member.getNewPersonFLAG().toString());
			pdPerson.setNewPersonFlag(newPersonFLAG);
		}
		pdPerson.setMaintenanceReasonCode(member.getMaintenanceReasonCode());
		if(member.getExistingMedicalEnrollmentID() != null && !("".equals(member.getExistingMedicalEnrollmentID()))){
			pdPerson.setHealthEnrollmentId(Long.parseLong(member.getExistingMedicalEnrollmentID()));
		}
		if(member.getExistingSADPEnrollmentID() != null && !("".equals(member.getExistingSADPEnrollmentID()))){
			pdPerson.setDentalEnrollmentId(Long.parseLong(member.getExistingSADPEnrollmentID()));
		}
		
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----formPersonObject : Forming PdPerson with memberId---->"+member.getMemberId()+" Complete", null, false);

		return pdPerson;
	}
	
	private void setSpcialEnrollmentData(Member member, PdHousehold pdHousehold, boolean isAdultInExistingMembers, PdPerson pdPerson, Map<String,String> memberCoverageDateMap) 
	{
		if (EnrollmentType.S.equals(pdHousehold.getEnrollmentType())) {
			if(member.getExistingMedicalEnrollmentID() != null && !("".equals(member.getExistingMedicalEnrollmentID()))) {
				pdPerson.setHealthCoverageStartDate(pdHousehold.getCoverageStartDate());
			}
			if (member.getExistingSADPEnrollmentID() != null && !("".equals(member.getExistingSADPEnrollmentID()))) {
				pdPerson.setDentalCoverageStartDate(pdHousehold.getCoverageStartDate());
			}
			
			if(memberCoverageDateMap != null && memberCoverageDateMap.get("H"+member.getMemberId()) != null) {
				pdPerson.setHealthCoverageStartDate(DateUtil.StringToDate(memberCoverageDateMap.get("H"+member.getMemberId()), "MM/dd/yyyy"));
			}
			if(memberCoverageDateMap != null && memberCoverageDateMap.get("D"+member.getMemberId()) != null) {
				pdPerson.setDentalCoverageStartDate(DateUtil.StringToDate(memberCoverageDateMap.get("D"+member.getMemberId()), "MM/dd/yyyy"));
			}
		}
		
		if (member.getNewPersonFLAG() != null && "Y".equals(member.getNewPersonFLAG().value())) {
			int age = PlanDisplayUtil.getAgeFromCoverageDate(pdHousehold.getCoverageStartDate(), DateUtil.StringToDate(member.getDob(),"MM/dd/yyyy"));
			if (age < PlanDisplayConstants.CHILD_AGE) {
				pdPerson.setSeekCoverage(YorN.Y);
			} else {
				String dentalPlanSelection = planDisplayUtil.getDentalConfig(pdHousehold.getCoverageStartDate()); 
				if(isAdultInExistingMembers && !"PO".equals(dentalPlanSelection)){
					pdPerson.setSeekCoverage(YorN.Y);
				}else if("CA".equalsIgnoreCase(stateCode) && !"PO".equals(dentalPlanSelection)){
					pdPerson.setSeekCoverage(YorN.Y);
				}else{
					pdPerson.setSeekCoverage(YorN.N);
				}
			}
		} else if (member.getExistingSADPEnrollmentID() != null && !("".equals(member.getExistingSADPEnrollmentID()))) {
			YorN seekCoverage = planDisplayUtil.setSeekCoverage(pdPerson.getDentalCoverageStartDate(), member.getDob());
			pdPerson.setSeekCoverage(seekCoverage);
		} else if("ID".equalsIgnoreCase(stateCode)){
			pdPerson.setSeekCoverage(YorN.N);
		}else{
			pdPerson.setSeekCoverage(YorN.Y);
		}
				
	}

	@Override
	public void savePreferences(PdPreferencesDTO pdPreferencesDTO,Long householdId) {
		String providers = pdPreferencesDTO.getProviders();
		String prescriptions = pdPreferencesDTO.getPrescriptions();
		
		PdHousehold pdHousehold= findById(householdId);
		pdHousehold.setProviders(providers);
		pdHousehold.setPrescriptions(prescriptions);
		save(pdHousehold);
		
	}
	
	@Override
	public void setSubscriber(Long householdId, String subscriberId, InsuranceType insuranceType) {
		PdHousehold pdHousehold = findById(householdId);
		if(InsuranceType.HEALTH.equals(insuranceType)){
			pdHousehold.setHealthSubscriberId(subscriberId);
		}else{
			pdHousehold.setDentalSubscriberId(subscriberId);
		}		
		save(pdHousehold);		
	}

	@Override
	public void updateSubsidies(PdHousehold pdHousehold, Float subsidy, BigDecimal stateSubsidy, InsuranceType insuranceType)
	{		
		if(InsuranceType.HEALTH.equals(insuranceType)) {
			pdHousehold.setHealthSubsidy(subsidy);
			pdHousehold.setStateSubsidy(stateSubsidy);
		}
		else {
			pdHousehold.setDentalSubsidy(subsidy);
		}
		pdHouseholdRepository.saveAndFlush(pdHousehold);	
	}
	
	
	private void setPersonSeekCoverageDental(PdHousehold pdHousehold, List<PdPerson> persons) 
    {
       Boolean showDentalLink = Boolean.TRUE;
       String dentalPlanSelection = planDisplayUtil.getDentalConfig(pdHousehold.getCoverageStartDate());  
      
       for (PdPerson pdPerson : persons) 
       {
    	   Date covgDate = pdPerson.getDentalCoverageStartDate() == null ? pdHousehold.getCoverageStartDate() : pdPerson.getDentalCoverageStartDate();
           int age = PlanDisplayUtil.getAgeFromCoverageDate(covgDate, pdPerson.getBirthDay());
           if (showDentalLink) {    
        	   if (YorN.N.equals(pdPerson.getSeekCoverage()) && pdPerson.getDentalEnrollmentId() == null) {
		        	if (("PO".equals(dentalPlanSelection) && age < PlanDisplayConstants.CHILD_AGE) || "ON".equals(dentalPlanSelection)) {
		        		showDentalLink = Boolean.TRUE;
		        	}
	            } else {
	            	   showDentalLink = Boolean.FALSE;
	            }
           }
       }
       if (showDentalLink) {
          for (PdPerson pdPerson : persons) {
            Date covgDate = pdPerson.getDentalCoverageStartDate() == null ? pdHousehold.getCoverageStartDate() : pdPerson.getDentalCoverageStartDate();
            int age = PlanDisplayUtil.getAgeFromCoverageDate(covgDate, pdPerson.getBirthDay());
            if (("PO".equals(dentalPlanSelection) && age < PlanDisplayConstants.CHILD_AGE) || "ON".equals(dentalPlanSelection)) {
            	pdPerson.setSeekCoverage(YorN.Y);
            }
          }
       }
    }
	
	@Transactional
	@Override
	public PdHousehold saveApplicantEligibilityDetails(ApplicantEligibilityData inputData) {
		PdHousehold pdHousehold= formHouseholdFromEligibilityService(inputData);
		pdPreferencesService.savePdPreferencesByEligLeadId(Long.valueOf(inputData.getEligLeadId()));
		pdHousehold =save(pdHousehold);
		
		List<PdPerson> personList = formPersonListFromEligibilityService(inputData, pdHousehold);
		pdPersonService.savePerson(personList);
		
		return pdHousehold;
	}
	
	private PdHousehold formHouseholdFromEligibilityService(ApplicantEligibilityData inputData){
		ApplicantEligibilityResponseType applicantEligibilityResponse =  inputData.getApplicantEligibilityResponse();		

		CostSharing csr= planDisplayUtil.getCostSharingFromFFM(applicantEligibilityResponse.getInsuranceApplication());
		Date coverageStartDate = planDisplayUtil.getCoverageStartDateFromFFM(applicantEligibilityResponse.getInsuranceApplication());
		Float monthlyAptc = planDisplayUtil.calculateMonthlyAptcFromFFM(applicantEligibilityResponse.getInsuranceApplication());
		
		String exchangeAssignedId = inputData.getfFMAssignedConsumerId();
		
		PdHousehold pdHousehold = new PdHousehold();
		pdHousehold.setCostSharing(csr);
		pdHousehold.setExternalId(exchangeAssignedId);
		pdHousehold.setCoverageStartDate(coverageStartDate);	
		pdHousehold.setEnrollmentType(EnrollmentType.I);
		pdHousehold.setMaxSubsidy(monthlyAptc);
		pdHousehold.setShoppingType(ShoppingType.INDIVIDUAL);
		pdHousehold.setRequestType(RequestType.FFM);		
		pdHousehold.setStateExchangeCode(applicantEligibilityResponse.getPartnerWebSiteInformationExchangeSystem().getInformationExchangeSystemStateCode().getValue().value());
		pdHousehold.setEligLeadId(Long.parseLong(inputData.getEligLeadId()));
		Long giWsPayloadId = inputData.getGiWsPayloadId()!=null?inputData.getGiWsPayloadId().longValue():null;
		pdHousehold.setGiWsPayloadId(giWsPayloadId);
		pdHousehold.setApplicationId(Long.parseLong(inputData.getSsapApplicationId()));
		return pdHousehold;		
	}
	
	private List<PdPerson> formPersonListFromEligibilityService(ApplicantEligibilityData inputData, PdHousehold pdHousehold){
		List<PdPerson> personList = new ArrayList<PdPerson>();
		
		InsuranceApplicationType insuranceApplication = inputData.getApplicantEligibilityResponse().getInsuranceApplication();
		Map<String,String> relationMap = planDisplayUtil.getRelationship(inputData.getApplicantEligibilityResponse().getSSFPrimaryContact());
		List<JAXBElement<? extends InsuranceApplicantType>> insuranceApplicantList = insuranceApplication.getInsuranceApplicant();
		for(JAXBElement<? extends InsuranceApplicantType> insuranceApplicantElement:insuranceApplicantList){
			if(insuranceApplicantElement.getValue()!=null){
				com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType insuranceApplicant = (com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType) insuranceApplicantElement.getValue();
				boolean isEligible = true;
				InitialEnrollmentPeriodEligibilityType initialEnrollmentPeriodEligibility = insuranceApplicant.getInitialEnrollmentPeriodEligibility();
				
				if(!initialEnrollmentPeriodEligibility.getEligibilityIndicator().isValue()){
					SpecialEnrollmentPeriodEligibilityType specialEnrollmentPeriodEligibility = insuranceApplicant.getSpecialEnrollmentPeriodEligibility();
					if(!specialEnrollmentPeriodEligibility.getEligibilityIndicator().isValue()){
						isEligible = false;
					}	
				}
				
				if(isEligible){
					ProgramEligibilitySnapshotType activeProgramEligibilitySnapshot = planDisplayUtil.getActiveEligibility(insuranceApplicant);
					if(activeProgramEligibilitySnapshot!=null){
						String eligibilityVal = planDisplayUtil.checkEligibility(activeProgramEligibilitySnapshot);
						if ("true".equals(eligibilityVal)) {
							Map<String, Object> personData = planDisplayUtil.getPersonDataFromFFM(insuranceApplicant, null);
							PdPerson pdPerson = new PdPerson();
							
							String memberId = (String) personData.get("memberId");
							pdPerson.setExternalId(memberId);
							
							FFMPerson personContactInfo = (FFMPerson) personData.get("personContactInfoMap");
							String countyCode = null;
							String zipCode = null;
							if(personContactInfo!=null){
								countyCode = personContactInfo.getCountyCode();
								zipCode = personContactInfo.getZipCode();
								pdPerson.setZipCode(zipCode);
								pdPerson.setCountyCode(countyCode);
							}							
							
							String dob = (String)personData.get("dob");
							Date dateOfBirth = dob!=null?DateUtil.StringToDate(dob,PlanDisplayConstants.DOB_DATE_FORMAT_REQUIRED):null;											
							pdPerson.setBirthDay(dateOfBirth);
							
							String firstName = (String)personData.get("firstName");
							pdPerson.setFirstName(firstName);
							
							String lastName = (String)personData.get("lastName");
							pdPerson.setLastName(lastName);
							String relationShipCode = relationMap.get(memberId);
							Relationship relationship = Relationship.valueOf(PlanDisplayConstants.relationships.get(relationShipCode).toUpperCase());
							pdPerson.setRelationship(relationship);
							
							String sexCode = (String) personData.get("sexCode");
							Gender gender = Gender.valueOf(sexCode.toUpperCase());
							pdPerson.setGender(gender);
							
							pdPerson.setCatastrophicEligible(YorN.N);
							pdPerson.setSeekCoverage(YorN.Y);
							pdPerson.setPdHousehold(pdHousehold);
							
							personList.add(pdPerson);
						}
					}	
				}
			}
		}
		return personList;		
	}
	
	@Override
	public PdHousehold getHouseholdByApplicationId(Long applicationId, String requestType) {
		List<PdHousehold> householdList = pdHouseholdRepository.getHouseholdByApplicationId(applicationId);
		return planDisplayUtil.getLatestHousehold(householdList, requestType);
	}

	@Override
	public List<PdHousehold> findPdHouseholdsByLeadId(String leadId) {
		return pdHouseholdRepository.findPreShoppingHousehold(Long.parseLong(leadId));
	}	
	
	@Override
	public void updateCostSharing(Long householdId, CostSharing costSharing) {
		PdHousehold pdHousehold = findById(householdId);
		pdHousehold.setCostSharing(costSharing);
		save(pdHousehold);		
	}
}
