package com.getinsured.plandisplay.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;
import com.getinsured.hix.model.PdOrderItem;
import com.getinsured.hix.model.PdOrderItem.Status;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanRateBenefit;
import com.getinsured.hix.model.plandisplay.PdSaveCartRequest;

public interface PdOrderItemService {
	List<Long> getOrderItemIdsByHouseholdId(Long householdId);
	List<PdOrderItem> findByHouseholdId(Long householdId);
	List<PdOrderItem> findByApplicationId(Long applicationId);
	PdOrderItem findById(Long id);
	Plan getPlanInfoByPlanId(Long planId);
	List<Long> saveItem(PdSaveCartRequest pdSaveCartRequest);
	PdOrderItem save(PdOrderItem pdOrderItem);
	boolean deleteItem(Long itemId);
	Map<Long, Double> getComparisonCosts(List<PlanRateBenefit> plans, Map<String, Integer> personalUsageForMedicalMap, Map<String, Integer> personalUsageForDrugsMap, PdPreferencesDTO pdPreferencesDTO, String coverageStartDate, String currentHiosId);
	boolean updateOrderItemStatus(Long itemId ,Status status);
	List<PdOrderItem> findAllOrderItemIdsByHouseholdId(Long householdId);
	Plan getPlanInfoByCmsPlanId(String hiosPlanId, String effectiveDate);
	String findRatingArea(Map<String, String> inputData);
}
