package com.getinsured.plandisplay.service.providerSearch;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import com.getinsured.hix.dto.plandisplay.ProviderSearchRequest;
import com.getinsured.hix.model.IndividualPlan;
import com.getinsured.hix.model.plandisplay.ProviderBean;

public interface ProviderSearch {
	
	String getSearchType();
	
	Map<String, Future<Map<String, Object>>> prepareProviderHiosMap(List<ProviderBean> providers);
	
	String getNetworkStatus(Map<String, Future<Map<String, Object>>> futureProviderResponseListMap, IndividualPlan individualPlan, ProviderBean providerBean, List<Map<String, String>> providersInPlan);
	
	Map<String,Object> searchProvider(ProviderSearchRequest providerSearchRequest);
}
