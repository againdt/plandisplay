package com.getinsured.plandisplay.service;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.dto.planmgmt.RatingAreaRequestDTO;
import com.getinsured.hix.dto.planmgmt.RatingAreaResponseDTO;
import com.getinsured.hix.model.PdHousehold;
import com.getinsured.hix.model.PdOrderItem;
import com.getinsured.hix.model.PdOrderItem.Status;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanDental;
import com.getinsured.hix.model.PlanHealth;
import com.getinsured.hix.model.PlanRateBenefit;
import com.getinsured.hix.model.plandisplay.PdBundlePlanData;
import com.getinsured.hix.model.plandisplay.PdSaveCartRequest;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ExchangeType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.MedicalCondition;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ShoppingType;
import com.getinsured.hix.model.plandisplay.PlanPreferences;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.plandisplay.model.costcalculator.PlanCost;
import com.getinsured.plandisplay.repository.PdOrderItemRepository;
import com.getinsured.plandisplay.service.rules.CostCalculator;
import com.getinsured.plandisplay.util.PlanDisplayUtil;
import com.google.gson.Gson;

@Service("pdOrderItemService")
public class PdOrderItemServiceImpl implements PdOrderItemService {
	private static final Logger LOGGER = LoggerFactory.getLogger(PdOrderItemServiceImpl.class);
	
	@Autowired private PdOrderItemRepository pdOrderItemRepository;
	@Autowired private PdHouseholdService pdHouseholdService;
	@Autowired private PdOrderItemPersonService pdOrderItemPersonService;
	@Autowired private ApplicationContext appContext;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private Gson platformGson;

	
	@Override
	public List<Long> getOrderItemIdsByHouseholdId(Long householdId) {
		return pdOrderItemRepository.findOrderItemIdsByHouseholdId(householdId);
	}
	
	@Override
	public List<PdOrderItem> findByHouseholdId(Long householdId) {
		return pdOrderItemRepository.findOrderItemsByHouseholdId(householdId);
	}
	
	@Override
	public List<PdOrderItem> findByApplicationId(Long applicationId) {
		return pdOrderItemRepository.findOrderItemsByApplicationId(applicationId);
	}
	
	@Override
	public PdOrderItem findById(Long id) {
		if(pdOrderItemRepository.exists(id)){
			return pdOrderItemRepository.findOne(id);
		}
		return null;
	}
	
	@Override
	@Transactional
	public PdOrderItem save(PdOrderItem pdOrderItem) {
		return pdOrderItemRepository.saveAndFlush(pdOrderItem);
	}
	
	@Override
	public Plan getPlanInfoByPlanId(Long planId) {
		Plan plan = new Plan();		
		try {
			PlanRequest planRequest= new PlanRequest();
			planRequest.setMinimizePlanData(true);
			planRequest.setPlanId(planId.toString());
			String planDataResponse = ghixRestTemplate.postForObject(GhixEndPoints.PlanMgmtEndPoints.GET_PLAN_DATA_BY_ID_URL, planRequest, String.class);
			PlanResponse planResponse = platformGson.fromJson(planDataResponse, PlanResponse.class);
			plan.setId(planId.intValue());
			if(planResponse!=null){
				plan.setName(planResponse.getPlanName());
				plan.setInsuranceType(planResponse.getInsuranceType());
				if(!Plan.PlanInsuranceType.STM.toString().equals(plan.getInsuranceType()) && !Plan.PlanInsuranceType.AME.toString().equals(plan.getInsuranceType())  && !Plan.PlanInsuranceType.VISION.toString().equals(plan.getInsuranceType())){
					if(plan.getInsuranceType().equals(InsuranceType.DENTAL.toString())){
						PlanDental planDental = new PlanDental();
						planDental.setCostSharing(planResponse.getCostSharing());
						plan.setPlanDental(planDental);
					}else{
						PlanHealth planHealth = new PlanHealth();
						planHealth.setCostSharing(planResponse.getCostSharing());
						plan.setPlanHealth(planHealth);
					}
				}
			}			
		}
		catch(Exception e) {			
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		return plan;
	}
	
	@Override
	public Plan getPlanInfoByCmsPlanId(String hiosPlanId, String effectiveDate) {
		Plan plan = new Plan();
				
		try {
			PlanRequest planRequest= new PlanRequest();
			planRequest.setHiosPlanId(hiosPlanId);
			planRequest.setEffectiveDate(effectiveDate);
			String planDataResponse = ghixRestTemplate.postForObject(GhixEndPoints.PlanMgmtEndPoints.GET_PLAN_INFO_BY_HIOS_PLAN_ID, planRequest, String.class);
			PlanResponse planResponse = platformGson.fromJson(planDataResponse, PlanResponse.class);
			if(planResponse!=null && GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(planResponse.getStatus())){
				plan.setId(planResponse.getPlanId());
				plan.setName(planResponse.getPlanName());
				plan.setInsuranceType(planResponse.getInsuranceType());
				if(planResponse.getPlanLevel()!=null){
					if("HEALTH".equalsIgnoreCase(planResponse.getInsuranceType()))
					{
						PlanHealth planHealth = new PlanHealth();
						planHealth.setPlanLevel(planResponse.getPlanLevel());
						plan.setPlanHealth(planHealth);
					}
					else if("DENTAL".equalsIgnoreCase(planResponse.getInsuranceType()))
					{
						PlanDental planDental = new PlanDental();
						planDental.setPlanLevel(planResponse.getPlanLevel());
						plan.setPlanDental(planDental);
					}
				}				
			}			
		}
		catch(Exception e) {			
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		
		return plan;
	}

	@Override
	@Transactional
	public List<Long> saveItem(PdSaveCartRequest pdSaveCartRequest) 
	{		
		List<Long> pdOrderItemIds = new ArrayList<Long>();
		List<PdOrderItem> existingPdOrderItems = pdOrderItemRepository.findOrderItemsByHouseholdId(pdSaveCartRequest.getHouseholdId());
		boolean planTypeAlreadyAdded = false;
		PdOrderItem newPdOrderItem = null;
 	 	if(existingPdOrderItems != null && !existingPdOrderItems.isEmpty()) {
            for(PdOrderItem existingPdOrderItem : existingPdOrderItems) {
            	newPdOrderItem = existingPdOrderItem;
            	InsuranceType insuranceType = existingPdOrderItem.getInsuranceType();
                if(insuranceType.equals(pdSaveCartRequest.getInsuranceType())) {
                	planTypeAlreadyAdded = true;
                }
            }
 	 	} 	 	
 	 	
 	 	boolean isPHIXProfile = "PHIX".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE));
		if(isPHIXProfile || !planTypeAlreadyAdded) 
		{
			PdHousehold pdHousehold = pdHouseholdService.findById(pdSaveCartRequest.getHouseholdId());
			Float monthlySubsidy = pdSaveCartRequest.getMonthlySubsidy();
			if(ShoppingType.INDIVIDUAL.equals(pdHousehold.getShoppingType()) && pdHousehold.getMaxSubsidy() == null)
			{
				monthlySubsidy = null;
			}

			BigDecimal monthlyStateSubsidy = pdSaveCartRequest.getMonthlyStateSubsidy();

			newPdOrderItem = createPdOrderItem(pdSaveCartRequest.getPlanId(), pdSaveCartRequest.getPremiumAfterCredit(), pdSaveCartRequest.getPremiumBeforeCredit(), monthlySubsidy, monthlyStateSubsidy, pdSaveCartRequest.getInsuranceType(), pdSaveCartRequest.getSsapApplicationId(), pdSaveCartRequest.getHouseholdId(), pdSaveCartRequest.getExchangeType() ,pdSaveCartRequest.getPlanPreferences());
 	 		pdOrderItemPersonService.saveItemPerson(newPdOrderItem, pdSaveCartRequest); 	 	 	
 	 	 	pdHouseholdService.updateSubsidies(pdHousehold, monthlySubsidy, monthlyStateSubsidy, pdSaveCartRequest.getInsuranceType());
 	 	 	
 	 	 	List<PdBundlePlanData> pdBundlePlanDataList = pdSaveCartRequest.getPdBundlePlanDataList();
 			if(pdBundlePlanDataList!=null && !pdBundlePlanDataList.isEmpty()) {
 				//List<Map<String,String>> premiumData = new ArrayList<Map<String,String>>();
 				for(PdBundlePlanData pdBundlePlanData:pdBundlePlanDataList) {
 					InsuranceType planType = InsuranceType.valueOf(pdBundlePlanData.getPlanType());
 					if(planType.equals(InsuranceType.AME)) {
 						PdOrderItem newAMEPdOrderItem = createPdOrderItem(pdBundlePlanData.getPlanId().longValue(), pdBundlePlanData.getPremium(), pdBundlePlanData.getPremium(), 0.0f, null, InsuranceType.AME, pdBundlePlanData.getSsapApplicationId(), pdSaveCartRequest.getHouseholdId(), ExchangeType.OFF,pdSaveCartRequest.getPlanPreferences());
 						pdOrderItemIds.add(newAMEPdOrderItem.getId());
 						//pdSaveCartRequest.setPremiumData(premiumData);
 						pdSaveCartRequest.setInsuranceType(planType);
 						pdOrderItemPersonService.saveItemPerson(newAMEPdOrderItem, pdSaveCartRequest);
 					}
 					if(planType.equals(InsuranceType.DENTAL)) {
 						PdOrderItem newDENTALPdOrderItem = createPdOrderItem(pdBundlePlanData.getPlanId().longValue(), pdBundlePlanData.getPremium(), pdBundlePlanData.getPremium(), 0.0f, null, InsuranceType.DENTAL, pdBundlePlanData.getSsapApplicationId(), pdSaveCartRequest.getHouseholdId(), ExchangeType.OFF,pdSaveCartRequest.getPlanPreferences());
 						pdOrderItemIds.add(newDENTALPdOrderItem.getId());
 						//pdSaveCartRequest.setPremiumData(premiumData);
 						pdSaveCartRequest.setInsuranceType(planType);
 						pdOrderItemPersonService.saveItemPerson(newDENTALPdOrderItem, pdSaveCartRequest);
 					}
 					if(planType.equals(InsuranceType.VISION)) {
 						PdOrderItem newVISIONPdOrderItem = createPdOrderItem(pdBundlePlanData.getPlanId().longValue(), pdBundlePlanData.getPremium(), pdBundlePlanData.getPremium(), 0.0f, null, InsuranceType.VISION, pdBundlePlanData.getSsapApplicationId(), pdSaveCartRequest.getHouseholdId(), ExchangeType.OFF ,pdSaveCartRequest.getPlanPreferences());
 						pdOrderItemIds.add(newVISIONPdOrderItem.getId());
 						//pdSaveCartRequest.setPremiumData(premiumData);
 						pdSaveCartRequest.setInsuranceType(planType);
 						pdOrderItemPersonService.saveItemPerson(newVISIONPdOrderItem, pdSaveCartRequest);
 					}
 				}
 			}	
 	 	}
		pdOrderItemIds.add(0, newPdOrderItem.getId());
		return pdOrderItemIds;
	}
	
	private PdOrderItem createPdOrderItem(Long planId, Float premiumAfterCredit, Float premiumBeforeCredit, Float monthlySubsidy, BigDecimal monthlyStateSubsidy, InsuranceType insuranceType, Long ssapApplicationId, Long pdHouseholdId, ExchangeType exchangeType, PlanPreferences planPreferences) {
		PdOrderItem pdOrderItem = new PdOrderItem();
 		pdOrderItem.setPlanId(planId);
 		if(premiumAfterCredit != null){
 			premiumAfterCredit = new BigDecimal(Float.toString(premiumAfterCredit)).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue();
 		}
		pdOrderItem.setPremiumAfterCredit(premiumAfterCredit);
		
		if(premiumBeforeCredit != null){
 		 	premiumBeforeCredit = new BigDecimal(Float.toString(premiumBeforeCredit)).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue();
 		}
		pdOrderItem.setPremiumBeforeCredit(premiumBeforeCredit);
		
		if(monthlySubsidy != null){
			monthlySubsidy = new BigDecimal(Float.toString(monthlySubsidy)).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue();
 		}
		pdOrderItem.setMonthlySubsidy(monthlySubsidy);

		if(monthlyStateSubsidy != null) {
			pdOrderItem.setMonthlyStateSubsidy(monthlyStateSubsidy.setScale(2,BigDecimal.ROUND_HALF_UP));
		}
		
		pdOrderItem.setInsuranceType(insuranceType); 		
		PdHousehold pdHousehold = new PdHousehold();
		pdHousehold.setId(pdHouseholdId);
		pdOrderItem.setPdHousehold(pdHousehold);
		pdOrderItem.setSsapApplicationId(ssapApplicationId);	
		pdOrderItem.setStatus(Status.ACTIVE);
		pdOrderItem.setExchangeType(exchangeType);
		if (planPreferences != null && InsuranceType.HEALTH.equals(insuranceType))
		{
			pdOrderItem.setPreferences(platformGson.toJson(planPreferences));
		}
		return pdOrderItemRepository.saveAndFlush(pdOrderItem);				
	}
	
	@Override
	@Transactional
	public boolean deleteItem(Long itemId) {
		boolean isItemDelete = false;
		PdOrderItem pdOrderItem = pdOrderItemRepository.findOne(itemId);
		if(pdOrderItem != null) {	
			PdHousehold pdHousehold = pdOrderItem.getPdHousehold();
			if(pdOrderItem.getInsuranceType() != null) {
				if(InsuranceType.HEALTH.equals(pdOrderItem.getInsuranceType())) {
					pdHousehold.setHealthSubsidy(null);
					pdHousehold.setStateSubsidy(null);
					pdHousehold.setDentalSubsidy(null);
				} else {
					pdHousehold.setDentalSubsidy(null);
				}
			}
			pdHouseholdService.save(pdHousehold);			
			//pdOrderItemPersonService.deleteByOrderItemId(itemId);
			//pdOrderItemRepository.delete(pdOrderItem);
			isItemDelete = updateOrderItemStatus(itemId, Status.DELETED);
		}
		return isItemDelete;
	}
	
	@Override
	public boolean updateOrderItemStatus(Long itemId, Status status) {
		boolean isDelete = false;
		int noOfRecordsUpdated = pdOrderItemRepository.updateOrderItemStatus(itemId, status);
		if(noOfRecordsUpdated >0 ){
			isDelete = true;
		}
		return isDelete;
	}
	
	@Override
	public List<PdOrderItem> findAllOrderItemIdsByHouseholdId(Long householdId) {
		return pdOrderItemRepository.findAllOrderItemsByHouseholdId(householdId);
	}
	
	private PlanCost getPlanCostFromMetadata(String stateCode, PdPreferencesDTO pdPreferencesDTO, String coverageStrStartDate, boolean forCurrentPlan){
		
			PlanCost planCost = new PlanCost();
			InputStream inputStreamUrl = null;
			JAXBContext context = null;
			
			stateCode = stateCode.trim();
			
			// get remaining months
			int remainingMonths = PlanDisplayUtil.getRemainingMonthsForCoverage(coverageStrStartDate, stateCode);
			
	        try {
	            context = JAXBContext.newInstance(PlanCost.class);
	            Unmarshaller unMarshaller = context.createUnmarshaller();
	            if (forCurrentPlan)
	            {
	            	remainingMonths = 12;
	            }
	            Resource resource = appContext.getResource("classpath:com/getinsured/plandisplay/planCostMetaData_"+stateCode+"_" + remainingMonths + "Months.xml");	            
	            if(!resource.exists()) {
	            		resource = appContext.getResource("classpath:com/getinsured/plandisplay/planCostMetaData_"+stateCode+".xml");
	            }
	            if(!resource.exists()) {
	            		resource = appContext.getResource("classpath:com/getinsured/plandisplay/planCostMetaData.xml");
	            }

	            MedicalCondition medicalCondition = pdPreferencesDTO.getMedicalCondition();
	            if(medicalCondition != null){
	            		if(MedicalCondition.HIV.equals(medicalCondition) || MedicalCondition.DIABETES.equals(medicalCondition) || MedicalCondition.HEARTDISEASE.equals(medicalCondition)){
	            			resource = appContext.getResource("classpath:com/getinsured/plandisplay/planCostMetaData_"+medicalCondition+".xml");
	            		}
	            }
	            
	            inputStreamUrl = resource.getInputStream();
	            planCost = PlanCost.class.cast(unMarshaller.unmarshal(inputStreamUrl));
	        } catch (JAXBException | IOException e) {
	        		throw new GIRuntimeException("Failed to read the plan cost metadata",e);
	        }
	        finally {
	            try {
	            		if (inputStreamUrl != null){
	            			inputStreamUrl.close();
	            		}
	            	} catch(Exception e) {
	            		throw new GIRuntimeException("Failed to read the plan cost metadata",e);
	            }
	        }
		
		return planCost;
	}
	
	
	// Updated to avoid parsing the cost metadata every time
	@Override
	public Map<Long, Double> getComparisonCosts(List<PlanRateBenefit> plans, Map<String, Integer> personalUsageForMedicalMap, Map<String, Integer> personalUsageForDrugsMap, PdPreferencesDTO pdPreferencesDTO, String coverageStartDate, String currentHiosId){

		Map<Long, Double> planCosts = new HashMap<Long, Double>();
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		PlanCost planCost = getPlanCostFromMetadata(stateCode, pdPreferencesDTO, coverageStartDate, false);
		PlanCost currentPlanCost = null;
		boolean hasCurrentHiosId = false;
		if (!org.springframework.util.StringUtils.isEmpty(currentHiosId))
		{
			currentPlanCost = getPlanCostFromMetadata(stateCode, pdPreferencesDTO, coverageStartDate, true);
			hasCurrentHiosId = true;
		}
		
		for (PlanRateBenefit plan : plans) {
			if (plan!=null){
				String planName=plan.getName();
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "currPlanId="+plan.getId()+",planName="+planName, null, true);
				CostCalculator costCalculator = null;
				if (hasCurrentHiosId && currentHiosId.equals(plan.getHiosPlanNumber()))
				{
					costCalculator = new CostCalculator(currentPlanCost);
				}
				else
				{
					costCalculator = new CostCalculator(planCost);
				}
				Double cost = costCalculator.calculatePlanCost(plan, personalUsageForMedicalMap, personalUsageForDrugsMap, stateCode);
				if(hasCurrentHiosId && currentHiosId.equals(plan.getHiosPlanNumber()))
				{
					/*
					 * 	For the Current Plan Cost calculation
					 *	1. Divide by 12 to get the monthly cost, to get the 
					 *	2. Get the remaining Month 
					 *	3. Multiply step 2 and step 3 to get actual Cost. 
					 */
					int remainingMonths = PlanDisplayUtil.getRemainingMonthsForCoverage(coverageStartDate, stateCode);
					cost = (cost / 12) * remainingMonths;
				}
				planCosts.put(Long.parseLong(""+plan.getId()), cost);
			}
		}
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----No Of Plans to costCompareData---->"+planCosts.size(), null, false);
		return planCosts;
	}
	
	@Override
	public String findRatingArea(Map<String, String> inputData) {
		String stateCode = inputData.get("stateCode");
		String countyCode = inputData.get("countyCode");
		String response = null;
		if(StringUtils.isNotEmpty(stateCode) && StringUtils.isNotEmpty(countyCode)){		
			try {
				RatingAreaRequestDTO ratingAreaRequestDTO = new RatingAreaRequestDTO();
				ratingAreaRequestDTO.setStateCode(stateCode);
				ratingAreaRequestDTO.setCountyFips(countyCode);
				String ratingAreaResponse = ghixRestTemplate.postForObject(GhixEndPoints.PlanMgmtEndPoints.GET_RATING_AREA, ratingAreaRequestDTO, String.class);
				RatingAreaResponseDTO ratingAreaResponseDTO = platformGson.fromJson(ratingAreaResponse, RatingAreaResponseDTO.class);
				if(ratingAreaResponseDTO!=null && GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(ratingAreaResponseDTO.getStatus())){
					response = ratingAreaResponseDTO.getRatingArea(); 			
				}			
			}catch(Exception e) {			
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			}
		}
		return response;
	}
	
}
