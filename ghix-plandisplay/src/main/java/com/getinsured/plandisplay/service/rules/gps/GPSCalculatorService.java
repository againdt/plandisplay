package com.getinsured.plandisplay.service.rules.gps;

import java.util.Map;

/**
 * GPS score calculation interface
 * There are 2 sets of inputs.
 * For all the plans we send following 2 values
 * 1. total expense
 * 2. count of benefits covered by given plan
 * 
 * Returns a score [40, 98] for each plan
 * 
 * @since 12/30/2013
 * @author bhatt_s
 *
 */
public interface GPSCalculatorService {
	
	public Map<Integer, Double> calculate(Map<Integer, Double> expense, Map<Integer, Integer> benefits, Integer benefitsSelected);
	public Map<Integer, Double> calculateUsingDeductible(Map<Integer, Double> expense, Map<Integer, Double> deductible, Map<Integer, Integer> benefits, Integer benefitsSelected);
	/**
	 * We should move towards more generic definition
	 * @param expense
	 * @param deductible
	 * @param benefits
	 * @param benefitsSelected
	 * @param doctorsInNetwork
	 * @param doctorsSelected
	 * @return
	 */
	public Map<Integer, Double> calculateUsingDeductibleAndDoctorSearch(Map<Integer, Double> expense, Map<Integer, Double> deductible, Map<Integer, Integer> benefits, Integer benefitsSelected, Map<Integer, Integer> doctorsInNetwork, Integer doctorsSelected);


}
