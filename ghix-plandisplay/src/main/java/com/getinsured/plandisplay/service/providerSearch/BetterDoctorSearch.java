package com.getinsured.plandisplay.service.providerSearch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.getinsured.hix.dto.plandisplay.Provider;
import com.getinsured.hix.dto.plandisplay.ProviderAddress;
import com.getinsured.hix.dto.plandisplay.ProviderSearchRequest;
import com.getinsured.hix.model.IndividualPlan;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.model.plandisplay.ProviderBean;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.plandisplay.util.PlanDisplayAsyncUtil;
import com.getinsured.plandisplay.util.PlanDisplayUtil;

@Service(value="betterDoctor")
public class BetterDoctorSearch implements ProviderSearch {
	private static final Logger LOGGER = LoggerFactory.getLogger(BetterDoctorSearch.class);
	
	@Autowired private PlanDisplayAsyncUtil planDisplayAsyncUtil;
	@Autowired private RestTemplate restTemplate;
	@Autowired private ZipCodeService zipCodeService;
	
	@Override
	public String getSearchType() {
		return "betterDoc";
	}

	@Override
	public Map<String, Future<Map<String, Object>>> prepareProviderHiosMap(List<ProviderBean> providers){		
		Map<String, Future<Map<String, Object>>> futureProviderResponseListMap = null;		
		String betterDoctorUrl = DynamicPropertiesUtil.getPropertyValue("planSelection.BetterDoctorUrl");
		String betterDoctorUserKey = DynamicPropertiesUtil.getPropertyValue("planSelection.BetterDoctorUserKey");
		HttpHeaders headers = new HttpHeaders();
		@SuppressWarnings({ "rawtypes", "unchecked" })
		HttpEntity entity = new HttpEntity(headers);
		futureProviderResponseListMap = new HashMap<String, Future<Map<String, Object>>>();
		for (ProviderBean providerBean : providers) {
			String sourceUrl = betterDoctorUrl+"/doctors/"+providerBean.getId()+"?insurance_program_type=QHP&user_key="+betterDoctorUserKey;
			if("hospital".equalsIgnoreCase(providerBean.getProviderType())){
				sourceUrl = betterDoctorUrl+"/practices/"+providerBean.getId()+"?insurance_program_type=QHP&fields=default,insurances&user_key="+betterDoctorUserKey;
			}
			Future<Map<String, Object>> futureProviderResponse = planDisplayAsyncUtil.callProviderAPIAsync(sourceUrl, entity);
			futureProviderResponseListMap.put(providerBean.getId(), futureProviderResponse);
		}	
		return futureProviderResponseListMap;
	}
	
	@Override
	public String getNetworkStatus(Map<String, Future<Map<String, Object>>> futureProviderResponseListMap, IndividualPlan individualPlan, ProviderBean providerBean, List<Map<String, String>> providersInPlan) {
		if(futureProviderResponseListMap.containsKey(providerBean.getId())){
			Future<Map<String, Object>> futureProviderResponse = futureProviderResponseListMap.get(providerBean.getId());
			Map<String, Object> providerResponse = null;
			try {
				providerResponse = futureProviderResponse.get();
			} catch (InterruptedException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			} catch (ExecutionException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			}
			String providerAvailabilityKey = individualPlan.getNetworkName()+"-"+individualPlan.getApplicableYear();
			return parseResponse(providerResponse, providerBean, providerAvailabilityKey);
		}		
		return PlanDisplayUtil.OUT_NETWORK;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Object> searchProvider(ProviderSearchRequest providerSearchRequest) {
		ZipCode userLocation = null;
		String searchKey = providerSearchRequest.getSearch_term()!=null ? providerSearchRequest.getSearch_term() : "";
		int currentPageSize = providerSearchRequest.getPer_page();
		String searchType = providerSearchRequest.getType();
		Float radius = providerSearchRequest.getRadius();
		
		try{
			userLocation = getUserLocation((String) providerSearchRequest.getZip_code());
		}catch(Exception ex){
			throw new GIRuntimeException("SOLRZIP",ex.getCause(), ex.getMessage(), GIRuntimeException.Severity.HIGH);
		}
		
		Map<String,Object> responseData = new HashMap<String,Object>();
		
		responseData.put(PlanDisplayUtil.STATUS, GhixConstants.RESPONSE_SUCCESS);
		JSONArray providers = new JSONArray();
		if(PlanDisplayUtil.DOCTOR.equalsIgnoreCase(searchType)){
			providers = callBetterDoctorDoctorSearch(searchKey, userLocation, currentPageSize, radius);
		}
		if(PlanDisplayUtil.HOSPITAL.equalsIgnoreCase(searchType)){
			providers = callBetterDoctorHospitalSearch(searchKey, userLocation, currentPageSize, radius);
		}
		
		providers.sort(new Comparator<Provider>() {
			public int compare(Provider a, Provider b) {
				if(a.getProviderAddress().get(0).getProximity() < b.getProviderAddress().get(0).getProximity()) {
					return -1;
				}else if(a.getProviderAddress().get(0).getProximity() > b.getProviderAddress().get(0).getProximity()) {
					return 1;
				}
				return 0;
			}
		});
		
		JSONArray providersNew = new JSONArray();
		if(providers != null) {
			currentPageSize = providers.size() > currentPageSize ? currentPageSize : providers.size();
			for (int i=0; i<currentPageSize; i++) {
				providersNew.add(providers.get(i));	
			}
		}
			
		responseData.put("providers", providersNew);
		return responseData;
	}
	
	private String parseResponse(Map<String, Object> providerResponse, ProviderBean providerBean, String providerAvailabilityKey){
		if(providerResponse!=null && providerResponse.get("data") != null){
			@SuppressWarnings("unchecked")
			Map<String, Object> betterDoctorProvider = (Map<String, Object>) providerResponse.get("data");
			if("hospital".equalsIgnoreCase(providerBean.getProviderType())){
				return findPlanNetworkName(betterDoctorProvider, providerAvailabilityKey);
			}else{
				if(betterDoctorProvider != null && betterDoctorProvider.get("practices") != null){
					@SuppressWarnings("unchecked")
					List<Map<String, Object>> practices = (List<Map<String, Object>>)betterDoctorProvider.get("practices");
					for(Map<String, Object> practice : practices){
						String practiceUid = (String)practice.get("uid");
						if(practiceUid != null && practiceUid.equalsIgnoreCase(providerBean.getLocationId())){
							return findPlanNetworkName(practice, providerAvailabilityKey);
						}
					}
				}
			}
		}
		return PlanDisplayUtil.OUT_NETWORK;
	}
	
	private String findPlanNetworkName(Map<String, Object> betterDoctorProvider, String providerAvailabilityKey){
		if(betterDoctorProvider != null && betterDoctorProvider.get("insurance_uids") != null && betterDoctorProvider.get("insurances") != null){
			List<String> insuranceUidList = (List<String>)betterDoctorProvider.get("insurance_uids");
			for(String insuranceUid : insuranceUidList){
				if(StringUtils.isNotBlank(insuranceUid)){
					if(insuranceUid.length() > 5){
						String networkUidYear = insuranceUid.substring(insuranceUid.length() - 4);
						String coverageYear = providerAvailabilityKey.substring(providerAvailabilityKey.length() - 4);
						if(networkUidYear.equalsIgnoreCase(coverageYear)){
							List<Map<String,Object>> insurances = (List<Map<String,Object>>)betterDoctorProvider.get("insurances");
							for(Map<String,Object> insuranceMap : insurances){
								if(insuranceMap != null && insuranceMap.get("insurance_plan") != null){
									Map<String,Object> insurancePlan = (Map<String,Object>)insuranceMap.get("insurance_plan");
									String uid = (String) insurancePlan.get("uid");
									if(StringUtils.isNotBlank(uid) && uid.equalsIgnoreCase(insuranceUid)){
										String betterDoctorNetworkName = (String) insurancePlan.get("name");
										if(providerAvailabilityKey.equalsIgnoreCase(betterDoctorNetworkName+"-"+networkUidYear)){
											return PlanDisplayUtil.IN_NETWORK;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return PlanDisplayUtil.OUT_NETWORK;
	}
	
	private JSONArray callBetterDoctorHospitalSearch(String searchKey, ZipCode userLocation, int currentPageSize, Float radius) {
		JSONArray providers = new JSONArray();
		String betterDoctorUrl = DynamicPropertiesUtil.getPropertyValue("planSelection.BetterDoctorUrl");
		String betterDoctorUserKey = DynamicPropertiesUtil.getPropertyValue("planSelection.BetterDoctorUserKey");
		String sourceUrl = betterDoctorUrl+"/practices?insurance_program_type=QHP&name="+searchKey+"&location="+userLocation.getLat()+","+userLocation.getLon()+","+radius+"&user_location="+userLocation.getLat()+","+userLocation.getLon()+"&sort=distance-asc&skip=0&limit="+currentPageSize+"&user_key="+betterDoctorUserKey;
		
		HttpHeaders headers = new HttpHeaders();
		HttpEntity entity = new HttpEntity(headers);
		HttpEntity<String> responseEntity =  restTemplate.exchange(sourceUrl, HttpMethod.GET, entity, String.class);
		String response = responseEntity.getBody();
		
		Map<String, Object> providerResponse = null;
		try {
			ObjectReader objreader = JacksonUtils.getJacksonObjectReaderForHashMap(String.class, Object.class);
			providerResponse = objreader.readValue(response);
		} catch (JsonParseException |JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		
		List<Map<String, Object>> betterDoctorProviderList = (List<Map<String, Object>>) providerResponse.get("data");
		for(Map<String, Object> betterDoctorProviderObj : betterDoctorProviderList){
			Provider provider = new Provider();
			provider.setStrenuus_id(String.valueOf(betterDoctorProviderObj.get("uid")));
			provider.setName((String) betterDoctorProviderObj.get("name"));
			provider.setSpecialty("");
			provider.setLocationId(provider.getStrenuus_id());
			
			Map<String, String> location = (Map<String, String>)betterDoctorProviderObj.get("visit_address");
			List<ProviderAddress> providerAddressList = new ArrayList<ProviderAddress>();
			ProviderAddress providerAddress = new ProviderAddress((String) location.get("street"));
			providerAddress.setAddLine2((String) location.get("street2"));
			providerAddress.setCity((String) location.get("city"));
			providerAddress.setZip((String) location.get("zip"));
			providerAddress.setState((String) location.get("state"));
			Double distance = (Double) betterDoctorProviderObj.get("distance");
			providerAddress.setProximity(distance!= null ? distance.floatValue() : 0);
			providerAddressList.add(providerAddress);
			provider.setProviderAddress(providerAddressList);
			
			providers.add(provider);
		}
		return providers;
	}

	private JSONArray callBetterDoctorDoctorSearch(String searchKey, ZipCode userLocation, int currentPageSize, Float radius) {
		JSONArray providers = new JSONArray();
		String betterDoctorUrl = DynamicPropertiesUtil.getPropertyValue("planSelection.BetterDoctorUrl");
		String betterDoctorUserKey = DynamicPropertiesUtil.getPropertyValue("planSelection.BetterDoctorUserKey");
		String sourceUrl = betterDoctorUrl+"/doctors?insurance_program_type=QHP&name="+searchKey+"&location="+userLocation.getLat()+","+userLocation.getLon()+","+radius+"&user_location="+userLocation.getLat()+","+userLocation.getLon()+"&sort=distance-asc&skip=0&limit="+currentPageSize+"&user_key="+betterDoctorUserKey;
		
		HttpHeaders headers = new HttpHeaders();
		HttpEntity entity = new HttpEntity(headers);
		HttpEntity<String> responseEntity =  restTemplate.exchange(sourceUrl, HttpMethod.GET, entity, String.class);
		String response = responseEntity.getBody();
		
		Map<String, Object> providerResponse = null;
		try {
			ObjectReader objreader = JacksonUtils.getJacksonObjectReaderForHashMap(String.class, Object.class);
			providerResponse = objreader.readValue(response);
		} catch (JsonParseException |JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		List<Map<String, Object>> betterDoctorProviderList = (List<Map<String, Object>>) providerResponse.get("data");
		for(Map<String, Object> betterDoctorProviderObj : betterDoctorProviderList){
			List<Map<String, Object>> locations = (List<Map<String, Object>>)betterDoctorProviderObj.get("practices");
			for(Map<String, Object> locationObj : locations){
				Provider provider = new Provider();
				provider.setStrenuus_id(String.valueOf(betterDoctorProviderObj.get("uid")));
				Map<String, Object> docProfile = (Map<String, Object>)betterDoctorProviderObj.get("profile");
				provider.setName((String) docProfile.get("first_name") +" "+ (String) docProfile.get("last_name"));
				
				List<Map<String, String>> specialtiesList = (List<Map<String, String>>)betterDoctorProviderObj.get("specialties");
				if(specialtiesList != null && specialtiesList.size() > 0){
					Map<String, String> specialty = specialtiesList.get(0);
					provider.setSpecialty(specialty.get("name"));
				}
				
				provider.setLocationId(String.valueOf(locationObj.get("uid")));
						
				Map<String, String> location = (Map<String, String>)locationObj.get("visit_address");
				List<ProviderAddress> providerAddressList = new ArrayList<ProviderAddress>();
				ProviderAddress providerAddress = new ProviderAddress((String) location.get("street"));
				providerAddress.setAddLine2((String) location.get("street2"));
				providerAddress.setCity((String) location.get("city"));
				providerAddress.setZip((String) location.get("zip"));
				providerAddress.setState((String) location.get("state"));
				Double distance = (Double) locationObj.get("distance");
				providerAddress.setProximity(distance!= null ? distance.floatValue() : 0);
				providerAddressList.add(providerAddress);
				provider.setProviderAddress(providerAddressList);
				
				providers.add(provider);
			}
		}
		return providers;
	}
	
	private ZipCode getUserLocation(String userzip) throws Exception{
		ZipCode zip = null;
		List<ZipCode> userLocationList = null;
		
		if(userzip != null){
			userLocationList = zipCodeService.findByZip(userzip);
			if(userLocationList != null && userLocationList.size() > 0)
				zip = userLocationList.get(0);
			else 
				throw new Exception("Failed to retrieve user's location using zip code:" + userzip);
			
		}else{
			throw new Exception("User Zipcode is NULL");
		}
		
		return zip;
	}	
}
