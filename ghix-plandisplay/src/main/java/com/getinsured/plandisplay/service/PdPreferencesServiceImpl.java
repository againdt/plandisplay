package com.getinsured.plandisplay.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.PdPreferences;
import com.getinsured.plandisplay.repository.PdPreferencesRepository;
import com.getinsured.plandisplay.util.PlanDisplayUtil;

@Service("pdPreferencesService")
public class PdPreferencesServiceImpl implements PdPreferencesService{
		
	@Autowired private PdPreferencesRepository pdPreferencesRepository;

	@Override
	public PdPreferences findPreferencesByPdHouseholdId(Long pdHouseholdId) {
		return pdPreferencesRepository.findPreferencesByPdHouseholdId(pdHouseholdId);
	}

	@Override
	public PdPreferences findPreferencesByEligLeadId(Long eligLeadId) {
		PdPreferences pdPreferences = null;
		List<PdPreferences> PdPreferencesList = pdPreferencesRepository.findPreferencesByEligLeadId(eligLeadId);
		if(PdPreferencesList!=null && !PdPreferencesList.isEmpty()){
			pdPreferences = PdPreferencesList.get(0);
		}		
		return pdPreferences;
	}

	@Override
	public void savePdPreferencesByEligLeadId(Long eligLeadId) {
		PdPreferences pdPreferences = findPreferencesByEligLeadId(eligLeadId);
		if(pdPreferences==null){
			pdPreferences = new PdPreferences();
			String defaultPreferences = PlanDisplayUtil.getDefaultPref();			
			pdPreferences.setEligLeadId(eligLeadId);
			pdPreferences.setPreferences(defaultPreferences);	
		}			
		save(pdPreferences);
	}

	@Override
	public void savePdPreferencesByHouseholdId(PdPreferences pdPreferences, Long pdHouseholdId) {
		if(pdPreferences==null){
			pdPreferences = new PdPreferences();
			String defaultPreferences = PlanDisplayUtil.getDefaultPref();			
			pdPreferences.setPdHouseholdId(pdHouseholdId);
			pdPreferences.setPreferences(defaultPreferences);	
		}			
		save(pdPreferences);		
	}

	@Override
	@Transactional
	public PdPreferences save(PdPreferences pdPreferences) {
		return pdPreferencesRepository.saveAndFlush(pdPreferences);
	}

	
}
