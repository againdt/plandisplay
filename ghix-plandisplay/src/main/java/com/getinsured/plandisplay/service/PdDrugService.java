package com.getinsured.plandisplay.service;

import javax.servlet.http.HttpServletResponse;

import com.getinsured.hix.dto.plandisplay.DrugDataRequestDTO;
import com.getinsured.hix.dto.plandisplay.DrugDataResponseDTO;

/**
 * Interface is used to declare service methods for Prescription Drug Data for PdDrugServiceImpl.
 * 
 * @since October 23, 2018
 */
public interface PdDrugService {

	void getDrugDataResponseByRxcuiList(DrugDataRequestDTO requestDTO, DrugDataResponseDTO responseDTO, HttpServletResponse response);
}
