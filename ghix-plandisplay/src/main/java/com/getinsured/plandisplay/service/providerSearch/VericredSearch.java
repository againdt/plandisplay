package com.getinsured.plandisplay.service.providerSearch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.hix.dto.plandisplay.Provider;
import com.getinsured.hix.dto.plandisplay.ProviderAddress;
import com.getinsured.hix.dto.plandisplay.ProviderSearchRequest;
import com.getinsured.hix.model.IndividualPlan;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.model.plandisplay.ProviderBean;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.plandisplay.util.PlanDisplayAsyncUtil;
import com.getinsured.plandisplay.util.PlanDisplayUtil;

@Service(value="vericred")
public class VericredSearch implements ProviderSearch {

	private static final Logger LOGGER = LoggerFactory.getLogger(VericredSearch.class);
	
	@Autowired private PlanDisplayAsyncUtil planDisplayAsyncUtil;
	@Autowired private RestTemplate restTemplate;
	
	@Override
	public String getSearchType() {
		return "vericred";
	}
	
	@Override
	public Map<String, Future<Map<String, Object>>> prepareProviderHiosMap(List<ProviderBean> providers){		
		Map<String, Future<Map<String, Object>>> futureProviderResponseListMap = new HashMap<String, Future<Map<String, Object>>>();;		
		String vericredUrl = DynamicPropertiesUtil.getPropertyValue("planSelection.vericredUrl");
		String vericredApiKey = DynamicPropertiesUtil.getPropertyValue("planSelection.vericredApiKey");
		HttpHeaders headers = new HttpHeaders();
		headers.set("VERICRED_API_KEY", vericredApiKey);
		@SuppressWarnings({ "rawtypes", "unchecked" })
		HttpEntity entity = new HttpEntity(headers);
		if(providers != null) {
			for (ProviderBean providerBean : providers) {
				String sourceUrl = vericredUrl+"/providers/"+providerBean.getId();
				Future<Map<String, Object>> futureProviderResponse = planDisplayAsyncUtil.callProviderAPIAsync(sourceUrl, entity);
				futureProviderResponseListMap.put(providerBean.getId(), futureProviderResponse);
			}
		}
		return futureProviderResponseListMap;
	}
	
	@Override
	public String getNetworkStatus(Map<String, Future<Map<String, Object>>> futureProviderResponseListMap, IndividualPlan individualPlan, ProviderBean providerBean, List<Map<String, String>> providersInPlan) {
		if(futureProviderResponseListMap.containsKey(providerBean.getId())){
			Future<Map<String, Object>> futureProviderResponse = futureProviderResponseListMap.get(providerBean.getId());
			Map<String, Object> providerResponse = null;
			try {
				providerResponse = futureProviderResponse.get();
			} catch (InterruptedException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			} catch (ExecutionException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			}
			String providerAvailabilityKey = individualPlan.getIssuerPlanNumber();
			return parseResponse(providerResponse, providerBean.getProviderType(), providerAvailabilityKey);
		}		
		return PlanDisplayUtil.OUT_NETWORK;
	}
	
	@Override
	public Map<String,Object> searchProvider(ProviderSearchRequest providerSearchRequest) {
		JSONArray providers = new JSONArray();
		int currentPageSize = providerSearchRequest.getPer_page();
		String searchType = providerSearchRequest.getType();
		
		Map<String,Object> responseData = new HashMap<String,Object>();
		responseData.put("providers", providers);
		List<Map<String, Object>> vericredProviderList = null;
		responseData.put(PlanDisplayUtil.STATUS, GhixConstants.RESPONSE_SUCCESS);
		String vericredUrl = DynamicPropertiesUtil.getPropertyValue("planSelection.vericredUrl");
		String vericredApiKey = DynamicPropertiesUtil.getPropertyValue("planSelection.vericredApiKey");
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set("VERICRED_API_KEY", vericredApiKey);
			HttpEntity entity = new HttpEntity(headers);				
			String postParams = null;
			try {
				ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(ProviderSearchRequest.class);
				postParams = writer.writeValueAsString(providerSearchRequest);		
			} catch (JsonParseException | JsonMappingException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			} catch (IOException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			}
			entity = new HttpEntity(postParams, headers);
			HttpEntity<String> responseEntity = restTemplate.exchange(vericredUrl+"/providers/search", HttpMethod.POST, entity, String.class);
			String response = responseEntity.getBody();		
			
			Map<String, Object> providerResponse = null;
			try {
				ObjectReader objreader = JacksonUtils.getJacksonObjectReaderForHashMap(String.class, Object.class);
				providerResponse = objreader.readValue(response);
			} catch (JsonParseException |JsonMappingException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			} catch (IOException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			}
			vericredProviderList = (List<Map<String, Object>>) providerResponse.get("providers");//((HashMap)providerResponse.get("meta")).get("total")
			String total = null;
			if(providerResponse.get("meta") != null && providerResponse.get("meta") instanceof HashMap)
			{
				total = ((Integer) ((HashMap)providerResponse.get("meta")).get("total")).toString();
			}
			responseData.put("total", total);
		} catch (Exception e) {
			responseData.put(PlanDisplayUtil.STATUS, GhixConstants.RESPONSE_FAILURE);
			responseData.put(PlanDisplayUtil.ERROR, "Provider Search function is currently not available.");
			return responseData;
		}	
		
		int docCount = 0;
		if(vericredProviderList != null){
			for(Map<String, Object> vericredProviderObj : vericredProviderList){
				if(vericredProviderObj != null){
					String type= (String)vericredProviderObj.get("type");
					if(searchType.equalsIgnoreCase(type) && docCount < currentPageSize){
						Provider provider = new Provider();
						StringBuilder name = new StringBuilder(2);
						if(PlanDisplayUtil.INDIVIDUAL.equalsIgnoreCase(type)){						
							if(StringUtils.isNotEmpty((String) vericredProviderObj.get("title"))){
								name.append((String) vericredProviderObj.get("title")).append(PlanDisplayUtil.BLANK_SPACE_STRING);
							}
							if(StringUtils.isNotEmpty((String) vericredProviderObj.get("first_name"))){
								name.append((String) vericredProviderObj.get("first_name")).append(PlanDisplayUtil.BLANK_SPACE_STRING);
							}
							if(StringUtils.isNotEmpty((String) vericredProviderObj.get("last_name"))){
								name.append((String) vericredProviderObj.get("last_name"));
							}
							
						} else {
							name.append((String) vericredProviderObj.get("presentation_name"));
						}
						provider.setName(name.toString());
						provider.setSpecialty((String) vericredProviderObj.get("specialty"));
						List<ProviderAddress> providerAddressList = new ArrayList<ProviderAddress>();
						
						ProviderAddress providerAddress = new ProviderAddress((String) vericredProviderObj.get("street_line_1"));
						providerAddress.setAddLine2((String) vericredProviderObj.get("street_line_2"));
						providerAddress.setCity((String) vericredProviderObj.get("city"));
						providerAddress.setZip((String) vericredProviderObj.get("zip_code"));
						providerAddress.setState((String) vericredProviderObj.get("state"));
						if(vericredProviderObj.get("latitude") != null){
							providerAddress.setLat(Double.parseDouble(vericredProviderObj.get("latitude").toString()));
						}
						if(vericredProviderObj.get("longitude") != null){
							providerAddress.setLon(Double.parseDouble(vericredProviderObj.get("longitude").toString()));
						}
						String phone = (String)vericredProviderObj.get("phone");
						providerAddress.setPhone(new String[]{phone});
						providerAddressList.add(providerAddress);
						provider.setProviderAddress(providerAddressList);
						provider.setProviderType(type);
						provider.setStrenuus_id(String.valueOf(vericredProviderObj.get("id")));													
						provider.setPracticePhones(new ArrayList<>(Arrays.asList(phone)));
						docCount++;					
						providers.add(provider);
					}
				}
			}
		}		
		return responseData;
	}
	
	private String parseResponse(Map<String, Object> providerResponse, String providerType, String providerAvailabilityKey){
		if(providerResponse!=null && providerResponse.get("provider") != null){
			@SuppressWarnings("unchecked")
			Map<String, Object> vericredProvider = (Map<String, Object>) providerResponse.get("provider");			
			if(vericredProvider != null && vericredProvider.get("hios_ids") != null){
				@SuppressWarnings("unchecked")
				List<String> hiosIdList = (List<String>)vericredProvider.get("hios_ids");
				String hiosPlanId = PlanDisplayUtil.create14DigitNum(providerAvailabilityKey);
				if(hiosIdList != null && hiosIdList.contains(hiosPlanId)){
					return PlanDisplayUtil.IN_NETWORK;
				}
			}
		}
		return PlanDisplayUtil.OUT_NETWORK;
	}
}
