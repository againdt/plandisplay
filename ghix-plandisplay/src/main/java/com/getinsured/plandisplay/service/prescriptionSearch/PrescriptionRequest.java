package com.getinsured.plandisplay.service.prescriptionSearch;

import java.util.List;

import com.getinsured.hix.dto.plandisplay.DrugDTO;

public class PrescriptionRequest  {
	private int applicableYear;
	public int getApplicableYear() {
		return applicableYear;
	}

	public void setApplicableYear(int applicableYear) {
		this.applicableYear = applicableYear;
	}

	private String state;
	private List<String> planHiosList;	
	List<DrugDTO> drugDTOList;

	public List<DrugDTO> getDrugDTOList() {
		return drugDTOList;
	}

	public void setDrugDTOList(List<DrugDTO> drugDTOList) {
		this.drugDTOList = drugDTOList;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<String> getPlanHiosList() {
		return planHiosList;
	}

	public void setPlanHiosList(List<String> planHiosList) {
		this.planHiosList = planHiosList;
	}
}
