package com.getinsured.plandisplay.service.rules.gps;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.plandisplay.util.PlanDisplayUtil;

/**
 * GPS algorithm as part of AB release.
 * @created May 8 2014
 * @author root
 * Please refer to : HIX-
 * Users can select at max 2 benefits, 2 doctors with 10 points each
 */
public class GPSSpiderManAB extends DoctorsInNetworkGPSwithDeductible {
	private static final Integer EXPENSE_WEIGHT_DEDUCTIBLE = 50;
	private static final Integer DEDUCTIBLE_WEIGHT = 10;
	private static final Double SCORE_OUT_OF_HIGH = 98.0;
	private static final Double SCORE_OUT_OF_LOW = 93.0;
	protected static final Integer BENEFIT_WEIGHT = 10;
	private static final Integer DOCTOR_WEIGHT = 10;
	private static final Integer DOCTOR_WEIGHT_MIN = 2;
	private static final Integer BENEFIT_WEIGHT_MIN = 2;
	private static final Logger LOGGER = LoggerFactory.getLogger(DoctorsInNetworkGPSwithDeductible.class);
	
	/**
	 * This method takes deductible into account.
	 * @param expense
	 * @param deductible
	 * @param benefit
	 * @param benefitsSelected
	 * @return
	 */
	private Map<Integer, Double> computeRawScore(Map<Integer, Double> expense, Map<Integer, Double> deductible, Map<Integer, Integer> benefit, Integer benefitsSelected, Map<Integer, Integer> doctorsInNW, Integer doctorsSelected){
		Double idealPlanScore = EXPENSE_WEIGHT_DEDUCTIBLE*1.0;
		boolean deductibleIncluded = false;
		Map<Integer, Double> deductibleScore = null;
		Map<Integer, Double> expenseScore = null;
		Map<Integer, Integer> benefitScore = null;
		Map<Integer, Integer> doctorsScore = null;
		boolean atleastOneBenefitPresent = (countValuesInMap(benefit)>0)?true:false;
		boolean atleastOneDoctorPresent = (countValuesInMap(doctorsInNW)>0)?true:false;
		if(deductible!=null && !deductible.isEmpty()){
			deductibleIncluded = true;
			consoleDebug("GPS: deductible input passed to GPS calculator", deductible);
			deductibleScore = computeScoreFromDeductible(deductible);
			consoleDebug("GPS: Scores from deductible", deductibleScore);
		}else{
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "GPS: No deductible input passed to GPS calculator :",null, false);
		}
		
		/**
		 * ideal plan score takes 10 points into account
		 * irrespective of deductible is on or not
		 */
		idealPlanScore +=DEDUCTIBLE_WEIGHT;
		consoleDebug("GPS: expense input to algorithm", expense);
		expenseScore = computeScoreFromExpense(expense, deductibleIncluded);
		consoleDebug("GPS: Scores from expenses", expenseScore);
		
		
		
		//EXPENSE_WEIGHT_DEDUCTIBLE + DEDUCTIBLE_WEIGHT +BENEFIT_WEIGHT*benefitsSelected*1.0 + DOCTOR_WEIGHT*doctorsSelected*1.0;
		
		if(benefitsSelected>0){
			benefitScore = computeScoreFromBenefits(benefit, atleastOneBenefitPresent);
			Integer selectedBenefitWeight = atleastOneBenefitPresent?BENEFIT_WEIGHT:BENEFIT_WEIGHT_MIN;
			idealPlanScore += selectedBenefitWeight*benefitsSelected*1.0;
			consoleDebugBenefits("GPS: Scores from benefits", benefitScore);
		}
		
		/**
		 * If Doctor search is enabled compute this
		 */
		if(doctorsSelected>0){
			consoleDebugBenefits("GPS: Doctors Input values", doctorsInNW);
			doctorsScore = computeScoreFromDoctorsAvailability(doctorsInNW, atleastOneDoctorPresent);
			consoleDebugBenefits("GPS: doctors scores", doctorsScore);
			Integer selectedDoctorWeight = atleastOneDoctorPresent?DOCTOR_WEIGHT:DOCTOR_WEIGHT_MIN;
			idealPlanScore += selectedDoctorWeight*doctorsSelected*1.0;
		}
		
		
		
		for(Map.Entry<Integer, Double> entry : expenseScore.entrySet()){
			if(deductibleIncluded){
				entry.setValue(entry.getValue() + deductibleScore.get(entry.getKey()));
			}
			if(doctorsSelected>0){
				entry.setValue(entry.getValue() + doctorsScore.get(entry.getKey()));
			}
			
			if(benefitsSelected>0){
				entry.setValue(entry.getValue() + benefitScore.get(entry.getKey()));
			}
			//Normalization with idealPlanScore
			Double maxPossible = deductibleIncluded?SCORE_OUT_OF_HIGH:SCORE_OUT_OF_LOW;
			entry.setValue((entry.getValue()/idealPlanScore)*maxPossible);
			entry.setValue(Double.valueOf(SCORE_FORMAT.format(Math.round(entry.getValue()))));
		}
		
		return expenseScore;
	}
	
	@Override
	public Map<Integer, Double> calculateUsingDeductibleAndDoctorSearch(
			Map<Integer, Double> expense, Map<Integer, Double> deductible,
			Map<Integer, Integer> benefits, Integer benefitsSelected,
			Map<Integer, Integer> doctorsInNetwork, Integer doctorsSelected) {
		
		Map<Integer, Double> scores = null;
		
		/**
		 * if deductible is included go the other route
		 * Let's assume deductible is included for a while
		 */
		
		scores = computeRawScore(expense, deductible, benefits, benefitsSelected, doctorsInNetwork, doctorsSelected);
		
		
		consoleDebug("GPS: Raw scores - before scaling to 100", scores);
		scores = disperseScoresBelowThreshold(scores);
		consoleDebug("GPS: final scores", scores);
		return scores;
		
	}
	
	/**
	 * computeScoreFromExpense
	 */
	private Map<Integer, Double> computeScoreFromExpense(Map<Integer, Double> expense, boolean deductibleIncluded){
		Map<Integer, Double> expenseScore = computeScaledRatio(expense);
		Integer expenseWeight = EXPENSE_WEIGHT_DEDUCTIBLE;
		if(!deductibleIncluded){
			expenseWeight += DEDUCTIBLE_WEIGHT;
		}
		for(Map.Entry<Integer, Double> entry : expenseScore.entrySet()){
			entry.setValue(entry.getValue()*expenseWeight);
		}
		
		return expenseScore;
	}
	
	/**
	 * Compute scores we get from doctors presence 
	 * @param doctorsPresence
	 * @return
	 */
	private Map<Integer, Integer> computeScoreFromDoctorsAvailability(Map<Integer, Integer> doctorsPresence, boolean atLeastOneDoctorPresent){
		Map<Integer, Integer> doctorsScore = new HashMap<Integer, Integer>(doctorsPresence);
		Integer weight = atLeastOneDoctorPresent?DOCTOR_WEIGHT:DOCTOR_WEIGHT_MIN;
		for(Map.Entry<Integer, Integer> entry : doctorsScore.entrySet()){
			entry.setValue(entry.getValue()*weight);
		}
		
		return doctorsScore;
	}
	
	/**
	 * Return total count of values in the Map
	 * @param items
	 * @return
	 */
	private int countValuesInMap(Map<Integer, Integer> items){
		if(items==null || items.isEmpty()){
			return 0;
		}
		
		int total = 0;
		
		for(Map.Entry<Integer, Integer> entry : items.entrySet()){
			total+=entry.getValue();
		}
		return total;
	}
	
	/**
	 * changing default impl
	 */
	protected Map<Integer, Integer> computeScoreFromBenefits(Map<Integer, Integer> benefits, boolean atleastOneBenefitSelected){
		Map<Integer, Integer> benefitScore = new HashMap<Integer, Integer>(benefits);
		Integer weight = atleastOneBenefitSelected?BENEFIT_WEIGHT:BENEFIT_WEIGHT_MIN;
		for(Map.Entry<Integer, Integer> entry : benefitScore.entrySet()){
			entry.setValue(entry.getValue()*weight);
		}
		
		return benefitScore;
	}
	
	
	public static void main(String args[]){
		GPSSpiderManAB ab = new GPSSpiderManAB();
		
		Map<Integer, Double> expense = new HashMap<Integer, Double>();
		Map<Integer, Double> deductible = new HashMap<Integer, Double>();
		Map<Integer, Integer> benefits = new HashMap<Integer, Integer>();
		Map<Integer, Integer> doctorsInNW = new HashMap<Integer, Integer>();
		
		expense.put(203, 2450.0);
		expense.put(206, 2238.7);
		expense.put(241, 1649.0);
		expense.put(259, 3610.7);
		expense.put(274, 2071.1);
		expense.put(2573, 3383.0);
		expense.put(2576, 2086.3);
		
		deductible.put(203, 6000.0);
		deductible.put(206, 5000.0);
		deductible.put(241, 6000.0);
		deductible.put(259, 0.0);
		deductible.put(274, 5000.1);
		deductible.put(2573, 0.0);
		deductible.put(2576, 2500.0);
		
		benefits.put(203, 1);
		benefits.put(206, 2);
		benefits.put(241, 0);
		benefits.put(259, 0);
		benefits.put(274, 0);
		benefits.put(2573, 0);
		benefits.put(2576, 0);
		
		
		doctorsInNW.put(203, 1);
		doctorsInNW.put(206, 2);
		doctorsInNW.put(241, 0);
		doctorsInNW.put(259, 0);
		doctorsInNW.put(274, 0);
		doctorsInNW.put(2573, 0);
		doctorsInNW.put(2576, 0);
		
		Map<Integer, Double> scores = ab.calculateUsingDeductibleAndDoctorSearch(expense, deductible, benefits, 2, null, 0);
		for(Map.Entry<Integer, Double> score : scores.entrySet()){
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "#" + score.getKey() + " - " + score.getValue() + "%", null, true);
		}
		
		
	}
	

}
