package com.getinsured.plandisplay.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.PdHousehold;
import com.getinsured.hix.model.PdOrderItem;
import com.getinsured.hix.model.PdOrderItemPerson;
import com.getinsured.hix.model.PdPerson;
import com.getinsured.hix.model.plandisplay.PdSaveCartRequest;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.plandisplay.repository.PdOrderItemPersonRepository;

@Service("PdOrderItemPersonService")
public class PdOrderItemPersonServiceImpl implements PdOrderItemPersonService {

	@Autowired private PdOrderItemPersonRepository pdOrderItemPersonRepository;
	@Autowired private PdPersonService pdPersonService;
	@Autowired private PdHouseholdService pdHouseholdService;
	
	
	@Override
	@Transactional
	public void saveItemPerson(PdOrderItem pdOrderItem, PdSaveCartRequest pdSaveCartRequest)
	{	
		PdHousehold pdHousehold = pdHouseholdService.findById(pdSaveCartRequest.getHouseholdId());
		List<PdPerson> pdPersonList = pdPersonService.getPersonListByHouseholdId(pdSaveCartRequest.getHouseholdId());
		InsuranceType insuranceType = pdSaveCartRequest.getInsuranceType();
		List<Map<String,String>> premiumData = pdSaveCartRequest.getPremiumData();
		Map<String,Float> subsidyData = pdSaveCartRequest.getSubsidyData();
		
		List<PdOrderItemPerson> pdOrderItemPersons = new ArrayList<PdOrderItemPerson>();
		if(premiumData != null){
			for(int i = 0; i < premiumData.size(); i++) {
				for(PdPerson pdPerson : pdPersonList) {
					String externalId = pdPerson.getExternalId().toString();
					if(premiumData.get(i).get("id").equalsIgnoreCase(externalId)) {
						PdOrderItemPerson pdOrderItemPerson = new PdOrderItemPerson();	
						BigDecimal premium = new BigDecimal(premiumData.get(i).get("premium"));
						pdOrderItemPerson.setPremium(premium.setScale(2,BigDecimal.ROUND_HALF_UP).floatValue());
						pdOrderItemPerson.setRegion(Integer.parseInt(premiumData.get(i).get("region")));	            	
						if(null!=subsidyData && subsidyData.get(externalId) != null) {
							pdOrderItemPerson.setAppliedSubsidy(subsidyData.get(externalId));
						}
						if(premiumData.get(i).get("personCovgDate") != null){
							pdOrderItemPerson.setPersonCoverageStartDate(DateUtil.StringToDate(premiumData.get(i).get("personCovgDate"), "MM/dd/yyyy"));
						}						
						pdOrderItemPerson.setSubscriberFlag(getSubscriberFlag(insuranceType, pdHousehold, externalId));
						pdOrderItemPerson.setPdOrderItem(pdOrderItem);
						pdOrderItemPerson.setPdPerson(pdPerson);
						pdOrderItemPersons.add(pdOrderItemPerson);
					}
				}
			}
		}else{
			for(PdPerson pdPerson : pdPersonList) {
				String externalId = pdPerson.getExternalId().toString();
				PdOrderItemPerson pdOrderItemPerson = new PdOrderItemPerson();
				pdOrderItemPerson.setSubscriberFlag(getSubscriberFlag(insuranceType, pdHousehold, externalId));
				pdOrderItemPerson.setPdOrderItem(pdOrderItem);
				pdOrderItemPerson.setPdPerson(pdPerson);
						
				pdOrderItemPersons.add(pdOrderItemPerson);
			}
		}
		pdOrderItemPersonRepository.save(pdOrderItemPersons);
	}
	
	private YorN getSubscriberFlag(InsuranceType insuranceType, PdHousehold pdHousehold, String externalId) {
		if(InsuranceType.HEALTH.equals(insuranceType)) {
			if(pdHousehold.getHealthSubscriberId().equals(externalId)){
				return YorN.Y;
			} else {
				return YorN.N;
			}
		} else {
			//For cart items added through cross sell in PHIX
			if(pdHousehold.getDentalSubscriberId() == null && pdHousehold.getHealthSubscriberId() != null){
				if(pdHousehold.getHealthSubscriberId().equals(externalId)){
					return YorN.Y;
				} else {
					return YorN.N;
				}
			}else {
				if(pdHousehold.getDentalSubscriberId() != null && pdHousehold.getDentalSubscriberId().equals(externalId)){
					return YorN.Y;
				} else {
					return YorN.N;
				}
			}
		}
	}
	
	@Override
	public List<PdOrderItemPerson> getByOrderItemId(Long orderItemId){
		return pdOrderItemPersonRepository.findOrderItemPersonsByOrderItemId(orderItemId);
	}
	
	@Override
	public void deleteByOrderItemId(Long orderItemId){
		pdOrderItemPersonRepository.deleteByOrderItemId(orderItemId);
	}

	@Override
	public Long countOrderItemPersonsByOrderItemId(Long orderItemId) {
		return pdOrderItemPersonRepository.countOrderItemPersonsByOrderItemId(orderItemId);
	}

}
