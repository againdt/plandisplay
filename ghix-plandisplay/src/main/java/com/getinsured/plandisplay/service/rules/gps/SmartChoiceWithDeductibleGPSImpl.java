package com.getinsured.plandisplay.service.rules.gps;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * HIX-28257
 * @author bhatt
 *
 */
public class SmartChoiceWithDeductibleGPSImpl extends
		SmartChoiceScoreAlgorithmGPSImpl {
	private static final Integer EXPENSE_WEIGHT = 50;
	private static final Integer DEDUCTIBLE_WEIGHT = 10;
	private static final Double SCORE_OUT_OF = 98.0;
	
	@Override
	public Map<Integer, Double> calculateUsingDeductible(
			Map<Integer, Double> expense, Map<Integer, Double> deductible,
			Map<Integer, Integer> benefits, Integer benefitsSelected) {
		Map<Integer, Double> scores = computeRawScore(expense, deductible, benefits, benefitsSelected);
		consoleDebug("GPS: Raw scores - before scaling to 100", scores);
		scores = disperseScoresBelowThreshold(scores);
		consoleDebug("GPS: final scores", scores);
		return scores;
	}
	
	protected Map<Integer, Double> computeScoreFromDeductible(Map<Integer, Double> deductible){
		Map<Integer, Double> deductibleScore = computeScaledDeductible(deductible);
		for(Map.Entry<Integer, Double> entry : deductibleScore.entrySet()){
			entry.setValue(entry.getValue()*DEDUCTIBLE_WEIGHT);
		}
		
		return deductibleScore;
	}
	
	/**
	 * Computes deductible component based on logistic function
	 * f(x) = 1/(1+((x-minVal+1)/stepVal)
	 * Agreed stepVal = 500 for now
	 * @param deductible
	 * @return
	 */
	protected Map<Integer, Double> computeScaledDeductible(Map<Integer, Double> deductible){
		Map<Integer, Double> deductibleValues = new HashMap<Integer, Double>(deductible);
		consoleDebug("GPS: deductible input", deductible);
		Double minVal = Collections.min(deductible.values());
		for(Map.Entry<Integer, Double> entry : deductibleValues.entrySet()){
			entry.setValue(1/(1+(entry.getValue() - minVal + 1)/500));
		}
		consoleDebug("GPS: scaled deductible", deductibleValues);
		return deductibleValues;
	}
	
	/**
	 * This method takes deductible into account.
	 * @param expense
	 * @param deductible
	 * @param benefit
	 * @param benefitsSelected
	 * @return
	 */
	private Map<Integer, Double> computeRawScore(Map<Integer, Double> expense, Map<Integer, Double> deductible, Map<Integer, Integer> benefit, Integer benefitsSelected){
		Double idealPlanScore = EXPENSE_WEIGHT + DEDUCTIBLE_WEIGHT +BENEFIT_WEIGHT*benefitsSelected*1.0;
		
		Map<Integer, Double> expenseScore = computeScoreFromExpense(expense);
		Map<Integer, Double> deductibleScore = computeScoreFromDeductible(deductible);
		consoleDebug("GPS: Scores from expenses", expenseScore);
		consoleDebug("GPS: Scores from deductible", deductibleScore);
		Map<Integer, Integer> benefitScore = computeScoreFromBenefits(benefit);
		consoleDebugBenefits("GPS: Scores from benefits", benefitScore);
		for(Map.Entry<Integer, Double> entry : expenseScore.entrySet()){
			entry.setValue(entry.getValue() + deductibleScore.get(entry.getKey()) + benefitScore.get(entry.getKey()));
			//Normalization with idealPlanScore
			entry.setValue((entry.getValue()/idealPlanScore)*SCORE_OUT_OF);
			entry.setValue(Double.valueOf(SCORE_FORMAT.format(Math.round(entry.getValue()))));
		}
		
		return expenseScore;
	}
	
	/**
	 * 
	 */
	private Map<Integer, Double> computeScoreFromExpense(Map<Integer, Double> expense){
		Map<Integer, Double> expenseScore = computeScaledRatio(expense);
		for(Map.Entry<Integer, Double> entry : expenseScore.entrySet()){
			entry.setValue(entry.getValue()*EXPENSE_WEIGHT);
		}
		
		return expenseScore;
	}
	
	@Override
	public Map<Integer, Double> calculateUsingDeductibleAndDoctorSearch(
			Map<Integer, Double> expense, Map<Integer, Double> deductible,
			Map<Integer, Integer> benefits, Integer benefitsSelected,
			Map<Integer, Integer> doctorsInNetwork, Integer doctorsSelected) {
		throw new UnsupportedOperationException();
	}

}
