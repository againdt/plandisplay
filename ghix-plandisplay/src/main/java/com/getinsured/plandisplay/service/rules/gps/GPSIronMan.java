package com.getinsured.plandisplay.service.rules.gps;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.plandisplay.util.PlanDisplayUtil;

/**
 * 1- Calculate applicable weights. When No of doctors selected are less than 1 then Doctor weight for the score needs to be 10. Same is applicable for drugs
 * 2- Calculate deductible score as (minDeductible/current deductible)*applicable deductible weight
 * 3- Calculate expense score as (minExpense/current expense)*applicable expense weight
 * 4- Calculate doctor score as noOfDoctorCoveredbyPlan * average applicable doctor weight. average applicable doctor weight = applicable doctor weight/no of doctors selected by user
 * 5- Calculate drug score as noOfDrugsCoveredbyPlan * average applicable drug weight. average applicable drug weight = applicable drug weight/no of drugs selected by user
 * 6- Calculate benefit score as noOfDrugsCoveredbyPlan * average applicable benefit weight. average applicable benefit weight = applicable benefit weight/no of benefits selected by user
 * 7. Add expense score + doctor score + drug score + benefit score
 * 8- Move score towards 100. (100*currentScore)/maxScore
 * 9- Round scores to whole number
 */

public class GPSIronMan{
	private static final Integer EXPENSE_WEIGHT = 60;
	private static final Integer DEDUCTIBLE_WEIGHT = 10;
	private static final Integer DOCTOR_WEIGHT = 15;
	private static final Integer DRUG_WEIGHT = 15;
	private static final Integer BENEFIT_WEIGHT = 0;
	private static final Logger LOGGER = LoggerFactory.getLogger(GPSIronMan.class);
	
	public Map<Integer, Double> computeRawScore(Map<Integer, Double> expense, Map<Integer, Double> deductible, Map<Integer, Integer> benefit, Integer benefitsSelected, Map<Integer, Integer> doctorsInNW, Integer doctorsSelected, Map<Integer, Integer> availableDrugs, Integer drugsSelected){
		boolean deductibleIncluded = false;
		Map<Integer, Double> deductibleScore = null;
		Map<Integer, Double> expenseScore = null;
		Map<Integer, Double> doctorsScore = null;
		Map<Integer, Double> drugsScore = null;
		Map<Integer, Double> benefitScore = null;
		Map<Integer, Double> planScore = null;
		
		//1- Calculate applicable weights. When No of doctors selected are less than 1 then Doctor weight for the score needs to be 10. Same is applicable for drugs
		Map<String,Integer> applicableWeights = calculateWeight(doctorsSelected, drugsSelected, benefitsSelected);
		
		//2- Calculate deductible score as (minDeductible/current deductible)*applicable deductible weight
		if(deductible!=null && !deductible.isEmpty()){
			deductibleIncluded = true;
			consoleDebug("GPS deductible input passed to GPS calculator", deductible);
			deductibleScore = computeScoreFromDeductible(deductible, applicableWeights.get("DEDUCTIBLE_WEIGHT"));
			consoleDebug("GPS Scores from deductible", deductibleScore);
		}else{
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "GPS No deductible input passed to GPS calculator :",null, false);
		}
		
		//3- Calculate expense score as (minExpense/current expense)*applicable expense weight
		consoleDebug("GPS expense input to algorithm", expense);
		expenseScore = computeScoreFromExpense(expense, applicableWeights.get("EXPENSE_WEIGHT"));
		consoleDebug("GPS Scores from expenses", expenseScore);
		
		//4- Calculate doctor score as noOfDoctorCoveredbyPlan * average applicable doctor weight. average applicable doctor weight = applicable doctor weight/no of doctors selected by user
		if(doctorsSelected>0){
			consoleDebugBenefits("GPS Doctors Input values", doctorsInNW);
			doctorsScore = computeScoreFromDoctorsAvailability(doctorsInNW, doctorsSelected, applicableWeights.get("DOCTOR_WEIGHT"));
			consoleDebug("GPS doctors scores", doctorsScore);
		}
		
		//5- Calculate drug score as noOfDrugsCoveredbyPlan * average applicable drug weight. average applicable drug weight = applicable drug weight/no of drugs selected by user
		if(drugsSelected>0){
			consoleDebugBenefits("GPS Drugs Input values", availableDrugs);
			drugsScore = computeScoreFromDrugsAvailability(availableDrugs, drugsSelected, applicableWeights.get("DRUG_WEIGHT"));
			consoleDebug("GPS Drugs scores", drugsScore);
		}
		
		//6- Calculate benefit score as noOfDrugsCoveredbyPlan * average applicable benefit weight. average applicable benefit weight = applicable benefit weight/no of benefits selected by user
		if(benefitsSelected>0){
			consoleDebugBenefits("GPS Scores input benefits", benefit);
			benefitScore = computeScoreFromBenefits(benefit, benefitsSelected, applicableWeights.get("BENEFIT_WEIGHT"));
			consoleDebug("GPS Scores from benefits", benefitScore);
		}
		
		//7. Add expense score + doctor score + drug score + benefit score
		for(Map.Entry<Integer, Double> entry : expenseScore.entrySet()){
			if(deductibleIncluded){
				entry.setValue(entry.getValue() + deductibleScore.get(entry.getKey()));
			}
			
			if(doctorsSelected>0){
				entry.setValue(entry.getValue() + doctorsScore.get(entry.getKey()));
			}
			
			if(benefitsSelected>0){
				entry.setValue(entry.getValue() + benefitScore.get(entry.getKey()));
			}
			
			if(drugsSelected>0){
				entry.setValue(entry.getValue() + drugsScore.get(entry.getKey()));
			}
		}
		
		//8- Move score towards 100. (100*currentScore)/maxScore
		Double maxScore = Collections.max(expenseScore.values());
		for(Map.Entry<Integer, Double> entry : expenseScore.entrySet()){
			entry.setValue((100*entry.getValue())/maxScore);
		}
		
		//9- Round scores to whole number
		planScore = roundScores(expenseScore);
		return planScore;
	}
	
	private Map<String, Integer> calculateWeight(Integer doctorsSelected, Integer drugsSelected, Integer benefitsSelected) {
		Integer doctorWeight = DOCTOR_WEIGHT;
		Integer drugWeight = DRUG_WEIGHT;
		Integer expenseWeight = EXPENSE_WEIGHT;
		
		if(doctorWeight > 10 && doctorsSelected < 2){
			doctorWeight = 10;
			expenseWeight += DOCTOR_WEIGHT - doctorWeight;
		}
		
		if(drugWeight > 10 && drugsSelected < 2){
			drugWeight = 10;
			expenseWeight += DRUG_WEIGHT - drugWeight;
		}
		
		Map<String, Integer> newWeight = new HashMap<String, Integer>();
		newWeight.put("DOCTOR_WEIGHT", doctorWeight);
		newWeight.put("DRUG_WEIGHT", drugWeight);
		newWeight.put("BENEFIT_WEIGHT", BENEFIT_WEIGHT);
		newWeight.put("EXPENSE_WEIGHT", expenseWeight);
		newWeight.put("DEDUCTIBLE_WEIGHT", DEDUCTIBLE_WEIGHT);
		
		return newWeight;
	}

	private void consoleDebug(String message, Map<Integer, Double> result){
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "----"+message+"----", null, true);
		for(Map.Entry<Integer, Double> entry : result.entrySet()){
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, entry.getKey() + " : " + entry.getValue(), null, true);
		}
	}
	
	public void consoleDebugBenefits(String message, Map<Integer, Integer> result) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "----"+message+"----", null, true);
		for (Map.Entry<Integer, Integer> entry : result.entrySet()) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, entry.getKey() + " : " + entry.getValue(), null, true);
		}
	}
	
	private Map<Integer, Double> roundScores(Map<Integer, Double> scores){
		for(Map.Entry<Integer, Double> entry : scores.entrySet()){
			entry.setValue(Double.valueOf(Math.round(entry.getValue())));
		}
		return scores;
	}
	
	private Map<Integer, Double> computeScoreFromDeductible(Map<Integer, Double> deductible, Integer weight){
		Map<Integer, Double> deductibleScore = new HashMap<Integer, Double>(deductible);
		Double minVal = Collections.min(deductible.values());
		//If one of the plan has 0 deductible then set deductible of that plan to 100.
		if(minVal <= 0){
			minVal = 100d;
		}
		for(Map.Entry<Integer, Double> entry : deductibleScore.entrySet()){
			Double currentDeductible = entry.getValue() > 0 ? entry.getValue() : 100d;
			entry.setValue(minVal/currentDeductible);
			entry.setValue(entry.getValue()*weight);
		}
		
		return deductibleScore;
	}
	
	private Map<Integer, Double> computeScoreFromExpense(Map<Integer, Double> expense, Integer weight){
		Map<Integer, Double> expenseScore = new HashMap<Integer, Double>(expense);
		Double minVal = Collections.min(expense.values());
		//If one of the plan has 0 expense then set expense of that plan to 100.
		if(minVal <= 0){
			minVal = 100d;
		}
		for(Map.Entry<Integer, Double> entry : expenseScore.entrySet()){
			Double currentExpense = entry.getValue() > 0 ? entry.getValue() : 100d;
			entry.setValue(minVal/currentExpense);
			entry.setValue(entry.getValue()*weight);
		}
		
		return expenseScore;
	}
	
	private Map<Integer, Double> computeScoreFromDoctorsAvailability(Map<Integer, Integer> doctorsPresence, Integer doctorsSelected, Integer weight){
		Map<Integer, Double> doctorsScore = new HashMap<Integer, Double>();
		double weightPerDoctor = (weight > 0 && doctorsSelected > 0) ? (weight.floatValue()/doctorsSelected.intValue()):0d;
		for(Map.Entry<Integer, Integer> entry : doctorsPresence.entrySet()){
			doctorsScore.put(entry.getKey(), entry.getValue()*weightPerDoctor);
		}
		
		return doctorsScore;
	}
	
	private Map<Integer, Double> computeScoreFromDrugsAvailability(Map<Integer, Integer> availableDrugs, Integer drugsSelected, Integer weight){
		Map<Integer, Double> drugsScore = new HashMap<Integer, Double>();
		double weightPerDrug = (weight > 0 && drugsSelected > 0) ? (weight.floatValue()/drugsSelected.intValue()):0d;
		for(Map.Entry<Integer, Integer> entry : availableDrugs.entrySet()){
			drugsScore.put(entry.getKey(), entry.getValue()*weightPerDrug);
		}
		
		return drugsScore;
	}
	
	private Map<Integer, Double> computeScoreFromBenefits(Map<Integer, Integer> benefits, Integer benefitsSelected, Integer weight){
		Map<Integer, Double> benefitScore = new HashMap<Integer, Double>();
		double weightPerBenefit = (weight > 0 && benefitsSelected > 0) ? (weight.floatValue()/benefitsSelected.intValue()):0d;
		for(Map.Entry<Integer, Integer> entry : benefits.entrySet()){
			benefitScore.put(entry.getKey(), entry.getValue()*weightPerBenefit);
		}
		
		return benefitScore;
	}

}

