package com.getinsured.plandisplay.service;

import java.util.List;
import com.getinsured.hix.model.plandisplay.PdResponse;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

public interface PlanSolrUpdateService {
	public void saveMissingSolrDocs( List<Integer> planIds , String insuranceType);
	public PdResponse saveMissingSolrDocsSynch( List<Integer> planIds , String insuranceType) throws GIRuntimeException;
}
