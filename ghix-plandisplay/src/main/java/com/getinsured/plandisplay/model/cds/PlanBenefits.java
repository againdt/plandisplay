package com.getinsured.plandisplay.model.cds;

public class PlanBenefits {

	private String name;
	private String subToNetDeduct;
	private String copayAttribute;
	private Double copayVal;
	private String coinsuranceAttribute;
	private Double coinsuranceVal;
	private String isCovered;
	
	public String getCopayAttribute() {
		return copayAttribute;
	}
	public void setCopayAttribute(String copayAttribute) {
		this.copayAttribute = copayAttribute;
	}
	public Double getCopayVal() {
		return copayVal;
	}
	public void setCopayVal(Double copayVal) {
		this.copayVal = copayVal;
	}
	public String getCoinsuranceAttribute() {
		return coinsuranceAttribute;
	}
	public void setCoinsuranceAttribute(String coinsuranceAttribute) {
		this.coinsuranceAttribute = coinsuranceAttribute;
	}
	public Double getCoinsuranceVal() {
		return coinsuranceVal;
	}
	public void setCoinsuranceVal(Double coinsuranceVal) {
		this.coinsuranceVal = coinsuranceVal;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSubToNetDeduct() {
		return subToNetDeduct;
	}
	public void setSubToNetDeduct(String subToNetDeduct) {
		this.subToNetDeduct = subToNetDeduct;
	}
	public String getIsCovered() {
		return isCovered;
	}
	public void setIsCovered(String isCovered) {
		this.isCovered = isCovered;
	}	

}
