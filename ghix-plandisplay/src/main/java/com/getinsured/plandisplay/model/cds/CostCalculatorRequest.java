package com.getinsured.plandisplay.model.cds;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


public class CostCalculatorRequest {
	
	@Min(value = 1, message = "There should be atleast one family member.")
	private int noOfFamilyMembers;
	
	private PreferencesDetails drugUsage;
	private PreferencesDetails medicalUsage;

	@Min(value=0)
	private int noOfProvidersSearched;

	@Min(value=0)
	private int noOfDrugsSearched;
	
	@NotNull
	private String guid;
	
	@Valid
	private List<PlanData> planData;
	
	private List<String> benefits;
	
	public int getNoOfFamilyMembers() {
		return noOfFamilyMembers;
	}
	public void setNoOfFamilyMembers(int noOfFamilyMembers) {
		this.noOfFamilyMembers = noOfFamilyMembers;
	}

	public PreferencesDetails getDrugUsage() {
		return drugUsage;
	}
	public void setDrugUsage(PreferencesDetails drugUsage) {
		this.drugUsage = drugUsage;
	}
	public PreferencesDetails getMedicalUsage() {
		return medicalUsage;
	}
	public void setMedicalUsage(PreferencesDetails medicalUsage) {
		this.medicalUsage = medicalUsage;
	}
	public int getNoOfProvidersSearched() {
		return noOfProvidersSearched;
	}
	public void setNoOfProvidersSearched(int noOfProvidersSearched) {
		this.noOfProvidersSearched = noOfProvidersSearched;
	}
	public int getNoOfDrugsSearched() {
		return noOfDrugsSearched;
	}
	public void setNoOfDrugsSearched(int noOfDrugsSearched) {
		this.noOfDrugsSearched = noOfDrugsSearched;
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public List<PlanData> getPlanData() {
		return planData;
	}
	public void setPlanData(List<PlanData> planData) {
		this.planData = planData;
	}
	public List<String> getBenefits() {
		return benefits;
	}
	public void setBenefits(List<String> benefits) {
		this.benefits = benefits;
	}

	@SuppressWarnings("unused")
	public class PreferencesDetails {
		private Integer low;
		private Integer medium;
		private Integer high;
		private Integer veryHigh;
				
		public Integer getLow() {
			return low;
		}
		public void setLow(Integer low) {
			this.low = low;
		}
		public Integer getMedium() {
			return medium;
		}
		public void setMedium(Integer medium) {
			this.medium = medium;
		}
		public Integer getHigh() {
			return high;
		}
		public void setHigh(Integer high) {
			this.high = high;
		}
		public Integer getVeryHigh() {
			return veryHigh;
		}
		public void setVeryHigh(Integer veryHigh) {
			this.veryHigh = veryHigh;
		}
				
	}
}
