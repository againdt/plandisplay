package com.getinsured.plandisplay.model.cds;

import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PlanData {
	
	@NotNull(message = "hiosPlanId cannot be null.")
	@Size(min = 14, max = 16, message = "hiosPlanId length must be of 14 characters")
	private String hiosPlanId;
	
	@Min(value=0)
	private Float netPremium;
	private Double oopMaxIndividual;
	private Double oopMaxFamily;
	private Double drugOopMaxIndividual;
	private Double drugOopMaxFamily;
	@Min(value=0)
	private Double deductibleIndividual;
	@Min(value=0)
	private Double deductibleFamily;
	private Double drugDeductibleIndividual;
	private Double drugDeductibleFamily;
	private String planLevel;
	@Min(value=0)
	private Integer noOfProvidersCovered;
	@Min(value=0)
	private Integer noOfDrugsCovered;
	
	private List<PlanBenefits> benefits;
		
	public String getHiosPlanId() {
		return hiosPlanId;
	}
	public void setHiosPlanId(String hiosPlanId) {
		this.hiosPlanId = hiosPlanId;
	}
	public Float getNetPremium() {
		return netPremium;
	}
	public void setNetPremium(Float netPremium) {
		this.netPremium = netPremium;
	}
	public Double getOopMaxIndividual() {
		return oopMaxIndividual;
	}
	public void setOopMaxIndividual(Double oopMaxIndividual) {
		this.oopMaxIndividual = oopMaxIndividual;
	}
	public Double getOopMaxFamily() {
		return oopMaxFamily;
	}
	public void setOopMaxFamily(Double oopMaxFamily) {
		this.oopMaxFamily = oopMaxFamily;
	}
	public Double getDrugOopMaxIndividual() {
		return drugOopMaxIndividual;
	}
	public void setDrugOopMaxIndividual(Double drugOopMaxIndividual) {
		this.drugOopMaxIndividual = drugOopMaxIndividual;
	}
	public Double getDrugOopMaxFamily() {
		return drugOopMaxFamily;
	}
	public void setDrugOopMaxFamily(Double drugOopMaxFamily) {
		this.drugOopMaxFamily = drugOopMaxFamily;
	}
	public Double getDeductibleIndividual() {
		return deductibleIndividual;
	}
	public void setDeductibleIndividual(Double deductibleIndividual) {
		this.deductibleIndividual = deductibleIndividual;
	}
	public Double getDeductibleFamily() {
		return deductibleFamily;
	}
	public void setDeductibleFamily(Double deductibleFamily) {
		this.deductibleFamily = deductibleFamily;
	}
	public Double getDrugDeductibleIndividual() {
		return drugDeductibleIndividual;
	}
	public void setDrugDeductibleIndividual(Double drugDeductibleIndividual) {
		this.drugDeductibleIndividual = drugDeductibleIndividual;
	}
	public Double getDrugDeductibleFamily() {
		return drugDeductibleFamily;
	}
	public void setDrugDeductibleFamily(Double drugDeductibleFamily) {
		this.drugDeductibleFamily = drugDeductibleFamily;
	}
	public Integer getNoOfProvidersCovered() {
		return noOfProvidersCovered;
	}
	public void setNoOfProvidersCovered(Integer noOfProvidersCovered) {
		this.noOfProvidersCovered = noOfProvidersCovered;
	}
	public Integer getNoOfDrugsCovered() {
		return noOfDrugsCovered;
	}
	public void setNoOfDrugsCovered(Integer noOfDrugsCovered) {
		this.noOfDrugsCovered = noOfDrugsCovered;
	}
	public List<PlanBenefits> getBenefits() {
		return benefits;
	}
	public void setBenefits(List<PlanBenefits> benefits) {
		this.benefits = benefits;
	}
	public String getPlanLevel() {
		return planLevel;
	}
	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}

}
