package com.getinsured.plandisplay.controller;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Future;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.Produces;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.Base64Utils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.hix.dto.consumer.MemberPortalResponseDTO;
import com.getinsured.hix.dto.consumer.goodrx.DrugDetailsDTO;
import com.getinsured.hix.dto.consumer.goodrx.GoodRxResponseDTO;
import com.getinsured.hix.dto.consumer.ssap.SsapApplicationDTO;
import com.getinsured.hix.dto.plandisplay.DrugDTO;
import com.getinsured.hix.dto.plandisplay.DrugDataRequestDTO;
import com.getinsured.hix.dto.plandisplay.DrugDataResponseDTO;
import com.getinsured.hix.dto.plandisplay.DrugDosageDTO;
import com.getinsured.hix.dto.plandisplay.DrugPropertiesRequestDTO;
import com.getinsured.hix.dto.plandisplay.DrugPropertiesResponseDTO;
import com.getinsured.hix.dto.plandisplay.DrugSearchDTO;
import com.getinsured.hix.dto.plandisplay.DrxDosageDTO;
import com.getinsured.hix.dto.plandisplay.DrxPrescriptionSearchDTO;
import com.getinsured.hix.dto.plandisplay.PdHouseholdDTO;
import com.getinsured.hix.dto.plandisplay.PdOrderItemDTO;
import com.getinsured.hix.dto.plandisplay.PdPersonDTO;
import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;
import com.getinsured.hix.dto.plandisplay.dst.SaveHouseholdRequest;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71Response;
import com.getinsured.hix.dto.plandisplay.ind71g.Ind71GRequest;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.model.IndividualPlan;
import com.getinsured.hix.model.PdHousehold;
import com.getinsured.hix.model.PdOrderItem;
import com.getinsured.hix.model.PdOrderItem.Status;
import com.getinsured.hix.model.PdOrderItemPerson;
import com.getinsured.hix.model.PdPerson;
import com.getinsured.hix.model.PdPreferences;
import com.getinsured.hix.model.Plan.PlanLevel;
import com.getinsured.hix.model.PlanRateBenefit;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.plandisplay.PdMtCartItem;
import com.getinsured.hix.model.plandisplay.PdMtCartItemMember;
import com.getinsured.hix.model.plandisplay.PdMtCartItemResponse;
import com.getinsured.hix.model.plandisplay.PdMtCartItemResponse.ApplicationStatus;
import com.getinsured.hix.model.plandisplay.PdMtCartItemResponse.ErrorCodes;
import com.getinsured.hix.model.plandisplay.PdMtCartItemResponse.MaleOrFemale;
import com.getinsured.hix.model.plandisplay.PdMtCartItemResponse.YesOrNo;
import com.getinsured.hix.model.plandisplay.PdQuoteRequest;
import com.getinsured.hix.model.plandisplay.PdSaveProviderPrefRequest;
import com.getinsured.hix.model.plandisplay.PlanAvailabilityForDrugRequest;
import com.getinsured.hix.model.plandisplay.PlanAvailabilityForDrugResponse;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ExchangeType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.Gender;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.PreferencesLevel;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.RequestType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ShoppingType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;
import com.getinsured.hix.model.plandisplay.PlanRecommendationRequest;
import com.getinsured.hix.model.plandisplay.ProviderBean;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.enums.OnOffEnum;
import com.getinsured.hix.platform.enums.YesNoEnum;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIException.GhixErrors;
import com.getinsured.plandisplay.model.cds.CostCalculatorRequest;
import com.getinsured.plandisplay.model.cds.PlanBenefits;
import com.getinsured.plandisplay.model.cds.PlanData;
import com.getinsured.plandisplay.service.AutoRenewalService;
import com.getinsured.plandisplay.service.GoodRxService;
import com.getinsured.plandisplay.service.IndividualPlanService;
import com.getinsured.plandisplay.service.PdDrugService;
import com.getinsured.plandisplay.service.PdHouseholdService;
import com.getinsured.plandisplay.service.PdOrderItemPersonService;
import com.getinsured.plandisplay.service.PdOrderItemService;
import com.getinsured.plandisplay.service.PdPreferencesService;
import com.getinsured.plandisplay.service.prescriptionSearch.PrescriptionData;
import com.getinsured.plandisplay.service.prescriptionSearch.PrescriptionFactory;
import com.getinsured.plandisplay.service.prescriptionSearch.PrescriptionRequest;
import com.getinsured.plandisplay.service.prescriptionSearch.PrescriptionSearch;
import com.getinsured.plandisplay.util.CreateDTOBean;
import com.getinsured.plandisplay.util.IND71Validator;
import com.getinsured.plandisplay.util.PlanDisplayAsyncUtil;
import com.getinsured.plandisplay.util.PlanDisplayConstants;
import com.getinsured.plandisplay.util.PlanDisplayUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


@Controller
@RequestMapping(value="/api")
public class PdMtController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PdMtController.class);
	
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private PdOrderItemService pdOrderItemService;
	@Autowired private PdOrderItemPersonService pdOrderItemPersonService;
	@Autowired private PlanDisplayUtil planDisplayUtil;	
	@Autowired private GIMonitorService giMonitorService;
	@Autowired private PlanDisplaySvcHelper planDisplaySvcHelper;
	@Autowired private CreateDTOBean createDTOBean;	
	@Autowired private PdPreferencesService pdPreferencesService;
	@Autowired private IndividualPlanService individualPlanService;
	@Autowired private Gson platformGson;	
	@Autowired private RestTemplate restTemplate;
	@Autowired private PdHouseholdService pdHouseholdService;
	@Autowired private GoodRxService goodRxService;
	@Autowired private PrescriptionFactory prescriptionFactory;
	@Autowired private PlanDisplayAsyncUtil planDisplayAsyncUtil;
	@Autowired private AutoRenewalService autoRenewalService;
	@Autowired private PdDrugService pdDrugService;

	@Autowired private GIWSPayloadService giwsPayloadService;
	@Autowired private IND71Validator iND71Validator;
	//@Autowired     private Validator validator;
	private String drxAuthKey;
	
	@PersistenceUnit 
	private EntityManagerFactory emf;
	
	@RequestMapping(value = "/cart/leads/{leadId}/cartItems", method = RequestMethod.GET)
	public @ResponseBody String findCartItemsByLeadId(@PathVariable("leadId") String leadId, @RequestHeader(value="apiKey") String apiKey){
		PdMtCartItemResponse pdMtCartItemResponse = new PdMtCartItemResponse();
		List<Map<String,String>> errorList = new ArrayList<Map<String,String>>();
		
		try {
			//String apiKey = (String) request.getHeader("apiKey");
			
			pdMtCartItemResponse = validateRequest(pdMtCartItemResponse, errorList, leadId, apiKey);
			Affiliate affiliate = findAffiliateByApiKey(apiKey);
			if(affiliate == null) {
				setValidateError(pdMtCartItemResponse, errorList, "apiKey is not valid");
				return platformGson.toJson(pdMtCartItemResponse);
			}
			
			if (pdMtCartItemResponse.getStatus() != null && pdMtCartItemResponse.getStatus() == 400) {
				return platformGson.toJson(pdMtCartItemResponse);
			}
			
			Long affiliateId = affiliate.getAffiliateId();
			
			pdMtCartItemResponse.setLeadId(leadId);
			String currentDateStr = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new TSDate());
			pdMtCartItemResponse.setTimeStamp(currentDateStr);
			
			if(leadId.matches("[0-9]+") == false){
				setValidateError(pdMtCartItemResponse, errorList, "Invalid lead id");
				return platformGson.toJson(pdMtCartItemResponse);
			}
			EligLead eligLead = planDisplayUtil.fetchEligLeadRecord(Long.parseLong(leadId));
			if(eligLead == null){
				setValidateError(pdMtCartItemResponse, errorList, "Invalid lead id");
				return platformGson.toJson(pdMtCartItemResponse);
			}
			if(eligLead.getAffiliateId() != affiliateId.longValue()){
				setValidateError(pdMtCartItemResponse, errorList, "Lead is not associated with this affiliate");
				return platformGson.toJson(pdMtCartItemResponse);
			}
						
			List<PdOrderItemDTO>  pdOrderItemDTOList = planDisplaySvcHelper.getPdOrderItemsByLeadId(leadId);
			List<PdMtCartItem> pdMtCartItemList = new ArrayList<PdMtCartItem>();			
			if(pdOrderItemDTOList != null && !pdOrderItemDTOList.isEmpty()){
				for(PdOrderItemDTO pdOrderItemDTO : pdOrderItemDTOList){
					PdMtCartItem pdMtCartItem = setCartItem(pdOrderItemDTO);
					pdMtCartItemList.add(pdMtCartItem);
				}
			}
			
			pdMtCartItemResponse.setStatus(ErrorCodes.SUCCESS.getId());
			pdMtCartItemResponse.setMessage(ErrorCodes.SUCCESS.getMessage());
			pdMtCartItemResponse.setCartItemList(pdMtCartItemList);
			
			return platformGson.toJson(pdMtCartItemResponse);

		} catch (Exception e) {
			setSystemError(pdMtCartItemResponse, errorList, e);
			return platformGson.toJson(pdMtCartItemResponse);
		}
	}
	
	@RequestMapping(value = "/cart/leads/{leadId}/cartItems/{cartItemId}", method = RequestMethod.GET)
	public @ResponseBody String findCartItemsByLeadId(@PathVariable("leadId") String leadId,@PathVariable("cartItemId") String cartItemId, @RequestHeader(value="apiKey") String apiKey){
		PdMtCartItemResponse pdMtCartItemResponse = new PdMtCartItemResponse();
		List<Map<String,String>> errorList = new ArrayList<Map<String,String>>();
		
		try {
			//String apiKey = (String) request.getHeader("apiKey");
			pdMtCartItemResponse = validateRequest(pdMtCartItemResponse, errorList, leadId, apiKey);
			Affiliate affiliate = findAffiliateByApiKey(apiKey);
			if(affiliate == null) {
				setValidateError(pdMtCartItemResponse, errorList, "apiKey is not valid");
				return platformGson.toJson(pdMtCartItemResponse);
			}
			
			if (pdMtCartItemResponse.getStatus() != null && pdMtCartItemResponse.getStatus() == 400) {
				return platformGson.toJson(pdMtCartItemResponse);
			}
			
			Long affiliateId = affiliate.getAffiliateId();
			
			pdMtCartItemResponse.setLeadId(leadId);
			String currentDateStr = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new TSDate());
			pdMtCartItemResponse.setTimeStamp(currentDateStr);
			
			if(leadId.matches("[0-9]+") == false){
				setValidateError(pdMtCartItemResponse, errorList, "Invalid lead id");
				return platformGson.toJson(pdMtCartItemResponse);
			}
			if(cartItemId.matches("[0-9]+") == false){
				setValidateError(pdMtCartItemResponse, errorList, "Invalid cart item id");
				return platformGson.toJson(pdMtCartItemResponse);
			}
			
			EligLead eligLead = planDisplayUtil.fetchEligLeadRecord(Long.parseLong(leadId));
			if(eligLead == null){
				setValidateError(pdMtCartItemResponse, errorList, "Invalid lead id");
				return platformGson.toJson(pdMtCartItemResponse);
			}
			if(eligLead.getAffiliateId() != affiliateId.longValue()){
				setValidateError(pdMtCartItemResponse, errorList, "Lead is not associated with this affiliate");
				return platformGson.toJson(pdMtCartItemResponse);
			}
			
			PdOrderItem pdOrderItem = pdOrderItemService.findById(Long.parseLong(cartItemId));
		    List<PdMtCartItem> pdMtCartItemList = new ArrayList<PdMtCartItem>();
		    if(pdOrderItem!=null && pdOrderItem.getPdHousehold() != null && pdOrderItem.getPdHousehold().getEligLeadId() != null && pdOrderItem.getPdHousehold().getEligLeadId().longValue() == eligLead.getId()){  
		    	PdOrderItemDTO pdOrderItemDTO = new PdOrderItemDTO();
		    	createDTOBean.copyProperties(pdOrderItemDTO, pdOrderItem); 
		    	pdOrderItemDTO.setPdHousehold(pdOrderItem.getPdHousehold());    
		    	PdMtCartItem pdMtCartItem = setCartItem(pdOrderItemDTO);
		    	pdMtCartItemList.add(pdMtCartItem);
		    }
			
			if(pdMtCartItemList.isEmpty()){
				setValidateError(pdMtCartItemResponse, errorList, "Invalid cart item id / Cart item is not associated with lead");
				return platformGson.toJson(pdMtCartItemResponse);
			}
			pdMtCartItemResponse.setStatus(ErrorCodes.SUCCESS.getId());
			pdMtCartItemResponse.setMessage(ErrorCodes.SUCCESS.getMessage());
			pdMtCartItemResponse.setCartItemList(pdMtCartItemList);
			
			return platformGson.toJson(pdMtCartItemResponse);

		} catch (Exception e) {
			setSystemError(pdMtCartItemResponse, errorList, e);
			return platformGson.toJson(pdMtCartItemResponse);
		}
	}
	
	@RequestMapping(value = "/getRecommendedPlans", method = RequestMethod.POST)
	public @ResponseBody List<IndividualPlan> getRecommendedPlans(@RequestBody PlanRecommendationRequest planRecommendationRequest) {
		List<IndividualPlan> recomemmendedPlans = new ArrayList<IndividualPlan>();
		
		//Form pdQuoteRequest
		PdQuoteRequest pdQuoteRequest = new PdQuoteRequest();
		pdQuoteRequest.setPdPreferencesDTO(planRecommendationRequest.getPdPreferencesDTO());
		pdQuoteRequest.setExchangeType(planRecommendationRequest.getExchangeType());
		pdQuoteRequest.setInsuranceType(planRecommendationRequest.getInsuranceType());
		pdQuoteRequest.setCurrentPlanId(planRecommendationRequest.getCurrentPlanId());
		pdQuoteRequest.setMinimizePlanData(planRecommendationRequest.isMinimizePlanData());
		
		pdQuoteRequest.setAffiliateIssuerHiosIdRestricted(OnOffEnum.ON);
		pdQuoteRequest.setShowPufPlans(YesNoEnum.NO);
		pdQuoteRequest.setTenant(planRecommendationRequest.getTenantCode());
		pdQuoteRequest.setShowCatastrophicPlan(false);
		
		List<PdPersonDTO> personList = new ArrayList<PdPersonDTO>();
		int memberCount = 1;
		for(PdMtCartItemMember pdMtCartItemMember: planRecommendationRequest.getPdMtCartItemMemberList()){
			
			PdPersonDTO pdPersonDTO = new PdPersonDTO();
			pdPersonDTO.setZipCode(planRecommendationRequest.getZipCode());
			pdPersonDTO.setCountyCode(planRecommendationRequest.getCountyCode());
			pdPersonDTO.setBirthDay(DateUtil.StringToDate(pdMtCartItemMember.getDob(), "MM/dd/yyyy"));
			pdPersonDTO.setCatastrophicEligible(YorN.N);
			pdPersonDTO.setRelationship(pdMtCartItemMember.getRelationship());
			pdPersonDTO.setSeekCoverage(YorN.Y);
			pdPersonDTO.setExternalId(""+memberCount);

			if(YesOrNo.YES.equals(pdMtCartItemMember.getTobacco())){
				pdPersonDTO.setTobacco(YorN.Y);
			}
			if(YesOrNo.NO.equals(pdMtCartItemMember.getTobacco())){
				pdPersonDTO.setTobacco(YorN.N);
			}
			
			personList.add(pdPersonDTO);
			memberCount++;
		}
		pdQuoteRequest.setPdPersonDTOList(personList);
		
		PdHouseholdDTO pdHouseholdDTO = new PdHouseholdDTO();
		pdHouseholdDTO.setCostSharing(planRecommendationRequest.getCostSharing());
		pdHouseholdDTO.setCoverageStartDate(DateUtil.StringToDate(planRecommendationRequest.getCoverageStartDate(), "MM/dd/yyyy"));
		pdHouseholdDTO.setEnrollmentType(EnrollmentType.I);
		pdHouseholdDTO.setMaxSubsidy(planRecommendationRequest.getMaxSubsidy());
		pdHouseholdDTO.setRequestType(RequestType.PRESHOPPING);
		pdHouseholdDTO.setShoppingType(ShoppingType.INDIVIDUAL);
		
		pdQuoteRequest.setPdHouseholdDTO(pdHouseholdDTO);
		
		if(planRecommendationRequest.getPdPreferencesDTO() == null){
			String defaultPref = planDisplayUtil.getDefaultPref();
			PdPreferencesDTO defaultPrefDTO = platformGson.fromJson(defaultPref, new TypeToken<PdPreferencesDTO>() {}.getType());
			pdQuoteRequest.setPdPreferencesDTO(defaultPrefDTO);
		}else{
			pdQuoteRequest.setPdPreferencesDTO(planRecommendationRequest.getPdPreferencesDTO());
		}
		
		List<IndividualPlan> individualPlanList = individualPlanService.getPlans(pdQuoteRequest);
		
		individualPlanList = planDisplaySvcHelper.setRecommendationRank(individualPlanList, planRecommendationRequest.getCurrentPlanId(), pdHouseholdDTO.getCoverageStartDate(), planRecommendationRequest.getExchangeType());
		recomemmendedPlans = planDisplaySvcHelper.filterNonRecommendedPlans(individualPlanList);
		
		return recomemmendedPlans;
	}
	
	private PdMtCartItem setCartItem(PdOrderItemDTO pdOrderItemDTO) {
		PdMtCartItem pdMtCartItem = new PdMtCartItem();

		setApplicationData(pdOrderItemDTO, pdMtCartItem);
		
		if(ApplicationStatus.ACTIVE.equals(pdMtCartItem.getStatus())){			
			if(InsuranceType.HEALTH.equals(pdMtCartItem.getInsuranceType())){
				setPrefereces(pdOrderItemDTO, pdMtCartItem);
			}
			
			setMemberList(pdOrderItemDTO, pdMtCartItem);
			setPlanData(pdOrderItemDTO.getPdHousehold().getCoverageStartDate(), pdOrderItemDTO.getPlanId().intValue(), pdMtCartItem);
		}
		return pdMtCartItem;
	}

	private PdMtCartItemResponse validateRequest(PdMtCartItemResponse pdMtCartItemResponse, List<Map<String, String>> errorList, String leadId, String apiKey) {
		if (leadId == null) {
			setValidateError(pdMtCartItemResponse, errorList, "Lead id not found in request URL");
			return pdMtCartItemResponse;
		}
		
		if (apiKey == null) {
			setValidateError(pdMtCartItemResponse, errorList, "apiKey not found in request");
			return pdMtCartItemResponse;
		} else {
			
			return pdMtCartItemResponse;
		}
	}

	private void setMemberList(PdOrderItemDTO pdOrderItemDTO, PdMtCartItem pdMtCartItem) {
		List<PdMtCartItemMember> pdMtCartItemMemberList = new ArrayList<PdMtCartItemMember>();
		List<PdOrderItemPerson> pdOrderItemPersonList = pdOrderItemPersonService.getByOrderItemId(pdOrderItemDTO.getId());
		for(PdOrderItemPerson pdOrderItemPerson : pdOrderItemPersonList){
			PdPerson pdPerson = pdOrderItemPerson.getPdPerson();
				
			PdMtCartItemMember pdMtCartItemMember = new PdMtCartItemMember();
			pdMtCartItemMember.setRelationship(pdPerson.getRelationship());
			pdMtCartItemMember.setZip(pdPerson.getZipCode());
			pdMtCartItemMember.setDob(DateUtil.dateToString(pdPerson.getBirthDay(), "MM/dd/yyyy"));
			pdMtCartItemMember.setSubscriberFlag(YorN.Y.equals(pdOrderItemPerson.getSubscriberFlag()) ? YesOrNo.YES : YesOrNo.NO );
			pdMtCartItemMember.setCountyCode(pdPerson.getCountyCode());
			if(pdPerson.getTobacco() != null){
				pdMtCartItemMember.setTobacco(YorN.Y.equals(pdPerson.getTobacco()) ? YesOrNo.YES : YesOrNo.NO);
			}
			if(pdPerson.getGender() != null){
				pdMtCartItemMember.setGender(Gender.M.equals(pdPerson.getGender()) ? MaleOrFemale.MALE : MaleOrFemale.FEMALE);
			}
			
			pdMtCartItemMemberList.add(pdMtCartItemMember);
		}
		pdMtCartItem.setMemberList(pdMtCartItemMemberList);
		
	}

	private void setApplicationData(PdOrderItemDTO pdOrderItemDTO, PdMtCartItem pdMtCartItem) {
		pdMtCartItem.setId(""+pdOrderItemDTO.getId());
		pdMtCartItem.setInsuranceType(pdOrderItemDTO.getInsuranceType());
		
		pdMtCartItem.setStatus(pdOrderItemDTO.getStatus()!=null && Status.DELETED.equals(pdOrderItemDTO.getStatus())?ApplicationStatus.DELETED:ApplicationStatus.ACTIVE);
		
		if(ApplicationStatus.ACTIVE.equals(pdMtCartItem.getStatus())){
			
			pdMtCartItem.setGrossPremium(pdOrderItemDTO.getPremiumBeforeCredit());
			pdMtCartItem.setAptc(pdOrderItemDTO.getMonthlySubsidy());
			pdMtCartItem.setNetPremium(pdOrderItemDTO.getPremiumAfterCredit().floatValue());
			
			String coverageStartDate = DateUtil.dateToString(pdOrderItemDTO.getPdHousehold().getCoverageStartDate(), "MM/dd/yyyy");
			pdMtCartItem.setCoverageStartDate(coverageStartDate);
			
			pdMtCartItem.setExchangeType(ExchangeType.OFF);
			if(pdOrderItemDTO.getExchangeType()!=null){
				pdMtCartItem.setExchangeType(pdOrderItemDTO.getExchangeType());
			}
		}
	}

	private void setPlanData(Date effectiveStartDate, int planId, PdMtCartItem pdMtCartItem) {
		String formattedCoverageDate = DateUtil.dateToString(effectiveStartDate, "yyyy-MM-dd");
		IndividualPlan individualPlan = planDisplayUtil.getPlanInfo(""+planId, formattedCoverageDate, false);
		
		//Calculate deductible according to number of members
		planDisplayUtil.calculateDeductible(individualPlan, individualPlan.getPlanCosts(), pdMtCartItem.getMemberList().size());
		
		pdMtCartItem.setPlanId(individualPlan.getIssuerPlanNumber());
		pdMtCartItem.setPlanName(individualPlan.getName());
		pdMtCartItem.setCarrierName(individualPlan.getIssuer());
		if(InsuranceType.HEALTH.equals(pdMtCartItem.getInsuranceType())){
			if(individualPlan.getPlanTier1() != null){
				if(individualPlan.getPlanTier1().get("PRIMARY_VISIT") != null){
					pdMtCartItem.setPrimaryCareVisit(individualPlan.getPlanTier1().get("PRIMARY_VISIT").get("displayVal"));
				}
				if(individualPlan.getPlanTier1().get("GENERIC") != null){
					pdMtCartItem.setGenericDrug(individualPlan.getPlanTier1().get("GENERIC").get("displayVal"));
				}          
			}
			if(individualPlan.getIntgMediDrugDeductible() != null){
				pdMtCartItem.setDeductible(individualPlan.getIntgMediDrugDeductible());
			}else if(individualPlan.getMedicalDeductible() != null || individualPlan.getDrugDeductible() != null){
				if(individualPlan.getMedicalDeductible() != null){
					pdMtCartItem.setDeductible(individualPlan.getMedicalDeductible());
				}
				if(individualPlan.getDrugDeductible() != null){
					pdMtCartItem.setDeductible(pdMtCartItem.getDeductible() + individualPlan.getDrugDeductible());
				}
			}
			pdMtCartItem.setProductType(individualPlan.getNetworkType());
			
			if(individualPlan.getLevel() != null){
				Map<String, PlanLevel> metalTierMap = new HashMap<String, PlanLevel>();
				metalTierMap.put("bronze", PlanLevel.BRONZE);
				metalTierMap.put("silver", PlanLevel.SILVER);
				metalTierMap.put("gold", PlanLevel.GOLD);
				metalTierMap.put("platinum", PlanLevel.PLATINUM);
				metalTierMap.put("catastrophic", PlanLevel.CATASTROPHIC);
				
				pdMtCartItem.setMetalTier(metalTierMap.get(individualPlan.getLevel().toLowerCase()));
			}
			
			if(individualPlan.getCostSharing()!=null){
				Map<String, CostSharing> csrLevelMap = new HashMap<String, CostSharing>();
				csrLevelMap.put("cs1", CostSharing.CS1);
				csrLevelMap.put("cs2", CostSharing.CS2);
				csrLevelMap.put("cs3", CostSharing.CS3);
				csrLevelMap.put("cs4", CostSharing.CS4);
				csrLevelMap.put("cs5", CostSharing.CS5);
				csrLevelMap.put("cs6", CostSharing.CS6);
				pdMtCartItem.setCsr(csrLevelMap.get(individualPlan.getCostSharing().toLowerCase()));
			}
			
			if(individualPlan.getHsa() != null){
				pdMtCartItem.setHsaCompatible("yes".equalsIgnoreCase(individualPlan.getHsa()) ? YesOrNo.YES : YesOrNo.NO);
			}
		}
		if(InsuranceType.STM.equals(pdMtCartItem.getInsuranceType()) || InsuranceType.DENTAL.equals(pdMtCartItem.getInsuranceType()) ){
			pdMtCartItem.setDeductible(individualPlan.getDeductible());
			pdMtCartItem.setProductType(individualPlan.getNetworkType());
		}
		if(InsuranceType.AME.equals(pdMtCartItem.getInsuranceType())){
			pdMtCartItem.setDeductible(individualPlan.getDeductible());
		}
		
	}

	private void setPrefereces(PdOrderItemDTO pdOrderItemDTO, PdMtCartItem pdMtCartItem) {
		if(pdOrderItemDTO != null){
			PdHousehold pdHousehold = pdOrderItemDTO.getPdHousehold();
			 PdPreferences pdPreferences = pdPreferencesService.findPreferencesByEligLeadId(pdHousehold.getEligLeadId());
			if(pdPreferences != null){
				PdPreferencesDTO pdPreferencesDTO = (PdPreferencesDTO)platformGson.fromJson(pdPreferences.getPreferences(), PdPreferencesDTO.class);
				if(pdPreferencesDTO != null){								
					Map<String,String> prefernecsMap = new HashMap<String,String>();
					if(pdPreferencesDTO.getMedicalUse() != null){
						Map<PreferencesLevel,String> primaryVisitMap = new HashMap<PreferencesLevel, String>();
						primaryVisitMap.put(PreferencesLevel.LEVEL1, "1-2");
						primaryVisitMap.put(PreferencesLevel.LEVEL2, "3-4");
						primaryVisitMap.put(PreferencesLevel.LEVEL3, "5-11");
						primaryVisitMap.put(PreferencesLevel.LEVEL4, "12 or more");
						prefernecsMap.put("doctorVisit", primaryVisitMap.get(pdPreferencesDTO.getMedicalUse()));
					}
					
					if(pdPreferencesDTO.getPrescriptionUse() != null){
						Map<PreferencesLevel,String> prescriptionMap = new HashMap<PreferencesLevel, String>();
						prescriptionMap.put(PreferencesLevel.LEVEL1, "0-2");
						prescriptionMap.put(PreferencesLevel.LEVEL2, "3-4");
						prescriptionMap.put(PreferencesLevel.LEVEL3, "5-11");
						prescriptionMap.put(PreferencesLevel.LEVEL4, "12 or more");
						prefernecsMap.put("prescriptions", prescriptionMap.get(pdPreferencesDTO.getPrescriptionUse()));
					}
					
					if(pdPreferencesDTO.getBenefits() != null ){
						prefernecsMap.put("benefits", StringUtils.join(pdPreferencesDTO.getBenefits(), ","));
					}
					pdMtCartItem.setPreferences(prefernecsMap);
				}
			}
		}
		
	}
	
	
	public List<SsapApplicationDTO> findApplicationsByLeadId(EligLead eligLead) throws Exception {
		List<SsapApplicationDTO> applicationDTOList = null;
		
		try {
			//String postParams = generateJSON(eligLead);
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(EligLead.class);
			String postParams = writer.writeValueAsString(eligLead);
			String response = ghixRestTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.GET_ALL_APPLICATIONS_BY_ELIG_LEAD, postParams,String.class);
			if(response != null){
				ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(MemberPortalResponseDTO.class);
				MemberPortalResponseDTO mpResponseDTO = reader.readValue(response);
				//MemberPortalResponseDTO mpResponseDTO = mapper.readValue(response, MemberPortalResponseDTO.class);
				return mpResponseDTO.getApplicationDTOList();
			}
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----PdMtController.findSsapApplicationDTOsByEligLead - Error in getting ssapApplications by eligLead---->", e, false);
			throw e;
		}
		return applicationDTOList;
	}
	
	private Affiliate findAffiliateByApiKey(String apiKey) {
		String url = String.format(GhixEndPoints.AffiliateServiceEndPoints.GET_AFFILIATE_BY_APIKEY, apiKey);
		Affiliate affiliate = null;
		try{
			affiliate = ghixRestTemplate.getForObject(url,Affiliate.class);
		} catch (Exception e){
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "----Exception occured while fetching record from Affilaite----", e, false);
		}
		return affiliate;
	}

	private void setSystemError(PdMtCartItemResponse pdMtCartItemResponse, List<Map<String, String>> errorList, Exception e) {
		pdMtCartItemResponse.setStatus(ErrorCodes.SYSTEM_ERROR.getId());
		pdMtCartItemResponse.setMessage(ErrorCodes.SYSTEM_ERROR.getMessage());
		Map<String,String> errorDetail = new HashMap<String,String>();
		if(e != null){
			GIMonitor gimonitor = giMonitorService.saveOrUpdateErrorLog("PD", new TSDate(), "PdMtController", ExceptionUtils.getFullStackTrace(e), null);
			String gimonitorId = gimonitor.getId() !=null ? gimonitor.getId().toString() : null;
			errorDetail.put("errorMonitorId", gimonitorId);
			errorDetail.put("errorMessage", "Exception occured while processing the request");
			errorList.add(errorDetail);
		}
		pdMtCartItemResponse.setErrors(errorList);
	}

	private void setValidateError(PdMtCartItemResponse pdMtCartItemResponse, List<Map<String, String>> errorList, String errorMsg) {
		pdMtCartItemResponse.setStatus(ErrorCodes.VALIDATION_ERROR.getId());
		pdMtCartItemResponse.setMessage(ErrorCodes.VALIDATION_ERROR.getMessage());
		Map<String,String> errorDetail = new HashMap<String,String>();
		errorDetail.put("description", errorMsg);
		errorList.add(errorDetail);
		pdMtCartItemResponse.setErrors(errorList);
		
	}
	
	 	
	@RequestMapping(value = "/savePdPreferences", method = RequestMethod.POST)
	public @ResponseBody String savePdPreferences(@RequestBody PdPreferencesDTO pdPreferencesDTO, @RequestHeader(value="apiKey") String apiKey) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside savePdPreferences SERVICE method---->", null, false);
		PdMtCartItemResponse pdMtCartItemResponse = new PdMtCartItemResponse();
		List<Map<String,String>> errorList = new ArrayList<Map<String,String>>();
		
		try {
			if(pdPreferencesDTO == null || pdPreferencesDTO.getEligLeadId() == null) {
				setValidateError(pdMtCartItemResponse, errorList, "pdPreferencesDTO is not valid");
				return platformGson.toJson(pdMtCartItemResponse);
			}
			//String apiKey = (String) request.getHeader("apiKey");
			String eligLeadId = String.valueOf(pdPreferencesDTO.getEligLeadId());
			pdMtCartItemResponse = validateRequest(pdMtCartItemResponse, errorList, eligLeadId, apiKey);
			Affiliate affiliate = findAffiliateByApiKey(apiKey);
			if(affiliate == null) {
				setValidateError(pdMtCartItemResponse, errorList, "apiKey is not valid ");
				return platformGson.toJson(pdMtCartItemResponse);
			}	
			if (pdMtCartItemResponse.getStatus() != null && pdMtCartItemResponse.getStatus() == 400) {
				return platformGson.toJson(pdMtCartItemResponse);
			}
			
			pdMtCartItemResponse.setLeadId(eligLeadId);
			String planPref = platformGson.toJson(pdPreferencesDTO);
			PdPreferences pdPreferences = pdPreferencesService.findPreferencesByEligLeadId(Long.valueOf(Long.parseLong(eligLeadId)));
			if(pdPreferences!=null){
				if (pdPreferencesDTO.getEligLeadId() != null) {
					pdPreferences.setEligLeadId(pdPreferencesDTO.getEligLeadId());
				}
				if (pdPreferencesDTO.getProviders() != null) {
					pdPreferences.setDoctors(pdPreferencesDTO.getProviders());
				}
				if (pdPreferencesDTO.getPrescriptions() != null) {
					pdPreferences.setPrescriptions(pdPreferencesDTO.getPrescriptions());
				}
				pdPreferences.setPreferences(planPref);
			}else{
				pdPreferences = new PdPreferences();
		        pdPreferences.setEligLeadId(pdPreferencesDTO.getEligLeadId());
		        pdPreferences.setDoctors(pdPreferencesDTO.getProviders());
		        pdPreferences.setPrescriptions(pdPreferencesDTO.getPrescriptions());
		        pdPreferences.setPreferences(planPref);
			}		
			pdPreferencesService.save(pdPreferences);
			pdMtCartItemResponse.setStatus(Integer.valueOf(PdMtCartItemResponse.ErrorCodes.SUCCESS.getId()));
			pdMtCartItemResponse.setMessage(PdMtCartItemResponse.ErrorCodes.SUCCESS.getMessage());
			return platformGson.toJson(pdMtCartItemResponse);
			
		} catch (Exception e) {
			setSystemError(pdMtCartItemResponse, errorList, e);
			return platformGson.toJson(pdMtCartItemResponse);
		}
					
	}
	
	@RequestMapping(value = "/getPreferencesByEligLeadId/{eligLeadId}", method = RequestMethod.GET)
	public @ResponseBody String getPreferencesByEligLeadId(@PathVariable("eligLeadId") String eligLeadId, @RequestHeader(value="apiKey") String apiKey) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside getPreferencesByEligLeadId SERVICE method---->", null, false);
		PdMtCartItemResponse pdMtCartItemResponse = new PdMtCartItemResponse();
		List<Map<String,String>> errorList = new ArrayList<Map<String,String>>();
		
		try {
			
			//String apiKey = (String) request.getHeader("apiKey");
			pdMtCartItemResponse = validateRequest(pdMtCartItemResponse, errorList, eligLeadId, apiKey);
			Affiliate affiliate = findAffiliateByApiKey(apiKey);
			if(affiliate == null) {
				setValidateError(pdMtCartItemResponse, errorList, "apiKey is not valid");
				return platformGson.toJson(pdMtCartItemResponse);
			}
			
			if (pdMtCartItemResponse.getStatus() != null && pdMtCartItemResponse.getStatus() == 400) {
				return platformGson.toJson(pdMtCartItemResponse);
			}
			
			Long leadId = Long.parseLong(eligLeadId);
			
			pdMtCartItemResponse = getPreferencesFromLeadId(leadId, pdMtCartItemResponse, errorList);
			if (pdMtCartItemResponse.getStatus() != null && pdMtCartItemResponse.getStatus() == 400) {
				return platformGson.toJson(pdMtCartItemResponse);
			}
			
			return platformGson.toJson(pdMtCartItemResponse);
			
		}catch (NumberFormatException e) {
			setValidateError(pdMtCartItemResponse, errorList, "Lead Id is not valid");
			return platformGson.toJson(pdMtCartItemResponse);
		} 
		catch (Exception e) {
			setSystemError(pdMtCartItemResponse, errorList, e);
			return platformGson.toJson(pdMtCartItemResponse);
		}
	}	
	
	private String getDRXAuthKey() throws GIException
	{
		long startTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "----getDRXAuthKey started----", null, false);
		if (drxAuthKey != null)
		{
			return drxAuthKey;
		}
		String authKey  = null;
		String drxAPIKey = DynamicPropertiesUtil.getPropertyValue("global.Drx.ApiKey");
		String drxAPISecret = DynamicPropertiesUtil.getPropertyValue("global.Drx.ApiSecret");
		String drxAuthUrl = DynamicPropertiesUtil.getPropertyValue("global.Drx.AuthUrl");
		if (StringUtils.isEmpty(drxAPISecret) || StringUtils.isEmpty(drxAPIKey))
		{
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "----Exception occured while fetching DRX Auth Keys from config----", null, false);
			throw new GIException(GhixErrors.DRX_AUTH_CONFIG_ERROR);
		}
		try {
			String tokenKey =  drxAPIKey + ":" + drxAPISecret ;
			String encToken = new String (Base64Utils.encode(tokenKey.getBytes()));
			
			HttpHeaders headers = new HttpHeaders();
			headers.set("Authorization", "Basic " + encToken);	
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

			MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<String, String>();
			bodyMap.add("grant_type", "client_credentials");
			HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(bodyMap, headers);
			String responseJson = restTemplate.postForObject(drxAuthUrl, requestEntity, String.class);
			if (!StringUtils.isEmpty(responseJson))
			{
				JSONParser parser = new JSONParser();
				JSONObject json = (JSONObject) parser.parse(responseJson);		
				authKey = (String)json.get("access_token");
				
				drxAuthKey = authKey;
			}
		} catch (Exception e) {
			Throwable ex = e.getCause();
			if(ex instanceof HttpClientErrorException){
				throw new GIException(ex.getMessage(), ex);
			}else {
				throw new GIException(e);
			}
		}
		long endTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "getDRXAuthKey ended in " + (endTime - startTime) + " ms", null, false);	
		return authKey;
	}
	
	private ResponseEntity<String> callDrxService(String url, HttpMethod method) throws GIException, Exception {
		long startTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "----callDrxService started----", null, false);
		ResponseEntity<String> responseEntity = null;
		HttpEntity entity = getAuthEntity();
		try{
			responseEntity =  restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
		}
		catch (Exception e) {
			if (e instanceof HttpClientErrorException) {
				HttpClientErrorException clError = (HttpClientErrorException) e;
				if(clError.getStatusCode().equals(HttpStatus.UNAUTHORIZED) || clError.getStatusCode().equals(HttpStatus.BAD_REQUEST))
				{
					entity = resetAndGetAuthEntity();
					responseEntity =  restTemplate.exchange(url, method, entity, String.class);
				}
				else
				{
					throw new GIException(clError.getStatusCode().value(),clError.getResponseBodyAsString(),null);
				}
			}
		}
		long endTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "callDrxService ended in " + (endTime - startTime) + " ms", null, false);	
		return responseEntity;
	}
	
	@RequestMapping(value = "/drugs/genericRxcui", method = RequestMethod.GET)
	public @ResponseBody String getRxcuiByNameAndStrength(HttpServletRequest request, HttpServletResponse response)
	{
		String drugSearchAPIConfig = DynamicPropertiesUtil.getPropertyValue("planSelection.drugSearch.API");
		if("GI".equalsIgnoreCase(drugSearchAPIConfig)){
			response.setHeader("FailureReason", "Not required");
			response.setStatus(404);
			return platformGson.toJson("");
		}
		String brandDrugName = (String) request.getParameter("name");
		String brandDrugStrength = (String) request.getParameter("strength");
		String drugType = null;
		JSONObject responseObj = new JSONObject();
		GoodRxResponseDTO goodRxResponseDTO = goodRxService.getFairPrice(brandDrugName);
		if(goodRxResponseDTO!=null && goodRxResponseDTO.isSuccess() && goodRxResponseDTO.getData()!=null){
			DrugDetailsDTO drugDetailsDTO = goodRxResponseDTO.getData();
			if(drugDetailsDTO != null) {
				if(drugDetailsDTO.getBrand() != null){
					for(String drugName: drugDetailsDTO.getBrand()) {
						if(drugName.equalsIgnoreCase(brandDrugName)) {
							drugType = "Brand";
							responseObj.put("drugType", drugType);
						}
					}	
				}
				if("Brand".equalsIgnoreCase(drugType)) {
					if(drugDetailsDTO.getGeneric() != null && drugDetailsDTO.getGeneric().size() > 0){
						String genericName = drugDetailsDTO.getGeneric().get(0);
						
						if(StringUtils.isNotBlank(genericName)) {
							responseObj.put("genericName", genericName);
							Map<String, Object> marketPlaceAPIResponseObj = planDisplayUtil.callMarketPlaceDrugSearch(genericName);
							List<Map<String, Object>> drugList =  (List<Map<String, Object>>) marketPlaceAPIResponseObj.get("drugs");
							
							if(drugList != null && drugList.size() > 0) {
								for(Map<String, Object> drug : drugList) {
									String name = (String) drug.get("name");
									if(name != null && genericName.equalsIgnoreCase(name)) {		
										String strength = (String) drug.get("strength");
										if(strength != null && brandDrugStrength.equalsIgnoreCase(strength)) {	
											responseObj.put("genericRxcui", (String)drug.get("rxcui"));
											responseObj.put("genericDosage", (String)drug.get("name") + " " + (String)drug.get("strength") + " " + (String)drug.get("rxnorm_dose_form"));
										}
									}
								}
							}
						}
					}
				} else {
					responseObj.put("drugType", "Generic");
				}
			}
		}
		
		
		return responseObj.toJSONString();
	}
	
	@RequestMapping(value = "/drugs", method = RequestMethod.GET)
	public @ResponseBody String searchByDrugId(HttpServletRequest request, HttpServletResponse response)
	{
		/*
		 * 1 API call to get brand details by id
		 * 2 extract the generic name and id from it
		 * 3 extract the dosage list of it
		 * 4 API call to get generic drug details
		 * 5 extract the dosage list of it
		 * 6 prepare dosage list based on the generic dosages
		 * 7 for the dto and send it back as response
		 */
		long startTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "----searchByDrugId started----", null, false);
		String drugId = (String) request.getParameter("drugId");
		String originalDrugId = (String) request.getParameter("drugId");
		List<DrugDosageDTO> dosages = new ArrayList<DrugDosageDTO>();
		try {
			String drugSearchAPIConfig = DynamicPropertiesUtil.getPropertyValue("planSelection.drugSearch.API");
			if("GoodRx".equalsIgnoreCase(drugSearchAPIConfig)){
				Map<String, Object> marketPlaceAPIResponseObj = planDisplayUtil.callMarketPlaceDrugSearch(drugId);
				List<Map<String, Object>> drugList =  (List<Map<String, Object>>) marketPlaceAPIResponseObj.get("drugs");
				
				if(drugList == null || drugList.size() <= 0) {
					if(drugId.indexOf('/') > -1) {
						drugId = drugId.substring(drugId.indexOf('/')+1, drugId.length()) +"/"+ drugId.substring(0, drugId.indexOf('/'));
					}
					
					marketPlaceAPIResponseObj = planDisplayUtil.callMarketPlaceDrugSearch(drugId);
					drugList =  (List<Map<String, Object>>) marketPlaceAPIResponseObj.get("drugs");
				}
				/* HIX-107287 Multi-Component Drug Defect - Quinidine Returns Unrelated Results if it is Only the First Name of the Drug
				if(drugList == null || drugList.size() <= 0) {
					drugId = originalDrugId;
					if(drugId.indexOf(' ') > -1) {
						drugId = drugId.substring(0, drugId.indexOf(' '));
					}
					marketPlaceAPIResponseObj = planDisplayUtil.callMarketPlaceDrugSearch(drugId);
					drugList =  (List<Map<String, Object>>) marketPlaceAPIResponseObj.get("drugs");
				}
				
				if(drugList == null || drugList.size() <= 0) {
					if(drugId.indexOf('/') > -1) {
						drugId = drugId.substring(0, drugId.indexOf('/'));
					}
					
					marketPlaceAPIResponseObj = planDisplayUtil.callMarketPlaceDrugSearch(drugId);
					drugList =  (List<Map<String, Object>>) marketPlaceAPIResponseObj.get("drugs");
				}
				*/
				if(drugList != null && drugList.size() > 0) {
					boolean matchOnName = false;
					for(Map<String, Object> drug : drugList) {
						if(drugId.equalsIgnoreCase((String) drug.get("name"))) {
							dosages.add(formMarketPlaceDrugDTO(drug));
							matchOnName = true;
						} 
					}
					if(!matchOnName) {
						for(Map<String, Object> drug : drugList) {
							if(drugId.indexOf(' ') > -1) {
								drugId = drugId.substring(0, drugId.indexOf(' '));
							}
							if(drugId.indexOf('/') > -1) {
								drugId = drugId.substring(0, drugId.indexOf('/'));
							}
							String drugName = (String) drug.get("name");
							if(StringUtils.isNotBlank(drugName) && StringUtils.startsWithIgnoreCase(drugName,drugId)){
								dosages.add(formMarketPlaceDrugDTO(drug));
							}
						}
					}
				}
			} else if("GI".equalsIgnoreCase(drugSearchAPIConfig)){
				String query = "select d1.rxcui, d1.name, d1.strength, d1.rxterm_dosage_form, " + 
						"d1.rxnorm_dosage_form, d1.full_name,d1.type, d2.rxcui as generic_rxcui, d2.name as generic_name " + 
						"from drugdata d1 left join drugdata d2 on d1.generic_rxcui = d2.rxcui and d2.status='ACTIVE'" + 
						"where d1.status='ACTIVE' and lower(d1.name) = :drugId";
				EntityManager em = emf.createEntityManager();
				String drugIdParam = drugId.toLowerCase();
				Query queryObj = em.createNativeQuery(query);
				queryObj.setParameter("drugId", drugIdParam);
				
	    			List<Object[]> giDosages = (List<Object[]>) queryObj.getResultList();
	    			em.clear();
	    			em.close();
	    			em = null;
				if (giDosages != null && giDosages.size() > 0) {
					for (Object[] row : giDosages){
						DrugDosageDTO dosageDTO = new DrugDosageDTO();
						dosageDTO.setDrugDosageID((String)row[0]);
						String strength = (String)row[2];
						String drugName = (String)row[1] + " " + strength;
						String rxtermDosage = (String)row[3];
						String rxnormDosage = (String)row[4];
						
						if(StringUtils.isNotBlank(rxtermDosage)) {
							if(StringUtils.isNotBlank(rxnormDosage)) {
								if(rxtermDosage.equalsIgnoreCase(rxnormDosage)) {
									drugName += " (" + rxnormDosage + ")";
								} else {
									drugName += " " + rxtermDosage + " (" + rxnormDosage + ")";
								}
							}else {
								drugName += " " + rxtermDosage;
							}
						}else if (StringUtils.isNotBlank(rxnormDosage)) {
							drugName += " (" + rxnormDosage + ")";
						}
						
						
						if(StringUtils.isNotBlank(strength) && !isDrugNamePresent(drugName, dosages)) {
							dosageDTO.setDrugDosageName(drugName);
						}else {
							dosageDTO.setDrugDosageName((String)row[5]);
						}
						dosageDTO.setDrugFullName((String)row[5]);
						dosageDTO.setDrugRxCui((String)row[0]);
						String genericRxCui = (String)row[7];
						if(StringUtils.isNotBlank(genericRxCui)) {
							dosageDTO.setGenericDosageID(genericRxCui);
							dosageDTO.setGenericDosageName((String)row[8]);
						}
						dosages.add(dosageDTO);
					}
				}
				
			} else {
				String drxServiceUrl = DynamicPropertiesUtil.getPropertyValue("global.Drx.ServiceUrl");
				ResponseEntity<String>  responseEntity = callDrxService(drxServiceUrl + "/APITools/v1/Drugs/"+drugId, HttpMethod.GET);
				if (responseEntity != null) {
					DrxPrescriptionSearchDTO brandPrescription = (DrxPrescriptionSearchDTO) platformGson
							.fromJson(responseEntity.getBody(), DrxPrescriptionSearchDTO.class);
					if (brandPrescription != null && brandPrescription.getDosages() != null
							&& brandPrescription.getDosages().size() > 0) {
						for (DrxDosageDTO dosage : brandPrescription.getDosages()) {
							DrugDosageDTO dosageDTO = new DrugDosageDTO();
							dosageDTO.setDrugDosageID(dosage.getDosageID());
							dosageDTO.setDrugDosageName(dosage.getLabelName());
							dosageDTO.setDrugNDC(dosage.getReferenceNDC());
							dosageDTO.setCommonDaysOfSupply(dosage.getCommonDaysOfSupply());
							if (dosage.getIsCommonDosage()) {
								dosageDTO.setCommonDosage(dosage.getIsCommonDosage());
							}
							if (StringUtils.isNotBlank(dosage.getGenericDosageID())) {
								dosageDTO.setGenericDosageID(dosage.getGenericDosageID());
							}
							dosages.add(dosageDTO);
						}
					}
					if (brandPrescription != null && "Brand".equals(brandPrescription.getDrugType())
							&& brandPrescription.getGenericDrugID() != null && dosages.size() > 0) {
						responseEntity = callDrxService(
								drxServiceUrl + "/APITools/v1/Drugs/" + brandPrescription.getGenericDrugID(),
								HttpMethod.GET);
						DrxPrescriptionSearchDTO genericPrescription = (DrxPrescriptionSearchDTO) platformGson
								.fromJson(responseEntity.getBody(), DrxPrescriptionSearchDTO.class);

						if (dosages.size() > 0 && genericPrescription != null
								&& genericPrescription.getDosages() != null
								&& genericPrescription.getDosages().size() > 0) {
							for (DrugDosageDTO dosageDTO : dosages) {
								for (DrxDosageDTO genericDosage : genericPrescription.getDosages()) {
									if (genericDosage.getDosageID().equalsIgnoreCase(dosageDTO.getGenericDosageID())) {
										dosageDTO.setGenericDosageName(genericDosage.getLabelName());
										dosageDTO.setGenericNDC(genericDosage.getReferenceNDC());
									}
								}
							}
						}
					} 
				}
			}
		}
		catch (GIException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "----Exception occured while performing DRX drug Search by Id for drugId :" + drugId, e, false);
			// Add failure Headers
			response.setHeader("FailureReason", (e.getErrorMsg()!= null ? e.getErrorMsg() : e.getMessage()));
			response.setStatus(e.getErrorCode());
		}
		catch (Exception e) {
			if(e instanceof HttpClientErrorException){
				HttpClientErrorException statCodeExcp = (HttpClientErrorException) e;
				String responseBody = statCodeExcp.getResponseBodyAsString();
				response.setHeader("FailureReason", responseBody);
				response.setStatus(521);
			}else{
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "----Exception occured while performing DRX drug Search by Id for drugId :" + drugId, e, false);
				// Add failure Headers
				response.setHeader("FailureReason", e.getMessage());
				response.setStatus(521);
			}
		}
		long endTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "searchByDrugId ended in " + (endTime - startTime) + " ms", null, false);
		return platformGson.toJson(dosages);
	}

	private boolean isDrugNamePresent(String drugName, List<DrugDosageDTO> dosages) {
		if(dosages != null && dosages.size() >0) {
			for(DrugDosageDTO drugDosage: dosages) {
				if(drugName.equalsIgnoreCase(drugDosage.getDrugDosageName())) {
					drugDosage.setDrugDosageName(drugDosage.getDrugFullName());
					return true;
				}
			}
		}
		return false;
	}

	private DrugDosageDTO formMarketPlaceDrugDTO(Map<String, Object> drug) {
		DrugDosageDTO dosageDTO = new DrugDosageDTO();
		dosageDTO.setDrugDosageID((String)drug.get("rxcui"));
		dosageDTO.setDrugDosageName((String)drug.get("name") + " " + (String)drug.get("strength") + " " + (String)drug.get("rxterms_dose_form") + " (" + (String)drug.get("rxnorm_dose_form")+")");
		dosageDTO.setDrugRxCui((String)drug.get("rxcui"));
		dosageDTO.setStrength((String)drug.get("strength"));
		return dosageDTO;
	}

	private HttpEntity resetAndGetAuthEntity() throws GIException {
		this.drxAuthKey = null;
		return getAuthEntity();
	}

	private HttpEntity getAuthEntity() throws GIException {
		String authKey  = getDRXAuthKey();
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + authKey);	
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		return new HttpEntity(headers);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/drugs/genericName", method = RequestMethod.GET)
	public @ResponseBody String getGenericDrugName(HttpServletRequest request, HttpServletResponse response) {
		String brandDrugName = (String) request.getParameter("name");
		JSONObject responseObj = new JSONObject();
		try{			
			GoodRxResponseDTO goodRxResponseDTO = goodRxService.getFairPrice(brandDrugName);
			
			if(goodRxResponseDTO!=null && goodRxResponseDTO.isSuccess() && goodRxResponseDTO.getData()!=null){
				DrugDetailsDTO drugDetailsDTO = goodRxResponseDTO.getData();
				if(drugDetailsDTO.getBrand() != null && (drugDetailsDTO.getBrand().stream().anyMatch(brandDrugName::equalsIgnoreCase))){
					responseObj.put("drugType", "Brand");
					if(CollectionUtils.isNotEmpty(drugDetailsDTO.getGeneric())){
						String genericName = drugDetailsDTO.getGeneric().get(0);
							
						if(StringUtils.isNotBlank(genericName)) {
							responseObj.put("genericName", genericName);
						}
					}
				}
					
				if(drugDetailsDTO.getGeneric() != null && (drugDetailsDTO.getGeneric().stream().anyMatch(brandDrugName::equalsIgnoreCase))){
					responseObj.put("drugType", "Generic");
				}
			}
		} catch (Exception e){
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "----Exception occured while fetching fairPrice----", e, false);
			response.setHeader("FailureReason", e.getMessage());
			response.setStatus(521);
		}
		return responseObj.toJSONString();
	}
	
	@RequestMapping(value = "/drugs/search", method = RequestMethod.GET)
	public @ResponseBody String searchByDrugName(HttpServletRequest request, HttpServletResponse response) {
		long startTime = TimeShifterUtil.currentTimeMillis();
		EntityManager em = null;
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "----searchByDrugName started----", null, false);
		String searchTerm =  (String) request.getParameter("name");
		if(StringUtils.isBlank(searchTerm))
		{
			return "";
		}
		List<DrugSearchDTO> drugSearchList = new ArrayList<DrugSearchDTO>();
		try {
			String drugSearchAPIConfig = DynamicPropertiesUtil.getPropertyValue("planSelection.drugSearch.API");
			if("GoodRx".equalsIgnoreCase(drugSearchAPIConfig)){
				GoodRxResponseDTO goodRxResponseDTO = goodRxService.getDrugNames(searchTerm);
				if (goodRxResponseDTO != null && goodRxResponseDTO.getData() != null &&  goodRxResponseDTO.getData().getCandidates() != null)
				{
					  for (String drugName : goodRxResponseDTO.getData().getCandidates())
					  {
						  if(StringUtils.containsIgnoreCase(drugName, searchTerm)) {
							  DrugSearchDTO drugSearchDTO = new DrugSearchDTO();
							  drugSearchDTO.setDrugID(drugName);
							  drugSearchDTO.setDrugName(drugName);
							  drugSearchList.add(drugSearchDTO);
						  }
					  }
				}
			} else if("GI".equalsIgnoreCase(drugSearchAPIConfig)){
				List<String> searchTermList = Arrays.asList(searchTerm.split(" ")); 
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "searchTerm" + searchTerm + " split length" + searchTermList.size(), null, false);
				if(searchTermList != null && searchTermList.size() > 0) {
					ArrayList<String> paramList = new ArrayList<String>();
					StringBuilder buildquery = new StringBuilder();
					List<Object[]> matchedNames = null;
					buildquery.append("select distinct lower(name), type from drugdata where status='ACTIVE' ");
					for(String word : searchTermList) {
						if(StringUtils.isNotBlank(word)) {
							buildquery.append(" and lower(name) SIMILAR TO :param_"  + paramList.size());
							paramList.add( "(" + word.toLowerCase() + "|%(/| )" + word.toLowerCase() + ")%" );
						}
					}
					if(paramList.size() > 0) {
						em = emf.createEntityManager();
						Query queryObj = em.createNativeQuery(buildquery.toString());
						for (int i = 0; i < paramList.size(); i++) {
							queryObj.setParameter("param_" + i, paramList.get(i));
						}
						matchedNames = (List<Object[]>) queryObj.getResultList();
						if (matchedNames != null && matchedNames.size() > 0){
							for(Object[] row : matchedNames){
								String drugName = (String) row[0];
								if(StringUtils.isNotBlank(drugName)) {
									DrugSearchDTO drugSearchDTO = new DrugSearchDTO();
									drugSearchDTO.setDrugID(drugName);
									drugSearchDTO.setDrugName(drugName);
									String drugType = (String) row[1];
									if(drugType != null && StringUtils.isNotBlank(drugType)) {
										drugSearchDTO.setDrugType(drugType);
									}
									drugSearchList.add(drugSearchDTO);
								}
							}
						}
					}
				}
			} else {
				String drxServiceUrl = DynamicPropertiesUtil.getPropertyValue("global.Drx.ServiceUrl");
				HttpEntity<String> responseEntity = callDrxService(drxServiceUrl + "/APITools/v1/Drugs/Search?q=" + searchTerm, HttpMethod.GET);
				if (responseEntity != null) {
					Type type = new TypeToken<List<DrxPrescriptionSearchDTO>>() {
					}.getType();
					List<DrxPrescriptionSearchDTO> drxDrugList = platformGson.fromJson(responseEntity.getBody(), type);
					if (drxDrugList != null && drxDrugList.size() > 0) {
						for (DrxPrescriptionSearchDTO drxPrescriptionSearchDTO : drxDrugList) {
							DrugSearchDTO drugSearchDTO = new DrugSearchDTO();
							drugSearchDTO.setDrugID(drxPrescriptionSearchDTO.getDrugID());
							drugSearchDTO.setDrugName(drxPrescriptionSearchDTO.getDrugName());
							drugSearchDTO.setDrugType(drxPrescriptionSearchDTO.getDrugType());
							if (drxPrescriptionSearchDTO.getGenericDrugID() != null) {
								drugSearchDTO.setGenericID(drxPrescriptionSearchDTO.getGenericDrugID());
							}
							drugSearchList.add(drugSearchDTO);
						}
					} 
				}
			}
		}
		catch (Exception e) {
			Throwable ex = e.getCause();
			if(ex instanceof HttpClientErrorException){
				HttpClientErrorException statCodeExcp = (HttpClientErrorException) ex;
				String responseBody = statCodeExcp.getResponseBodyAsString();
				response.setHeader("FailureReason", responseBody);
				response.setStatus(521);
			}else{
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "----Exception occured while performing DRX drug Search by Name for searchTerm----" + searchTerm, e, false);
				response.setHeader("FailureReason", e.getMessage());
				response.setStatus(521);
			}
		}
		finally {
			if(null != em && em.isOpen()) {
				em.clear();
				em.close();
				em = null;
			}
		}
		long endTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "searchByDrugName ended in " + (endTime - startTime) + " ms", null, false);
		return platformGson.toJson(drugSearchList);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/drugs/properties", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody DrugPropertiesResponseDTO getDrugProperties(@RequestBody DrugPropertiesRequestDTO drugPropertiesRequest, HttpServletResponse response) {
		long startTime = TimeShifterUtil.currentTimeMillis();
		DrugPropertiesResponseDTO drugPropertiesResponse = null;
		if(LOGGER.isInfoEnabled()) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "----getDrugProperties started----", null, false);
		}
		if(drugPropertiesRequest != null && CollectionUtils.isNotEmpty(drugPropertiesRequest.getDrugRxCodeList())) {
			EntityManager entityManager = null;
			try {
				String drugSearchAPIConfig = DynamicPropertiesUtil.getPropertyValue("planSelection.drugSearch.API");
				if(StringUtils.isBlank(drugSearchAPIConfig) || "GI".equalsIgnoreCase(drugSearchAPIConfig)){
					String query = "select rxcui, name, strength, full_name, type, generic_rxcui from drugdata where rxcui IN (:rxcuiList)";
					entityManager = emf.createEntityManager();
					Query queryObj = entityManager.createNativeQuery(query);
					queryObj.setParameter("rxcuiList", drugPropertiesRequest.getDrugRxCodeList());
					List<Object[]> drugList = (List<Object[]>) queryObj.getResultList();
					drugPropertiesResponse = new DrugPropertiesResponseDTO();
					if (drugList != null && drugList.size() > 0){
						for(Object[] row : drugList){
							if(row[0] != null) {
								String drugName = (row[1] != null) ? (String) row[1] : PlanDisplayConstants.EMPTY;
								String drugStrength = (row[2] != null) ? (String) row[2] : PlanDisplayConstants.EMPTY;
								String drugFullName = (row[3] != null) ? (String) row[3] : PlanDisplayConstants.EMPTY;
								String drugType = (row[4] != null) ? (String) row[4] : PlanDisplayConstants.EMPTY;
								String geneticId = (row[5] != null) ? (String) row[5] : PlanDisplayConstants.EMPTY;
								DrugSearchDTO drugSearchDTO = new DrugSearchDTO();
								drugSearchDTO.setDrugID((String) row[0]);
								drugSearchDTO.setDrugName(drugName);
								drugSearchDTO.setDrugFullName(drugFullName);
								drugSearchDTO.setDrugType(drugType);
								drugSearchDTO.setGenericID(geneticId);
								drugSearchDTO.setStrength(drugStrength);
								drugPropertiesResponse.addDrugToList(drugSearchDTO);
							}
						  }
					} else {
						if(LOGGER.isInfoEnabled()) {
							LOGGER.info("No records found for DrugData for RXCUI : " + drugPropertiesRequest.getDrugRxCodeList());
						}
					}
				} else {
					LOGGER.error("getDrugProperties is Not Implemeted for configuration: " + drugSearchAPIConfig);
					response.setHeader("FailureReason", "Not configured correctly");
					response.setStatus(HttpStatus.NOT_IMPLEMENTED.value());
				}
			}
			catch (Exception e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "----Exception occured while getting drug properties ----", e, false);
				response.setHeader("FailureReason", e.getMessage());
				response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			} finally {
				 if(entityManager !=null && entityManager.isOpen()){
					 entityManager.clear();
					 entityManager.close();
					 entityManager = null;
				 }
			}
		} else {
			response.setHeader("FailureReason", "Empty input parameters");
			response.setStatus(HttpStatus.PRECONDITION_FAILED.value());
		}
		if(LOGGER.isInfoEnabled()) {
			long endTime = TimeShifterUtil.currentTimeMillis();
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "getDrugProperties ended in " + (endTime - startTime) + " ms", null, false);
		}
		return drugPropertiesResponse;
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/drugs/planAvailability", method = RequestMethod.POST)
	public @ResponseBody String getPlanAvailabilityForDrug(@RequestBody PlanAvailabilityForDrugRequest planAvailabilityForDrugRequest) {	
		PrescriptionSearch search = prescriptionFactory.getObject();
		long startTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO,"---- prepareFuturePlanAvailabilityMap ----",null,false);
		
		PrescriptionRequest prescriptionRequest = new PrescriptionRequest();
		prescriptionRequest.setState(planAvailabilityForDrugRequest.getState());
		prescriptionRequest.setPlanHiosList(planAvailabilityForDrugRequest.getPlanHiosList());
		
		if(planAvailabilityForDrugRequest.getDrugNdcList() != null){
			List<DrugDTO> drugDTOList = new ArrayList<DrugDTO>();
			for(String drugNdc : planAvailabilityForDrugRequest.getDrugNdcList()){
				DrugDTO drugDto = new DrugDTO();
				drugDto.setDrugNdc(drugNdc);
				drugDTOList.add(drugDto);
			}
			prescriptionRequest.setDrugDTOList(drugDTOList);
		}
		
		Map<String, Future<PrescriptionData>> futurePlanAvailabilityListMap = search.prepareFuturePlanAvailabilityMap(prescriptionRequest);
		long endTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO,"---- prepareFuturePlanAvailabilityMap ----"+(endTime-startTime),null,false);
		long startTime2 = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO,"---- prepareDrugListMap ----",null,false);
		Map<String, List<DrugDTO>> availabilityResponse = search.prepareDrugListMap(prescriptionRequest.getDrugDTOList(), prescriptionRequest.getPlanHiosList(), futurePlanAvailabilityListMap);
		long endTime2 = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO,"---- prepareDrugListMap ----"+(endTime2-startTime2),null,false);
		long startTime1 = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO,"---- convertToDrugResponse ----",null,false);
		List<PlanAvailabilityForDrugResponse> planAvailabilityForDrugResponseList = convertToDrugResponse(availabilityResponse, planAvailabilityForDrugRequest.getDrugNdcList(), planAvailabilityForDrugRequest.getPlanHiosList());
		long endTime1 = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO,"---- convertToDrugResponse ----"+(endTime1-startTime1),null,false);
		return platformGson.toJson(planAvailabilityForDrugResponseList);
	}

	

	private List<PlanAvailabilityForDrugResponse> convertToDrugResponse(
			Map<String, List<DrugDTO>> availabilityResponse, List<String> drugNdcList, List<String> hiosList) {
		List<PlanAvailabilityForDrugResponse> planAvailabilityList = new ArrayList<PlanAvailabilityForDrugResponse>();
		for (String drugNdc : drugNdcList)
		{
			PlanAvailabilityForDrugResponse planAvailabilityForDrugResponse = new PlanAvailabilityForDrugResponse();
			planAvailabilityForDrugResponse.setDrugNdc(drugNdc);
			List<Map<String, String>> plans = new ArrayList<Map<String, String>>();
			for (String planHiosId : hiosList) {
				Map<String, String> planAvailability = new HashMap<String, String>();
				planAvailability.put("planHiosId", planHiosId);
				planAvailability.put("tier", getHiosIdDrugCoverage(planHiosId, drugNdc,availabilityResponse));
				plans.add(planAvailability);
			}
			planAvailabilityForDrugResponse.setPlans(plans);
			planAvailabilityList.add(planAvailabilityForDrugResponse);
		}
		
		return planAvailabilityList;
	}

	private String getHiosIdDrugCoverage(String planHiosId, String drugNdc, Map<String, List<DrugDTO>> availabilityResponse) {
		List<DrugDTO> drugNdcs = availabilityResponse.get(planHiosId);
		for(DrugDTO drugDTO : drugNdcs)
		{
			if(StringUtils.isNotBlank(drugNdc) && drugNdc.equals(drugDTO.getDrugNdc()))
			{
				return drugDTO.getDrugTier();
			}
		}
		return null;
	}

	/**
	 * @deprecated This method is not in use.
	 * @param drugNdcList
	 * @return
	 */
	@Deprecated
	private List<DrugDTO> formDrugDTOs(List<String> drugNdcList) {
		List<DrugDTO> dtos = new ArrayList<DrugDTO>();
		for(String ndcName : drugNdcList)
		{
			DrugDTO drugDto = new DrugDTO();
			drugDto.setDrugNdc(ndcName);
			dtos.add(drugDto);
		}
 		return dtos;
	}

	@RequestMapping(value = "/drugs/fairPrice/{ndc}", method = RequestMethod.GET)
	public @ResponseBody Double getFairPrice(@PathVariable("ndc") String ndc, HttpServletResponse response)
	{
		long startTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "----getFairPrice started----", null, false);
		try{
			GoodRxResponseDTO goodRxResponseDTO = goodRxService.getFairPriceByNDC(ndc);
			if(goodRxResponseDTO!=null && goodRxResponseDTO.isSuccess() && goodRxResponseDTO.getData()!=null){
				DrugDetailsDTO drugDetailsDTO = goodRxResponseDTO.getData();
				if(drugDetailsDTO != null &&  drugDetailsDTO.getPrice() != null){
					long endTime = TimeShifterUtil.currentTimeMillis();
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "getFairPrice ended in " + (endTime - startTime) + " ms", null, false);	
					return drugDetailsDTO.getPrice();
				}else{
					response.setHeader("FailureReason", "Drug Price not found");
					response.setStatus(521);
				}
			}else{
				response.setHeader("FailureReason", "Drug not found");
				response.setStatus(521);
			}
		} catch (Exception e){
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "----Exception occured while fetching fairPrice----", e, false);
			response.setHeader("FailureReason", e.getMessage());
			response.setStatus(521);
		}
		long endTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "getFairPrice ended in " + (endTime - startTime) + " ms", null, false);
		return null;
	}
	
	@RequestMapping(value = "/drugs/rxcui/{ndc}", method = RequestMethod.GET)
	public @ResponseBody String getRxcuiByNdc(@PathVariable("ndc") String ndc, HttpServletResponse response)
	{
		long startTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "----getRxcuiByNdc started----", null, false);
		Map<String, String> result = new HashMap<String, String>();
		try{
			Map<String, Object> rxNormResponseObj = planDisplayUtil.getRxNormResponseForNdc(ndc);
			if(rxNormResponseObj != null){
				Map idGroupMap = (Map<String, Object>) rxNormResponseObj.get("idGroup");
				if(idGroupMap!=null && idGroupMap.get("rxnormId") != null){
					List<String> rxnormIdList =  (List<String>) idGroupMap.get("rxnormId");
					if(rxnormIdList!=null && rxnormIdList.size() > 0){
						return rxnormIdList.get(0);
					} else{
						result.put("message", "rxnormId not found");
						response.setStatus(521);
					}
				}else{
					result.put("message", "Invalid NDC");
					response.setStatus(521);
				}
			}else{
				result.put("message", "Invalid NDC - No response for given NDC");
				response.setStatus(521);
			}
		} catch (Exception e){
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "----Exception occured while fetching rxcui----", e, false);
			result.put("message", e.getMessage());
			response.setStatus(521);
		}
		long endTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "getRxcuiByNdc ended in " + (endTime - startTime) + " ms", null, false);
		return platformGson.toJson(result);
	}
	
	@RequestMapping(value = "/dst/household", method = RequestMethod.POST)
	public @ResponseBody String saveHouseholdData(@RequestBody String saveHouseholdRequestStr, HttpServletResponse response){
		Map<String, String> result = new HashMap<String, String>();
		try{
			SaveHouseholdRequest saveHouseholdRequest = platformGson.fromJson(saveHouseholdRequestStr, SaveHouseholdRequest.class);
			String validationResponse = planDisplayUtil.validateSaveHouseholdRequest(saveHouseholdRequest);
			PdHousehold pdHousehold = new PdHousehold();
			if(saveHouseholdRequest.getHouseholdId() != null){
				pdHousehold = pdHouseholdService.findByShoppingId(saveHouseholdRequest.getHouseholdId());
				if(pdHousehold == null){
					validationResponse = "Invalid Household Id";
				}
			}
			if("VALID".equalsIgnoreCase(validationResponse)){
				
				/**
				 * Pickup the saveHouseholdRequest.coverageStateDate as coverage start date if present 
				 * else use coverageYear to form coverage start date, 
				 * (coverageStartDate = saveHouseholdRequest.getCoverageYear() + "-01-01").
				 */
				String coverageStartDate = StringUtils.isNotBlank(saveHouseholdRequest.getCoverageStartDate())
						? saveHouseholdRequest.getCoverageStartDate()
						: planDisplayUtil.getCoverageDate(saveHouseholdRequest.getCoverageYear());
				
				pdHousehold.setCoverageStartDate(DateUtil.StringToDate(coverageStartDate,"yyyy-MM-dd"));
				pdHousehold.setEnrollmentType(EnrollmentType.I);
				pdHousehold.setShoppingType(ShoppingType.INDIVIDUAL);
				pdHousehold.setHouseholdData(saveHouseholdRequestStr);
				pdHousehold = pdHouseholdService.save(pdHousehold);

				result.put("householdId", pdHousehold.getShoppingId());
				response.setStatus(200);
			}else{
				result.put(PlanDisplayConstants.MESSAGE, validationResponse);
				response.setStatus(400);
			}
		}catch(Exception e){
			result.put(PlanDisplayConstants.MESSAGE, e.getMessage());
			response.setStatus(500);
		}
		
		String responseStr = platformGson.toJson(result);
		planDisplayAsyncUtil.populateGiWSPayload(saveHouseholdRequestStr, responseStr, "/dst/household", "Save Household Data", StringUtils.isBlank(result.get(PlanDisplayConstants.MESSAGE)) ? "SUCCESS" : "FAILURE");
		
		return responseStr;
	}
	
	@RequestMapping(value = "/dst/saveProviderPreferences", method = RequestMethod.POST)
	public @ResponseBody String saveProviderPreferences(@RequestBody String pdSaveProviderPrefRequestStr, HttpServletResponse response){
		Map<String, String> result = new HashMap<String, String>();
		try{
			PdSaveProviderPrefRequest pdSaveProviderPrefRequest = platformGson.fromJson(pdSaveProviderPrefRequestStr, PdSaveProviderPrefRequest.class);
			if(pdSaveProviderPrefRequest != null){
				if(StringUtils.isNotBlank(pdSaveProviderPrefRequest.getHouseholdId())){
					PdHousehold pdHousehold = pdHouseholdService.findByShoppingId(pdSaveProviderPrefRequest.getHouseholdId());
					if(pdHousehold != null && StringUtils.isNotBlank(pdHousehold.getHouseholdData())){
						String validationResponse = planDisplayUtil.validatePreferencesRequest(pdSaveProviderPrefRequest);
						if("VALID".equalsIgnoreCase(validationResponse)){
							List<ProviderBean> providerList = pdSaveProviderPrefRequest.getProviders();
							PdPreferencesDTO pdPreferencesDTO = new PdPreferencesDTO();
							String providers = platformGson.toJson(providerList);
							pdPreferencesDTO.setProviders(providers);
							
							planDisplaySvcHelper.savePdPreferences(pdPreferencesDTO, pdHousehold.getId(), true);
							
							result.put(PlanDisplayConstants.STATUS, PlanDisplayConstants.SUCCESS);
						}else{
							result.put(PlanDisplayConstants.MESSAGE, validationResponse);
							response.setStatus(400);
						}
					}else{
						result.put(PlanDisplayConstants.MESSAGE, PlanDisplayConstants.HOUSEHOLD_NOT_FOUND);
						response.setStatus(400);
					}
				}else{
					result.put(PlanDisplayConstants.MESSAGE, PlanDisplayConstants.HOUSEHOLD_ID_NOT_EMPTY);
					response.setStatus(400);
				}
			}else{
				result.put(PlanDisplayConstants.MESSAGE, PlanDisplayConstants.BLANK_REQUEST);
				response.setStatus(400);
			}
		}catch(Exception e){
			result.put(PlanDisplayConstants.MESSAGE, e.getMessage());
			response.setStatus(500);
		}
		
		String responseStr = platformGson.toJson(result);
		planDisplayAsyncUtil.populateGiWSPayload(pdSaveProviderPrefRequestStr, responseStr, "/dst/saveProviderPreferences", "Save Provider Preferences", StringUtils.isBlank(result.get(PlanDisplayConstants.MESSAGE)) ? "SUCCESS" : "FAILURE");
		
		return responseStr;
	}
	
	@RequestMapping(value = "/dst/planScore/{householdId}", method = RequestMethod.GET)
	public @ResponseBody String calculatePlanScore(@PathVariable("householdId") String householdId, HttpServletResponse response){
		Map<String, String> result = new HashMap<String, String>();
		String responseStr = "";
		try{
			if(StringUtils.isNotBlank(householdId)){
				PdHousehold pdHousehold = pdHouseholdService.findByShoppingId(householdId);
				if(pdHousehold != null && StringUtils.isNotBlank(pdHousehold.getHouseholdData())){				
					SaveHouseholdRequest saveHouseholdRequest = platformGson.fromJson(pdHousehold.getHouseholdData(), SaveHouseholdRequest.class);
					if(saveHouseholdRequest.getPlanPremiumList() != null && saveHouseholdRequest.getPlanPremiumList().size() > 0) {
						PdPreferences pdPreferences = pdPreferencesService.findPreferencesByPdHouseholdId(pdHousehold.getId());
						if(pdPreferences != null && StringUtils.isNotBlank(pdPreferences.getPreferences())){
							PdPreferencesDTO pdPreferencesDTO = planDisplayUtil.getPdPreferencesDTO(pdPreferences);
							if(pdPreferencesDTO.getMedicalUse() != null && pdPreferencesDTO.getPrescriptionUse() != null){
								String coverageStartDate = DateUtil.dateToString(pdHousehold.getCoverageStartDate(), "yyyy-MM-dd");
								responseStr = planDisplaySvcHelper.calculatePlanScoreForPreferences(pdPreferencesDTO, saveHouseholdRequest.getPlanPremiumList(), saveHouseholdRequest.getNoOfMembers(), saveHouseholdRequest.getZipCode(), coverageStartDate, saveHouseholdRequest.getCurrentHiosId());
								if(StringUtils.isBlank(responseStr)){
									result.put(PlanDisplayConstants.MESSAGE, PlanDisplayConstants.PLANS_NOT_AVAILABLE);
									response.setStatus(400);
								}	
							}else{
								result.put(PlanDisplayConstants.MESSAGE, PlanDisplayConstants.PREFERENCES_NOT_FOUND);
								response.setStatus(400);
							}	
						}else{
							result.put(PlanDisplayConstants.MESSAGE, PlanDisplayConstants.PREFERENCES_NOT_FOUND);
							response.setStatus(400);
						}
					}else{
						result.put(PlanDisplayConstants.MESSAGE, "No Plans Found for the household");
						response.setStatus(400);
					}
				}else{
					result.put(PlanDisplayConstants.MESSAGE, PlanDisplayConstants.HOUSEHOLD_NOT_FOUND);
					response.setStatus(400);
				}
			}else{
				result.put(PlanDisplayConstants.MESSAGE, PlanDisplayConstants.BLANK_REQUEST);
				response.setStatus(400);
			}
		}catch(Exception e){
			result.put(PlanDisplayConstants.MESSAGE, e.getMessage());
			response.setStatus(500);
			LOGGER.error("Error occured while calculatePlanScore : ", e);
		}
		
		if(StringUtils.isBlank(responseStr)){
			responseStr = platformGson.toJson(result);
		}
		
		planDisplayAsyncUtil.populateGiWSPayload(householdId, responseStr, "/dst/planScore", "Plan Score", StringUtils.isBlank(result.get(PlanDisplayConstants.MESSAGE)) ? "SUCCESS" : "FAILURE");
		
		return responseStr;
	}
	
	@RequestMapping(value = "/callCds", method = RequestMethod.POST )
	public @ResponseBody String callCds(@RequestBody String inputDataStr, HttpServletRequest request){
		CostCalculatorRequest costCalculatorRequest = new CostCalculatorRequest();
		try{			
			String[] helathBenefitListForWAState = {"PRIMARY_VISIT", "SPECIAL_VISIT", "LABORATORY_SERVICES", "IMAGING_XRAY",
			        "IMAGING_SCAN", "OUTPATIENT_SERVICES_OFFICE_VISIT", "OUTPATIENT_FACILITY_FEE", "OUTPATIENT_SURGERY_SERVICES", "OUTPATIENT_REHAB_SERVICES",
			        "INPATIENT_PHY_SURGICAL_SERVICE", "INPATIENT_HOSPITAL_SERVICE", "DURABLE_MEDICAL_EQUIP","MAJOR_DENTAL_CARE_CHILD", "ACUPUNCTURE", "CHIROPRACTIC", "WEIGHT_LOSS", "GENERIC", "PREFERRED_BRAND", "NON_PREFERRED_BRAND", "SPECIALTY_DRUGS"};
			List<String> benefitList = Arrays.asList(helathBenefitListForWAState);
			
			Type type = new TypeToken<Map<String, String>>(){}.getType();
			Map<String, String> inputData = platformGson.fromJson(inputDataStr, type);
			
			String coverageStartDate = inputData.get("coverageStartDate");
			String planDataListStr = inputData.get("planData");
			
			List<PlanData> planDataList = platformGson.fromJson(planDataListStr,new TypeToken<List<PlanData>>() {}.getType());	
			
			List<String> hiosPlanIds = new ArrayList<String>();
			for(PlanData planData : planDataList){
				if(planData.getHiosPlanId() != null){
					hiosPlanIds.add(planData.getHiosPlanId());
				}			
			}	
			
			List<PlanRateBenefit> planRateBenefitList = planDisplayUtil.getPlanRateBenefitsByplanId(hiosPlanIds, PlanDisplayEnum.InsuranceType.HEALTH.toString(), coverageStartDate);
			for(PlanRateBenefit planRateBenefit : planRateBenefitList){
				String hiosId = planRateBenefit.getHiosPlanNumber();
				for(PlanData planData : planDataList){
					if(planData.getHiosPlanId().equalsIgnoreCase(hiosId)){
						planData.setNoOfDrugsCovered(planData.getNoOfDrugsCovered()!=null?planData.getNoOfDrugsCovered():0);
						planData.setNoOfProvidersCovered(planData.getNoOfProvidersCovered()!=null?planData.getNoOfProvidersCovered():0);
						
						Map<String, Map<String, String>>  planHealthCosts = planRateBenefit.getPlanCosts();				
						if(null != planHealthCosts){					
							for(Entry entry : planHealthCosts.entrySet()){
								String name = (String)entry.getKey();
								Map<String, String> currCost = (Map<String, String>)entry.getValue();
								String inNetworkIndCost = currCost.get("inNetworkInd");
								String inNetworkFlyCost = currCost.get("inNetworkFly");
								
								double inNetworkIndVal = 0;
								if(!StringUtils.isEmpty(inNetworkIndCost)){
									inNetworkIndVal = Double.parseDouble(inNetworkIndCost);
								}
								
								double inNetworkFlyVal = 0;
								if(inNetworkFlyCost!=null && !StringUtils.isEmpty(inNetworkFlyCost)){
									inNetworkFlyVal = Double.parseDouble(inNetworkFlyCost);
								}
																
								if("MAX_OOP_INTG_MED_DRUG".equalsIgnoreCase(name)){
									planData.setOopMaxIndividual(inNetworkIndVal);
									planData.setOopMaxFamily(inNetworkFlyVal);
								}else if("MAX_OOP_MEDICAL".equalsIgnoreCase(name)) {
									planData.setOopMaxIndividual(inNetworkIndVal);
									planData.setOopMaxFamily(inNetworkFlyVal);
								}else if("MAX_OOP_DRUG".equalsIgnoreCase(name)) {
									planData.setDrugOopMaxIndividual(inNetworkIndVal);
									planData.setDrugOopMaxFamily(inNetworkFlyVal);
								}else if("DEDUCTIBLE_INTG_MED_DRUG".equalsIgnoreCase(name)) {
									planData.setDeductibleIndividual(inNetworkIndVal);
									planData.setDeductibleFamily(inNetworkFlyVal);
								}else if("DEDUCTIBLE_MEDICAL".equalsIgnoreCase(name)) {
									planData.setDeductibleIndividual(inNetworkIndVal);
									planData.setDeductibleFamily(inNetworkFlyVal);
								}else if("DEDUCTIBLE_DRUG".equalsIgnoreCase(name)) {
									planData.setDrugDeductibleIndividual(inNetworkIndVal);
									planData.setDrugDeductibleFamily(inNetworkFlyVal);
								}						
							}
						}	
						
						Map<String, Map<String, String>>  planBenefitsMap = planRateBenefit.getPlanBenefits();
						if(null != planBenefitsMap){	
							List<PlanBenefits> planBenefits = new  ArrayList<PlanBenefits>();
							for(Entry entry : planBenefitsMap.entrySet()){
								String name = (String) entry.getKey().toString();								
								if(benefitList.contains(name)){
									Map<String, String> planBenefitMap = (Map<String, String>)entry.getValue();
									if(planBenefitMap != null && !planBenefitMap.isEmpty()){
										PlanBenefits planBenefit = new PlanBenefits();
										planBenefit.setName(name);
										String tier1CopayAttrib = planBenefitMap.get("tier1CopayAttrib");
										String tier1CoinsAttrib = planBenefitMap.get("tier1CoinsAttrib");
										String tier1CopayVal = planBenefitMap.get("tier1CopayVal");								
										String tier1CoinsVal = planBenefitMap.get("tier1CoinsVal");
										String isCovered = planBenefitMap.get("isCovered");										
										
										if(!StringUtils.isBlank(tier1CopayAttrib) || !StringUtils.isBlank(tier1CoinsAttrib)){
											if(!StringUtils.isBlank(tier1CopayAttrib)){
												planBenefit.setCopayAttribute(tier1CopayAttrib);
												planBenefit.setCopayVal(Double.valueOf(tier1CopayVal));
											}
											if(!StringUtils.isBlank(tier1CoinsAttrib)){
												planBenefit.setCoinsuranceAttribute(tier1CoinsAttrib);
												planBenefit.setCoinsuranceVal(Double.valueOf(tier1CoinsVal));
											}
											planBenefit.setIsCovered(isCovered);
											planBenefits.add(planBenefit);
										}										
									}								
								}
							}
							planData.setBenefits(planBenefits);
						}
					}
				}				
			}
			costCalculatorRequest.setPlanData(planDataList);
			
		}catch(Exception e){
			LOGGER.error("Error occured while getPlanScorForPreferences : ", e);
		}		
		return platformGson.toJson(costCalculatorRequest);
	}
	
	private PdMtCartItemResponse getPreferencesFromLeadId(Long eligLeadId, PdMtCartItemResponse pdMtCartItemResponse, List<Map<String, String>> errorList) {
		PdPreferences pdPreferences = pdPreferencesService.findPreferencesByEligLeadId(eligLeadId);
		if(pdPreferences == null) {
		    setValidateError(pdMtCartItemResponse, errorList, "eligLeadId not found.");
		    return pdMtCartItemResponse;
		}
		
		PdPreferencesDTO pdPreferencesDTO = planDisplayUtil.getPdPreferencesDTO(pdPreferences);
		pdMtCartItemResponse.setStatus(Integer.valueOf(PdMtCartItemResponse.ErrorCodes.SUCCESS.getId()));
		pdMtCartItemResponse.setMessage(PdMtCartItemResponse.ErrorCodes.SUCCESS.getMessage());
		pdMtCartItemResponse.setPdPreferencesDTO(pdPreferencesDTO);
		return pdMtCartItemResponse;
	}

	@RequestMapping(value = "/getPdPreferencesByLeadId/{eligLeadId}", method = RequestMethod.GET)
	public @ResponseBody PdMtCartItemResponse getPdPreferencesByLeadId(@PathVariable String eligLeadId){
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside getPreferencesByLeadId method---->", null, false);
		PdMtCartItemResponse pdMtCartItemResponse = new PdMtCartItemResponse();
		List<Map<String,String>> errorList = new ArrayList<Map<String,String>>();
		try{
			if(StringUtils.isBlank(eligLeadId)){
				setValidateError(pdMtCartItemResponse, errorList, "eligLead id not found in request URL");
				return pdMtCartItemResponse;
			}
			Long leadId = Long.parseLong(eligLeadId);
			pdMtCartItemResponse = getPreferencesFromLeadId(leadId, pdMtCartItemResponse, errorList);
			return pdMtCartItemResponse;
			
		}catch (NumberFormatException e) {
			setValidateError(pdMtCartItemResponse, errorList, "eligLead id is not valid");
			return pdMtCartItemResponse;
		} catch (Exception e) {
			setSystemError(pdMtCartItemResponse, errorList, e);
			return pdMtCartItemResponse;
		}	
	}	
	
	@RequestMapping(value = "/ind71g", method = RequestMethod.POST)
	public @ResponseBody String getIND71g( @RequestBody Ind71GRequest ind71GRequest, BindingResult result) {	
		IND71Response response = new IND71Response();
		GIWSPayload newGiWSPayload = new GIWSPayload();
		try {
			newGiWSPayload.setCreatedTimestamp(new TSDate());
			newGiWSPayload.setEndpointUrl(GhixPlatformEndPoints.APP_URL+"api/ind71g");
			newGiWSPayload.setEndpointFunction("IND71G");
			newGiWSPayload.setRequestPayload(platformGson.toJson(ind71GRequest));
			
			newGiWSPayload = giwsPayloadService.save(newGiWSPayload);
		}
		catch(Exception e){
			LOGGER.error("Error occured while logging into GI_WS_Payload request : ", e);
		}
		//validator.validate(ind71GRequest, result);
		iND71Validator.validate(ind71GRequest, result);
		if (result.hasErrors())
		{
			response.setResponseCode("400");
			List<String> responseErrors= new ArrayList<String>(2);
			response.setErrors(responseErrors);
			for(ObjectError error : result.getAllErrors())
			{
				responseErrors.add(error.getDefaultMessage());
			}
			//System.out.println(result.getAllErrors().get(0).getObjectName());
			newGiWSPayload.setResponsePayload(platformGson.toJson(response));
			newGiWSPayload.setStatus("FAILURE");
			giwsPayloadService.save(newGiWSPayload);
			return platformGson.toJson(response);
		}
		response = autoRenewalService.processIND71G(ind71GRequest, response);
		if("200".equals(response.getResponseCode()))
		{
			newGiWSPayload.setStatus("SUCCESS");
		}
		else
		{
			newGiWSPayload.setStatus("FAILURE");
		}
		String jsonResponse = null;
		try 
		{
			jsonResponse = platformGson.toJson(response);
			newGiWSPayload.setResponsePayload(jsonResponse);
			giwsPayloadService.save(newGiWSPayload);
		}
		catch(Exception e){
			LOGGER.error("Error occured while logging into GI_WS_Payload response  : ", e);
			jsonResponse = "Error occured while logging into GI_WS_Payload response : " + e.getMessage();
		}
		return jsonResponse;
	}

	/**
	 * API is used to get Drug Data by RXCUI-List.
	 */
	@RequestMapping(value = "/drugs/byRxcuiList", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody DrugDataResponseDTO getDrugDataByRxcuiSearchList(@RequestBody DrugDataRequestDTO requestDTO, HttpServletResponse response) {

		DrugDataResponseDTO responseDTO = new DrugDataResponseDTO();
		responseDTO.startResponse();

		if (LOGGER.isInfoEnabled()) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "---- API Drug Data RXCUI List started ----", null, false);
		}

		try {
			pdDrugService.getDrugDataResponseByRxcuiList(requestDTO, responseDTO, response);
		}
		finally {
			responseDTO.endResponse();

			if (LOGGER.isInfoEnabled()) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "---- API Drug Data RXCUI List ended in " + responseDTO.getExecutionDuration() + " ms ----", null, false);
			}
		}
		return responseDTO;
	}
}
