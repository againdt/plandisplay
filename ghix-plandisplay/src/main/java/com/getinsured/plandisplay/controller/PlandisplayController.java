package com.getinsured.plandisplay.controller;

import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.ws.http.HTTPException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectReader;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.plandisplay.APTCCalculatorMemberData;
import com.getinsured.hix.dto.plandisplay.APTCCalculatorRequest;
import com.getinsured.hix.dto.plandisplay.APTCCalculatorResponse;
import com.getinsured.hix.dto.plandisplay.APTCPlanRequestDTO;
import com.getinsured.hix.dto.plandisplay.APTCPlanResponseDTO;
import com.getinsured.hix.dto.plandisplay.PdHouseholdDTO;
import com.getinsured.hix.dto.plandisplay.PdOrderItemDTO;
import com.getinsured.hix.dto.plandisplay.PdOrderItemPersonDTO;
import com.getinsured.hix.dto.plandisplay.PdPersonDTO;
import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;
import com.getinsured.hix.dto.plandisplay.PdSession;
import com.getinsured.hix.dto.plandisplay.PlanPremiumDTO;
import com.getinsured.hix.dto.plandisplay.ProviderSearchRequest;
import com.getinsured.hix.dto.plandisplay.planavailability.PdRequestPlanAvailability;
import com.getinsured.hix.dto.plandisplay.planavailability.PdResponsePlanAvailability;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.dto.shop.mlec.MLECRestResponse;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.model.IndividualPlan;
import com.getinsured.hix.model.PdHousehold;
import com.getinsured.hix.model.PdOrderItem;
import com.getinsured.hix.model.PdOrderItem.Status;
import com.getinsured.hix.model.PdOrderItemPerson;
import com.getinsured.hix.model.PdPerson;
import com.getinsured.hix.model.PdPreferences;
import com.getinsured.hix.model.PldOrderRequest;
import com.getinsured.hix.model.consumer.FFMMemberResponse;
import com.getinsured.hix.model.plandisplay.ApplicantEligibilityData;
import com.getinsured.hix.model.plandisplay.FFMPerson;
import com.getinsured.hix.model.plandisplay.PdCrossWalkIssuerIdRequest;
import com.getinsured.hix.model.plandisplay.PdOrderResponse;
import com.getinsured.hix.model.plandisplay.PdPreShopRequest;
import com.getinsured.hix.model.plandisplay.PdQuoteRequest;
import com.getinsured.hix.model.plandisplay.PdResponse;
import com.getinsured.hix.model.plandisplay.PdSaveCartRequest;
import com.getinsured.hix.model.plandisplay.PdSavePrefRequest;
import com.getinsured.hix.model.plandisplay.PdSubscriberData;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentFlowType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ExchangeType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.FlowType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;
import com.getinsured.hix.model.plandisplay.PlanDisplayRequest;
import com.getinsured.hix.model.plandisplay.PlanDisplayResponse;
import com.getinsured.hix.model.plandisplay.PldOrderResponse;
import com.getinsured.hix.model.plandisplay.ProviderBean;
import com.getinsured.hix.planmgmt.controller.PlanRestController;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.config.PlanDisplayConfiguration;
import com.getinsured.hix.platform.events.PartnerEventTypeEnum;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.PlanMgmtEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.ApplicantEligibilityResponseType;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest.Household;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest.Household.Members;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.HouseHoldContact;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Members.Member;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Members.Member.Race;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Members.Member.Relationship;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.ResponsiblePerson;
import com.getinsured.plandisplay.service.IndividualPlanService;
import com.getinsured.plandisplay.service.PdHouseholdService;
import com.getinsured.plandisplay.service.PdOrderItemPersonService;
import com.getinsured.plandisplay.service.PdOrderItemService;
import com.getinsured.plandisplay.service.PdPersonService;
import com.getinsured.plandisplay.service.PdPreferencesService;
import com.getinsured.plandisplay.service.PlanSolrUpdateService;
import com.getinsured.plandisplay.service.providerSearch.ProviderFactory;
import com.getinsured.plandisplay.service.providerSearch.ProviderSearch;
import com.getinsured.plandisplay.util.CreateDTOBean;
import com.getinsured.plandisplay.util.PlanDisplayConstants;
import com.getinsured.plandisplay.util.PlanDisplayUtil;
import com.getinsured.timeshift.TSCalendar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.thoughtworks.xstream.XStream;

@Controller
@RequestMapping(value="/plandisplay")
public class PlandisplayController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PlandisplayController.class);

	@Autowired private PlanDisplayUtil planDisplayUtil;	
	@Autowired private IndividualPlanService individualPlanService;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private PdHouseholdService pdHouseholdService;
	@Autowired private PdPersonService pdPersonService;
	@Autowired private PdOrderItemService pdOrderItemService;
	@Autowired private CreateDTOBean createDTOBean;	
	@Autowired private PlanDisplaySvcHelper planDisplaySvcHelper; 
	@Autowired private PlanSolrUpdateService planSolrUpdateService;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private AutoRenewalHelper autoRenewalHelper;	
	@Autowired private PdPreferencesService pdPreferencesService;
	@Autowired private PdOrderItemPersonService pdOrderItemPersonService;
	@Autowired private GIWSPayloadService giWSPayloadService;
	@Autowired private Gson platformGson;
	@Autowired private ProviderFactory providerFactory;
	@Autowired private PlanRestController planRestController;
		
	
    /**
     * Fetches household object based on shopping ID  from DB and returns PdHouseholdDTO.
     * @param request
     *              HttpServletRequest having parameter "shoppingId"
     * @return
     *              Object of <class>PdHouseholdDTO</class>
     */
	@RequestMapping(value = "/findHouseholdByShoppingId", method = RequestMethod.GET)
	public @ResponseBody PdResponse findHouseholdByShoppingId(HttpServletRequest request){
		try {
			String shoppingId = (String) request.getParameter("shoppingId");
			if (shoppingId == null) {//RequestCorrelationContext.getCurrent().getCorrelationId()
				return null;
			}

			PdResponse pdResponse = new PdResponse();
			PdHousehold pdHousehold = pdHouseholdService.findByShoppingId(shoppingId);
			if(pdHousehold != null) {
				PdHouseholdDTO pdHouseholdDTO = new PdHouseholdDTO();
				createDTOBean.copyProperties(pdHouseholdDTO, pdHousehold);
				pdResponse.setPdHouseholdDTO(pdHouseholdDTO);
				PdPreferences pdPreferences = pdPreferencesService.findPreferencesByPdHouseholdId(pdHousehold.getId());
				if(pdPreferences == null){
					pdPreferences = new  PdPreferences();
					pdPreferences.setEligLeadId(pdHousehold.getEligLeadId());
					pdPreferences.setPdHouseholdId(pdHousehold.getId());
					pdPreferences.setPreferences(PlanDisplayUtil.getDefaultPref());
				}
				PdPreferencesDTO pdPreferencesDTO = planDisplayUtil.getPdPreferencesDTO(pdPreferences);
				pdResponse.setPdPreferencesDTO(pdPreferencesDTO);
				return pdResponse;
			}else {
				return null;
			}

		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occured while executing findHouseholdByShoppingId method---->", e, false);
			return null;
		}
	}
	
    /**
     * Fetches List<PdPerson> object based on household ID from DB and returns Json of list of PdPersonDTO objects.
     * @param request
     *              HttpServletRequest having parameter "householdId"
     * @return
     *              Json of list of <class>PdPersonDTO</class>
     */
	
	@RequestMapping(value = "/findPersonByHouseholdId", method = RequestMethod.GET)
	public @ResponseBody String findPersonByHouseholdId(HttpServletRequest request){
		try {
			String householdId = (String) request.getParameter("householdId");
			if (householdId == null) {
				return null;
			}
			
			List<PdPerson> pdPersonList = pdPersonService.getPersonListByHouseholdId(Long.parseLong(householdId));
			List<PdPersonDTO> pdPersonDTOList = new ArrayList<PdPersonDTO>();
			for(PdPerson pdPerson : pdPersonList){
				PdPersonDTO pdPersonDTO = new PdPersonDTO();
				createDTOBean.copyProperties(pdPersonDTO, pdPerson);
				pdPersonDTOList.add(pdPersonDTO);
			}
			return platformGson.toJson(pdPersonDTOList);

		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occured while executing findPersonByHouseholdId method---->", e, false);
			return null;
		}
	}
	
    /**
     * Saves preferences in household based on household ID in DB.
     * @param request
     *              HttpServletRequest having parameter "householdId"
     * @return
     *              Object of <class>PdResponse</class>
     */
	
	@RequestMapping(value = "/savePreferencesByHouseholdId", method = RequestMethod.POST)
	public @ResponseBody PdResponse savePreferencesByHouseholdId(@RequestBody PdSavePrefRequest pdSavePrefRequest) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside savePreferences SERVICE method---->", null, false);
		try {
			if (pdSavePrefRequest == null || pdSavePrefRequest.getHouseholdId()==0 || pdSavePrefRequest.getPdPreferencesDTO()==null) {
				return planDisplaySvcHelper.sendNull();
			}
			
			PdResponse pdResponse = new PdResponse();
			pdResponse.startResponse();
			 
			PdPreferencesDTO pdPreferencesDTO = pdSavePrefRequest.getPdPreferencesDTO();
			//pdHouseholdService.savePreferences(pdPreferencesDTO, pdSavePrefRequest.getHouseholdId());
			planDisplaySvcHelper.savePdPreferences(pdPreferencesDTO, pdSavePrefRequest.getHouseholdId(), false);
						
			return planDisplaySvcHelper.sendSuccess(pdResponse);
		} catch (Exception e) {
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_EXECUTE_SAVE_PREFERENCES_BY_HOUSEHOLD_ID,e);
		}
	}
	
    /**
     * Saves subsidy for each person in PdPerson in DB taking List<PdPersonDTO> as input.
     * @param request
     *              HttpServletRequest having parameter List<PdPersonDTO>
     * @return
     *              Object of <class>PdResponse</class>
     */
	@RequestMapping(value = "/updatePersonSubsidy", method = RequestMethod.POST)
	public @ResponseBody PdResponse updatePersonSubsidy(@RequestBody List<PdPersonDTO> pdPersonDTOList){
		try {
			
			PdResponse pdResponse = new PdResponse();
			pdResponse.startResponse();
			
			if (pdPersonDTOList != null && !pdPersonDTOList.isEmpty()) {
				for(PdPersonDTO pdPersonDTO: pdPersonDTOList) {
					if(pdPersonDTO.getId() != null){
						PdPerson pdPerson = pdPersonService.findById(pdPersonDTO.getId());
						pdPerson.setHealthSubsidy(pdPersonDTO.getHealthSubsidy());
						pdPerson.setDentalSubsidy(pdPersonDTO.getDentalSubsidy());
						pdPersonService.savePerson(pdPerson);
					}
				}
			}			
			return planDisplaySvcHelper.sendSuccess(pdResponse);
		} catch (Exception e) {
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_EXECUTE_UPDATE_PERSON_SUBSIDY,e);
		}
	}
	
	
    /**
     * Fetches plans object based on PdQuoteRequest(quoting api request to plan management module ) from DB and returns List<IndividualPlan>.
     * @param request
     *              HttpServletRequest having parameter PdQuoteRequest
     * @return
     *              List of <class>IndividualPlan</class>
     */
	@RequestMapping(value = "/getPlans", method = RequestMethod.POST)
	public @ResponseBody List<IndividualPlan> getplans(@RequestBody PdQuoteRequest pdQuoteRequest) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside getplans SERVICE method---->", null, false);

		try{
			if (pdQuoteRequest == null || pdQuoteRequest.getPdHouseholdDTO() == null
					|| (pdQuoteRequest.getPdPersonDTOList()== null || pdQuoteRequest.getPdPersonDTOList().size() <= 0) || pdQuoteRequest.getInsuranceType() == null ) {
				return null;
			}
			
			List<IndividualPlan> finalList = new ArrayList<>();
			
			//HIX-113167 : adding already enrolled plan info for ID
			String showPrevYearEnrolledPlan = DynamicPropertiesUtil.getPropertyValue(PlanDisplayConfiguration.PlanDisplayConfigurationEnum.ENABLE_ENROLLED_PLAN_COMPARISON);
			if(Boolean.parseBoolean(showPrevYearEnrolledPlan) && (pdQuoteRequest.getShowPrevYearEnrlPlan() != null && pdQuoteRequest.getShowPrevYearEnrlPlan())) {
				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
				if("ID".equalsIgnoreCase(stateCode)){
					boolean isInsideOEEnrollmentWindow = false;
					Calendar cal = TSCalendar.getInstance();
					cal.setTime(pdQuoteRequest.getPdHouseholdDTO().getCoverageStartDate());
					int coverageYear = cal.get(Calendar.YEAR);
					try {
						isInsideOEEnrollmentWindow = (ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL + "eligibility/indportal/isInsideOEEnrollment/"+coverageYear, 
								GhixConstants.USER_NAME_EXCHANGE, 
								HttpMethod.GET, 
								MediaType.APPLICATION_JSON, 
								Boolean.class,
								null)).getBody();
					
					} catch (Exception e) {
						String errorMsg = "Exception occurred while checking if coverageYear Is Inside OE Enrollment Window fro coverageYear - " + coverageYear;
						LOGGER.error(errorMsg);
						throw new GIRuntimeException(errorMsg);
					}	
				
					if(isInsideOEEnrollmentWindow) { // Need to add household enrolled plan info only during OE period.
						IndividualPlan enrolledPlan = individualPlanService.getHouseholdEnrolledPlanInfo(pdQuoteRequest);
						if(enrolledPlan!=null) {
							finalList.add(enrolledPlan);
						}
					}else {
						if(LOGGER.isInfoEnabled()) {
							PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Not enquiring for current Enrolled Plan as not inside OE period ---->" + isInsideOEEnrollmentWindow, null, false);
						}
					}
				}
			}
			
			PdResponse pdResponse = new PdResponse();
			pdResponse.startResponse();
		
			List<IndividualPlan> individualPlanList = individualPlanService.getPlans(pdQuoteRequest);
			//Plan Recommendation
			if(pdQuoteRequest.getRecommendPlans()){
				individualPlanList = planDisplaySvcHelper.setRecommendationRank(individualPlanList, pdQuoteRequest.getCurrentPlanId(), pdQuoteRequest.getPdHouseholdDTO().getCoverageStartDate(), pdQuoteRequest.getExchangeType());
			}
			finalList.addAll(individualPlanList);
			return finalList;
			
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occured while executing getplans method---->", e, false);
			return null;
		}
	}
	
	/**
     * Fetches PdOrderItem based on householdId from DB and returns List<PdOrderItemDTO>.
     * @param request
     *              HttpServletRequest having parameter PdRequestPlanAvailability
     * @return
     *              <class>PdResponsePlanAvailability</class>
     */
	
	@RequestMapping(value = "/planAvailability", method = RequestMethod.POST)
	public @ResponseBody PdResponsePlanAvailability planAvailability(@RequestBody PdRequestPlanAvailability pdRequestPlanAvailability)
	{
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside planAvailability method---->", null, false);
		PdResponsePlanAvailability pdResponsePlanAvailability = planDisplaySvcHelper.getPlanAvailability(pdRequestPlanAvailability);
		return pdResponsePlanAvailability;
	}

	/**
     * Fetches PdOrderItem based on householdId from DB and returns List<PdOrderItemDTO>.
     * @param request
     *              HttpServletRequest having parameter "householdId"
     * @return
     *              List of <class>PdOrderItemDTO</class>
     */
	
	@RequestMapping(value = "/findCartItemsByHouseholdId", method = RequestMethod.GET)
    public @ResponseBody List<PdOrderItemDTO> findCartItemsByHouseholdId(HttpServletRequest request){
        try {
        	//Gson gson = gsonFactory.getObject();
               String householdId = (String) request.getParameter("householdId");
               if (householdId == null) {
                     return null;
               }
               
               List<PdOrderItem> pdOrderItemList = pdOrderItemService.findByHouseholdId(Long.parseLong(householdId));
               List<PdOrderItemDTO> pdOrderItemDTOList = new ArrayList<PdOrderItemDTO>();
               for(PdOrderItem pdOrderItem : pdOrderItemList){
                     PdOrderItemDTO pdOrderItemDTO = new PdOrderItemDTO();
                     List<PdOrderItemPerson> pdOrderItemPersons = pdOrderItemPersonService.getByOrderItemId(pdOrderItem.getId());
                     List<PdOrderItemPersonDTO> pdOrderItemPersonDTOList = new ArrayList<PdOrderItemPersonDTO>();
                     for(PdOrderItemPerson pdOrderItemPerson : pdOrderItemPersons)
                     {
                            PdOrderItemPersonDTO pdOrderItemPersonDTO = new PdOrderItemPersonDTO();
                            createDTOBean.copyProperties(pdOrderItemPersonDTO, pdOrderItemPerson);
                            pdOrderItemPersonDTO.setPdPersonId(pdOrderItemPerson.getPdPerson().getId());
                            pdOrderItemPersonDTOList.add(pdOrderItemPersonDTO);
                     }
                     pdOrderItemDTO.setPdOrderItemPersonDTOList(pdOrderItemPersonDTOList);
                     createDTOBean.copyProperties(pdOrderItemDTO, pdOrderItem);
                     pdOrderItemDTOList.add(pdOrderItemDTO);
               }
               return pdOrderItemDTOList;

        } catch (Exception e) {
               PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occured while executing findCartItemsByHouseholdId method---->", e, false);
               return null;
        }
	}
	
    /**

     * Fetches orderItemIds based on householdId from DB and returns list of orderItemIds.
     * @param request
     *              HttpServletRequest having parameter "householdId"
     * @return
     *              List of orderItemIds
     */
	
	@RequestMapping(value = "/findOrderItemIdsByHouseholdId", method = RequestMethod.POST)
	public @ResponseBody PdResponse findOrderItemIdsByHouseholdId(@RequestBody String householdId) 
	{
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside findOrderItemIdsByHouseholdId SERVICE method---->", null, false);
		try {
			if (householdId == null) {
				return planDisplaySvcHelper.sendNull();
			}
			PdResponse pdResponse = new PdResponse();
			pdResponse.startResponse();

			Long pdHouseholdId = Long.parseLong(householdId);
			List<Long> orderItemIds = pdOrderItemService.getOrderItemIdsByHouseholdId(pdHouseholdId);
			pdResponse.setOrderItemIds(orderItemIds);
			return planDisplaySvcHelper.sendSuccess(pdResponse);
		} catch (Exception e) {
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_EXECUTE_FIND_ORDERITEM_IDS_BY_HOUSEHOLDID,e);
		}
	}
	
    /**
     * Retrieves PlanInfo based on planId,coverageStartDate,minimizeData as input from DB and returns IndividualPlan object.
     * @param request
     *              HttpServletRequest having parameter "planId,coverageStartDate,minimizeData"
     * @return
     *              Object of <class>IndividualPlan</class>
     */
	@RequestMapping(value = "/getPlanDetails", method = RequestMethod.GET)
	public @ResponseBody IndividualPlan getPlanDetails(HttpServletRequest request){
		try {
			String planId = (String) request.getParameter("planId");
			String coverageStartDate = (String) request.getParameter("coverageStartDate");
			String minimizeDataStr = (String) request.getParameter("minimizeData");
			
			if (planId == null) {
				return null;
			}
			
			boolean minimizeData = false;
			if(minimizeDataStr!=null){
				minimizeData = "true".equalsIgnoreCase(minimizeDataStr) ? true : false;
			}
			return  planDisplayUtil.getPlanInfo(planId,coverageStartDate,minimizeData);

		} catch (Exception e) {
			return null;
		}
	}
	
    /**

     * Saves PdOrderItem in DB taking Object <PdSaveCartRequest> as input.
     * @param request
     *              HttpServletRequest having parameter PdSaveCartRequest
     * @return
     *              Object of <class>PdResponse</class>
     */
	@RequestMapping(value = "/saveCartItem", method = RequestMethod.POST)
	public @ResponseBody PdResponse saveCartItem(@RequestBody PdSaveCartRequest pdSaveCartRequest) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside getplans SERVICE method---->", null, false);

		try {
			if (pdSaveCartRequest == null) {
				return planDisplaySvcHelper.sendNull();
			}
			
			PdResponse pdResponse = new PdResponse();
			pdResponse.startResponse();
			
			List<Long> existingPdOrderItemIds = pdOrderItemService.getOrderItemIdsByHouseholdId(pdSaveCartRequest.getHouseholdId());
			if(existingPdOrderItemIds != null && !existingPdOrderItemIds.isEmpty() && "FFM".equals(pdSaveCartRequest.getRequestType())){
				pdResponse.setResponse("FAIL");
				pdResponse.setStatus("FAIL");
				return pdResponse;
			}else{
				List<Long> pdOrderItemIds = pdOrderItemService.saveItem(pdSaveCartRequest);
				PdHousehold pdHousehold = pdHouseholdService.findById(pdSaveCartRequest.getHouseholdId());
				/*
				 * if stateEchange is individual and adding a Health Plan
				 * 		 if already Has dental plan
				 * 				only then we need to update/adjust  the APTC and premium of plans by calling updateSubsidyInOrderItem()
				 *  
				 */
				final String stateExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);
				if("Individual".equalsIgnoreCase(stateExchangeType) && (InsuranceType.HEALTH.equals(pdSaveCartRequest.getInsuranceType()))){
					List<PdOrderItem> pdOrderItemList = pdOrderItemService.findByHouseholdId(pdSaveCartRequest.getHouseholdId());
					boolean alreadyHasDentalPlan = false;
					if(pdOrderItemList != null)
					{
						for (PdOrderItem orderItem : pdOrderItemList)
						{
							if(com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType.DENTAL.equals(orderItem.getInsuranceType()))
							{
								alreadyHasDentalPlan = true;
								break;
							}
							
						}
					}
					if(alreadyHasDentalPlan && pdHousehold.getMaxSubsidy() != null)
					{
						updateSubsidyInOrderItem(pdSaveCartRequest.getHouseholdId().toString(),pdHousehold.getMaxSubsidy().toString(), pdOrderItemList);
					}
				}
				
				pdResponse.setOrderItemIds(pdOrderItemIds);
				pdResponse.setResponse(pdOrderItemIds != null && !pdOrderItemIds.isEmpty() ? pdOrderItemIds.get(0).toString() : null);
			}	
			
			return planDisplaySvcHelper.sendSuccess(pdResponse);
			
		} catch (Exception e) {
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_EXECUTE_SAVE_CART_ITEM,e);
		}
	}
	
    /**
     * Deletes PdOrderItem from DB taking itemId as input .
     * @param request
     *              HttpServletRequest having parameter itemId
     * @return
     *              Object of <class>PdResponse</class>
     */
	@RequestMapping(value = "/deleteCartItem", method = RequestMethod.POST)
	public @ResponseBody PdResponse deleteCartItem(@RequestBody String itemId) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside deleteCartItem SERVICE method---->", null, false);
		try
		{
			if (itemId != null) {
				PdOrderItem pdOrderItem = pdOrderItemService.findById(Long.parseLong(itemId));
				InsuranceType insType = pdOrderItem.getInsuranceType();
				boolean isItemDelete = pdOrderItemService.deleteItem(Long.parseLong(itemId));
				if(isItemDelete) {
					PdResponse pdResponse = new PdResponse();
					pdResponse.startResponse();
					pdResponse.setResponse(insType.toString());
					
					return planDisplaySvcHelper.sendSuccess(pdResponse);
				} else {
					return planDisplaySvcHelper.sendFailure("Invalid input: itemId could not be found "+itemId);
				}
			} else {
				return planDisplaySvcHelper.sendFailure("Invalid input: itemId cannot be Null");
			}
		} catch (Exception e) {
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_EXECUTE_DELETE_CART_ITEM,e);
		}
	}
	
    /**
     * Saves SeekCoverageDental flag in PdPerson in DB taking List<PdPersonDTO> as input .
     * @param request
     *              HttpServletRequest having parameter List<PdPersonDTO>
     * @return
     *              Object of <class>PdResponse</class>
     */
	@RequestMapping(value = "/updateSeekCoverageDental", method = RequestMethod.POST)
	public @ResponseBody PdResponse updateSeekCoverageDental(@RequestBody List<PdPersonDTO> pdPersonDTOList){
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside updateSeekCoverageDental() method---->", null, false);

		try {
			if (pdPersonDTOList == null || pdPersonDTOList.isEmpty()) {
				return planDisplaySvcHelper.sendNull();
			}	

			PdResponse pdResponse = new PdResponse();
			pdResponse.startResponse();
			
			for(PdPersonDTO pdPersonDTO : pdPersonDTOList){
				PdPerson pdPerson = pdPersonService.findById(pdPersonDTO.getId());
				pdPerson.setSeekCoverage(pdPersonDTO.getSeekCoverage());				
				pdPersonService.savePerson(pdPerson);
			}
			
			pdResponse.setResponse("SUCCESS");
			return planDisplaySvcHelper.sendSuccess(pdResponse);
			
		} catch (Exception e) {
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_EXECUTE_UPDATE_SEEK_COVERAGE_DENTAL,e);
		}
	}
	
    /**

     * Fetches PdHousehold object based on householdId as input from DB and returns json of PdOrderResponse.
     * @param request
     *              HttpServletRequest having parameter "householdId"
     * @return
     *              Object of <class>PdOrderResponse</class>
     */
	@RequestMapping(value="/findbyhouseholdid/{id}", method = RequestMethod.GET)
	public @ResponseBody String findByHouseholdId(@PathVariable("id") String householdId) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside findByHouseholdId SERVICE method---->"+householdId, null, false);
		
		String response = "";
		PdOrderResponse pdOrderResponse = new PdOrderResponse();

		Long pdHouseholdId = null;
		try{
		pdHouseholdId  = Long.parseLong(householdId);
		}
		catch (NumberFormatException e) {
			return "Invalid HouseholdId"+householdId;
		}
		PdHousehold pdHousehold = pdHouseholdService.findById(pdHouseholdId);
		if(pdHousehold == null){ 
			return "Invalid Household Id = "+householdId;
		}
		
		List<PdOrderItem> pdOrderItems = pdOrderItemService.findByHouseholdId(pdHouseholdId);
		if(pdOrderItems == null || pdOrderItems.isEmpty()){ 
			return "Invalid Household Id = "+householdId;
		}
		
		planDisplaySvcHelper.createPdOrderResponseForEnrollment(pdHousehold, pdOrderItems, pdOrderResponse, false, null);
		pdOrderResponse.setGiWsPayloadId(pdHousehold.getGiWsPayloadId());
		response = platformGson.toJson(pdOrderResponse);

		return response;
	}
	
    /**
     * Saves PlanBenefits and cost in SOLR taking encryptedPlanId and insuranceType as input.
     * @param request
     *              HttpServletRequest having parameter encryptedPlanId and insuranceType
     * @return
     *              Object of <class>PdResponse</class>
     */
	@RequestMapping(value = "/updatePlanBenefitsAndCostInSOLR/{insuranceType}/{encryptedPlanId}", method = RequestMethod.GET)
	public @ResponseBody String updatePlanBenefitsAndCostInSOLR(@PathVariable String insuranceType,@PathVariable String encryptedPlanId){
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside updatePlanBenefitsAndCostInSOLR SERVICE method---->"+encryptedPlanId, null, false);
		
		List<Integer> planIds=new ArrayList<Integer>(1);
		try	{
			String decryptedPlanId=ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedPlanId); 
			Integer planId = Integer.valueOf(decryptedPlanId);
			planIds.add(planId);
		} catch(Exception e) {
			PdResponse pdResponse = planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_EXECUTE_UPDATE_PLAN_DATA_SOLR,e);
			return pdResponse.getResponse();
		}
		
		PdResponse pdResponse = planSolrUpdateService.saveMissingSolrDocsSynch(planIds, insuranceType);
		return pdResponse.getResponse();
	}

    /**
     * Fetches plan details from Solr taking planId as input.
     * @param request
     *              HttpServletRequest having parameter planId
     * @return
     *              Json response.
     */
	@RequestMapping(value="/getPlanDetailsFromSolr/{id}",method = RequestMethod.GET)
	public @ResponseBody String findPlanFromSolrbyId(@PathVariable String id){
		String response = "";
		
		if (id == null || StringUtils.isEmpty(id) || !NumberUtils.isNumber(id)) {
			PdResponse pdResponse = planDisplaySvcHelper.sendNull();
			response = platformGson.toJson(pdResponse);
		} else {
			Map<String, Object> outputData = individualPlanService.findPlanDetailsFromSolr(id);
			response = platformGson.toJson(outputData);
		}
		return response;
	}
	
	/**
     * Fetches plan details from DB taking shoppingId as input.
     * @param request
     *              HttpServletRequest having parameter shoppingId
     * @return
     *              XML response.
     */
	
	@RequestMapping(value="/findPlanAvailable", method = RequestMethod.POST)
	public @ResponseBody String findPlanAvailable(@RequestBody String shoppingId) {
		//String response = ghixRestTemplate.exchange(GhixEndPoints.GHIXWEB_SERVICE_URL+"private/setHousehold/"+shoppingId, GhixConstants.USER_NAME_EXCHANGE, HttpMethod.GET, MediaType.APPLICATION_JSON, String.class, null).getBody();
		try {
			
			if (shoppingId == null) {
				return autoRenewalHelper.setErrorResponse("Shopping id not found in request");
			}			
			
			PdHousehold pdHousehold = pdHouseholdService.findByShoppingId(shoppingId);
			if(pdHousehold == null){
				return autoRenewalHelper.setErrorResponse("Household corresponding to Shopping id not found");
			} 
			
			PdHouseholdDTO pdHouseholdDTO = new PdHouseholdDTO();
			createDTOBean.copyProperties(pdHouseholdDTO, pdHousehold);	
			
			PdSession pdSession = new PdSession();
			pdSession.setPdHouseholdDTO(pdHouseholdDTO);
			pdSession.setFlowType(FlowType.PLANSELECTION);
				
			List<PdPerson> pdPersonList = pdPersonService.getPersonListByHouseholdId(pdHouseholdDTO.getId());
			List<PdPersonDTO> pdPersonDTOList = new ArrayList<PdPersonDTO>();
			for(PdPerson pdPerson : pdPersonList){
				PdPersonDTO pdPersonDTO = new PdPersonDTO();
				createDTOBean.copyProperties(pdPersonDTO, pdPerson);
				pdPersonDTOList.add(pdPersonDTO);
			}
			
			pdSession.setPdPersonDTOList(pdPersonDTOList);
				
			EnrollmentType enrollmentType = pdHouseholdDTO.getEnrollmentType();
				
			pdSession.setSpecialEnrollmentFlowType(EnrollmentFlowType.NEW);
			if(EnrollmentType.A.equals(enrollmentType) ){
				pdSession.setSpecialEnrollmentFlowType(EnrollmentFlowType.KEEP);
			}
			
			MLECRestResponse shopResponse = autoRenewalHelper.getEmployerContribution(pdHouseholdDTO, pdPersonDTOList, pdSession.getSpecialEnrollmentFlowType());
			if(shopResponse != null){
				autoRenewalHelper.setPersonSubsidy(shopResponse.getMembers(), pdPersonDTOList); 
				autoRenewalHelper.updatePersonSubsidy(pdPersonDTOList);
			
				pdSession.setEmployerCoverageStartDate(shopResponse.getCoverageStartDate());
				pdSession.setEmployerPlanTier(shopResponse.getTierName().toString());
			} else {
				return autoRenewalHelper.setErrorResponse("<application Id> " + pdSession.getPdHouseholdDTO().getApplicationId() + " : Failed contribution data is not availalble for autorenewal case : "+ pdSession.getPdHouseholdDTO().getShoppingId());
			}
			
			Map<String, String> workSiteData = autoRenewalHelper.getEmployerWorksiteData(pdHouseholdDTO.getEmployerId());
			if(workSiteData != null){
				pdSession.setEmployerWorksiteZip(workSiteData.get("employerWorksiteZip"));
				pdSession.setEmployerWorksiteCounty(workSiteData.get("employerWorksiteCounty"));
			} else {
				return autoRenewalHelper.setErrorResponse("<application Id> " + pdSession.getPdHouseholdDTO().getApplicationId() + " : Failed employer worksite data is not availalble for autorenewal case : "+ pdSession.getPdHouseholdDTO().getShoppingId());
			}
			
			if(YorN.Y.equals(pdHouseholdDTO.getAutoRenewal())) {
				PlanDisplayResponse planDisplayResponse = autoRenewalHelper.processAutoRenewal(pdSession);

				XStream xstream = GhixUtils.getXStreamStaxObject();
				return xstream.toXML(planDisplayResponse);
			} 
			
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occured while processing autorenewal for case---->", e, false);
			return autoRenewalHelper.setErrorResponse("Error occured while processing autorenewal for case : "+ shoppingId);
		}
		return autoRenewalHelper.setErrorResponse("Failed to process autorenewal for case : "+ shoppingId);
	}
	
	/**
     * Fetches plan benefit & cost data from DB taking map as input.
     * @param request
     *              HttpServletRequest having map containing list of planIds , insuranceType, coverageDate
     * @return
     *              Object of <class>PdResponse</class>.
     */
	
	@RequestMapping(value = "/getPlanBenefitCostData", method = RequestMethod.POST)
	public @ResponseBody PdResponse getPlanBenefitCostData(@RequestBody String inputDataStr) {
		//LOGGER.info("========Inside getPlanBenefitCostData SERVICE method=========="+inputData);
		Type type = new TypeToken<Map<String, Object>>(){}.getType();
		Map<String, Object> inputData = platformGson.fromJson(inputDataStr, type);
		try {
			if (inputData == null) {
				return planDisplaySvcHelper.sendNull();
			}
			
			List<Double> planIdsDouble = (List<Double>)inputData.get("planIds");
			List<Integer> planIdsInt = new ArrayList<Integer>();
			for(Double planId : planIdsDouble){
				planIdsInt.add(planId.intValue());
			}
			PdResponse pdResponse = new PdResponse();
			pdResponse.startResponse();
			
			List<IndividualPlan> individualPlanList = new ArrayList<IndividualPlan>();
			PlanRequest planRequest = new PlanRequest();
			planRequest.setPlanIds(planIdsInt);
			planRequest.setInsuranceType(inputData.get("insuranceType").toString());
			planRequest.setEffectiveDate(inputData.get("coverageDate").toString());
			
			String planResponseStr = (ghixRestTemplate.exchange(PlanMgmtEndPoints.GET_PLAN_BENEFIT_AND_COST_DATA, GhixConstants.USER_NAME_EXCHANGE,HttpMethod.POST,MediaType.APPLICATION_JSON,  String.class, planRequest)).getBody();
			Type typePdRes = new TypeToken<List<PlanResponse>>() {}.getType();
			List<PlanResponse> planResponseList = (List<PlanResponse>)platformGson.fromJson(planResponseStr, typePdRes);
			//LOGGER.info("========planResponseList=========="+planResponseList);
			for(PlanResponse planResponse : planResponseList){
				IndividualPlan individualPlan = PlanDisplayUtil.buildPlanDetails(planResponse);
				individualPlanList.add(individualPlan);
			}
			pdResponse.setIndividualPlanList(individualPlanList);
			
			return planDisplaySvcHelper.sendSuccess(pdResponse);
			
		} catch (Exception e) {
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_EXECUTE_GET_PLAN_BENEFIT_COST_DATA,e);
		}
	}
	
    /**
     * Saves HouseholdSubsidy in PdOrderItem table taking <class>PdHouseholdDTO</class> as input .
     * @param request
     *              HttpServletRequest having <class>PdHouseholdDTO</class>
     * @return
     *              Object of <class>PdResponse</class>
     */
	
	@RequestMapping(value = "/updateHouseholdSubsidy", method = RequestMethod.POST)
	public @ResponseBody PdResponse updateHouseholdSubsidy(@RequestBody PdHouseholdDTO pdHouseholdDTO){
		try {
			PdResponse pdResponse = new PdResponse();
			pdResponse.startResponse();			
			
			if (pdHouseholdDTO != null) {
				List<PdOrderItem> pdOrderItemList = pdOrderItemService.findByHouseholdId(pdHouseholdDTO.getId());
				if (pdOrderItemList != null && !pdOrderItemList.isEmpty()) {
					for(PdOrderItem pdOrderItem : pdOrderItemList) {
						if(InsuranceType.DENTAL.equals(pdOrderItem.getInsuranceType())) {
							Float monthlySubsidy = pdHouseholdDTO.getMaxSubsidy() == null ? null : 0f; 
							pdOrderItem.setMonthlySubsidy(monthlySubsidy);
							pdOrderItem.setPremiumAfterCredit(pdOrderItem.getPremiumBeforeCredit());
							pdOrderItemService.save(pdOrderItem);
						}
					}
				}
			}
			
			return planDisplaySvcHelper.sendSuccess(pdResponse);
		} catch (Exception e) {
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_EXECUTE_UPDATE_HOUSEHOLD_SUBSIDY,e);
		}
	}
	
	/**
     * Recalculates and updates subsidy in PdOrderItem table taking map as input .
     * @param request
     *              HttpServletRequest having map containing householdId and aptc
     * @return
     *              Object of <class>PdHouseholdDTO</class>
     */
	
	@RequestMapping(value = "/reCalculateSubsidy", method = RequestMethod.POST)
	public @ResponseBody PdHouseholdDTO reCalculateSubsidy(@RequestBody String inputDataStr){	
		Type type = new TypeToken<Map<String, String>>(){}.getType();
		Map<String, String> inputData = platformGson.fromJson(inputDataStr, type);
		
		String householdId = inputData.get("householdId");
		String aptc = inputData.get("aptc");
		
		if (householdId == null || aptc == null) {
			return null;
		}
		
		PdHousehold pdHousehold = updateSubsidyInOrderItem(householdId, aptc, null);
		
		try {
			PdHouseholdDTO pdHouseholdDTO = new PdHouseholdDTO();
			createDTOBean.copyProperties(pdHouseholdDTO, pdHousehold);
			return pdHouseholdDTO;
		}catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occured while reCalculateSubsidy method---->", e, false);
			return null;
		}

	}
	
	private PdHousehold updateSubsidyInOrderItem(String householdId, String aptc, List<PdOrderItem> pdOrderItemList) {
		PdHousehold pdHousehold = pdHouseholdService.findById(Long.parseLong(householdId));
		if (pdOrderItemList == null)
		{
			pdOrderItemList = pdOrderItemService.findByHouseholdId(Long.parseLong(householdId));
		}		
		String coverageStartDate = DateUtil.dateToString(pdHousehold.getCoverageStartDate(), "yyyy-MM-dd");
		
		PdOrderItem healthOrderItem = null;
		PdOrderItem dentalOrderItem = null;
		Long healthMemberCount = null;
		Long dentalMemberCount = null;
		for(PdOrderItem pdOrderItem : pdOrderItemList){
			if(InsuranceType.HEALTH.equals(pdOrderItem.getInsuranceType())){
				healthOrderItem = pdOrderItem;
				healthMemberCount = pdOrderItemPersonService.countOrderItemPersonsByOrderItemId(pdOrderItem.getId());
			}
			if(InsuranceType.DENTAL.equals(pdOrderItem.getInsuranceType())){
				dentalOrderItem = pdOrderItem;
				dentalMemberCount = pdOrderItemPersonService.countOrderItemPersonsByOrderItemId(pdOrderItem.getId());
			}
		}
		
		IndividualPlan healthIndividualPlan = null;
		List<APTCPlanRequestDTO> planRequestList = new ArrayList<>();
		APTCCalculatorRequest aptcCalculatorRequest = new APTCCalculatorRequest();		
		aptcCalculatorRequest.setCurrentAptc(Float.valueOf(aptc));
		
		if(healthOrderItem != null){
			healthIndividualPlan = planDisplayUtil.getPlanInfo(healthOrderItem.getPlanId().toString(),coverageStartDate,true);			
			aptcCalculatorRequest.setMemberCount(healthMemberCount.intValue());
			if(healthOrderItem.getMonthlyStateSubsidy() != null) {
				aptcCalculatorRequest.setCurrentStateSubsidy(healthOrderItem.getMonthlyStateSubsidy());
			}
			APTCPlanRequestDTO healtPlanRequestDTO = planDisplayUtil.createPlanRequestDTO(healthOrderItem.getInsuranceType().toString(), healthOrderItem.getPremiumBeforeCredit(), healthIndividualPlan.getLevel(), healthIndividualPlan.getEhbPrecentage());
			planRequestList.add(healtPlanRequestDTO);
		}
		if(dentalOrderItem != null){
			List<PdOrderItemPerson> orderitemPersons = pdOrderItemPersonService.getByOrderItemId(dentalOrderItem.getId());
			boolean hasChild = false;
			List<APTCCalculatorMemberData> members = new ArrayList<APTCCalculatorMemberData>();
			for (PdOrderItemPerson pdOrderitemPerson : orderitemPersons)
			{
				PdPerson persons = pdOrderitemPerson.getPdPerson();
				Date covStartDate = null;
				if(persons.getDentalCoverageStartDate() != null)
				{
					covStartDate = persons.getDentalCoverageStartDate();
				}
				else
				{
					covStartDate = pdHousehold.getCoverageStartDate();
				}
				Integer age = planDisplayUtil.getAgeFromCoverageDate(covStartDate, persons.getBirthDay());
				APTCCalculatorMemberData member =  new APTCCalculatorMemberData();
				member.setId(pdOrderitemPerson.getId());
				member.setAge(age);
				member.setPremium(pdOrderitemPerson.getPremium());
				members.add(member);
				if(age < 19 && com.getinsured.hix.model.plandisplay.PlanDisplayEnum.Relationship.CHILD.equals(persons.getRelationship()))
				{
					hasChild = true;
					//break;
				}
			}
			
			IndividualPlan dentalIndividualPlan = planDisplayUtil.getPlanInfo(dentalOrderItem.getPlanId().toString(),coverageStartDate,true);			
			aptcCalculatorRequest.setMemberCount(dentalMemberCount.intValue());
			aptcCalculatorRequest.setIsChildPresent(hasChild);
			aptcCalculatorRequest.setMemberData(members);
			APTCPlanRequestDTO dentalPlanRequestDTO = planDisplayUtil.createPlanRequestDTO(dentalOrderItem.getInsuranceType().toString(), dentalOrderItem.getPremiumBeforeCredit(), dentalIndividualPlan.getLevel(), dentalIndividualPlan.getEhbPrecentage());
			planRequestList.add(dentalPlanRequestDTO);
		}
		aptcCalculatorRequest.setPlans(planRequestList);
		
		APTCCalculatorResponse aptcCalculatorResponse = planDisplayUtil.calculateSliderAptc(aptcCalculatorRequest);
		if(aptcCalculatorResponse != null && aptcCalculatorResponse.getPlans() != null && !aptcCalculatorResponse.getPlans().isEmpty()){
			for(APTCPlanResponseDTO planResponseDTO : aptcCalculatorResponse.getPlans()){
				if(planResponseDTO != null){
					Float appliedAptc = planResponseDTO.getAppliedAptc();
					Float premiumAfterCredit = planResponseDTO.getPremiumAfterCredit();
					BigDecimal appliedStateSubsidy = planResponseDTO.getAppliedStateSubsidy();
					String insuranceType = planResponseDTO.getInsuranceType();
					
					if(InsuranceType.HEALTH.toString().equals(insuranceType)){
						healthOrderItem.setMonthlySubsidy(appliedAptc);
						healthOrderItem.setPremiumAfterCredit(premiumAfterCredit);
						if(appliedStateSubsidy != null){
							healthOrderItem.setMonthlyStateSubsidy(appliedStateSubsidy);
						}
						pdOrderItemService.save(healthOrderItem);
					}else{
						dentalOrderItem.setMonthlySubsidy(appliedAptc);
						dentalOrderItem.setPremiumAfterCredit(premiumAfterCredit);
						pdOrderItemService.save(dentalOrderItem);
					}
				}
			}
		}
		
		return pdHousehold;
	}
	
    /**
     * Fetches PdOrderItem object based on cartItemId as input from DB and returns json of PdOrderItemDTO.
     * @param request
     *              HttpServletRequest having parameter "cartItemId"
     * @return
     *              Object of <class>PdOrderItemDTO</class>
     */
	@RequestMapping(value = "/findCartItemById", method = RequestMethod.GET)
	public @ResponseBody String findCartItemById(HttpServletRequest request){
		try {
			String cartItemId = (String) request.getParameter("cartItemId");
			if (cartItemId == null) {
				return null;
			}
			
			PdOrderItem pdOrderItem = pdOrderItemService.findById(Long.parseLong(cartItemId));
			PdOrderItemDTO pdOrderItemDTO = new PdOrderItemDTO();
			createDTOBean.copyProperties(pdOrderItemDTO, pdOrderItem);
			return platformGson.toJson(pdOrderItemDTO);
			
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occured while findCartItemById method---->", e, false);
			return null;
		}
	}
	
	/**
     * Fetches PdOrderItem list based on leadId & year as input from DB and returns json of PdOrderItemDTO.
     * @param request
     *              HttpServletRequest having parameter "leadId" and "year"
     * @return
     *              List of <class>PdOrderItemDTO</class>
     */
	
	@RequestMapping(value = "/findPreshoppingCartItems", method = RequestMethod.GET)
	public @ResponseBody List<PdOrderItemDTO> findPreshoppingCartItems(HttpServletRequest request){
		try {
			String leadId = (String) request.getParameter("leadId");
			String year = (String) request.getParameter("year");
			if (leadId == null) {
				return null;
			}
			
			List<PdOrderItemDTO> pdOrderItemDTOList = new ArrayList<PdOrderItemDTO>();
			PdHousehold pdHousehold = pdHouseholdService.findPreshoppingHousehold(leadId , year);
			if(pdHousehold != null){
				List<PdOrderItem> pdOrderItemList = pdOrderItemService.findByHouseholdId(pdHousehold.getId());
				for(PdOrderItem pdOrderItem : pdOrderItemList){
					PdOrderItemDTO pdOrderItemDTO = new PdOrderItemDTO();
					createDTOBean.copyProperties(pdOrderItemDTO, pdOrderItem);
					pdOrderItemDTOList.add(pdOrderItemDTO);
				}
			}
			return pdOrderItemDTOList;

		} catch (Exception e) {
			return null;
		}
	}
	
	/**
     * Fetches PdOrderItem list based on leadId as input from DB and returns json of PdResponse.
     * @param request
     *              HttpServletRequest having parameter "leadId"
     * @return
     *              Object of <class>PdResponse</class>
     */
	
	@RequestMapping(value="/findPlanIdByLeadId/{leadId}",method = RequestMethod.GET)
	public @ResponseBody PdResponse findPlanIdByLeadId(@PathVariable String leadId) {

		return getPlansByLeadIdAndYear(leadId, null);
	}
	
	/**
     * Fetches PdOrderItem list based on leadId and year as input from DB and returns json of PdResponse.
     * @param request
     *              HttpServletRequest having parameter "leadId" and "year"
     * @return
     *              Object of <class>PdResponse</class>
     */
	
	@RequestMapping(value={"/findPlanIdByLeadId/{leadId}/{year}"},method = RequestMethod.GET)
	public @ResponseBody PdResponse findPlanIdByLeadId(@PathVariable String leadId, @PathVariable String year) {
		
		return getPlansByLeadIdAndYear(leadId, year);
	}
	
	 /**
     * Saves preShopping data in PdHousehold table taking <class>PdPreShopRequest</class> as input .
     * @param request
     *              HttpServletRequest having <class>PdPreShopRequest</class>
     * @return
     *              Object of <class>PdHouseholdDTO</class>
     */
	
	@RequestMapping(value = "/savePreShoppingData", method = RequestMethod.POST)
	public @ResponseBody PdResponse savePreShoppingData(@RequestBody PdPreShopRequest pdPreshopRequest){
		if (pdPreshopRequest == null) {
			return null;
		}
			
		PdResponse pdResponse = new PdResponse();
		if (pdPreshopRequest.getEligLead() != null){
			pdResponse = planDisplaySvcHelper.createPdHousehold(pdPreshopRequest.getEligLead(), pdPreshopRequest.getInsuranceType(), pdPreshopRequest.getEffectiveStartDate());	
		} else {
			pdResponse = planDisplaySvcHelper.createPdHousehold(pdPreshopRequest.getGroupData(), pdPreshopRequest.getInsuranceType(), pdPreshopRequest.getEffectiveStartDate());
		}
		return pdResponse;
	}

	/**
     * Saves OrderItem for cmsPlanId in PdOrderItem table taking map as input .
     * @param request
     *              HttpServletRequest having map which contains "houseHoldId" and "issuerPlanId"
     * @return
     *              Object of <class>PdResponse</class>
     */
	
	@RequestMapping(value = "/createOrderItemForCmsPlanId", method = RequestMethod.POST)
	public @ResponseBody PdResponse createOrderItemForCmsPlanId(@RequestBody String inputDataStr) 
	{
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside createOrderItemForCmsPlanId() method---->", null, false);	
		Type type = new TypeToken<Map<String, String>>(){}.getType();
		Map<String, String> inputData = platformGson.fromJson(inputDataStr, type);
		
		PdResponse pdResponse = new PdResponse();
		try {
			pdResponse.startResponse();
			if (inputData == null) {
				return planDisplaySvcHelper.sendNull();
			}

			Long pdHouseholdId = Long.parseLong(inputData.get("houseHoldId"));
			String issuerPlanId = (String) inputData.get("issuerPlanId");
			String exchangeTypeStr = (String) inputData.get("exchangeType");
			
			ExchangeType exchangeType = null;
			if(StringUtils.isNotBlank(exchangeTypeStr)){
				exchangeType = ExchangeType.valueOf(exchangeTypeStr);
			}
			
			if(exchangeType == null){
				exchangeType = ExchangeType.ON;
			}
			PdHousehold pdHousehold = pdHouseholdService.findById(pdHouseholdId);
			if(pdHousehold == null){ 
				return planDisplaySvcHelper.sendFailure("Invalid Household Id = "+pdHouseholdId);
			}
			PdHouseholdDTO pdHouseholdDTO = new PdHouseholdDTO();
			createDTOBean.copyProperties(pdHouseholdDTO, pdHousehold);

			List<PdPerson> pdPersonList = pdPersonService.getPersonListByHouseholdId(pdHouseholdId);
			if(pdPersonList==null || pdPersonList.isEmpty()){
				return planDisplaySvcHelper.sendFailure("Invalid Household Id = "+pdHouseholdId);
			}
			List<PdPersonDTO> pdPersonDTOList = new ArrayList<PdPersonDTO>();
			for(PdPerson pdPerson : pdPersonList){
				PdPersonDTO pdPersonDTO = new PdPersonDTO();
				createDTOBean.copyProperties(pdPersonDTO, pdPerson);
				pdPersonDTOList.add(pdPersonDTO);
			}
			
			PdQuoteRequest pdQuoteRequest = planDisplaySvcHelper.createPdQuoteRequest(pdHouseholdDTO, pdPersonDTOList, null, issuerPlanId, EnrollmentType.I, InsuranceType.HEALTH, exchangeType);						
			pdQuoteRequest.setInitialCmsPlanId(issuerPlanId);
			List<IndividualPlan> individualPlanList= individualPlanService.getPlans(pdQuoteRequest);
			
			if(individualPlanList != null && !individualPlanList.isEmpty()) {
				IndividualPlan individualPlan =	individualPlanList.get(0);
				PdSaveCartRequest pdSaveCartRequest = new PdSaveCartRequest();
				pdSaveCartRequest.setInsuranceType(InsuranceType.HEALTH);
				pdSaveCartRequest.setHouseholdId(pdHouseholdDTO.getId());
				pdSaveCartRequest.setMonthlySubsidy(new BigDecimal(Float.toString(individualPlan.getAptc())).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
				pdSaveCartRequest.setMonthlyStateSubsidy(individualPlan.getStateSubsidy());
				pdSaveCartRequest.setPlanId(individualPlan.getPlanId().longValue());
				pdSaveCartRequest.setPremiumAfterCredit(new BigDecimal(Float.toString(individualPlan.getPremiumAfterCredit())).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
				pdSaveCartRequest.setPremiumBeforeCredit(new BigDecimal(Float.toString(individualPlan.getPremiumBeforeCredit())).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
				pdSaveCartRequest.setPremiumData(individualPlan.getPlanDetailsByMember());
				pdSaveCartRequest.setExchangeType(exchangeType);
		
				pdResponse = this.saveCartItem(pdSaveCartRequest);
				return planDisplaySvcHelper.sendSuccess(pdResponse);
			}else{
				return planDisplaySvcHelper.sendFailure("Plan is not available");
			}
		} catch (NumberFormatException e) {
			return planDisplaySvcHelper.sendFailure("Invalid Household Id");
		} catch (Exception e) {
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_EXECUTE_SAVE_PREFERENCES_BY_HOUSEHOLD_ID,e);
		}
	}
	/**
     * Fetches Plan details based on applicationId as input from DB and returns json of PlanDisplayResponse.
     * @param request
     *              HttpServletRequest having PlanDisplayRequest
     * @return
     *              Object of <class>PlanDisplayResponse</class>
     */
	
	@RequestMapping(value="/findPlanIdByApplicationId", method = RequestMethod.POST)
	public @ResponseBody PlanDisplayResponse findPlanIdByApplicationId(@RequestBody PlanDisplayRequest planDisplayRequest) 
	{
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside findPlanIdByApplicationId() method---->", null, false);
		PlanDisplayResponse planDisplayResponse = new PlanDisplayResponse();

		try 
		{
			planDisplayResponse.startResponse();
			if (planDisplayRequest == null ) {
				planDisplayResponse.setStatus("FAILED");
				planDisplayResponse.setErrMsg("Invalid input: Search criteria cannot be Null or empty");
				planDisplayResponse.endResponse();
				return planDisplayResponse;
			}
			
			List<PldOrderRequest> consumerData = planDisplayRequest.getConsumerData();
			List<PldOrderResponse> applicationDetails = new ArrayList<PldOrderResponse>();
			
			for(PldOrderRequest consumer : consumerData)
			{
				PldOrderResponse pldOrderResponse = new PldOrderResponse();
				List<PdOrderItem> pdOrderItemList = pdOrderItemService.findByApplicationId(consumer.getApplicationId());
				for(PdOrderItem pdOrderItem : pdOrderItemList){
					PdHousehold pdHousehold = pdOrderItem.getPdHousehold();
					pldOrderResponse.setPlanId(pdOrderItem.getPlanId().toString());
					pldOrderResponse.setPremium(pdOrderItem.getPremiumAfterCredit());
					pldOrderResponse.setOrderDate(DateUtil.dateToString(pdHousehold.getCoverageStartDate(), "MMddyyyy"));
					pldOrderResponse.setPlanPremium(pdOrderItem.getPremiumBeforeCredit());
					pldOrderResponse.setSsapApplicationId(consumer.getApplicationId());
					pldOrderResponse.setRequestType(pdOrderItem.getInsuranceType().toString());
					pldOrderResponse.setOrderId(pdHousehold.getId().toString());
					applicationDetails.add(pldOrderResponse);
				}
			}
			
			
			planDisplayResponse.setPldOrderResponse(applicationDetails);
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----findPlanIdByApplicationId getLatestHouseholdIdByEligLeadId call---->", null, false);
			planDisplayResponse.setStatus("SUCCESS");
			planDisplayResponse.setErrMsg("");
		}
		catch (Exception e) 
		{
			planDisplayResponse.setStatus("FAILED");
			planDisplayResponse.setErrMsg(e.getMessage());
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occured while finding planId by leadId---->", e, false);
		}
		planDisplayResponse.endResponse();
		return planDisplayResponse;
	}
	
	/**
     * Fetches Provider details based on ssapApplicationId as input from DB and returns json of PlanDisplayResponse.
     * @param request
     *              HttpServletRequest having ssapApplicationId parameter
     * @return
     *              Object of <class>PlanDisplayResponse</class>
     */
	
	@RequestMapping(value = "/findProviderBySsapApplicationId/{ssapApplicationId}", method = RequestMethod.POST)
	public @ResponseBody PlanDisplayResponse findProviderBySsapApplicationId(@PathVariable String ssapApplicationId) 
	{
		PlanDisplayResponse pldresponse = new PlanDisplayResponse();
		List<ProviderBean> providerList =  new ArrayList<ProviderBean>();
		//ObjectMapper mapper = new ObjectMapper();
		pldresponse.startResponse();
		if( StringUtils.isNotEmpty(ssapApplicationId) && !StringUtils.isNumeric(ssapApplicationId)){
			pldresponse.setStatus("FAILED");
			pldresponse.setResponse("FAILED");
			pldresponse.setErrMsg("Invalid input: ssapApplicationId cannot be Null or empty");
			pldresponse.endResponse();
			return pldresponse;
		}
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Finding provider for ssapApplicationId---->"+ssapApplicationId, null, false);
		try {
			List<PdOrderItem> pdOrderItemList = pdOrderItemService.findByApplicationId(Long.parseLong(ssapApplicationId));
			if (pdOrderItemList != null && pdOrderItemList.get(0) != null) {
				PdHousehold pdHousehold = pdOrderItemList.get(0).getPdHousehold();
				
				String providerJson =  pdHousehold.getProviders();
				if(providerJson!=null && providerJson.length() > 0){  
					ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaTypeList(ArrayList.class, ProviderBean.class);
					providerList = reader.readValue(providerJson);
				}
				
				pldresponse.setProvidersList(providerList);
			}
			pldresponse.setStatus("SUCCESS");
		} catch (Exception e) {
			pldresponse.setStatus("FAILED");
			pldresponse.setErrMsg(e.getMessage());
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occured while finding providers by leadId---->", e, false);
		}
		pldresponse.endResponse();
		return pldresponse;
	}
	
	/**
     * Saves ApplicantEligibilityDetails in PdHousehold table taking applicantEligibilityRequest in XML.
     * @param request
     *              HttpServletRequest having applicantEligibilityRequest in XML.
     * @return
     *              Object of <class>PdHouseholdDTO</class>
     */
	
	@RequestMapping(value = "/saveApplicantEligibilityDetails", method = RequestMethod.POST)
	@ResponseBody public PdHouseholdDTO saveApplicantEligibilityDetails(@RequestBody String applicantEligibilityRequestStr) throws HTTPException, Exception
	{
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside PlandisplayController saveApplicantEligibilityDetails method---->", null, false);
		XStream xstream = GhixUtils.getXStreamStaxObject();
		ApplicantEligibilityData applicantEligibilityData = (ApplicantEligibilityData)xstream.fromXML(applicantEligibilityRequestStr);
		try{
			if (applicantEligibilityData == null) {
				return null;
			}
			
			PdHousehold pldHosuehold = pdHouseholdService.saveApplicantEligibilityDetails(applicantEligibilityData);
			PdHouseholdDTO pdHouseholdDTO = new PdHouseholdDTO();
			createDTOBean.copyProperties(pdHouseholdDTO, pldHosuehold);
			return pdHouseholdDTO;
		}catch(Exception e){
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, PlanDisplayConstants.ErrorCodes.FAILED_TO_SAVE_APPLICATION_ELIGIBILITY_DATA.getMessage()+PlanDisplayConstants.COLON, e, false);
			return null;
			
		}
	}
	
	/**
     * Fetches Household details based on ssapApplicationId and requestType as input from DB and returns json of PdResponse.
     * @param request
     *              HttpServletRequest having map which contains "ssapApplicationId" and "requestType"
     * @return
     *              Object of <class>PlanDisplayResponse</class>
     */
	
	@RequestMapping(value="/findHouseholdBySsapIdAndReqType", method = RequestMethod.POST)
	public @ResponseBody PdResponse findHouseholdBySsapIdAndReqType(@RequestBody String inputDataStr) 
	{	
		Type type = new TypeToken<Map<String, Object>>(){}.getType();
		Map<String, Object> inputData = platformGson.fromJson(inputDataStr, type);
		
		PdResponse pdResponse = new PdResponse();

		try{
			Double ssapApplicationIdDouble = (Double) inputData.get("ssapApplicationId");
			Long ssapApplicationId = ssapApplicationIdDouble.longValue();
			String requestType =  inputData.get("requestType") != null ?  inputData.get("requestType").toString() : "";
			
			PdHousehold latestHousehold = pdHouseholdService.getHouseholdByApplicationId(ssapApplicationId, requestType);
			if( latestHousehold == null){
				throw new Exception(PlanDisplayConstants.ErrorCodes.FAILED_TO_FIND_HOUSEHOLD_BY_SSAPIDANDREQTYPE.getMessage());
			}
			PdHouseholdDTO pdHouseholdDTO = new PdHouseholdDTO();
			createDTOBean.copyProperties(pdHouseholdDTO, latestHousehold);
			pdResponse.setPdHouseholdDTO(pdHouseholdDTO);
			
			PdPreferences pdPreferences = pdPreferencesService.findPreferencesByEligLeadId(pdHouseholdDTO.getEligLeadId());
			PdPreferencesDTO pdPreferencesDTO = planDisplayUtil.getPdPreferencesDTO(pdPreferences);
			pdResponse.setPdPreferencesDTO(pdPreferencesDTO);
			
			planDisplaySvcHelper.sendSuccess(pdResponse);
		} catch(Exception e) {
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_FIND_HOUSEHOLD_BY_SSAPIDANDREQTYPE,e);
		}

		return pdResponse;
	}
	
	/**
     * Fetches NonEligible member list based on giWsPayloadId as input from DB and returns json.
     * @param request
     *              HttpServletRequest having giWsPayloadId
     * @return
     *              Json of List<class>PdPersonDTO</class>
     */
	
	@RequestMapping(value="/getNonEligibleMemberList/{giWsPayloadId}", method= RequestMethod.GET)
	public @ResponseBody String getNonEligibleMemberList(@PathVariable String giWsPayloadId) {
		try{
			if (giWsPayloadId == null) {
				return null;
			}
			ApplicantEligibilityResponseType applicantEligibilityResponse = planDisplayUtil.getEligiblityResponse(Long.parseLong(giWsPayloadId));
			List<PdPersonDTO> pdPersonDTOList = planDisplayUtil.getNonEligibleMemberList(applicantEligibilityResponse); 
			return platformGson.toJson(pdPersonDTOList);
		}catch(Exception e){	
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, PlanDisplayConstants.ErrorCodes.FAILED_TO_GET_NON_ELIGIBLE_MEMBERS.getMessage() + PlanDisplayConstants.COLON, e, false);
			return null;
		}
	}
	
	/**
     * Saves tobaco flag in PdPerson table taking List<PdPersonDTO> pdPersonDTOList as input.
     * @param request
     *              HttpServletRequest having List<PdPersonDTO>.
     * @return
     *              Object of <class>PdResponse</class>
     */
	
	@RequestMapping(value = "/updatePersonTobacco", method = RequestMethod.POST)
	public @ResponseBody PdResponse updatePersonTobacco(@RequestBody List<PdPersonDTO> pdPersonDTOList){
		try {
			PdResponse pdResponse = new PdResponse();
			pdResponse.startResponse();
			
			if (pdPersonDTOList != null && !pdPersonDTOList.isEmpty()) {
				for(PdPersonDTO pdPersonDTO: pdPersonDTOList) {
					if(pdPersonDTO.getId() != null){
						PdPerson pdPerson = pdPersonService.findById(pdPersonDTO.getId());
						pdPerson.setTobacco(pdPersonDTO.getTobacco());
						pdPersonService.savePerson(pdPerson);
					}
				}
			}			
			return planDisplaySvcHelper.sendSuccess(pdResponse);
		} catch (Exception e) {
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_EXECUTE_UPDATE_PERSON_TOBACCO,e);
		}
	}
	
	/**
     * Updates coverageDate in PdHousehold table taking PdHouseholdDTO as input.
     * @param request
     *              HttpServletRequest having PdHouseholdDTO.
     * @return
     *              Object of <class>PdResponse</class>
     */
	
	@RequestMapping(value = "/setCoverageDate", method = RequestMethod.POST)
	public @ResponseBody PdResponse setCoverageDate(@RequestBody PdHouseholdDTO pdHouseholdDTO) 
	{
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside saveHousehold() method---->", null, false);
		try {
			PdResponse pdResponse = new PdResponse();
			pdResponse.startResponse();
			if (pdHouseholdDTO == null) {				
				return null;
			}
			
			PdHousehold pdHousehold = pdHouseholdService.findById(pdHouseholdDTO.getId());
			pdHousehold.setCoverageStartDate(pdHouseholdDTO.getCoverageStartDate());
			pdHousehold = pdHouseholdService.save(pdHousehold);
			createDTOBean.copyProperties(pdHouseholdDTO, pdHousehold);
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----PdHousehold saved successfully---->", null, false);
			pdResponse.setPdHouseholdDTO(pdHouseholdDTO);	
			PdPreferences pdPreferences = pdPreferencesService.findPreferencesByEligLeadId(pdHousehold.getEligLeadId());
			PdPreferencesDTO pdPreferencesDTO = this.planDisplayUtil.getPdPreferencesDTO(pdPreferences);
		    pdResponse.setPdPreferencesDTO(pdPreferencesDTO);		      
				
			return planDisplaySvcHelper.sendSuccess(pdResponse);

		} catch (Exception e) {
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_SAVE_PDHOUSHOLD,e);
		}
	}
	
	@RequestMapping(value = "/setLeadId", method = RequestMethod.POST)
	public @ResponseBody PdResponse setLeadId(@RequestBody PdHouseholdDTO pdHouseholdDTO) 
	{
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside saveHousehold() method---->", null, false);
		try {
			PdResponse pdResponse = new PdResponse();
			pdResponse.startResponse();
			if (pdHouseholdDTO == null) {				
				return null;
			}
			
			PdHousehold pdHousehold = pdHouseholdService.findById(pdHouseholdDTO.getId());
			pdHousehold.setEligLeadId(pdHouseholdDTO.getEligLeadId());
			pdHousehold = pdHouseholdService.save(pdHousehold);
			pdResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			pdResponse.endResponse();
			return pdResponse;
		} catch (Exception e) {
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_SAVE_PDHOUSHOLD,e);
		}
	}
	
	/**
     * Fetches HouseholdContact details based on giWsPayloadId as input from DB and returns json.
     * @param request
     *              HttpServletRequest having giWsPayloadId
     * @return
     *              Json of <class>FFMPerson</class>
     */
	
	@RequestMapping(value="/getHouseholdContact/{giWsPayloadId}", method= RequestMethod.GET)
	public @ResponseBody String getHouseholdContact(@PathVariable String giWsPayloadId) {
		try{
			if (giWsPayloadId == null && !StringUtils.isNumeric(giWsPayloadId)) {
				return null;
			}
			
			FFMPerson householdContact = planDisplayUtil.getHouseholdContact(Long.parseLong(giWsPayloadId)); 
			return platformGson.toJson(householdContact);
		}catch(Exception e){	
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, PlanDisplayConstants.ErrorCodes.FAILED_TO_GET_NON_ELIGIBLE_MEMBERS.getMessage() + PlanDisplayConstants.COLON, e, false);
			return null;
		}
	}
	
	/**
     * Fetches PdOrderItem details based on OrderItemId as input from DB and returns json.
     * @param request
     *              HttpServletRequest having cartItemId parameter
     * @return
     *              Object of <class>PdResponse</class>
     */
	
	@RequestMapping(value = "/findSsapApplicationIdByOrderItemId", method = RequestMethod.POST)
	public @ResponseBody PdResponse findSsapApplicationIdByOrderItemId(@RequestBody String cartItemId)
	{
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside findPldOrderItemBySsapApplicationId() method---->", null, false);
		try {
			if (cartItemId == null) {
				return planDisplaySvcHelper.sendNull();
			}
			PdResponse pdResponse = new PdResponse();
			pdResponse.startResponse();
			PdOrderItem pdOrderItem = pdOrderItemService.findById(Long.parseLong(cartItemId));
			pdResponse.setApplicationId(pdOrderItem.getSsapApplicationId());
			return planDisplaySvcHelper.sendSuccess(pdResponse);
		} catch (Exception e) {
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_FIND_PDORDERITEM_BY_SSAPAPPLICATIONID,e);
		}		
	}
	
	/**
     * Fetches FFMMemberResponse details based on giWsPayloadId as input from DB and returns json.
     * @param request
     *              HttpServletRequest having giWsPayloadId
     * @return
     *              Json of List<class>FFMMemberResponse</class>
     */
	
	@RequestMapping(value="/getFFMMemberResponseList/{giWsPayloadId}", method= RequestMethod.GET)
	public @ResponseBody String getFFMMemberResponseList(@PathVariable String giWsPayloadId) {
		try{
			if (giWsPayloadId == null) {
				return null;
			}
			
			List<FFMMemberResponse> fFMMemberResponseList = planDisplayUtil.getFFMMemberResponseList(Long.parseLong(giWsPayloadId));
			return platformGson.toJson(fFMMemberResponseList);
		}catch(Exception e){	
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, PlanDisplayConstants.ErrorCodes.FAILED_TO_GET_NON_ELIGIBLE_MEMBERS.getMessage() + PlanDisplayConstants.COLON, e, false);
			return null;
		}
	}
	
	/**
     * Fetches AME plans with maximum benefit based on PdQuoteRequest as input from DB and returns json.
     * @param request
     *              HttpServletRequest having PdQuoteRequest
     * @return
     *              Object of <class>PlanDisplayResponse</class>
     */
	
	@RequestMapping(value = "/getmaxbenefitameplan", method = RequestMethod.POST)
	public @ResponseBody PlanDisplayResponse getMaxBenefitAmePlan(@RequestBody PdQuoteRequest pdQuoteRequest) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside getMaxBenefitAmePlan() method---->", null, false);
		try{
			if (pdQuoteRequest == null) {
				return null;
			}			
			PlanDisplayResponse planDisplayResponse = new PlanDisplayResponse();
			planDisplayResponse.startResponse();
		
			PdHouseholdDTO pdHouseholdDTO = pdQuoteRequest.getPdHouseholdDTO();
			List<PdPersonDTO> pdPersonDTOList = pdQuoteRequest.getPdPersonDTOList();
			Map<String, String> subscriberData = individualPlanService.getSubscriberData(pdPersonDTOList, pdHouseholdDTO, pdQuoteRequest.getInitialSubscriberId(), pdQuoteRequest.getSpecialEnrollmentFlowType(), pdQuoteRequest.getInsuranceType());
			List<Map<String,String>> censusList = PlanDisplayUtil.getMemberListForQuoting(pdPersonDTOList, subscriberData, pdHouseholdDTO.getCoverageStartDate(), pdHouseholdDTO.getEnrollmentType(), pdQuoteRequest.getSpecialEnrollmentFlowType(), pdQuoteRequest.getInsuranceType());
			String coverageStartDate = DateUtil.dateToString(pdHouseholdDTO.getCoverageStartDate(), "yyyy-MM-dd");
			Map<String, Object> result = planDisplayUtil.getMaxBenefitAmePlan(coverageStartDate, censusList);
			
			planDisplayResponse.setOutputData(result);
			planDisplayResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			planDisplayResponse.endResponse();
			return planDisplayResponse;
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
     * Fetches Dental plans with lowest cost based on PdQuoteRequest as input from DB and returns json.
     * @param request
     *              HttpServletRequest having PdQuoteRequest
     * @return
     *              Object of <class>PlanDisplayResponse</class>
     */
	
	@RequestMapping(value = "/getlowestdentalplan", method = RequestMethod.POST)
	public @ResponseBody PlanDisplayResponse getLowestDentalPlan(@RequestBody PdQuoteRequest pdQuoteRequest) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside getLowestDentalPlan() method---->", null, false);
		try{
			if (pdQuoteRequest == null) {
				return null;
			}			
			PlanDisplayResponse planDisplayResponse = new PlanDisplayResponse();
			planDisplayResponse.startResponse();
		
			PdHouseholdDTO pdHouseholdDTO = pdQuoteRequest.getPdHouseholdDTO();
			List<PdPersonDTO> pdPersonDTOList = pdQuoteRequest.getPdPersonDTOList();
			Map<String, String> subscriberData = individualPlanService.getSubscriberData(pdPersonDTOList, pdHouseholdDTO, pdQuoteRequest.getInitialSubscriberId(), pdQuoteRequest.getSpecialEnrollmentFlowType(), pdQuoteRequest.getInsuranceType());
			List<Map<String,String>> censusList = PlanDisplayUtil.getMemberListForQuoting(pdPersonDTOList, subscriberData, pdHouseholdDTO.getCoverageStartDate(), pdHouseholdDTO.getEnrollmentType(), pdQuoteRequest.getSpecialEnrollmentFlowType(), pdQuoteRequest.getInsuranceType());
			String coverageStartDate = DateUtil.dateToString(pdHouseholdDTO.getCoverageStartDate(), "yyyy-MM-dd");			
			Map<String, Object> result = planDisplayUtil.getLowestDentalPlan(coverageStartDate, censusList, pdQuoteRequest.getTenant(), pdQuoteRequest.getExchangeType().toString());
			
			planDisplayResponse.setOutputData(result);
			planDisplayResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			planDisplayResponse.endResponse();
			return planDisplayResponse;
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
     * Fetches Vision plans with lowest cost based on PdQuoteRequest as input from DB and returns json.
     * @param request
     *              HttpServletRequest having PdQuoteRequest
     * @return
     *              Object of <class>PlanDisplayResponse</class>
     */
	
	@RequestMapping(value = "/getlowestvisionplan", method = RequestMethod.POST)
	public @ResponseBody PlanDisplayResponse getLowestVisionPlan(@RequestBody PdQuoteRequest pdQuoteRequest) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside getLowestVisionPlan() method---->", null, false);
		try{
			if (pdQuoteRequest == null) {
				return null;
			}			
			PlanDisplayResponse planDisplayResponse = new PlanDisplayResponse();
			planDisplayResponse.startResponse();
		
			PdHouseholdDTO pdHouseholdDTO = pdQuoteRequest.getPdHouseholdDTO();
			List<PdPersonDTO> pdPersonDTOList = pdQuoteRequest.getPdPersonDTOList();
			Map<String, String> subscriberData = individualPlanService.getSubscriberData(pdPersonDTOList, pdHouseholdDTO, pdQuoteRequest.getInitialSubscriberId(), pdQuoteRequest.getSpecialEnrollmentFlowType(), pdQuoteRequest.getInsuranceType());
			List<Map<String,String>> censusList = PlanDisplayUtil.getMemberListForQuoting(pdPersonDTOList, subscriberData, pdHouseholdDTO.getCoverageStartDate(), pdHouseholdDTO.getEnrollmentType(), pdQuoteRequest.getSpecialEnrollmentFlowType(), pdQuoteRequest.getInsuranceType());
			String coverageStartDate = DateUtil.dateToString(pdHouseholdDTO.getCoverageStartDate(), "yyyy-MM-dd");			
			Map<String, Object> result = planDisplayUtil.getLowestVisionPlan(coverageStartDate, censusList);
			planDisplayResponse.setOutputData(result);
			planDisplayResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			planDisplayResponse.endResponse();
			return planDisplayResponse;
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
     * Updates OrderItem As Enrolled in DB taking List<Long> orderItemIds as input.
     * @param request
     *              HttpServletRequest having List<Long> orderItemIds.
     * @return
     *              Object of <class>PlanDisplayResponse</class>
     */
	@RequestMapping(value = "/updateOrderItemAsEnrolled", method = RequestMethod.POST)
	public @ResponseBody PlanDisplayResponse updateOrderItemAsEnrolled(@RequestBody List<Long> orderItemIds){
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside updateOrderItemAsEnrolled() method---->", null, false);
		boolean result = false;
		Map<String,Object> updateStatus = new HashMap<String,Object>();
		PlanDisplayResponse planDisplayResponse = new PlanDisplayResponse();
		planDisplayResponse.startResponse();
		
		if (orderItemIds == null || orderItemIds.isEmpty()) {
			planDisplayResponse.setErrMsg(GhixConstants.EMPTY_OR_NULL);
			planDisplayResponse.endResponse();
			return planDisplayResponse;
		}
		for (Long orderItemId : orderItemIds) {
		result = pdOrderItemService.updateOrderItemStatus(orderItemId, Status.ENROLLED);
			if(result){
				updateStatus.put(String.valueOf(orderItemId),GhixConstants.UPDATE_SUCCESS);
			}
			else{
				updateStatus.put(String.valueOf(orderItemId),GhixConstants.UPDATE_FAILED);
			}
		}
		planDisplayResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		planDisplayResponse.setOutputData(updateStatus);
		planDisplayResponse.endResponse();
		return planDisplayResponse;
	}
	
	/**
     * Fetches all Order Items from DB taking leadId & year as input.
     * @param request
     *              HttpServletRequest having "leadId" and "year" parameters.
     * @return
     *              List<PdOrderItem>
     */
	@RequestMapping(value = "/getPdOrderItemsByLeadId", method = RequestMethod.GET)
	public @ResponseBody List<PdOrderItemDTO> getPdOrderItemsByLeadId(HttpServletRequest request){
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside getPdOrderItemsByLeadId() method---->", null, false);
		PlanDisplayResponse planDisplayResponse = new PlanDisplayResponse();
		planDisplayResponse.startResponse();
		String leadId = (String) request.getParameter("leadId");
		String year = (String) request.getParameter("year");
		
		if (leadId == null) {
			return null;
		}
		List<PdOrderItemDTO> pdOrderItemDTOList = new ArrayList<PdOrderItemDTO>();
		try{
			PdHousehold pdHousehold = pdHouseholdService.findPreshoppingHousehold(leadId,year);
			if(pdHousehold != null){
				planDisplaySvcHelper.getPdOrderItemsByHouseholdId(pdHousehold, pdOrderItemDTOList);
			}
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occured while getting OrderItemStatus List in orderItemStatusResponse---->", e, false);
		}
		
		return pdOrderItemDTOList;
	}

	/**
     * Deletes ssapApplicationId from OrderItem  taking ssapApplicationId as input.
     * @param request
     *              HttpServletRequest having "ssapApplicationId" parameter.
     * @return
     *              Object of <class>PlanDisplayResponse</class>
     */
	
	@RequestMapping(value = "/removeSsapApplicationIdFromOrderItem", method = RequestMethod.POST)
	public @ResponseBody PlanDisplayResponse removeSsapApplicationIdFromOrderItem(@RequestBody String ssapApplicationId)
	{
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside removeSsapApplicationIdFromOrderItem() method---->", null, false);
		PlanDisplayResponse planDisplayResponse = new PlanDisplayResponse();
		try {
			planDisplayResponse.startResponse();

			if (ssapApplicationId != null) {
				String id=ssapApplicationId.replaceAll("\"", "");
				List<PdOrderItem> pdOrderItemList =  pdOrderItemService.findByApplicationId(Long.parseLong(id));
				
				if(pdOrderItemList.isEmpty()){
					planDisplayResponse.setStatus("FAILED");
					planDisplayResponse.setErrMsg("Record not found for Ssap_Application_Id :"+id);
					planDisplayResponse.endResponse();
					return planDisplayResponse;
				}
				PdOrderItem pdOrderItem = pdOrderItemList.get(0);
				pdOrderItemService.deleteItem(pdOrderItem.getId());
				planDisplaySvcHelper.publishPartnerEvent(String.valueOf(pdOrderItem.getId()), pdOrderItem.getPdHousehold().getEligLeadId(), PartnerEventTypeEnum.REMOVE_FROM_CART);
				planDisplayResponse.setStatus("SUCCESS");
			} else {
				planDisplayResponse.setStatus("FAILED");
				planDisplayResponse
				.setErrMsg("Invalid input: itemId cannot be Null");
			}
			planDisplayResponse.endResponse();
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occured while setting ssapApplicationId null in removeSsapApplicationIdFromOrderItem---->", e, false);
		}
		return planDisplayResponse;
	}
	
	/**
     * Fetches all Order Items from DB taking pdHouseholdId & coverageStartDate as input.
     * @param request
     *              HttpServletRequest having "pdHouseholdId" and "coverageStartDate" parameters.
     * @return
     *              Object of <class>PdResponse</class>
     */
	
	@RequestMapping(value = "/getCartItems", method = RequestMethod.POST)
	public @ResponseBody PdResponse getCartItems(@RequestBody String inputDataStr){
		Type type = new TypeToken<Map<String, String>>(){}.getType();
		Map<String, String> inputData = platformGson.fromJson(inputDataStr, type);
		
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside getCartItems() method---->", null, false);
		try {
			if (inputData == null) {
				return planDisplaySvcHelper.sendNull();
			}
			
			PdResponse pdResponse = new PdResponse();
			pdResponse.startResponse();		
			
			Long pdHouseholdId = Long.valueOf(inputData.get("pdHouseholdId").toString());
			String coverageStartDate = inputData.get("coverageStartDate").toString();
			
			List<IndividualPlan> individualPlanList = new ArrayList<IndividualPlan>();
			List<PdOrderItem> pdOrderItemList = pdOrderItemService.findByHouseholdId(pdHouseholdId);
			if(!pdOrderItemList.isEmpty()){
				boolean mininmizeData = true;
				for(PdOrderItem pdOrderItem : pdOrderItemList){
					mininmizeData = pdOrderItem.getInsuranceType().equals(InsuranceType.HEALTH)?false:true;
					IndividualPlan individualPlan = planDisplayUtil.getPlanInfo(pdOrderItem.getPlanId().toString(), coverageStartDate, mininmizeData);
					individualPlan.setPlanId(pdOrderItem.getPlanId().intValue());
					individualPlan.setPremiumBeforeCredit(pdOrderItem.getPremiumBeforeCredit());
					individualPlan.setPremiumAfterCredit(pdOrderItem.getPremiumAfterCredit());
					individualPlan.setPlanType(pdOrderItem.getInsuranceType().toString());
					individualPlan.setPremium(pdOrderItem.getPremiumBeforeCredit());
					individualPlan.setAptc(pdOrderItem.getMonthlySubsidy() != null ? pdOrderItem.getMonthlySubsidy() : 0f);
					individualPlan.setOrderItemId(pdOrderItem.getId().intValue());	                    
					individualPlanList.add(individualPlan);
				}
			}
			pdResponse.setIndividualPlanList(individualPlanList);			
			return planDisplaySvcHelper.sendSuccess(pdResponse);
		} catch (Exception e) {
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_FIND_CART_ITEMS,e);
		}		
	}
		
	private PdResponse getPlansByLeadIdAndYear(String leadId,String year)
	{
		PdResponse pdResponse = new PdResponse();
		if (leadId == null) {
			return planDisplaySvcHelper.sendNull();
		}
		
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "Finding plan id for leadId : " + leadId + " and for year : " + year, null, false);
		
		try 
		{
			PdHousehold latestHousehold = pdHouseholdService.findPreshoppingHousehold(leadId,year);
			List<IndividualPlan> individualPlanList = new ArrayList<IndividualPlan>();
			if (latestHousehold != null) {
				List<PdOrderItem> pdOrderItemList = pdOrderItemService.findByHouseholdId(latestHousehold.getId());
				if(pdOrderItemList != null && !pdOrderItemList.isEmpty()){
					for(PdOrderItem pdOrderItem : pdOrderItemList){
						IndividualPlan individualPlan = new IndividualPlan();
						individualPlan.setPlanId(pdOrderItem.getPlanId().intValue());
						individualPlan.setPremiumBeforeCredit(pdOrderItem.getPremiumBeforeCredit());
						individualPlan.setPremiumAfterCredit(pdOrderItem.getPremiumAfterCredit());
						individualPlan.setPlanType(pdOrderItem.getInsuranceType().toString());
						individualPlanList.add(individualPlan);
					}
				}
			}
			pdResponse.setIndividualPlanList(individualPlanList);
			return planDisplaySvcHelper.sendSuccess(pdResponse);
			
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occured while finding planId by leadId & year---->", e, false);
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_FIND_PDORDERITEM_BY_LEADID,e);
		}
	}
	
	/**
     * Updates ssapApplicationId in OrderItem taking ssapApplicationId as input.
     * @param request
     *              HttpServletRequest having map containing ssapApplicationId as parameters.
     * @return
     *              Object of <class>pdResponse</class>
     */
	
	@RequestMapping(value="/updateApplicationIdInOrderItem", method = RequestMethod.POST)
	public @ResponseBody PdResponse updateApplicationIdInOrderItem(@RequestBody String inputDataStr) 
	{	
		Type type = new TypeToken<Map<String, Long>>(){}.getType();
		Map<String, Long> inputData = platformGson.fromJson(inputDataStr, type);
		
		PdResponse pdResponse = new PdResponse();		
		try{
			Long itemId = inputData.get("pldOrderItemId");
			Long ssapApplicationId =  inputData.get("ssapApplicationId");		
			PdOrderItem pdOrderItem = pdOrderItemService.findById(itemId);
			pdOrderItem.setSsapApplicationId(ssapApplicationId);
			pdOrderItemService.save(pdOrderItem);
			planDisplaySvcHelper.sendSuccess(pdResponse);
		} catch(Exception e) {
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_EXECUTE_UPDATE_APPLICATIONID_IN_ORDERITEM,e);
		}
		return pdResponse;
	}
	
	@RequestMapping(value = "/findRatingArea", method=RequestMethod.POST)
	@ResponseBody
	public String findRatingArea(@RequestBody String inputDataStr){	
		Type type = new TypeToken<Map<String, String>>(){}.getType();
		Map<String, String> inputData = platformGson.fromJson(inputDataStr, type);
		
		return pdOrderItemService.findRatingArea(inputData);
	}
	
	
	@RequestMapping(value = "/sendAdminUpdate", method = RequestMethod.POST)
	@ResponseBody
	public PlanDisplayResponse sendAdminUpdate(@RequestBody PlanDisplayRequest planDisplayRequest) {

		PlanDisplayResponse planDisplayResponse = new PlanDisplayResponse();

		try {
			planDisplayResponse.startResponse();
			if (planDisplayRequest == null || planDisplayRequest.getInputData().size() == 0) {
				planDisplayResponse.setStatus("FAILED");
				planDisplayResponse.setErrMsg("Invalid input: inputs cannot be Null or empty");
				planDisplayResponse.endResponse();
				return planDisplayResponse;
			}

			Map<String, Object> inputData = planDisplayRequest.getInputData();
			// Integer householdId =
			// Integer.parseInt(inputData.get("householdId").toString());
			Long householdId = Long.parseLong(inputData.get("householdId").toString());
			Long enrollmentId = Long.parseLong(inputData.get("enrollmentId").toString());

			planDisplayResponse.setStatus("SUCCESS");

			Household adminUpdatehousehold = new Household();

			PdHousehold pdHousehold = pdHouseholdService.findById(householdId);
			Long giWsPayloadId = pdHousehold.getGiWsPayloadId();
			GIWSPayload giWSPayload = giWSPayloadService.findById(giWsPayloadId.intValue());
			IndividualInformationRequest ind19Request = null;
			try {
				String payload = giWSPayload.getRequestPayload();
				JAXBContext context = JAXBContext.newInstance(IndividualInformationRequest.class);
				StringReader stringReaderIND19 = new StringReader(payload);
				ind19Request = (IndividualInformationRequest) context.createUnmarshaller().unmarshal(stringReaderIND19);
			} catch (Exception e) {
				e.printStackTrace();
			}
			HouseHoldContact houseHoldContactObj = ind19Request.getHousehold().getHouseHoldContact();
			ResponsiblePerson responsiblePersonObj = ind19Request.getHousehold().getResponsiblePerson();

			com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest.Household.HouseHoldContact aUHouseHoldContactObj = new com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest.Household.HouseHoldContact();
			aUHouseHoldContactObj.setHouseHoldContactFederalTaxIdNumber(houseHoldContactObj.getHouseHoldContactFederalTaxIdNumber());
			aUHouseHoldContactObj.setHouseHoldContactFirstName(houseHoldContactObj.getHouseHoldContactFirstName());
			aUHouseHoldContactObj.setHouseHoldContactId(houseHoldContactObj.getHouseHoldContactId());
			aUHouseHoldContactObj.setHouseHoldContactLastName(houseHoldContactObj.getHouseHoldContactLastName());
			aUHouseHoldContactObj.setHouseHoldContactMiddleName(houseHoldContactObj.getHouseHoldContactMiddleName());
			aUHouseHoldContactObj.setHouseHoldContactPreferredEmail(houseHoldContactObj.getHouseHoldContactPreferredEmail());
			aUHouseHoldContactObj.setHouseHoldContactPreferredPhone(houseHoldContactObj.getHouseHoldContactPreferredPhone());
			aUHouseHoldContactObj.setHouseHoldContactPrimaryPhone(houseHoldContactObj.getHouseHoldContactPrimaryPhone());
			aUHouseHoldContactObj.setHouseHoldContactSecondaryPhone(houseHoldContactObj.getHouseHoldContactSecondaryPhone());
			aUHouseHoldContactObj.setHouseHoldContactSuffix(houseHoldContactObj.getHouseHoldContactSuffix());

			com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest.Household.ResponsiblePerson aUResponsiblePersonObj = new com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest.Household.ResponsiblePerson();
			aUResponsiblePersonObj.setResponsiblePersonFirstName(responsiblePersonObj.getResponsiblePersonFirstName());
			aUResponsiblePersonObj.setResponsiblePersonHomeAddress1(responsiblePersonObj.getResponsiblePersonHomeAddress1());
			aUResponsiblePersonObj.setResponsiblePersonHomeAddress2(responsiblePersonObj.getResponsiblePersonHomeAddress2());
			aUResponsiblePersonObj.setResponsiblePersonHomeCity(responsiblePersonObj.getResponsiblePersonHomeCity());
			aUResponsiblePersonObj.setResponsiblePersonHomeState(responsiblePersonObj.getResponsiblePersonHomeState());
			aUResponsiblePersonObj.setResponsiblePersonHomeZip(responsiblePersonObj.getResponsiblePersonHomeZip());
			aUResponsiblePersonObj.setResponsiblePersonId(responsiblePersonObj.getResponsiblePersonId());
			aUResponsiblePersonObj.setResponsiblePersonMiddleName(responsiblePersonObj.getResponsiblePersonMiddleName());
			aUResponsiblePersonObj.setResponsiblePersonPreferredEmail(responsiblePersonObj.getResponsiblePersonPreferredEmail());
			aUResponsiblePersonObj.setResponsiblePersonPreferredPhone(responsiblePersonObj.getResponsiblePersonPreferredPhone());
			aUResponsiblePersonObj.setResponsiblePersonPrimaryPhone(responsiblePersonObj.getResponsiblePersonPrimaryPhone());
			aUResponsiblePersonObj.setResponsiblePersonSecondaryPhone(responsiblePersonObj.getResponsiblePersonSecondaryPhone());
			aUResponsiblePersonObj.setResponsiblePersonSsn(responsiblePersonObj.getResponsiblePersonSsn());
			aUResponsiblePersonObj.setResponsiblePersonSuffix(responsiblePersonObj.getResponsiblePersonSuffix());
			aUResponsiblePersonObj.setResponsiblePersonType(responsiblePersonObj.getResponsiblePersonType());
			aUResponsiblePersonObj.setResponsiblePersonLastName(responsiblePersonObj.getResponsiblePersonLastName());
			adminUpdatehousehold.setHouseHoldContact(aUHouseHoldContactObj);
			adminUpdatehousehold.setResponsiblePerson(aUResponsiblePersonObj);

			Members members = new Members();
			for (Member personDataObj : ind19Request.getHousehold().getMembers().getMember()) {

				com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest.Household.Members.Member aUPersonDataObj = new com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest.Household.Members.Member();

				aUPersonDataObj.setCustodialParentFirstName(personDataObj.getCustodialParentFirstName());
				aUPersonDataObj.setCustodialParentHomeAddress1(personDataObj.getCustodialParentHomeAddress1());
				aUPersonDataObj.setCustodialParentHomeAddress2(personDataObj.getCustodialParentHomeAddress2());
				aUPersonDataObj.setCustodialParentHomeCity(personDataObj.getCustodialParentHomeCity());
				aUPersonDataObj.setCustodialParentHomeState(personDataObj.getCustodialParentHomeState());
				aUPersonDataObj.setCustodialParentHomeZip(personDataObj.getCustodialParentHomeZip());
				aUPersonDataObj.setCustodialParentId(personDataObj.getCustodialParentId());
				aUPersonDataObj.setCustodialParentLastName(personDataObj.getCustodialParentLastName());
				aUPersonDataObj.setCustodialParentMiddleName(personDataObj.getCustodialParentMiddleName());
				aUPersonDataObj.setCustodialParentPreferredEmail(personDataObj.getCustodialParentPreferredEmail());
				aUPersonDataObj.setCustodialParentPreferredPhone(personDataObj.getCustodialParentPreferredPhone());
				aUPersonDataObj.setCustodialParentPrimaryPhone(personDataObj.getCustodialParentPrimaryPhone());
				aUPersonDataObj.setCustodialParentSecondaryPhone(personDataObj.getCustodialParentSecondaryPhone());
				aUPersonDataObj.setCustodialParentSsn(personDataObj.getCustodialParentSsn());
				aUPersonDataObj.setCustodialParentSuffix(personDataObj.getCustodialParentSuffix());
				aUPersonDataObj.setDob(personDataObj.getDob());
				aUPersonDataObj.setFirstName(personDataObj.getFirstName());
				aUPersonDataObj.setGenderCode(personDataObj.getGenderCode());
				aUPersonDataObj.setLastName(personDataObj.getLastName());
				aUPersonDataObj.setHomeAddress1(personDataObj.getHomeAddress1());
				aUPersonDataObj.setHomeAddress2(personDataObj.getHomeAddress2());
				aUPersonDataObj.setHomeCity(personDataObj.getHomeCity());
				aUPersonDataObj.setHomeState(personDataObj.getHomeState());
				aUPersonDataObj.setHomeZip(personDataObj.getHomeZip());
				aUPersonDataObj.setMailingAddress1(personDataObj.getMailingAddress1());
				aUPersonDataObj.setMailingAddress2(personDataObj.getMailingAddress2());
				aUPersonDataObj.setMailingCity(personDataObj.getMailingCity());
				aUPersonDataObj.setMailingState(personDataObj.getMailingState());
				aUPersonDataObj.setMailingZip(personDataObj.getMailingZip());
				aUPersonDataObj.setMaintenanceReasonCode(personDataObj.getMaintenanceReasonCode());
				aUPersonDataObj.setMemberId(personDataObj.getMemberId());
				aUPersonDataObj.setMiddleName(personDataObj.getMiddleName());
				aUPersonDataObj.setPreferredEmail(personDataObj.getPreferredEmail());
				aUPersonDataObj.setPreferredPhone(personDataObj.getPreferredPhone());
				aUPersonDataObj.setSecondaryPhone(personDataObj.getSecondaryPhone());
				aUPersonDataObj.setSpokenLanguageCode(personDataObj.getSpokenLanguageCode());
				aUPersonDataObj.setSsn(personDataObj.getSsn());
				aUPersonDataObj.setSuffix(personDataObj.getSuffix());
				aUPersonDataObj.setPrimaryPhone(personDataObj.getPrimaryPhone());
				if (personDataObj.getTobacco() != null) {
					aUPersonDataObj.setTobacco(com.getinsured.hix.webservice.enrollment.adminupdateindividual.YesNoVal
							.fromValue(personDataObj.getTobacco().toString()));
				}
				aUPersonDataObj.setWrittenLanguageCode(personDataObj.getWrittenLanguageCode());

				for (Race personRace : personDataObj.getRace()) {
					com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest.Household.Members.Member.Race aURaceObj = new com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest.Household.Members.Member.Race();
					aURaceObj.setDescription(personRace.getDescription());
					aURaceObj.setRaceEthnicityCode(personRace.getRaceEthnicityCode());
					aUPersonDataObj.getRace().add(aURaceObj);
				}

				for (Relationship personRelationship : personDataObj.getRelationship()) {
					com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest.Household.Members.Member.Relationships aURelationshipsObj = new com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest.Household.Members.Member.Relationships();
					aURelationshipsObj.setRelationshipMemberId(personRelationship.getMemberId());
					aURelationshipsObj.setRelationshipToMember(personRelationship.getRelationshipCode());
					aUPersonDataObj.getRelationships().add(aURelationshipsObj);
				}

				members.getMember().add(aUPersonDataObj);
			}

			EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
			AdminUpdateIndividualRequest adminUpdateIndividualRequest = new AdminUpdateIndividualRequest();

			adminUpdatehousehold.setEnrollmentId(enrollmentId);
			adminUpdatehousehold.setHouseholdCaseId(ind19Request.getHousehold().getIndividual().getHouseholdCaseId());
			adminUpdatehousehold.setMaintenanceReasonCode("43");
			adminUpdatehousehold.setMembers(members);
			adminUpdateIndividualRequest.setHousehold(adminUpdatehousehold);
			enrollmentRequest.setAdminUpdateIndividualRequest(adminUpdateIndividualRequest);

			XStream xstream = GhixUtils.getXStreamStaxObject();
			Map<String, Object> outputData = new HashMap<String, Object>();
			outputData.put("enrollmentRequest", xstream.toXML(enrollmentRequest));
			planDisplayResponse.setOutputData(outputData);
		} catch (Exception e) {
			LOGGER.error("Error occured while sendAdminUpdate : ", e);
		}
		return planDisplayResponse;

	}
	
	
	@RequestMapping(value = "/getCrossWalkIssuerPlanId", method = RequestMethod.POST)
	@ResponseBody
	public PdResponse getCrossWalkIssuerPlanId(@RequestBody PdCrossWalkIssuerIdRequest crossWalkRequest) {

		PdResponse pdResponse = new PdResponse();
		try {
			pdResponse.startResponse();
			if (crossWalkRequest == null) {
				pdResponse.setStatus("FAILED");
				pdResponse.setErrMsg("Invalid input: inputs cannot be Null or empty");
				pdResponse.endResponse();
				return pdResponse;
			}
			
			String zipcode = crossWalkRequest.getZipcode();
			String countycode = crossWalkRequest.getCountyCode();
			if(crossWalkRequest.getPdSubscriberData() != null && crossWalkRequest.getPdSubscriberData().getHouseholdId() != null)
			{
				PdSubscriberData pdSubscriberData = crossWalkRequest.getPdSubscriberData();
				PdHousehold pdHousehold = pdHouseholdService.findById(pdSubscriberData.getHouseholdId());
				List<PdPerson> persons = pdPersonService.getPersonListByHouseholdId(pdSubscriberData.getHouseholdId());
				
				PdHouseholdDTO pdHouseholdDTO = new PdHouseholdDTO();
				List<PdPersonDTO> pdPersonDTOList = new ArrayList<PdPersonDTO>();
				try {
					createDTOBean.copyProperties(pdHouseholdDTO, pdHousehold);
					for(PdPerson pdPerson : persons){
						PdPersonDTO pdPersonDTO = new PdPersonDTO();
						createDTOBean.copyProperties(pdPersonDTO, pdPerson);
						pdPersonDTOList.add(pdPersonDTO);
					}
				} catch (IllegalAccessException e) {
					LOGGER.error("Error occured while getSubscriberData : ", e);
				}catch ( InvocationTargetException e) {
					LOGGER.error("Error occured while getSubscriberData : ", e);
				}
				if (InsuranceType.DENTAL.equals(pdSubscriberData.getInsuranceType()))
				{
					pdPersonDTOList = planDisplayUtil.getSeekingCovgPersonList(pdPersonDTOList);
				}
				Map<String,String> subscriberData = individualPlanService.getSubscriberData(pdPersonDTOList, pdHouseholdDTO, pdSubscriberData.getInitialSubscriberId(), pdSubscriberData.getSpecialEnrollmentFlowType(), pdSubscriberData.getInsuranceType());
				zipcode = subscriberData.get(GhixConstants.SUBSCRIBER_MEMBER_ZIP);
				countycode = subscriberData.get(GhixConstants.SUBSCRIBER_MEMBER_COUNTY);
			}
			
			String issuerPlanId = null;
			try {
				issuerPlanId = planDisplaySvcHelper.getCrossWalkIssuerPlanId(zipcode, countycode, crossWalkRequest.getCoverageDate(), crossWalkRequest.getIsEligibleForCatastrophic(), crossWalkRequest.getRenewalPlanId(), crossWalkRequest.getCsrValue());
				pdResponse.setStatus("SUCCESS");
			} catch (GIException e) {
				pdResponse.setErrCode(e.getErrorCode());
				pdResponse.setErrMsg(e.getErrorMsg());
			}
			pdResponse.setResponse(issuerPlanId);
			pdResponse.endResponse();
			}catch (Exception e) {
				LOGGER.error("Error occured while sendAdminUpdate : ", e);
			}
		return pdResponse;	
	}
	
	@RequestMapping(value = "/getProvidersSearchResults", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> getProvidersSearchResults(@RequestBody ProviderSearchRequest providerSearchRequest) {
		ProviderSearch providerSearch = providerFactory.getObject();
		return providerSearch.searchProvider(providerSearchRequest);
	}
	
	@RequestMapping(value = "/appendHiosPlanIdWithCSR", method = RequestMethod.POST)
	@ResponseBody
	public PdResponse appendHiosPlanIdWithCSR(@RequestBody String inputDataStr) {
		Type type = new TypeToken<Map<String, String>>(){}.getType();
		Map<String, String> inputData = platformGson.fromJson(inputDataStr, type);
		PdResponse pdResponse = new PdResponse();		
		try{
			Long householdId = Long.valueOf(inputData.get("householdId"));
			String costSharing =  inputData.get("costSharing");
			String planLevel =  inputData.get("planLevel");
			String initialHealthCmsPlanId = inputData.get("initialHealthCmsPlanId");		
			String isEligibleCatastrophic = inputData.get("isEligibleCatastrophic");
			boolean catastrophic= false;
			if("Y".equals(isEligibleCatastrophic))
			{
				catastrophic = true;
			}
			
			String healthCmsPlanId = planDisplaySvcHelper.appendHiosPlanIdWithCSR(initialHealthCmsPlanId, costSharing, householdId, planLevel,catastrophic);
			pdResponse.setResponse(healthCmsPlanId);
		} catch(Exception e) {
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_EXECUTE_APPEND_HIOSPLANID_WITH_CSR,e);
		}
		return pdResponse;
	}
	
	@RequestMapping(value = "/getPlanScoreForPreferences", method = RequestMethod.POST)
	@ResponseBody
	public List<IndividualPlan> getPlanScoreForPreferences(@RequestBody String inputDataStr, HttpServletResponse response) {
		Type type = new TypeToken<Map<String, String>>(){}.getType();
		Map<String, String> inputData = platformGson.fromJson(inputDataStr, type);
		try{
			String householdId = inputData.get("householdId");
			String planPremiumListStr = inputData.get("planPremiumListStr");
			int noOfMembers = Integer.valueOf(inputData.get("personCount"));
			String zipCode = inputData.get("zipCode");
			List<PlanPremiumDTO> planPremiumList = platformGson.fromJson(planPremiumListStr,new TypeToken<List<PlanPremiumDTO>>() {}.getType());
			
			if(StringUtils.isNotBlank(householdId)){
				PdHousehold pdHousehold = pdHouseholdService.findById(Long.valueOf(householdId));
				if(pdHousehold != null){
					String coverageStartDate = DateUtil.dateToString(pdHousehold.getCoverageStartDate(), "yyyy-MM-dd");
					if(pdHousehold.getEligLeadId() != null){
					if(planPremiumList != null && planPremiumList.size() > 0) {
							PdPreferences pdPreferences = pdPreferencesService.findPreferencesByEligLeadId(pdHousehold.getEligLeadId());
						if(pdPreferences != null && StringUtils.isNotBlank(pdPreferences.getPreferences())){
							PdPreferencesDTO pdPreferencesDTO = planDisplayUtil.getPdPreferencesDTO(pdPreferences);
							if(pdPreferencesDTO.getMedicalUse() != null && pdPreferencesDTO.getPrescriptionUse() != null){
									String planScoreResponse = planDisplaySvcHelper.calculatePlanScoreForPreferences(pdPreferencesDTO, planPremiumList, noOfMembers, zipCode, coverageStartDate, null);
									List<IndividualPlan> individualPlans = planDisplaySvcHelper.getPlansFromPlanScoreResponseList(planScoreResponse);
									return individualPlans;
								}								
							}							
							}
						}
					}
			}			
		} catch(Exception e) {
			LOGGER.error("Error occured while getPlanScorForPreferences : ", e);
		}
		return Collections.emptyList();
	}
	
		
	@RequestMapping(value = "/restoreCSRValue/{id}", method = RequestMethod.POST)
	@ResponseBody
	public PdResponse restoreCSRValue(@PathVariable("id") String householdId) {
		PdResponse pdResponse = new PdResponse();
		PdHousehold pdHousehold = null;
		Integer pdHouseholdId = null;
		try{
			pdHouseholdId  = Integer.parseInt(householdId);
			pdHousehold = pdHouseholdService.findById(pdHouseholdId.longValue());
			if(pdHousehold == null){ 
				LOGGER.error("Invalid HouseholdId"+householdId);
				pdResponse.setErrMsg("Invalid HouseholdId"+householdId);
				pdResponse.setStatus("FAILURE");
				return pdResponse;
			}
		}
		catch (NumberFormatException e) {
				LOGGER.error("Invalid HouseholdId"+householdId,e);
				pdResponse.setErrMsg("Invalid HouseholdId"+householdId);
				pdResponse.setStatus("FAILURE");
		}
		
		GIWSPayload giWSPayload = giWSPayloadService.findById(pdHousehold.getGiWsPayloadId().intValue());
		IndividualInformationRequest ind19Request = null;
		try {
			String payload = giWSPayload.getRequestPayload();
			JAXBContext context = JAXBContext.newInstance(IndividualInformationRequest.class);
			StringReader stringReaderIND19 = new StringReader(payload);
			ind19Request = (IndividualInformationRequest) context.createUnmarshaller().unmarshal(stringReaderIND19);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(ind19Request != null)
		{
			CostSharing costSharingIND19  = pdHousehold.getCostSharing();
			if(ind19Request.getHousehold().getIndividual().getCsr() != null )
			{
				costSharingIND19 = CostSharing.valueOf(ind19Request.getHousehold().getIndividual().getCsr());
			}
			if(pdHousehold.getCostSharing() != null && !pdHousehold.getCostSharing().equals(costSharingIND19))
			{
				pdHousehold.setCostSharing(costSharingIND19);
				pdHouseholdService.save(pdHousehold);
			}
			PdHouseholdDTO pdHouseholdDTO = new PdHouseholdDTO();
			pdHouseholdDTO.setCostSharing(costSharingIND19);
			pdResponse.setPdHouseholdDTO(pdHouseholdDTO);
			pdResponse.setStatus("SUCCESS");
		}
		return pdResponse;
	}
	
	@RequestMapping(value = "/calculateSliderAptc", method = RequestMethod.POST)
	public @ResponseBody APTCCalculatorResponse calculateSliderAptc(@RequestBody APTCCalculatorRequest aptcCalculatorRequest){
		APTCCalculatorResponse aptcCalculatorResponse = planDisplayUtil.calculateSliderAptc(aptcCalculatorRequest);
		return aptcCalculatorResponse;
	}
	
	@RequestMapping(value = "/findPdHouseholdByLeadIdAndYear", method = RequestMethod.GET)
	public @ResponseBody PdResponse findPdHouseholdByLeadIdAndYear(HttpServletRequest request){
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside findPdHouseholdByLeadIdAndYear() method---->", null, false);
		try {
			PdResponse pdResponse = new PdResponse();
			pdResponse.startResponse();
			String leadId = (String) request.getParameter("leadId");
			String year = (String) request.getParameter("year");
			if (leadId == null) {
				pdResponse.setErrMsg("Invalid input: leadId cannot be Null or empty");
				pdResponse.setStatus("FAILURE");
				return pdResponse;
			}
			
			PdHousehold pdHousehold = pdHouseholdService.findPreshoppingHousehold(leadId , year);
			if( pdHousehold == null){
				pdResponse.setErrMsg("PdHousehold not found for given leadId and year");
				pdResponse.setStatus("FAILURE");
				return pdResponse;
			}
			PdHouseholdDTO pdHouseholdDTO = new PdHouseholdDTO();
			createDTOBean.copyProperties(pdHouseholdDTO, pdHousehold);
			pdResponse.setPdHouseholdDTO(pdHouseholdDTO);			
			pdResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			pdResponse.endResponse();
			return pdResponse;

		} catch (Exception e) {
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_FIND_PDHOUSHOLD,e);
		}
	}
	
	@RequestMapping(value = "/getSubscriberZip", method = RequestMethod.POST)
	public @ResponseBody PdResponse getSubscriberZip(@RequestBody PdQuoteRequest pdQuoteRequest) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside getSubscriberZip() method---->", null, false);
		try{
			if (pdQuoteRequest == null) {
				return planDisplaySvcHelper.sendNull();
			}
			
			PdResponse pdResponse = new PdResponse();
			pdResponse.startResponse();	
		
			PdHouseholdDTO pdHouseholdDTO = pdQuoteRequest.getPdHouseholdDTO();
			List<PdPersonDTO> pdPersonDTOList = pdQuoteRequest.getPdPersonDTOList();
			if (InsuranceType.DENTAL.equals(pdQuoteRequest.getInsuranceType())){
				pdPersonDTOList = planDisplayUtil.getSeekingCovgPersonList(pdPersonDTOList);
			}
			
			Map<String, String> subscriberData = individualPlanService.getSubscriberData(pdPersonDTOList, pdHouseholdDTO, pdQuoteRequest.getInitialSubscriberId(), pdQuoteRequest.getSpecialEnrollmentFlowType(), pdQuoteRequest.getInsuranceType());
			String subsriberZipCode = subscriberData.get(GhixConstants.SUBSCRIBER_MEMBER_ZIP);
			pdResponse.setResponse(subsriberZipCode);
			return planDisplaySvcHelper.sendSuccess(pdResponse);
		} catch (Exception e) {
			return planDisplaySvcHelper.sendException(PlanDisplayConstants.ErrorCodes.FAILED_TO_FIND_SUBSCRIBER_ZIP,e);
		}		
	}
	
}
