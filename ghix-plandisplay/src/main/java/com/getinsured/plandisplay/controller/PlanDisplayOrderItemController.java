package com.getinsured.plandisplay.controller;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.w3c.dom.Document;

import com.getinsured.hix.dto.consumer.ssap.SsapApplicationDTO;
import com.getinsured.hix.dto.plandisplay.PdOrderItemDTO;
import com.getinsured.hix.dto.plandisplay.PdPersonDTO;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.model.PdHousehold;
import com.getinsured.hix.model.PdOrderItem;
import com.getinsured.hix.model.PdOrderItemPerson;
import com.getinsured.hix.model.PdPerson;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.plandisplay.FFMPerson;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;
import com.getinsured.hix.model.plandisplay.PldMemberData;
import com.getinsured.hix.model.plandisplay.PldOrderResponse;
import com.getinsured.hix.model.plandisplay.PldPlanData;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.ConsumerServiceEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.ApplicantEligibilityResponseType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.PersonType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.ResponseType;
import com.getinsured.plandisplay.service.PdHouseholdService;
import com.getinsured.plandisplay.service.PdOrderItemPersonService;
import com.getinsured.plandisplay.service.PdOrderItemService;
import com.getinsured.plandisplay.util.CreateDTOBean;
import com.getinsured.plandisplay.util.PlanDisplayUtil;
import com.getinsured.timeshift.TimeShifterUtil;
import com.google.gson.Gson;

@Controller
@RequestMapping(value="/plandisplay")
public class PlanDisplayOrderItemController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlanDisplayOrderItemController.class);
	@Autowired private GIWSPayloadService giWSPayloadService;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private PdHouseholdService pdHouseholdService;
	@Autowired private PdOrderItemService pdOrderItemService;
	@Autowired private PdOrderItemPersonService pdOrderItemPersonService;
	@Autowired private PlanDisplayUtil planDisplayUtil;
	@Autowired private CreateDTOBean createDTOBean;	
	@Autowired private Gson platformGson;
	
	@RequestMapping(value="/findBySsapApplicationId/{ssapApplicationId}", method = RequestMethod.GET)
	public @ResponseBody String findInfoBySsapApplicationId(@PathVariable Long ssapApplicationId)
	{		
		String response = "";
		PldOrderResponse pldOrderResponse = new PldOrderResponse();
		List<PdOrderItem> pdOrderItemList = pdOrderItemService.findByApplicationId(ssapApplicationId);
		if(pdOrderItemList.isEmpty())
		{
			pldOrderResponse.setStatus("FAILED");
			pldOrderResponse.setErrMsg("No cart items found for given applicaiton Id" + ssapApplicationId);
			response = platformGson.toJson(pldOrderResponse);
			return response;
		}

		pldOrderResponse = createPldOrderResponseForExchangeType(pdOrderItemList);
		response = platformGson.toJson(pldOrderResponse);

		return response;
	}
	
	private PldOrderResponse createPldOrderResponseForExchangeType(List<PdOrderItem> pdOrderItemList) {
		long startTime = TimeShifterUtil.currentTimeMillis();
		
		PldOrderResponse pldOrderResponse = new PldOrderResponse();
		List<PldPlanData> planDataList = new ArrayList<PldPlanData>();	
		List<PldMemberData> memberInfoList = new ArrayList<PldMemberData>();
		
		for(PdOrderItem pdOrderItem : pdOrderItemList){
			PldPlanData pldPlanData = new PldPlanData();
			
			Map<String,String> plan = new HashMap<String, String>();
			plan.put("orderItemId", ""+pdOrderItem.getId());
			plan.put("planId", ""+pdOrderItem.getPlanId());
			plan.put("planType", pdOrderItem.getInsuranceType().toString());
			plan.put("coverageStartDate", ""+DateUtil.dateToString(pdOrderItem.getPdHousehold().getCoverageStartDate(), "yyyy-MM-dd hh:mm:ss.S"));
			plan.put("netPremiumAmt", ""+ new BigDecimal(Float.toString(pdOrderItem.getPremiumAfterCredit())).setScale(2,BigDecimal.ROUND_HALF_UP));
			plan.put("grossPremiumAmt", ""+ new BigDecimal(Float.toString(pdOrderItem.getPremiumBeforeCredit())).setScale(2,BigDecimal.ROUND_HALF_UP));
			plan.put("marketType", "FI");
			if(pdOrderItem.getMonthlySubsidy() != null){
				plan.put("aptc", ""+new BigDecimal(Float.toString(pdOrderItem.getMonthlySubsidy())).setScale(2,BigDecimal.ROUND_HALF_UP));
			}
			plan.put("csr", pdOrderItem.getPdHousehold().getCostSharing() != null ? pdOrderItem.getPdHousehold().getCostSharing().toString() : "");
			
			pldPlanData.setPlan(plan);	
			
			List<PdOrderItemPerson> pdOrderItemPersonList = pdOrderItemPersonService.getByOrderItemId(pdOrderItem.getId());
        	for(PdOrderItemPerson pdOrderItemPerson : pdOrderItemPersonList){
       		    PdPerson pdPerson = pdOrderItemPerson.getPdPerson();
       		    Map<String,String> memberInfo = new HashMap<String, String>();
		  		memberInfo.put("memberId", ""+pdPerson.getExternalId());
		  		memberInfo.put("dob", DateUtil.dateToString(pdPerson.getBirthDay(), GhixConstants.REQUIRED_DATE_FORMAT));
		        memberInfo.put("tobacco", PlanDisplayEnum.YorN.Y.equals(pdPerson.getTobacco()) ? "YES" : "NO");
		        memberInfo.put("homeZip", pdPerson.getZipCode());	        		
		        memberInfo.put("countyCode", pdPerson.getCountyCode());
		        memberInfo.put("subscriberFlag", PlanDisplayEnum.YorN.Y.equals(pdOrderItemPerson.getSubscriberFlag()) ? "YES" : "NO");
		        
		        PlanDisplayEnum.Relationship relationship = pdPerson.getRelationship();
		        if(PlanDisplayEnum.Relationship.SELF.equals(relationship)){
		        	memberInfo.put("houseHoldContactRelationship", "18");
		        }else if(PlanDisplayEnum.Relationship.SPOUSE.equals(relationship)){
		        	memberInfo.put("houseHoldContactRelationship", "01");
		        }else if(PlanDisplayEnum.Relationship.CHILD.equals(relationship)){
		        	memberInfo.put("houseHoldContactRelationship", "19");
		        }
		        
		        if(pdPerson.getGender() != null){
		        	memberInfo.put("gender", pdPerson.getGender().toString());
		        }		        		
		        
		        PldMemberData pldMemberData = new PldMemberData();
		        pldMemberData.setMemberInfo(memberInfo);
		        memberInfoList.add(pldMemberData);
        	}
        	pldPlanData.setMemberInfoList(memberInfoList);
        	planDataList.add(pldPlanData);        		
    	}
		pldOrderResponse.setPlanDataList(planDataList);	
		
		long endTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Total time taken by executing createPldOrderResponseForExchangeType---->"+(endTime - startTime), null, false);
		return pldOrderResponse;
	}
	
	@RequestMapping(value="/findbyorderid/{cartId}", method = RequestMethod.GET)
	@ResponseBody public String findbyorderid(@PathVariable final String cartId) throws GIRuntimeException
	{	
		PldOrderResponse pldOrderResponse = new PldOrderResponse();
		// 1. Call Plan Display service to get plan selection data
		Long pdHouseholdId = Long.parseLong(cartId);
		PdHousehold pdHousehold = pdHouseholdService.findById(pdHouseholdId);
		List<PdOrderItem> pdOrderItemDTOList = findbyhouseholdidpd(pdHouseholdId);
		if(pdOrderItemDTOList== null || pdOrderItemDTOList.size() != 1){
			pldOrderResponse.setStatus("FAILED");
			pldOrderResponse.setErrMsg("Cart items not present or multiple cart items are present");
		}else{
			try {
				PdOrderItemDTO pdOrderItemDTO = new PdOrderItemDTO();
				createDTOBean.copyProperties(pdOrderItemDTO, pdOrderItemDTOList.get(0));
				pdOrderItemDTO.setPdHousehold(pdHousehold);
				// 2. Get FFMResponse from GIWSPayload
				Long giWsPayloadId = pdHousehold.getGiWsPayloadId();
				GIWSPayload giWSPayload = giWSPayloadService.findById(giWsPayloadId.intValue());
				ApplicantEligibilityResponseType applicantEligibilityResponse = null;
			
	        	JAXBContext jaxbContext = JAXBContext.newInstance(
						com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.exchange._1.ObjectFactory.class,
						com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.structures._2.ObjectFactory.class,
						com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_core.ObjectFactory.class,
						com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_pm.ObjectFactory.class);
	        	
	        	Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	        	DocumentBuilderFactory dbf = GhixUtils.getDocumentBuilderFactoryInstance();
				dbf.setExpandEntityReferences(false);
				dbf.setNamespaceAware(true);
				DocumentBuilder db = dbf.newDocumentBuilder();
				Document document = db.parse(new ByteArrayInputStream(giWSPayload.getResponsePayload().getBytes("UTF-8")));
		        @SuppressWarnings("unchecked")
				JAXBElement<ResponseType> jaxbApplicantEligibilityResponse = (JAXBElement<ResponseType>)jaxbUnmarshaller.unmarshal(document);
		        applicantEligibilityResponse = ((ResponseType)jaxbApplicantEligibilityResponse.getValue()).getApplicantEligibilityResponse();
	       
	
				// 3. Merge plan selection data and ApplicantEligibilityResponseType
				SsapApplicationDTO	ssapApplicationDTO = null;
				long ssapApplicationId = pdOrderItemDTOList.get(0).getSsapApplicationId();				
				ssapApplicationDTO = ghixRestTemplate.postForObject(ConsumerServiceEndPoints.FIND_SSAP_APPLICATION_BY_ID, ssapApplicationId,SsapApplicationDTO.class);
				if(ssapApplicationDTO!=null && ssapApplicationDTO.getGiHouseholdId()!=null){
					pldOrderResponse.setGiHouseholdId(ssapApplicationDTO.getGiHouseholdId());
				}	
		
				pldOrderResponse = createPldOrderResponseForFFM(applicantEligibilityResponse, pldOrderResponse, pdOrderItemDTO);
			} catch (GIException e) {
				pldOrderResponse.setStatus("FAILED");
				pldOrderResponse.setErrMsg(e.getErrorMsg());
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----GIException occured while processing pldOrderResponse ffm---->", e, false);
			} catch (Exception e) {
		        pldOrderResponse.setStatus("FAILED");
				pldOrderResponse.setErrMsg("error occurs while parsing FFM response");
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----error occurs while parsing FFM response---->"+e.getMessage(), e, false);
			 } 
		}

		String response = platformGson.toJson(pldOrderResponse);
		return response;
	}
	
	private PldOrderResponse createPldOrderResponseForFFM(ApplicantEligibilityResponseType applicantEligibilityResponse, PldOrderResponse pldOrderResponse, PdOrderItemDTO pdOrderItemDTO) throws GIException {
		
		long startTime = TimeShifterUtil.currentTimeMillis();
		
		PdHousehold pdHouseHold = pdOrderItemDTO.getPdHousehold();
		List<PldPlanData> planDataList = new ArrayList<PldPlanData>();				

		pldOrderResponse.setHouseholdId(String.valueOf(pdHouseHold.getId()));
		pldOrderResponse.setShoppingId(String.valueOf(pdHouseHold.getShoppingId()));
		
		Map<String,String> memberApplicantMap= new HashMap<String,String> ();
		List<JAXBElement<? extends com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType>> applicantList=(ArrayList<JAXBElement<? extends com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType>>)applicantEligibilityResponse.getInsuranceApplication().getInsuranceApplicant();
		for(JAXBElement<? extends com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType> applicant : applicantList){
			InsuranceApplicantType value=(InsuranceApplicantType) applicant.getValue();
			memberApplicantMap.put(((PersonType)value.getRoleOfPersonReference().getRef() ).getId(), value.getExchangeAssignedInsuranceApplicantIdentification().getIdentificationID().getValue());
		}
		
		Map<String,String> houseHoldContact = new HashMap<String, String>();
		com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.PersonType ssfPrimaryContactPerson= (com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.PersonType)applicantEligibilityResponse.getSSFPrimaryContact().getRoleOfPersonReference().getRef();
		houseHoldContact.put("houseHoldContactFirstName", ssfPrimaryContactPerson.getPersonName().getPersonGivenName()!=null?ssfPrimaryContactPerson.getPersonName().getPersonGivenName().getValue():"");
		houseHoldContact.put("houseHoldContactMiddleName",ssfPrimaryContactPerson.getPersonName().getPersonMiddleName()!=null? ssfPrimaryContactPerson.getPersonName().getPersonMiddleName().getValue():""); // Optional
		houseHoldContact.put("houseHoldContactLastName", ssfPrimaryContactPerson.getPersonName().getPersonSurName()!=null?ssfPrimaryContactPerson.getPersonName().getPersonSurName().getValue():"");
		houseHoldContact.put("houseHoldContactSuffix", ssfPrimaryContactPerson.getPersonName().getPersonNameSuffixText()!=null?ssfPrimaryContactPerson.getPersonName().getPersonNameSuffixText().getValue():""); // Optional
		houseHoldContact.put("genderCode",ssfPrimaryContactPerson.getPersonSexCode().getValue().value());
		houseHoldContact.put("personId",ssfPrimaryContactPerson.getId());
		houseHoldContact.put("houseHoldContactId",memberApplicantMap.get(ssfPrimaryContactPerson.getId()));
		pldOrderResponse.setHouseHoldContact(houseHoldContact);

		//FFE Assigned Consumer ID a.k.a HouseholdCaseId
		pldOrderResponse.setHouseholdCaseId(pdHouseHold.getExternalId());
		pldOrderResponse.setEnrollmentType('I');
		pldOrderResponse.setSubsidized(true);
		
		PldPlanData pldPlanData = new PldPlanData();
		
		Plan planObj = pdOrderItemService.getPlanInfoByPlanId(pdOrderItemDTO.getPlanId());
		Map<String,String> plan = new HashMap<String, String>();
		plan.put("orderItemId", ""+pdOrderItemDTO.getId());
		plan.put("planId", ""+pdOrderItemDTO.getPlanId());
		plan.put("planType", pdOrderItemDTO.getInsuranceType().toString());
		plan.put("coverageStartDate", ""+DateUtil.dateToString(pdOrderItemDTO.getPdHousehold().getCoverageStartDate(), "yyyy-MM-dd hh:mm:ss.S"));
		BigDecimal premiumBeforeCredit = new BigDecimal(Float.toString(pdOrderItemDTO.getPremiumBeforeCredit())).setScale(2,BigDecimal.ROUND_HALF_UP);
		plan.put("grossPremiumAmt", ""+ premiumBeforeCredit);	
		BigDecimal premiumAfterCredit = new BigDecimal(Float.toString(pdOrderItemDTO.getPremiumAfterCredit())).setScale(2,BigDecimal.ROUND_HALF_UP);
		plan.put("netPremiumAmt", ""+ premiumAfterCredit);
		BigDecimal monthlySubsidy = new BigDecimal(Float.toString(pdOrderItemDTO.getMonthlySubsidy())).setScale(2,BigDecimal.ROUND_HALF_UP);
		plan.put("aptc", ""+ monthlySubsidy);						
		BigDecimal grossPremium= premiumAfterCredit.add(monthlySubsidy).setScale(2,BigDecimal.ROUND_HALF_UP);
		if(premiumBeforeCredit.compareTo(grossPremium) != 0){			
			String values = "premiumBeforeCredit : "+premiumBeforeCredit+", grossPremium : "+ grossPremium+", aptc : "+monthlySubsidy+", orderItmId : "+pdOrderItemDTO.getId();
			throw new GIException("premiumBeforeCredit.compareTo(grossPremium) = '"+premiumBeforeCredit.compareTo(grossPremium) +"'. Details : "+ values);	
		}
		plan.put("marketType", "FI");
		plan.put("csr", planObj.getPlanHealth().getCostSharing() != null ? planObj.getPlanHealth().getCostSharing().toString() : "");
		
		pldPlanData.setPlan(plan);	
		
		//Added for FFM integration
		pldPlanData.getPlan().put("stateExchangeCode",pdOrderItemDTO.getPdHousehold().getStateExchangeCode());
				
		//APTC %  is percentage of Max-APTC(household) to Applied-APTC(group)
		if(pdOrderItemDTO.getMonthlySubsidy()!=null && pdHouseHold.getMaxSubsidy() != null){
			//BigDecimal aptcPercentage = BigDecimal.valueOf(pdOrderItemDTO.getMonthlySubsidy().longValue()*100).divide(BigDecimal.valueOf(pdHouseHold.getMaxSubsidy().longValue())).setScale(2,BigDecimal.ROUND_HALF_UP);
			//pldPlanData.getPlan().put("aptcPercentage",""+aptcPercentage);
			if(!pdHouseHold.getMaxSubsidy().equals(0f)){
				BigDecimal aptcPercentage = new BigDecimal(Float.toString(pdOrderItemDTO.getMonthlySubsidy())).multiply(new BigDecimal(100));
				pldPlanData.getPlan().put("aptcPercentage",""+ aptcPercentage.divide(new BigDecimal(Float.toString(pdHouseHold.getMaxSubsidy())), 2, BigDecimal.ROUND_HALF_UP).setScale(2,BigDecimal.ROUND_HALF_UP));
			}else{
				pldPlanData.getPlan().put("aptcPercentage","0");
			}
		} else {
			pldPlanData.getPlan().put("aptcPercentage","0");
		}
		
		List<PldMemberData> memberInfoList = new ArrayList<PldMemberData>();
		List<PdOrderItemPerson> pdOrderItemPersonList = pdOrderItemPersonService.getByOrderItemId(pdOrderItemDTO.getId());
    	for(PdOrderItemPerson pdOrderItemPerson : pdOrderItemPersonList){
    		PdPerson pdPerson = pdOrderItemPerson.getPdPerson();
    		for(JAXBElement<? extends com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType> applicant : applicantList){
    			InsuranceApplicantType insuranceApplicant=(InsuranceApplicantType) applicant.getValue();
    			Map<String, Object> personData = planDisplayUtil.getPersonDataFromFFM(insuranceApplicant, applicantEligibilityResponse);
    			String memberId = (String) personData.get("memberId");
    			if(memberId.equalsIgnoreCase(pdPerson.getExternalId().toString())){
    				Map<String,String> memberInfo = createMemberInfoMapFromFFMPersonData(personData, pdPerson);
    				
    				memberInfo.put("subscriberFlag", YorN.Y.equals(pdOrderItemPerson.getSubscriberFlag()) ? "YES" : "NO");
    				memberInfo.put("individualPremium",pdOrderItemPerson.getPremium().toString());
            		memberInfo.put("ratingArea",""+pdOrderItemPerson.getRegion());
            		
    				//mapPersonIdsToExchangeAssignedIds(memberInfo,memberApplicantMap);
	        		  		
	        		PldMemberData pldMemberData = new PldMemberData();
	        		pldMemberData.setMemberInfo(memberInfo);
	        		List<Map<String, String>> personRelationshipList = (List <Map<String, String>>)personData.get("relationInfoList");
	        		for(Map<String, String> personRelationship : personRelationshipList){
	        			personRelationship.put("memberId",personRelationship.get("exchangeAssignedApplicantId")); 
	        			personRelationship.remove("exchangeAssignedApplicantId");
	        		}
	        		pldMemberData.setRelationshipInfo(personRelationshipList);
	        		memberInfoList.add(pldMemberData);
    			}
    		}
    		
    	}

        pldPlanData.setMemberInfoList(memberInfoList);
        planDataList.add(pldPlanData);
		
		pldOrderResponse.setPlanDataList(planDataList);
				
		List<Map<String, String>> nonEligiblePersonNameList = new ArrayList<Map<String, String>>();
		List<PdPersonDTO> pdPersonDTOList = planDisplayUtil.getNonEligibleMemberList(applicantEligibilityResponse); 
		for(PdPersonDTO pdPersonDTO : pdPersonDTOList){
			Map<String, String> nonEligiblePersonName = new HashMap<String, String>();
			nonEligiblePersonName.put("firstName", pdPersonDTO.getFirstName());
			nonEligiblePersonName.put("lastName", pdPersonDTO.getLastName());
			nonEligiblePersonNameList.add(nonEligiblePersonName);
		}
		pldOrderResponse.setNonEligiblePersonNameList(nonEligiblePersonNameList);
		
		long endTime = TimeShifterUtil.currentTimeMillis();
		
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Total time taken by executing createPldOrderResponseForFFM---->"+(endTime - startTime), null, false);

		return pldOrderResponse;

	}
	
	private Map<String, String> createMemberInfoMapFromFFMPersonData(Map personDataMap, PdPerson pdPerson) {
		Map<String, String> memberInfo = new HashMap<String, String>();
		memberInfo.put("memberId", (String)personDataMap.get("exchangeAssignedInsuranceApplicantIdentificationId"));        		
		memberInfo.put("firstName", (String)personDataMap.get("firstName"));
		memberInfo.put("middleName", (String)personDataMap.get("middleName")); // Optional
		memberInfo.put("lastName", (String)personDataMap.get("lastName"));
		memberInfo.put("suffix", (String)personDataMap.get("suffix")); // Optional
		memberInfo.put("dob", (String)personDataMap.get("dob"));        		
		memberInfo.put("ssn", (String)personDataMap.get("ssn")); // Optional        		
		memberInfo.put("genderCode", (String)personDataMap.get("sexCode"));
		memberInfo.put("insuranceApplicantId",(String)personDataMap.get("insuranceApplicantId"));
		memberInfo.put("personId",(String)personDataMap.get("personId"));
		memberInfo.put("tobacco", YorN.Y.equals(pdPerson.getTobacco()) ? "Y" : "N");
		
		if(null!=personDataMap.get("personContactInfoMap")){
			FFMPerson ffmPerson = (FFMPerson)personDataMap.get("personContactInfoMap");
			if(null!=ffmPerson.getZipCode()){	
				memberInfo.put("homeZip", ffmPerson.getZipCode());    				
			}
			if(null!=ffmPerson.getStreetName()){
				memberInfo.put("homeAddress1", ffmPerson.getStreetName());    				
			}
			if(null!=ffmPerson.getCityName())
			{
				memberInfo.put("homeCity", ffmPerson.getCityName());
	    		
			}
			if(null!=ffmPerson.getStateCode()){
				memberInfo.put("homeState", ffmPerson.getStateCode());    				
			}     			
			if(null!=ffmPerson.getEmail()){
				memberInfo.put("preferredEmail", ffmPerson.getEmail());  				
			} 
		}
		if(null!=personDataMap.get("personMailingAddress")){
			FFMPerson ffmPerson = (FFMPerson)personDataMap.get("personMailingAddress");
			if(null!=ffmPerson.getZipCode()){	
				memberInfo.put("mailingZip", ffmPerson.getZipCode());    				
			}
			if(null!=ffmPerson.getStreetName()){
				memberInfo.put("mailingAddress1", ffmPerson.getStreetName());    				
			}
			if(null!=ffmPerson.getCityName())
			{
				memberInfo.put("mailingCity", ffmPerson.getCityName());
	    		
			}
			if(null!=ffmPerson.getStateCode()){
				memberInfo.put("mailingState", ffmPerson.getStateCode());    				
			}     			
			if(null!=ffmPerson.getEmail()){
				memberInfo.put("preferredEmail", ffmPerson.getEmail());  				
			} 
		}
		if(personDataMap.get("telephoneNumber")!=null){
			String phoneNumber = (String)personDataMap.get("telephoneNumber");
			phoneNumber = phoneNumber.replaceAll("[()\\s-]+", "");
			memberInfo.put("primaryPhone", phoneNumber);
		}

		return memberInfo;
	}

	private List<PdOrderItem> findbyhouseholdidpd(Long pdHouseholdId) {
		List<PdOrderItem> pdOrderItems = pdOrderItemService.findByHouseholdId(pdHouseholdId);
		List<PdOrderItem> healthPdOrderItemList = new ArrayList<PdOrderItem>();
		for(PdOrderItem pdOrderItem : pdOrderItems){
			if(InsuranceType.HEALTH.equals(pdOrderItem.getInsuranceType())){
				healthPdOrderItemList.add(pdOrderItem);
			}
		}
		
		return healthPdOrderItemList;
	}
}
