package com.getinsured.plandisplay.controller;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.hix.dto.plandisplay.PdHouseholdDTO;
import com.getinsured.hix.dto.plandisplay.PdPersonDTO;
import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;
import com.getinsured.hix.dto.plandisplay.PdSession;
import com.getinsured.hix.dto.shop.EmployerDTO;
import com.getinsured.hix.dto.shop.ShopResponse;
import com.getinsured.hix.dto.shop.mlec.MLECRestRequest;
import com.getinsured.hix.dto.shop.mlec.MLECRestResponse;
import com.getinsured.hix.dto.shop.mlec.Member;
import com.getinsured.hix.dto.shop.mlec.Member.Relationship;
import com.getinsured.hix.model.EmployerLocation;
import com.getinsured.hix.model.IndividualPlan;
import com.getinsured.hix.model.PldOrder;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.plandisplay.PdQuoteRequest;
import com.getinsured.hix.model.plandisplay.PdResponse;
import com.getinsured.hix.model.plandisplay.PdSaveCartRequest;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentFlowType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ExchangeType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;
import com.getinsured.hix.model.plandisplay.PlanDisplayResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.PlandisplayEndpoints;
import com.getinsured.hix.platform.util.GhixEndPoints.ShopEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.plandisplay.service.IndividualPlanService;
import com.getinsured.plandisplay.service.PdOrderItemService;
import com.getinsured.plandisplay.util.PlanDisplayConstants;
import com.getinsured.plandisplay.util.PlanDisplayUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.thoughtworks.xstream.XStream;

@Component
public class AutoRenewalHelper {
	private static final Logger LOGGER = LoggerFactory.getLogger(PlanDisplaySvcHelper.class);
	
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private IndividualPlanService individualPlanService;
	@Autowired private PdOrderItemService pdOrderItemService;
	@Autowired private PlanDisplayUtil planDisplayUtil;
	@Autowired private Gson platformGson;
	
	private static String EMPTY_STRING = "";
	
	public MLECRestResponse getEmployerContribution(PdHouseholdDTO pdHouseholdDTO, List<PdPersonDTO> pdPersonDTOList, PlanDisplayEnum.EnrollmentFlowType specialEnrollmentFlowType) 
	{		
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Invoking planDisplayUtil.getEmployerContributionAmt() REST call---->", null, false);
		List<Member> members = getMembersForContribution(pdHouseholdDTO, pdPersonDTOList, specialEnrollmentFlowType);
		
		MLECRestRequest mLECRestRequest = new MLECRestRequest();
		mLECRestRequest.setEmployerId(pdHouseholdDTO.getEmployerId().intValue());
		mLECRestRequest.setEmployeeQuotingCountyCode(pdPersonDTOList.get(0).getCountyCode());
		mLECRestRequest.setEmployeeQuotingZip(pdPersonDTOList.get(0).getZipCode());
		mLECRestRequest.setEffectiveDate(pdHouseholdDTO.getCoverageStartDate());
		mLECRestRequest.setMembers(members);
		mLECRestRequest.setEmployeeApplicationId(pdHouseholdDTO.getApplicationId().intValue());
		try{
			return getEmployerContribution(mLECRestRequest);
		}catch(Exception e){
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error in getEmployerContribution()---->"+e.getMessage(), e, false);
			return null;
		}
	}
	
	private MLECRestResponse getEmployerContribution(MLECRestRequest restEmployeeDTO) throws GIException {
		String employeeContributionAmount = ghixRestTemplate.postForObject(ShopEndPoints.GET_EMPLOYER_CONTRIBUTION_AMOUNT, restEmployeeDTO, String.class);// send request to planMgmt for Employer Plans
		
		XStream xStream = GhixUtils.getXStreamStaxObject();
		ShopResponse shopResponse = (ShopResponse) xStream.fromXML(employeeContributionAmount);
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.DEBUG, "<----planDisplayRestClient.getEmployerContributionAmt() REST call  : RESPONSE : ---->"+shopResponse, null, true);
		
		MLECRestResponse mLECRestResponse = null;
		if (shopResponse != null && shopResponse.getErrCode() == 0) {
			mLECRestResponse = (MLECRestResponse) shopResponse.getResponseData().get("MLECRestResponse");
			if(mLECRestResponse.getCoverageStartDate()==null) {
				throw new GIException("Coverage Date Not Found");
			}
			return mLECRestResponse;
		}else if(shopResponse != null){
			throw new GIException(shopResponse.getErrMsg());
		}else{
			throw new GIException("Unable to get Employer Plan Details.");
		}
	}
	
	private List<Member> getMembersForContribution(PdHouseholdDTO pdHouseholdDTO, List<PdPersonDTO> pdPersonDTOList, EnrollmentFlowType specialEnrollmentFlowType) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside getHouseHoldCensusList() :  Forming the Members List.---->", null, false);
		List<Member> membersData = new ArrayList<Member>();
		
		for(PdPersonDTO pdPersonDTO: pdPersonDTOList){
			Member member = new Member();
			member.setMemberID(pdPersonDTO.getExternalId().toString());
			PlanDisplayEnum.Relationship relationship = pdPersonDTO.getRelationship();
			if(PlanDisplayEnum.Relationship.SELF.equals(relationship)) {
				member.setRelationship(Relationship.SE);
			} else if(PlanDisplayEnum.Relationship.SPOUSE.equals(relationship)) {
				member.setRelationship(Relationship.SP);
			} else if(PlanDisplayEnum.Relationship.CHILD.equals(relationship)) {
				member.setRelationship(Relationship.CH);
			} else {
				member.setRelationship(Relationship.DT);
			}
			member.setSmoker(false);
			
			Date coverageDateforAge = pdHouseholdDTO.getCoverageStartDate();
			if(EnrollmentType.S.equals(pdHouseholdDTO.getEnrollmentType()) && EnrollmentFlowType.KEEP.equals(specialEnrollmentFlowType)){
				// Dental contribution is not dependent on age so always pass Health coverage start date 
				coverageDateforAge = pdPersonDTO.getHealthCoverageStartDate() == null ? coverageDateforAge:pdPersonDTO.getHealthCoverageStartDate() ;
			}
			
			int age = PlanDisplayUtil.getAgeFromCoverageDate(coverageDateforAge, pdPersonDTO.getBirthDay());
			member.setAge(age);
			
			membersData.add(member);
		}
		return membersData;
	}
	
	public void setPersonSubsidy(List<Member> members, List<PdPersonDTO> pdPersonDTOList) {
		for(PdPersonDTO pdPersonDTO : pdPersonDTOList){
			for (Member member : members) {
				if(member.getMemberID().equals(pdPersonDTO.getExternalId().toString())){
					BigDecimal bd = new BigDecimal(Float.toString(member.getBenchmarkPremium()));
					bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
					pdPersonDTO.setHealthSubsidy(bd.floatValue());
					
					bd = new BigDecimal(Float.toString(member.getDentalContribution()));
					bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
					pdPersonDTO.setDentalSubsidy(bd.floatValue());
				}
			}
		}
	}
	
	public void updatePersonSubsidy(List<PdPersonDTO> pdPersonDTOList) {
		String postParams = null;
		try {
			ObjectWriter objwriter =JacksonUtils.getJacksonObjectWriterForJavaTypeList(pdPersonDTOList.getClass(), PdPersonDTO.class);
			postParams = objwriter.writeValueAsString(pdPersonDTOList);
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		
		try {
			ghixRestTemplate.postForObject(PlandisplayEndpoints.UPDATE_PERSON_SUBSIDY, postParams,String.class);
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
	}
	
public PlanDisplayResponse processAutoRenewal(PdSession pdSession) {
		
		PlanDisplayResponse planDisplayResponse = new PlanDisplayResponse();		
		processEnrollment(pdSession);
		
		if(YorN.N.equals(pdSession.getInitialHealthPlanAvailable())) {
			planDisplayResponse.startResponse();
			planDisplayResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planDisplayResponse.setErrMsg("<application Id> " + pdSession.getPdHouseholdDTO().getApplicationId() + " : Failed as Health Plan id : "+pdSession.getInitialHealthPlanId()+" is not available for autorenewal case "+ pdSession.getPdHouseholdDTO().getShoppingId());
			planDisplayResponse.endResponse();
		}else{
			planDisplayResponse.startResponse();
			PldOrder responseOrder = new PldOrder();
			responseOrder.setId(pdSession.getPdHouseholdDTO().getId().intValue());
			planDisplayResponse.setPldOrder(responseOrder);
			planDisplayResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);			
			planDisplayResponse.endResponse();
		}
		return planDisplayResponse;
	}

public String setErrorResponse(String errorMsg) {
	
	PlanDisplayResponse planDisplayResponse = new PlanDisplayResponse();		
	
	planDisplayResponse.startResponse();
	planDisplayResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
	planDisplayResponse.setErrMsg(errorMsg);
	planDisplayResponse.endResponse();
	XStream xstream = GhixUtils.getXStreamStaxObject();

	return xstream.toXML(planDisplayResponse);
}

public Map<String, String> getEmployerWorksiteData(Long employerId) {
	PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Invoking planDisplayUtil.getEmployer() REST call---->", null, false);
	try{
		EmployerDTO employerDTO = getEmployer(employerId.toString());
	
		if (employerDTO != null && employerDTO.getLocations() != null) {
			List<EmployerLocation> empLocations = employerDTO.getLocations();
			Map<String, String> workSiteData = new HashMap<String, String>();
			for (EmployerLocation location : empLocations) {
				if (EmployerLocation.PrimaryLocation.YES.equals(location.getPrimaryLocation())) {
					workSiteData.put("employerWorksiteZip",location.getLocation().getZip());
					workSiteData.put("employerWorksiteCounty",location.getLocation().getCountycode());
				}
			}
			return workSiteData;
		} else {
			return null;
		}
	} catch(Exception e) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error in getEmployerWorksiteData()---->" + e.getMessage(), e, false);
		return null;
	}
}

public EmployerDTO getEmployer(String employerId) throws GIException {
	String shopResponseStr =  ghixRestTemplate.getForObject(ShopEndPoints.GET_EMPLOYER_DETAILS+"?employerId="+employerId, String.class);
			
	XStream xstream = GhixUtils.getXStreamStaxObject();
	ShopResponse shopResponse = (ShopResponse) xstream.fromXML(shopResponseStr);
	if(shopResponse != null){
		if(shopResponse.getErrCode()==1){
			throw new GIException(shopResponse.getErrMsg());
		} else {
			return (EmployerDTO) shopResponse.getResponseData().get("Employer");
		}
	} else {
		throw new GIException("Unable to get Employer.");
	}
}

public void processEnrollment(PdSession pdSession) {
	List<PdPersonDTO> pdPersonDTOList = pdSession.getPdPersonDTOList();		
	
	pdSession.setInitialHealthPlanAvailable(YorN.N);
	pdSession.setInitialDentalPlanAvailable(YorN.N);
	
	String defaultPref = planDisplayUtil.getDefaultPref();
	PdPreferencesDTO defaultPrefDTO = platformGson.fromJson(defaultPref, new TypeToken<PdPreferencesDTO>() {}.getType());
	pdSession.setPdPreferencesDTO(defaultPrefDTO);
	
	Long healthEnrollmentID = null;
	Long dentalEnrollmentID = null;		
	
	for (PdPersonDTO pdPersonDTO : pdPersonDTOList) {
		if(healthEnrollmentID == null || EMPTY_STRING.equals(healthEnrollmentID.toString())) {
			healthEnrollmentID = pdPersonDTO.getHealthEnrollmentId();
		}
		if(dentalEnrollmentID == null || EMPTY_STRING.equals(dentalEnrollmentID.toString())) {
			dentalEnrollmentID = pdPersonDTO.getDentalEnrollmentId();
		}
	}
		
	if(healthEnrollmentID != null && !EMPTY_STRING.equals(healthEnrollmentID.toString())) {
		EnrollmentResponse enrollmentResponse = planDisplayUtil.findPlanIdAndOrderItemIdByEnrollmentId(healthEnrollmentID.toString());
		
		if(enrollmentResponse != null && enrollmentResponse.getPlanId() != null)	{
			String initialHealthCmsPlanId = enrollmentResponse.getCmsPlanId();
			String initialHealthSubscriberId = enrollmentResponse.getSubscriberMemberId();
			Float initialHealthPremium = enrollmentResponse.getNetPremiumAmt();
			Float initialHealthSubsidy = new BigDecimal(Float.toString(enrollmentResponse.getGrossPremiumAmt())).subtract(new BigDecimal(Float.toString(enrollmentResponse.getNetPremiumAmt()))).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue();

			pdSession.setInitialHealthPremium(initialHealthPremium);
			pdSession.setInitialHealthSubsidy(initialHealthSubsidy);
			pdSession.setInitialHealthSubscriberId(initialHealthSubscriberId);
			pdSession.setInsuranceType(InsuranceType.HEALTH);
			
			List<IndividualPlan> individualPlans = new ArrayList<IndividualPlan>();
			individualPlans = getPlans(pdSession, false, initialHealthCmsPlanId);
			

			if(individualPlans != null && !individualPlans.isEmpty() && individualPlans.size() == 1) {
				pdSession.setInitialHealthPlanId(Long.valueOf(individualPlans.get(0).getPlanId()));
				pdSession.setInitialHealthPlanAvailable(YorN.Y);
				savePdOrderItem(pdSession, individualPlans.get(0));
			}					
		} 
	}
		
	if(dentalEnrollmentID != null && !EMPTY_STRING.equals(dentalEnrollmentID.toString())) {
		if(YorN.N.equals(pdSession.getInitialHealthPlanAvailable()) && YorN.Y.equals(pdSession.getPdHouseholdDTO().getAutoRenewal())){
			pdSession.setInitialDentalPlanAvailable(YorN.N);
		}else{
			EnrollmentResponse enrollmentResponse = planDisplayUtil.findPlanIdAndOrderItemIdByEnrollmentId(dentalEnrollmentID.toString());
			
			if(enrollmentResponse != null && enrollmentResponse.getPlanId() != null)	{
				String initialDentalCmsPlanId = enrollmentResponse.getCmsPlanId();
				String initialDentalSubscriberId = enrollmentResponse.getSubscriberMemberId();
				Float initialDentalPremium = enrollmentResponse.getNetPremiumAmt();
				Float initialDentalSubsidy = new BigDecimal(Float.toString(enrollmentResponse.getGrossPremiumAmt())).subtract(new BigDecimal(Float.toString(enrollmentResponse.getNetPremiumAmt()))).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue();

				pdSession.setInitialDentalPremium(initialDentalPremium);
				pdSession.setInitialDentalSubsidy(initialDentalSubsidy);
				pdSession.setInitialDentalSubscriberId(initialDentalSubscriberId);
				pdSession.setInsuranceType(InsuranceType.DENTAL);
				
				List<IndividualPlan> individualPlans = new ArrayList<IndividualPlan>();
				individualPlans = getPlans(pdSession, false, initialDentalCmsPlanId);
				
									
				if(individualPlans != null && !individualPlans.isEmpty() && individualPlans.size() == 1) {
					pdSession.setInitialDentalPlanId(Long.valueOf(individualPlans.get(0).getPlanId()));
					pdSession.setInitialDentalPlanAvailable(YorN.Y);
					savePdOrderItem(pdSession, individualPlans.get(0));
				}
			}
		}
	}	
}


public List<IndividualPlan> getPlans(PdSession pdSession, boolean minimizePlanData, String initialCmsPlanId) throws GIRuntimeException {
	String issuerId = null;
	
	PdQuoteRequest pdQuoteRequest = new PdQuoteRequest();		
	pdQuoteRequest.setPdPreferencesDTO(pdSession.getPdPreferencesDTO());
	pdQuoteRequest.setPdHouseholdDTO(pdSession.getPdHouseholdDTO());
	pdQuoteRequest.setPdPersonDTOList(pdSession.getPdPersonDTOList());
	pdQuoteRequest.setEmployerCoverageStartDate(pdSession.getEmployerCoverageStartDate());
	pdQuoteRequest.setEmployerPlanTier(pdSession.getEmployerPlanTier());
	pdQuoteRequest.setEmployerWorksiteZip(pdSession.getEmployerWorksiteZip());
	pdQuoteRequest.setEmployerWorksiteCounty(pdSession.getEmployerWorksiteCounty());
	pdQuoteRequest.setExchangeType(ExchangeType.ON);
	pdQuoteRequest.setShowCatastrophicPlan(false);
	pdQuoteRequest.setInsuranceType(pdSession.getInsuranceType());
	pdQuoteRequest.setMinimizePlanData(minimizePlanData);
	pdQuoteRequest.setIssuerId(issuerId);
	if(EnrollmentType.S.equals(pdSession.getPdHouseholdDTO().getEnrollmentType())){
		pdQuoteRequest.setSpecialEnrollmentFlowType(pdSession.getSpecialEnrollmentFlowType());
		if(InsuranceType.HEALTH.equals(pdSession.getInsuranceType())){
			pdQuoteRequest.setInitialSubscriberId(pdSession.getInitialHealthSubscriberId());
		}else{
			pdQuoteRequest.setInitialSubscriberId(pdSession.getInitialDentalSubscriberId());
		}
		if(EnrollmentFlowType.NEW.equals(pdSession.getSpecialEnrollmentFlowType())){
			if(InsuranceType.HEALTH.equals(pdSession.getInsuranceType())){
				pdQuoteRequest.setInitialPlanIdToEliminate(pdSession.getInitialHealthPlanId());
			}else{
				pdQuoteRequest.setInitialPlanIdToEliminate(pdSession.getInitialDentalPlanId());
			}
		}else{
			pdQuoteRequest.setInitialCmsPlanId(initialCmsPlanId);
		}
	}
	
	if(EnrollmentType.A.equals(pdSession.getPdHouseholdDTO().getEnrollmentType())) {
		pdQuoteRequest.setInitialCmsPlanId(initialCmsPlanId);
	}
	
	try{
				
		PdResponse pdResponse = new PdResponse();
		pdResponse.startResponse();
	
		List<IndividualPlan> individualPlanList = individualPlanService.getPlans(pdQuoteRequest);
		
		return individualPlanList;
		
	} catch (Exception e) {
		return null;
	}
}

	public void savePdOrderItem(PdSession pdSession, IndividualPlan individualPlan) {
		PdSaveCartRequest pdSaveCartRequest = new PdSaveCartRequest();
		pdSaveCartRequest.setInsuranceType(pdSession.getInsuranceType());
		pdSaveCartRequest.setHouseholdId(pdSession.getPdHouseholdDTO().getId());
		pdSaveCartRequest.setMonthlySubsidy(Float.valueOf(individualPlan.getAptc()));
		pdSaveCartRequest.setMonthlyStateSubsidy(individualPlan.getStateSubsidy());
		pdSaveCartRequest.setPlanId(individualPlan.getPlanId().longValue());
		pdSaveCartRequest.setPremiumAfterCredit(individualPlan.getPremiumAfterCredit());
		pdSaveCartRequest.setPremiumBeforeCredit(individualPlan.getPremiumBeforeCredit());
		pdSaveCartRequest.setPremiumData(individualPlan.getPlanDetailsByMember());
		pdSaveCartRequest.setSubsidyData(individualPlan.getContributionData());
		
		boolean validationSuccess = validateEncodedPremium(individualPlan);
		if(validationSuccess){
			if(InsuranceType.HEALTH.equals(pdSession.getInsuranceType()))
			{
				pdSaveCartRequest.setPlanPreferences(PlanDisplayUtil.getDefaultPlanPreferences());
			}
			saveCartItem(pdSaveCartRequest);		
		}	
	}

	public boolean validateEncodedPremium(IndividualPlan individualPlan) {
		boolean result = false;
		ArrayList<String> encodedPremiumList = new ArrayList<String>();
		encodedPremiumList.add(Long.toString(individualPlan.getPlanId()));
		encodedPremiumList.add(Float.toString(individualPlan.getPremiumBeforeCredit()));
		encodedPremiumList.add(Float.toString(individualPlan.getPremiumAfterCredit()));
	
		if(individualPlan.getPlanDetailsByMember()!=null && !individualPlan.getPlanDetailsByMember().isEmpty()){			
			for(Map<String, String> planDetailsByMemberMap : individualPlan.getPlanDetailsByMember()){
				encodedPremiumList.add(planDetailsByMemberMap.get("premium"));
			}
		}
		try {
			result = GhixUtils.validateGhixSecureCheckSum(encodedPremiumList, individualPlan.getEncodedPremium());
		} catch (NoSuchAlgorithmException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occur while converting encoded premium---->" + e.getMessage(), e, false);
			return result;
		}
		return result;
	}

	public void saveCartItem(PdSaveCartRequest pdSaveCartRequest) throws GIRuntimeException {
		try {		
				pdOrderItemService.saveItem(pdSaveCartRequest);		
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, PlanDisplayConstants.ErrorCodes.FAILED_TO_EXECUTE_SAVE_CART_ITEM + e.getMessage(), e, false);
			throw new GIRuntimeException("Error occurred while executing saveCartItem()");
		}	
	}
	

}
