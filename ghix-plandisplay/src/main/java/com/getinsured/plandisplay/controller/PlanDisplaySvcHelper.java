package com.getinsured.plandisplay.controller;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.plandisplay.DrugDTO;
import com.getinsured.hix.dto.plandisplay.PdHouseholdDTO;
import com.getinsured.hix.dto.plandisplay.PdOrderItemDTO;
import com.getinsured.hix.dto.plandisplay.PdOrderItemPersonDTO;
import com.getinsured.hix.dto.plandisplay.PdPersonDTO;
import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;
import com.getinsured.hix.dto.plandisplay.PlanPremiumDTO;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GEnrollment;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GMember;
import com.getinsured.hix.dto.plandisplay.ind71g.Ind71GRequest;
import com.getinsured.hix.dto.plandisplay.planavailability.PdOrderItemPersonResponse;
import com.getinsured.hix.dto.plandisplay.planavailability.PdOrderItemResponse;
import com.getinsured.hix.dto.plandisplay.planavailability.PdPersonRequest;
import com.getinsured.hix.dto.plandisplay.planavailability.PdRequestPlanAvailability;
import com.getinsured.hix.dto.plandisplay.planavailability.PdResponsePlanAvailability;
import com.getinsured.hix.dto.planmgmt.PlanCrossWalkRequestDTO;
import com.getinsured.hix.dto.planmgmt.PlanCrossWalkResponseDTO;
import com.getinsured.hix.dto.planmgmt.PlanRateBenefitRequest;
import com.getinsured.hix.dto.planmgmt.PlanRateBenefitResponse;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.dto.planmgmt.SbcScenarioDTO;
import com.getinsured.hix.model.IndividualPlan;
import com.getinsured.hix.model.PdHousehold;
import com.getinsured.hix.model.PdOrderItem;
import com.getinsured.hix.model.PdOrderItemPerson;
import com.getinsured.hix.model.PdPerson;
import com.getinsured.hix.model.PdPreferences;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanRateBenefit;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.plandisplay.GroupData;
import com.getinsured.hix.model.plandisplay.PdOrderResponse;
import com.getinsured.hix.model.plandisplay.PdQuoteRequest;
import com.getinsured.hix.model.plandisplay.PdResponse;
import com.getinsured.hix.model.plandisplay.PdWSResponse;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentFlowType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ExchangeType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.MedicalProcedure;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;
import com.getinsured.hix.model.plandisplay.ProviderBean;
import com.getinsured.hix.planmgmt.controller.PlanRestController;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.events.PartnerEventDataFieldEnum;
import com.getinsured.hix.platform.events.PartnerEventService;
import com.getinsured.hix.platform.events.PartnerEventTypeEnum;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.PlanMgmtEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIException.GhixErrors;
import com.getinsured.plandisplay.service.IndividualPlanService;
import com.getinsured.plandisplay.service.PdHouseholdService;
import com.getinsured.plandisplay.service.PdOrderItemPersonService;
import com.getinsured.plandisplay.service.PdOrderItemService;
import com.getinsured.plandisplay.service.PdPersonService;
import com.getinsured.plandisplay.service.PdPreferencesService;
import com.getinsured.plandisplay.service.prescriptionSearch.PrescriptionData;
import com.getinsured.plandisplay.service.prescriptionSearch.PrescriptionFactory;
import com.getinsured.plandisplay.service.prescriptionSearch.PrescriptionRequest;
import com.getinsured.plandisplay.service.prescriptionSearch.PrescriptionSearch;
import com.getinsured.plandisplay.service.providerSearch.ProviderFactory;
import com.getinsured.plandisplay.service.providerSearch.ProviderSearch;
import com.getinsured.plandisplay.util.CreateDTOBean;
import com.getinsured.plandisplay.util.PlanDisplayConstants;
import com.getinsured.plandisplay.util.PlanDisplayConstants.ErrorCodes;
import com.getinsured.plandisplay.util.PlanDisplayUtil;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

@Component
public class PlanDisplaySvcHelper {
	private static final Logger LOGGER = LoggerFactory.getLogger(PlanDisplaySvcHelper.class);
	
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private PdOrderItemPersonService pdOrderItemPersonService;
	@Autowired private CreateDTOBean createDTOBean;	
	@Autowired private PdOrderItemService pdOrderItemService;
	@Autowired private PlanDisplayUtil planDisplayUtil;
	@Autowired private ZipCodeService zipCodeService;
	@Autowired private PdHouseholdService pdHouseholdService;
	@Autowired private PartnerEventService partnerEventService;
	@Autowired private PdPreferencesService pdPreferencesService;
	@Autowired private PlanRestController planRestController;
	@Autowired private IndividualPlanService individualPlanService;
	@Autowired private Gson platformGson;
	@Autowired private PrescriptionFactory prescriptionFactory;
	@Autowired private ProviderFactory providerFactory;
	@Autowired private PdPersonService pdPersonService;
	
	public PdResponse sendSuccess(PdResponse pdResponse) {
		pdResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);			
		pdResponse.endResponse();
		return pdResponse;
	}
	
	public PdResponse sendException(ErrorCodes errorCode, Exception e) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, errorCode.getMessage() + PlanDisplayConstants.COLON, e, false);
		PdResponse pdResponse = new PdResponse();
		pdResponse.startResponse();
		pdResponse.setStatus("FAILED");
		pdResponse.setErrCode(errorCode.getId());
		pdResponse.setErrMsg(errorCode.getMessage());
		pdResponse.setNestedStackTrace(ExceptionUtils.getFullStackTrace(e));
		pdResponse.endResponse();
		return pdResponse;
	}
	public PdResponse sendNull() {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, PlanDisplayConstants.ErrorCodes.REQUEST_IS_NULL.getMessage(), null, false);
		PdResponse pdResponse = new PdResponse();
		pdResponse.startResponse();
		pdResponse.setStatus("FAILED");
		pdResponse.setErrCode(PlanDisplayConstants.ErrorCodes.REQUEST_IS_NULL.getId());
		pdResponse.setErrMsg(PlanDisplayConstants.ErrorCodes.REQUEST_IS_NULL.getMessage());				
		pdResponse.endResponse();
		return pdResponse;
	}
	
	public PdResponse sendFailure(String errorMsg){
		PdResponse pdResponse = new PdResponse();
		pdResponse.startResponse();
		pdResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		pdResponse.setErrMsg(errorMsg);
		pdResponse.endResponse();
		return pdResponse;
	}
	
	public void createPdOrderResponseForEnrollment(PdHousehold pdHousehold, List<PdOrderItem> pdOrderItems, PdOrderResponse pdOrderResponse, boolean isIND71G, Map<Long, String> orderItemToEnrollment){
		List<PdOrderItemDTO> pdOrderItemDTOList = new ArrayList<PdOrderItemDTO>();
		for(PdOrderItem pdOrderItem : pdOrderItems)	{
			try {
				PdOrderItemDTO pdOrderItemDTO = new PdOrderItemDTO();
				BigDecimal premiumBeforeCredit = new BigDecimal(Float.toString(pdOrderItem.getPremiumBeforeCredit())).setScale(2,BigDecimal.ROUND_HALF_UP);
				pdOrderItem.setPremiumBeforeCredit(premiumBeforeCredit.floatValue());
				BigDecimal premiumAfterCredit = new BigDecimal(Float.toString(pdOrderItem.getPremiumAfterCredit())).setScale(2,BigDecimal.ROUND_HALF_UP);
				pdOrderItem.setPremiumAfterCredit(premiumAfterCredit.floatValue());
				BigDecimal monthlySubsidy = BigDecimal.ZERO;
				BigDecimal totalSubsidy = BigDecimal.ZERO;
				if(pdOrderItem.getMonthlySubsidy() != null){
					monthlySubsidy = new BigDecimal(Float.toString(pdOrderItem.getMonthlySubsidy())).setScale(2,BigDecimal.ROUND_HALF_UP);
					pdOrderItem.setMonthlySubsidy(monthlySubsidy.floatValue());
				}
				if(pdOrderItem.getMonthlyStateSubsidy() != null){
					totalSubsidy = monthlySubsidy.add(pdOrderItem.getMonthlyStateSubsidy());
				}
				else{
					totalSubsidy = monthlySubsidy;
				}

				BigDecimal grossPremium= premiumAfterCredit.add(totalSubsidy).setScale(2,BigDecimal.ROUND_HALF_UP);
				if(premiumBeforeCredit.compareTo(grossPremium) != 0){
					String values = "premiumBeforeCredit : "+premiumBeforeCredit+", grossPremium : "+ grossPremium+", aptc : "+monthlySubsidy+", orderItmId : "+pdOrderItem.getId();
					throw new GIException("premiumBeforeCredit.compareTo(grossPremium) = '"+premiumBeforeCredit.compareTo(grossPremium) +"'. Details : "+ values);
				}
				createDTOBean.copyProperties(pdOrderItemDTO, pdOrderItem);
				if(orderItemToEnrollment != null)
				{
					pdOrderItemDTO.setPreviousEnrollmentId(orderItemToEnrollment.get(pdOrderItem.getId()));
				}
				List<PdOrderItemPersonDTO> pdOrderItemPersonDTOList = new ArrayList<PdOrderItemPersonDTO>();
				List<PdOrderItemPerson> pdOrderItemPersons = pdOrderItemPersonService.getByOrderItemId(pdOrderItem.getId());
				if(isIND71G)
				{
					pdHousehold = pdOrderItem.getPdHousehold();
				}
				PlanResponse planResponse = planDisplayUtil.getPlanDetails(pdOrderItem.getPlanId().toString(), DateUtil.dateToString(pdHousehold.getCoverageStartDate(), "yyyy-MM-dd"), true);
				if(planResponse != null){
					BigDecimal aggrAptc = null;					
					if(InsuranceType.HEALTH.toString().equalsIgnoreCase(planResponse.getInsuranceType())){
						if("CATASTROPHIC".equalsIgnoreCase(planResponse.getPlanLevel())){
							aggrAptc = pdHousehold.getMaxSubsidy() != null ? new BigDecimal(Float.toString(pdHousehold.getMaxSubsidy())):null;

							if(pdHousehold.getMaxStateSubsidy() != null){
								pdOrderItemDTO.setMaxStateSubsidy(pdHousehold.getMaxStateSubsidy());
							}
						}else{
							aggrAptc = pdHousehold.getHealthSubsidy() != null ? new BigDecimal(Float.toString(pdHousehold.getHealthSubsidy())) : null;

							if(pdHousehold.getMaxStateSubsidy() != null){
								pdOrderItemDTO.setMaxStateSubsidy(pdHousehold.getMaxStateSubsidy());
							}
						}
					}else if(InsuranceType.DENTAL.toString().equalsIgnoreCase(planResponse.getInsuranceType())){
						aggrAptc = pdHousehold.getDentalSubsidy() != null ? new BigDecimal(Float.toString(pdHousehold.getDentalSubsidy())) : null;
					}
					if(aggrAptc != null){
						pdOrderItemDTO.setAggrAptc(aggrAptc.setScale(2, BigDecimal.ROUND_HALF_UP));
					}
				}				
				
				for(PdOrderItemPerson pdOrderItemPerson : pdOrderItemPersons) {
					PdOrderItemPersonDTO pdOrderItemPersonDTO = new PdOrderItemPersonDTO();
					createDTOBean.copyProperties(pdOrderItemPersonDTO, pdOrderItemPerson);
					pdOrderItemPersonDTO.setExternalId(pdOrderItemPerson.getPdPerson().getExternalId());
					pdOrderItemPersonDTO.setRelationship(pdOrderItemPerson.getPdPerson().getRelationship());
					pdOrderItemPersonDTOList.add(pdOrderItemPersonDTO);
				}
				pdOrderItemDTO.setPdOrderItemPersonDTOList(pdOrderItemPersonDTOList);
				pdOrderItemDTO.setPdHousehold(pdHousehold);
				pdOrderItemDTOList.add(pdOrderItemDTO);
			} catch (GIException e) {
				pdOrderResponse.setErrMsg(e.getErrorMsg());
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----GIException occured while processing pdOrderResponse---->", e, false);
			}catch (Exception e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occured in createPdOrderResponseForEnrollment---->", e, false);
			}
		}
		pdOrderResponse.setPdOrderItemDTOList(pdOrderItemDTOList);
		if(!isIND71G)
		{
			pdOrderResponse.setGiWsPayloadId(pdHousehold.getGiWsPayloadId());
		}
	}	

	/**
	 * @deprecated This method is not in use.
	 * @param hiosPlanNumber
	 * @param memberList
	 * @param enrollmentCoverageStartDate
	 * @return
	 * @throws GIException
	 */
	@Deprecated
	public PlanRateBenefitResponse getPlanRateBenefitResponse(String hiosPlanNumber, List<Map<String,String>> memberList, Date enrollmentCoverageStartDate) throws GIException{
		PlanRateBenefitRequest planRequest = new PlanRateBenefitRequest();
		Map<String,Object> requestParam = new HashMap<String,Object>();
		requestParam.put("memberList", memberList);
		requestParam.put("effectiveDate", DateUtil.dateToString(enrollmentCoverageStartDate, "yyyy-MM-dd"));
		requestParam.put("hiosPlanNumber", hiosPlanNumber);
		planRequest.setRequestParameters(requestParam);
		planRequest.setUrl(null);
		String planRequestString = platformGson.toJson(planRequest);
		String postRespPlanMgmnt = (ghixRestTemplate.exchange(PlanMgmtEndPoints.GET_MEMBER_LEVEL_PLAN_RATE, GhixConstants.USER_NAME_EXCHANGE ,HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, planRequestString)).getBody();	
		PlanRateBenefitResponse planRateResponse = new PlanRateBenefitResponse();
		try{
			planRateResponse = platformGson.fromJson(postRespPlanMgmnt, planRateResponse.getClass());
		}catch(JsonSyntaxException jse){
			throw new GIException(jse);
		}		
		return planRateResponse;		
	}
	
		
	/*
	 * Use to validate Request Plan Availability
	 */
	public String pdRequestPlanValidate(PdRequestPlanAvailability pdRequestPlanAvailability) throws GIException	{

		PdResponse pdResponse = new PdResponse();
	 	pdResponse.startResponse();
		String coverageDate = null;
		String result = PlanDisplayConstants.SUCCESS;
		
		List<PdPersonRequest> pdPersonRequest = pdRequestPlanAvailability.getPdPersonCRDTOList();
		
		coverageDate = pdRequestPlanAvailability.getCoverageDate();
		PdResponse coverageDateMsg = validateCoverageDate(coverageDate, pdResponse);
		result= !PlanDisplayConstants.SUCCESS.equals(coverageDateMsg.getStatus()) ? coverageDateMsg.getErrMsg() : coverageDateMsg.getStatus();
		if (result != null && !result.equals( PlanDisplayConstants.SUCCESS))
		{
			return result;
		}
		
		PdResponse shoppingTypeMsg = (pdRequestPlanAvailability.getShoppingType() == null || !"INDIVIDUAL".equals(pdRequestPlanAvailability.getShoppingType().toString() ) ) ? sendFailure("Invalid shoppingType : "+ pdRequestPlanAvailability.getShoppingType()) : sendSuccess(pdResponse) ;		
		result = !PlanDisplayConstants.SUCCESS.equals(shoppingTypeMsg.getStatus()) ? shoppingTypeMsg.getErrMsg() : shoppingTypeMsg.getStatus();
		if (result != null && !result.equals( PlanDisplayConstants.SUCCESS))
		{
			return result;
		}
		
		PdResponse enrollmentTypeMsg = validateEnrollmentType(pdRequestPlanAvailability.getEnrollmentType(), pdResponse);		
		result = !PlanDisplayConstants.SUCCESS.equals(enrollmentTypeMsg.getStatus()) ? enrollmentTypeMsg.getErrMsg() : enrollmentTypeMsg.getStatus();
		if (result != null && !result.equals( PlanDisplayConstants.SUCCESS))
		{
			return result;
		}
		
		if(pdPersonRequest != null && !pdPersonRequest.isEmpty()) {
			
		for (PdPersonRequest personRequest : pdPersonRequest) {
			
			PdResponse memberGUIDMsg = StringUtils.isEmpty(personRequest.getMemberGUID()) || personRequest.getMemberGUID().equalsIgnoreCase("0") ? sendFailure("Invalid MemberGUID : "+ personRequest.getMemberGUID()) : sendSuccess(pdResponse) ;
			result =  !PlanDisplayConstants.SUCCESS.equals(memberGUIDMsg.getStatus()) ? memberGUIDMsg.getErrMsg() : memberGUIDMsg.getStatus();
			if(result != null && !result.equals( PlanDisplayConstants.SUCCESS)){
				return result;
			}
			
			PdResponse dobMsg = validateDOB(personRequest.getDob(), pdResponse);	
			result = !PlanDisplayConstants.SUCCESS.equals(dobMsg.getStatus()) ? dobMsg.getErrMsg() : dobMsg.getStatus();
			if(result != null && !result.equals( PlanDisplayConstants.SUCCESS)){
				return result;
			}
			
			PdResponse zipCodeMsg = (personRequest.getZipCode() == null || !StringUtils.isNumeric(personRequest.getZipCode())) ? sendFailure("Invalid ZipCode : "+ personRequest.getZipCode()) : sendSuccess(pdResponse) ;
			result =  !PlanDisplayConstants.SUCCESS.equals(zipCodeMsg.getStatus()) ? zipCodeMsg.getErrMsg() : zipCodeMsg.getStatus();
			if(result != null && !result.equals( PlanDisplayConstants.SUCCESS)){
				return result;
			}
			
			PdResponse countyCodeMsg = (personRequest.getCountyCode() == null || !StringUtils.isNumeric(personRequest.getCountyCode())) ? sendFailure("Invalid CountyCode : "+ personRequest.getCountyCode()) : sendSuccess(pdResponse) ;
			result =  !PlanDisplayConstants.SUCCESS.equals(countyCodeMsg.getStatus()) ? countyCodeMsg.getErrMsg() : countyCodeMsg.getStatus();
			if(personRequest.getCountyCode()!= null && !("".equals(personRequest.getCountyCode()))) {
				if (!(zipCodeService.validateZipAndCountyCode(personRequest.getZipCode(), personRequest.getCountyCode()))) {
					
					PdResponse countyCodeErrorMsg = sendFailure("Invalid CountyCode : "+ personRequest.getCountyCode());
					result = countyCodeErrorMsg.getErrMsg();
				}				
			}
			
			if(result != null && !result.equals( PlanDisplayConstants.SUCCESS)){
				return result;
			}
		  }
		}
		return result;
	}
	
	public PdResponse validateEnrollmentType(EnrollmentType enrollmentType, PdResponse pdResponse) {
		if (enrollmentType == null)
		{
			return sendFailure("Invalid EnrollmentType : ");
		}
		String enrollType = enrollmentType.toString();
		 switch (enrollType) {
	        case "I":
	            break;
	        case "S":
	            break;
	        case "A":
	            break;
	        default:
	        	return sendFailure("Invalid EnrollmentType : "+ enrollmentType);
	     }
		 pdResponse.startResponse();
		 return sendSuccess(pdResponse);
	}

	public PdResponse validateCoverageDate(String coverageDate,PdResponse pdResponse) throws GIException {
		if (coverageDate == null || coverageDate.isEmpty())
		{
			return sendFailure("Invalid CoverageDate : "+ coverageDate);
		}
		try {
			
		    DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		    formatter.setLenient(false);
		    formatter.parse(coverageDate);
		} catch (ParseException e) { 
			return sendFailure("Invalid CoverageDate : "+ coverageDate);
		}
		return sendSuccess(pdResponse);
	}

	public PdResponse validateDOB(String dob, PdResponse pdResponse) throws GIException {
		
		if (dob == null || dob.isEmpty())
		{
			return sendFailure("Invalid Dob : "+ dob);
		}
		try {
		    DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		    formatter.setLenient(false);
		    formatter.parse(dob);
		} catch (ParseException e) { 
			return sendFailure("Invalid Dob : "+ dob);
		}
		
		return sendSuccess(pdResponse);
	}
	
	public PdHouseholdDTO populateHouseholdDtoFromPdRequest(PdRequestPlanAvailability pdRequestPlanAvailability) {
		PdHouseholdDTO  pdHouseholdDTO = new PdHouseholdDTO(); 
		CostSharing costShare = CostSharing.CS1;
		if(pdRequestPlanAvailability.getCostSharing() != null){
			costShare = pdRequestPlanAvailability.getCostSharing();
		}
		pdHouseholdDTO.setCostSharing(costShare);
		Date coverageStartDate = DateUtil.StringToDate(pdRequestPlanAvailability.getCoverageDate(), "MM/dd/yyyy");
		pdHouseholdDTO.setCoverageStartDate(coverageStartDate);
		pdHouseholdDTO.setShoppingType(pdRequestPlanAvailability.getShoppingType());
		pdHouseholdDTO.setMaxSubsidy(pdRequestPlanAvailability.getMaxSubsidy());
		pdHouseholdDTO.setMaxStateSubsidy(pdRequestPlanAvailability.getMaxStateSubsidy());
		String externalId = String.valueOf(pdRequestPlanAvailability.getHouseholdGUID());
		pdHouseholdDTO.setExternalId(externalId);
		EnrollmentType enrollmentType = pdRequestPlanAvailability.getEnrollmentType();
		pdHouseholdDTO.setEnrollmentType(enrollmentType);
		pdHouseholdDTO.setId(pdRequestPlanAvailability.getHouseholdId());
		return pdHouseholdDTO;
	}

	public List<PdPersonDTO> populatePdPersonDTOFromPdPersonRequest(List<PdPersonRequest> pdPersonRequest, Date coverageStartDate, boolean isAdultInExistingMembers, Map<String,String> memberCoverageDataMap) {
		List<PdPersonDTO> pdPersonDTOList = new ArrayList<PdPersonDTO>(); 
		for (PdPersonRequest personRequest : pdPersonRequest) {			
			PdPersonDTO pdPersonDTO = new PdPersonDTO();
			pdPersonDTO.setId(personRequest.getId());
			pdPersonDTO.setExternalId(personRequest.getMemberGUID());
			Date birthDay = DateUtil.StringToDate(personRequest.getDob(), "MM/dd/yyyy");
			pdPersonDTO.setBirthDay(birthDay);
			pdPersonDTO.setRelationship(personRequest.getRelationship());
			pdPersonDTO.setZipCode(personRequest.getZipCode());
			pdPersonDTO.setCountyCode(personRequest.getCountyCode());
			pdPersonDTO.setTobacco(personRequest.getTobacco());
			pdPersonDTO.setHealthCoverageStartDate(coverageStartDate);
			pdPersonDTO.setDentalCoverageStartDate(coverageStartDate);			
			Long healthEnrollmentID = personRequest.getHealthEnrollmentId()!=null ?personRequest.getHealthEnrollmentId():null;
			Long dentalEnrollmentID = personRequest.getDentalEnrollmentId()!=null ?personRequest.getDentalEnrollmentId():null;
			pdPersonDTO.setHealthEnrollmentId(healthEnrollmentID);
			pdPersonDTO.setDentalEnrollmentId(dentalEnrollmentID);			
			pdPersonDTO.setCatastrophicEligible(personRequest.getCatastrophicEligible());		
			setSpcialEnrollmentData(coverageStartDate, isAdultInExistingMembers, pdPersonDTO, memberCoverageDataMap);
			pdPersonDTOList.add(pdPersonDTO);
		}
		
		return pdPersonDTOList;
	}
	
	public void setSpcialEnrollmentData(Date coverageStartDate, boolean isAdultInExistingMembers, PdPersonDTO pdPerson, Map<String,String> memberCoverageDateMap) 
	{		
		pdPerson.setHealthCoverageStartDate(coverageStartDate);
		pdPerson.setDentalCoverageStartDate(coverageStartDate);
		String memberId = String.valueOf(pdPerson.getExternalId());
		if(memberCoverageDateMap != null){ 
			if(memberCoverageDateMap.get("H"+memberId) != null) {
				pdPerson.setHealthCoverageStartDate(DateUtil.StringToDate(memberCoverageDateMap.get("H"+memberId), "MM/dd/yyyy"));
			}
			
			if(memberCoverageDateMap.get("D"+memberId) != null) {
				pdPerson.setDentalCoverageStartDate(DateUtil.StringToDate(memberCoverageDateMap.get("D"+memberId), "MM/dd/yyyy"));
			}
		}	
		
		if (pdPerson.getDentalEnrollmentId()!=null) {
			YorN seekCoverage = planDisplayUtil.setSeekCoverage(pdPerson.getDentalCoverageStartDate(), DateUtil.dateToString(pdPerson.getBirthDay(), "MM/dd/yyyy"));
			pdPerson.setSeekCoverage(seekCoverage);
		} else {
			int age = PlanDisplayUtil.getAgeFromCoverageDate(coverageStartDate, pdPerson.getBirthDay());
			if (age < PlanDisplayConstants.CHILD_AGE) {
				pdPerson.setSeekCoverage(YorN.Y);
			}else{
				String dentalPlanSelection = planDisplayUtil.getDentalConfig(coverageStartDate); 
				
				if(isAdultInExistingMembers && !"PO".equals(dentalPlanSelection)){
					pdPerson.setSeekCoverage(YorN.Y);
				}else{
					pdPerson.setSeekCoverage(YorN.N);
				}
			}
		}
	}
	
	public Map<String,String> getMemberCoverageDate(List<PdPersonRequest> pdPersonRequest, String changeEffectiveDate)
	{
		Map<String,String> data = new HashMap<String,String>();
		String userName = planDisplayUtil.getCurrentUser(null);
		for(PdPersonRequest member : pdPersonRequest)
		{
			String medicalEnrollmentId  = null;
			String sadpEnrollmentId  = null;
			if(member.getHealthEnrollmentId() != null){
				medicalEnrollmentId = Long.toString(member.getHealthEnrollmentId());
			} 
			if(member.getDentalEnrollmentId() != null){
				sadpEnrollmentId = Long.toString(member.getDentalEnrollmentId());
			}
			if(medicalEnrollmentId != null) {
				long time = System.currentTimeMillis();
				planDisplayUtil.getMemberCoverageDateforHealthDental(data, medicalEnrollmentId, userName, String.valueOf(member.getMemberGUID()), "H", changeEffectiveDate);
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.WARN, "received getMemberCoverageDateforHealthDental response for EnrollmentId " + medicalEnrollmentId + " in " + (System.currentTimeMillis() - time) + " ms", null, false);
			}
			
			if(sadpEnrollmentId != null) {
				long time = System.currentTimeMillis();
				planDisplayUtil.getMemberCoverageDateforHealthDental(data, sadpEnrollmentId, userName, String.valueOf(member.getMemberGUID()), "D", changeEffectiveDate);
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.WARN, "received getMemberCoverageDateforHealthDental response for EnrollmentId " + sadpEnrollmentId + " in " + (System.currentTimeMillis() - time) + " ms", null, false);
			}
		}
		return data;
	}
	public boolean isAdultPresentInExistingMemberList(List<PdPersonRequest> pdPersonRequest, Map<String,String> memberCoverageDateMap) 
	{
		boolean isAdult = false;
		for(PdPersonRequest member : pdPersonRequest)
		{			
			if(member.getDentalEnrollmentId() != null) 
			{
				if(isAdult == false){
					String memberId = String.valueOf(member.getMemberGUID());
					String coverageStartDate = memberCoverageDateMap.get("D"+memberId);
					isAdult = planDisplayUtil.isAdultMember(coverageStartDate, member.getDob());					
				}
				if(isAdult == true){
					return true;
				}
			}			
		}
		return isAdult;
	}
	

	public PdQuoteRequest createPdQuoteRequest(PdHouseholdDTO pdHouseholdDTO, 	List<PdPersonDTO> pdPersonDTOList, String initialSubscriberId, String initialCmsPlanId, EnrollmentType enrollmentType, InsuranceType insuranceType, ExchangeType exchangeType) {
		PdQuoteRequest pdQuoteRequest = new PdQuoteRequest();		
		String defaultPref = PlanDisplayUtil.getDefaultPref();
		PdPreferencesDTO defaultPrefDTO = platformGson.fromJson(defaultPref, new TypeToken<PdPreferencesDTO>() {}.getType());
		pdQuoteRequest.setPdPreferencesDTO(defaultPrefDTO);
		pdQuoteRequest.setPdHouseholdDTO(pdHouseholdDTO);
		pdQuoteRequest.setPdPersonDTOList(pdPersonDTOList);
		pdQuoteRequest.setExchangeType(exchangeType);
		boolean isEligibleForCatastrophic = true;
		for(PdPersonDTO pdPersonDTO : pdPersonDTOList){
			if(YorN.N.equals(pdPersonDTO.getCatastrophicEligible())){
				isEligibleForCatastrophic = false;
			}
		}
		pdQuoteRequest.setShowCatastrophicPlan(isEligibleForCatastrophic);
		pdQuoteRequest.setMinimizePlanData(true);
		pdQuoteRequest.setIssuerId(null);
		pdQuoteRequest.setInitialCmsPlanId(initialCmsPlanId);
		if(EnrollmentType.S.equals(enrollmentType)){
			pdQuoteRequest.setSpecialEnrollmentFlowType(EnrollmentFlowType.KEEP);
			pdQuoteRequest.setInitialSubscriberId(initialSubscriberId);	
		}
		pdQuoteRequest.setInsuranceType(insuranceType);	
		
		return pdQuoteRequest;
	}
	
	public List<PdOrderItemDTO> getPdOrderItemsByLeadId(String leadId) {
		if (leadId == null) {
			return null;
		}
		List<PdOrderItemDTO> pdOrderItemDTOList = new ArrayList<PdOrderItemDTO>();
		try{
			List<PdHousehold> pdHouseholdList = pdHouseholdService.findPdHouseholdsByLeadId(leadId);
			if(pdHouseholdList != null && !pdHouseholdList.isEmpty()){
				for(PdHousehold pdHousehold:pdHouseholdList){
					getPdOrderItemsByHouseholdId(pdHousehold, pdOrderItemDTOList);
				}				
			}
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occured while getting list of pd order items by lead id---->", e, false);
		}
		return pdOrderItemDTOList;
	}
	
	public void getPdOrderItemsByHouseholdId(PdHousehold pdHousehold, List<PdOrderItemDTO> pdOrderItemDTOList) throws Exception{
		List<PdOrderItem> pdOrderItemList = pdOrderItemService.findAllOrderItemIdsByHouseholdId(pdHousehold.getId());
		if(pdOrderItemList != null && !pdOrderItemList.isEmpty()){
			for(PdOrderItem pdOrderItem : pdOrderItemList){
				PdOrderItemDTO pdOrderItemDTO = new PdOrderItemDTO();
				createDTOBean.copyProperties(pdOrderItemDTO, pdOrderItem);
				pdOrderItemDTO.setPdHousehold(pdHousehold);
				pdOrderItemDTOList.add(pdOrderItemDTO);
			}
		}		
	}
	
	public void publishPartnerEvent(String individualOrderItemId, Long leadId, PartnerEventTypeEnum event) {
		if(individualOrderItemId != null && leadId != null) {
			Map<PartnerEventDataFieldEnum, String> eventData = new HashMap<>();
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Event Notification: Attempting to affiliate from context if available:---->"+TenantContextHolder.getAffiliateId(), null, false);
			Long affiliateId = TenantContextHolder.getAffiliateId();
			
			//In case of add to cart, the eliglead object does not have affiliate id set in the object
			//So a DB fetch of the eligLead using primary key is done to re-fetch the latest lead data from database.
			if(affiliateId == null) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Event Notification: Attempting to get affiliate from lead if available:---->"+leadId, null, false);
				EligLead eligLead = planDisplayUtil.fetchEligLeadRecord(leadId);
				affiliateId = eligLead.getAffiliateId();
			}
			
			if(affiliateId != null) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Event Notification: Publishing event as affiliate found:---->"+affiliateId, null, false);
				eventData.put(PartnerEventDataFieldEnum.AFFILIATE_ID, String.valueOf(affiliateId));
				eventData.put(PartnerEventDataFieldEnum.LEAD_ID, leadId.toString());
				eventData.put(PartnerEventDataFieldEnum.CART_ITEM_ID, individualOrderItemId);
				eventData.put(PartnerEventDataFieldEnum.EVENT_NAME, event.name());
				partnerEventService.publishEvent(eventData);
			}
		}
	}
	
	public void savePdPreferences(PdPreferencesDTO pdPreferencesDTO, Long householdId, boolean saveProviderOnly) {		
		String providers = pdPreferencesDTO.getProviders();
		String prescriptions = pdPreferencesDTO.getPrescriptions();
		
		pdPreferencesDTO.setProviders("");
		pdPreferencesDTO.setPrescriptions("");
		
		PdPreferences pdPreferences = null;
		PdHousehold pdHousehold = pdHouseholdService.findById(householdId);
		String exchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE);
		if("PHIX".equalsIgnoreCase(exchangeType) || (!"PHIX".equalsIgnoreCase(exchangeType) && pdHousehold.getGiWsPayloadId()==null && pdHousehold.getEligLeadId()!= null)){
			pdPreferences = pdPreferencesService.findPreferencesByEligLeadId(pdHousehold.getEligLeadId());
		}else{
			pdPreferences = pdPreferencesService.findPreferencesByPdHouseholdId(pdHousehold.getId());
			if(pdPreferences == null){
				pdPreferences = new PdPreferences();
				pdPreferences.setPdHouseholdId(householdId);
			}
		}
					
		pdPreferences.setDoctors(providers);

		if(!saveProviderOnly){
		pdPreferences.setPrescriptions(prescriptions);			
			String planPref = platformGson.toJson(pdPreferencesDTO);
		pdPreferences.setPreferences(planPref);
		}		
		pdPreferencesService.save(pdPreferences);		
	}
	
	public PdResponse createPdHousehold(EligLead eligLead, InsuranceType insuranceType, String effectiveStartDate) {
		try {
			PdResponse pdResponse = new PdResponse();
			
			Date coverageStartDate = eligLead.getCoverageStartDate();
			if(effectiveStartDate != null){
				coverageStartDate = DateUtil.StringToDate(effectiveStartDate, "MM/dd/yyyy");
			}
			
			PdHousehold pdHousehold = pdHouseholdService.saveEligibility(eligLead,insuranceType, coverageStartDate);
			PdHouseholdDTO pdHouseholdDTO = new PdHouseholdDTO();
			createDTOBean.copyProperties(pdHouseholdDTO, pdHousehold);
			pdResponse.setPdHouseholdDTO(pdHouseholdDTO);
			
			PdPreferences pdPreferences = pdPreferencesService.findPreferencesByEligLeadId(pdHousehold.getEligLeadId());
			PdPreferencesDTO pdPreferencesDTO = planDisplayUtil.getPdPreferencesDTO(pdPreferences);
			pdResponse.setPdPreferencesDTO(pdPreferencesDTO);
			
			return pdResponse;
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occured in createPdHousehold---->", e, false);
			return null;
		}
	}
	
	public PdResponse createPdHousehold(GroupData groupData, InsuranceType insuranceType, String effectiveStartDate) {
		try {
			PdResponse pdResponse = new PdResponse();
			
			Date coverageStartDate = null;
			if(effectiveStartDate != null){
				coverageStartDate = DateUtil.StringToDate(effectiveStartDate, "MM/dd/yyyy");
			}
			
			PdHousehold pdHousehold = pdHouseholdService.saveEligibility(groupData,insuranceType, coverageStartDate);
			PdHouseholdDTO pdHouseholdDTO = new PdHouseholdDTO();
			createDTOBean.copyProperties(pdHouseholdDTO, pdHousehold);
			pdResponse.setPdHouseholdDTO(pdHouseholdDTO);
			PdPreferences pdPreferences = pdPreferencesService.findPreferencesByPdHouseholdId(pdHousehold.getId());
			PdPreferencesDTO pdPreferencesDTO = planDisplayUtil.getPdPreferencesDTO(pdPreferences);
			pdResponse.setPdPreferencesDTO(pdPreferencesDTO);
			
			return pdResponse;
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occured in createPdHousehold---->", e, false);
			return null;
		}
	}
	
	public List<IndividualPlan> setRecommendationRank(List<IndividualPlan> individualPlans, String currentPlanId, Date coverageStartDate, ExchangeType exchangeType) {
		float lowestPremium = 0;
		float lowestPlanPremiumForLevel = 0;
		int highestGps = 0;
		Integer lowestPremiumForLevelPlanId = null;
		Integer lowestPremiumPlanId = null;
		Integer highestGpsPlanId = null;
		
		boolean currentPlanAvailable = false;
		String currentPlanIssuer = null;
		
		// Default level of RANK 2 is SILVER, overwrite it with current plan level if available
		String planLevel = "SILVER";
		
		// If currentPlanId is present in result set then overwrite plan level, set RANK 1 and set currentPlanAvailable FLAG true
		if(currentPlanId != null && currentPlanId.length() == 14){
			currentPlanIssuer = currentPlanId.length() >= 5 ? currentPlanId.substring(0,5) : null;
			
			for(IndividualPlan individualPlan : individualPlans){
				if(individualPlan.getIssuerPlanNumber() != null){
					String hiosPlanId = individualPlan.getIssuerPlanNumber().length() >= 14 ? individualPlan.getIssuerPlanNumber().substring(0, 14) : null;
					if(currentPlanId.equalsIgnoreCase(hiosPlanId) && individualPlan.getRecommendationRank() == null){
						individualPlan.setRecommendationRank(1);
						planLevel = individualPlan.getLevel();
						currentPlanIssuer = individualPlan.getIssuerPlanNumber().length()  >= 5 ? individualPlan.getIssuerPlanNumber().substring(0,5) : null;
						currentPlanAvailable = true;
						break;
					}
				}
			}
			if(!currentPlanAvailable){
				String issuerPlanNumber = "";
				if(ExchangeType.ON.equals(exchangeType)){
					issuerPlanNumber = currentPlanId.concat("01");
				}else{
					issuerPlanNumber = currentPlanId.concat("00");
				}
				Plan planObj = pdOrderItemService.getPlanInfoByCmsPlanId(issuerPlanNumber, DateUtil.dateToString(coverageStartDate, "yyyy-MM-dd"));
				if(planObj != null && planObj.getPlanHealth() != null && planObj.getPlanHealth().getPlanLevel() != null){
					planLevel = planObj.getPlanHealth().getPlanLevel();
				}
			}
		} 
		
		boolean moreThanOneIssuerPresent = false;
		if(currentPlanIssuer != null){
			for(IndividualPlan individualPlan : individualPlans){
				if(individualPlan.getIssuerPlanNumber() != null && individualPlan.getIssuerPlanNumber().length() >= 5){
				String issuerHiosId = individualPlan.getIssuerPlanNumber().substring(0,5);
				if(!currentPlanIssuer.equalsIgnoreCase(issuerHiosId)){
					moreThanOneIssuerPresent = true;
				}
			}
		}
		}
		
		if(!moreThanOneIssuerPresent){
			currentPlanIssuer = null;
		}
		
		// Recommend plans from issuers other than current issuer
		// Get lowestPremiumForLevelPlanId, lowestPremiumPlanId and highestGpsPlanId
		for(IndividualPlan individualPlan : individualPlans){
			if(PlanDisplayUtil.Y.equalsIgnoreCase(individualPlan.getIsPuf())){
				continue;
			}
			
			if(currentPlanIssuer != null && currentPlanIssuer.equalsIgnoreCase(individualPlan.getIssuerPlanNumber().substring(0,5))){
				continue;
			}
			
			if(planLevel.equalsIgnoreCase(individualPlan.getLevel())){
				if(lowestPlanPremiumForLevel == 0){
					lowestPlanPremiumForLevel = individualPlan.getPremiumBeforeCredit();
					lowestPremiumForLevelPlanId = individualPlan.getPlanId();
				}
				if(lowestPlanPremiumForLevel > individualPlan.getPremiumBeforeCredit()){
					lowestPlanPremiumForLevel = individualPlan.getPremiumBeforeCredit();
					lowestPremiumForLevelPlanId = individualPlan.getPlanId();
				}
			}
			
			if(lowestPremium == 0){
				lowestPremium = individualPlan.getPremiumBeforeCredit();
				lowestPremiumPlanId = individualPlan.getPlanId();
			}
			if(lowestPremium > individualPlan.getPremiumBeforeCredit()){
				lowestPremium = individualPlan.getPremiumBeforeCredit();
				lowestPremiumPlanId = individualPlan.getPlanId();
			}
			
			if(individualPlan.getSmartScore() != null && individualPlan.getSmartScore().intValue() != 0){
				if(highestGps == 0){
					highestGps = individualPlan.getSmartScore();
					highestGpsPlanId = individualPlan.getPlanId();
				}
				if(highestGps < individualPlan.getSmartScore()){
					highestGps = individualPlan.getSmartScore();
					highestGpsPlanId = individualPlan.getPlanId();
				}
			}
			
		}
		
		// If currentPlanId is NOT available then set lowest premium plan as RANK1 
		// If the Rank is already present then do not overwrite it
		if(!currentPlanAvailable){
			for(IndividualPlan individualPlan : individualPlans){
				if(PlanDisplayUtil.Y.equalsIgnoreCase(individualPlan.getIsPuf())){
					continue;
				}
				if(lowestPremiumPlanId != null && individualPlan.getPlanId() != null && lowestPremiumPlanId.intValue() == individualPlan.getPlanId().intValue() && individualPlan.getRecommendationRank() == null){
					individualPlan.setRecommendationRank(1);
					break;
				}
			}
		}
		
		// Set RANK 2 for the plan with lowestPremiumForLevelPlanId
		// If the Rank is already present then do not overwrite it
		if(lowestPremiumForLevelPlanId != null){
			for(IndividualPlan individualPlan : individualPlans){
				if(PlanDisplayUtil.Y.equalsIgnoreCase(individualPlan.getIsPuf())){
					continue;
				}
				if(individualPlan.getPlanId() != null && lowestPremiumForLevelPlanId.intValue() == individualPlan.getPlanId().intValue() && individualPlan.getRecommendationRank() == null){
					individualPlan.setRecommendationRank(2);
					break;
				}
			}
		}
		
		// Set highest GPS plan as RANK3
		// If the Rank is already present then do not overwrite it
		if(highestGpsPlanId != null){
			for(IndividualPlan individualPlan : individualPlans){
				if(PlanDisplayUtil.Y.equalsIgnoreCase(individualPlan.getIsPuf())){
					continue;
				}
				if(individualPlan.getPlanId() != null && highestGpsPlanId.intValue() == individualPlan.getPlanId().intValue() && individualPlan.getRecommendationRank() == null){
					individualPlan.setRecommendationRank(3);
					break;
				}
			}
		}

		return individualPlans;
	}
	
	public List<IndividualPlan> filterNonRecommendedPlans(List<IndividualPlan> allPlans) {
		List<IndividualPlan> recomemmendedPlans = new ArrayList<IndividualPlan>();
		for(IndividualPlan individualPlan : allPlans){
			if(individualPlan.getRecommendationRank() != null){
				recomemmendedPlans.add(individualPlan);
			}
		}
		return recomemmendedPlans;
	}
	
	
	public String getCrossWalkIssuerPlanId(String zipCode,String countyCode,String coverageDate,String isEligibleForCatastrophic,String renewalPlanId,String csrValue) throws GIException
	{
		String hiosPlanId = this.getHiosPlanIdFromCrossWalkApi(renewalPlanId, zipCode, countyCode, coverageDate, isEligibleForCatastrophic);
		if (hiosPlanId == null || hiosPlanId.isEmpty()) {
			throw new GIException(GhixErrors.ERROR_WHILE_CALLING_CROSS_WALK_API);
		}
		String currentCoverageYear = coverageDate.substring(0,4);
		String lastYearCoverageDate = (Integer.parseInt(currentCoverageYear) - 1)+"-01-01";
		
		Plan renewlPlan = pdOrderItemService.getPlanInfoByCmsPlanId(renewalPlanId, lastYearCoverageDate);
		String planLevel = "";
		try{
			if ("HEALTH".equals(renewlPlan.getInsuranceType()))
			{
				planLevel = renewlPlan.getPlanHealth().getPlanLevel();
			}
			else if ("DENTAL".equals(renewlPlan.getInsuranceType()))
			{
				planLevel = renewlPlan.getPlanDental().getPlanLevel();
			}
		}catch(Exception ex){
			throw new GIException(GhixErrors.PLAN_LEVEL_NOT_FOUND);
		}
		
		if(StringUtils.isBlank(planLevel)){
			throw new GIException(GhixErrors.PLAN_LEVEL_NOT_FOUND);
		}
		boolean catastrophicEligibile = false;
		if("Y".equals(isEligibleForCatastrophic))
		{
			catastrophicEligibile = true;
		}
		String issuerPlanId = this.appendCSRtoHiosPlanId(hiosPlanId, csrValue, planLevel, catastrophicEligibile).toString();
		
		if (issuerPlanId == null ||  issuerPlanId.isEmpty()) {
			throw new GIException(GhixErrors.ERROR_WHILE_APPENDING_CSR_TO_HIOSID);
		}
		return issuerPlanId;
	}
	
	private String getHiosPlanIdFromCrossWalkApi(String hiosPlanId, String zip, String countyCode, String effectiveDate, String isEligibleForCatastrophic) throws GIException
	{
		PlanCrossWalkRequestDTO planCrossWalkRequest = new PlanCrossWalkRequestDTO();
		planCrossWalkRequest.setHiosPlanNumber(hiosPlanId);
		planCrossWalkRequest.setZip(zip);
		planCrossWalkRequest.setCountyCode(countyCode);
		planCrossWalkRequest.setEffectiveDate(effectiveDate);
		planCrossWalkRequest.setIsEligibleForCatastrophic(isEligibleForCatastrophic);
		
		String response = planRestController.getCrosswalkHiosPlanId(planCrossWalkRequest);
		PlanCrossWalkResponseDTO planCrossWalkResponse = platformGson.fromJson(response, PlanCrossWalkResponseDTO.class);
		if(planCrossWalkResponse.getIsAvailable() != null && "Y".equals(planCrossWalkResponse.getIsAvailable())) {
			return planCrossWalkResponse.getHiosPlanNumber();
		}
		return null;
	}
	
	private StringBuilder appendCSRtoHiosPlanId(String hiosPlanId, String csrValue, String planTier, boolean isCatastrophic)
	{
		  StringBuilder issuerPlanId = null;
		  if(hiosPlanId != null && !hiosPlanId.isEmpty())
		  {
			  issuerPlanId = new StringBuilder(hiosPlanId);
			  if(!"".equals(csrValue) && !"CS1".equals(csrValue))
			  {
				  String csrNumber = "";
				  if("CS2".equalsIgnoreCase(csrValue)) {
					  csrNumber = "02";
				  }
				  else if("CS3".equalsIgnoreCase(csrValue)) {
					  csrNumber = "03";
				  }
				  else if("CS4".equalsIgnoreCase(csrValue)) {
					  csrNumber = "04";
				  }
				  else if("CS5".equalsIgnoreCase(csrValue)) {
					  csrNumber = "05";
				  }
				  else if("CS6".equalsIgnoreCase(csrValue)) {
					  csrNumber = "06";
				  }
				  if(!"".equals(planTier) && "SILVER".equalsIgnoreCase(planTier)) {
					  issuerPlanId.append(csrNumber);
				  }else if("CATASTROPHIC".equalsIgnoreCase(planTier) && isCatastrophic) {
					  issuerPlanId.append("01");
				  }
				  else if("02".equals(csrNumber) || "03".equals(csrNumber)) {
					  issuerPlanId.append(csrNumber);
				  }
				  else{
					  issuerPlanId.append("01");
				  }
			  }	 
			  else {
				  issuerPlanId.append("01");
			  }
			  return issuerPlanId;
		  }
		  return null;
	}
	
	public CostSharing getCostSharing(String csrNumber){
		CostSharing costSharing = null;

		if("01".equalsIgnoreCase(csrNumber)) {
			costSharing = CostSharing.CS1;
		}
		else if("02".equalsIgnoreCase(csrNumber)) {
			costSharing = CostSharing.CS2;
		}
		else if("03".equalsIgnoreCase(csrNumber)) {
			costSharing = CostSharing.CS3;
		}
		else if("04".equalsIgnoreCase(csrNumber)) {
			costSharing = CostSharing.CS4;
		}
		else if("05".equalsIgnoreCase(csrNumber)) {
			costSharing = CostSharing.CS5;
		}
		else if("06".equalsIgnoreCase(csrNumber)) {
			costSharing = CostSharing.CS6;
		}
		return costSharing;
	}
	
	public PdResponsePlanAvailability getPlanAvailability(PdRequestPlanAvailability pdRequestPlanAvailability) {
    	PdResponsePlanAvailability pdResponsePlanAvailability = new PdResponsePlanAvailability();
		
		try{
			String requestValidate = this.pdRequestPlanValidate(pdRequestPlanAvailability);			
			if(requestValidate !=null && !requestValidate.equalsIgnoreCase(PlanDisplayConstants.SUCCESS))
			{
				pdResponsePlanAvailability.setErrCode(PlanDisplayConstants.ERRORCODE1);
				pdResponsePlanAvailability.setResponse(requestValidate);
				return pdResponsePlanAvailability;
			}
			
			String healthSubscriberId = null;
			String dentalSubscriberId = null;
			List<IndividualPlan> healthPlanList = null;
			List<IndividualPlan> dentalPlanList = null;
			Long healthEnrollmentID = null;
			Long dentalEnrollmentID = null;
			ExchangeType exchangeType = ExchangeType.ON;
			
			Date coverageStartDate = DateUtil.StringToDate(pdRequestPlanAvailability.getCoverageDate(), "MM/dd/yyyy");
			PdHouseholdDTO  pdHouseholdDTO = this.populateHouseholdDtoFromPdRequest(pdRequestPlanAvailability);		
			
			Map<String,String> memberCoverageDataMap = this.getMemberCoverageDate(pdRequestPlanAvailability.getPdPersonCRDTOList(), pdRequestPlanAvailability.getCoverageDate());
						
			boolean isAdultInExistingMembers = this.isAdultPresentInExistingMemberList(pdRequestPlanAvailability.getPdPersonCRDTOList(), memberCoverageDataMap);
			List<PdPersonDTO> pdPersonDTOList = this.populatePdPersonDTOFromPdPersonRequest(pdRequestPlanAvailability.getPdPersonCRDTOList(), coverageStartDate, isAdultInExistingMembers, memberCoverageDataMap);

			if (pdPersonDTOList != null && !pdPersonDTOList.isEmpty()) {
				for(PdPersonDTO pdPersonDTO: pdPersonDTOList) {
					if(pdPersonDTO.getId() != null && (pdPersonDTO.getHealthCoverageStartDate()!=null || pdPersonDTO.getDentalCoverageStartDate()!=null)){
						PdPerson pdPerson = pdPersonService.findById(pdPersonDTO.getId());
						if(pdPersonDTO.getHealthCoverageStartDate()!=null){
							pdPerson.setHealthCoverageStartDate(pdPersonDTO.getHealthCoverageStartDate());
						}
						if(pdPersonDTO.getDentalCoverageStartDate()!=null){
							pdPerson.setDentalCoverageStartDate(pdPersonDTO.getDentalCoverageStartDate());
						}
						pdPersonService.savePerson(pdPerson);
					}
				}
			}	
			
			for (PdPersonDTO pdPersonDTO : pdPersonDTOList) {
				if(healthEnrollmentID == null) {
					healthEnrollmentID = pdPersonDTO.getHealthEnrollmentId();
				}
				if(dentalEnrollmentID == null) {
					dentalEnrollmentID = pdPersonDTO.getDentalEnrollmentId();
				}
			}
			
			if(healthEnrollmentID != null){
				long time = System.currentTimeMillis();
				EnrollmentResponse enrollmentResponse = planDisplayUtil.findPlanIdAndOrderItemIdByEnrollmentId(Long.toString(healthEnrollmentID));
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.WARN, "received findPlanIdAndOrderItemIdByEnrollmentId response for EnrollmentId " + healthEnrollmentID + " in " + (System.currentTimeMillis() - time) + " ms", null, false);
				if(enrollmentResponse == null || !enrollmentResponse.getStatus().equals(PlanDisplayConstants.SUCCESS)){
					pdResponsePlanAvailability.setErrCode(PlanDisplayConstants.ERRORCODE1);
					pdResponsePlanAvailability.setResponse(PlanDisplayConstants.ENROLLMENTRECORD_NOTFOUND);			
					return pdResponsePlanAvailability;
				}
				if(enrollmentResponse.getPlanId() != null)	{
					String initialHealthCmsPlanId = enrollmentResponse.getCmsPlanId();
					boolean isEligibleForCatastrophic = true;
					for(PdPersonDTO pdPersonDTO : pdPersonDTOList){
						if(YorN.N.equals(pdPersonDTO.getCatastrophicEligible())){
							isEligibleForCatastrophic = false;
						}
					}
					
					long startTime = System.currentTimeMillis();
					Plan planDetailObj = pdOrderItemService.getPlanInfoByCmsPlanId(initialHealthCmsPlanId, DateUtil.dateToString(coverageStartDate, "yyyy-MM-dd"));
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.WARN, "received getPlanInfoByCmsPlanId response for EnrollmentId " + healthEnrollmentID + " in " + (System.currentTimeMillis() - startTime) + " ms", null, false);
					
					if(pdHouseholdDTO.getCostSharing().toString()!=null && planDetailObj.getPlanHealth() != null && planDetailObj.getPlanHealth().getPlanLevel() != null ){
						String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
						boolean isCAProfile = stateCode != null ? stateCode.equalsIgnoreCase("CA") : false;
						if((!isCAProfile) || (isCAProfile && "SILVER".equalsIgnoreCase(planDetailObj.getPlanHealth().getPlanLevel())) ){
							initialHealthCmsPlanId = appendHiosPlanIdWithCSR(initialHealthCmsPlanId, pdHouseholdDTO.getCostSharing().toString(), pdHouseholdDTO.getId(), planDetailObj.getPlanHealth().getPlanLevel(), isEligibleForCatastrophic);
							String newCostSharing = initialHealthCmsPlanId.substring((initialHealthCmsPlanId.length() - 2), initialHealthCmsPlanId.length());
							CostSharing costSharingChange = this.getCostSharing(newCostSharing);
							if(!costSharingChange.equals(pdHouseholdDTO.getCostSharing())){
								pdHouseholdDTO.setCostSharing(costSharingChange);
							}
						}						
					}
					
					String initialHealthSubscriberId = enrollmentResponse.getSubscriberMemberId();
					PdQuoteRequest pdQuoteRequest = this.createPdQuoteRequest(pdHouseholdDTO, pdPersonDTOList, initialHealthSubscriberId, initialHealthCmsPlanId, pdRequestPlanAvailability.getEnrollmentType(), InsuranceType.HEALTH, exchangeType);
					long startNewTime = System.currentTimeMillis();
					healthPlanList = individualPlanService.getPlans(pdQuoteRequest);
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.WARN, "received getPlans response for EnrollmentId " + healthEnrollmentID + " in " + (System.currentTimeMillis() - startNewTime) + " ms", null, false);
					Map<String,String> subscriberData = individualPlanService.getSubscriberData(pdPersonDTOList, pdHouseholdDTO, pdQuoteRequest.getInitialSubscriberId(), pdQuoteRequest.getSpecialEnrollmentFlowType(), pdQuoteRequest.getInsuranceType());
					healthSubscriberId = subscriberData.get(GhixConstants.SUBSCRIBER_MEMBER_ID);	
					pdHouseholdDTO.setHealthSubsidy(pdHouseholdDTO.getMaxSubsidy());
					if(healthPlanList != null && !healthPlanList.isEmpty()){			
						IndividualPlan healthPlan = healthPlanList.get(0);
						if(pdHouseholdDTO.getMaxSubsidy() != null && pdHouseholdDTO.getMaxSubsidy() != 0){
							pdHouseholdDTO.setHealthSubsidy(healthPlan.getAptc());
						}
						if(pdHouseholdDTO.getMaxStateSubsidy() != null && pdHouseholdDTO.getMaxStateSubsidy().compareTo(BigDecimal.ZERO) > 0){
							pdHouseholdDTO.setStateSubsidy(healthPlan.getStateSubsidy());
						}
					}
				}
			}
			
			if(dentalEnrollmentID != null) {	
				long time = System.currentTimeMillis();
				EnrollmentResponse dentEnrollmentResponse = planDisplayUtil.findPlanIdAndOrderItemIdByEnrollmentId(Long.toString(dentalEnrollmentID));
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.WARN, "received findPlanIdAndOrderItemIdByEnrollmentId response for EnrollmentId " + dentalEnrollmentID + " in " + (System.currentTimeMillis() - time) + " ms", null, false);
				if(dentEnrollmentResponse==null || !dentEnrollmentResponse.getStatus().equals(PlanDisplayConstants.SUCCESS)){
					pdResponsePlanAvailability.setErrCode(PlanDisplayConstants.ERRORCODE1);
					pdResponsePlanAvailability.setResponse(PlanDisplayConstants.ENROLLMENTRECORD_NOTFOUND);			
					return pdResponsePlanAvailability;
				}
				if(dentEnrollmentResponse.getPlanId() != null)	{
					String initialDentalCmsPlanId = dentEnrollmentResponse.getCmsPlanId();
					String initialDentalSubscriberId = dentEnrollmentResponse.getSubscriberMemberId();
					pdHouseholdDTO.setDentalSubsidy(null);

					String applyAptcToDentalConfig = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.APPLY_APTC_TO_DENTAL);

					boolean applyAptcToDental = !applyAptcToDentalConfig.equalsIgnoreCase("false");

					if(pdHouseholdDTO.getMaxSubsidy() != null && pdHouseholdDTO.getMaxSubsidy() != 0 && applyAptcToDental){
						Float healthSubsidy = pdHouseholdDTO.getHealthSubsidy() != null ? pdHouseholdDTO.getHealthSubsidy() : 0;
						pdHouseholdDTO.setDentalSubsidy(new BigDecimal(Float.toString(pdHouseholdDTO.getMaxSubsidy())).subtract(new BigDecimal(Float.toString(healthSubsidy))).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue());
					}
					PdQuoteRequest pdQuoteRequest = this.createPdQuoteRequest(pdHouseholdDTO, pdPersonDTOList, initialDentalSubscriberId, initialDentalCmsPlanId, pdRequestPlanAvailability.getEnrollmentType(), InsuranceType.DENTAL, exchangeType);
					long startNewTime = System.currentTimeMillis();
					dentalPlanList = individualPlanService.getPlans(pdQuoteRequest);
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.WARN, "received getPlans response for EnrollmentId " + dentalEnrollmentID + " in " + (System.currentTimeMillis() - startNewTime) + " ms", null, false);
					
					List<PdPersonDTO> seekingCovgPersonList = planDisplayUtil.getSeekingCovgPersonList(pdQuoteRequest.getPdPersonDTOList());
					Map<String,String> subscriberData = individualPlanService.getSubscriberData(seekingCovgPersonList, pdHouseholdDTO, pdQuoteRequest.getInitialSubscriberId(), pdQuoteRequest.getSpecialEnrollmentFlowType(), pdQuoteRequest.getInsuranceType());
					dentalSubscriberId = subscriberData.get(GhixConstants.SUBSCRIBER_MEMBER_ID);
				} 
			}
						
			if((healthPlanList == null || healthPlanList.isEmpty()) && (dentalPlanList == null || dentalPlanList.isEmpty())){
				pdResponsePlanAvailability.setErrCode(PlanDisplayConstants.ERRORCODE2);
				pdResponsePlanAvailability.setResponse(PlanDisplayConstants.HEALTH_AND_DENTAL_PLAN_NOTFOUND);			
				return pdResponsePlanAvailability;
			}
			
			pdResponsePlanAvailability.setErrCode(PlanDisplayConstants.SUCCESS_CODE);
			pdResponsePlanAvailability.setResponse(PlanDisplayConstants.SUCCESS);
			
			List<PdOrderItemResponse> pdOrderItemResponseList = new ArrayList<PdOrderItemResponse>();		
			List<PdOrderItemPersonResponse> healthPdOrderItemPersonResponseList = new  ArrayList<PdOrderItemPersonResponse>();
			if(healthEnrollmentID!=null){
				if(healthPlanList != null && !healthPlanList.isEmpty()){			
					IndividualPlan healthPlan = healthPlanList.get(0);
					long planId = healthPlan.getPlanId();
					PdOrderItemResponse healthPdOrderItemResponse = new PdOrderItemResponse();
					healthPdOrderItemResponse.setPlanId(planId);
					healthPdOrderItemResponse.setPremiumAfterCredit(healthPlan.getPremiumAfterCredit());
					healthPdOrderItemResponse.setPremiumBeforeCredit(healthPlan.getPremiumBeforeCredit());
					healthPdOrderItemResponse.setInsuranceType(InsuranceType.HEALTH);
					healthPdOrderItemResponse.setMonthlySubsidy(healthPlan.getAptc());
					healthPdOrderItemResponse.setMonthlyStateSubsidy(healthPlan.getStateSubsidy());
					
					if(healthPlan.getPlanDetailsByMember()!= null && !healthPlan.getPlanDetailsByMember().isEmpty()){			
						for(Map<String, String> planDetailsByMemberMap : healthPlan.getPlanDetailsByMember()){
							PdOrderItemPersonResponse pdOrderItemPersonResponse = new PdOrderItemPersonResponse();
							int region = Integer.parseInt(planDetailsByMemberMap.get(PlanDisplayConstants.REGION));
							String memberGUID = planDetailsByMemberMap.get(PlanDisplayConstants.ID);
							float premium = Float.parseFloat(planDetailsByMemberMap.get(PlanDisplayConstants.MEMBER_PREMIUM));
							if(planDetailsByMemberMap.get(PlanDisplayConstants.PERSON_COVG_DATE) != null){
								pdOrderItemPersonResponse.setCoverageStartDate(planDetailsByMemberMap.get(PlanDisplayConstants.PERSON_COVG_DATE));
							}						
							pdOrderItemPersonResponse.setMemberGUID(memberGUID);
							pdOrderItemPersonResponse.setRegion(region);
							pdOrderItemPersonResponse.setPremium(premium);
							if(healthSubscriberId !=null && healthSubscriberId.equalsIgnoreCase(planDetailsByMemberMap.get(PlanDisplayConstants.ID))){
								pdOrderItemPersonResponse.setSubscriberFlag(YorN.Y);
							}else{
								pdOrderItemPersonResponse.setSubscriberFlag(YorN.N);
							}
							healthPdOrderItemPersonResponseList.add(pdOrderItemPersonResponse);
						}
					}
					
					healthPdOrderItemResponse.setPdOrderItemPersonResponseList(healthPdOrderItemPersonResponseList);
					pdOrderItemResponseList.add(healthPdOrderItemResponse);			
				}else{
					pdResponsePlanAvailability.setErrCode(PlanDisplayConstants.ERRORCODE3);
					pdResponsePlanAvailability.setResponse(PlanDisplayConstants.HEALTH_PLAN_NOTFOUND);					
				}
			}
						
			List<PdOrderItemPersonResponse> dentalPdOrderItemPersonResponseList = new  ArrayList<PdOrderItemPersonResponse>();
			if(dentalEnrollmentID!=null){
				if(dentalPlanList !=null && !dentalPlanList.isEmpty()){
					IndividualPlan dentalPlan = dentalPlanList.get(0);
					long planId = dentalPlan.getPlanId();
					PdOrderItemResponse dentalPdOrderItemResponse = new PdOrderItemResponse();
					dentalPdOrderItemResponse.setPlanId(planId);
					dentalPdOrderItemResponse.setPremiumAfterCredit(dentalPlan.getPremiumAfterCredit());
					dentalPdOrderItemResponse.setPremiumBeforeCredit(dentalPlan.getPremiumBeforeCredit());
					dentalPdOrderItemResponse.setInsuranceType(InsuranceType.DENTAL);
					String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
					boolean isCAProfile = stateCode != null ? stateCode.equalsIgnoreCase("CA") : false;
					if(isCAProfile){
						dentalPdOrderItemResponse.setMonthlySubsidy(null);
					}else{
						dentalPdOrderItemResponse.setMonthlySubsidy(dentalPlan.getAptc());
					}
					
					if(dentalPlan.getPlanDetailsByMember()!=null && !dentalPlan.getPlanDetailsByMember().isEmpty()){			
						for(Map<String, String> planDetailsByMemberMap : dentalPlan.getPlanDetailsByMember()){
							PdOrderItemPersonResponse pdOrderItemPersonResponse = new PdOrderItemPersonResponse();
							int region = Integer.parseInt(planDetailsByMemberMap.get(PlanDisplayConstants.REGION));
							String memberGUID = planDetailsByMemberMap.get(PlanDisplayConstants.ID);
							float premium = Float.parseFloat(planDetailsByMemberMap.get(PlanDisplayConstants.MEMBER_PREMIUM));
							if(planDetailsByMemberMap.get(PlanDisplayConstants.PERSON_COVG_DATE) != null){
								pdOrderItemPersonResponse.setCoverageStartDate(planDetailsByMemberMap.get(PlanDisplayConstants.PERSON_COVG_DATE));
							}	
							pdOrderItemPersonResponse.setMemberGUID(memberGUID);
							pdOrderItemPersonResponse.setRegion(region);
							pdOrderItemPersonResponse.setPremium(premium);
							if(dentalSubscriberId != null && dentalSubscriberId.equalsIgnoreCase(planDetailsByMemberMap.get(PlanDisplayConstants.ID))){
								pdOrderItemPersonResponse.setSubscriberFlag(YorN.Y);
							}else{
								pdOrderItemPersonResponse.setSubscriberFlag(YorN.N);
							}
							dentalPdOrderItemPersonResponseList.add(pdOrderItemPersonResponse);
						}
					}
					
					dentalPdOrderItemResponse.setPdOrderItemPersonResponseList(dentalPdOrderItemPersonResponseList);
					pdOrderItemResponseList.add(dentalPdOrderItemResponse);
				}else{
					pdResponsePlanAvailability.setErrCode(PlanDisplayConstants.ERRORCODE4);
					pdResponsePlanAvailability.setResponse(PlanDisplayConstants.DENTAL_PLAN_NOTFOUND);			
				}
			}
						
			pdResponsePlanAvailability.setPdOrderItemResponse(pdOrderItemResponseList);
		
			return pdResponsePlanAvailability;
		}catch(Exception e){
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occured while executing planAvailability method---->", e, false);
			pdResponsePlanAvailability.setErrCode(PlanDisplayConstants.ERRORCODE5);
			pdResponsePlanAvailability.setResponse(PlanDisplayConstants.TECHNICAL_ISSUE_FOUND);
			pdResponsePlanAvailability.setNestedStackTrace(ExceptionUtils.getFullStackTrace(e));
			return pdResponsePlanAvailability;
		}
	}

	public String getCatastrophicEligibility(List<PdPersonDTO> pdPersonDTOList) {
		String isEligibleForCatastrophic = "Y";
		for(PdPersonDTO pdPersonDTO : pdPersonDTOList){
			if(YorN.N.equals(pdPersonDTO.getCatastrophicEligible())){
				isEligibleForCatastrophic = YorN.N.name();
			}
		}
		return isEligibleForCatastrophic;
	}
	
	public String appendHiosPlanIdWithCSR(String initialHealthCmsPlanId, String costSharing, Long householdId, String planLevel, boolean isCatastrophic){		
		String hiosPlanId=initialHealthCmsPlanId.substring(0, (initialHealthCmsPlanId.length() - 2));			
		initialHealthCmsPlanId =  this.appendCSRtoHiosPlanId(hiosPlanId, costSharing, planLevel,isCatastrophic).toString();
		String newCostSharing = initialHealthCmsPlanId.substring((initialHealthCmsPlanId.length() - 2), initialHealthCmsPlanId.length());
		CostSharing costSharingChange = this.getCostSharing(newCostSharing);
		if(!costSharingChange.equals(CostSharing.valueOf(costSharing))){
			if(householdId != null){
				pdHouseholdService.updateCostSharing(householdId, costSharingChange);
			}
		}
		
		return initialHealthCmsPlanId;
	}
	
	public String calculatePlanScoreForPreferences(PdPreferencesDTO pdPreferencesDTO, List<PlanPremiumDTO> planPremiumList, int noOfMembers, String zipCode, String coverageStartDate, String currentHiosId){
		String responseStr = "";
		Map<String, Integer> personalUsageForMedicalMap = individualPlanService.formMedicalUsageMap(pdPreferencesDTO, noOfMembers);
		Map<String, Integer> personalUsageForDrugsMap = individualPlanService.formDrugsUsageMap(pdPreferencesDTO, noOfMembers);
				
		List<ProviderBean> providers = getProviderListFromJson(pdPreferencesDTO.getProviders());
		Map<String, Future<Map<String, Object>>> futureProviderResponseListMap = prepareProviderHiosMap(providers);
					
		List<String> hiosPlanIds = getHiosPlanIds(planPremiumList);
			
		PrescriptionSearch search = prescriptionFactory.getObject();							
		List<DrugDTO> drugList = new ArrayList<DrugDTO>();
		
		if(StringUtils.isNotBlank(pdPreferencesDTO.getPrescriptions())){
			drugList = planDisplayUtil.parsePrescriptionJson(pdPreferencesDTO.getPrescriptions());
		}			
		
		int coverageYear = PlanDisplayUtil.extractYearFromDate(coverageStartDate);
		
		Map<String, Future<PrescriptionData>> futurePlanAvailabilityListMap = prepareFuturePlanAvailabilityMap(drugList, hiosPlanIds, search, zipCode,coverageYear);
		
		List<PlanRateBenefit> planRateBenefitList = planDisplayUtil.getPlanRateBenefitsByplanId(hiosPlanIds, PlanDisplayEnum.InsuranceType.HEALTH.toString(), coverageStartDate);
		
		if(planRateBenefitList!=null && !planRateBenefitList.isEmpty()){
			Map<Long, Double> costCompareData = pdOrderItemService.getComparisonCosts(planRateBenefitList, personalUsageForMedicalMap, personalUsageForDrugsMap, pdPreferencesDTO, coverageStartDate, currentHiosId);
			
			setSbcScenarioDTO(pdPreferencesDTO.getMedicalProcedure(), hiosPlanIds, coverageStartDate, planRateBenefitList);
				
			Map<String, List<DrugDTO>> planDrugListMap = prepareDrugListMap(search, drugList, hiosPlanIds, futurePlanAvailabilityListMap);
														
			List<IndividualPlan> individualPlans = planDisplayUtil.getEstimatedCosts(planRateBenefitList, costCompareData, planDrugListMap, pdPreferencesDTO, PlanDisplayEnum.InsuranceType.HEALTH, noOfMembers, planPremiumList, providers, futureProviderResponseListMap, GhixConstants.helathBenefitList, coverageStartDate);
				
			Integer doctorsSelected = providers != null && providers.size() > 0 ? providers.size() : 0;	
			Integer drugsSelected = drugList != null && drugList.size() > 0 ? drugList.size() : 0;
			planDisplayUtil.calculateSmartScore(pdPreferencesDTO.getBenefits(), individualPlans, personalUsageForMedicalMap, personalUsageForDrugsMap, doctorsSelected, drugsSelected);
			planDisplayUtil.setBenefitCoverage(individualPlans, planRateBenefitList);
			List<String> benefitList = pdPreferencesDTO.getBenefits();			
			planDisplayUtil.calculateExpenseEstimate(individualPlans);
				
			String planScoreResponse = planDisplayUtil.generatePlanScoreResponse(individualPlans, providers, benefitList, hiosPlanIds);
			responseStr = planScoreResponse;			
		}
		
		return responseStr;
	}
	
	public Map<String, Future<PrescriptionData>> prepareFuturePlanAvailabilityMap(List<DrugDTO> drugList, List<String> hiosPlanIds, PrescriptionSearch search, String zipCode,int applicableYear) {
		Map<String, Future<PrescriptionData>> futurePlanAvailabilityListMap = new HashMap<String, Future<PrescriptionData>>();
		if(CollectionUtils.isNotEmpty(drugList)){
			ZipCode zipCodeObj = zipCodeService.findByZipCode(zipCode);  
			PrescriptionRequest planAvailabilityForDrugRequest = new PrescriptionRequest();
			planAvailabilityForDrugRequest.setApplicableYear(applicableYear);
			planAvailabilityForDrugRequest.setState(zipCodeObj.getState());
			planAvailabilityForDrugRequest.setDrugDTOList(drugList);
			planAvailabilityForDrugRequest.setPlanHiosList(hiosPlanIds);
			futurePlanAvailabilityListMap = search.prepareFuturePlanAvailabilityMap(planAvailabilityForDrugRequest);                                                      
		}
		return futurePlanAvailabilityListMap;
	}

	public Map<String, Future<Map<String, Object>>> prepareProviderHiosMap(List<ProviderBean> providers) {
		Map<String, Future<Map<String, Object>>> futureProviderResponseListMap = new HashMap<String, Future<Map<String, Object>>>();
		if(providers != null && providers.size() > 0){
			ProviderSearch providerSearch = providerFactory.getObject();						
			futureProviderResponseListMap = providerSearch.prepareProviderHiosMap(providers);
		}
		return futureProviderResponseListMap;
	}

	public List<ProviderBean> getProviderListFromJson(String providerJson) {
		List<ProviderBean> providers = new ArrayList<ProviderBean>();
		if(StringUtils.isNotBlank(providerJson)){
			try{
				providers = platformGson.fromJson(providerJson, new TypeToken<List<ProviderBean>>() {}.getType());
			}catch(Exception e){}
		}
		return providers;
	}

	public Map<String, List<DrugDTO>> prepareDrugListMap(PrescriptionSearch search, List<DrugDTO> drugList, List<String> hiosPlanIds, Map<String, Future<PrescriptionData>> futurePlanAvailabilityListMap) {
		Map<String, List<DrugDTO>> planDrugListMap = new HashMap<String, List<DrugDTO>>();                                                      
		if(CollectionUtils.isNotEmpty(drugList)){	
			planDrugListMap = search.prepareDrugListMap(drugList, hiosPlanIds, futurePlanAvailabilityListMap);
		}
		return planDrugListMap;
	}

	public List<String> getHiosPlanIds(List<PlanPremiumDTO> planPremiumList) {
		List<String> hiosPlanIds = new ArrayList<String>();
		for(PlanPremiumDTO planPremiumDTO : planPremiumList){
			if(planPremiumDTO.getHiosId() != null){
				hiosPlanIds.add(planPremiumDTO.getHiosId());
			}			
		}	
		return hiosPlanIds;
	}
	
	public void setSbcScenarioDTO(MedicalProcedure medicalProcedure, List<String> hiosPlanIds, String coverageStartDate, List<PlanRateBenefit> planRateBenefitList) {
		if(medicalProcedure != null){
			Map<String, SbcScenarioDTO> sbcScenarioDTOMap = planDisplayUtil.getSbcCost(hiosPlanIds, PlanDisplayEnum.InsuranceType.HEALTH.toString(), Integer.valueOf(coverageStartDate));
			if(!sbcScenarioDTOMap.isEmpty()){                                                                       
			for(PlanRateBenefit plan : planRateBenefitList){
					String hiosPlanNo = plan.getHiosPlanNumber();
					SbcScenarioDTO sbcScenarioDTO  = sbcScenarioDTOMap.get(hiosPlanNo);
					if(sbcScenarioDTO != null){
						plan.setSbcScenarioDTO(sbcScenarioDTO);
					}
				}
			}
		}	
	}

 public List<IndividualPlan> getPlansFromPlanScoreResponseList(String planScoreResponse) {
		List<IndividualPlan> individualPlans = new ArrayList<IndividualPlan>();
		if(StringUtils.isNotBlank(planScoreResponse)){
			List<PdWSResponse> pdWSResponseList = platformGson.fromJson(planScoreResponse, new TypeToken<List<PdWSResponse>>() {}.getType());			
			for(PdWSResponse pdWSResponse:pdWSResponseList){
				if(pdWSResponse != null){
					IndividualPlan plan = new IndividualPlan();
					plan.setSmartScore(pdWSResponse.getScore());
					plan.setExpenseEstimate(pdWSResponse.getExpenseEstimate());
					plan.setIssuerPlanNumber(pdWSResponse.getHiosId());
					plan.setEstimatedTotalHealthCareCost(pdWSResponse.getEstimatedCost());
					
					if(pdWSResponse.getPlanTab() != null){
						plan.setPlanTab(pdWSResponse.getPlanTab());	
					}									
										
					List<PdWSResponse.Provider> providerList = pdWSResponse.getProviders();
					List<HashMap<String, Object>> doctors = new ArrayList<HashMap<String, Object>>();
					if(providerList != null && !providerList.isEmpty()){
						for(PdWSResponse.Provider provider : providerList){
							HashMap<String, Object> doctor = new HashMap<String, Object>();
							doctor.put("providerId", provider.getId());
							doctor.put("providerName", provider.getName());
							doctor.put("locationId", provider.getLocationId());
							doctor.put("providerType", provider.getProviderType());
							String coveredByPlan = provider.getCoveredByPlan();
							if("Y".equals(coveredByPlan)){
								doctor.put("networkStatus", PlanDisplayUtil.IN_NETWORK);
							}else{
								doctor.put("networkStatus", PlanDisplayUtil.OUT_NETWORK);
							}	
							doctors.add(doctor);
						}
					}					
					plan.setDoctors(doctors);
					
					List<PdWSResponse.Benefit> benefits = pdWSResponse.getBenefits();
					Map<String, String> benefitsCoverage = new HashMap<String, String>();
					if(benefits!=null && !benefits.isEmpty()){
						for(PdWSResponse.Benefit benefit : benefits){													
							String name = benefit.getName();
							String coveredByPlan = benefit.getCoveredByPlan();
							if("Y".equals(coveredByPlan)){
								benefitsCoverage.put(name, "GOOD");
							}else{
								benefitsCoverage.put(name, "NA");
							}													
						}
					}					
					plan.setBenefitsCoverage(benefitsCoverage);
					
					List<DrugDTO> prescriptionResponseList = new ArrayList<DrugDTO>();
					if(pdWSResponse.getDrugs()!=null && !pdWSResponse.getDrugs().isEmpty()){
						for(PdWSResponse.Drug drugObj : pdWSResponse.getDrugs()){
							DrugDTO drug = new DrugDTO();
							drug.setDrugID(drugObj.getId());
							drug.setDrugName(drugObj.getName());
							drug.setDrugType(drugObj.getType());
							drug.setIsDrugCovered(drugObj.getCoveredByPlan());
							drug.setDrugFairPrice(drugObj.getFairPrice());
							drug.setDrugDosage(drugObj.getDosage());
							drug.setDrugCost(drugObj.getCost());
							drug.setGenericFairPrice(drugObj.getGenericFairPrice());
							drug.setGenericDosage(drugObj.getGenericDosage());
							drug.setGenericCost(drugObj.getGenericCost());
							drug.setIsGenericCovered(drugObj.getGenericCoveredByPlan());
							drug.setDrugTier(drugObj.getTier());
							drug.setGenericID(drugObj.getGenericId());
							drug.setGenericTier(drugObj.getGenericTier());
							prescriptionResponseList.add(drug);
						}
					}					
					plan.setPrescriptionResponseList(prescriptionResponseList);
					
					individualPlans.add(plan);
				}
			}			
		}
		return individualPlans;
 	}
 
 	public void addDisenrolledGroups(List<PdOrderItem> orderItems, PdOrderResponse pdOrderResponse, Ind71GRequest ind71gRequest) {
		List<IND71GEnrollment> enrollmentGroups = ind71gRequest.getEnrollments();
		if(pdOrderResponse.getPdOrderItemDTOList() == null) {
			pdOrderResponse.setPdOrderItemDTOList(new ArrayList<PdOrderItemDTO>());
		}
		for(IND71GEnrollment enrollmentGroup : enrollmentGroups)	{
			int totalMembers = enrollmentGroup.getMembers().size();
			int disenrolledMembers = 0;
			for(IND71GMember enrollmentMember : enrollmentGroup.getMembers())	{
				if("Y".equalsIgnoreCase(enrollmentMember.getDisenrollMemberFlag())) {
					disenrolledMembers++;
				}
			}
			if(disenrolledMembers == totalMembers) {
				PdOrderItemDTO pdOrderItemDTO = new PdOrderItemDTO();
				pdOrderItemDTO.setPreviousEnrollmentId(enrollmentGroup.getEnrollmentId());
				pdOrderItemDTO.setDisenrollAllMembers(true);
				pdOrderResponse.getPdOrderItemDTOList().add(pdOrderItemDTO);
			}
		}
	}
	
	
}
